<?php

include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
$modName = 'time_table';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
//isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
$page = isset($_GET['Page']) ? $_GET['Page'] : '';
if ($id != $_SESSION['admin_session_secure']['user_id']) {
    Redirect(make_admin_url('home'));
}
#handle actions here.
switch ($action):
    case 'list':
        // Get Staff Time Table
        $obj = new timeTable();
        $times = $obj->getStaffTiming($id);
        $temp = array();
        foreach ($times as $time_table) {
            $ts = strtotime($time_table->time_from);
            $temp[$ts] = $time_table;
        }
        ksort($temp);
        $time_tables = $temp;
        break;
    default:break;
endswitch;
