<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');

$modName = 'student';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '';
//isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['type']) ? $type = $_GET['type'] : $type = 'student_detail';
isset($_GET['session_id']) ? $session_id = $_GET['session_id'] : $session_id = '';
isset($_GET['ct_section']) ? $ct_section = $_GET['ct_section'] : $ct_section = '';
isset($_GET['month']) ? $month = $_GET['month'] : $month = date('m');
isset($_GET['doc_id']) ? $doc_id = $_GET['doc_id'] : $doc_id = '';

$page = isset($_GET['Page']) ? $_GET['Page'] : '';


#get Student Info 

$checkUserIdExistOrNot = student :: checkUserIdExistOrNot($_GET['id']);

if ($checkUserIdExistOrNot == false) {
    Redirect(make_admin_url('classes'));
}

$QueryInfo = new student();
$s_info = $QueryInfo->getStudent($id);


#get students current session
$CurrObj = new studentSession();
$current_session = $CurrObj->getStudentCurrentSessionId($school->id, $id);
if (empty($session_id)):
    $session_id = $current_session;
endif;

#get student permanent address
$QueryP = new studentAddress();
$s_p_ad = $QueryP->getStudentAddressByType($id, 'permanent');

#get student correspondence address
$QueryC = new studentAddress();
$s_c_ad = $QueryC->getStudentAddressByType($id, 'correspondence');

#get student correspondence address
$QueryF = new studentFamily();
$s_f = $QueryF->getStudentFamilyDeatils($id);

#get student Sibling Info
$QueryS = new studentSibling();
$QueryS->getStudentSibling($id);

#get student Previous School Info
$P_school = new studentPreviousSchool();
$P_school->getStudentPreviousSchool($id);

#get student Document Info
$S_doc = new studentDocument();
$S_doc->getStudentDocument($id);

#Get Student previous session
$QuerySList = new studentSession();
$Session_list = $QuerySList->getStudentSessionList($id);

#Get Session info
$sessionIQuery = new session();
$session_info = $sessionIQuery->getRecord($session_id);

#Get Student Section Name
$sectionQuery = new studentSession();
$section_name = $sectionQuery->checkStudentSection($session_id, $id);

#Get Student previous Fee List
$QueryFList = new studentFee();
$Fee_list = $QueryFList->listFeeSingleSutdent($session_id, $id);


#Get Student print doc List
$QueryDocList = new SchoolDocumentPrintHistory();
$Print_list = $QueryDocList->listPrintingdocs($school->id, $id, $session_id);

/* Get school document template for student id card */
$QueryDocs = new SchoolDocumentTemplate();
$student_icard = $QueryDocs->get_school_single_document($school->id, 1);

/* Get school document template for student id character certificate */
$QueryDocsChr = new SchoolDocumentTemplate();
$certificate = $QueryDocsChr->get_school_single_document($school->id, 6);

/* Get the student exam list */
$QueryExam = new examination();
$ExamList = $QueryExam->studentAllExam($id, $session_id);

/* Get exams Groups */
$QueryObj = new examinationGroup();
$result = $QueryObj->listAll($school->id, $session_id);

$month_array = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');

#handle actions here.
switch ($action):
    case 'list':
        if (!$_GET['id']) {
            Redirect(make_admin_url('classes'));
        }
        break;

    default:break;
endswitch;
?>
