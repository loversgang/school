<?php

include_once(DIR_FS_SITE . 'include/functionClass/remarksClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
$modName = 'remarks';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];

$page = isset($_GET['Page']) ? $_GET['Page'] : '';
$staff_id = $_SESSION['admin_session_secure']['user_id'];

switch ($action) :
    case'list':
        $query = new remarks();
        $remarks = $query->remarks($staff_id);
        break;
    case'insert':
        $query = new timeTable;
        $session_ids = $query->getSessionIdByStaffId($staff_id);

        if (isset($_POST['submit'])) {
            $query = new remarks();
            $query->saveRemarks($_POST);
            $admin_user->set_pass_msg('Remarks Added Successfully');
            Redirect(make_admin_url('remarks', 'list', 'list'));
        }
        break;
    case 'delete':
        $query = new remarks();
        $query->delete_remark($id);
        $admin_user->set_pass_msg('Remarks Deleted Successfully');
        Redirect(make_admin_url('remarks', 'list', 'list'));
        break;

    case 'update':
        
        $query = new remarks();
        $remark = $query->remark_using_id($id);

        $query = new timeTable;
        $session_ids = $query->getSessionIdByStaffId($staff_id);

        $query = new studentSession();
        $student_ids = $query->get_student_id_using_session_id_no_section($remark->session_id);

        if (isset($_POST['submit'])) {
            $query = new remarks();
            $query->saveRemarks($_POST);
            $admin_user->set_pass_msg('Remarks Edit Successfully');
            Redirect(make_admin_url('remarks', 'list', 'list'));
        }
        break;
endswitch;
?>