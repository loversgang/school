<?php

include_once(DIR_FS_SITE . 'include/functionClass/homeworkClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
$modName = 'reports';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
$staff_id = isset($_GET['staff_id']) ? $_GET['staff_id'] : 0;

$page = isset($_GET['Page']) ? $_GET['Page'] : '';
$login_staff_id = $_SESSION['admin_session_secure']['user_id'];
switch ($action):
    case 'list':
        $object = new timeTable();
        $sec = $object->getSectionByStaff_session_id_single($login_staff_id, $_GET['session_id']);

        $query = new studentSession;
        $student_ids = $query->get_student_id_using_session_id($_GET['session_id'], $_GET['sec']);
        


        $query = new homework();
        $homework_using_id = $query->listsHomeWork($id);
        $subject = get_object('subject_master', $homework_using_id->subject);

        if (isset($_POST['submit'])) {
            $array = array();
            foreach ($_POST['final_array'] as $key => $save_report) {
                $array['student_id'] = $key;
                $array['assignment_id'] = $_POST['assignment_id'];
                $array['school_id'] = $_POST['school_id'];
                $array['staff_id'] = $login_staff_id;
                $array['status'] = $save_report['status'];
                $array['grade'] = $save_report['grade'];
                $query = new reports();
                if ($query->check_assingnment_student_id_exist($_POST['assignment_id'], $key)) {
                    $query = new reports();
                    $query->Data['status'] = $save_report['status'];
                    $query->Data['grade'] = $save_report['grade'];
                    $query->Where = "WHERE `assignment_id` = '" . $_POST['assignment_id'] . "' AND `student_id` = '$key'";
                    $query->UpdateCustom();
                } else {
                    $query = new reports();
                    $query->saveReports($array);
                }
            }
            $admin_user->set_pass_msg('Student Reports Added Successfully');
            Redirect(make_admin_url('reports', 'list', 'list&id=' . $_GET['id'] . '&session_id=' . $_GET['session_id'] . '&sec=' . $_GET['sec']));
        }
        break;

    case 'delete':
        $query = new reports;
        $query->delete_using_id($_GET['delete_id']);
        $admin_user->set_pass_msg('Student Reports Deleted Successfully');
        Redirect(make_admin_url('reports', 'list', 'list&id=' . $_GET['id'] . '&session_id=' . $_GET['session_id']));
        break;


    default:break;
endswitch;
?>
