<?php

include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');



$modName = 'exam_results';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
$staff_id = isset($_GET['staff_id']) ? $_GET['staff_id'] : 0;
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';

$page = isset($_GET['Page']) ? $_GET['Page'] : '';
$login_staff_id = $_SESSION['admin_session_secure']['user_id'];
#handle actions here.
switch ($action):
    case'list':
        /* Get exams */
        $QueryObj = new examination();
        $result = $QueryObj->listAll_for_staff($login_staff_id, $school->id, $session_id);
        /* Get exams sessions */
        $QuerySessObj = new examination();
        $Session_list = $QuerySessObj->getExamSession($school->id);
        break;

    case 'view':
        $QueryS = new examination();
        $exam = $QueryS->getRecord($id);

        $QuerySession = new session();
        $object = $QuerySession->getRecord($exam->session_id);

        $QueryOb = new examination();
        $result = $QueryOb->examination_result($id);
        break;

    default:break;
endswitch;
?>
