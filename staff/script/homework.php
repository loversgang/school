<?php

include_once(DIR_FS_SITE . 'include/functionClass/homeworkClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
$modName = 'homework';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
$staff_id = isset($_GET['staff_id']) ? $_GET['staff_id'] : 0;

$page = isset($_GET['Page']) ? $_GET['Page'] : '';
$login_staff_id = $_SESSION['admin_session_secure']['user_id'];
switch ($action):
    case'list':
        $query = new homework();
        $lists = $query->listsHomeWork();

        break;
    case'insert':
        $object = new timeTable();
        $lists = $object->getSessionIdByStaffId($login_staff_id);


        if (isset($_POST['submit'])) {
            $query = new homework();
            $query->saveHomeWork($_POST, $login_staff_id);
            $admin_user->set_pass_msg('Assignments Added Successfully');
            Redirect(make_admin_url('homework', 'list', 'list'));
        }
        break;
    case'delete':
        $query = new homework();
        $query->deleteHomeWork($id);
        $admin_user->set_pass_msg('Assignments Deleted Successfully');
        Redirect(make_admin_url('homework', 'list', 'list'));
        break;
    case'update':
        $query = new homework();
        $list = $query->listsHomeWork($id);
        $object = new timeTable();
        $list_subjects = $object->getSessionIdByStaffId($login_staff_id);
        if ($list->file) {
            $extensions = array('jpeg', 'jpg', 'png', 'bmp', 'gif');
            $file_extension = pathinfo($list->file);
            $extension = $file_extension['extension'];
        }

        if (isset($_POST['submit'])) {
            $query = new homework();
            $query->saveHomeWork($_POST, $login_staff_id, $list->file);
            $admin_user->set_pass_msg('Assignments Updated Successfully');
            Redirect(make_admin_url('homework', 'list', 'list'));
        }
        break;
    case 'reports':
        $query = new studentSession;
        $student_ids = $query->get_student_id_using_session_id($_GET['session_id']);

        if (isset($_POST['submit'])) {
            $query = new reports;
            $query->saveReport($_POST);
            $admin_user->set_pass_msg('Student Reports Added Successfully');
            Redirect(make_admin_url('homework', 'reports', 'reports&id=' . $_GET['id'] . '&session_id=' . $_GET['session_id']));
        }
        break;
    default:break;
endswitch;
?>
