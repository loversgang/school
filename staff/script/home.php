<?php

include_once(DIR_FS_SITE . 'include/functionClass/circularsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/homeworkClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEventClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/applicationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/leave_typesClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/messageClass.php');

$modName = 'home';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
$page = isset($_GET['Page']) ? $_GET['Page'] : $page = '';
if ($id != $_SESSION['admin_session_secure']['user_id']) {
    Redirect(make_admin_url('home'));
}
#handle actions here.
switch ($action):
    case 'list':
        $CurrObj = new studentSession();
        $session_id = $CurrObj->getStudentCurrentSession($school->id, $id, TRUE);

        $query = new circulars();
        $circulars = $query->listLimitedCirculars();

        // Get Syllabus
        $obj = new event;
        $events = $obj->getEventsBySessionId($session_id);

        // Get Staff Time Table
        $obj = new timeTable();
        $times = $obj->getStaffTiming($id);
        $temp = array();
        foreach ($times as $time_table) {
            $ts = strtotime($time_table->time_from);
            $temp[$ts] = $time_table;
        }
        ksort($temp);
        $time_tables = $temp;

        $query = new homework;
        $homeworks = $query->listsHomeWork();

        $query = new application;
        $list_applications = $query->listApplication($_SESSION['admin_session_secure']['user_id']);

        $obj = new school_header;
        $inbox_messages = $obj->getInBoxMessagesStaff($school->id, $id);

        $query = new staff;
        $list_staff_dob = $query->getStaffDOB($school->id);
        

        $query = new staff();
        $record = $query->getSchoolStaffDetailActiveDeactive($school->id, 1);

        break;
    case 'update':
        break;
    case 'view':
        break;
    case'print':
        break;
    default:break;
endswitch;
