<?php

include_once(DIR_FS_SITE . 'include/functionClass/complaintsClass.php');
$modName = 'referers';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : '';

$page = isset($_GET['Page']) ? $_GET['Page'] : '';
$staff_id = $_SESSION['admin_session_secure']['user_id'];

switch ($action) :
    case'list':
        $query = new referers();
        $referers = $query->referers($staff_id);
        break;

    case'complaints':
        $query = new complaints();
        $complaints = $query->complaints_using_complaint_id($id);
        break;

    case'resolve':
        $query = new complaints();
        $query->complaint_solved($_GET['complaint_id'], $_GET['action']);
        $admin_user->set_pass_msg('Complaint Resolved Successfully');
        Redirect(make_admin_url('referers', 'complaints', 'complaints&id=' . $_GET['complaint_id']));
        break;

    case'resolved':
        $query = new complaints();
        $complaints = $query->complaint_solved($_GET['complaint_id'], $_GET['action']);
        $admin_user->set_pass_msg('Complaint Resolved Successfully');
        Redirect(make_admin_url('referers', 'complaints', 'complaints&id=' . $_GET['complaint_id']));
        break;

    case 'message':
        $query = new referers_messages;
        $referers_messages = $query->all_referers_message($id, $staff_id);


        if (isset($_POST['submit'])) {
            $query = new referers_messages;
            $query->save_referers_messages($_POST);
            $admin_user->set_pass_msg('Referer Message Sent Successfully');
            Redirect(make_admin_url('referers', 'message', 'message&id=' . $id));
        }
        break;

endswitch;
?>