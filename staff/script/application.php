<?php

include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/applicationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/leave_typesClass.php');

$modName = 'application';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
$page = isset($_GET['Page']) ? $_GET['Page'] : '';
#handle actions here.
switch ($action):
    case'list':
        $query = new application;
        $list_applications = $query->listApplication($_SESSION['admin_session_secure']['user_id']);
        break;

    case'insert':
        $object = new staff;
        $s_info = $object->getLoginStaffInformation($_SESSION['admin_session_secure']['user_id']);
        if (isset($_POST['submit'])) {
            $to = SCHOOL_EMAIL;
            $subject = $_POST['subject'];
            $message = '<div id=":bn" class="a3s" style="overflow: hidden;">

<p style="line-height:20.7999992370605px">Hi <strong>Admin</strong>,</p>

<p style="line-height:20.7999992370605px">You received school leave Application from ' . $s_info->title . ' ' . $s_info->first_name . ' ' . $s_info->last_name . '</p>

<p style="line-height:20.7999992370605px">' . $_POST['message'] . '</p>


<p style="line-height:20.7999992370605px">Best Regards,</p>

<p style="line-height:20.7999992370605px"><strong>' . SITE_NAME . '</strong></p><div class="yj6qo"></div><div class="adL">


</div></div>';

            $send_email = send_email_by_cron($to, $message, $subject, $school->school_name);

            $query = new application;
            $_POST['staff_id'] = $_SESSION['admin_session_secure']['user_id'];
            $_POST['send_to'] = SCHOOL_EMAIL;
            $_POST['on_date'] = date('Y-m-d');
            $query->saveData($_POST);
            $admin_user->set_pass_msg('Application SendSuccessfully');
            Redirect(make_admin_url('application', 'list', 'list'));
        }

        $query = new leave_types();
        $list_leave_types = $query->lists();
        break;
    default:break;
endswitch;
?>
