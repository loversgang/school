<?php

include_once(DIR_FS_SITE . 'include/functionClass/syllabusClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
$modName = 'syllabus';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
//isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
$page = isset($_GET['Page']) ? $_GET['Page'] : '';

$QueryObjQ = new session();
$AllSessYear = $QueryObjQ->listOfYearSess($school->id);
if (empty($sess_int)):
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
endif;
#handle actions here.
switch ($action):
    case'list':
        $QueryObj = new syllabus();
        $syllabus_list = $QueryObj->listSyllabus($sess_int);
        break;
    default:
        break;
endswitch;
