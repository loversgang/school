<?php

include_once(DIR_FS_SITE . 'include/functionClass/schoolEventClass.php');

$modName = 'event';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
//isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
$page = isset($_GET['Page']) ? $_GET['Page'] : '';
if ($id != $_SESSION['admin_session_secure']['user_id']) {
    Redirect(make_admin_url('home'));
}
#handle actions here.
switch ($action):
    case 'list':
        // Get Events
        $obj = new event;
        $events = $obj->getSchoolEvents();
        break;
    case 'update':
        break;
    case 'view':
        break;
    case'print':
        break;
    default:break;
endswitch;
