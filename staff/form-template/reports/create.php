<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Add Student Reports
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Add Student Reports</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Add Student Reports</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <form class="form-horizontal" method="POST" id="validation">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="title">Student Name:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="student_name" Value="<?php echo $student->first_name . ' ' . $student->last_name ?>" class="form-control validate[required]"  disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="status">Status:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="status" class="form-control validate[required]" id="title" placeholder="Enter Status">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="grade">Grade:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="grade" class="form-control validate[required]"  placeholder="Enter Grade">
                                        </div>
                                    </div>
                                    <div class="form-group">        
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <input type="hidden" name="school_id" value="<?php echo $school->id ?>" />
                                            <input type="hidden" name="student_id" value="<?php echo $_GET['student_id'] ?>" />
                                            <input type="hidden" name="assignment_id" value="<?php echo $_GET['assignment_id'] ?>" />
                                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                            <a href="<?php echo make_admin_url('reports', 'list', 'list&id=' . $_GET['assignment_id'] . '&session_id=' . $_GET['session_id']) ?>" class="btn btn-default">Cancel</a>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>