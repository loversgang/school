<?php
include_once(DIR_FS_SITE . 'include/functionClass/homeworkClass.php');
if (isset($_POST)) {
    extract($_POST);
    if ($action == 'show_report_inputs') {
        $query = new homework();
        $homework_using_id = $query->listsHomeWork($id);
        $subject = get_object('subject_master', $homework_using_id->subject);
        $subject = $subject->name;
        ?>
        <form class="form-horizontal" method="POST" id="validation">
            <div class="form-group">
                <label class="control-label col-sm-2" for="title">Student Name:</label>
                <div class="col-sm-10">
                    <input type="text" name="student_name" Value="<?php echo $student_name ?>" class="form-control validate[required]"  disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="title">Title:</label>
                <div class="col-sm-10">
                    <input type="text" name="title" class="form-control validate[required]" id="title" placeholder="Enter Title">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="title">Subject:</label>
                <div class="col-sm-10">
                    <input type="text" name="subject" value="<?php echo $subject ?>" class="form-control validate[required]" id="title" placeholder="Enter Subject" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="title">Date:</label>
                <div class="col-sm-10">
                    <input type="date" placeholder="Enter Assignments Date" name="date_of_assignment" class="datepicker form-control validate[required]" id="date">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="status">Status:</label>
                <div class="col-sm-10">
                    <input type="text" name="status" class="form-control validate[required]" id="title" placeholder="Enter Status">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="grade">Grade:</label>
                <div class="col-sm-10">
                    <input type="text" name="grade" class="form-control validate[required]"  placeholder="Enter Grade">
                </div>
            </div>
            <div class="form-group">        
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="hidden" name="school_id" value="<?php echo $school->id ?>" />
                    <input type="hidden" name="student_id" value="<?php echo $student_id ?>" />
                    <input type="hidden" name="assignment_id" value="<?php echo $id ?>" />
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    <a href="<?php echo make_admin_url('homework') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </form>

        <?php
    }
}
?>