<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Student Reports
                    <?php
                    $class = get_object('session', $_GET['session_id']);
                    echo $class->title . ' ' . $_GET['sec'];
                    ?>
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="javascript:;">Student Reports</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Student Reports (<?php echo $subject->name ?>)</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div style ="overflow : hidden">
                                    <form method="POST">
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>Roll No</th>
                                                    <th>Student</th>
                                                    <th>Status</th>
                                                    <th>Grade</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($student_ids as $student_id) { ?>
                                                    <?php
                                                    $student = get_object('student', $student_id['student_id']);
                                                    if (is_object($student)) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $student_id['roll_no'] ?></td>
                                                            <td>
                                                                <?php
                                                                $student = get_object('student', $student_id['student_id']);
                                                                echo $student->first_name . ' ' . $student->last_name;
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $reports_s_g = reports :: get_status_grade_using_assignment_student_id($id, $student->id);
                                                                ?>
                                                                <input type="text" class="form-control" name="final_array[<?php echo $student_id['student_id'] ?>][status]" Value="<?php echo isset($reports_s_g->status) ? $reports_s_g->status : '' ?>" />
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $reports_s_g = reports :: get_status_grade_using_assignment_student_id($id, $student->id);
                                                                ?>
                                                                <input type="text" class="form-control" name="final_array[<?php echo $student_id['student_id'] ?>][grade]" Value="<?php echo isset($reports_s_g->grade) ? $reports_s_g->grade : '' ?>" />
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <div class="form-group">        
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <input type="hidden" name="school_id" value="<?php echo $school->id ?>" />
                                                <input type="hidden" name="assignment_id" value="<?php echo $_GET['id'] ?>" />
                                                <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>