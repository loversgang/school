<style>
    .tiles .tile {
        height : 60px;
    }
    .tiles .tile .tile-body i {
        margin-top: 0px;
        display: block;
        font-size: 30px;
        line-height: 15px;
        text-align: center;
    }

</style>
<div class="tile bg-blue <?php echo ($section == 'insert') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('homework', 'list', 'list') ?>">
        <div class="corner"></div>
        <div class="tile-body">
            <!--<i class="fa fa-plus"></i>-->
        </div>
        <div class="tile-object">
            <div class="name">
                <center>List Assignment</center>
            </div>
        </div>
    </a> 
</div>