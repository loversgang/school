<?php
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
if (isset($_POST)) {
    extract($_POST);
    if ($act == 'get_session_student') {
        $QueryStu = new studentSession();
        $AllStu = $QueryStu->sessionStudents($session_id);
        ?>
        <?php foreach ($AllStu as $key => $list) { ?>
            <div class="col-md-6">
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="col-md-4">
                            <?
                            if (is_object($list) && $list->photo):
                                $im_obj = new imageManipulation()
                                ?>
                                <img src="<?= $im_obj->get_image('student', 'medium', $list->photo); ?>" class="img-responsive" style="width: 120px;height: 120px" />
                            <? else: ?>
                                <img src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" class="img-responsive" style="width: 120px;height: 120px" />
                            <? endif; ?>
                        </div>
                        <div class="col-md-8">
                            <span class="name"><?php echo $list->first_name ?></span><br /><br />
                            <span class="nam"><?php echo $list->address1 ?></span><br /><br />
                            <span class="name"><a class="btn btn-success btn-xs" href="<?php echo make_admin_url('student', 'list', 'list&id=' . $list->id) ?>">
                                    <i class="fa fa-file-text"></i>
                                    View Profile
                                </a></span>
                            <br /><br />
                            <a class="text text-success" href="<?php echo make_admin_url('messages', 'compose', 'compose', '&user_id=' . $list->id . '&user_type=student') ?>"><i class="fa fa-envelope"></i> Send Message</a>
                        </div>
                        <div class="clearfix"></div>
                    </li>
                </ul>
            </div>
            <?php
        }
    }
}
?>