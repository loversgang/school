<div class="page-content-wrapper">
    <div class="page-content">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    My Students
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">My Students</a>
                        </li>
                    </ul>
                </div>
                <div class="row tab-pane active" id="tab1">
                    <ul class="thumbnails product-list-inline-small">
                        <?php if (count($lists) !== 0) { ?>
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <select class="form-control" name="select_category" id="select_category">
                                    <?php
                                    foreach ($lists as $list) {
                                        $session = get_object('session', $list['session_id']);
                                        ?>
                                        <option value="<?php echo $list['session_id'] ?>"><?php echo $session->title ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <br />
                        <?php } else { ?>
                            <ul class="list-group">
                                <li class="list-group-item">
                                <center>
                                    <span class="name">Sorry, No Record Found..!</span>
                                </center>
                                </li>
                            </ul>
                        <?php } ?>

                        <div id="display_students"></div>
                    </ul>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="//code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(function () {
        $('select#select_category').change(function () {
            var session_id = $(this).val();
            $('#display_students').html('<div style="width:100%;text-align:center;padding-top:5px;"><i class="fa fa-spinner fa-pulse fa-5x"></i></div>');
            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=classes'); ?>', {act: 'get_session_student', session_id: session_id}, function (data) {
                $('#display_students').html(data);
            });
        }).change();
    });
</script>