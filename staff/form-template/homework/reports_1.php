<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Assignments
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Assignments</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                /* display message */
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Assignments</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (count($lists) !== 0) { ?>
                                <div style="overflow-y: hidden">
                                    <table class="table table-striped table-bordered table-hover" id="sample_">
                                        <thead>
                                            <tr>
                                                <th>Status</th>
                                                <th>Title</th>
                                                <th>Subject</th>
                                                <th>Class</th>
                                                <th>Assignment Date</th>
                                                <th>Submission Date</th>
                                            </tr>
                                        </thead>
                                        <?php foreach ($lists as $key => $list) { ?>
                                            <tr>
                                                <td>
                                                    <?php if ($list['mark'] == 0) { ?>

                                                        <button type="button" class="btn btn-xs green icn-only tooltips" id="mark" data-id="<?php echo $list['id'] ?>" title="Set Project as Completed">Completed</button>
                                                        <button type="button" class="btn btn-xs red icn-only tooltips" id="marked" data-id="<?php echo $list['id'] ?>" style="display:none"   title="Set Project as Uncomplete">Uncomplete</button>

                                                    <?php } else { ?>
                                                        <button type="button" class="btn btn-xs green icn-only tooltips" id="mark"  style="display:none" data-id="<?php echo $list['id'] ?>"  title="Set Project as Completed">Completed</button>
                                                        <button type="button" class="btn btn-xs red icn-only tooltips" id="marked" data-id="<?php echo $list['id'] ?>"  title="Set Project as Uncomplete">Uncomplete</button>
                                                    <?php } ?>
                                                </td>
                                                <td><?php echo ucfirst($list['title']) ?></td>
                                                <td>
                                                    <?php
                                                    $subject_master = get_object('subject_master', $list['subject']);
                                                    echo $subject_master->name
                                                    ?>    
                                                </td>
                                                <td>
                                                    <?php
                                                    $class = get_object('session', $list['class']);
                                                    echo $class->title;
                                                    ?>
                                                </td>	
                                                <td><?php echo $list['date'] ?></td>
                                                <td><?php echo $list['submission'] ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>                          
                                    </table>
                                </div>
                                <?php } else { ?>
                                    <div class="alert alert-info">You added zero Assignments..!</div>
                                <?php } ?>
                            </div>
                        </div>                                                
                    </div>
                </div>
                <?php require 'model.php'; ?>
            </div>
        </section>
    </div>
</div>
<?php require 'script.php'; ?>