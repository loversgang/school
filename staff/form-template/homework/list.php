<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Assignments
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Assignments</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Assignments</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (count($lists) !== 0) { ?>
                                    <div style="overflow-y: hidden">
                                        <table class="table table-striped table-bordered table-hover" id="sample_">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Subject</th>
                                                    <th>Class</th>
                                                    <th>Date</th>
                                                    <th>Submission Date</th>
                                                    <th>Detail</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <?php foreach ($lists as $key => $list) { ?>
                                                <tr>
                                                    <td><?php echo ucfirst($list['title']) ?></td>
                                                    <td>
                                                        <?php
                                                        $subject_master = get_object('subject_master', $list['subject']);
                                                        echo $subject_master->name
                                                        ?>    
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $class = get_object('session', $list['class']);
                                                        echo $class->title . ' ' . $list['section'];
                                                        ?>
                                                    </td>	
                                                    <td><?php echo date('d M Y', strtotime($list['date'])) ?></td>
                                                    <td><?php echo $list['submission'] ?></td>
                                                    <td>
                                                        <span class="col-md-6">
                                                            <?php echo substr(ucfirst($list['detail']), 0, 10) ?><?php echo (strlen($list['detail']) > 10) ? '....' : '' ?>
                                                        </span>
                                                        <span class="col-md-6">
                                                            <a href="javascript:;" data-detail="<?php echo ucfirst($list['detail']) ?>" data-title="<?php echo ucfirst($list['title']) ?>" data-file="<?php echo DIR_WS_SITE_UPLOAD . 'file/homework/' . $list['file'] ?>" data-valid="<?php echo $list['file'] ?>" id="view" class="btn btn-xs blue tooltips" title="View Assignments Details">View</a>
                                                        </span>
                                                    </td>
                                                    <td style='text-align:right;'>
                                                        <div class="col-md-5">
                                                            <a class="btn btn-xs blue tooltips" href="<?php echo make_admin_url('reports', 'list', 'list&id=' . $list['id'] . '&session_id=' . $list['class'] . '&sec=' . $list['section']) ?>" title="click here to view this reports">View Reports</a>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <a class="btn btn-xs blue tooltips" href="<?php echo make_admin_url('homework', 'update', 'update&id=' . $list['id']) ?>" title="click here to edit this record"><i class="fa fa-edit icon-white"></i></a>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <a class="btn btn-xs red icn-only tooltips" href="<?php echo make_admin_url('homework', 'delete', 'delete', '&id=' . $list['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="click here to delete this record"><i class="fa fa-remove icon-white"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>                          
                                        </table>
                                    </div>
                                <?php } else { ?>
                                    <div class="alert alert-info">You added zero Assignments..!</div>
                                <?php } ?>
                            </div>
                        </div>                                                
                    </div>
                </div>
                <?php require 'model.php'; ?>
            </div>
        </section>
    </div>
</div>
<?php require 'script.php'; ?>