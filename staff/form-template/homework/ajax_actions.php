<?php
include_once(DIR_FS_SITE . 'include/functionClass/homeworkClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');

if (isset($_POST)) {
    extract($_POST);
    if ($action == 'mark') {
        $query = new homework();
        $query->saveMark($id, 1);
    }

    if ($action == 'marked') {
        $query = new homework();
        $query->saveMark($id, 0);
    }

    if ($action == 'delete_img') {
        $query = new homework();
        $query->removeImg($id);
    }

    if ($action == 'get_section_session_id') {
        $object = new timeTable();
        $section = $object->getSectionByStaff_session_id_all($staff_id, $session_id);
        ?>
        <div class="form-group">
            <label class="control-label col-sm-2" for="section">Section</label>
            <div class="col-sm-10">  
                <select name="section" id="section" class="form-control">
                    <?php
                    foreach ($section as $list) {
                        ?>
                        <option value="<?php echo $list['section'] ?>"><?php echo $list['section'] ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php
    }
}
?>