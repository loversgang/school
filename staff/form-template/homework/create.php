<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Assignments
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Assignments</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="clearfix"></div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="fa fa-plus"></i>Add Assignments</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="validation">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="title">Title:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="title" class="form-control validate[required]" id="title" placeholder="Enter Title">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="subject">Subject</label>
                                        <div class="col-sm-10">  
                                            <select name="subject" id="subject" class="form-control validate[required]">
                                                <option value="">Select Subject</option>
                                                <?php
                                                foreach ($lists as $list) {
                                                    $subject_master = get_object('subject_master', $list['subject']);
                                                    ?>
                                                    <option value="<?php echo $list['subject'] ?>"><?php echo $subject_master->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="detail">Detail</label>
                                        <div class="col-sm-10">          
                                            <textarea class="form-control validate[required]" name="detail" id="detail" placeholder="Enter Detail"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="class">Class</label>
                                        <div class="col-sm-10">  
                                            <select name="class" id="class" class="form-control validate[required]" data-staff_id="<?php echo $login_staff_id ?>">
                                                <option value="">Select Class</option>
                                                <?php
                                                foreach ($lists as $list) {
                                                    $session = get_object('session', $list['session_id']);
                                                    ?>
                                                    <option value="<?php echo $list['session_id'] ?>"><?php echo $session->title ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="section"></div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="upload_file">Upload File:</label>
                                        <div class="col-sm-10">
                                            <input type="file" name="file"  id="file">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="title">Date:</label>
                                        <div class="col-sm-10">
                                            <input type="text" placeholder="Enter Assignments Date" name="date" class="datepicker form-control validate[required]" id="date">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="submission">Date of Submission:</label>
                                        <div class="col-sm-10">
                                            <input type="text" placeholder="Date of Submission" name="submission" class="datepicker form-control validate[required]" id="submission">
                                        </div>
                                    </div>
                                    <div class="form-group">        
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="status" id="status" value="1" checked>Status</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">        
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <input type="hidden" name="school_id" value="<?php echo $school->id ?>" />
                                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                            <a href="<?php echo make_admin_url('homework') ?>" class="btn btn-default">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.standalone.min.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script>
    $('.datepicker').datepicker()
</script>
<script>
    $(document).on('change', '#class', function () {
        var session_id = $(this).val();
        var staff_id = $(this).attr('data-staff_id');
        $('#section').show();
        if (session_id) {
            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=homework'); ?>', {action: 'get_section_session_id', session_id: session_id, staff_id: staff_id}, function (data) {
                $('#section').html(data);
            });
        } else {
            $('#section').hide();
        }
    });
</script>