<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Assignments
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Assignments</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="clearfix"></div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="fa fa-plus"></i>Edit Assignments</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="validation">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="title">Title:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="title" class="form-control validate[required]" id="title" placeholder="Enter Title" value="<?php echo $list->title ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="subject">Subject</label>
                                        <div class="col-sm-10">  
                                            <select name="subject" id="subject" class="form-control validate[required]">
                                                <option value="">Select Subject</option>
                                                <?php
                                                foreach ($list_subjects as $list_subject) {
                                                    $subject_master = get_object('subject_master', $list_subject['subject']);
                                                    ?>
                                                    <option value="<?php echo $list_subject['subject'] ?>"  <?php echo ($list->subject == $list_subject['subject']) ? 'selected' : '' ?>><?php echo $subject_master->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="detail">Detail</label>
                                        <div class="col-sm-10">          
                                            <textarea class="form-control validate[required]" name="detail" id="detail" placeholder="Enter Detail"><?php echo $list->detail ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="class">Class</label>
                                        <div class="col-sm-10">  
                                            <select name="class" id="class" class="form-control validate[required]">
                                                <?php
                                                foreach ($list_subjects as $l) {
                                                    $session = get_object('session', $l['session_id']);
                                                    ?>
                                                    <option value="<?php echo $l['session_id'] ?>" <?php echo ($l['session_id'] == $list->class) ? 'selected' : '' ?>><?php echo $session->title ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="upload_file">Upload File:</label>
                                        <div class="col-sm-10">
                                            <input type="file" name="file" value="<?php echo $list->file ?>" id="file">
                                            <div id="hide_img">
                                                <?php if ($list->file !== '') { ?>
                                                    <?php if (in_array($extension, $extensions)) { ?>
                                                        <br /><img src="<?php echo DIR_WS_SITE_UPLOAD . 'file/homework/' . $list->file ?>" style="height:200px;width:200px" class="img img-responsive"  /><br /><button type="button" class="btn btn-xs red icn-only tooltips" id="remove_img" data-id="<?php echo $_GET['id'] ?>">Remove</button>
                                                    <?php } else { ?>
                                                        <br /><br /><span style="margin-left: 14px;position: absolute;"><?php echo strtoupper($extension) ?></span><i class="fa fa-file-o fa-5x"></i><br /><br /><button type="button" class="btn btn-xs red icn-only tooltips" id="remove_img" data-id="<?php echo $_GET['id'] ?>">Remove</button>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="title">Date:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="date" class="datepicker form-control validate[required]" id="date" value="<?php echo $list->date ?>">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="title">Date of Submission:</label>
                                        <div class="col-sm-10">
                                            <input type="text" placeholder="Date of Submission Date" name="submission" value="<?php echo $list->submission ?>" class="datepicker form-control validate[required]" id="submission">
                                        </div>
                                    </div>
                                    <div class="form-group">        
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="status" id="status" value="1"  <?php echo ($list->status == 1) ? 'checked' : '' ?>  />Status</label>
                                            </div>
                                        </div>
                                        <input type="hidden" name="id" value="<?php echo $id ?>" />
                                    </div>
                                    <div class="form-group">        
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                            <a href="<?php echo make_admin_url('homework') ?>" class="btn btn-default">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.standalone.min.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<?php require 'script.php'; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
<script>
    $('.datepicker').datepicker()
</script>