<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Syllabus
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Syllabus</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <?php
                /* display message */
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Syllabus</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>Course</th>
                                            <th>Subject</th>
                                            <th>File</th>
                                        </tr>
                                    </thead>
                                    <?php foreach ($syllabus_list as $syllabus) { ?>
                                        <tr class="odd gradeX">
                                            <td>
                                                <?php
                                                $session = get_object('session', $syllabus->session_id);
                                                $course = get_object('school_course', $session->course_id);
                                                echo $course->course_name;
                                                ?>
                                            </td>
                                            <td>
                                                <?php $subject = get_object('subject_master', $syllabus->subject_id); ?>
                                                <?= $subject->name ?>
                                            </td>
                                            <td>
                                                <a class="btn btn-success btn-xs" href="<?= DIR_WS_SITE_UPLOAD ?>file/syllabus/<?= $syllabus->file ?>" target="_blank"><i class="fa fa-download"></i><span class="hidden-xs">   Download</span></a>
                                            </td>	
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>