<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Send Application
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Send Application</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="clearfix"></div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="fa fa-plus"></i>Add Homework</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <form method="POST" class="form-horizontal" id="validation">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">To</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="to" class="form-control" Value="Principal" id="to"  disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Select Leave Type</label>
                                        <div class="col-sm-10">
                                            <select name="leave_type_id" class="form-control validate[required]">
                                                <?php foreach ($list_leave_types as $list_type) { ?>
                                                    <option value="<?php echo $list_type['id'] ?>"><?php echo $list_type['type'] ?></option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Subject</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="subject" class="form-control validate[required]" placeholder="Subject" id="subject">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2">Message</label>
                                        <div class="col-sm-10">
                                            <textarea rows="5" name="message" class="form-control validate[required]" placeholder="Message" id="message"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" name="submit" id="submit" class="btn btn-primary">Send</button>
                                            <a href="<?php echo make_admin_url('application') ?>" class="btn btn-default">Cancel</a>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>