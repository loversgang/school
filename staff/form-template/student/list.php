<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    <?php echo $s_info->first_name; ?> <?php echo $s_info->last_name; ?>
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#"><?php echo $s_info->first_name; ?> <?php echo $s_info->last_name; ?></a>
                        </li>
                    </ul>
                </div>
                <div class="row tab-pane active" id="tab1">
                    <ul class="thumbnails product-list-inline-small">
                        <div class="col-lg-12 col-sm-12">
                            <div class="card hovercard">
                                <div class="card-background">
                                    <?
                                    if (is_object($s_info) && $s_info->photo):
                                        $im_obj = new imageManipulation()
                                        ?>
                                        <img class="card-bkimg" src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>"  />
                                    <? else: ?>
                                        <img class="card-bkimg" src="<?php echo DIR_WS_SITE_GRAPHIC . 'profile_cover.jpg' ?>" alt="" >
                                    <? endif; ?> 
                                </div>
                                <div class="useravatar">
                                    <?
                                    if (is_object($s_info) && $s_info->photo):
                                        $im_obj = new imageManipulation()
                                        ?>
                                        <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" class="img img-circle" />
                                    <? else: ?>
                                        <img src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="" class="img img-circle">
                                    <? endif; ?> 
                                </div>
                                <div class="card-info"> <span class="card-title" style="color:#fff"><?php echo $s_info->first_name; ?> <?php echo $s_info->last_name; ?></span>
                                </div>
                            </div>

                            <div class="row" style="padding-top: 10px">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="tab1">

                                        <div class="col-md-4">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">Basic Information</div>
                                                <div class="panel-body">
                                                    <div class="info_profile" style="word-wrap: break-word;">
                                                        <strong>Registration Id:</strong>
                                                        <br/>
                                                        <?= $s_info->reg_id ?>
                                                        <hr/>
                                                        <strong>Reg Date:</strong>
                                                        <br/>
                                                        <?= $s_info->date_of_admission ?>
                                                        <hr/>
                                                        <strong>Sex:</strong>
                                                        <br/>
                                                        <?= $s_info->sex ?>
                                                        <hr/>
                                                        <strong>Caste:</strong>
                                                        <br/>
                                                        <?= $s_info->cast ?>
                                                        <hr/>
                                                        <strong>Religion:</strong>
                                                        <br/>
                                                        <?= $s_info->religion ?>
                                                        <hr/>
                                                        <strong>Category:</strong>
                                                        <br/>
                                                        <?= $s_info->category ?>
                                                        <hr/>

                                                        <strong>Date Of Birth:</strong>
                                                        <br/>
                                                        <?= $s_info->date_of_birth ?><br/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">Address Information</div>
                                                <div class="panel-body">
                                                    <div class="info_profile" style="word-wrap: break-word;">
                                                        <strong>Permanent Address :</strong><br/>
                                                        <?php
                                                        if (isset($s_p_ad) && !empty($s_p_ad->address1)) {
                                                            echo $s_p_ad->address1 . ", ";
                                                        }
                                                        if (isset($s_p_ad) && !empty($s_p_ad->city)) {
                                                            echo $s_p_ad->city . ", ";
                                                        }
                                                        if (isset($s_p_ad) && !empty($s_p_ad->tehsil)) {
                                                            echo $s_p_ad->tehsil . ", ";
                                                        }
                                                        if (isset($s_p_ad) && !empty($s_p_ad->district)) {
                                                            echo $s_p_ad->district . ", ";
                                                        }
                                                        if (isset($s_p_ad) && !empty($s_p_ad->state)) {
                                                            echo $s_p_ad->state . ", ";
                                                        }
                                                        if (isset($s_p_ad) && !empty($s_p_ad->zip)) {
                                                            echo $s_p_ad->zip;
                                                        }
                                                        ?>
                                                        <hr/>
                                                        <strong>Correspondence Address :</strong><br/>
                                                        <?php
                                                        if (isset($s_c_ad) && !empty($s_c_ad->address1)) {
                                                            echo $s_c_ad->address1 . ", ";
                                                        }
                                                        if (isset($s_c_ad) && !empty($s_c_ad->city)) {
                                                            echo $s_c_ad->city . ", ";
                                                        }
                                                        if (isset($s_c_ad) && !empty($s_c_ad->tehsil)) {
                                                            echo $s_c_ad->tehsil . ", ";
                                                        }
                                                        if (isset($s_c_ad) && !empty($s_c_ad->district)) {
                                                            echo $s_c_ad->district . ", ";
                                                        }
                                                        if (isset($s_c_ad) && !empty($s_c_ad->state)) {
                                                            echo $s_c_ad->state . ", ";
                                                        }
                                                        if (isset($s_c_ad) && !empty($s_c_ad->zip)) {
                                                            echo $s_c_ad->zip;
                                                        }
                                                        ?>
                                                        <hr/>
                                                        <strong>Mobile:</strong>
                                                        <br/>
                                                        <?= $s_info->phone ?>
                                                        <hr/>
                                                        <strong>Email:</strong>
                                                        <br/>
                                                        <?= $s_info->email ?>
                                                        <hr/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">Family Information</div>
                                                <div class="panel-body">
                                                    <div class="info_profile" style="word-wrap: break-word;">
                                                        <strong>Father Name:</strong>
                                                        <br/>
                                                        <?php
                                                        if (is_object($s_f)) {
                                                            echo $s_f->father_name;
                                                        }
                                                        ?>
                                                        <hr/>
                                                        <strong>Father Occupation:</strong>
                                                        <br/>
                                                        <?php
                                                        if (is_object($s_f)) {
                                                            echo $s_f->father_occupation;
                                                        }
                                                        ?>
                                                        <hr/>
                                                        <strong>Father Education:</strong>
                                                        <br/>
                                                        <?php
                                                        if (is_object($s_f)) {
                                                            echo $s_f->father_education;
                                                        }
                                                        ?>
                                                        <hr/>
                                                        <strong>Mother Name:</strong>
                                                        <br/>
                                                        <?php
                                                        if (is_object($s_f)) {
                                                            echo $s_f->mother_name;
                                                        }
                                                        ?>
                                                        <hr/>
                                                        <strong>Mother Occupation:</strong>
                                                        <br/>
                                                        <?php
                                                        if (is_object($s_f)) {
                                                            echo $s_f->mother_occupation;
                                                        }
                                                        ?>
                                                        <hr/>
                                                        <strong>Mother Education:</strong>
                                                        <br/>
                                                        <?php
                                                        if (is_object($s_f)) {
                                                            echo $s_f->mother_education;
                                                        }
                                                        ?>
                                                        <hr/>
                                                        <strong>Annual Income (<i class="fa fa-inr"></i>):</strong>
                                                        <br/>
                                                        <?php
                                                        if (is_object($s_f)) {
                                                            echo $s_f->household_annual_income;
                                                        }
                                                        ?>
                                                        <hr/>
                                                        <strong>Email Address:</strong>
                                                        <br/>
                                                        <?php
                                                        if (is_object($s_f)) {
                                                            echo $s_f->email;
                                                        }
                                                        ?>
                                                        <hr/>
                                                        <strong>Mobile Number:</strong>
                                                        <br/>
                                                        <?php
                                                        if (is_object($s_f)) {
                                                            echo $s_f->mobile;
                                                        }
                                                        ?>
                                                        <hr/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
        </section>
    </div>
</div>