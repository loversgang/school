<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <h3 class="page-title">
            Messages
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Messages</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-sm-3 col-md-2">
                <a href="<?php echo make_admin_url('messages', 'compose', 'compose') ?>" class="btn btn-danger btn-sm btn-block" role="button">COMPOSE</a>
                <br />
                <ul class="nav nav-pills nav-stacked">
                    <li <?= $m_type == 'inbox' ? ' class="active"' : '' ?>>
                        <a href="<?php echo make_admin_url('messages') ?>">
                            <?php if ($inbox_count > 0) { ?>
                                <span style="margin-top: 1px;" class="badge pull-right">
                                    <?php echo $inbox_count; ?>
                                </span>
                            <?php } ?>
                            Inbox </a>
                    </li>
                    <li <?= $m_type == 'outbox' ? ' class="active"' : '' ?>>
                        <a href="<?php echo make_admin_url('messages', 'list', 'list', 'm_type=outbox') ?>">
                            <?php if ($outbox_count > 0) { ?>
                                <span style="margin-top: 1px;" class="badge pull-right">
                                    <?php echo $outbox_count; ?>
                                </span>
                            <?php } ?>
                            Sent Message </a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9 col-md-10">
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="clearfix"></div>
            </div>
            <?php if ($m_type == 'inbox') { ?>
                <div class="col-sm-9 col-md-10">
                    <?php if ($staff_inbox_count > 0) { ?>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="home">
                                <div class="list-group">
                                    <?php
                                    foreach ($inbox_messages as $header) {
                                        $obj = new messages;
                                        $message = $obj->getMessageDetails($header->id);
                                        if ($header->from_type == 'admin') {
                                            $to_name = 'Admin';
                                        } else {
                                            $CurrObj = new studentSession();
                                            $current_session = $CurrObj->getStudentCurrentSession($school->id, $header->from_id);
                                            $student = get_object('student', $header->from_id);
                                            $to_name = $student->first_name . ' ' . $student->last_name . '(' . $current_session . ')';
                                        }
                                        ?>
                                        <a class="list-group-item msg <?php echo $message->is_read == '0' ? 'unread' : 'read' ?>" href="<?php echo make_admin_url('messages', 'read', 'read', 'h_id=' . $header->id . '&msg_box=inbox') ?>">
                                            <span class="badge"><?php echo date('d-m-Y', strtotime($header->time)); ?></span>
                                            <span class="badge">Read Message</span>
                                            <span class="name" style="display: inline-block;">
                                                <?php echo $to_name; ?>
                                            </span>
                                            <br/>
                                            <span class=""><?php echo $header->subject; ?></span>
                                            <span class="text-muted" style="font-size: 11px;"></span>
                                            <br/>
                                            <span style="color: green">
                                                <?php echo date('h:i A', strtotime($header->time)); ?> 
                                            </span>
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-info">Inbox Empty..!</div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ($m_type == 'outbox') { ?>
                <div class="col-sm-9 col-md-10">
                    <?php if ($staff_outbox_count > 0) { ?>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="home">
                                <div class="list-group">
                                    <?php
                                    foreach ($outbox_messages as $header) {
                                        $obj = new messages;
                                        $message = $obj->getMessageDetails($header->id);
                                        if ($header->to_type == 'admin') {
                                            $to_name = 'Admin';
                                        } else {
                                            $CurrObj = new studentSession();
                                            $current_session = $CurrObj->getStudentCurrentSession($school->id, $header->to_id);
                                            $student = get_object('student', $header->to_id);
                                            $to_name = $student->first_name . ' ' . $student->last_name . ' (' . $current_session . ')';
                                        }
                                        ?>
                                        <a class="list-group-item msg <?php echo $message->is_read == '0' ? 'unread' : 'read' ?>" href="<?php echo make_admin_url('messages', 'read', 'read', 'h_id=' . $header->id . '&msg_box=outbox') ?>">
                                            <span class="badge"><?php echo date('d-m-Y', strtotime($header->time)); ?></span>
                                            <span class="badge">Read Message</span>
                                            <span class="name" style="display: inline-block;">
                                                <?php echo $to_name; ?>
                                            </span>
                                            <br/>
                                            <span class=""><?php echo $header->subject; ?></span>
                                            <span class="text-muted" style="font-size: 11px;"></span>
                                            <br/>
                                            <span style="color: green">
                                                <?php echo date('h:i A', strtotime($header->time)); ?> 
                                            </span>
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-info">OutBox Empty..!</div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>