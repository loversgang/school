<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <h3 class="page-title">
            Compose Message
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="fa fa-inbox"></i>
                    <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>">Inbox</a>
                    <i class="fa fa-angle-right"></i>                                       
                </li>
                <li>
                    <a href="#">Compose Message</a>
                </li>
            </ul>
        </div>
        <div class="row tab-pane active" id="tab1">
            <ul class="thumbnails product-list-inline-small">
                <div class="row-fliud">
                    <div class="form-group col-md-12">
                        <div class="input-group">
                            <span class="input-group-addon" >User Type:</span>
                            <select class="form-control" id="user_type" autofocus="" name="user_type" <?php echo (isset($_GET['user_type'])) ? 'disabled' : '' ?>>
                                <option value="admin">Principal</option>
                                <option value="student" <?php
                                if (isset($_GET['user_type']) && $_GET['user_type'] == 'student') {
                                    echo "selected";
                                }
                                ?>>Student</option>
                            </select>
                        </div>
                    </div>
                    <?php if (!isset($_GET['user_id'])) { ?>
                        <div id="hide_section">
                            <div id="session_list_details"></div>
                            <div id="get_sections"></div>
                        </div>
                    <?php } ?>
                    <form method="post" action="" id="validation">
                        <?php if (isset($_GET['user_id'])) { ?>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon">To:</span>
                                    <input type="hidden" name="to_id" value="<?php echo $to_id; ?>">
                                    <input disabled type="text" value="<?php echo $to_name; ?>" class="form-control" autofocus="">
                                </div>
                            </div>
                        <?php } else { ?>
                            <div id="get_users"></div>
                        <?php } ?>
                        <div class="form-group col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">Subject:</span>
                                <input name="subject" type="text" class="form-control validate[required]">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-group">
                                <label class="input-group-addon" style="text-align: left; padding: 10px">Message:</label>
                                <textarea name="content" rows="10" class="form-control validate[required]"></textarea>
                            </div>
                        </div>
                        <center>
                            <input type="hidden" name="school_id" value="<?php echo $school->id; ?>">
                            <input type="hidden" name="from_id" value="<?php echo $id; ?>">
                            <input type="hidden" name="from_type" value="staff">
                            <input type="hidden" name="to_type" id="to_type" value="<?php echo $_GET['user_type']; ?>">

                            <button name="submit" value="Submit" class="btn btn-success"><i class="fa fa-envelope"></i> Send</button>
                            <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>" class="btn btn-green btn-danger"><i class="fa fa-close"></i> Cancel</a>
                        </center>
                    </form>
                </div>
            </ul>
        </div>
    </div>
</div>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(function () {
        $('select#user_type').change(function () {
            var type = $(this).val();
            $('#to_type').val(type);
            if (type === 'student') {
                $('#hide_section').show();
                $("#session_list_details").html('<div style="width:100%;text-align:center;padding-top:5px;"><i class="fa fa-spinner fa-pulse fa-5x"></i></div>');
                $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_sessions_list'}, function (data) {
                    $("#session_list_details").html(data);
                    $('#session_id').change();
                });
            }
            if (type === 'admin') {
                $('#hide_section').hide();
                $("#get_users").html('<div style="width:100%;text-align:center;padding-top:5px;"><i class="fa fa-spinner fa-pulse fa-5x"></i></div>');
                $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_admin'}, function (data) {
                    $('#get_users').html(data);
                });
            }
        }).change();
        $(document).on('change', '#session_id', function () {
            var session_id = $(this).val();
            $("#get_sections").html('<div style="width:100%;text-align:center;padding-top:5px;"><i class="fa fa-spinner fa-pulse fa-5x"></i></div>');
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_sections', session_id: session_id}, function (data) {
                $('#get_sections').html(data);
                $('#section').change();
            });
        }).change();
        $(document).on('change', '#section', function () {
            var section = $(this).val();
            var session_id = $('#session_id').val();
            $("#get_users").html('<div style="width:100%;text-align:center;padding-top:5px;"><i class="fa fa-spinner fa-pulse fa-5x"></i></div>');
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_students', session_id: session_id, section: section}, function (data) {
                $('#get_users').html(data);
            });
        });
    }).change();
</script>