<?php
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
$QueryObjQ = new session();
$AllSessYear = $QueryObjQ->listOfYearSess($school->id, 1);
if (empty($sess_int)) {
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
}
$login_staff_id = $_SESSION['admin_session_secure']['user_id'];
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'get_session_years') {
        ?>
        <div class="control-group">
            <label class="control-label span2">Session Year</label>
            <div class="controls">
                <select class="select2_category span10 m-wrap" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                    <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                        <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                    <? endforeach; ?>
                </select>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_sessions_list') {
        $object = new timeTable();
        $All_Session = $object->getSessionIdByStaffId($login_staff_id);
        ?>
        <div class="form-group col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Session:</span>
                <select class="form-control" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                    <?php
                    foreach ($All_Session as $list) {
                        $session = get_object('session', $list['session_id']);
                        ?>
                        <option value="<?php echo $list['session_id'] ?>"><?php echo $session->title ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_sections') {
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);
        ?>
        <div class="form-group col-md-12">
            <div class="input-group">
                <span class="input-group-addon">Section:</span>
                <? if ($QuerySec->GetNumRows() >= 0) { ?>	
                    <select class="form-control" data-placeholder="Select Session Students" name='section' id='section'>
                        <?php while ($sec = $QuerySec->GetObjectFromRecord()) { ?>
                            <option value='<?= $sec->section ?>'><?= ucfirst($sec->section) ?></option>
                        <?php } ?>
                    </select>
                <?php } else { ?>
                    <input type='text' class="span10 m-wrap" value='No Section.' disabled />
                <?php } ?>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_students') {
        $QueryOb = new studentSession();
        $students = $QueryOb->sectionSessionStudents($session_id, $section);
        ?>
        <div class="form-group col-md-12">
            <div class="input-group">
                <span class="input-group-addon">To:</span>
                <select class="form-control" data-placeholder="Select Student" name='to_id' id='to_id'>
                    <?php
                    foreach ($students as $student) {
                        $CurrObj = new studentSession();
                        $current_session = $CurrObj->getStudentCurrentSession($school->id, $student->id);
                        $to_name = $student->first_name . ' ' . $student->last_name;
                        ?>
                        <option value="<?php echo $student->id; ?>"><?php echo $to_name ?> <?php echo $current_session ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_admin') {
        ?>
        <div class="form-group col-md-12">
            <div class="input-group">
                <span class="input-group-addon">To:</span>
                <input type="hidden" name="to_id" value="1">
                <input disabled type="text" value="Principal" class="form-control" autofocus="">
            </div>
        </div>
        <?php
    }
}