<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Exam Results
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Exam Results</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                /* display message */
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Subjects</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <form action="<?php echo make_admin_url('exam', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >
                                    <div style="overflow-y:hidden">
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
                                                <tr>							<th>Roll No.</th>
                                                    <th>Name</th>
                                                    <th>Father Name</th>
                                                    <th>Minimum Marks</th>
                                                    <th>Marks Obtained</th>
                                                    <th>Maximum Marks</th>
                                                    <th>Percentage</th>
                                                </tr>
                                            </thead>
                                            <? if (!empty($result)): ?>
                                                <tbody>
                                                    <?php
                                                    $sr = 1;
                                                    foreach ($result as $key => $obj):
                                                        ?>
                                                        <tr>								<td><?= $obj->roll_no ?></td>
                                                            <td><?= $obj->first_name . " " . $obj->last_name ?></td>
                                                            <td class='main_title'><?= ucfirst($obj->father_name) ?></td>
                                                            <td><?= $obj->minimum_marks ?></td>
                                                            <td><?= $obj->marks_obtained ?></td>
                                                            <td><?= $obj->maximum_marks ?></td>
                                                            <td><?= number_format(($obj->marks_obtained / $obj->maximum_marks) * 100, 2) ?>%</td>
                                                        </tr>
                                                        <?php
                                                        $sr++;
                                                    endforeach;
                                                    ?>
                                                </tbody>
                                            <?php endif; ?>  
                                        </table>
                                    </div>
                                </form>    
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>