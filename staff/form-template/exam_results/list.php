<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Exam Results
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Exam Results</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Subjects</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <form action="<?php echo make_admin_url('exam_results', 'list', 'list') ?>" method="GET" id='session_filter'>
                                    <input type='hidden' name='Page' value='exam_results'/>
                                    <input type='hidden' name='action' value='list'/>
                                    <input type='hidden' name='section' value='list'/>															
                                    <div class="row">
                                        <div class="col-md-5">															
                                            <label class="control-label">Select Session</label>
                                            <div class="controls">
                                                <select class="form-control session_filter" data-placeholder="Select Session Students" name='session_id' >
                                                    <option value="">All</option>
                                                    <?php
                                                    $session_sr = 1;
                                                    foreach ($Session_list as $s_k => $session):
                                                        ?>
                                                        <option value='<?= $session->session_id ?>' <?
                                                        if ($session->session_id == $session_id) {
                                                            echo 'selected';
                                                        }
                                                        ?> ><?= ucfirst($session->session_name) ?></option>
                                                                <?
                                                                $session_sr++;
                                                            endforeach;
                                                            ?>
                                                </select>
                                            </div>	
                                        </div>
                                        <div class="col-md-7"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                        <div class="clearfix"></div>				</form>
                                <br />
                                    <form  method="POST" id="form_data" name="form_data" >
                                    <div style="overflow-y: hidden">
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>Examination Title</th>
                                                    <th>Session</th>
                                                    <th>Section</th>
                                                    <th>Subject Name</th>
                                                    <th>On Date</th>
                                                    <th>Results</th>
                                                </tr>
                                            </thead>
                                            <? if (!empty($result)): ?>
                                                <tbody>
                                                    <?php
                                                    $sr = 1;
                                                    foreach ($result as $key => $object):
                                                        ?>
                                                        <tr>
                                                            <td><?= $object->title ?></td>
                                                            <td><?= $object->session_name ?></td>
                                                            <td><?= $object->section ?></td>
                                                            <td><?= $object->subject ?></td>
                                                            <td><?= $object->date ?></td>
                                                            <td><a class="btn mini green btn-xs tooltips" href="<?php echo make_admin_url('exam_results', 'view', 'view', 'id=' . $object->id) ?>" title="click here to see the Result">Result</a> </td>
                                                            </td>

                                                        </tr>
                                                        <?php
                                                        $sr++;
                                                    endforeach;
                                                    ?>
                                                </tbody>
                                            <?php endif; ?>  
                                        </table>
                                    </div>
                                </form>    
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">
    $(".session_filter").on("change", function () {
        var session_id = $(this).val();
        $('#session_filter').submit();
    });
</script>