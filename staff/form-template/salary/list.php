<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Salary
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Salary</a>
                        </li>
                    </ul>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Salary</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (count($lists) > 0) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Salary (<i class="fa fa-inr"></i>)</th>
                                                <th>Month & Year</th>
                                            </tr>
                                        </thead>
                                        <?php foreach ($lists as $key => $list) { ?>
                                            <tr class="odd gradeX">
                                                <td><?php echo $list['on_date'] ?></td>
                                                <td><i class="fa fa-inr"></i> <?php echo $list['amount'] ?></td>
                                                <td> <?
                                                    if (!empty($list['month_year'])): echo date('M, Y', strtotime($list['month_year']));
                                                    endif;
                                                    ?></td>

                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                <?php } else { ?>
                                    <div class="alert alert-info text-center"><strong>No Salary Records Found..!</strong></div>
                                <?php } ?>
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>