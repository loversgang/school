<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Complaint Referers
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Complaint Referers</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Complaint Referers</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (count($referers) > 0) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="sample_">
                                        <thead>
                                            <tr>
                                                <th>Sender Name</th>
                                                <th>Description</th>
                                                <th>Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <?php foreach ($referers as $referer) { ?>
                                            <tr>
                                                <td>ADMIN</td>
                                                <td><?php echo $referer['description'] ?></td>
                                                <td><?php echo date('d-m-Y', $referer['date']) ?></td>
                                                <td>
                                                    <div class="col-md-4">
                                                        <a href="<?php echo make_admin_url('referers', 'complaints', 'complaints&id=' . $referer['complaint_id']) ?>" class="btn btn-xs blue tooltips" title="View Complaint Details">View Complaint</a>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <a href="<?php echo make_admin_url('referers', 'message', 'message&id=' . $referer['complaint_id']) ?>" class="btn btn-xs green tooltips" title="Send and view Messages">View Messages</a>
                                                    </div>
                                                    <div class="col-md-4"></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>                          
                                    </table>
                                <?php } else { ?>
                                    <div class="alert alert-info">You added zero Complaint Referers..!</div>
                                <?php } ?>
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>