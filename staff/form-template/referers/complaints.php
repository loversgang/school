<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Complaint Referers
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Complaint Referers</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Complaint Referers</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (count($complaints) !== 0) { ?>
                                    <div style="overflow-y: hidden">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Complaint Title</th>
                                                    <th>Complaint Description</th>
                                                    <th>Complaint Against</th>
                                                    <th>Complaint By</th>
                                                    <th>Complaint Date</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <?php foreach ($complaints as $key => $list_complaint) { ?>
                                                <tr>

                                                    <td><?php echo $list_complaint['title'] ?></td>
                                                    <td>
                                                        <?php
                                                        if (strlen($list_complaint['description']) > 5) {
                                                            echo substr($list_complaint['description'], 0, 5) . '.....';
                                                        } else {
                                                            echo $list_complaint['description'];
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        echo $list_complaint['against'];
                                                        ?>

                                                    </td>
                                                    <td>
                                                        <?php
                                                        $student = get_object('student', $list_complaint['student_id']);
                                                        echo $student->first_name . ' ' . $student->last_name;
                                                        ?>
                                                    </td>
                                                    <td><?php echo date('d M Y', strtotime($list_complaint['date'])) ?></td>
                                                    <td>
                                                        <?php
                                                        if ($list_complaint['status'] == 0) {
                                                            echo 'Pending';
                                                        } else {
                                                            echo 'Solved';
                                                        }
                                                        ?>
                                                    </td>

                                                </tr>
                                            <?php } ?>
                                            </tbody>                          
                                        </table>
                                    </div>
                                <?php } else { ?>
                                    <div class="alert alert-info">No Complaints found..!</div>
                                <?php } ?>
                            </div></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
    </div>
</div>