<style>
    .tiles .tile {
        height : 60px;
    }
    .tiles .tile .tile-body i {
        margin-top: 0px;
        display: block;
        font-size: 30px;
        line-height: 15px;
        text-align: center;
    }

</style>
<div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('referers', 'list', 'list'); ?>">
        <div class="corner"></div>
        <div class="tile-body">
        </div>
        <div class="tile-object">
            <div class="name">
                <center>List Referers</center>
            </div>
        </div>
    </a>   
</div>