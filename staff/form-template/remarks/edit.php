<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Remarks
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Remarks</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="clearfix"></div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="fa fa-plus"></i>Add Remarks</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="validation">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="detail">Remaks</label>
                                        <div class="col-sm-10">          
                                            <textarea class="form-control validate[required]" name="remarks" id="remarks" placeholder="Enter Remarks"><?php echo $remark->remarks ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="class">Class</label>
                                        <div class="col-sm-10">  
                                            <select name="session_id" id="session_id" class="form-control validate[required]">
                                                <option value="">Select Class</option>      
                                                <?php
                                                foreach ($session_ids as $session_id) {
                                                    $classes = get_object('session', $session_id['session_id']);
                                                    ?>
                                                    <option value="<?php echo $session_id['session_id'] ?>" <?php echo ($remark->session_id == $session_id['session_id']) ? 'selected' : '' ?>><?php echo $classes->title ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="class">Select Student</label>
                                        <div class="col-sm-10">  
                                            <select name="student_id" id="student_id" class="form-control validate[required]">
                                                <option>Select Student</option>
                                                <?php
                                                foreach ($student_ids as $student_id) {
                                                    $student = get_object('student', $student_id['student_id']);
                                                    if (is_object($student)) {
                                                        ?>
                                                        <option value="<?php echo $student_id['student_id'] ?>" <?php echo ($remark->student_id == $student_id['student_id']) ? 'selected' : '' ?>><?php echo $student->first_name . ' ' . $student->last_name ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">        
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <input type="hidden" name="school_id" value="<?php echo $school->id ?>" />
                                            <input type="hidden" name="staff_id" value="<?php echo $staff_id ?>" />
                                            <input type="hidden" name="id" value="<?php echo $id ?>" />
                                            <input type="hidden" name="date" value="<?php echo time() ?>" />
                                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                            <a href="<?php echo make_admin_url('remarks') ?>" class="btn btn-default">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
</div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).on('change', '#session_id', function () {
        var session_id = $(this).val();
        if (session_id) {
            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=remarks'); ?>', {action: 'get_student_using_session_id', session_id: session_id}, function (data) {
                $('#student_id').html(data);
            });
        } else {
            $('#student_id').html('<option value="">Select Product<option>');
        }
    });
</script>