<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Remarks
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Remarks</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Remarks</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (count($remarks) > 0) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="sample_">
                                        <thead>
                                            <tr>
                                                <th>Class</th>
                                                <th>Student</th>
                                                <th>Remark</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <?php foreach ($remarks as $remark) { ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    $session = get_object('session', $remark->session_id);
                                                    if (is_object($session)) {
                                                        echo $session->title;
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $student = get_object('student', $remark->student_id);

                                                    if (is_object($student)) {
                                                        echo $student->first_name . ' ' . $student->last_name;
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $remark->remarks ?></td>
                                                <td>
                                                    <div class="col-md-2">
                                                        <a class="btn btn-xs red icn-only tooltips" href="<?php echo make_admin_url('remarks', 'update', 'update', '&id=' . $remark->id) ?>"  title="click here to Edit this record"><i class="fa fa-pencil icon-white"></i></a>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a class="btn btn-xs red icn-only tooltips" href="<?php echo make_admin_url('remarks', 'delete', 'delete', '&id=' . $remark->id) ?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="click here to delete this record"><i class="fa fa-remove icon-white"></i></a>
                                                    </div>
                                                    <div class="col-md-6"></div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>                          
                                    </table>
                                <?php } else { ?>
                                    <div class="alert alert-info">You added zero Remarks..!</div>
                                <?php } ?>
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>