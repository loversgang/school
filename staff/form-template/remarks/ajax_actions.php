<?php
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
if (isset($_POST)) {
    extract($_POST);
    if ($action == 'get_student_using_session_id') {
        $query = new studentSession();
        $student_ids = $query->get_student_id_using_session_id_no_section($session_id);
        ?>
        <?php
        foreach ($student_ids as $student_id) {
            $student = get_object('student', $student_id['student_id']);
            if (is_object($student)) {
                ?>
                <option value="<?php echo $student_id['student_id'] ?>"><?php echo $student->first_name . ' ' . $student->last_name ?></option>
                <?php
            }
        }
    }
}
?>