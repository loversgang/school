<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Time Table
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Time Table</a>
                        </li>
                    </ul>
                </div>
                <div class="row-fluid">
                    <div style="overflow-y: hidden">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr style="background: #eee">
                                    <th style="text-align: center">Time</th>
                                    <th style="text-align: center">Monday</th>
                                    <th style="text-align: center">Tuesday</th>
                                    <th style="text-align: center">Wednesday</th>
                                    <th style="text-align: center">Thursday</th>
                                    <th style="text-align: center">Friday</th>
                                    <th style="text-align: center">Saturday</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($time_tables as $time_table) { ?>
                                    <tr>
                                        <td style="background: #eee">
                                <center>
                                    <?php echo $time_table->time_from ?> - <?php echo $time_table->time_to ?>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <?php
                                    $day = timeTable::getStaffTime($id, 'Mon', $time_table->time_from, $time_table->time_to);
                                    $session = is_object($day) ? get_object('session', $day->session_id) : '';
                                    echo is_object($session) ? $session->title : '-';
                                    ?>
                                    <br/>
                                    <?php
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? '<font color="green">' . $subject->name . '</font>' : '-';
                                    ?>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <?php
                                    $day = timeTable::getStaffTime($id, 'Tue', $time_table->time_from, $time_table->time_to);
                                    $session = is_object($day) ? get_object('session', $day->session_id) : '';
                                    echo is_object($session) ? $session->title : '-';
                                    ?>
                                    <br/>
                                    <?php
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? '<font color="green">' . $subject->name . '</font>' : '-';
                                    ?>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <?php
                                    $day = timeTable::getStaffTime($id, 'Wed', $time_table->time_from, $time_table->time_to);
                                    $session = is_object($day) ? get_object('session', $day->session_id) : '';
                                    echo is_object($session) ? $session->title : '-';
                                    ?>
                                    <br/>
                                    <?php
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? '<font color="green">' . $subject->name . '</font>' : '-';
                                    ?>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <?php
                                    $day = timeTable::getStaffTime($id, 'Thu', $time_table->time_from, $time_table->time_to);
                                    $session = is_object($day) ? get_object('session', $day->session_id) : '';
                                    echo is_object($session) ? $session->title : '-';
                                    ?>
                                    <br/>
                                    <?php
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? '<font color="green">' . $subject->name . '</font>' : '-';
                                    ?>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <?php
                                    $day = timeTable::getStaffTime($id, 'Fri', $time_table->time_from, $time_table->time_to);
                                    $session = is_object($day) ? get_object('session', $day->session_id) : '';
                                    echo is_object($session) ? $session->title : '-';
                                    ?>
                                    <br/>
                                    <?php
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? '<font color="green">' . $subject->name . '</font>' : '-';
                                    ?>
                                </center>
                                </td>
                                <td>
                                <center>
                                    <?php
                                    $day = timeTable::getStaffTime($id, 'Sat', $time_table->time_from, $time_table->time_to);
                                    $session = is_object($day) ? get_object('session', $day->session_id) : '';
                                    echo is_object($session) ? $session->title : '-';
                                    ?>
                                    <br/>
                                    <?php
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? '<font color="green">' . $subject->name . '</font>' : '-';
                                    ?>
                                </center>
                                </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>