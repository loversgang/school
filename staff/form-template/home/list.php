<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <h3 class="page-title">
            Dashboard
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Dashboard</a>
                </li>
            </ul>
        </div>
        <div style="margin-top:40px"></div>
        <div class="container-fluid">
            <center>
                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('profile', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-user fa-3x"></i><br/><br/>
                        <span class="span_port">Profile</span>
                    </a>
                </div>
                <div style="margin-top:40px"></div>

                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-envelope fa-3x"></i><br/><br/>
                        <span class="span_port">Messages</span>
                    </a>
                </div>
                <div style="margin-top:40px"></div>
                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('salary', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-inr fa-3x"></i><br/><br/>
                        <span class="span_port">Salary</span>
                    </a>
                </div>
                <div style="margin-top:40px"></div>
                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('syllabus', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-file fa-3x"></i><br/><br/>
                        <span class="span_port">Syllabus</span>
                    </a>
                </div>
                <div style="margin-top:40px"></div>
                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('classes', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-users fa-3x"></i><br/><br/>
                        <span class="span_port">Classes</span>
                    </a>
                </div>
                <div style="margin-top:40px"></div>
                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('school_sales', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-shopping-cart fa-3x"></i><br/><br/>
                        <span class="span_port">School Products</span>
                    </a>
                </div>
                <div style="margin-top:40px"></div>
                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('circulars', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-inbox fa-3x"></i><br/><br/>
                        <span class="span_port">Circulars</span>
                    </a>
                </div>
                <div style="margin-top:40px"></div>
                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('time_table', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-clock-o fa-3x"></i><br/><br/>
                        <span class="span_port">Time Table</span>
                    </a>
                </div>
                <div style="margin-top:40px"></div>
                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('event', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-calendar fa-3x"></i><br/><br/>
                        <span class="span_port">Activity Calendar</span>
                    </a>
                </div>
                <div style="margin-top:40px"></div>
                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('application', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-pencil-square-o fa-3x"></i><br/><br/>
                        <span class="span_port">Leave Application</span>
                    </a>
                </div>
                <div style="margin-top:40px"></div>
                <div class="col-md-1">
                    <a href="<?php echo make_admin_url('logout', 'list', 'list'); ?>" class="portfolio-link">
                        <i class="fa fa-lock fa-3x"></i><br/><br/>
                        <span class="span_port">Logout</span>
                    </a>
                </div>
            </center>
        </div>
        <div style="margin-top:40px"></div>
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="portlet box blue-hoki tasks-widget">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar"></i>Events
                        </div>
                    </div>
                    <div class="portlet-body" style="">
                        <div class="task-content">
                            <ul class="task-list">
                                <?php foreach ($events as $event) { ?>
                                    <li>
                                        <div class="task-title">
                                            <i class="fa fa-calendar"></i>
                                            <span class="task-title-sp" style="margin-bottom: 5px">
                                                <b><?php echo $event->event ?> <font color="green">(<?php echo $event->on_date ?>)</font></b><br/>
                                                <?php
                                                if (strlen($event->description) > 200) {
                                                    echo substr($event->description, 0, 200) . '..';
                                                } else {
                                                    echo $event->description;
                                                }
                                                ?>
                                            </span>
                                            <br/>

                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="task-footer">
                            <div class="btn-arrow-link pull-right">
                                <a href="<?php echo make_admin_url('event', 'list', 'list'); ?>">See All Records</a>
                                <i class="fa fa-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="portlet box blue-hoki tasks-widget">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar"></i>Upcoming Birthdays
                        </div>
                    </div>
                    <div class="portlet-body" style="">
                        <div class="task-content">
                            <ul class="task-list">
                                <?php foreach ($list_staff_dob as $staff) { ?>
                                    <div class="col-md-6" style="border:1px solid #eee;padding:5px">
                                        <div class="col-md-3">
                                            <?php
                                            if ($staff['photo'] !== ''):
                                                $im_obj = new imageManipulation();
                                                ?>
                                                <img src="<?= $im_obj->get_image('staff', 'medium', $staff['photo']); ?>" style="height:45px;width: 45px" class="img img-circle img-thumbnail"/>
                                            <? else: ?>
                                                <img style="height:45px;width: 45px" class="img img-circle img-thumbnail" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                            <? endif; ?>
                                        </div>
                                        <div class="col-md-9">
                                            <div style="margin-bottom: 5px;font-weight: bold">
                                                <?php echo $staff['first_name'] . ' ' . $staff['last_name']; ?>
                                            </div>
                                            <div style="margin-bottom: 10px;color:darkred">
                                                <b><i class="fa fa-birthday-cake"></i></b> <?php echo date('d M', strtotime($staff['date_of_birth'])); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="task-footer"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="portlet box blue-hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-clock-o"></i>Time Table - <?php echo date('l'); ?> (<?php echo date('d-m-Y'); ?>)
                        </div>
                    </div>
                    <div class="portlet-body" style="">
                        <?php
                        if (date('D') == 'Sun') {
                            echo '<div class="alert alert-info"><h3>Oops.. Today Is Sunday!</h3></div>';
                        } else {
                            ?>
                            <table class="table table-bordered">
                                <thead>
                                    <tr style="background: #eee">
                                        <?php foreach ($time_tables as $time_table) { ?>
                                            <th style="text-align: center"><?php echo $time_table->time_from ?> - <?php echo $time_table->time_to ?></th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php foreach ($time_tables as $time_table) { ?>
                                            <td style="background: #fff">
                                    <center>
                                        <?php
                                        $day_name = date('D');
                                        $day = timeTable::getStaffTime($id, $day_name, $time_table->time_from, $time_table->time_to);
                                        $session = is_object($day) ? get_object('session', $day->session_id) : '';
                                        echo is_object($session) ? $session->title : '-';
                                        ?>
                                        <br/>
                                        <?php
                                        $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                        echo is_object($subject) ? '<font color="green">' . $subject->name . '</font>' : '-';
                                        ?>
                                    </center>
                                    </td>
                                <?php } ?>
                                </tr>
                                </tbody>
                            </table>
                        <?php } ?>
                        <div class="btn-arrow-link pull-right">
                            <a href="<?php echo make_admin_url('time_table', 'list', 'list'); ?>">See Full Time Table</a>
                            <i class="fa fa-arrow-right"></i>
                        </div>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="fa fa-male"></i>Staff Information</div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <?php foreach ($record as $key => $record_list) { ?>
                            <?php if ($key == 6) { ?>
                                <div class="clearfix"></div>
                            <?php } ?>
                            <div class='col-md-2'>
                                <div class="text-center">
                                    <?php if ($record_list->photo == '') { ?>
                                        <img src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" class="img img-responsive"/>
                                    <?php } else { ?>
                                        <?php
                                        $im_obj = new imageManipulation()
                                        ?>
                                        <img src="<?= $im_obj->get_image('staff', 'medium', $record_list->photo); ?>" class="img img-responsive " style="width:215px;height:107px"/>
                                    <?php } ?>
                                    <?php echo $record_list->title . ' ' . $record_list->first_name . ' ' . $record_list->last_name ?>
                                    <div class="clearfix"></div>
                                    <?php echo $record_list->mobile ?>
                                </div>
                                <br /><br />
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>					
        <div class="clearfix"></div>
    </div>
</div>