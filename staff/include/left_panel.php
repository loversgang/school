<div class="navbar navbar-inverse navbar-twitch" role="navigation" style="background-color:#2c3e50">
    <div class="container">
        <!--        <button type="button" class="btn btn-default btn-xs navbar-twitch-toggle">
                    <span class="glyphicon glyphicon-chevron-right nav-open"></span>		
                    <span class="glyphicon glyphicon-chevron-left nav-close"></span>
                </button>-->
        <ul class="nav navbar-nav" id="title">
            <li class="hidden-xs">
                <a>
                    <span> 
                        &nbsp
                    </span>
                    <span class="full-nav"></span>
                </a>
            </li>
            <li>
                <a href="<?php echo make_admin_url('home') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="Home"> 
                        <span class="glyphicon glyphicon-home"></span> 
                    </span>
                    <span class="full-nav">Home</span>
                </a>
            </li>
            <li <?php echo ($page == 'profile') ? 'class="active"' : '' ?>>
                <a href="<?php echo ($page == 'profile') ? 'javascript:;' : make_admin_url('profile') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="Profile"> 
                        <span class="fa fa-user"></span> 
                    </span>
                    <span class="full-nav"> Profile </span>
                </a>
            </li>
            <li <?php echo ($page == 'salary') ? 'class="active"' : '' ?>>
                <a href="<?php echo ($page == 'salary') ? 'javascript:;' : make_admin_url('salary') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="Salary"> 
                        <span class="fa fa-inr"></span> 
                    </span>
                    <span class="full-nav"> Salary </span>
                </a>
            </li>
            <li <?php echo ($page == 'school_sales') ? 'class="active"' : '' ?>>
                <a href="<?php echo ($page == 'school_sales') ? 'javascript:;' : make_admin_url('school_sales') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="School Sales"> 
                        <i class="fa fa-shopping-cart"></i> 
                    </span>
                    <span class="full-nav"> School Sales </span>
                </a>
            </li>
            <li <?php echo ($page == 'syllabus') ? 'class="active"' : '' ?>>
                <a href="<?php echo ($page == 'syllabus') ? 'javascript:;' : make_admin_url('syllabus') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="Syllabus"> 
                        <i class="fa fa-file-o"></i> 
                    </span>
                    <span class="full-nav"> Syllabus </span>
                </a>
            </li>
            <li <?php echo ($page == 'circulars') ? 'class="active"' : '' ?>>
                <a href="<?php echo ($page == 'circulars') ? 'javascript:;' : make_admin_url('circulars') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="Circulars"> 
                        <i class="fa fa-inbox"></i> 
                    </span>
                    <span class="full-nav"> News and Circulars </span>
                </a>
            </li>
            <li <?php echo ($page == 'classes') ? 'class="active"' : '' ?>>
                <a href="<?php echo ($page == 'classes') ? 'javascript:;' : make_admin_url('classes') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="classes"> 
                        <i class="fa fa-users"></i> 
                    </span>
                    <span class="full-nav"> Classes </span>
                </a>
            </li>
            <li <?php echo ($page == 'time_table') ? 'class="active"' : '' ?>>
                <a href="<?php echo ($page == 'time_table') ? 'javascript:;' : make_admin_url('time_table') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="Time Table"> 
                        <i class="fa fa-clock-o"></i> 
                    </span>
                    <span class="full-nav"> Time Table </span>
                </a>
            </li>
            <li <?php echo ($page == 'event') ? 'class="active"' : '' ?>>
                <a href="<?php echo ($page == 'event') ? 'javascript:;' : make_admin_url('event') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="Activity Calendar"> 
                        <i class="fa fa-calendar"></i> 
                    </span>
                    <span class="full-nav"> Activity Calendar </span>
                </a>
            </li>
            <li <?php echo ($page == 'application') ? 'class="active"' : '' ?>>
                <a href="<?php echo ($page == 'application') ? 'javascript:;' : make_admin_url('application') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="Leave Application"> 
                        <i class="fa fa-pencil-square-o"></i> 
                    </span>
                    <span class="full-nav"> Leave Application </span>
                </a>
            </li>
            <li>
                <a href="<?php echo make_admin_url('logout') ?>">
                    <span class="small-nav" data-toggle="tooltip" data-placement="right" title="Logout"> 
                        <i class="fa fa-lock"></i> 
                    </span>
                    <span class="full-nav"> Logout </span>
                </a>
            </li>
        </ul>
    </div>
</div>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).ready(function () {
        $('.navbar-nav [data-toggle="tooltip"]').tooltip();
        $('.navbar-twitch-toggle').on('click', function (event) {
            event.preventDefault();
            $('.navbar-twitch').toggleClass('open');
        });
        $('.nav-style-toggle').on('click', function (event) {
            event.preventDefault();
            var $current = $('.nav-style-toggle.disabled');
            $(this).addClass('disabled');
            $current.removeClass('disabled');
            $('.navbar-twitch').removeClass('navbar-' + $current.data('type'));
            $('.navbar-twitch').addClass('navbar-' + $(this).data('type'));
        });
    });
</script>
