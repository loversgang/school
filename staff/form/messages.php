<?php

switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'update':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/edit.php');
        break;
    case 'insert':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create.php');
        break;
    case 'view':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/view.php');
        break;
    case 'thrash':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/thrash.php');
        break;
    case 'compose':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/compose.php');
        break;
    case 'getlist':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/getlist.php');
        break;
    case 'read':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/read.php');
        break;
    default:break;
endswitch;
