<?
# Login messages.
define("MSG_LOGIN_INVALID_USERNAME_PASSWORD","Invalid <b>Username</b> or <b>Password</b>.",true);
define("MSG_LOGIN_WELCOME_MSG","Welcome to my account section.",true);
define("MSG_LOGIN_EMAIL_ADDRESS_ALREADY_EXIST","This email address already exists. Please try again.",true);
define("MSG_LOGIN_USERNAME_ALREADY_EXIST","This Username already exists. Please try again.",true);
define("MSG_LOGIN_WRONG_PASSWORD","Password does not match.",true);
define("MSG_LOGIN_EMPTY_PASSWORD","Password cannot be left empty.",true);
define("MSG_LOGIN_ALREADY_LOGGED_IN", "Sorry! you are already logged in.");
define("MSG_LOGIN_EMAIL_FORMATE", "Please use correct Email format <b>like: username@username.com</b>.<br/>You cannot leave <b>password field</b> blank.");
define("MSG_LOGIN_NOT_BLANK", "You cannot leave <b>email field</b> blank.<br/>You cannot leave <b>password field</b> blank.");
define("MSG_LOGIN_ELSE", "You cannot leave <b>email field</b> blank.<br/> Please use correct Email format <b>like: username@username.com</b>.<br/> You cannot leave <b>password field</b> blank.");
define("MSG_SEC_NOT_VALID", "Security code could not be validated. Please try again.");
# my account messages.
define("MSG_REGISTER_PASSWORDS_NOT_SAME","Your passwords do not match",true);


define("MSG_ACCOUNT_PASSWORD_CHANGE_SUCCESS","Your password has been successfully changed.",true);
define("MSG_ACCOUNT_UPDATE_SUCCESS","Your account information has been updated successfully.",true);
define("MSG_ACCOUNT_UPDATE_FAILED","Your account was not updated. Please try again.",true);
define("MSG_ACCOUNT_ALREADY_ACTIVE", "Your account is already active. Please <a href='".DIR_WS_SITE."?page=login'>click here</a> to login.");
define("MSG_ACCOUNT_MADE_ACTIVE", "Your account has been activated. An email containing your login details has been sent to your email address. Please <a href='".DIR_WS_SITE."?page=login'>click here</a> to login.");
define("MSG_ACCOUNT_NOT_MADE_ACTIVE", "Alas! some techinical malfunction has taken place. Please try again.");
define("MSG_ACCOUNT_WRONG_USER_INFO", "You have used a wrong user information. If you are not a registred user yet. Please <a href='".DIR_WS_SITE."?page=register'>click here</a> to register.");
define("MSG_ACCOUNT_WRONG_URL", "You have used a wrong URL. If you are not a registred user yet. Please <a href='".DIR_WS_SITE."?page=login'>click here</a> to register.");
define("MSG_ACCOUNT_CONFIRM_EMAIL_RESEND_SUCCESS", "Confirmation email has been successfully resent to your registered email address.");
define("MSG_ACCOUNT_CONFIRM_EMAIL_NEW_RESEND_SUCCESS", "Confirmation email has been successfully sent to your new email address.");


#logout messages.
define("MSG_LOGOUT_SUCCESS","You have successfully <b>logged out</b>.",true);


#registrations messages.
define("MSG_REGISTR_FAILED", "Sorry! Registration process failed. Please try again.");
define("MSG_REGISTER_PASSWORDS_NOT_SAME", "Password and confirm password must be same.");
define("MSG_REGISTER_EMAIL_ALREADY_EXIST", "Sorry! this email address already exists. If you have forgotten your login details. Please <a href='".DIR_WS_SITE."?page=login'>click here</a> to know your login details.");
define("MSG_REGISTER_SUCCESS", "You details have been accepted.");


# forgot password messages.
define("MSG_FORGOT_PASSWORD_SUCCESS", "A link has been sent to your email address. Please click that link to set new password.");
define("MSG_FORGOT_PASSWORD_FAIL", "Your email address does not exist in our database.<br />If you are a new user. Please <a href='".DIR_WS_SITE."?page=register'>click here</a> to register.<br /> You can <a href='".DIR_WS_SITE."?page=forgotpassword'>click here</a> to try again.");



# change password messages.
define("MSG_CHANGE_PASSWORD_SUCCESS", "Your password has been changed successfully.<br>Your new login details have been sent to your email address.");
define("MSG_CHANGE_PASSWORD_FAILED", "You have entered a wrong password.");
define("MSG_PASSWORD_RESET", "Your <b>account password</b> has been reset successfully.");
define("MSG_NOT_AUTHORIZED", "You are not <b>authorized</b> to do this.");

#others.
define("MSG_ONLINE_ENQUIRY_SUCCESS", "Your online enquiry has successfully been submitted.");
define("MSG_FREE_SAMPLE_SENT_SUCCESS", "Thank you, Your free sample request has been sent. Our support staff will contact you soon.");
define("MSG_PAYMENT_SUCCESS","Your order has successfully been placed. You can track your order states by logging into your online account.<br/> Your login details
have been emailed to you (in case you are a new customer). Please <a href='".DIR_WS_SITE."?page=login'><strong><u>click here</u></strong></a> to login.");
//define("MSG_CUSTOM_SUCCESS","Thank you for shopping at ".SITE_NAME.".Your order will be processed shortly.");

#cart messages.
define("MSG_CART_OUT_OF_STOCK", 'Sorry! This item is currently out of stock.');
define("MSG_CART_ALREADY_IN_CART", 'This item is already in the cart.');

#shopping messages.
define("MSG_SHOP_CATEGORY_NO_PRODUCT_FOUND","Sorry! There is no product in this category.",true);
define("MSG_SHOP_ORDER_COMPLETE_SUCCESS","You have successfully completed your order.",true);


# website control panel messages.
define("MSG_ADMIN_UPDATE_SUCCESS","Record updated successfully",true);
define("MSG_ADMIN_DELETE_SUCCESS","Record deleted successfully",true);
define("MSG_ADMIN_ADDITION_SUCCESS","Record added successfully",true);
define("MSG_ADMIN_PERMISSION_DENIED","You donot have permission to access the page.",true);


define("MSG_CONTACTUS","Your request have been submitted successfully.");
define("MSG_CONTACTUS_COMPLETE_INFO","Please enter compalete information.");

define("MSG_BUSINESS","Your Business has been added successfully. Please wait for the administrator approvel.");
define("MSG_INTERACTIVE_MAP","Please select a Business Category.");

/**For front end msg**/

define("MSG_MAIL_SEND","Mail send successfully.");
define("MSG_DOCTYPE_ERROR","Please upload valid document type.");
define("MSG_FIELD_EMPTY_ERROR","Please fill your empty fields.");

define("MSG_PLACEMENT_ERROR","Sorry! No placement detail available.");

define("MSG_RESOURCES_ERROR","Sorry! No links available.");

define("MSG_DOCUMENT_ERROR","Sorry! No document available.");

define("MSG_COURSE_ERROR","No courses started for this department."); //If no course exist in department
define("MSG_COURSE_DEP_ERROR","Sorry! Currently no courses started."); //If no course exist in department

define("MSG_JOB_APPLY_MAIL_SUCCESS","Your job apply request send successfully.");

define("MSG_PROSPECTUS_REQUEST_SUCCESS","Your request has been send successfully.");

define("MSG_FEEDBACK_SUCCESS","Thanks for your feedback.");

define("MSG_JOBS_ERROR","Currently no job open for this department."); //If Job not exist for department
define("MSG_JOBS_DEP_ERROR","Sorry! Currently no job open."); //If Job department not active

define("MSG_FACULTY_ERROR","Currently No faculty apointed."); //If No faculty available.

define("MSG_NEWS_ERROR","Sorry! No news available."); //If No News available.

define("MSG_EVENT_ERROR","Sorry! No new event started."); //If No EVENT available.
define("MSG_EVENT_CALENDAR_ERROR","Sorry! No new event started."); //If No EVENT available.

define("MSG_ACHIEVEMENT_ERROR","Sorry! No achievement available."); //If No Achievement available.

define("MSG_NOTICE_BOARD_ERROR","No new notice available."); //If No New Notice available.

define("MSG_AFFILIATIONS_ERROR","Sorry! No Affiliations available."); //If No Affiliations available.

define("MSG_FAQ_ERROR","Sorry! No FAQS."); //If No Affiliations available.

define("SEARCH_ERROR_MSG","Sorry, but nothing matched your search criteria. Please try again with some different keywords.");

#Institute message
define("MSG_COURSE_ADDED","New Course has been added successfully."); //Add Course .
define("MSG_COURSE_UPDATE","Course has been updated successfully."); //Update Course .
define("MSG_EXPENSE_ADDED","New Expense has been added successfully."); //Add Expense .
define("MSG_EXPENSE_UPDATE","Expense has been updated successfully."); //Update Expense .
define("MSG_EXPENSE_CAT_ADDED","New Expense Category has been added successfully."); //Add Expense .
define("MSG_EXPENSE_CAT_UPDATE","Expense Category has been updated successfully."); //Update Expense .

define("MSG_INCOME_ADDED","New Income has been added successfully."); //Add Income .
define("MSG_INCOME_UPDATE","Income has been updated successfully."); //Update Income .
define("MSG_INCOME_CAT_ADDED","New Income Category has been added successfully."); //Add Income .
define("MSG_INCOME_CAT_UPDATE","Income Category has been updated successfully."); //Update Income .

define("MSG_SCHOOL_INFO_UPDATE","Institute information has been updated successfully."); //school info update  .
define("MSG_CONTENT_ADDED","New Content page has been added successfully."); //Add Content .
define("MSG_CONTENT_UPDATE","Content page has been updated successfully."); //Update Content .
define("MSG_NOTICE_ADDED","New Notice has been added successfully."); //Add Notice .
define("MSG_NOTICE_UPDATE","Notice has been updated successfully."); //Update Notice .
define("MSG_GALLERY_ADDED","Gallery Image has been inserted successfully."); //Add GALLERY .
define("MSG_GALLERY_UPDATE","Gallery Image has been updated successfully."); //Update GALLERY .
define("MSG_STAFF_ADDED","Staff Member has been added successfully."); //Add Staff .
define("MSG_STAFF_UPDATE","Staff Member details has been updated successfully."); //Add Staff .
define("MSG_STUDENT_DETAIL_ADDED","Student detail has been added successfully."); //Add student .
define("MSG_STUDENT_ADDRESS_ADDED","Student address has been added successfully."); //Add address .
define("MSG_STUDENT_FAMILY_ADDED","Family detail has been added successfully."); //Add family .
define("MSG_STUDENT_PREVIOUS_SCHOOL_ADDED","Previous Institute's Information has been added successfully."); //Add Previous Institute .
define("MSG_STUDENT_DOCUMENT_ADDED","Document has been added successfully."); //Add Student doc .
define("MSG_STUDENT_DOCUMENT_UPDATE","Document has been updated successfully."); //Update doc head.
define("MSG_STUDENT_ID_REQUIRE","Please add the basic detail of student first."); //Add Student doc .

define("MSG_SESSION_ADDED","New Session has been added successfully."); //Add Expense .
define("MSG_SESSION_UPDATE","Session has been updated successfully."); //Update Expense .
define("MSG_SESSION_STUDENT_ADDED","Student has been successfully added into session."); //session student added .
define("MSG_SESSION_STUDENT_DELETE","Student has been successfully deleted from session."); //session student delete .
define("MSG_SESSION_MULTI_STUDENT_ADDED","Multiple Students has been successfully added into session."); //session Multiple student added .
define("MSG_SELECT_MULTI_STUDENT","Please select the students which you want to add into session."); //session student require .
define("MSG_SESSION_STUDENT_ALREADY_ADDED","This Student already added into this session."); //session student added .
define("MSG_SESSION_EMPTY","Sorry, This session has no student."); //session empty.
define("MSG_SUBJECT_STUDENT_EMPTY","Sorry, There is no student in this subject."); //no student empty.

define("MSG_VEHICLE_ADDED","New Vehicle has been added successfully."); //Add Vehicle .
define("MSG_VEHICLE_UPDATE","Vehicle has been updated successfully."); //Update Vehicle .
define("MSG_VEHICLE_SINGLE_STUDENT_ADDED","Student has been successfully added into vehicle."); //vehicle single student added .
define("MSG_VEHICLE_SINGLE_STUDENT_ALREDY_ADDED","Vehicle already assigned for this student. If you want to reselect please remove student from vehicle section."); //vehicle single student added .
define("MSG_VEHICLE_SINGLE_STUDENT_ALREDY_ADDED_OLD","Vehicle already assigned for this student. If you want to reselect please stop service for that vehicle."); //vehicle single student added .
define("MSG_VEHICLE_MULTI_STUDENT_ADDED","Multiple Students has been successfully added into vehicle."); //vehicle Multiple student added .
define("MSG_ATTENDANCE_ADDED","attendance has been added successfully."); //Add attendance .


define("MSG_SESSION_SECTION_MORE_DATE","Sorry, You can add attendance from session start to end date."); //Add attendance .

define("MSG_ATTENDANCE_ALREDY_ADDED","For this date attendance has been already added."); //already added attendance .
define("MSG_EXAM_ADDED","New Examination has been added successfully."); //Add exam .
define("MSG_EXAM_UPDATE","Examination has been updated successfully."); //Update exam  .
define("MSG_EXAM_NEED","Please select the examinations."); //need select exam  .
define("MSG_EXAM_MARKS_ADDED","Examination Marks has been added successfully."); //add exam marks .
define("MSG_EXAM_MARKS_UPDATE","Examination Marks has been updated successfully."); //Update exam marks .
define("MSG_EXAM_MARKS_NEED","Please add all student's marks."); //need exam marks .
define("MSG_EXAM_MARKS_NOT_MAX","Student marks should be less than maximum marks."); //need exam marks .
define("MSG_EXAM_GROUP_ADDED","Examination Group has been added successfully."); //add exam Group .
define("MSG_EXAM_GROUP_UPDATE","Examination Group has been updated successfully."); //add exam Group .
define("MSG_STUDENT_FEE_ADDED","Student Fee has been added successfully."); //add student fee .
define("MSG_STUDENT_FEE_UPDATED","Student Fee has been updated successfully."); //update student fee .
define("MSG_STUDENT_FEE_ALREAY_ADDED_MONTH","Student Fee has been already added for this month."); //already add student fee .
define("MSG_STUDENT_NEED_SELECT","Please select the student firstly."); //already add student fee .
define("MSG_SESSION_SECTION_NEED_SELECT","Please select the session & section."); //already add student fee .
define("MSG_MAX_COURSE_PART_1","Sorry, You are not allowed to add more than "); //max course part 1 .
define("MSG_MAX_COURSE_PART_2"," course"); //max course part 2 .
define("MSG_SESSION_STUDENT_LIMIT","Sorry, Your maximum section limit is over in this session."); //max course part 1 .
define("MSG_ROLL_NO_DUPLICATE","Sorry, This roll no is already assigned to another student."); //duplicate roll no .
define("MSG_STUDENT_INFO_UPDATE","Student information updated successfully."); //update student section and roll no .
define("MSG_MEX_BOY_LIMIT_IN_SECTION_1","Your maximum boys limit exceed in Section "); //max boy in section part 1 .
define("MSG_MEX_BOY_LIMIT_IN_SECTION_2"," please change the limit in the session."); //max boy in sectionpart 2 .
define("MSG_MEX_GIRL_LIMIT_IN_SECTION_1","Your maximum girls limit exceed in section "); //max girl in section part 1 .
define("MSG_MEX_GIRL_LIMIT_IN_SECTION_2"," please change the limit in the session."); //max girl in sectionpart 2 .
define("SESSION_STAFF_UPDATE","Section In-charge name has been updated successfully."); //section staff update .
define("MSG_HOLIDAY_ADDED","New Holiday has been added successfully."); //Add Holiday .
define("MSG_HOLIDAY_UPDATE","Holiday has been updated successfully."); //Update Holiday  .

define("MSG_STAFF_CAT_ADDED","Staff Category has been added successfully."); //Add Staff Cat.
define("MSG_SALARY_ADDED","Staff Salary has been added successfully."); //Add Staff Salary.
define("MSG_SALARY_UPDATE","Staff Salary has been updated successfully."); //update Staff Salary.
define("MSG_SALARY_ALREAY_ADDED_MONTH","Sorry, Salary already added of this month for this staff member."); //already added Staff Salary.

define("MSG_STAFF_CAT_UPDATE","Staff Category has been updated successfully."); //Update Staff Cat.
define("MSG_STAFF_DESIGNATION_ADDED","Staff Designation has been added successfully."); //Add Staff Designation.
define("MSG_STAFF_DESIGNATION_UPDATE","Staff Designation has been updated successfully."); //Update Staff Designation.
define("MSG_SUBJECT_ADDED","New Subject has been added successfully."); //Add Subject.
define("MSG_SUBJECT_UPDATE","Subject has been updated successfully."); //Subject fee head.
define("MSG_FEE_HEADS_ADDED","Fee Head has been added successfully."); //Add fee head.
define("MSG_FEE_HEAD_UPDATE","Fee Head has been updated successfully."); //Update fee head.
define("MSG_GRADE_HEADS_ADDED","New Grade has been added successfully."); //Add Grade head.
define("MSG_GRADE_HEAD_UPDATE","Grade has been updated successfully."); //Grade fee head.
define("MSG_DOCUMENT_GENERATETD","Record added to the document history."); //Update fee head.
define("MSG_NOT_ALLLOWED_MORE_THEN_THREE_MONTH","Sorry, You are not allowed to select more than three months."); //hight range of dates .
define("MSG_STUDENT_FEE_UPDATE","Student Fee details has been updated successfully."); //update student fee .

define("MSG_SCHOOL_TIME_UPDATE","Institute Timing has been updated successfully."); //Update school time.
define("MSG_SOCIAL_MEDIA_UPDATE","Social Media has been updated successfully."); //Update Social Media.

define("MSG_ONLY_LIMIT_STUDENT_1","Sorry, You cannot add more than "); //Limit Student.
define("MSG_ONLY_LIMIT_STUDENT_2"," student in your session."); //Limit Student.
define("OPERATION_PERFORM_SUCCESS","Operation has been performed successfully"); //operation perform.
define("OPERATION_CANCEL","The operation has been cancelled"); //operation cancel.
define("MSG_REG_ID_EXIST","Sorry, This registration id is already exist"); //reg id exist.
define("MSG_NOT_ADDED_TO_SESSION","Sorry, this student can't be added in this session, please try again."); //reg id exist.
define("MSG_ENQUIRY_ADDED","New Enquiry has been added successfully."); //Add Enquiry.
define("MSG_ENQUIRY_UPDATE","Enquiry has been updated successfully."); //Enquiry fee head.

define("MSG_LAST_DOC_PRINT","Last date of printing this document is "); //last date printing.
define("MSG_RESELECT_PAYMENT_INTERVAL","Please reselect payment inverval."); //last date .
define("MSG_SESSION_NOT_DELETE","Sorry, You can't delete this session until it has students."); //not empty .
define("MSG_HOLIDAY_DATE_SUNDAY","Sorry, You can't mark the attendance for sunday."); //holiday  .
define("MSG_HOLIDAY_DATE","Sorry, You can't mark the attendance for school holiday."); //holiday  .

define("MSG_THIS_SESSSION_PASSED","Sorry, This session already passed away."); //holiday  .

define("PATMMENT_DATE_WITH_IN","Your payment date should be within "); //not empty .
?>