<?php

# WEBSITE CONSTANTS
$constants = new query('setting');
$constants->DisplayAll();
while ($constant = $constants->GetObjectFromRecord()):
    define("$constant->key", $constant->value, true);
endwhile;

# PHP Validation types
define('VALIDATE_REQUIRED', "req", true);
define('VALIDATE_EMAIL', "email", true);
define("VALIDATE_MAX_LENGTH", "maxlength");
define("VALIDATE_MIN_LENGTH", "minlength");
define("VALIDATE_NUMERIC", "num");
define("VALIDATE_ALPHA", "alpha");
define("VALIDATE_ALPHANUM", "alphanum");

define("TEMPLATE", "default");
define("CURRENCY_SYMBOL", "<i class='icon-inr'></i> ");

define("ADMIN_FOLDER", 'webgarh');
define("ADMIN_PUBLIC_FOLDER", 'webgarh');
define("STAFF_PUBLIC_FOLDER", 'staff');
define("PUBLIC_FOLDER", 'public');

define("ADD_ATTRIBUTE_PRICE_TO_PRODUCT_PRICE", true);
define("ATTRIBUTE_PRICE_OVERLAP", false);

define('VERIFY_EMAIL_ON_REGISTER', true);
define('ERROR_EMAIL', 'rocky.developer004@gmail.com', true);


$conf_shipping_type = array('quantity', 'subtotal');

define('SHIPPING_TYPE', 'Quantity');

define('CSS_VERSION', "1.4", true);
define('JS_VERSION', "1.5", true);

$AllowedImageTypes = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$AllowedFileTypes = array('application/vnd.ms-excel');

# new allowed photo mime type array.
$conf_allowed_file_mime_type = array('application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'applications/vnd.pdf', 'application/pdf');
$conf_allowed_import_file_mime_type = array('application/vnd.ms-excel', 'application/msexcel');
$conf_allowed_photo_mime_type = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$conf_allowed_audio_mime_type = array('audio/mpeg', 'audio/mpeg', 'audio/mpg', 'audio/x-mpeg', 'audio/mp3', 'application/force-download', 'application/octet-stream');

$conf_order_status = array('Received', 'Processing', 'Shipped', 'Delivered');

define("DOC_TYPE", '<!DOCTYPE html>', true);
define("DOC_LANGUAGE", "en", true);
define("DOC_CHAR_SET", 'utf-8', true);

/* set soft delete status */
define("SOFT_DELETE", 1, true);


$conf_rewrite_url = array(
    'achievement' => 'achievement',
    'affiliations' => 'affiliations',
    'contact-us' => 'contact-us',
    'content' => 'content',
    'courses' => 'courses',
    'document' => 'document',
    'event' => 'event',
    'eventcalendar' => 'eventcalendar',
    'faculty' => 'faculty',
    'faqs' => 'faqs',
    'gallery' => 'gallery',
    'home' => '',
    'jobapply' => 'jobapply',
    'jobs' => 'jobs',
    'location' => 'location',
    'news' => 'news',
    'notice' => 'notice',
    'placement' => 'placement',
    'request-a-prospectus' => 'request-a-prospectus',
    'resources' => 'resources',
    'schooltiming' => 'schooltiming',
    '404' => '404'
);



/* Decide the file logo according to extension */

$file_extension = array(
    'pdf' => 'pdf.jpg',
    'doc' => 'word.png',
    'docx' => 'word.png',
    'txt' => 'file.png',
);


$imageThumbConfig = array(
    'school' => array(
        'thumb' => array('width' => '112', 'height' => '60', 'crop' => '1'),
        'medium' => array('width' => '259', 'height' => '180', 'crop' => '1'),
        'big' => array('width' => '800', 'height' => '600'),
    ),
);

$members_array = array(
    "Name" => "name",
    "Username" => "username",
    "ContactPersonName" => "contact_name",
    "ContactPersonEmail" => "contact_email",
    "ContactPersonPhone" => "contact_phone",
    "Link" => "link",
    "MailingAddress" => "mailing_address",
    "PhysicalAddress" => "physical_address",
    "Mobile" => "mobile",
    "Phone" => "phone",
    "PhoneTollfree" => "phone_tollfree",
    "Fax" => "fax",
    "Email" => "email_address",
    "FacebookPage" => "facebook",
    "TwitterPage" => "twitter",
    "YoutubePage" => "youtube"
);

$not_to_open_page = array(
    '1' => 'slider',
    '2' => 'navigation',
    '3' => 'innerfooter'
);

/* module cache array */
$module_cache_folder = array(
//'news'=>array('0'=>'news','1'=>'news_item'),
    'achievement' => array('0' => 'achievement'),
    'affiliations' => array('0' => 'affiliations'),
    'content' => array('0' => 'content'),
    'courses' => array('0' => 'courses'),
    'document' => array('0' => 'document'),
    'event' => array('0' => 'event', '1' => 'eventcalendar'),
    'faculty' => array('0' => 'faculty'),
    'faqs' => array('0' => 'faqs'),
    'gallery' => array('0' => 'gallery'),
    'news' => array('0' => 'news'),
    'jobs' => array('0' => 'jobs'),
    'notice' => array('0' => 'notice'),
    'placement' => array('0' => 'placement'),
    'resources' => array('0' => 'resources')
        )
?>