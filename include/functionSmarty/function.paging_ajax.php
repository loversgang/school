<?php 
/** 
 * Smarty plugin for ajax paging
 * 
 * @package Smarty 
 * @subpackage Plugin Paging
 * @author varun@cwebconsultants.com 
 */ 


function smarty_function_paging_ajax($params, &$smarty)
{
              
                $max_records=10;$url='';$querystring='';
                $page=$params['page'];
                $totalPages=$params['total_pages'];
                $totalRecords=$params['total_records'];
               
                
                 if(isset($params['query'])):
                   $querystring=$params['query'];
                 endif;
                 if(isset($params['url'])):
                   $url=$params['url'];
                 endif;
                 if(isset($params['max_records'])):
                   $max_records=$params['max_records'];
                 endif;
           
                
	        /*Paging by Ajax*/
		# $Pp-previous page
		# $Np- next page
		($page>=$totalPages)?$Np=$totalPages:$Np=$page+1;
		($page<=1)?$Pp=1:$Pp=$page-1;
		if($totalPages>3):
			if(($page+3) <=$totalPages):
				$end=$page+3;
				$begin=$page;
			else:
				$begin=$totalPages-3;
				$end=$totalPages;
			endif;
		else:
			$begin=1;
			$end=$totalPages;
		endif;
               
		?>

<!---------------------------------------------------------new paging div starts here---------------------------------------------->
                <div class="row-fluid">
                        <div class="pagination pagination-centered">
                              <ul>
                                  <li>
                                    <a class="paging_change" href="javascript:void(0)" id="<?php echo $Pp ?>" rel="<?php echo $totalPages?>" max_records="<?php echo $max_records?>"  query_string="<?php echo $querystring?>" title="Previous Page">&laquo;</a>                                        
                                  </li>
                                     <?
                                        for($i=$begin;$i<=$totalPages && $i<=$end;$i++):
                                        if($i==$page):
                                    ?>
                                          <li><a href="javascript:void(0)" id="<?php echo $i ?>" rel="<?php echo $totalPages?>" max_records="<?php echo $max_records?>"  query_string="<?php echo $querystring?>" class="active paging_change"><?php echo $i;?></a></li>
                                      <?php else: ?>
                                           <li><a href="javascript:void(0)" id="<?php echo $i ?>" rel="<?php echo $totalPages?>" max_records="<?php echo $max_records?>"  query_string="<?php echo $querystring?>" class="paging_change"><?php echo $i;?></a></li> 
                                  <?php endif;					
                                        endfor;
                                  ?>
                                   <li>
                                       <a  class="paging_change" href="javascript:void(0)" id="<?php echo $Np ?>" rel="<?php echo $totalPages?>" max_records="<?php echo $max_records?>"  query_string="<?php echo $querystring?>" title="Next Page">&raquo;</a>
                                   </li>            
                             </ul>
                        </div>
                </div>  

<!--------------------------------------------------------new paging div ends here---------------------------------------------------->

 <?php          
}
?> 