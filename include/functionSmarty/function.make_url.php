<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     function.url.php
 * Type:     function
 * Name:     make url
 * Purpose:  to make url function
 * Author    varun@cwebconsultants.com
 * -------------------------------------------------------------
 */

function smarty_function_make_url($params, &$smarty)
{
    $query=null;
 
    $page=$params['page'];
    if(isset($params['query'])):
     $query=$params['query'];    
    endif;
    return make_url($page,$query);
   
}
?>