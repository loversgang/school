<?php

function make_url($page, $query=NULL) {
    
    global $conf_rewrite_url;
    parse_str($query, $string);
    if (isset($conf_rewrite_url[strtolower($page)]))
        return _makeurl($page, $string);
    else 
        return DIR_WS_SITE . '?page=' . $page . '&' . $query;
}

function verify_string($string) {

    if ($string != '')
        if (substr($string, -1) == '/')
            return substr($string, 0, strlen($string) - 1);

    return $string;
}

function load_url() {
    global $conf_rewrite_url;
    $prefix = str_replace(HTTP_SERVER, '', DIR_WS_SITE);
    $URL = $_SERVER['REQUEST_URI'];
    if (strpos($URL, '?') === false):
        $string = substr($URL, -(strlen($URL) - strlen($prefix)));
        $string = verify_string($string);
        $string_parts = explode('/', $string);
        $url_array = array_flip($conf_rewrite_url);
        if (isset($url_array[$string_parts['0']])):
            _load($url_array[$string_parts['0']], $string_parts);
        endif;
    endif;
}

function _makeurl($page, $string) {

    switch ($page) {

        case 'content':
            if (isset($string['id'])):
                $object = get_object('content', $string['id']);
                return DIR_WS_SITE . 'content/' . $object->urlname;
            endif;
            break;

        case 'home':
            if (isset($string['msg'])):
                return DIR_WS_SITE . $string['msg'];
            else:
                return DIR_WS_SITE;
            endif;
            break;

        case '404':
            return DIR_WS_SITE . '404';
            break;

        case 'faqs':
            if (isset($string['id'])):
                $object = get_object('faq', $string['id']);
                return DIR_WS_SITE . 'faqs/' . $object->id;
            else:
                return DIR_WS_SITE . 'faqs';
            endif;
            break;

//        case 'faqs':
//            return DIR_WS_SITE . 'faqs';
//            break;

        case 'news':
            if (isset($string['id'])):
                $object = get_object('news', $string['id']);
                return DIR_WS_SITE . 'news/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'news';
            endif;
            break;

        case 'contact':
            if (isset($string['msg'])):
                return DIR_WS_SITE . 'contact/' . $string['msg'];
            else:
                return DIR_WS_SITE . 'contact';
            endif;
            break;

        case 'achievement':
            if (isset($string['id'])):
                $object = get_object('achievement', $string['id']);
                return DIR_WS_SITE . 'achievement/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'achievement';
            endif;
            break;

        case 'affiliations':
            return DIR_WS_SITE . 'affiliations';
            break;

        case 'courses':

            if (isset($string['id']) && isset($string['docid'])):
                $object = get_object('course', $string['id']);
                if (is_object($object)):
                    if ($object->urlname == ''):
                        return DIR_WS_SITE . 'courses/' . $object->id . '/' . urlencode($string['docid']);
                    else:
                        return DIR_WS_SITE . 'courses/' . $object->urlname . '/' . urlencode($string['docid']);
                    endif;
                else:
                    return DIR_WS_SITE . 'courses';
                endif;
            elseif (isset($string['id'])):
                $object = get_object('course', $string['id']);
                if (is_object($object)):
                    if ($object->urlname == ''):
                        return DIR_WS_SITE . 'courses/' . $object->id;
                    else:
                        return DIR_WS_SITE . 'courses/' . $object->urlname;
                    endif;
                else:
                    return DIR_WS_SITE . 'courses';
                endif;
            else:
                return DIR_WS_SITE . 'courses';
            endif;
            break;

        case 'document':
            if (isset($string['id'])):
                $object = get_object('document', $string['id']);
                return DIR_WS_SITE . 'document/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'document';
            endif;
            break;

        case 'event':
            if (isset($string['id'])):
                $object = get_object('event', $string['id']);
                return DIR_WS_SITE . 'event/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'event';
            endif;
            break;

        case 'eventcalendar':
            if (isset($string['id'])):
                $object = get_object('event', $string['id']);
                return DIR_WS_SITE . 'eventcalendar/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'eventcalendar';
            endif;
            break;

        case 'faculty':
            if (isset($string['id']) && !isset($string['depid'])){
                $object = get_object('team', $string['id']);
                return DIR_WS_SITE . 'faculty/' . $object->urlname;
            }
            else if (isset($string['depid']) && !isset($string['id'])){
                //print_r($object);die;
                $object = get_object('dropdown_values', $string['depid']);
                return DIR_WS_SITE . 'faculty/department/'.$object->title;
            }
            else if (isset($string['id']) && isset($string['depid'])){
                //print_r($object);die;
                $object = get_object('dropdown_values', $string['depid']);
                $object1 = get_object('team', $string['id']);
                //print_r($object1);die;
                return DIR_WS_SITE . 'faculty/department/'.$object->title.'/'.$object1->urlname;
            }
            else{
                return DIR_WS_SITE . 'faculty';
            }
            break;

            case 'notice':
            if (isset($string['id'])):
                $object = get_object('notice', $string['id']);
                return DIR_WS_SITE . 'notice/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'notice';
            endif;
            break;
			
            case 'notice_detail':
            if (isset($string['id'])):
                $object = get_object('notice_board', $string['id']);
                return DIR_WS_SITE . 'notice_detail/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'notice_detail';
            endif;
            break;
			
            case 'staff':
            return DIR_WS_SITE . 'staff';
            break;			

            case 'gallery':
            if (isset($string['id'])):
                $object = get_object('gallery', $string['id']);
                return DIR_WS_SITE . 'gallery/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'gallery';
            endif;
            break;
        default:break;
    }
}

function _load($page, $string_parts) {
    global $conf_rewrite_url;
    switch ($page) {

        case 'content':
            if (count($string_parts) == 2):
                $object = get_object_by_col('content', 'urlname', urldecode($string_parts['1']));
                $_REQUEST['page'] = 'content';
                $_GET['id'] = $object->id;
            endif;
            break;

        case 'home':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'home';
                $_GET['msg'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'home';
            endif;
            break;

        case '404':
            $_REQUEST['page'] = '404';
            break;

        case 'faqs':
            if (count($string_parts) == 2):
                $object = get_object_by_col_urlrewrite('faq', 'id', urldecode($string_parts['1']));
                    if(is_object($object)):
                        $_GET['id'] = $object->id;
                    else:
                         $_GET['id'] = 0;
                    endif;
                    $_REQUEST['page'] = 'faqs';
            else:
                $_REQUEST['page'] = 'faqs';
            endif;
            break;

        case 'staff':
            $_REQUEST['page'] = 'staff';
           break;

        case 'news':
            if (count($string_parts) == 2):
                $object = get_object_by_col_urlrewrite('news', 'urlname', urldecode($string_parts['1']));
                    if(is_object($object)):
                        $_GET['id'] = $object->id;
                    else:
                         $_GET['id'] = 0;
                    endif;
                    $_REQUEST['page'] = 'news';
            else:
                $_REQUEST['page'] = 'news';
            endif;
            break;

        case 'contact':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'contact';
                $_GET['msg'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'contact';
            endif;
            break;


        case 'courses':
            if (count($string_parts) == 3):
                $object = get_object_by_col_urlrewrite('course', 'urlname', urldecode($string_parts['1']));
                $_REQUEST['page'] = 'courses';
                $_GET['docid'] = urldecode($string_parts['2']);
                if (is_object($object)):
                    $_GET['id'] = $object->id;
                else:
                    $_GET['id'] = urldecode($string_parts['1']);
                endif;
            elseif (count($string_parts) == 2):
                $object = get_object_by_col_urlrewrite('course', 'urlname', urldecode($string_parts['1']));
                $_REQUEST['page'] = 'courses';
                if (is_object($object)):
                    $_GET['id'] = $object->id;
                else:
                    $_GET['id'] = urldecode($string_parts['1']);
                endif;
            else:
                $_REQUEST['page'] = 'courses';
            endif;
            break;


        case 'event':
            if (count($string_parts) == 2):
                $object = get_object_by_col_urlrewrite('event', 'urlname', urldecode($string_parts['1']));
                if(is_object($object)):
                    $_GET['id'] = $object->id;
                else:
                     $_GET['id'] = 0;
                endif;
                $_REQUEST['page'] = 'event';
            else:
                $_REQUEST['page'] = 'event';
            endif;
            break;


        case 'faculty':
            if (count($string_parts) == 4):
                $object = get_object_by_col_urlrewrite('dropdown_values', 'title', urldecode($string_parts['2']));
                $object1 = get_object_by_col_urlrewrite('team', 'urlname', urldecode($string_parts['3']));
                $_REQUEST['page'] = 'faculty';
                $_GET['depid'] = urldecode($string_parts['2']);
                $_GET['id'] = urldecode($string_parts['3']);
                if (is_object($object) && is_object($object1)):
                    $_GET['depid'] = $object->id;
                    $_GET['id']=$object1->id;
                else:
                    $_GET['depid'] = 0;
                    $_GET['id']= 0;
                endif;
                $_REQUEST['page'] = 'faculty';
            elseif (count($string_parts) == 3):
                $object = get_object_by_col_urlrewrite('dropdown_values', 'title', urldecode($string_parts['2']));
                $_REQUEST['page'] = 'faculty';
                $_GET['depid'] = urldecode($string_parts['2']);
                if (is_object($object)):
                    $_GET['depid'] = $object->id;
                else:
                    $_GET['depid'] = 0;
                endif;
                $_REQUEST['page'] = 'faculty';
            elseif (count($string_parts) == 2):
                $object = get_object_by_col_urlrewrite('team', 'urlname', urldecode($string_parts['1']));
                if(is_object($object)):
                    $_GET['id'] = $object->id;
                else:
                     $_GET['id'] = 0;
                endif;
                $_REQUEST['page'] = 'faculty';
            else:
                $_REQUEST['page'] = 'faculty';
            endif;
            break;


            case 'jobs':
            if (count($string_parts) == 2):
                $object = get_object_by_col_urlrewrite('jobposting', 'urlname', urldecode($string_parts['1']));
                if(is_object($object)):
                    $_GET['id'] = $object->id;
                else:
                     $_GET['id'] = 0;
                endif;
                $_REQUEST['page'] = 'jobs';
            else:
                $_REQUEST['page'] = 'jobs';
            endif;
            break;

            case 'location':
            $_REQUEST['page'] = 'location';
            break;

            case 'notice':
            if (count($string_parts) == 2):
                $object = get_object_by_col_urlrewrite('notice_board', 'urlname', urldecode($string_parts['1']));
                if(is_object($object)):
                    $_GET['id'] = $object->id;
                else:
                     $_GET['id'] = 0;
                endif;
                $_REQUEST['page'] = 'notice';
            else:
                $_REQUEST['page'] = 'notice';
            endif;
            break;
			
            case 'notice_detail':
				if (count($string_parts) == 2):
					$object = get_object_by_col_urlrewrite('notice_board', 'urlname', urldecode($string_parts['1']));
					if(is_object($object)):
						$_GET['id'] = $object->id;
					else:
						 $_GET['id'] = 0;
					endif;
					$_REQUEST['page'] = 'notice_detail';
				else:
					$_REQUEST['page'] = 'notice_detail';
				endif;
            break;
			
            case 'resources':
            $_REQUEST['page'] = 'resources';
            break;

            case 'schooltiming':
            $_REQUEST['page'] = 'schooltiming';
            break;

            case 'gallery':
            if (count($string_parts) == 2):
                $object = get_object_by_col_urlrewrite('gallery', 'urlname', urldecode($string_parts['1']));
                if(is_object($object)):
                    $_GET['id'] = $object->id;
                else:
                     $_GET['id'] = 0;
                endif;
                $_REQUEST['page'] = 'gallery';
            else:
                $_REQUEST['page'] = 'gallery';
            endif;
            break;

        default:break;
    }
}
?>
