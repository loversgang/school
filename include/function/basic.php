<?

function include_functions($functions) {
    foreach ($functions as $value):
        if (file_exists(DIR_FS_SITE . 'include/function/' . $value . '.php')):
            include_once(DIR_FS_SITE . 'include/function/' . $value . '.php');
        endif;
    endforeach;
}

function __autoload($class_name) {
    include DIR_FS_SITE . 'include/functionClass/' . $class_name . '.php';
}

function display_message($unset = 0) {
    $admin_user = new admin_session();

    if ($admin_user->isset_pass_msg()):
        if (isset($_SESSION['admin_session_secure']['msg_type']) && $_SESSION['admin_session_secure']['msg_type'] == 0):
            $error_type = 'danger alert-error';
        else:
            $error_type = 'success';
        endif;
        foreach ($admin_user->get_pass_msg() as $value):
            echo '<div class="alert alert-' . $error_type . '">';
            echo '<button class="close" data-dismiss="alert"></button><strong>';
            echo $value;
            echo '</strong></div>';
        endforeach;
    endif;
    ($unset) ? $admin_user->unset_pass_msg() : '';
}

function get_var_if_set($array, $var, $preset = '') {
    return isset($array[$var]) ? $array[$var] : $preset;
}

function get_var_set($array, $var, $var1) {
    if (isset($array[$var])):
        return $array[$var];
    else:
        return $var1;
    endif;
}

function get_template($template_name, $array, $selected = '') {
    include_once(DIR_FS_SITE . 'template/' . TEMPLATE . '/' . $template_name . '.php');
}

if (!function_exists('_sanitize')) {

    function _sanitize($value) {
        $prohibited_chars = array(" ", "/", "$", "&", "'", '%', '"', "@");
        foreach ($prohibited_chars as $k => $v):
            $value = str_replace($v, '-', $value);
        endforeach;
        return strtolower($value);
    }

}

function get_meta($page) {
    $page = trim($page);
    if ($page != ''):
        $query = new query('keywords');
        $query->Where = "where page_name='$page'";
        if ($content = $query->DisplayOne()):
            return $content;
        else:
            return null;
        endif;
    endif;
    return null;
}

function head($content = '') {
    /* include all the header related things here... like... common meta tags/javascript files etc. */
    global $page;
    global $content;
    if (is_object($content)) {
        ?>
        <title><?php echo isset($content->name) && $content->name ? $content->name : ''; ?></title>                    
        <meta name='og:site_name' content='<?php echo defined('SITE_NAME') ? SITE_NAME : '' ?>'>
        <meta name='og:email' content='<?php echo defined('SUPPORT_EMAIL') ? SUPPORT_EMAIL : '' ?>'>
        <meta name='og:phone_number' content='<?php echo defined('PHONE_NUMBER') ? PHONE_NUMBER : '' . ', ' . defined('MOBILE_NO') ? MOBILE_NO : ''; ?>'>
        <meta name='og:fax_number' content='<?php echo defined('FAX_NO') ? FAX_NO : '' ?>'>
        <meta name='og:street-address' content='<?php echo defined('ADDRESS1') ? ADDRESS1 : '' ?>'>
        <meta name='og:postal-code' content='<?php echo defined('ZIP_CODE') ? ZIP_CODE : '' ?>'>
        <meta name='og:country-name' content='<?php echo defined('COUNTRY_NAME') ? COUNTRY_NAME : '' ?>'>
        <meta name="keywords" content="<?php echo isset($content->meta_keyword) ? $content->meta_keyword : ''; ?>" />
        <meta name="description" content="<?php echo isset($content->meta_description) ? $content->meta_description : ''; ?>" />
    <?php } ?>

    <link rel="shortcut icon" href="<?php echo DIR_WS_SITE_GRAPHIC ?>img/favicon.ico" />
    <?php include_once(DIR_FS_SITE . 'include/template/stats/google_analytics.php'); ?>
    <?
}

function css($array = array('reset', 'master')) {
    foreach ($array as $k => $v):
        if ($v == 'style' && isset($_SESSION['use_stylesheet'])):
            echo '<link href="' . THEME_PATH . '/css/' . $_SESSION['use_stylesheet'] . '.css" rel="stylesheet" type="text/css" media="screen, projection" >' . "\n";
        else:
            echo '<link href="' . THEME_PATH . '/css/' . $v . '.css" rel="stylesheet" type="text/css" media="screen, projection" >' . "\n";
        endif;
    endforeach;
}

function js($array = array('jquery-1.2.6.min', 'search-reset')) {
    foreach ($array as $k => $v):
        echo '<script src="' . THEME_PATH . '/javascript/' . $v . '.js" type="text/javascript"></script> ' . "\n";
    endforeach;
}

/*
  function css($array=array('reset','master')){
  foreach ($array as $k=>$css):

  echo '<link href="'.make_url('css','css='.$css.'&version='.CSS_VERSION).'" rel="stylesheet" type="text/css" media="screen, projection" >'."\n";

  endforeach;

  }




  function js($array=array('jquery-1.2.6.min','search-reset')){
  foreach ($array as $k=>$js):
  echo '<script src="'.make_url('js','js='.$js.'&version='.JS_VERSION).'" type="text/javascript"></script> '."\n";
  endforeach;
  } */

function get_image($image) {

    echo DIR_WS_SITE . 'graphic.php?image=' . $image;
}

function body() {
    /* # include all the body related things like... tracking code here. */
}

function footer() {
    /* # if you need to add something to the website footer... please add here. */
}

function array_map_recursive($callback, $array) {
    $b = Array();
    foreach ($array as $key => $value) {
        $b[$key] = is_array($value) ? array_map_recursive($callback, $value) : call_user_func($callback, $value);
    }
    return $b;
}

function if_set_in_post_then_display($var) {
    if (isset($_POST[$var])):
        echo $_POST[$var];
    endif;
    echo '';
}

function validate_captcha() {
    global $privatekey;

    if ($_POST["recaptcha_response_field"]) {
        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
        if ($resp->is_valid) {
            return true;
        } else {
            /* # set the error code so that we can display it */
            return false;
        }
    }
}

function is_set($array = array(), $item, $default = 1) {
    if (isset($array[$item]) && $array[$item] != 0) {
        return $array[$item];
    } else {
        return $default;
    }
}

function limit_text($text, $limit = 100) {
    if (strlen($text) > $limit):
        return substr($text, 0, strpos($text, ' ', $limit));
    else:
        return $text;
    endif;
}

function get_object($tablename, $id, $type = 'object') {
    $query = new query($tablename);
    $query->Where = "WHERE id='" . mysql_real_escape_string($id) . "'";
    $data = $query->DisplayOne($type);
    return is_object($data) ? $data : '';
}

function get_object_by_col($tablename, $col, $col_value, $type = 'object') {
    $query = new query($tablename);
    $query->Where = "where $col='" . mysql_real_escape_string($col_value) . "'";
    return $query->DisplayOne($type);
}

function get_object_by_col_urlrewrite($tablename, $col, $col_value, $type = 'object') {
    $query = new query($tablename);
    $query->Where = "WHERE $col='" . mysql_real_escape_string($col_value) . "' AND is_active='1' AND is_deleted='0'";
    return $query->DisplayOne($type);
}

function get_object_var($tablename, $id, $var) {
    $q = new query($tablename);
    $q->Field = "$var";
    $q->Where = "where id='" . mysql_real_escape_string($id) . "'";
    if ($obj = $q->DisplayOne()):
        return $obj->$var;
    else:
        return false;
    endif;
}

function echo_y_or_n($status) {
    echo ($status) ? 'Yes' : 'No';
}

function target_dropdown($name, $selected = '', $tabindex = 1) {
    $values = array('new window' => '_blank', 'same window' => '_parent');
    echo '<select name="' . $name . '" size="1" tabindex="' . $tabindex . '">';
    foreach ($values as $k => $v):
        if ($v == $selected):
            echo '<option value="' . $v . '" selected>' . ucfirst($k) . '</option>';
        else:
            echo '<option value="' . $v . '">' . ucfirst($k) . '</option>';
        endif;
    endforeach;
    echo '</select>';
}

function make_csv_from_array($array) {
    $sr = 1;
    $heading = '';
    $file = '';
    foreach ($array as $k => $v):
        foreach ($v as $key => $value):
            if ($sr == 1):$heading.=$key . ', ';
            endif;
            $file.=str_replace("\r\n", "<<>>", str_replace(",", ".", html_entity_decode($value))) . ', ';
        endforeach;
        $file = substr($file, 0, strlen($file) - 2);
        $file.="\n";
        $sr++;
    endforeach;
    return $file = $heading . "\n" . $file;
}

function get_y_n_drop_down($name, $selected) {
    echo '<select class="span6 m-wrap" name="' . $name . '">';
    if ($selected):
        echo '<option value="1" selected>Yes</option>';
        echo '<option value="0">No</option>';
    else:
        echo '<option value="0" selected>No</option>';
        echo '<option value="1">Yes</option>';
    endif;
    echo '</select>';
}

function get_setting_control($key, $type, $value) {
    switch ($type) {
        case 'text':
            echo '<input class="span12 m-wrap" type="text" name="key[' . $key . ']" value="' . $value . '">';
            break;
        case 'textarea':
            echo '<textarea class="span12 m-wrap" name="key[' . $key . ']" rows="8" >' . $value . '</textarea>';
            break;
        case 'select':
            echo get_y_n_drop_down('key[' . $key . ']', $value);
            break;
        default: echo get_y_n_drop_down('key[' . $key . ']', $value);
    }
}

function css_active($page, $value, $class) {
    if ($page == $value)
        echo 'class=' . $class;
}

function parse_into_array($string) {
    return explode(',', $string);
}

function MakeDataArray($post, $not) {
    $data = array();
    foreach ($post as $key => $value):
        if (!in_array($key, $not)):
            $data[$key] = $value;
        endif;
    endforeach;
    return $data;
}

function is_var_set_in_post($var, $check_value = false) {
    if (isset($_POST[$var])):
        if ($check_value):
            if ($_POST[$var] === $check_value):
                return true;
            else:
                return false;
            endif;
        endif;
        return true;
    else:
        return false;
    endif;
}

function display_form_error() {
    $login_session = new user_session();
    if ($login_session->isset_pass_msg()):
        $array = $login_session->get_pass_msg();
        ?>
        <div class="errorMsg">
            <?php
            foreach ($array as $value):
                echo $value . '<br/>';
            endforeach;
            ?>
        </div>
    <?php elseif (isset($_GET['msg']) && $_GET['msg'] != ''):
        ?>
        <div class="errorMsg">
            <?php echo urldecode($_GET['msg']) . '<br/>'; ?>
        </div>
        <?php
    endif;
    $login_session->isset_pass_msg() ? $login_session->unset_pass_msg() : '';
}

function Redirect($URL) {
    header("location:$URL");
    exit;
}

function Redirect1($filename) {
    if (!headers_sent())
        header('Location: ' . $filename);
    else {
        echo '<script type="text/javascript">';
        echo 'window.location.href="' . $filename . '";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url=' . $filename . '" />';
        echo '</noscript>';
    }
}

function re_direct($URL) {
    header("location:$URL");
    exit;
}

//	function make_url($page, $query=null)
//	{
//		return DIR_WS_SITE.'?page='.$page.'&'.$query;
//	}

function make_url_user($page, $query = null) {
    return DIR_WS_SITE_USER . '?page=' . $page . '&' . $query;
}

function display_url_user($title, $page, $query = '', $class = '') {
    return '<a href="' . make_url_user($page, $query) . '" class="' . $class . '">' . $title . '</a>';
}

function display_url($title, $page, $query = '', $class = '') {
    return '<a href="' . make_url($page, $query) . '" class="' . $class . '">' . $title . '</a>';
}

function make_admin_url($page, $action = 'list', $section = 'list', $query = '') {
    return DIR_WS_SITE . ADMIN_PUBLIC_FOLDER . '/control.php?Page=' . $page . '&action=' . $action . '&section=' . $section . '&' . $query;
}

function display_admin_url($title, $page, $action = 'list', $section = 'list', $query = '', $class = '') {
    return '<a href="' . make_admin_url($page, $action, $section, $query) . '" class="' . $class . '">' . $title . '</a>';
}

function make_admin_url_window($page, $action = 'list', $section = 'list', $query = '') {
    return DIR_WS_SITE . ADMIN_PUBLIC_FOLDER . '/control_window.php?Page=' . $page . '&action=' . $action . '&section=' . $section . '&' . $query;
}

function admin_css_js($page, $query = '') {

    return DIR_WS_SITE . ADMIN_PUBLIC_FOLDER . '/control.php?Page=' . $page . '&' . $query;
}

function make_admin_url2($page, $action = 'list', $section = 'list', $query = '') {
    if ($page == 'home'):
        return DIR_WS_SITE . 'index.php';
    else:
        return DIR_WS_SITE . ADMIN_PUBLIC_FOLDER . '/control_window.php?Page=' . $page . '&action2=' . $action . '&section2=' . $section . '&' . $query;
    endif;
}

function make_admin_link($url, $text, $title = '', $class = '', $alt = '') {
    return '<a href="' . $url . '" class="' . $class . '" title="' . $title . '" alt="' . $alt . '" >' . $text . '</a>';
}

function quit($message = 'processing stopped here') {
    echo $message;
    exit;
}

function download_orders($payment_status, $order_status) {
    $orders = new query('orders');
    $orders->Field = "id,user_id,sub_total,vat,shipping,voucher_code,voucher_amount,shipping_firstname,shipping_lastname,shipping_address1,shipping_address2,shipping_city,shipping_state,shipping_zip,shipping_country,shipping_phone,shipping_fax,billing_firstname,billing_lastname,billing_email,billing_address1,billing_address2,billing_city,billing_state,billing_zip,billing_country,billing_phone,billing_fax,grand_total,order_type,order_status,order_date,ip_address,order_comment,shipping_comment,cart_id";
    if ($order_status == 'paid'):
        $orders->Where = "where payment_status=" . $payment_status . " and order_status!='delivered'";
    elseif ($order_status == 'attempted'):
        $orders->Where = "where payment_status=" . $payment_status . " and order_status='received'";
    else:
        $orders->Where = "where payment_status=" . $payment_status . " and order_status='delivered'";
    endif;
    $orders->DisplayAll();

    $orders_arr = array();
    if ($orders->GetNumRows()):
        while ($order = $orders->GetArrayFromRecord()):
            $order['Username'] = get_username_by_orders($order['user_id']);
            array_push($orders_arr, $order);
        endwhile;
    endif;
    $file = make_csv_from_array($orders_arr);
    $filename = "orders" . '.csv';
    $fh = @fopen('download/' . $filename, "w");
    fwrite($fh, $file);
    fclose($fh);
    download_file('download/' . $filename);
}

function download_orders_by_criteria($from_date, $to_date, $payment_status, $order_status) {
    $orders = new query('orders');
    $orders->Field = "id,user_id,sub_total,vat,shipping,voucher_code,voucher_amount,shipping_firstname,shipping_lastname,shipping_address1,shipping_address2,shipping_city,shipping_state,shipping_zip,shipping_country,shipping_phone,shipping_fax,billing_firstname,billing_lastname,billing_email,billing_address1,billing_address2,billing_city,billing_state,billing_zip,billing_country,billing_phone,billing_fax,grand_total,order_type,order_status,order_date,ip_address,order_comment,shipping_comment,cart_id";
    $orders->Where = "where order_status='$order_status' AND payment_status='$payment_status' AND (order_date BETWEEN CAST('$from_date' as DATETIME) AND CAST('$to_date' as DATETIME)) order by order_date";
    $orders->DisplayAll();

    $orders_arr = array();
    if ($orders->GetNumRows()):
        while ($order = $orders->GetArrayFromRecord()):
            $order['Username'] = get_username_by_orders($order['user_id']);
            array_push($orders_arr, $order);
        endwhile;
    endif;
    $file = make_csv_from_array($orders_arr);
    $filename = "orders" . '.csv';
    $fh = @fopen('download/' . $filename, "w");
    fwrite($fh, $file);
    fclose($fh);
    download_file('download/' . $filename);
}

function get_username_by_orders($id) {
    if ($id == 0):
        return 'Guest';
    elseif ($id):
        $q = new query('user');
        $q->Field = "firstname,lastname";
        $q->Where = "where id='" . $id . "'";
        $o = $q->DisplayOne();
        return $o->firstname;
    endif;
}

function get_zones_box($selected = 0) {
    $q = new query('zone');
    $q->DisplayAll();
    echo '<select name="zone" size="1">';
    while ($obj = $q->GetObjectFromRecord()):
        if ($selected = $obj->id):
            echo '<option value="' . $obj->id . '" selected>' . $obj->name . '</option>';
        else:
            echo '<option value="' . $obj->id . '">' . $obj->name . '</option>';
        endif;
    endwhile;
    echo '</select>';
}

function zone_drop_down($zone_id, $id, $s, $z) {
    $query = new query('zone_country');
    $query->Where = "where zone_id=$zone_id";
    $query->DisplayAll();
    $country_list = array();
    $country_name = '';
    while ($object = $query->GetObjectFromRecord()):
        $country_name = get_country_name_by_id($object->country_id);
        $idd = $object->country_id;
        /* $country_list('id'=>$object->id, 'name'=>$country_name)); */
        array_push($country_list, array('name' => $country_name, 'id' => $object->id));
    /* $country_list[$object->id]=$country_name; */
    endwhile;
    $total_list = array();
    foreach ($country_list as $k => $v):
        $total_list[] = $v['name'];
    endforeach;
    array_multisort($total_list, SORT_ASC, $country_list);

    echo '<select name="' . $id . '" size="10" style="width:200px;" multiple>';
    foreach ($country_list as $k => $v):
        if (($z == $zone_id) && $s):
            echo '<option value="' . $v['id'] . '" selected="selected">' . ucfirst($v['name']) . '</option>';
        else:
            echo '<option value="' . $v['id'] . '">' . ucfirst($v['name']) . '</option>';
        endif;
    endforeach;
    echo'</select>';
}

/* Function to add meta tags to content pages */

function add_metatags($title = "", $keyword = "", $description = "") {
    /* description rule */
    $description_length = 150; /* characters */
    if (strlen($description) > $description_length)
        $description = substr($description, 0, $description_length);

    /* title rule */
    $title_length = 100;
    if (strlen($title) > $title_length)
        $title = SITE_NAME . ' | ' . substr($title, 0, $title_length);
    else
        $title = SITE_NAME . ' | ' . $title;

    $content = new stdClass();
    $content->name = ucwords($title);
    $content->meta_keyword = $keyword;
    $content->meta_description = ucfirst($description);

    return $content;
}

/* sanitize funtion */

function sanitize($value) {
    $prohibited_chars = array(" ", "/", "$", "&", "'", '%', '"', "@");
    foreach ($prohibited_chars as $k => $v):
        $value = str_replace($v, '-', $value);
    endforeach;
    return strtolower($value);
}

function category_chain($id, $tablename = 'category') {
    $chain = array();
    while ($id != 0):
        $QueryObj = new query($tablename);
        $QueryObj->Where = "where id='" . $id . "'";
        //$QueryObj->print=1;
        $cat = $QueryObj->DisplayOne();
        $chain[] = make_admin_link(make_admin_url($tablename, 'list', 'list', 'id=' . $cat->parent_id), $cat->name, 'click here to reach this ' . $tablename);
        $id = $cat->parent_id;
    endwhile;
    $cat_chain = '';
    for ($i = count($chain) - 1; $i >= 0; $i--):
        $cat_chain.=$chain[$i] . ' :: ';
    endfor;
    return $cat_chain . ucfirst(get_plural($tablename));
}

function get_plural($noun) {
    switch ($noun) {
        case 'category': return 'Categories';
        case 'gallery': return 'Galleries';
        case 'product': return 'Products';
        default:'Categories';
    }
}

function make_file_name($name, $id) {
    $file_name_parts = explode('.', $name);
    $file_name_parts['0'].=$id;
    $file = $file_name_parts['0'] . '.' . $file_name_parts['1'];
    return $file;
}

function get_all_modules() {
    $query = new query('module');
    $query->DisplayAll();
    $modules = array();
    if ($query->GetNumRows()):
        while ($object = $query->GetObjectFromRecord()):
            $modules[$object->page_name] = $object->display_name;
        endwhile;
    endif;
    return($modules);
}

/* Get all Content Pages */

function get_all_content_pages() {
    $query = new query('content');
    $query->DisplayAll();
    $pages = array();
    if ($query->GetNumRows()):
        while ($object = $query->GetObjectFromRecord()):
            $pages[$object->id] = $object->page_name;
        endwhile;
    endif;
    return($pages);
}

function get_blog_category($category_id = 0) {
    $query = new query('blog_category');
    $query->Where = "where is_active='1' AND is_deleted='0'";
    $query->DisplayAll();
    $options = '';
    while ($faq = $query->GetObjectFromRecord()) {
        if ($category_id && $category_id == $faq->id)
            $options.='<option value="' . $faq->id . '" selected>' . ucwords($faq->name) . '</option>';
        else
            $options.='<option value="' . $faq->id . '">' . ucwords($faq->name) . '</option>';
    }
    return $options;
}

function pagingAdmin($page, $totalPages, $totalRecords, $url, $action = 'list', $section = 'list', $querystring = '', $type = 1, $Class = 'pad', $tdclass = '', $Title = 'Records', $LClass = 'cat') {

    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 3):
        if (($page + 3) <= $totalPages):
            $end = $page + 3;
            $begin = $page;
        else:
            $begin = $totalPages - 3;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>

    <div class="row-fluid">
        <div class="span12">
            <div class="pagination pull-right">
                <ul>
                    <li>  <a href="<?php echo $page == 1 ? "" : make_admin_url($url, $action, $section, 'p=' . $Pp . '&' . $querystring) ?>" title="Previous Page"><?php echo "&larr; Prev" ?></a></li>
                    <?
                    for ($i = $begin; $i <= $totalPages && $i <= $end; $i++):
                        if ($i == $page):
                            ?>
                            <li> <?php echo display_admin_url($i, $url, $action, $section, 'p=' . $i . '&' . $querystring, 'blockselected'); ?></li>
                        <?php else: ?>
                            <li><?php echo display_admin_url($i, $url, $action, $section, 'p=' . $i . '&' . $querystring, $LClass); ?></li>
                        <?php
                        endif;
                    endfor;
                    ?>
                    <li><a href="<?php echo $totalPages == $page ? "" : make_admin_url($url, $action, $section, 'p=' . $Np . '&' . $querystring) ?>"  title="Next Page"  >&nbsp;<?php echo "Next &rarr;" ?></a></li>
                </ul>
            </div>
        </div>
    </div>


    <?
}

function paging($page, $totalPages, $totalRecords, $url, $querystring = '', $type = 1, $Class = 'pad', $tdclass = '', $Title = 'Records', $LClass = 'cat') {

    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 3):
        if (($page + 3) <= $totalPages):
            $end = $page + 3;
            $begin = $page;
        else:
            $begin = $totalPages - 3;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>

    <div class="row-fluid">
        <div class="span12">
            <div class="pagination pull-right">
                <ul>
                    <li>  <a href="<?php echo $page == 1 ? "" : make_url_user($url, 'p=' . $Pp . '&' . $querystring) ?>" title="Previous Page"><?php echo "&larr; Prev" ?></a></li>
                    <?
                    for ($i = $begin; $i <= $totalPages && $i <= $end; $i++):
                        if ($i == $page):
                            ?>
                            <li> <?php echo display_url_user($i, $url, 'p=' . $i . '&' . $querystring, 'blockselected'); ?></li>
                        <?php else: ?>
                            <li><?php echo display_url_user($i, $url, 'p=' . $i . '&' . $querystring, $LClass); ?></li>
                        <?php
                        endif;
                    endfor;
                    ?>
                    <li><a href="<?php echo $totalPages == $page ? "" : make_url_user($url, 'p=' . $Np . '&' . $querystring) ?>"  title="Next Page"  >&nbsp;<?php echo "Next &rarr;" ?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <?
}

function get_charcter_value($value) {
    $charchter = array('A' => '1', 'B' => '2', 'C' => '3', 'D' => '4', 'E' => '5', 'F' => '6', 'G' => '7', 'H' => '8', 'I' => '9', 'J' => '10', 'K' => '11', 'L' => '12');
    if (array_key_exists($value, $charchter)):
        return $charchter[$value];
    endif;
    return false;
}

function select_month($selected = '') {
    $month = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');

    for ($i = 1; $i < 13; $i++):
        if ($i == $selected):
            echo '<option value="' . $i . '" selected>' . $month[$i] . '</option>';
        else:
            echo '<option value="' . $i . '">' . $month[$i] . '</option>';
        endif;
    endfor;
}

function select_date($selected = '') {

    for ($i = 1; $i <= 31; $i++):
        if ($i == $selected):
            echo '<option value="' . $i . '" selected>' . $i . '</option>';
        else:
            echo '<option value="' . $i . '">' . $i . '</option>';
        endif;
    endfor;
}

function select_year($selected = '') {
    $end = date(Y) + 2;
    $start = '2008';
    while ($end >= $start):
        if ($end == $selected):
            echo '<option value="' . $end . '" selected>' . $end . '</option>';
        else:
            echo '<option value="' . $end . '">' . $end . '</option>';
        endif;
        $end--;
    endwhile;
}

function select_year_current($selected = '') {
    $end = date(Y);
    $start = '2010';
    while ($end >= $start):
        if ($end == $selected):
            echo '<option value="' . $end . '" selected>' . $end . '</option>';
        else:
            echo '<option value="' . $end . '">' . $end . '</option>';
        endif;
        $end--;
    endwhile;
}

function getMonths($date1, $date2, $selected = '') {
    $time1 = strtotime($date1);
    $time2 = strtotime($date2);
    $my = date('mY', $time2);

    $months = array(date('F Y', $time1));

    while ($time1 < $time2) {
        $time1 = strtotime(date('Y-m-d', $time1) . ' +1 days');

        $months[] = date('F Y', $time1);
    }

    $months[] = date('F Y', $time2);
    $months = array_unique($months);

    foreach ($months as $key => $val):
        if ($val == date('F Y')):
            echo '<option value="' . date('m-Y', strtotime($val)) . '" selected>' . $val . '</option>';
        else:
            echo '<option value="' . date('m-Y', strtotime($val)) . '">' . $val . '</option>';
        endif;
    endforeach;
}

function getMonthsOnly($date1, $date2, $selected = '') {
    $time1 = strtotime($date1);
    $time2 = strtotime($date2);
    $my = date('mY', $time2);

    $months = array(date('F Y', $time1));

    while ($time1 < $time2) {
        $time1 = strtotime(date('Y-m-d', $time1) . ' +1 days');

        $months[] = date('F Y', $time1);
    }

    $months[] = date('F Y', $time2);
    $months = array_unique($months);

    foreach ($months as $key => $val):
        if (!empty($selected) && date('m-Y', strtotime($val)) == $selected):
            echo '<option value="' . date('m-Y', strtotime($val)) . '" selected>' . $val . '</option>';
        else:
            echo '<option value="' . date('m-Y', strtotime($val)) . '">' . $val . '</option>';
        endif;
    endforeach;
}

function getMonthsArray($date1, $date2) {
    $time1 = strtotime($date1);
    $time2 = strtotime($date2);
    $my = date('mY', $time2);

    $months = array(date('Y-m', $time1));

    while ($time1 < $time2) {
        $time1 = strtotime(date('Y-m-d', $time1) . ' +1 days');

        $months[] = date('Y-m', $time1);
    }

    $months[] = date('Y-m', $time2);
    $months = array_unique($months);

    return $months;
}

function get_previous_year($total = '20') {
    $year = date("Y");

    /* echo '<option value="">Select Year'.'&nbsp&nbsp>'.'</option>'; */
    echo '<option value="' . ($year) . '">Select Year</option>';

    for ($i = 0; $i <= $total; $i++):

        echo '<option value="' . ($year - $i) . '">' . ($year - $i) . '</option>';

    endfor;
}

function getTotalSunday($start, $end) {
    $current = $start;
    $count = 0;

    while ($current != $end) {

        if (date('l', strtotime($current)) == 'Sunday') {
            $count++;
        }
        $current = date('Y-m-d', strtotime($current . ' +1 day'));
    };
    return $count;
}

function CheckSunday($date) {

    if (date('l', strtotime($date)) == 'Sunday'):
        return 'Sunday';
    else:
        return false;
    endif;
}

function getTotalDays($start, $end) {
    $current = $start;
    $count = 0;

    while ($current != $end) {
        $count++;
        $current = date('Y-m-d', strtotime($current . ' +1 day'));
    };
    return $count;
}

function get_database_msg($id, $array = array()) {
    $object = get_object('email', $id);
    $content = strip_tags($object->email_text);
    if (is_object($object)):
        if (count($array)):
            foreach ($array as $k => $v):
                $literal = '{' . trim(strtoupper($k)) . '}';
                $content = html_entity_decode(str_replace($literal, $v, $content));
            endforeach;
        endif;
    endif;
    return $content;
}

function get_database_msg_only($id, $array = array()) {
    $object = get_object('email', $id);
    $content = html_entity_decode($object->email_text);

    if (is_object($object)):
        if (count($array)):
            foreach ($array as $k => $v):
                $literal = '{' . trim(strtoupper($k)) . '}';
                $content = str_replace($literal, $v, $content);
            endforeach;
        endif;
    endif;

    return urlencode($content);
}

function get_database_msg_subject($id, $array = array()) {
    $object = get_object('email', $id);
    $content = strip_tags($object->email_subject);
    if (is_object($object)):
        if (count($array)):
            foreach ($array as $k => $v):
                $literal = '{' . trim(strtoupper($k)) . '}';
                $content = html_entity_decode(str_replace($literal, $v, $content));
            endforeach;
        endif;
    endif;

    return $content;
}

function send_sms($to, $msg) {

    $user_name = 'arshsoft';
    $password = 'qwerty123';
    $recipient = $to;
    $sender_id = 'WEBSMS';

    $post_url = "https://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=" . $user_name . "&pwd=" . $password . "&to=" . $recipient . "&sid=" . $sender_id . "&msg=" . $msg . "&fl=0&gwid=2";
    $result = @file_get_contents($post_url);

    $pos = strpos($result, '-');

    if ($pos):
        return '1';
    else:
        return '';
    endif;

    /*
      $user_name='rocky.developer002@gmail.com';
      $password='developer002';
      $recipient="+48526";
      $sender_id='16110';
      $call_back='5465454645564';
      #by smsgatewayhub.com
      $post_url = "http://login.smsgatewayhub.com/API/WebSMS/Http/v1.0a/index.php?username=".$user_name."&password=".$password."&sender=".$sender_id."&to=".$recipient."&message=".nl2br($msg)."&reqid=1&format=text&route_id=route+id&callback=".$call_back."&unique=0&sendondate=".date('d m Y H:m');

      $ch = curl_init();
      $timeout = 5; // set to zero for no timeout
      curl_setopt ($ch, CURLOPT_URL, $post_url);
      curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

      ob_start();
      curl_exec($ch);
      curl_close($ch);
      $file_contents = ob_get_contents();
     */
}

function make_new_entry($table, $school_id, $student_id, $type) {
    $new_query = new query($table);
    $new_query->Data['school_id'] = $school_id;
    $new_query->Data['type'] = $type;
    $new_query->Data['student_id'] = $student_id;
    $new_query->Data['on_date'] = date('Y-m-d H:m:t');
    $new_query->Insert();
    return true;
}

function get_school_reg_prefix($name = '', $school_id) {
    $name = str_replace(' ', '', $name);
    $name = strtoupper($name);
    $found = '1';
    $sr = '3';
    if (strlen(trim($name)) < 3):
        $no = rand(1, 9);
        $name = $name . $no;
    endif;

    while ($found = 1):
        if (strlen(trim($name)) < $sr):
            $no = rand(1, 9);
            $name = $name . $no;
        endif;
        $QueryObj = new query('school_setting');
        $QueryObj->Where = "where reg_id_prefix='" . substr($name, 0, $sr) . "' and school_id!='" . $school_id . "' ORDER BY id asc";
        $rec = $QueryObj->DisplayOne();
        if (is_object($rec)):
            $found = '1';
            $sr++;
        else:
            $found = '0';
            return substr($name, 0, $sr);
        endif;
    endwhile;
}

function make_session_name($date1, $date2) {
    $name = date('Y', strtotime($date1)) . "-" . date('y', strtotime($date2));
    return $name;
}

function get_difference_in_months($date1, $date2) {
    $ts1 = strtotime($date1);
    $ts2 = strtotime($date2);

    $year1 = date('Y', $ts1);
    $year2 = date('Y', $ts2);

    $month1 = date('m', $ts1);
    $month2 = date('m', $ts2);
    $month1 = $month1 - 1;

    return $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
}

function encrypt_decrypt($action, $string) {
    $output = false;

    $key = 'SchoolManagementSystem';

    // initialization vector 
    $iv = md5(md5($key));

    if ($action == 'encrypt') {
        $output = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, $iv);
        $output = rtrim($output, "");
    }
    return $output;
}

function number_to_word($num = '') {
    $num = (string) ( (int) $num );

    if ((int) ( $num ) && ctype_digit($num)) {
        $words = array();

        $num = str_replace(array(',', ' '), '', trim($num));

        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven',
            'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
            'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen');

        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty',
            'seventy', 'eighty', 'ninety', 'hundred');

        $list3 = array('', 'thousand', 'million', 'billion', 'trillion',
            'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion',
            'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion',
            'octodecillion', 'novemdecillion', 'vigintillion');

        $num_length = strlen($num);
        $levels = (int) ( ( $num_length + 2 ) / 3 );
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);

        foreach ($num_levels as $num_part) {
            $levels--;
            $hundreds = (int) ( $num_part / 100 );
            $hundreds = ( $hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ( $hundreds == 1 ? '' : '' ) . ' ' : '' );
            $tens = (int) ( $num_part % 100 );
            $singles = '';

            if ($tens < 20) {
                $tens = ( $tens ? ' ' . $list1[$tens] . ' ' : '' );
            } else {
                $tens = (int) ( $tens / 10 );
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ( $num_part % 10 );
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . ( ( $levels && (int) ( $num_part ) ) ? ' ' . $list3[$levels] . ' ' : '' );
        }

        $commas = count($words);

        if ($commas > 1) {
            $commas = $commas - 1;
        }

        $words = implode(', ', $words);

        //Some Finishing Touch
        //Replacing multiples of spaces with one space
        $words = trim(str_replace(' ,', ',', trim_all(ucwords($words))), ', ');
        if ($commas) {
            $words = str_replace_last(',', ' and', $words);
        }

        return $words . ' only.';
    } else if (!( (int) $num )) {
        return 'Zero';
    }
    return '';
}

function trim_all($str, $what = NULL, $with = ' ') {
    if ($what === NULL) {
        //  Character      Decimal      Use
        //  "\0"            0           Null Character
        //  "\t"            9           Tab
        //  "\n"           10           New line
        //  "\x0B"         11           Vertical Tab
        //  "\r"           13           New Line in Mac
        //  " "            32           Space

        $what = "\\x00-\\x20";    //all white-spaces and control chars
    }

    return trim(preg_replace("/[" . $what . "]+/", $with, $str), $what);
}

function str_replace_last($search, $replace, $str) {
    if (( $pos = strrpos($str, $search) ) !== false) {
        $search_length = strlen($search);
        $str = substr_replace($str, $replace, $pos, $search_length);
    }
    return $str;
}

function get_possible_section($value) {
    $section = array();
    $charchter = array('A' => '1', 'B' => '2', 'C' => '3', 'D' => '4', 'E' => '5', 'F' => '6', 'G' => '7', 'H' => '8', 'I' => '9', 'J' => '10', 'K' => '11', 'L' => '12');
    $start = '0';
    foreach ($charchter as $key => $val):
        $section[] = $key;
        $start++;
        if ($start == $value):
            break;
        endif;
    endforeach;

    return $section;
}

function get_database_quick_sms($id, $array = array()) {
    $object = get_object('quicksms', $id);
    $content = html_entity_decode($object->sms_text);

    if (is_object($object)):
        if (count($array)):
            foreach ($array as $k => $v):
                $literal = '{' . trim(strtoupper($k)) . '}';
                $content = str_replace($literal, $v, $content);
            endforeach;
        endif;
    endif;

    return urlencode($content);
}

function pr($e) {
    echo "<pre>";
    print_r($e);
    echo "</pre>";
}

function maybe_unserialize($original) {
    if (is_serialized($original))
        return @unserialize($original);
    return $original;
}

function maybe_serialize($data) {
    if (is_array($data) || is_object($data))
        return serialize($data);
    if (is_serialized($data, false))
        return serialize($data);
    return $data;
}

function is_serialized($data, $strict = true) {
    if (!is_string($data)) {
        return false;
    }
    $data = trim($data);
    if ('N;' == $data) {
        return true;
    }
    if (strlen($data) < 4) {
        return false;
    }
    if (':' !== $data[1]) {
        return false;
    }
    if ($strict) {
        $lastc = substr($data, -1);
        if (';' !== $lastc && '}' !== $lastc) {
            return false;
        }
    } else {
        $semicolon = strpos($data, ';');
        $brace = strpos($data, '}');
        if (false === $semicolon && false === $brace)
            return false;
        if (false !== $semicolon && $semicolon < 3)
            return false;
        if (false !== $brace && $brace < 4)
            return false;
    }
    $token = $data[0];
    switch ($token) {
        case 's' :
            if ($strict) {
                if ('"' !== substr($data, -2, 1)) {
                    return false;
                }
            } elseif (false === strpos($data, '"')) {
                return false;
            }
        case 'a' :
        case 'O' :
            return (bool) preg_match("/^{$token}:[0-9]+:/s", $data);
        case 'b' :
        case 'i' :
        case 'd' :
            $end = $strict ? '$' : '';
            return (bool) preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
    }
    return false;
}
?>