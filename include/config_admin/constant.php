<?php
# WEBSITE CONSTANTS
$constants=new query('setting');
$constants->DisplayAll();
while($constant=$constants->GetObjectFromRecord()):
	define("$constant->key", $constant->value, true);
endwhile;

# PHP Validation types
define('VALIDATE_REQUIRED', "req", true);
define('VALIDATE_EMAIL',"email", true);
define("VALIDATE_MAX_LENGTH","maxlength");
define("VALIDATE_MIN_LENGTH","minlength");
define("VALIDATE_NUMERIC","num");
define("VALIDATE_ALPHA","alpha");
define("VALIDATE_ALPHANUM","alphanum");

define("TEMPLATE","default");
define("CURRENCY_SYMBOL", "<i class='icon-inr'></i> ");

define("ADMIN_FOLDER",'control');
define("ADMIN_PUBLIC_FOLDER",'admin');
define("PUBLIC_FOLDER",'public');

define("ADD_ATTRIBUTE_PRICE_TO_PRODUCT_PRICE", true);
define("ATTRIBUTE_PRICE_OVERLAP", false);

define('VERIFY_EMAIL_ON_REGISTER', true);
define('ERROR_EMAIL', 'rocky.developer004@gmail.com', true);

$conf_shipping_type=array('quantity', 'subtotal');

define('SHIPPING_TYPE', 'Quantity');

define('CSS_VERSION', "1.4", true);
define('JS_VERSION',"1.5", true);

$AllowedImageTypes=array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$AllowedFileTypes=array('application/vnd.ms-excel');

# new allowed photo mime type array.
$conf_allowed_file_mime_type=array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','applications/vnd.pdf','application/pdf');
$conf_allowed_import_file_mime_type=array('application/vnd.ms-excel', 'application/msexcel');
$conf_allowed_photo_mime_type=array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$conf_allowed_audio_mime_type= array('audio/mpeg','audio/mpeg','audio/mpg','audio/x-mpeg','audio/mp3','application/force-download','application/octet-stream');

$conf_order_status=array('Received', 'Processing', 'Shipped', 'Delivered');

define("DOC_TYPE", '<!DOCTYPE html>', true);
define("DOC_LANGUAGE", "en", true);
define("DOC_CHAR_SET", 'utf-8', true);

/* set soft delete status */
define("SOFT_DELETE", 1, true);

/* set pages for afilate User */
define("ADMINISTRATOR_PAGES", '*', true);

/* set pages for afilate User */
define("AFFILIATE_PAGES", 'home@@school@@admin@@logout@@status_off@@status_on@@error', true);

/* set pages for Payment User */
define("PAYMENT_PAGES", 'home@@school@@logout@@error', true);

$pre_defined_pages=array(
        'about'=>'About Us',
        'infrastructure'=>'Infrastructure',
        'awards'=>'Awards',
		'info'=>'School Information',
        'admission'=>'Admission Box'
	);

$conf_rewrite_url=array(
        'achievement'=>'achievement',
        'affiliations'=>'affiliations',
        'contact-us'=>'contact-us',
        'content'=>'content',
        'courses'=>'courses',
        'document'=>'document',
        'event'=>'event',
        'eventcalendar'=>'eventcalendar',
        'faculty'=>'faculty',
        'faqs'=>'faqs',
        'gallery'=>'gallery',
        'home'=>'',
        'jobapply'=>'jobapply',
        'jobs'=>'jobs',
        'location'=>'location',
        'news'=>'news',
        'notice'=>'notice',
        'placement'=>'placement',
        'request-a-prospectus'=>'request-a-prospectus',
        'resources'=>'resources',
        'schooltiming'=>'schooltiming',
        '404'=>'404'
);

$countries = array("AF" => "Afghanistan",
"AX" => "Åland Islands",
"AL" => "Albania",
"DZ" => "Algeria",
"AS" => "American Samoa",
"AD" => "Andorra",
"AO" => "Angola",
"AI" => "Anguilla",
"AQ" => "Antarctica",
"AG" => "Antigua and Barbuda",
"AR" => "Argentina",
"AM" => "Armenia",
"AW" => "Aruba",
"AU" => "Australia",
"AT" => "Austria",
"AZ" => "Azerbaijan",
"BS" => "Bahamas",
"BH" => "Bahrain",
"BD" => "Bangladesh",
"BB" => "Barbados",
"BY" => "Belarus",
"BE" => "Belgium",
"BZ" => "Belize",
"BJ" => "Benin",
"BM" => "Bermuda",
"BT" => "Bhutan",
"BO" => "Bolivia",
"BA" => "Bosnia and Herzegovina",
"BW" => "Botswana",
"BV" => "Bouvet Island",
"BR" => "Brazil",
"IO" => "British Indian Ocean Territory",
"BN" => "Brunei Darussalam",
"BG" => "Bulgaria",
"BF" => "Burkina Faso",
"BI" => "Burundi",
"KH" => "Cambodia",
"CM" => "Cameroon",
"CA" => "Canada",
"CV" => "Cape Verde",
"KY" => "Cayman Islands",
"CF" => "Central African Republic",
"TD" => "Chad",
"CL" => "Chile",
"CN" => "China",
"CX" => "Christmas Island",
"CC" => "Cocos (Keeling) Islands",
"CO" => "Colombia",
"KM" => "Comoros",
"CG" => "Congo",
"CK" => "Cook Islands",
"CR" => "Costa Rica",
"CI" => "Cote D'ivoire",
"HR" => "Croatia",
"CU" => "Cuba",
"CY" => "Cyprus",
"CZ" => "Czech Republic",
"DK" => "Denmark",
"DJ" => "Djibouti",
"DM" => "Dominica",
"DO" => "Dominican Republic",
"EC" => "Ecuador",
"EG" => "Egypt",
"SV" => "El Salvador",
"GQ" => "Equatorial Guinea",
"ER" => "Eritrea",
"EE" => "Estonia",
"ET" => "Ethiopia",
"FK" => "Falkland Islands (Malvinas)",
"FO" => "Faroe Islands",
"FJ" => "Fiji",
"FI" => "Finland",
"FR" => "France",
"GF" => "French Guiana",
"PF" => "French Polynesia",
"TF" => "French Southern Territories",
"GA" => "Gabon",
"GM" => "Gambia",
"GE" => "Georgia",
"DE" => "Germany",
"GH" => "Ghana",
"GI" => "Gibraltar",
"GR" => "Greece",
"GL" => "Greenland",
"GD" => "Grenada",
"GP" => "Guadeloupe",
"GU" => "Guam",
"GT" => "Guatemala",
"GG" => "Guernsey",
"GN" => "Guinea",
"GW" => "Guinea-bissau",
"GY" => "Guyana",
"HT" => "Haiti",
"HM" => "Heard Island",
"VA" => "Holy See",
"HN" => "Honduras",
"HK" => "Hong Kong",
"HU" => "Hungary",
"IS" => "Iceland",
"IN" => "India",
"ID" => "Indonesia",
"IR" => "Iran, Islamic Republic of",
"IQ" => "Iraq",
"IE" => "Ireland",
"IM" => "Isle of Man",
"IL" => "Israel",
"IT" => "Italy",
"JM" => "Jamaica",
"JP" => "Japan",
"JE" => "Jersey",
"JO" => "Jordan",
"KZ" => "Kazakhstan",
"KE" => "Kenya",
"KI" => "Kiribati",
"KP" => "Korea",
"KW" => "Kuwait",
"KG" => "Kyrgyzstan",
"LA" => "Lao People's Democratic Republic",
"LV" => "Latvia",
"LB" => "Lebanon",
"LS" => "Lesotho",
"LR" => "Liberia",
"LY" => "Libyan Arab Jamahiriya",
"LI" => "Liechtenstein",
"LT" => "Lithuania",
"LU" => "Luxembourg",
"MO" => "Macao",
"MK" => "Macedonia",
"MG" => "Madagascar",
"MW" => "Malawi",
"MY" => "Malaysia",
"MV" => "Maldives",
"ML" => "Mali",
"MT" => "Malta",
"MH" => "Marshall Islands",
"MQ" => "Martinique",
"MR" => "Mauritania",
"MU" => "Mauritius",
"YT" => "Mayotte",
"MX" => "Mexico",
"FM" => "Micronesia, Federated States of",
"MD" => "Moldova, Republic of",
"MC" => "Monaco",
"MN" => "Mongolia",
"ME" => "Montenegro",
"MS" => "Montserrat",
"MA" => "Morocco",
"MZ" => "Mozambique",
"MM" => "Myanmar",
"NA" => "Namibia",
"NR" => "Nauru",
"NP" => "Nepal",
"NL" => "Netherlands",
"AN" => "Netherlands Antilles",
"NC" => "New Caledonia",
"NZ" => "New Zealand",
"NI" => "Nicaragua",
"NE" => "Niger",
"NG" => "Nigeria",
"NU" => "Niue",
"NF" => "Norfolk Island",
"MP" => "Northern Mariana Islands",
"NO" => "Norway",
"OM" => "Oman",
"PK" => "Pakistan",
"PW" => "Palau",
"PS" => "Palestinian Territory, Occupied",
"PA" => "Panama",
"PG" => "Papua New Guinea",
"PY" => "Paraguay",
"PE" => "Peru",
"PH" => "Philippines",
"PN" => "Pitcairn",
"PL" => "Poland",
"PT" => "Portugal",
"PR" => "Puerto Rico",
"QA" => "Qatar",
"RE" => "Reunion",
"RO" => "Romania",
"RU" => "Russian Federation",
"RW" => "Rwanda",
"SH" => "Saint Helena",
"KN" => "Saint Kitts and Nevis",
"LC" => "Saint Lucia",
"PM" => "Saint Pierre and Miquelon",
"VC" => "Saint Vincent and The Grenadines",
"WS" => "Samoa",
"SM" => "San Marino",
"ST" => "Sao Tome and Principe",
"SA" => "Saudi Arabia",
"SN" => "Senegal",
"RS" => "Serbia",
"SC" => "Seychelles",
"SL" => "Sierra Leone",
"SG" => "Singapore",
"SK" => "Slovakia",
"SI" => "Slovenia",
"SB" => "Solomon Islands",
"SO" => "Somalia",
"ZA" => "South Africa",
"GS" => "South Georgia",
"ES" => "Spain",
"LK" => "Sri Lanka",
"SD" => "Sudan",
"SR" => "Suriname",
"SJ" => "Svalbard and Jan Mayen",
"SZ" => "Swaziland",
"SE" => "Sweden",
"CH" => "Switzerland",
"SY" => "Syrian Arab Republic",
"TW" => "Taiwan, Province of China",
"TJ" => "Tajikistan",
"TZ" => "Tanzania, United Republic of",
"TH" => "Thailand",
"TL" => "Timor-leste",
"TG" => "Togo",
"TK" => "Tokelau",
"TO" => "Tonga",
"TT" => "Trinidad and Tobago",
"TN" => "Tunisia",
"TR" => "Turkey",
"TM" => "Turkmenistan",
"TC" => "Turks and Caicos Islands",
"TV" => "Tuvalu",
"UG" => "Uganda",
"UA" => "Ukraine",
"AE" => "United Arab Emirates",
"GB" => "United Kingdom",
"US" => "United States",
"UY" => "Uruguay",
"UZ" => "Uzbekistan",
"VU" => "Vanuatu",
"VE" => "Venezuela",
"VN" => "Viet Nam",
"VG" => "Virgin Islands, British",
"VI" => "Virgin Islands, U.S.",
"WF" => "Wallis and Futuna",
"EH" => "Western Sahara",
"YE" => "Yemen",
"ZM" => "Zambia",
"ZW" => "Zimbabwe");

/*Decide the file logo according to extension*/

$file_extension=array(
	'pdf'=>'pdf.jpg',
	'doc'=>'word.png',
        'docx'=>'word.png',
	'txt'=>'file.png',
);


$imageThumbConfig=array(
    'school'=>array(
        'thumb'=>array('width'=>'120', 'height'=>'60','crop'=>'1'),
        'medium'=>array('width'=>'150', 'height'=>'100','crop'=>'1'),
        'big'=>array('width'=>'800', 'height'=>'600'),
    ),

);

 $members_array=array(
                   "Name"=>"name",
                   "Username"=>"username",
                   "ContactPersonName"=>"contact_name",
                   "ContactPersonEmail"=>"contact_email",
                   "ContactPersonPhone"=>"contact_phone",
                   "Link"=>"link",
                   "MailingAddress"=>"mailing_address",
                   "PhysicalAddress"=>"physical_address",
                   "Mobile"=>"mobile",
                   "Phone"=>"phone",
                   "PhoneTollfree"=>"phone_tollfree",
                   "Fax"=>"fax",
                   "Email"=>"email_address",
                   "FacebookPage"=>"facebook",
                   "TwitterPage"=>"twitter",
                   "YoutubePage"=>"youtube"
                   );

 $not_to_open_page = array(
    '1' => 'slider',
    '2' => 'navigation',
    '3' => 'innerfooter'
);

 /*module cache array*/
$module_cache_folder=array(

//'news'=>array('0'=>'news','1'=>'news_item'),
'achievement'=>array('0'=>'achievement'),
'affiliations'=>array('0'=>'affiliations'),
'content'=>array('0'=>'content'),
'courses'=>array('0'=>'courses'),
'document'=>array('0'=>'document'),
'event'=>array('0'=>'event','1'=>'eventcalendar'),
'faculty'=>array('0'=>'faculty'),
'faqs'=>array('0'=>'faqs'),
'gallery'=>array('0'=>'gallery'),
'news'=>array('0'=>'news'),
'jobs'=>array('0'=>'jobs'),
'notice'=>array('0'=>'notice'),
'placement'=>array('0'=>'placement'),
'resources'=>array('0'=>'resources')
)



?>