<?php

/* output buffering started */
ob_start();

/* set error level */
error_reporting(E_ALL);
ini_set('display_errors', 1);

/* start session by default */

@session_start();

/*
 *   Code Developed By: cWebConsultants Team - India (Chandigarh)
 *   Project Name: <content managment system> - cWebConsultants
 *   Dated: <10 Jan, 2012>
 *   *** Copyrighted by cWebConsultants India - We reserve the right to take legal action against anyone using this software without our permission.  ***
 */


/* set website filesystem */

define("DIR_FS_SITE", dirname(dirname(dirname(__FILE__))) . '/', true);



/*
 * Be very carefully while setting these variables 
 * These are used in the URL Rewrite
 */

/*
 * set databse details here 
 */
$DBHostName = "localhost";
$DBDataBase = "school";
$DBUserName = "root";
$DBPassword = "cwebco";

$is_local = false;
$is_qtx = false;
if (strpos($_SERVER['HTTP_HOST'], 'qtx.in') !== false) {
    $is_qtx = true;
} else {
    $is_local = true;
}
if ($is_qtx) {
    define("HTTP_SERVER", "http://school.qtx.in", true);
    $DBPassword = "GtZhBjPDRYA5aDSVm";
} else {
    define("HTTP_SERVER", "http://school", true);
}

define("DIR_WS_SITE", HTTP_SERVER . "/", true);


/*
 *   --- WARNING ---
 *  All the files below are location sensitive. 
 *  Maintain the sequence of files
 *
 */
# include sub-configuration files here.
//ini_set('session.gc_maxlifetime', 1*60);
require_once(DIR_FS_SITE . "include/config_staff/url.php");

# include the database class files.
require_once(DIR_FS_SITE_INCLUDE_CLASS . "mysql.php");
require_once(DIR_FS_SITE_INCLUDE_CLASS . "query.php");


# include the utitlity files here
require_once(DIR_FS_SITE_INCLUDE_CLASS . "phpmailer.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG . "constant.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG . "message.php");

# custom files
//include_once(DIR_FS_SITE_INCLUDE_CLASS.'login_session.php');
include_once(DIR_FS_SITE_INCLUDE_CLASS . 'admin_session.php');


# include function files here.
include_once(DIR_FS_SITE . 'include/function/basic.php');

/* include function classes here */
include_once(DIR_FS_SITE . 'include/functionClass/imageManipulationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/fileClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/dateClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/errorClass.php');

/* include smarty class */
require_once(DIR_FS_SITE . 'include/libs/Smarty.class.php');
require_once(DIR_FS_SITE . 'include/class/smartyClass.php');

date_default_timezone_set('Asia/Calcutta');

# fix for stripslashes issues in php
if (get_magic_quotes_gpc()):
    $_POST = array_map_recursive('stripslashes', $_POST);
    $_GET = array_map_recursive('stripslashes', $_GET);
    $_REQUEST = array_map_recursive('stripslashes', $_REQUEST);
endif;
$publickey = "6LcWw7kSAAAAADZv_vEtAK2oEHvk3XcHuEoThrbN";
$privatekey = "6LcWw7kSAAAAACgQDdDBBIei-rxYTOPlgI02lDG9";

//$files1 = glob(DIR_FS_SITE.'cache/*');// get all file names
//print_r($files1);die;
?>
