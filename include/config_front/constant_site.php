<?php

# WEBSITE CONSTANTS

/* get user from id */
$user_obj = new school();
$school = $user_obj->getAllSchoolDetail($school_id);

/* define all basic constants for user */

define('SITE_NAME', ucfirst($school->school_name), true);
define('ADDRESS1', $school->address1, true);
define('ADDRESS2', $school->address2, true);
define('CITY', $school->city, true);
define('STATE', $school->state, true);
define('COUNTRY', $school->country, true);
?>