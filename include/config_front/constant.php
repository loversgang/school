<?php
# WEBSITE CONSTANTS

$constants=new query('setting');
$constants->DisplayAll();
while($constant=$constants->GetObjectFromRecord()):
	if(($constant->key!='SITE_NAME') && ($constant->key!='ADDRESS1') && $constant->key!='ADDRESS2' && $constant->key!='CITY' && $constant->key!='STATE' && $constant->key!='COUNTRY'):
		define("$constant->key", $constant->value, true);
	endif;	
endwhile;



# PHP Validation types
define('VALIDATE_REQUIRED', "req", true);
define('VALIDATE_EMAIL',"email", true);
define("VALIDATE_MAX_LENGTH","maxlength");
define("VALIDATE_MIN_LENGTH","minlength");
define("VALIDATE_NUMERIC","num");
define("VALIDATE_ALPHA","alpha");
define("VALIDATE_ALPHANUM","alphanum");

define("TEMPLATE","default");
define("CURRENCY_SYMBOL", "<i class='icon-inr'></i> ");

define("ADMIN_FOLDER",'control');
define("ADMIN_PUBLIC_FOLDER",'admin');
define("PUBLIC_FOLDER",'public');

define("ADD_ATTRIBUTE_PRICE_TO_PRODUCT_PRICE", true);
define("ATTRIBUTE_PRICE_OVERLAP", false);

define('VERIFY_EMAIL_ON_REGISTER', true);
define('ERROR_EMAIL', 'rocky.developer004@gmail.com', true);

$conf_shipping_type=array('quantity', 'subtotal');

define('SHIPPING_TYPE', 'Quantity');

define('CSS_VERSION', "1.4", true);
define('JS_VERSION',"1.5", true);

$AllowedImageTypes=array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$AllowedFileTypes=array('application/vnd.ms-excel');

# new allowed photo mime type array.
$conf_allowed_file_mime_type=array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','applications/vnd.pdf','application/pdf');
$conf_allowed_import_file_mime_type=array('application/vnd.ms-excel', 'application/msexcel');
$conf_allowed_photo_mime_type=array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$conf_allowed_audio_mime_type= array('audio/mpeg','audio/mpeg','audio/mpg','audio/x-mpeg','audio/mp3','application/force-download','application/octet-stream');

$conf_order_status=array('Received', 'Processing', 'Shipped', 'Delivered');

define("DOC_TYPE", '<!DOCTYPE html>', true);
define("DOC_LANGUAGE", "en", true);
define("DOC_CHAR_SET", 'utf-8', true);

/* set soft delete status */
define("SOFT_DELETE", 1, true);




$conf_rewrite_url=array(

        'achievement'=>'achievement',
        'affiliations'=>'affiliations',
        'contact'=>'contact',
        'content'=>'content',
        'staff'=>'staff', 
        'gallery'=>'gallery',
        'home'=>'',       
        'notice'=>'notice',
        'notice_detail'=>'notice_detail',
        'placement'=>'placement',        
        '404'=>'404'		
);



/*Decide the file logo according to extension*/

$file_extension=array(
	'pdf'=>'pdf.jpg',
	'doc'=>'word.png',
        'docx'=>'word.png',
	'txt'=>'file.png',
);




 $not_to_open_page = array(
    '1' => 'slider',
    '2' => 'navigation',
    '3' => 'innerfooter'
);

 /*module cache array*/
$module_cache_folder=array(

//'news'=>array('0'=>'news','1'=>'news_item'),
'achievement'=>array('0'=>'achievement'),
'affiliations'=>array('0'=>'affiliations'),
'content'=>array('0'=>'content'),
'courses'=>array('0'=>'courses'),
'document'=>array('0'=>'document'),
'event'=>array('0'=>'event','1'=>'eventcalendar'),
'faculty'=>array('0'=>'faculty'),
'faqs'=>array('0'=>'faqs'),
'gallery'=>array('0'=>'gallery'),
'news'=>array('0'=>'news'),
'jobs'=>array('0'=>'jobs'),
'notice'=>array('0'=>'notice'),
'placement'=>array('0'=>'placement'),
'resources'=>array('0'=>'resources')
);


        
 /*user settings array*/
        
 $user_settings=array (
     
     "0"=>array("title"=>"Admin email adress","key"=>"ADMIN_EMAIL","value"=>"","name"=>"email","type"=>"text"),
     "1"=>array("title"=>"Support email adress","key"=>"SUPPORT_EMAIL","value"=>"","name"=>"email","type"=>"text"),
     "2"=>array("title"=>"BCC email adress","key"=>"BCC_EMAIL","value"=>"","name"=>"email","type"=>"text"),
     "3"=>array("title"=>"Facebook Page","key"=>"FACEBOOK_PAGE","value"=>"","name"=>"social","type"=>"text"),
     "4"=>array("title"=>"Twitter Page","key"=>"TWITTER_PAGE","value"=>"","name"=>"social","type"=>"text"),
     "5"=>array("title"=>"Youtube Page","key"=>"YOUTUBE_PAGE","value"=>"","name"=>"social","type"=>"text"),
     "6"=>array("title"=>"Google Verify Code","key"=>"GOOGLE_VC","value"=>"","name"=>"site-verify","type"=>"text"),
     "7"=>array("title"=>"Yahoo Verify Code","key"=>"YAHOO_VC","value"=>"","name"=>"site-verify","type"=>"text"),
     "8"=>array("title"=>"Bing Verify Code","key"=>"BING_VC","value"=>"","name"=>"site-verify","type"=>"text"),
     "9"=>array("title"=>"Google Analytic Code","key"=>"GOOGLE_AC","value"=>"","name"=>"analytics","type"=>"text"),
     "10"=>array("title"=>"Google Analytic Username","key"=>"ANALYTIC_USER","value"=>"","name"=>"analytics","type"=>"text"),
     "11"=>array("title"=>"Google Analytic Password","key"=>"ANALYTIC_PASS","value"=>"","name"=>"analytics","type"=>"text"),
     "12"=>array("title"=>"Google Analytic Table(ga:XXXXXXX)","key"=>"ANALYTIC_TABLE","value"=>"","name"=>"analytics","type"=>"text"),
     "13"=>array("title"=>"404 Page Content","key"=>"NOT_FOUND","value"=>"","name"=>"general","type"=>"textarea"),
     "14"=>array("title"=>"Enable Website Cache","key"=>"SITE_CACHE_STATUS","value"=>"1","name"=>"cache-debugging","type"=>"select"),

     
 );      
        
        

?>