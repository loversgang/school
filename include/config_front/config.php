<?php

/* output buffering started */
ob_start();

/* set error level */
error_reporting(E_ALL);

/* start session by default */

session_start();

/*
 *   Code Developed By: cWebConsultants Team - India (Chandigarh)
 *   Project Name: <content managment system> - cWebConsultants
 *   Dated: <10 Jan, 2012>
 *   *** Copyrighted by cWebConsultants India - We reserve the right to take legal action against anyone using this software without our permission.  ***
 */


/* set website filesystem */

define("DIR_FS_SITE", dirname(dirname(dirname(__FILE__))).'/',true);


/*
* Be very carefully while setting these variables 
* These are used in the URL Rewrite
*/

define("HTTP_SERVER", "http://school",true);    /* write only domain name  (without slash in the end i.e. http://www.abc.com) */
define("DIR_WS_SITE", HTTP_SERVER."/site/".$school_id."/",true);   /* whatever comes after the domain name (i.e. /folder name1/folder name 2/) */
define("DIR_WS_SITE_PATH", HTTP_SERVER."/",true); 
/* 
* set databse details here 
*/

$DBHostName = "localhost";
$DBDataBase = "school1";
$DBUserName = "root";
$DBPassword = "";


if (!$link = mysql_connect($DBHostName,$DBUserName,'')) {
    echo 'Could not connect to mysql';
    exit;
}

if (!mysql_select_db($DBDataBase, $link)) {
    echo 'Could not select database';
    exit;
}

$sql = "SELECT theme.folder_name FROM school, theme where school.is_active='1' and school.id='".$school_id."' and school.is_deleted='0' and school.is_website='1' and school.theme=theme.id ORDER BY school.id desc";

$result = mysql_query($sql,$link);
$record=mysql_fetch_object($result);
if(is_object($record)):
	$theme=$record->folder_name;
else:
	echo die('Sorry,This Institute is not authorized for website..');
endif;


define('CURRENT_THEME', $theme);

define('THEME_PATH',DIR_WS_SITE_PATH.'theme/'.CURRENT_THEME,true);



/*
*   --- WARNING ---
*  All the files below are location sensitive. 
*  Maintain the sequence of files
*
*/
# include sub-configuration files here.

require_once(DIR_FS_SITE."include/config_front/url.php");

# include the database class files.
require_once(DIR_FS_SITE_INCLUDE_CLASS."mysql.php");
require_once(DIR_FS_SITE_INCLUDE_CLASS."query.php");


# include the utitlity files here
require_once(DIR_FS_SITE_INCLUDE_CLASS."phpmailer.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG."constant.php");
require_once(DIR_FS_SITE_INCLUDE_CONFIG."message.php");

# custom files
//include_once(DIR_FS_SITE_INCLUDE_CLASS.'login_session.php');
include_once(DIR_FS_SITE_INCLUDE_CLASS.'admin_session.php');


# include function files here.
include_once(DIR_FS_SITE.'include/function/basic.php');

/*include function classes here*/

include_once(DIR_FS_SITE.'include/functionClass/imageManipulationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/fileClass.php');
include_once(DIR_FS_SITE.'include/functionClass/dateClass.php');
include_once(DIR_FS_SITE.'include/functionClass/emailClass.php');
include_once(DIR_FS_SITE.'include/functionClass/errorClass.php');

/*include smarty class*/
require_once(DIR_FS_SITE.'include/libs/Smarty.class.php');
require_once(DIR_FS_SITE.'include/class/smartyClass.php');


date_default_timezone_set('Asia/Calcutta');

# fix for stripslashes issues in php
if(get_magic_quotes_gpc()):
	$_POST=array_map_recursive('stripslashes', $_POST);
	$_GET=array_map_recursive('stripslashes', $_GET);
	$_REQUEST=array_map_recursive('stripslashes', $_REQUEST);
endif;
$publickey = "6LcWw7kSAAAAADZv_vEtAK2oEHvk3XcHuEoThrbN";
$privatekey = "6LcWw7kSAAAAACgQDdDBBIei-rxYTOPlgI02lDG9";



include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSettingClass.php');

require_once(DIR_FS_SITE."include/config_front/constant_site.php");/*user constant file*/

?>

