<?php


class quickSms extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;


    function __construct($order='asc', $orderby='position'){
        parent::__construct('quicksms');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'school_id','sms_text','position','is_deleted', 'is_active','date_added');

	}

    /*
     * Create new course or update existing theme
     */
    function saveData($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
       
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              return $this->Data['id'];
        }
        else{   
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get sms by id
     */
    function getRecord($id){
        return $this->_getObject('quicksms', $id);
    }

    
    /*
     * Get List of quick sms 
     */
    function listAll($show_active=0){
		
		if($show_active)
			$this->Where="where is_deleted='0' and is_active='1'  ORDER BY position asc";
		else
			$this->Where="where is_deleted='0' ORDER BY position asc";

		return $this->ListOfAllRecords('object');
    }

  

    /*
     * delete a sms by id
     */
    function deleteRecord($id){ 
        $this->id=$id;
            return $this->Delete();
    }

    /*
     * Update sms position
     */
    function updatePosition($position, $id){
        $this->Data['id']=$id;
        $this->Data['position']=($position!='')?$position:0;
        $this->Update();
    }

    

   
}
?>