<?php

class schoolSms extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='id'){
        parent::__construct('school_sms');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'school_id','type');
	}

    /* Create new School SMS */
    function saveData($POST,$school){
        if(!empty($POST)):
            foreach($POST as $key=>$value):
                $newQuery=new query('school_sms');
                $newQuery->Data['school_id']=$school;
                $newQuery->Data['type']=$key;
                $newQuery->Insert();
            endforeach;
        endif;
       return true;
        
    }

    /*
     * Get School SMS by id
     */
    function getPaymentRecord($id){
        return $this->_getObject('school_sms', $id);
    }

    function listAll($school=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($school)
			$this->Where="where school_id='".$school."' ORDER BY id asc";
		else
			$this->Where="where id!='0' ORDER BY id asc";
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('array');
    }
    
    function getSchoolSmsArray($school=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
                        $records=array();
			$this->Where="where school_id='".$school."' ORDER BY id asc";

			$this->DisplayAll();
				if($this->GetNumRows()): 
                                    while($Obj=$this->GetObjectFromRecord()): 				
                                                            $records[]=$Obj->type;		
                                    endwhile;
                                endif;
                         return $records;       
    }
    

    /* delete a School SMS by id */
    function deleteRecord($id){
        $this->id=$id;        
            return $this->Delete();
    }
     /* delete a School SMS by id */
    function deleteSchoolRecord($school){
                    /* Check for previous value */
                $newQuery=new query('school_sms');
                $newQuery->Where="where school_id='".$school."'";
                $newQuery->Delete_where(); 
                return ture;
    }
    
    function getTypeSms($school,$type){
                $this->Where="where school_id='".$school."' and type='".mysql_real_escape_string($type)."'";
                return $this->DisplayOne(); 
    }    

    function getMonthlyCount($school){
                $Query=new query('school_sms_counter');
                $Query->Field=('count(*) as count');
                $Query->Where="where school_id='".$school."' and MONTH(on_date)='".date('m')."' and YEAR(on_date)='".date('Y')."'";
                $rec=$Query->DisplayOne(); 
                if($rec):
                    return $rec->count; 
                else:
                    return '0';
                endif;
    }   
    
}
?>