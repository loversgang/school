<?php


class noticeBoard extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order='asc', $orderby='position'){
        parent::__construct('notice_board');
	$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id','school_id','name','short_description','description','urlname','on_date','image','meta_name','meta_keyword','meta_description','position','is_active','is_deleted');
    }

    /*
     * Create new noticeBoard or update existing content
     */
    function saveData($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['on_date']=date('Y-m-d',strtotime($this->Data['on_date']));
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';

        if($this->Data['meta_name']==''){
            $this->Data['meta_name']=$this->Data['name'];
        }

        if($this->Data['meta_keyword']==''){
            $this->Data['meta_keyword']=$this->Data['name'];
        }

        if($this->Data['meta_description']==''){
            $this->Data['meta_description']=$this->Data['name'];
        }

        if($this->Data['urlname']==''){
            $this->Data['urlname']=$this->_sanitize($this->Data['urlname']);
        }
        
         if($_FILES['image']):
            $rand=rand(0, 99999999);
            $image_obj=new imageManipulation();

            if($image_obj->upload_photo('notice_board', $_FILES['image'], $rand)):
                    $this->Data['image']=$image_obj->makeFileName($_FILES['image']['name'], $rand);
            endif;  
        endif;         

        if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              $max_id=$this->Data['id'];
        }
        else{
            $this->Insert();
            $max_id=$this->GetMaxId();
        }
		
        $quryObj=new noticeBoard();
        $object=$quryObj->getNoticeBoard($max_id);
        if($object){            
          $Query = new query('notice_board');  
          $Query->Data['id']=$max_id;
          $Query->Data['urlname']=$Query->_sanitize($object->name.'-'.$max_id);          
          $Query->Update();
        }
        return $max_id;		
    }

    /*
     * Get noticeBoard by id
     */
    function getNoticeBoard($id){
        return $this->_getObject('notice_board', $id);
    }
    
     function deleteImage($id){
        $this->Data['image']='';
        $this->Data['id']=$id;
        
          return $this->Update();
    }    
 /*check if noticeBoard exists for user*/
    function getSchoolNoticeBoardList($school_id,$show_active=0, $result_type='object'){      
		if($show_active)
			$this->Where="where is_deleted='0' and school_id='".mysql_real_escape_string($school_id)."' and is_active='1'  ORDER BY on_date desc";
		else
			$this->Where="where is_deleted='0' and school_id='".mysql_real_escape_string($school_id)."' ORDER BY on_date desc";
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
      
    }
    
  
    function deleteRecord($id){ 
        $this->id=$id;
            return $this->Delete();
    }
 

      
}
?>