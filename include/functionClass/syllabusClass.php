<?php

class syllabus extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('school_syllabus');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'session_id', 'session_year', 'subject_id', 'file', 'description', 'is_active', 'date_add', 'date_upd');
    }

    /*
     * Create New Syllabus
     */

    function saveSyllabus($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['date_upd'] = date('d-m-Y');
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_add'] = date('d-m-Y');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Syllabus list by id
     */

    function getRecord($id) {
        return $this->_getObject('school_syllabus', $id);
    }

    /*
     * Get List of all Syllabus list in array
     */

    function listSyllabus($sess_int) {
        $this->Where = "where session_year='$sess_int'";
        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a subject list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /*
     * Update subject list position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    // Get Syllabus By Session ID
    function getSyllabusBySessionId($session_id) {
        $this->Where = "Where session_id='$session_id' and is_active='1'";
        return $this->ListOfAllRecords('object');
    }

}
