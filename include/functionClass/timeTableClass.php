<?php

class timeTable extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('school_time_table');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'session_id', 'session_year', 'section', 'day', 'subject', 'staff_id', 'comment', 'is_active', 'time_from', 'time_to', 'date_add', 'date_upd');
    }

    /*
     * Create New Syllabus
     */

    function saveTimeTable($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['date_upd'] = date('d-m-Y');
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_add'] = date('d-m-Y');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Syllabus list by id
     */

    function getRecord($id) {
        return $this->_getObject('school_time_table', $id);
    }

    /*
     * Get List of all Syllabus list in array
     */

    function listTimeTable($sess_int) {
        $this->Where = "where session_year='$sess_int'";
        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a subject list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /*
     * Update subject list position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    // Get Time Table By Session ID
    function getTimeTableBySessionId($session_id) {
        $this->Where = "Where session_id='$session_id' and is_active='1'";
        return $this->ListOfAllRecords('object');
    }

    // Get Session_id By Staff ID
    function getSessionIdByStaffId($login_staff_id) {
        $this->Where = "WHERE `staff_id` = '$login_staff_id' group by session_id";
        return $this->ListOfAllRecords();
    }

    function getSectionByStaff_session_id($login_staff_id, $session_id) {
        $this->Where = "WHERE `staff_id` = '$login_staff_id' AND `session_id` = '$session_id' group by session_id";
        return $this->ListOfAllRecords();
    }
    function getSectionByStaff_session_id_all($login_staff_id, $session_id) {
        $this->Where = "WHERE `staff_id` = '$login_staff_id' AND `session_id` = '$session_id'";
        return $this->ListOfAllRecords();
    }

    function getSectionByStaff_session_id_single($login_staff_id, $session_id) {
        $this->Where = "WHERE `staff_id` = '$login_staff_id' AND `session_id` = '$session_id' group by session_id";
        return $this->DisplayOne();
    }

    // Get Staff List By Student Session ID
    function getStaffBySessionId($session_id) {
        $this->Where = "where session_id='$session_id' and is_active='1' group by staff_id";
        return $this->ListOfAllRecords('object');
    }

    // Get Staff Time Table
    function getTimeTableByStaffId($staff_id) {
        $this->Where = "where staff_id='$staff_id' and is_active='1'";
        return $this->ListOfAllRecords('object');
    }

    // Get Class Time Table
    function getSessionTiming($session_id, $section) {
        $this->Where = "where session_id='$session_id' and section='$section' group by time_from";
        return $this->ListOfAllRecords('object');
    }

    // Get class Time
    public static function getClassTime($session_id, $ct_sec, $day, $time_from, $time_to) {
        $obj = new timeTable();
        $obj->Where = "where session_id='$session_id' and section='$ct_sec' and day='$day' and time_from='$time_from' and time_to='$time_to'";
        return $obj->DisplayOne();
    }

    // Get Staff Time Table
    function getStaffTiming($staff_id) {
        $this->Where = "where staff_id='$staff_id' group by time_from";
        return $this->ListOfAllRecords('object');
    }

    // Get class Time
    public static function getStaffTime($staff_id, $day, $time_from, $time_to) {
        $obj = new timeTable();
        $obj->Where = "where staff_id='$staff_id' and day='$day' and time_from='$time_from' and time_to='$time_to'";
        return $obj->DisplayOne();
    }

}