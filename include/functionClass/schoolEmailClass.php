<?php

class schoolEmail extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='id'){
        parent::__construct('school_email');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'school_id','type');
	}

    /* Create new School Email */
    function saveData($POST,$school){
        if(!empty($POST)):
            foreach($POST as $key=>$value):
                $newQuery=new query('school_email');
                $newQuery->Data['school_id']=$school;
                $newQuery->Data['type']=$key;
                $newQuery->Insert();
            endforeach;
        endif;
       return true;
        
    }

    /*
     * Get School Email by id
     */
    function getPaymentRecord($id){
        return $this->_getObject('school_email', $id);
    }

    function listAll($school=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($school)
			$this->Where="where school_id='".$school."' ORDER BY id asc";
		else
			$this->Where="where id!='0' ORDER BY id asc";
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('array');
    }
    
    function getSchoolEmailArray($school=0, $result_type='object'){		
                        $records=array();
			$this->Where="where school_id='".$school."' ORDER BY id asc";

			$this->DisplayAll();
				if($this->GetNumRows()): 
                                    while($Obj=$this->GetObjectFromRecord()): 				
                                             $records[]=$Obj->type;		
                                    endwhile;
                                endif;
                         return $records;       
    }
    

    /* delete a School Email by id */
    function deleteRecord($id){
        $this->id=$id;        
            return $this->Delete();
    }
     /* delete a School Email by id */
    function deleteSchoolRecord($school){
                    /* Check for previous value */
                $newQuery=new query('school_email');
                $newQuery->Where="where school_id='".$school."'";
                $newQuery->Delete_where(); 
                return ture;
    }
    
    function getTypeEmail($school,$type){
                $this->Where="where school_id='".$school."' and type='".mysql_real_escape_string($type)."'";
                return $this->DisplayOne(); 
    } 
    
    function getMonthlyCount($school){
                $Query=new query('school_email_counter');
                $Query->Field=('count(*) as count');
                $Query->Where="where school_id='".$school."' and MONTH(on_date)='".date('m')."' and YEAR(on_date)='".date('Y')."'";
                $rec=$Query->DisplayOne(); 
                if($rec):
                    return $rec->count; 
                else:
                    return '0';
                endif;
    }     
   
}
?>