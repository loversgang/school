<?php


class admin extends cwebc {
    
    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;
    
     /*
      * 
     */
    function __construct($order='asc', $orderby='position'){
        $this->InitilizeSQL();
        $this->orderby=$orderby;
        $this->order=$order;
        parent::__construct('admin');
        $this->TableName='admin_user';
        $this->requiredVars=array('id', 'username', 'password', 'last_access', 'allow_pages', 'type','list_clients', 'created_by','is_active', 'is_loggedin');
    }

     /*
     * Create new page or update existing page
     */
    function saveAdminUser($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['email']=$POST['email'];
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
        if(isset($POST['list_clients']) && !empty($POST['list_clients'])):
             $this->Data['list_clients']=implode(",",$POST['list_clients']);
        endif;
        if(!empty($this->Data['password'])):
                $this->Data['password']=encrypt_decrypt('encrypt',$this->Data['password']);
        endif;  
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){
           
            if($this->Update())
              return $Data['id'];
        }
        else{
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
    /*
     * Get page by id
     */
    function getAdminUser($id){
        return $this->_getObject('admin_user', $id);
    }
    
    
    /*
     * Get List of all pages in array
     */
    function listAdminUsers($list=0){
        
        if($list):
            $this->Where="where is_deleted='0' and created_by='".  mysql_real_escape_string($list)."'";  
        else:
            $this->Where="where is_deleted='0' ";  
        endif;
        $this->DisplayAll();        
    }
    
    /*
     * check admin user with name
     */
    function checkUsers($username,$email){
        $this->Where="where is_deleted='0' AND (username='".mysql_real_escape_string($username)."' OR email='".mysql_real_escape_string($email)."')";    
        $this->DisplayAll();        
    }
    
    /*
     * check admin user with name in update case
     */
    function checkUsersWithID($username,$email,$id){
        $this->Where="where is_deleted='0' AND id!='$id' AND (username='".mysql_real_escape_string($username)."' OR email='".mysql_real_escape_string($email)."')";  
        $this->DisplayAll();        
    }
    
    /*
     * delete a page by id
     */
    function deleteAdminUser($id){
        $this->id=$id;
        if(SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }
    
     function getThrash($list=0){
         if($list):
            $this->Where="where is_deleted='1' and created_by='".  mysql_real_escape_string($list)."'";  
        else:
            $this->Where="where is_deleted='1' ";  
        endif;        
          
          $this->DisplayAll();
    }
    
   
}
?>