<?php
/*
 * Thrash Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class theme extends cwebc {
    
    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;
    protected $thrash;
    protected $modules;
    /*
     * 
     */
    function __construct($order='asc', $orderby='id'){
        parent::__construct('theme');
		$this->orderby=$orderby;
        $this->order=$order;
         $this->requiredVars=array('id','name','folder_name','image','is_active','remarks','on_date','update_date');

	}

    /*
     * Get theme by id
     */
    function getTheme($id){
        return $this->_getObject('theme', $id);
    }
    
	 
    /*
     * Get List of all theme in array
     */
    function listThemes($show_active=0, $result_type='object'){
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        $this->Where="where is_active='1'";
        return $this->ListOfAllRecords('object');    
    }
    
    
    function getThemeName($id){
		$this->Field=('name');
		$this->Where="where id='".mysql_real_escape_string($id)."'";
		$rec=$this->DisplayOne(); 
		if($rec):
			return $rec->name;
		else:
			return false;
		endif;
    }
	
    function getFolderName($id){
		$this->Field=('folder_name');
		$this->Where="where id='".mysql_real_escape_string($id)."'";
		$rec=$this->DisplayOne(); 
		if($rec):
			return $rec->folder_name;
		else:
			return false;
		endif;
    }
	
}
?>