<?php

class school_header extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('school_header');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'from_id', 'from_type', 'to_id', 'to_type', 'subject', 'from_status', 'to_status', 'time');
    }

    /*
     * Create New Syllabus
     */

    function saveHeader($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Syllabus list by id
     */

    function getRecord($id) {
        return $this->_getObject('school_header', $id);
    }

    // Delete Student Messages
    function updateStatusStudentInbox($h_id, $student_id) {
        $this->Data = array(
            'to_status' => '0'
        );
        $this->Where = "where to_id='$student_id' and to_type='student' and id='$h_id'";
        $this->UpdateCustom();
    }

    function updateStatusStudentOutbox($h_id, $student_id) {
        $this->Data = array(
            'from_status' => '0'
        );
        $this->Where = "where from_id='$student_id' and from_type='student' and id='$h_id'";
        $this->UpdateCustom();
    }

    // Delete Staff Messages
    function updateStatusStaffInbox($h_id, $staff_id) {
        $this->Data = array(
            'to_status' => '0'
        );
        $this->Where = "where to_id='$staff_id' and to_type='staff' and id='$h_id'";
        $this->UpdateCustom();
    }

    function updateStatusStaffOutbox($h_id, $staff_id) {
        $this->Data = array(
            'from_status' => '0'
        );
        $this->Where = "where from_id='$staff_id' and from_type='staff' and id='$h_id'";
        $this->UpdateCustom();
    }

    // Delete Admin Messages
    function updateStatusAdminInbox($h_id, $admin_id) {
        $this->Data = array(
            'to_status' => '0'
        );
        $this->Where = "where to_id='$admin_id' and to_type='admin' and id='$h_id'";
        $this->UpdateCustom();
    }

    function updateStatusAdminOutbox($h_id, $admin_id) {
        $this->Data = array(
            'from_status' => '0'
        );
        $this->Where = "where from_id='$admin_id' and from_type='admin' and id='$h_id'";
        $this->UpdateCustom();
    }

    // Get Messages Count
    public static function getInboxCount($school_id, $user_id, $user_type) {
        $obj = new school_header;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where school_id='$school_id' and to_id='$user_id' and to_type='$user_type' and to_status='1'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    public static function getOutboxCount($school_id, $user_id, $user_type) {
        $obj = new school_header;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where school_id='$school_id' and from_id='$user_id' and from_type='$user_type' and from_status='1'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    // Get student Messages Count
    public static function getInboxCountUnread($school_id, $user_id, $user_type) {
        $obj = new query('school_header,school_messages');
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where school_header.school_id='$school_id' and school_header.to_id='$user_id' and school_header.to_type='$user_type' and school_header.id=school_messages.header_id and school_messages.is_read='0' and to_status='1'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    public static function getOutboxCountTotal($school_id, $user_id, $user_type) {
        $obj = new query('school_header,school_messages');
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where school_header.school_id='$school_id' and school_header.from_id='$user_id' and school_header.from_type='$user_type' and school_header.id=school_messages.header_id and from_status='1'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    // Get Student Messages
    function getInBoxMessagesStudent($school_id, $student_id) {
        $this->Where = "where school_id='$school_id' and to_id='$student_id' and to_type='student' and to_status='1' order by id desc";
        return $this->ListOfAllRecords('object');
    }

    function getOutBoxMessagesStudent($school_id, $student_id) {
        $this->Where = "where school_id='$school_id' and from_id='$student_id' and from_type='student' and from_status='1' order by id desc";
        return $this->ListOfAllRecords('object');
    }

    // Get Staff Messages
    function getInBoxMessagesStaff($school_id, $staff_id) {
        $this->Where = "where school_id='$school_id' and to_id='$staff_id' and to_type='staff' and to_status='1' order by id desc";
        return $this->ListOfAllRecords('object');
    }

    function getOutBoxMessagesStaff($school_id, $staff_id) {
        $this->Where = "where school_id='$school_id' and from_id='$staff_id' and from_type='staff' and from_status='1' order by id desc";
        return $this->ListOfAllRecords('object');
    }

    // Get Admin Messages
    function getInBoxMessagesAdmin($school_id, $admin_id) {
        $this->Where = "where school_id='$school_id' and to_id='$admin_id' and to_type='admin' and to_status='1' order by id desc";
        return $this->ListOfAllRecords('object');
    }

    function getOutBoxMessagesAdmin($school_id, $admin_id) {
        $this->Where = "where school_id='$school_id' and from_id='$admin_id' and from_type='admin' and from_status='1' order by id desc";
        return $this->ListOfAllRecords('object');
    }

    public static function checkAuth($school_id, $user_id, $user_type, $h_id) {
        $obj = new query('school_header as h,school_messages as m');
        $obj->Where = "where h.school_id='$school_id' and (h.from_id='$user_id' or h.to_id='$user_id') AND (h.from_type='$user_type' or h.to_type='$user_type') AND m.header_id='$h_id' and m.header_id=h.id";
        return $obj->DisplayOne();
    }

}

class messages extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('school_messages');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'header_id', 'content', 'is_read', 'time');
    }

    /*
     * Create New Syllabus
     */

    function saveMessage($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Syllabus list by id
     */

    function getRecord($id) {
        return $this->_getObject('school_messages', $id);
    }

    /*
     * Get List of all Syllabus list in array
     */

    function listMessages($school_id) {
        $this->Where = "where school_id='$school_id'";
        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a subject list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    function getMessageDetails($h_id) {
        $this->Where = "where header_id='$h_id'";
        return $this->DisplayOne();
    }

}
