<?php

class student extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'reg_id', 'date_of_admission', 'course_of_admission', 'first_name', 'last_name', 'sex', 'date_of_birth', 'email', 'phone', 'cast', 'category', 'photo', 'religion', 'blood_group', 'height', 'weight', 'vision', 'ailment', 'medical_issue', 'ref_name', 'allergies', 'ref_email', 'ref_phone', 'ref_remark', 'on_date', 'update_date', 'is_deleted', 'is_active', 'quit_date', 'quit', 'eye_right', 'eye_left', 'family_doctor', 'family_doctor_no', 'contact_person', 'contact_relation', 'contact_person_no', 'student_login_id', 'password', 'msg_notif_email');
    }

    // Get student Classmate  by session_id
    function getStudentClassmates($session_id, $student_id) {
        $obj = new query("student,session_students_rel");
        $obj->Field = "student.first_name,student.last_name,student.photo,student.id as student_id";
        $obj->Where = "where student.id=session_students_rel.student_id and session_students_rel.session_id='$session_id' AND student.id != '$student_id' GROUP BY student.id";
        $data = $obj->ListOfAllRecords('object');
        return $data;
    }

    function getStudentSessionId($student_id) {
        $query = new query("student_session_interval");
        $query->Where = "WHERE `student_id` = '$student_id'";
        $student_session = $query->DisplayOne();
        return $student_session->session_id;
    }

    public static function getStudentBirthInformation($student_id) {
        $query = new student;
        $query->Where = "WHERE `id` = '$student_id'";
        return $query->DisplayOne();
    }

    /* Create new student or update existing  */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['date_of_birth'] = date('Y-m-d', strtotime($this->Data['date_of_birth']));
        $this->Data['date_of_admission'] = date('Y-m-d', strtotime($this->Data['date_of_admission']));
        if (isset($this->Data['quit_date']) && !empty($this->Data['quit_date'])):
            $this->Data['quit_date'] = date('Y-m-d', strtotime($this->Data['quit_date']));
        endif;

        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if ($_FILES['image']):
            $rand = rand(0, 99999999);
            $image_obj = new imageManipulation();
            if ($image_obj->upload_photo('student', $_FILES['image'], $rand)):
                $this->Data['photo'] = $image_obj->makeFileName(_sanitize($_FILES['image']['name']), $rand);
            endif;
        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    // Save or Update Student Details
    function saveStudentDetails($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /* Get student by id */

    function getStudent($id) {
        return $this->_getObject('student', $id);
    }

    function deleteImage($id) {
        $this->Data['photo'] = '';
        $this->Data['id'] = $id;

        return $this->Update();
    }

    function cehck_reg_id($school_id, $reg_id, $student_id = 0) {
        if ($student_id)
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and reg_id='" . mysql_real_escape_string($reg_id) . "' and id!='" . mysql_real_escape_string($student_id) . "'  ORDER BY id asc";
        else
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and reg_id='" . mysql_real_escape_string($reg_id) . "' ORDER BY id asc";

        return $this->DisplayOne();
    }

    function getStudentPrefix($school_id) {
        $this->Field = ('reg_id');
        $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' ORDER BY id desc";

        $rec = $this->DisplayOne();

        if (is_object($rec)):
            $pos = strrpos($rec->reg_id, '-');
            if ($pos):
                $record = explode('-', $rec->reg_id);
                return ($record['1'] + 1);
            else:
                return $rec->reg_id;
            endif;
            ;
        else:
            return 1;
        endif;
    }

    /* Get List of all student  */

    function getSchoolStudents($school_id, $show_active = 0, $result_type = 'object') {
        if ($show_active) {
            $this->Where = "where is_deleted='0' and school_id='" . mysql_real_escape_string($school_id) . "' and is_active='1'  ORDER BY id asc";
        } else {
            $this->Where = "where is_deleted='0' and school_id='" . mysql_real_escape_string($school_id) . "' ORDER BY id asc";
        }
        if ($result_type == 'object') {
            return $this->DisplayAll();
        } else {
            return $this->ListOfAllRecords('object');
        }
    }

    /* Get List of all student  */

    function getSchoolStudentsWithParents($school_id) {
        $query = new query('student,student_family_details');
        $query->Field = ('student.*,student_family_details.father_name,student_family_details.mother_name,student_family_details.father_occupation,student_family_details.mother_occupation,student_family_details.household_annual_income,student_family_details.father_education,student_family_details.mother_education,student_family_details.total_male_sibling,student_family_details.total_female_sibling');
        $query->Where = "where student.school_id='" . mysql_real_escape_string($school_id) . "' and student.id=student_family_details.student_id and student.is_active='1' GROUP BY student.id ORDER BY student.id desc";
        return $query->ListOfAllRecords('object');
    }

    /* delete a student by id   */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /* Get List of all student  */

    function getActiveDeactiveStudents($school_id) {
        $record = array('active' => '0', 'in_active' => '0');

        $this->Field = ('is_active,count(*) as total');
        $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' GROUP BY is_active";

        $rec = $this->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $val):
                if ($val->is_active = '0'):
                    $record['in_active'] = $val->total;
                endif;
            endforeach;
        endif;
        return true;
    }

    public static function checkUserIdExistOrNot($id) {
        $object = new student;
        $object->Where = "WHERE `id` = '$id'";
        return $object->DisplayOne();
    }

    function notificationEnabaleDisable($id) {
        if (isset($_POST['msg_notif_email'])) {
            $this->Data['msg_notif_email'] = 1;
        } else {
            $this->Data['msg_notif_email'] = 0;
        }
        $this->Where = "WHERE `id` = '$id'";
        $this->UpdateCustom();
    }

    function sendEmailIfNotificationEnable($id) {
        $this->Where = "WHERE `msg_notif_email` = 1 AND `id` = '$id'";
        return $this->DisplayOne();
    }

    public static function getStudentInformation($id) {
        $query = new student;
        $query->Where = "WHERE `id` = '$id'";
        return $query->DisplayOne();
    }

}

/* student Address Class */

class studentAddress extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_address');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'student_id', 'address1', 'address2', 'city', 'state', 'zip', 'country', 'address_type', 'on_date', 'update_date', 'tehsil', 'district');
    }

    /* Create new student_address or update existing  */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /* Get student_address by id */

    function getSutdendAddress($id) {
        return $this->_getObject('student_address', $id);
    }

    /* Get List of all student_address */

    function getStudentAddress($student_id, $result_type = 'object') {

        $this->Where = "where student_id='" . mysql_real_escape_string($student_id) . "' ORDER BY id asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* Get List of all student_address by type */

    function getStudentAddressByType($student_id, $type) {
        $this->Where = "where student_id='" . mysql_real_escape_string($student_id) . "' AND address_type='" . mysql_real_escape_string($type) . "' ORDER BY id asc";
        return $this->DisplayOne();
    }

    /* delete a student_address by id */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

/* Student Family Class */

class studentFamily extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_family_details');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'student_id', 'father_name', 'mother_name', 'father_occupation', 'mother_occupation', 'household_annual_income', 'father_education', 'mother_education', 'total_male_sibling', 'total_female_sibling', 'email', 'mobile', 'family_photo', 'update_date', 'to_date', 'phone', 'mobile2');
    }

    /* Create new Student family or update existing  */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if ($_FILES['image']):
            $rand = rand(0, 99999999);
            $image_obj = new imageManipulation();
            if ($image_obj->upload_photo('student_family', $_FILES['image'], $rand)):
                $this->Data['family_photo'] = $image_obj->makeFileName($_FILES['image']['name'], $rand);
            endif;
        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function add_blank_entry($student) {

        $this->Data['student_id'] = $student;
        $this->Data['on_date'] = date('Y-m-d');
        $this->Insert();
        return $this->GetMaxId();
    }

    /* Get Student family by id */

    function getStudentFamilyDeatils($student_id) {
        $this->Where = "where student_id='" . mysql_real_escape_string($student_id) . "' ORDER BY id asc";
        return $this->DisplayOne();
    }

    /* delete a family by id Experience */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    function deleteImage($id) {
        $this->Data['family_photo'] = '';
        $this->Data['id'] = $id;
        return $this->Update();
    }

}

/* Student Sibling Class */

class studentSibling extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_sibling_stuty');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'student_id', 'reg_id', 'name', 'class', 'on_date', 'relation', 'update_date');
    }

    /* Create new entry or update existing  */

    function saveData($POST, $student_id) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (!empty($POST)):
            foreach ($POST as $key => $value):

                if (!empty($value['reg_id'])):
                    $ObjQuery = new query('student_sibling_stuty');
                    $ObjQuery->Data['student_id'] = $student_id;
                    $ObjQuery->Data['reg_id'] = $value['reg_id'];
                    $ObjQuery->Data['name'] = $value['name'];
                    $ObjQuery->Data['class'] = $value['class'];
                    $ObjQuery->Data['relation'] = $value['relation'];
                    if (isset($value['id'])):
                        $ObjQuery->Data['update_date'] = date('Y-m-d');
                        $ObjQuery->Data['id'] = $value['id'];
                        $ObjQuery->Update();
                    else:
                        $ObjQuery->Data['on_date'] = date('Y-m-d');
                        $ObjQuery->Insert();
                    endif;

                endif;
            endforeach;
        endif;
        return true;
    }

    /* Get info by id */

    function getSiblingInfo($id) {
        return $this->_getObject('student_sibling_stuty', $id);
    }

    /* Get List of student siblings */

    function getStudentSibling($student_id, $result_type = 'object') {

        $this->Where = "where student_id='" . mysql_real_escape_string($student_id) . "' ORDER BY id asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* delete a id  */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

/* Student Previous School Class */

class studentPreviousSchool extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_previous_school');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'student_id', 'school_name', 'address1', 'address2', 'city', 'state', 'country', 'class', 'percentage', 'on_date', 'update_date');
    }

    /* Create new entry or update existing  */

    function saveData($POST, $student_id) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (!empty($POST)):
            foreach ($POST as $key => $value):

                if (!empty($value['school_name'])):
                    $ObjQuery = new query('student_previous_school');
                    $ObjQuery->Data['student_id'] = $student_id;
                    $ObjQuery->Data['address1'] = $value['address1'];
                    $ObjQuery->Data['school_name'] = $value['school_name'];
                    $ObjQuery->Data['address2'] = $value['address2'];
                    $ObjQuery->Data['city'] = $value['city'];
                    $ObjQuery->Data['state'] = $value['state'];
                    $ObjQuery->Data['country'] = $value['country'];
                    $ObjQuery->Data['class'] = $value['class'];
                    $ObjQuery->Data['percentage'] = $value['percentage'];
                    if (isset($value['id'])):
                        $ObjQuery->Data['update_date'] = date('Y-m-d');
                        $ObjQuery->Data['id'] = $value['id'];
                        $ObjQuery->Update();
                    else:
                        $ObjQuery->Data['on_date'] = date('Y-m-d');
                        $ObjQuery->Insert();
                    endif;

                endif;
            endforeach;
        endif;
        return true;
    }

    /* Get info by id */

    function getPreviousSchool($id) {
        return $this->_getObject('student_previous_school', $id);
    }

    /* Get List of student PreviousSchool */

    function getStudentPreviousSchool($student_id, $result_type = 'object') {

        $this->Where = "where student_id='" . mysql_real_escape_string($student_id) . "' ORDER BY id asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* delete a id  */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

/* Student Documents Class */

class studentDocument extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_document');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'student_id', 'title', 'file', 'on_date', 'update_date');
    }

    /* Create new Student Documents or update existing  */

    function saveData($POST, $student_id) {

        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (!empty($POST)):
            foreach ($POST as $key => $value):
                if (!empty($value['title'])):
                    /* Make Array to file upload */

                    $_FILES['document'] = array();
                    $_FILES['document'] = array('name' => $_FILES['file']['name'][$key], 'type' => $_FILES['file']['type'][$key], 'tmp_name' => $_FILES['file']['tmp_name'][$key], 'error' => $_FILES['file']['error'][$key], 'size' => $_FILES['file']['size'][$key]);
                    if (!empty($_FILES['document']['name'])):
                        $rand = rand(0, 99999999);
                        $file_obj = new file();
                        if ($file_obj->uploadDocument('student_document', $_FILES['document'], $rand)):
                            $ObjQuery = new query('student_document');
                            $ObjQuery->Data['file'] = $this->_sanitize($file_obj->makeFileName($_FILES['document']['name'], $rand));
                            $ObjQuery->Data['title'] = $value['title'];
                            $ObjQuery->Data['student_id'] = $student_id;
                            if (isset($value['id'])):
                                $ObjQuery->Data['update_date'] = date('Y-m-d');
                                $ObjQuery->Data['id'] = $value['id'];
                                $ObjQuery->Update();
                            else:
                                $ObjQuery->Data['on_date'] = date('Y-m-d');
                                $ObjQuery->Insert();
                            endif;
                            $admin_user = new admin_session();
                            $admin_user->set_pass_msg(MSG_STUDENT_DOCUMENT_ADDED);
                        endif;
                    endif;
                endif;
            endforeach;
        endif;
        return true;
    }

    /* Create new Student Documents or update existing  */

    function saveDataInsertCase($POST, $student_id) {

        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (!empty($POST)): $st = '0';
            foreach ($POST as $key => $value):
                if (!empty($value['title'])):
                    /* Make Array to file upload */

                    $_FILES['document'] = array();
                    $_FILES['document'] = array('name' => $_FILES['file']['name'][$key], 'type' => $_FILES['file']['type'][$key], 'tmp_name' => $_FILES['file']['tmp_name'][$key], 'error' => $_FILES['file']['error'][$key], 'size' => $_FILES['file']['size'][$key]);
                    if (!empty($_FILES['document']['name'])):
                        $rand = rand(0, 99999999);
                        $file_obj = new file();
                        if ($file_obj->uploadDocument('student_document', $_FILES['document'], $rand)):
                            $ObjQuery = new query('student_document');
                            $ObjQuery->Data['file'] = $this->_sanitize($file_obj->makeFileName($_FILES['document']['name'], $rand));
                            $ObjQuery->Data['title'] = $value['title'];
                            $ObjQuery->Data['student_id'] = $student_id;
                            if (isset($value['id'])):
                                $ObjQuery->Data['update_date'] = date('Y-m-d');
                                $ObjQuery->Data['id'] = $value['id'];
                                $ObjQuery->Update();
                            else:
                                $ObjQuery->Data['on_date'] = date('Y-m-d');
                                $ObjQuery->Insert();
                            endif;
                        endif;
                    endif;
                    $st++;
                endif;
            endforeach;
        endif;
        return true;
    }

    /* Get Staff Student Documents by id */

    function getSDocument($id) {
        return $this->_getObject('student_document', $id);
    }

    /* Get List of all Student  Qualification */

    function getStudentDocument($student_id, $result_type = 'object') {

        $this->Where = "where student_id='" . mysql_real_escape_string($student_id) . "' ORDER BY id asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* delete a Student by id Documents */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

#Student Fee Class

class studentFee extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_fee');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'session_id', 'concession', 'section', 'student_id', 'payment_date', 'total_amount', 'absent_fine', 'vehicle_fee', 'absent_days', 'late_fee', 'month', 'current_year', 'on_date', 'update_date', 'remarks');
    }

    /*
     * Create Student Fee  list or update existing theme
     */

    function saveData($POST) {
        $POST['total_amount'] = str_replace(',', '', $POST['total_amount']);

        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function add_fee_pay_heads_month($POST, $max_id) {
        #add fee heads 
        if (!empty($POST['fee_heads'])):
            #firstly delete all previous heads of this id
            $PayHeadsQuery = new query('student_fee_pay_heads');
            $PayHeadsQuery->Where = "where student_fee_id='" . $max_id . "'";
            $PayHeadsQuery->Delete_where();
            foreach ($POST['fee_heads'] as $key => $value):
                $PayHeads = new query('student_fee_pay_heads');
                $PayHeads->Data['student_fee_id'] = $max_id;
                $PayHeads->Data['pay_heads'] = $key;
                $PayHeads->Data['amount'] = $value;
                $PayHeads->Data['student_id'] = $POST['student_id'];
                $PayHeads->Data['on_date'] = date('Y-m-d');
                $PayHeads->Insert();
            endforeach;
        endif;
        return true;
    }

    function delete_fee_heads_of_entry($id) {
        $PayHeadsQuery = new query('student_fee_pay_heads');
        $PayHeadsQuery->Where = "where student_fee_id='" . $id . "'";
        $PayHeadsQuery->Delete_where();
        return true;
    }

    #previous record of this month fee   

    function previous_record_of_month($session_id, $section, $student_id, $date) {
        $CheckQuery = new query('student_fee');
        $CheckQuery->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and  student_id='" . mysql_real_escape_string($student_id) . "' and month(payment_date)='" . date('m', strtotime($date)) . "' and year(payment_date)='" . date('Y', strtotime($date)) . "' ORDER BY id asc";
        return $CheckQuery->ListOfAllRecords('object');
    }

    #previous record of this month fee   

    function listOfPreviousRecord($session_id, $section, $student_id, $from, $to) {
        $CheckQuery = new query('student_fee');
        $CheckQuery->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and  student_id='" . mysql_real_escape_string($student_id) . "' 
                     AND DATE(payment_date) >= CAST('" . $from . "' AS DATE) AND DATE(payment_date) <= CAST('" . $to . "' AS DATE) ORDER BY on_date desc";
        //$CheckQuery->print=1;
        return $rec = $CheckQuery->ListOfAllRecords('object');
    }

    #previous record of this month fee   

    function listOfAllPreviousRecords($session_id, $section, $student_id, $from, $to) {
        $CheckQuery = new query('student_fee');
        $CheckQuery->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and  student_id='" . mysql_real_escape_string($student_id) . "'
                     ORDER BY on_date desc";
        //$CheckQuery->print=1;
        return $rec = $CheckQuery->ListOfAllRecords('object');
    }

    #previous Total paid   

    function TotalPreviousRecordPaid($session_id, $section, $student_id) {
        $amount = '0';
        $CheckQuery = new query('student_fee');
        $CheckQuery->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and  student_id='" . mysql_real_escape_string($student_id) . "' ORDER BY on_date desc";

        $rec = $CheckQuery->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $val):
                $amount = $amount + $val->total_amount;
            endforeach;
        endif;
        return $amount;
    }

    #previous Total paid   

    function TotalPaidInThisInterval($session_id, $section, $student_id, $from, $to) {
        $amount = '';
        $CheckQuery = new query('student_fee');
        $CheckQuery->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and  student_id='" . mysql_real_escape_string($student_id) . "' 
                     AND DATE(payment_date) >= CAST('" . $from . "' AS DATE) AND DATE(payment_date) <= CAST('" . $to . "' AS DATE) ORDER BY on_date desc";
        $rec = $CheckQuery->ListOfAllRecords('object');

        if (!empty($rec)):
            foreach ($rec as $key => $val):
                $amount = $amount + $val->total_amount;
            endforeach;
        endif;
        return $amount;
    }

    #previous Total paid   

    function TotalPaidInThisIntervalCurrent($session_id, $section, $student_id, $from, $to) {
        $amount = '0';
        $CheckQuery = new query('student_fee');
        $CheckQuery->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and  student_id='" . mysql_real_escape_string($student_id) . "' 
                     AND DATE(payment_date) >= CAST('" . $from . "' AS DATE) AND DATE(payment_date) <= CAST('" . $to . "' AS DATE) ORDER BY on_date desc";
        $rec = $CheckQuery->ListOfAllRecords('object');

        if (!empty($rec)):
            foreach ($rec as $key => $val):
                $amount = $amount + $val->total_amount;
            endforeach;
        endif;
        return $amount;
    }

    function checkVehicleFeePaid($session_id, $section, $student_id, $from, $to) {
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student_id) . "' and payment_date>='" . date(mysql_real_escape_string($from)) . "' and payment_date<='" . date(mysql_real_escape_string($to)) . "' and (vehicle_fee!='' || vehicle_fee IS NOT NULL)";
        $rec = $this->DisplayOne();
        if ($rec):
            return true;
        else:
            return false;
        endif;
    }

    #get student _pending_fee  

    function get_pending_fee_old($student_id, $session_id, $section, $Month_names) {
        $reocrd = array();

        if (!empty($Month_names)):
            foreach ($Month_names as $key => $val):
                $divide = explode('-', $val);
                $month = $divide['0'];
                $year = $divide['1'];

                $CheckQuery = new query('student_fee,student_fee_record');
                $CheckQuery->Field = ('student_fee.id,student_fee.total_amount,(student_fee.total_amount-sum(student_fee_record.amount)) as pending,sum(student_fee_record.amount) as amount');
                $CheckQuery->Where = "where student_fee.session_id='" . mysql_real_escape_string($session_id) . "' and student_fee.section='" . mysql_real_escape_string($section) . "' and student_fee.student_id='" . mysql_real_escape_string($student_id) . "' and student_fee.month='" . mysql_real_escape_string($month) . "' and student_fee.current_year='" . mysql_real_escape_string($year) . "' and student_fee.id=student_fee_record.student_fee_id ORDER BY id asc";
                $rec = $CheckQuery->DisplayOne();
                if (isset($rec) && !empty($rec->id)):
                    if (!empty($rec->pending)):
                        $reocrd[$val] = $rec->pending;
                    endif;
                else:
                    $reocrd[$val] = 'pending';
                endif;
            endforeach;
        endif;

        return $reocrd;
    }

    #get student _pending_fee  

    function get_pending_fee($pay_heads, $FeeInterval, $VehicleFee, $session, $section, $student) {

        $record = array();
        $session_info = get_object('session', $session);
        unset($FeeInterval['0']);
        $FeeInterval = array_reverse($FeeInterval);

        if (!empty($FeeInterval)): $sr_no = '1';
            foreach ($FeeInterval as $key => $val):
                $total = '';
                if (!empty($val)):
                    foreach ($val as $kkey => $vall):
                        $from = $kkey;
                        $to = $vall;
                    endforeach;
                endif;

                #Get late fee 
                $QueryLateFee = new session();
                $LateFee = $QueryLateFee->LateFee($session, $section, $student, $from, $to);

                #get Student Absent Now
                $QurAtten = new attendance();
                $Absent = $QurAtten->getStudentAbsent($session, $section, $student, $from, $to);

                if (!empty($pay_heads)):
                    foreach ($pay_heads as $k => $v):
                        if ($v['type'] != "Regular"):
                            if ($sr_no == '1'):
                                $total = $total + $v['amount'];
                            endif;
                        else:
                            $total = $total + $v['amount'];
                        endif;
                    endforeach;
                    // Add Vehicle fee
                    $total = $total + $VehicleFee;
                endif;


                #Get concession 
                $QueryCon = new studentSession();
                $Concession = $QueryCon->getConsessionAmount($session, $section, $student);
                $total = $total - $Concession;

                $query = new query('student_fee');
                $query->Field = ('SUM(total_amount) as total');
                $query->Where = "where session_id='" . mysql_real_escape_string($session) . "' and student_id='" . mysql_real_escape_string($student) . "' 
                                            AND DATE(payment_date) >= CAST('" . $from . "' AS DATE) AND DATE(payment_date) <= CAST('" . $to . "' AS DATE) order by payment_date desc";
                if ($rec = $query->DisplayOne()):
                    $total = $total - $rec->total;
                else:
                    $total = $total + $LateFee + $Absent * $session_info->absent_fine;
                endif;

                $record[] = array('from' => $from, 'to' => $to, 'amount' => $total);
                $sr_no++;
            endforeach;
        endif;

        return $record;
    }

    #get student _pending_fee  

    function get_pending_fee_for_single($FeeInterval, $pay_heads, $from, $to, $session, $section, $student) {

        $record = array();
        $sr_no = '1';
        $total = '';
        if (!empty($FeeInterval)):
            foreach ($FeeInterval as $key => $val):

                if (!empty($val)):
                    foreach ($val as $kkey => $vall):
                        $from = $kkey;
                        $to = $vall;
                    endforeach;
                endif;


                #get Student vehicle fee
                $QueryStuVehFee = new vehicleStudent();
                $Vehicle_Fee = $QueryStuVehFee->checkVehicleStudentFee($session, $section, $student);

                if (is_object($Vehicle_Fee)):
                    $VehicleFee = $Vehicle_Fee->amount;
                else:
                    $VehicleFee = '';
                endif;

                #Get late fee 
                $QueryLateFee = new session();
                $LateFee = $QueryLateFee->LateFee($session, $section, $student, $from, $to);

                #get Student Absent Now
                $QurAtten = new attendance();
                $Absent = $QurAtten->getStudentAbsent($session, $section, $student, $from, $to);


                if (!empty($pay_heads)):
                    foreach ($pay_heads as $k => $v):
                        if ($v['type'] != "Regular"):
                            if ($sr_no == '1'):
                                $total = $total + $v['amount'];
                            endif;
                        else:
                            $total = $total + $v['amount'];
                        endif;
                    endforeach;
                    // Add Vehicle fee
                    $total = $total + $VehicleFee;
                endif;

                #Get concession 
                $QueryCon = new studentSession();
                $Concession = $QueryCon->getConsessionAmount($session, $section, $student);
                $total = $total - $Concession;

                $query = new query('student_fee');
                $query->Field = ('SUM(total_amount) as total');
                $query->Where = "where session_id='" . mysql_real_escape_string($session) . "' and student_id='" . mysql_real_escape_string($student) . "' 
                                            AND DATE(payment_date) >= CAST('" . $from . "' AS DATE) AND DATE(payment_date) <= CAST('" . $to . "' AS DATE) order by payment_date desc";
                if ($rec = $query->DisplayOne()):
                    $total = $total - $rec->total;
                else:
                    $total = $total + $LateFee + $Absent * $session_info->absent_fine;
                endif;
                $sr_no++;
            endforeach;

        else:


            $total = 0;

        endif;

        return $total;
    }

    #get student _pending_fee  

    function get_fee_with_interval($pay_heads, $FeeInterval, $VehicleFee, $session, $section, $student) {

        $reocrd = array();
        $session_info = get_object('session', $session);
        // $FeeInterval=array_reverse($FeeInterval);

        if (!empty($FeeInterval)): $sr_no = '1';
            foreach ($FeeInterval as $key => $val):
                $total = '';
                if (!empty($val)):
                    foreach ($val as $kkey => $vall):
                        $from = $kkey;
                        $to = $vall;
                    endforeach;
                endif;

                #Get late fee 
                $QueryLateFee = new session();
                $LateFee = $QueryLateFee->LateFee($session, $section, $student, $from, $to);

                #get Student Absent Now
                $QurAtten = new attendance();
                $Absent = $QurAtten->getStudentAbsent($session, $section, $student, $from, $to);


                if (!empty($pay_heads)):
                    foreach ($pay_heads as $k => $v):
                        if ($v['type'] != "Regular"):
                            if ($sr_no == '1'):
                                $total = $total + $v['amount'];
                            endif;
                        else:
                            $total = $total + $v['amount'];
                        endif;
                    endforeach;
                    // Add Vehicle fee
                    $total = $total + $VehicleFee;
                endif;

                #Get concession 
                $QueryCon = new studentSession();
                $Concession = $QueryCon->getConsessionAmount($session, $section, $student);


                $query = new query('student_fee');
                $query->Field = ('SUM(total_amount) as total');
                $query->Where = "where session_id='" . mysql_real_escape_string($session) . "' and student_id='" . mysql_real_escape_string($student) . "' 
                                            AND DATE(payment_date) >= CAST('" . $from . "' AS DATE) AND DATE(payment_date) <= CAST('" . $to . "' AS DATE) order by payment_date desc";
                if ($rec = $query->DisplayOne()):
                    $total = $total;
                    $paid = $rec->total;
                else:
                    $total = $total + $LateFee + $Absent * $session_info->absent_fine;
                    $total = $total - $Concession;
                    $paid = '';
                endif;

                $record[] = array('from' => $from, 'to' => $to, 'amount' => $total, 'paid' => $paid);
                $sr_no++;
            endforeach;
        endif;

        return $record;
    }

#previous pay_heads   

    function check_previous_pay_heads($id) {
        $records = array();
        $CheckQuery = new query('student_fee_pay_heads');
        $CheckQuery->Field = ('student_fee_pay_heads.pay_heads as id');
        $CheckQuery->Where = "where student_fee_id='" . mysql_real_escape_string($id) . "' ORDER BY id asc";
        $rec = $CheckQuery->ListOfAllRecords('object');

        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = $value->id;
            endforeach;
        endif;
        return $records;
    }

#previous pay_heads   

    function check_previous_pay_heads_by_name($id) {
        $records = array();
        $CheckQuery = new query('student_fee,student_fee_pay_heads,fee_type_master');
        $CheckQuery->Field = ('fee_type_master.id as fee_id,fee_type_master.type,fee_type_master.name as title,student_fee_pay_heads.amount');
        $CheckQuery->Where = "where student_fee.id='" . mysql_real_escape_string($id) . "' and student_fee.id=student_fee_pay_heads.student_fee_id and student_fee_pay_heads.pay_heads=fee_type_master.id GROUP By student_fee_pay_heads.pay_heads ORDER BY student_fee.id asc";
        return $records = $CheckQuery->ListOfAllRecords('array');
    }

#previous pay_heads   

    function getLastFeeReceipt($school_id, $prefix, $start) {
        $CheckQuery = new query('student_fee,student');
        $CheckQuery->Field = ('count(*) as total');
        $CheckQuery->Where = "where student.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.student_id=student.id";

        $records = $CheckQuery->DisplayOne();
        if ($records):
            if ($start):
                $records->total = $records->total + $start;
            endif;
            if (!empty($prefix)):
                return $prefix . '-' . ($records->total);
            else:
                return $records->total;
            endif;
        else:
            if ($start == '0'):
                $start = '1';
            endif;
            return $start;
        endif;
    }

    function check_fee_in_interval($session, $section, $student, $from, $to) {
        $query = new query('student_fee');
        $query->Field = ('SUM(total_amount) as total');
        $query->Where = "where session_id='" . mysql_real_escape_string($session) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student) . "' 
            AND DATE(payment_date) >= CAST('" . $from . "' AS DATE) AND DATE(payment_date) <= CAST('" . $to . "' AS DATE) order by payment_date desc";
        if ($rec = $query->DisplayOne()):
            return $rec;
        else:
            return '';
        endif;
    }

    function check_fee_in_intervalTotal($session, $section, $student, $from, $to, $id) {
        $query = new query('student_fee');
        $query->Field = ('SUM(total_amount) as total');
        $query->Where = "where session_id='" . mysql_real_escape_string($session) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student) . "' 
            AND DATE(payment_date) >= CAST('" . $from . "' AS DATE) AND DATE(payment_date) <= CAST('" . $to . "' AS DATE) AND id!='" . mysql_real_escape_string($id) . "' and id<='" . mysql_real_escape_string($id) . "' order by payment_date desc";
        if ($rec = $query->DisplayOne()):
            return $rec->total;
        else:
            return '';
        endif;
    }

    #Get List of all Student Fee list in array

    function listAll($session, $section, $student) {
        $query = new query('session,student_fee');
        $query->Field = ('student_fee.*,session.title as session_name');
        $query->Where = "where student_fee.session_id='" . mysql_real_escape_string($session) . "' and student_fee.section='" . mysql_real_escape_string($section) . "' and student_fee.student_id='" . mysql_real_escape_string($student) . "' and student_fee.session_id=session.id ORDER BY student_fee.payment_date desc";
        return $query->ListOfAllRecords('object');
    }

    #Get List of single Student Fee list in array

    function listFeeSingleSutdent($session, $student) {
        $this->Where = "where session_id='" . mysql_real_escape_string($session) . "' and student_id='" . mysql_real_escape_string($student) . "' order by payment_date desc";
        // $this->print=1;
        return $this->ListOfAllRecords('object');
    }

    function getBalanceAmount($session, $interval, $student) {
        $query = new query('student_session_interval');
        $query->Field = ('SUM(total) as total,SUM(paid) as paid');
        $query->Where = "where session_id='" . mysql_real_escape_string($session) . "' and student_id='" . mysql_real_escape_string($student) . "' and interval_id='" . mysql_real_escape_string($interval) . "' GROUP BY interval_id";
        $rec = $query->DisplayOne();
        if (is_object($rec)):
            return $rec->total - $rec->paid;
        else:
            return '0';
        endif;
    }

    #Get List of all Student Fee list in array

    function listRecordMonth($session, $section, $student, $month, $year) {
        $query = new query('session,student_fee');
        $query->Field = ('student_fee.*,session.title as session_name');
        $query->Where = "where student_fee.session_id='" . mysql_real_escape_string($session) . "' and student_fee.section='" . mysql_real_escape_string($section) . "' and student_fee.student_id='" . mysql_real_escape_string($student) . "' and month='" . $month . "' and current_year='" . $year . "'  and student_fee.session_id=session.id ORDER BY student_fee.payment_date desc";
        return $query->DisplayOne();
    }

    #Check the previous payment for this month

    function checkMonthFeeUpdate($session_id, $section, $student, $date, $id) {
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and  id!='" . mysql_real_escape_string($id) . "' and student_id='" . mysql_real_escape_string($student) . "' and month(payment_date)='" . date('m', strtotime($date)) . "' and year(payment_date)='" . date('Y', strtotime($date)) . "' ORDER BY id asc";
        return $this->DisplayOne();
    }

    function getRecord($id) {
        return $this->_getObject('student_fee', $id);
    }

    /*
     * delete a Student Fee list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

#Student Fee Class

class studentSessionInterval extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_session_interval');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'session_id', 'section', 'student_id', 'interval_id', 'total', 'paid', 'vehicle_fee', 'concession', 'late_fee', 'other_fee_name', 'other_fee_type', 'other_fee', 'absent_fine', 'heads', 'on_date', 'update_date');
    }

    function AddCompleteSessionFee($pay_heads, $POST) {
        $session_id = $POST['session_id'];
        $student_id = $POST['student_id'];
        $ct_sect = $POST['section'];

        $regular = '';
        $other = '';
        $regular_heads = array();
        $other_heads = array();
        $obj = new studentSession;
        $check_concession = $obj->getConcession($session_id, $ct_sect, $student_id);
        if ($check_concession) {
            if ($check_concession->type == 'regular') {
                $regular = $regular + $check_concession->amount;
                $regular_heads[] = array('title' => $check_concession->title, 'type' => $check_concession->type, 'amount' => $check_concession->amount);
            } else {
                $other = $other + $check_concession->amount;
                $other_heads[] = array('title' => $check_concession->title, 'type' => $check_concession->type, 'amount' => $check_concession->amount);
            }
        }
        if (!empty($pay_heads)):
            foreach ($pay_heads as $key => $value):
                if ($value['type'] == 'Regular'):
                    $regular = $regular + $value['amount'];
                    $regular_heads[] = array('title' => $value['title'], 'type' => $value['type'], 'amount' => $value['amount']);
                else:
                    $other = $other + $value['amount'];
                    $other_heads[] = array('title' => $value['title'], 'type' => $value['type'], 'amount' => $value['amount']);
                endif;
            endforeach;
        endif;

        #get Session Interval
        $QueryInt = new session_interval();
        $interval = $QueryInt->listOfInterval($POST['session_id']);

        #get Student Info
        $QueryStu = new studentSession();
        $student_info = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sect);

        #concession
        $concession = $student_info->concession_type;

        if (!empty($interval)):
            foreach ($interval as $i_k => $i_v):
                $heads = array();

                if ($i_v->interval_position == '1'):
                    $total = $regular + $other;
                    $heads = array_merge($other_heads, $regular_heads);
                else:
                    $total = $regular;
                    if (empty($regular_heads)):
                        $regular_heads[] = array('title' => 'No Fee', 'type' => 'regular', 'amount' => '0');
                    endif;
                    $heads = $regular_heads;
                endif;


                $ObjQuery = new query('student_session_interval');
                $ObjQuery->Where = "where session_id='" . $session_id . "' and student_id='" . $student_id . "' and section='" . $ct_sect . "' and interval_id='" . $i_v->id . "'";
                $rec = $ObjQuery->DisplayOne();

                $ObjQuery = new query('student_session_interval');
                $ObjQuery->Data['session_id'] = $session_id;
                $ObjQuery->Data['student_id'] = $student_id;
                $ObjQuery->Data['interval_id'] = $i_v->id;
                $ObjQuery->Data['concession'] = $concession;

                $ObjQuery->Data['total'] = ($total - $concession);
                $ObjQuery->Data['heads'] = serialize($heads);
                if ($rec):
                    $ObjQuery->Data['update_date'] = date('Y-m-d');
                    $ObjQuery->Data['id'] = $rec->id;
                    $ObjQuery->Update();
                else:
                    $ObjQuery->Data['on_date'] = date('Y-m-d');
                    $ObjQuery->Insert();
                endif;
            endforeach;
        endif;
        return true;
    }

    function UpdateCurrentAndFutureInterval($interval, $pay_heads, $POST) {
        $session_id = $POST['session_id'];
        $student_id = $POST['student_id'];
        $ct_sect = $POST['section'];

        $regular = '';
        $other = '';
        $heads = array();
        $regular_heads = array();
        $other_heads = array();
        $obj = new studentSession;
        $check_concession = $obj->getConcession($session_id, $ct_sect, $student_id);
        if ($check_concession) {
            if ($check_concession->type == 'regular') {
                $regular = $regular + $check_concession->amount;
                $regular_heads[] = array('title' => $check_concession->title, 'type' => $check_concession->type, 'amount' => $check_concession->amount);
            } else {
                $other = $other + $check_concession->amount;
                $other_heads[] = array('title' => $check_concession->title, 'type' => $check_concession->type, 'amount' => $check_concession->amount);
            }
        }
        if (!empty($pay_heads)):
            foreach ($pay_heads as $key => $value):
                if ($value['type'] == 'Regular'):
                    $regular = $regular + $value['amount'];
                    $regular_heads[] = array('title' => $value['title'], 'type' => $value['type'], 'amount' => $value['amount']);
                else:
                    $other = $other + $value['amount'];
                    $other_heads[] = array('title' => $value['title'], 'type' => $value['type'], 'amount' => $value['amount']);
                endif;
            endforeach;
        endif;


        #get Student Info
        $QueryStu = new studentSession();
        $student_info = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sect);

        #concession
        $concession = $student_info->concession_type;

        if (!empty($interval)):
            foreach ($interval as $i_k => $i_v):
                $heads = array();
                if (empty($Absent)):
                    $Absent = '0';
                endif;

                if ($i_v->interval_position == '1'):
                    $total = $regular + $other;
                    $heads = array_merge($other_heads, $regular_heads);
                else:
                    $total = $regular;
                    if (empty($regular_heads)):
                        $regular_heads[] = array('title' => 'No Fee', 'type' => 'regular', 'amount' => '0');
                    endif;
                    $heads = $regular_heads;
                endif;



                $ObjQuery = new query('student_session_interval');
                $ObjQuery->Where = "where session_id='" . $session_id . "' and student_id='" . $student_id . "' and interval_id='" . $i_v->id . "'";
                $rec = $ObjQuery->DisplayOne();

                $ObjQuery = new query('student_session_interval');
                $ObjQuery->Data['session_id'] = $session_id;
                $ObjQuery->Data['student_id'] = $student_id;
                $ObjQuery->Data['interval_id'] = $i_v->id;
                $ObjQuery->Data['concession'] = $concession;

                $ObjQuery->Data['heads'] = serialize($heads);

                if ($rec):
                    $total = ($total + $rec->vehicle_fee + $rec->late_fee + $rec->absent_fine) - $concession;
                    if (!empty($rec->other_fee)):
                        if ($rec->other_fee_type == '+'):
                            $total = $total + $rec->other_fee;
                        elseif ($rec->other_fee_type == '-'):
                            $total = $total - $rec->other_fee;
                        endif;
                    endif;

                    $ObjQuery->Data['id'] = $rec->id;
                    $ObjQuery->Data['update_date'] = date('Y-m-d');
                    $ObjQuery->Data['total'] = $total;
                    $ObjQuery->Update();
                else:
                    $ObjQuery->Data['on_date'] = date('Y-m-d');
                    $ObjQuery->Data['total'] = ($total - $concession);
                    $ObjQuery->Insert();
                endif;
            endforeach;
        endif;

        return true;
    }

    function CheckPreviousFeeRecord($session_id, $ct_sect, $student_id, $interval_id, $pay_heads) {
        $regular = '';
        $other = '';
        $regular_heads = array();
        $other_heads = array();
        $obj = new studentSession;
        $check_concession = $obj->getConcession($session_id, $ct_sect, $student_id);
        if ($check_concession) {
            if ($check_concession->type == 'regular') {
                $regular = $regular + $check_concession->amount;
                $regular_heads[] = array('title' => $check_concession->title, 'type' => $check_concession->type, 'amount' => $check_concession->amount);
            } else {
                $other = $other + $check_concession->amount;
                $other_heads[] = array('title' => $check_concession->title, 'type' => $check_concession->type, 'amount' => $check_concession->amount);
            }
        }
        if (!empty($pay_heads)):
            foreach ($pay_heads as $key => $value):
                if ($value['type'] == 'Regular'):
                    $regular = $regular + $value['amount'];
                    $regular_heads[] = array('title' => $value['title'], 'type' => $value['type'], 'amount' => $value['amount']);
                else:
                    $other = $other + $value['amount'];
                    $other_heads[] = array('title' => $value['title'], 'type' => $value['type'], 'amount' => $value['amount']);
                endif;
            endforeach;
        endif;

        #get Session Interval
        $QueryInt = new session_interval();
        $interval = $QueryInt->listOfInterval($session_id);

        $current_date = date('Y-m-d');
        $current = '0';


        #get Student Info
        $QueryStu = new studentSession();
        $student_info = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sect);

        #concession
        $concession = $student_info->concession_type;

        #session info
        $session = get_object('session', $session_id);

        $Absent = '0';

        if (!empty($interval)):
            foreach ($interval as $i_k => $i_v):
                $heads = array();
                if (empty($Absent)):
                    $Absent = '0';
                endif;

                if ($i_v->interval_position == '1'):
                    $total = $regular + $other;
                    $heads = array_merge($other_heads, $regular_heads);
                else:
                    $total = $regular;
                    if (empty($regular_heads)):
                        $regular_heads[] = array('title' => 'No Fee', 'type' => 'regular', 'amount' => '0');
                    endif;
                    $heads = $regular_heads;
                endif;

                #Get late fee 
                $QueryLateFee = new session();
                $LateFee = $QueryLateFee->LateFee($session_id, $ct_sect, $student_id, $i_v->from_date, $i_v->to_date);

                #get Student vehicle fee
                $QueryStuVehFee = new vehicleStudent();
                $Vehicle_Fee = $QueryStuVehFee->checkVehicleStudentFeeInInterval($session_id, $ct_sect, $student_id, $i_v->from_date, $i_v->to_date);

                if ((strtotime($current_date) >= strtotime($i_v->from_date)) && (strtotime($current_date) <= strtotime($i_v->to_date))):
                    $current = '1';
                endif;
                $ObjQuery = new query('student_session_interval');
                $ObjQuery->Where = "where session_id='" . $session_id . "' and student_id='" . $student_id . "' and interval_id='" . $i_v->id . "'";
                $rec = $ObjQuery->DisplayOne();
                if ($rec):

                else:

                    $ObjQuery = new query('student_session_interval');
                    $ObjQuery->Data['session_id'] = $session_id;
                    $ObjQuery->Data['student_id'] = $student_id;
                    $ObjQuery->Data['interval_id'] = $i_v->id;
                    $ObjQuery->Data['section'] = $ct_sect;
                    $ObjQuery->Data['vehicle_fee'] = $Vehicle_Fee;
                    $ObjQuery->Data['late_fee'] = $LateFee;
                    $ObjQuery->Data['concession'] = $concession;
                    $ObjQuery->Data['absent_fine'] = $Absent * $session->absent_fine;
                    $ObjQuery->Data['total'] = (($total + $Vehicle_Fee + $LateFee + $Absent * $session->absent_fine) - $concession);
                    $ObjQuery->Data['on_date'] = date('Y-m-d');
                    $ObjQuery->Data['heads'] = serialize($heads);
                    $ObjQuery->Insert();
                endif;
                if ($current == '1'): break;
                endif;

                #get Student Absent Now
                $QurAtten = new attendance();
                $Absent = $QurAtten->getStudentAbsent($session_id, $ct_sect, $student_id, $i_v->from_date, $i_v->to_date);
            endforeach;
        endif;
    }

    function updateIntervalFee($session_id, $ct_sect, $student_id, $interval_id, $pay_heads) {
        $regular = '';
        $other = '';
        $heads = array();
        $regular_heads = array();
        $other_heads = array();
        $obj = new studentSession;
        $check_concession = $obj->getConcession($session_id, $ct_sect, $student_id);
        if ($check_concession) {
            if ($check_concession->type == 'regular') {
                $regular = $regular + $check_concession->amount;
                $regular_heads[] = array('title' => $check_concession->title, 'type' => $check_concession->type, 'amount' => $check_concession->amount);
            } else {
                $other = $other + $check_concession->amount;
                $other_heads[] = array('title' => $check_concession->title, 'type' => $check_concession->type, 'amount' => $check_concession->amount);
            }
        }
        if (!empty($pay_heads)):
            foreach ($pay_heads as $key => $value):
                if ($value['type'] == 'Regular'):
                    $regular = $regular + $value['amount'];
                    $regular_heads[] = array('title' => $value['title'], 'type' => $value['type'], 'amount' => $value['amount']);
                else:
                    $other = $other + $value['amount'];
                    $other_heads[] = array('title' => $value['title'], 'type' => $value['type'], 'amount' => $value['amount']);
                endif;
            endforeach;
        endif;

        #Get Session Interval In array
        $SessIntOb = new session_interval();
        $interval = $SessIntOb->listOfIntervalInArray($session_id);

        #Get interval detail        
        $SessIntOb = new studentSessionInterval();
        $interval_detail = $SessIntOb->getDetail($session_id, $ct_sect, $student_id, $interval_id);

        #get first date and last list of this interval		
        $from = $interval_detail->from_date;
        $to = $interval_detail->to_date;

        #Get late fee 
        $QueryLateFee = new session();
        $LateFee = $QueryLateFee->LateFee($session_id, $ct_sect, $student_id, $from, $to);

        #get Student vehicle fee
        $QueryStuVehFee = new vehicleStudent();
        $Vehicle_Fee = $QueryStuVehFee->checkVehicleStudentFeeInInterval($session_id, $ct_sect, $student_id, $from, $to);

        #get Student Info
        $QueryStu = new studentSession();
        $student_info = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sect);

        #concession
        $concession = $student_info->concession_type;

        #session info
        $session = get_object('session', $session_id);

        #check the absent fee if not the first fee
        if ($interval_detail->interval_position == '1'):
            $Absent = '0';
            $total = $regular + $other;
            $heads = array_merge($other_heads, $regular_heads);
        else:
            foreach ($interval as $ik => $iv):
                if ($iv->id == $interval_id):
                    break;
                endif;
                $atten_from = $iv->from_date;
                $atten_to = $iv->to_date;
            endforeach;

            #get Student Absent Now for previous date
            $QurAtten = new attendance();
            $Absent = $QurAtten->getStudentAbsent($session_id, $ct_sect, $student_id, $atten_from, $atten_to);

            $total = $regular;
            if (empty($regular_heads)):
                $regular_heads[] = array('title' => 'No Fee', 'type' => 'regular', 'amount' => '0');
            endif;
            $heads = $regular_heads;
        endif;

        $total = ($total + $Vehicle_Fee + $Absent * $session->absent_fine) - $concession;
        /* now update */
        $ObjQuery = new query('student_session_interval');
        $ObjQuery->Data['session_id'] = $session_id;
        $ObjQuery->Data['student_id'] = $student_id;
        $ObjQuery->Data['interval_id'] = $interval_id;
        $ObjQuery->Data['vehicle_fee'] = $Vehicle_Fee;
        if (!empty($interval_detail->late_fee)):
            $ObjQuery->Data['late_fee'] = $interval_detail->late_fee;
            $total = $total + $interval_detail->late_fee;
        endif;

        if (!empty($interval_detail->other_fee)):
            if ($interval_detail->other_fee_type == '+'):
                $total = $total + $interval_detail->other_fee;
            elseif ($interval_detail->other_fee_type == '-'):
                $total = $total - $interval_detail->other_fee;
            endif;
        endif;

        $ObjQuery->Data['concession'] = $concession;
        $ObjQuery->Data['absent_fine'] = $Absent * $session->absent_fine;
        $ObjQuery->Data['total'] = $total;
        $ObjQuery->Data['update_date'] = date('Y-m-d');
        $ObjQuery->Data['id'] = $interval_detail->id;
        $ObjQuery->Data['heads'] = serialize($heads);
        $ObjQuery->Update();

        return true;
    }

    function getPreviousBalanceFee($session_id, $ct_sect, $student_id, $interval_id) {
        $sum = '';
        $ObjQuery = new query('session_interval,student_session_interval');
        $ObjQuery->Field = ('session_interval.id,session_interval.session_id as session_id,student_session_interval.*');
        $ObjQuery->Where = "where session_interval.session_id='" . $session_id . "' and session_interval.id<'" . $interval_id . "' and student_session_interval.student_id='" . $student_id . "' and session_interval.id=student_session_interval.interval_id";
        // $ObjQuery->print=1;
        $rec = $ObjQuery->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $val):
                $sum = $sum + ($val->total - $val->paid);
            endforeach;
        endif;
        return $sum;
    }

    function getPreviousSessionBalance($session_id, $interval_id) {
        $sum = array('total' => '', 'paid' => '');
        $ObjQuery = new query('student_session_interval');
        $ObjQuery->Field = ('SUM(total) as total,SUM(paid) as paid');
        $ObjQuery->Where = "where session_id='" . $session_id . "' and interval_id='" . $interval_id . "' group by session_id";
        $rec = $ObjQuery->DisplayOne();
        if (!empty($rec)):
            $sum['total'] = $rec->total;
            $sum['paid'] = $rec->paid;
        endif;
        return $sum;
    }

    function getOnlyPreviousSessionBalance($session_id, $student_id) {

        $sum = array();
        $ObjQuery = new query('student_session_interval');
        $ObjQuery->Field = ('SUM(total) as total,SUM(paid) as paid,MAX(interval_id) as int_id');
        $ObjQuery->Where = "where session_id='" . $session_id . "' AND student_id='" . $student_id . "' group by session_id";

        $rec = $ObjQuery->DisplayOne();
        if (!empty($rec)):
            $sum['session_id'] = $session_id;
            $sum['interval_id'] = $rec->int_id;
            $sum['sum'] = $rec->total - $rec->paid;
        endif;
        return $sum;
    }

    function getDetail($session_id, $ct_sect, $student_id, $interval_id) {
        $ObjQuery = new query('session_interval,student_session_interval');
        $ObjQuery->Field = ('student_session_interval.*,session_interval.interval_position,session_interval.from_date,session_interval.to_date');
        $ObjQuery->Where = "where session_interval.session_id='" . $session_id . "' and session_interval.id='" . $interval_id . "' and student_session_interval.student_id='" . $student_id . "' and session_interval.id=student_session_interval.interval_id";
        //$ObjQuery->print=1;
        return $rec = $ObjQuery->DisplayOne();
    }

    function getEndInterval($session_id) {
        $ObjQuery = new query('student_session_interval');
        $ObjQuery->Where = "where session_id='" . $session_id . "' order by interval_id desc";
        $rec = $ObjQuery->DisplayOne();
        if ($rec):
            return $rec->interval_id;
        else:
            return '';
        endif;
    }

    function updatePaymentData($SUBMITION, $id) {

        $detail = get_object('student_session_interval', $id);
        $ObjQuery = new query('student_session_interval');
        $ObjQuery->Data['paid'] = $detail->paid + str_replace(',', '', $SUBMITION['total_amount']);
        $ObjQuery->Data['vehicle_fee'] = $SUBMITION['vehicle_fee'];
        $ObjQuery->Data['concession'] = $SUBMITION['concession'];
        $ObjQuery->Data['absent_fine'] = $SUBMITION['absent_fine'];
        if (!empty($SUBMITION['other_fee']) && is_numeric($SUBMITION['other_fee'])):
            #here is the extra field
            if ($SUBMITION['other_fee_type'] == "-"):
                $total = ($detail->total - $SUBMITION['other_fee']);
            elseif ($SUBMITION['other_fee_type'] == "+"):
                $total = ($detail->total + $SUBMITION['other_fee']);
            endif;
            $ObjQuery->Data['total'] = $total;
            $ObjQuery->Data['other_fee_type'] = $SUBMITION['other_fee_type'];
            $ObjQuery->Data['other_fee'] = $SUBMITION['other_fee'];
            $ObjQuery->Data['other_fee_name'] = $SUBMITION['other_fee_name'];
        else:
            $ObjQuery->Data['total'] = $detail->total + '0';
            $ObjQuery->Data['other_fee'] = '';
            $ObjQuery->Data['other_fee_name'] = '';
            $ObjQuery->Data['other_fee_name'] = '';
        endif;
        $ObjQuery->Data['id'] = $id;

        $ObjQuery->Update();
    }

    function updateInDelete($amount, $id) {
        $detail = get_object('student_session_interval', $id);
        $ObjQuery = new query('student_session_interval');
        $ObjQuery->Data['paid'] = $detail->paid - str_replace(',', '', $amount);
        $ObjQuery->Data['id'] = $id;
        $ObjQuery->Update();
    }

    function addPaidAmount($amount, $id) {
        $detail = get_object('student_session_interval', $id);
        $ObjQuery = new query('student_session_interval');
        $ObjQuery->Data['paid'] = $detail->paid + str_replace(',', '', $amount);
        $ObjQuery->Data['id'] = $id;
        $ObjQuery->Update();
    }

    function updateTotalPaidFeeToZero($id) {
        $detail = get_object('student_session_interval', $id);
        $ObjQuery = new query('student_session_interval');
        $ObjQuery->Data['paid'] = '0';
        $ObjQuery->Data['id'] = $id;
        $ObjQuery->Update();
    }

    function checkIntervaFeeId($interval_id, $session_id) {
        $this->Where = "where interval_id='" . mysql_real_escape_string($interval_id) . "' AND session_id='" . mysql_real_escape_string($session_id) . "' group by interval_id";
        $record = $this->DisplayOne();
        if ($record):
            return $record;
        else:
            return false;
        endif;
    }

}

#Student Fee Class

class studentFeeRecord extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_fee_record');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'student_fee_id', 'fee_type', 'amount', 'remarks', 'on_date', 'update_date');
    }

    /*
     * Create Student Fee  record or update existing theme
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['on_date'] = date('Y-m-d');
        $this->Insert();
        return $this->GetMaxId();
    }

    function saveFeeRecords($POST, $fee_id) {
        if (!empty($POST)):
            foreach ($POST as $key => $value):
                $ObjQuery = new query('student_fee_record');
                $ObjQuery->Data['student_fee_id'] = $fee_id;
                $ObjQuery->Data['fee_type'] = mysql_real_escape_string($key);
                $ObjQuery->Data['amount'] = $value;
                $ObjQuery->Data['on_date'] = date('Y-m-d');
                $ObjQuery->Insert();
            endforeach;
        endif;
        return true;
    }

    function UpdateData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    #Get List of all Student Fee list 

    function listAll($id) {
        $this->Where = "where student_fee_id='" . mysql_real_escape_string($id) . "' order by id asc";
        //$this->print = 1;

        return $this->ListOfAllRecords('object');
    }

    function sum_amount($id) {
        $this->Field = ('sum(amount) as amount');
        $this->Where = "where student_fee_id='" . mysql_real_escape_string($id) . "'";
        $rec = $this->DisplayOne();
        if ($rec):
            return $rec->amount;
        else:
            return '0';
        endif;
    }

    #Get List of single Student Fee list in array

    function listReport($school_id, $from, $to, $session_id, $section) {

        $query = new query('session,student_fee,student,student_family_details');
        $query->Field = ('student_fee.student_id,student.reg_id,student.first_name,student.last_name,student_family_details.father_name,student_fee.section,student_fee.id as student_fee_id,student_fee.month,student_fee.payment_date,session.title as session_name,student_fee.total_amount as amount');
        if (!empty($session_id) && !empty($section)):
            $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.session_id='" . mysql_real_escape_string($session_id) . "' and student_fee.section='" . mysql_real_escape_string($section) . "' and student_fee.payment_date>='" . mysql_real_escape_string($from) . "' and student_fee.payment_date<='" . mysql_real_escape_string($to) . "' and student_fee.session_id=session.id and student.id=student_fee.student_id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student_fee.payment_date desc";
        elseif (!empty($session_id) && empty($section)):
            $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.session_id='" . mysql_real_escape_string($session_id) . "' and student_fee.payment_date>='" . mysql_real_escape_string($from) . "' and student_fee.payment_date<='" . mysql_real_escape_string($to) . "' and student_fee.session_id=session.id and student.id=student_fee.student_id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student_fee.payment_date desc";
        else:
            $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.on_date>='" . mysql_real_escape_string($from) . "' and student_fee.payment_date<='" . mysql_real_escape_string($to) . "' and student_fee.session_id=session.id and student.id=student_fee.student_id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student_fee.payment_date desc";
        endif;

        return $query->ListOfAllRecords('object');
    }

    function listReportDownload($school_id, $from, $to, $session_id, $section) {

        $query = new query('session,student_fee,student,student_family_details');
        $query->Field = ('student_fee.student_id,student.reg_id,student.first_name,student.last_name,student_family_details.father_name,student_fee.section,student_fee.id as student_fee_id,student_fee.month,student_fee.payment_date,session.title as session_name,student_fee.total_amount as amount');
        if (!empty($session_id) && !empty($section)):
            $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.session_id='" . mysql_real_escape_string($session_id) . "' and student_fee.section='" . mysql_real_escape_string($section) . "' and student_fee.payment_date>='" . mysql_real_escape_string($from) . "' and student_fee.payment_date<='" . mysql_real_escape_string($to) . "' and student_fee.session_id=session.id and student.id=student_fee.student_id and student_family_details.student_id=student.id ORDER BY student_fee.payment_date desc";
        elseif (!empty($session_id) && empty($section)):
            $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.session_id='" . mysql_real_escape_string($session_id) . "' and student_fee.payment_date>='" . mysql_real_escape_string($from) . "' and student_fee.payment_date<='" . mysql_real_escape_string($to) . "' and student_fee.session_id=session.id and student.id=student_fee.student_id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student_fee.payment_date desc";
        else:
            $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.on_date>='" . mysql_real_escape_string($from) . "' and student_fee.payment_date<='" . mysql_real_escape_string($to) . "' and student_fee.session_id=session.id and student.id=student_fee.student_id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student_fee.payment_date desc";
        endif;
        $record = $query->ListOfAllRecords('object');

        if (!empty($record)): $sr = 1;
            $total = '0';
            $filename = "StudentFeeReport.csv";

            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;filename=$filename");

            if ($session_id): $category = get_object('session', $session_id);
                echo "Session Name " . "\t" . $category->title . "\n";
            endif;
            if ($section): $category = get_object('session', $session_id);
                echo "Section " . "\t" . $section . "\n";
            endif;
            echo "From Date" . "\t" . $from . "\n";
            echo "To Date " . "\t" . $to . "\n \n";
            echo 'Reg ID' . "\t" . 'Name' . "\t" . 'Father Name' . "\t" . 'Session' . "\t" . 'Date' . "\t" . 'Amount' . "\n";

            foreach ($record as $key => $value):
                echo $value->reg_id . ".\t" . $value->first_name . ' ' . $value->last_name . "\t" . $value->father_name . "\t" . $value->session_name . '(' . $value->section . ')' . "\t" . $value->payment_date . "\t" . number_format($value->amount, 2) . "\t\n";
                $total = $total + $value->amount;
                $sr++;
            endforeach;
            echo '' . "\t" . '' . '' . "\t" . '' . "\t" . '' . "\t" . 'Total' . "\t" . '' . number_format($total, 2) . "\n";
            exit;
        endif;
    }

    #Get List of single Student Fee list in array

    function listFeeReport($school_id, $from, $to, $session_id, $section) {

        $query = new query('session,student_fee,student,student_family_details');
        $query->Field = ('student_fee.student_id,student.reg_id,student.first_name,student.last_name,student_family_details.father_name,student_fee.section,student_fee.id as student_fee_id,student_fee.month,student_fee.payment_date,session.title as session_name,student_fee.total_amount as amount');
        if (!empty($session_id) && !empty($section)):
            $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.session_id='" . mysql_real_escape_string($session_id) . "' and student_fee.section='" . mysql_real_escape_string($section) . "' and student_fee.payment_date>='" . mysql_real_escape_string($from) . "' and student_fee.payment_date<='" . mysql_real_escape_string($to) . "' and student_fee.session_id=session.id and student.id=student_fee.student_id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student_fee.payment_date desc";
        elseif (!empty($session_id) && empty($section)):
            $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.session_id='" . mysql_real_escape_string($session_id) . "' and student_fee.payment_date>='" . mysql_real_escape_string($from) . "' and student_fee.payment_date<='" . mysql_real_escape_string($to) . "' and student_fee.session_id=session.id and student.id=student_fee.student_id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student_fee.payment_date desc";
        else:
            $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.on_date>='" . mysql_real_escape_string($from) . "' and student_fee.payment_date<='" . mysql_real_escape_string($to) . "' and student_fee.session_id=session.id and student.id=student_fee.student_id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student_fee.payment_date desc";
        endif;

        return $query->ListOfAllRecords('object');
    }

    function OverAlllistReport($school_id, $from, $to) {

        $query = new query('session,student_fee,student,student_family_details');
        $query->Field = ('SUM(student_fee.total_amount) as amount');
        $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' and student_fee.payment_date>='" . mysql_real_escape_string($from) . "' and student_fee.payment_date<='" . mysql_real_escape_string($to) . "' and student_fee.session_id=session.id and student.id=student_fee.student_id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student_fee.payment_date desc";

        return $query->ListOfAllRecords('object');
    }

    function SingleDayReport($school_id, $to) {
        $query = new query('student_fee as sf');
        $query->Field = ('sf.id,sf.student_id,sf.total_amount,sf.payment_date,ss.title,sf.section,st.reg_id,st.first_name,st.last_name,st_f.father_name');
        $query->Where = " left join session as ss on sf.session_id=ss.id";
        $query->Where.=" left join student as st on st.id=sf.student_id";
        $query->Where.=" left join student_family_details as st_f on st_f.student_id=st.id";
        $query->Where.=" where st.school_id='" . mysql_real_escape_string($school_id) . "' and sf.payment_date='" . mysql_real_escape_string($to) . "' GROUP BY sf.id ORDER BY sf.id";
        //$query->print=1;
        return $query->ListOfAllRecords('object');
    }

    function getRecord($id) {
        return $this->_getObject('student_fee_record', $id);
    }

    /*
     * delete a Student Fee list by id
     */

    function deleteAllRecord($id) {
        $this->Where = "where student_fee_id='" . mysql_real_escape_string($id) . "'";
        return $this->Delete_where();
    }

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    public static function getExtraFee($student_id, $amount, $date) {
        $query = new query('student_session_interval');
        $query->Field = "student_session_interval.other_fee as of,student_session_interval.other_fee_type as oft";
        $query->Where = "where student_id='$student_id' and (on_date='$date' or update_date='$date')";
        //$query->print = 1;
        $data = $query->DisplayOne();
        return is_object($data) ? $data->oft . $data->of : '0';
    }

}

#Student Subject Class

class studentSubject extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_subject');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'session_id', 'student_id', 'subject_id', 'section', 'roll_no', 'type', 'on_date', 'update_date');
    }

    /*
     * Create Student Fee  record or update existing theme
     */

    function saveSubjects($POST) {
        if (!empty($POST['Compulsory'])):
            foreach ($POST['Compulsory'] as $key => $value):
                $QurSub = new query('student_subject');
                $QurSub->Data['session_id'] = $POST['session_id'];
                $QurSub->Data['section'] = $POST['section'];
                $QurSub->Data['student_id'] = $POST['student_id'];
                $QurSub->Data['roll_no'] = $POST['roll_no'];
                $QurSub->Data['subject_id'] = $value;
                $QurSub->Data['type'] = 'Compulsory';
                $QurSub->Data['on_date'] = date('Y-m-d');
                $QurSub->Insert();
            endforeach;
        endif;

        if (!empty($POST['Elective'])):
            foreach ($POST['Elective'] as $key => $value):
                $QurSub = new query('student_subject');
                $QurSub->Data['session_id'] = $POST['session_id'];
                $QurSub->Data['section'] = $POST['section'];
                $QurSub->Data['student_id'] = $POST['student_id'];
                $QurSub->Data['roll_no'] = $POST['roll_no'];
                $QurSub->Data['subject_id'] = $value;
                $QurSub->Data['type'] = 'Elective';
                $QurSub->Data['on_date'] = date('Y-m-d');
                $QurSub->Insert();
            endforeach;
        endif;
        return true;
    }

    function saveSubjectsFromPrevious($student_array, $ct_session) {
        #current Session detail  
        $session = get_object('session', $ct_session);
        $Compulsory = explode(',', $session->compulsory_subjects);

        if (!empty($student_array)):
            foreach ($student_array as $key => $value):
                #get previous section and roll no info
                $P_session = new query('session_students_rel');
                $P_session->Where = "where session_id='" . mysql_real_escape_string($value) . "' and student_id='" . mysql_real_escape_string($key) . "'";
                $previous = $P_session->DisplayOne();


                #delete all subject for the student
                $QuerySub = new studentSubject();
                $QuerySub->DeleteSubjects($ct_session, $key);

                if (!empty($Compulsory)):
                    foreach ($Compulsory as $s_key => $s_value):
                        $QurSub = new query('student_subject');
                        $QurSub->Data['session_id'] = $ct_session;
                        $QurSub->Data['section'] = $previous->section;
                        $QurSub->Data['student_id'] = $key;
                        $QurSub->Data['roll_no'] = $previous->roll_no;
                        $QurSub->Data['subject_id'] = $s_value;
                        $QurSub->Data['type'] = 'Compulsory';
                        $QurSub->Data['on_date'] = date('Y-m-d');
                        $QurSub->Insert();
                    endforeach;
                endif;


            endforeach;
        endif;

        return true;
    }

    function GetSubjectStudents($session, $subject, $section) {
        $query = new query('student_subject,student,session_students_rel,student_family_details');
        $query->Field = ('student_subject.subject_id,student_subject.student_id,student_subject.type,student.id,session_students_rel.id as rel_id,session_students_rel.session_id as session_id,session_students_rel.roll_no as roll_no, session_students_rel.section as section,session_students_rel.concession,session_students_rel.concession_type,student.first_name, student.last_name, student.sex,student.email, student.phone,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where student_subject.session_id='" . mysql_real_escape_string($session) . "' AND session_students_rel.section='" . mysql_real_escape_string($section) . "' AND student_subject.subject_id='" . mysql_real_escape_string($subject) . "' and student_subject.student_id=student.id and session_students_rel.student_id=student.id and session_students_rel.session_id=student_subject.session_id and student.is_active=1 and student.is_deleted=0 and student_family_details.student_id=student.id GROUP BY student.id ORDER BY session_students_rel.section asc";
        return $query->ListOfAllRecords('object');
    }

    function DeleteSubjects($session_id, $student_id) {
        $QurSub = new query('student_subject');
        $QurSub->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and student_id='" . mysql_real_escape_string($student_id) . "'";
        $QurSub->Delete_where();
    }

    function getStudentSubjectArray($session_id, $student_id, $type) {
        $records = array();
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and student_id='" . mysql_real_escape_string($student_id) . "' and type='" . mysql_real_escape_string($type) . "' ORDER BY id asc";
        $rec = $this->ListOfAllRecords('object');

        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = $value->subject_id;
            endforeach;
        endif;
        return $records;
    }

    function getStudentSubjectTitle($session_id, $student_id, $type) {
        $result = '';
        $records = array();
        $Query = new query('student_subject,subject_master');
        $Query->Field = ('student_subject.*,subject_master.name');
        $Query->Where = "where student_subject.session_id='" . mysql_real_escape_string($session_id) . "' and student_subject.student_id='" . mysql_real_escape_string($student_id) . "' and student_subject.type='" . mysql_real_escape_string($type) . "' and subject_master.id=student_subject.subject_id ORDER BY student_subject.id asc";

        $rec = $Query->ListOfAllRecords('object');

        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = $value->name;
            endforeach;
            $result = implode(', ', $records);
        endif;
        return $result;
    }

    function getStudentSubjectTitleInArray($session_id, $student_id, $type) {
        $result = '';
        $records = array();
        $Query = new query('student_subject,subject_master');
        $Query->Field = ('student_subject.*,subject_master.name');
        $Query->Where = "where student_subject.session_id='" . mysql_real_escape_string($session_id) . "' and student_subject.student_id='" . mysql_real_escape_string($student_id) . "' and student_subject.type='" . mysql_real_escape_string($type) . "' and subject_master.id=student_subject.subject_id ORDER BY student_subject.id asc";

        $rec = $Query->ListOfAllRecords('object');

        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = $value->name;
            endforeach;
        endif;
        return $records;
    }

}

#Student Yearly DMC Class

class studentDMC extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_yearly_dmc');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'student_id', 'session_id', 'section', 'type', 'sem', 'title', 'description', 'grade', 'date_add', 'date_upd');
    }

    /*
     * Create Student Yearly DMC Record
     */

    function saveDMCData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['date_add'] = date('Y-m-d');
        $this->Insert();
        return $this->GetMaxId();
    }

    function listDMCData($student_id, $type, $sem) {
        $this->Where = "where student_id='$student_id' and type='$type' and sem='$sem'";
        return $this->ListOfAllRecords('object');
    }

    public static function DMCData($student_id, $type, $sem, $title, $field) {
        $obj = new studentDMC;
        $obj->Field = "$field as field";
        $obj->Where = "where student_id='$student_id' and type='$type' and sem='$sem' and title='$title'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->field : '';
    }

    public static function checkTypeExists($student_id, $type) {
        $obj = new studentDMC;
        $obj->Where = "where student_id='$student_id' and type='$type' order by id desc limit 1";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data : '';
    }

    function listTitles($student_id, $type) {
        $this->Field = "DISTINCT (title) as title";
        $this->Where = "where student_id='$student_id' and type='$type'";
        return $this->ListOfAllRecords('object');
    }

}

#Student Stucked Off students Class

class stuck_off extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('stuck_off_list');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'student_id', 'session_id', 'stuck_off', 'revert', 'stuck_off_date', 'revert_date');
    }

    /*
     * Stuck Off a Student
     */

    function saveStuckOffStudent($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    // List All stucked off students
    function listStuckOffStudents($session_id, $section, $sess_int = '') {
        if ($sess_int != '') {
            $val = explode(',', $sess_int);
            $start = $val[0];
            $end = $val[0];
            $obj = new query('session');
            $obj->Field = ('id');
            $obj->Where = "where start_date='$start' and end_date='$end'";
            $data = $obj->ListOfAllRecords();
            $session_ids_array = array();
            foreach ($data as $sess_data) {
                $session_ids_array[] = $sess_data['id'];
            }
            $session_ids = implode(',', $session_ids_array);
        }
        $query = new query('student,session,session_students_rel,stuck_off_list');
        $query->Field = "student.id as student_id,student.date_of_admission,student.first_name,student.last_name,session.title,stuck_off_list.id,stuck_off_list.stuck_off_date";
        if ($session_id == '0') {
            $query->Where = "where student.id=stuck_off_list.student_id and session.id=stuck_off_list.session_id and session_students_rel.session_id=stuck_off_list.session_id and session_students_rel.session_id=stuck_off_list.session_id and stuck_off_list.stuck_off='1' and stuck_off_list.revert='0' and stuck_off_list.session_id IN($session_ids) group by student.id";
        } elseif ($section == '0') {
            $query->Where = "where student.id=stuck_off_list.student_id and session.id='$session_id' and session_students_rel.session_id=stuck_off_list.session_id and session_students_rel.session_id=stuck_off_list.session_id and session_students_rel.session_id='$session_id' and session.id='$session_id' and stuck_off_list.stuck_off='1' and stuck_off_list.revert='0' group by student.id";
        } else {
            $query->Where = "where student.id=stuck_off_list.student_id and session.id='$session_id' and session_students_rel.session_id=stuck_off_list.session_id and session_students_rel.session_id=stuck_off_list.session_id and session_students_rel.session_id='$session_id' and session.id='$session_id' and session_students_rel.section='$section' and stuck_off_list.stuck_off='1' and stuck_off_list.revert='0' group by student.id";
        }
        return $query->ListOfAllRecords('object');
    }

}

?>