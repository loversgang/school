<?php

class circulars extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('circulars');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'circular_title', 'file', 'on_date', 'description', 'is_deleted', 'is_active');
    }

    /*
     * Create new course or update existing theme
     */

    function saveData($POST, $remove_file = '') {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';
        if ($_FILES['file']['error'] == 0) {
            $name = $_FILES['file']['name'];
            $tmp_name = $_FILES['file']['tmp_name'];
            $filename = rand() . $name;
            $target_dir = DIR_FS_SITE_UPLOAD . 'file/circulars/';
            $target_file = $target_dir . $filename;
            move_uploaded_file($tmp_name, $target_file);
            $this->Data['file'] = $filename;
        }
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($_FILES['file']['error'] == 0) {
                $unlink = unlink(DIR_FS_SITE_UPLOAD . 'file/circulars/' . $remove_file);
            }
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function listCirculars() {
        $this->Where = "ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function listCircularsActive() {
        $this->Where = "WHERE `is_deleted` = 0 ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function listLimitedCirculars() {
        $current_date = date('Y-m-d');
        $this->Where = "WHERE `on_date` >= '$current_date' order by on_date desc limit 5";
        return $this->ListOfAllRecords('object');
    }

    function listNotificationCirculars() {
        $current_date = date('Y-m-d');
        $this->Where = "WHERE `notification` = 0 AND `on_date` >= '$current_date' order by on_date desc limit 5";
        return $this->ListOfAllRecords('object');
    }

    public static function countCirculars() {
        $query = new circulars;
        $current_date = date('Y-m-d');
        $query->Field = "COUNT(*) as count";
        $query->Where = "WHERE `notification` = 0 AND `on_date` >= '$current_date' order by on_date desc limit 5";
        $data = $query->DisplayOne();
        return $data->count;
    }

    public static function checkId($id) {
        $query = new circulars;
        $query->Where = "WHERE `id` = '$id'";
        return $query->DisplayOne();
    }

    function delete_school_sales($id, $removed_file) {
        $this->Where = "WHERE `id` = '$id'";
        $delete = $this->Delete_where();
        if ($delete) {
            unlink(DIR_FS_SITE_UPLOAD . 'file/circulars/' . $removed_file);
        }
    }

//    function delete_school_sales($id) {
//        $this->Where = "WHERE `id` = '$id'";
//        $list = $this->DisplayOne();
//        $delete = $this->Delete_where();
//        if ($delete) {
//            unlink(DIR_FS_SITE_UPLOAD . 'file/circulars/' . $list->file);
//        }
//    }

}

?>