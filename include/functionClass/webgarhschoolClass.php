<?php

class webgarhSchool extends cwebc {
    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('webgarh_school');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id','school_id', 'monthly_fee', 'signup_date','is_paid', 'on_date');

	}

    /*
     * Create new school or update existing school
     */

  

    /*
     * Get school by id
     */
    function getSchool($id){
        return $this->_getObject('school', $id);
    }
    
     #get single school with plan
     function getSchoolWithPlan($id){
			$QueryObj = new query('school, school_subscription');               
			$QueryObj->Field='school.*,school_subscription.subscription_id as subscription_id,school_subscription.id as subscription_record_id,school_subscription.is_active as subscription_active,school_subscription.set_up_fee as set_up_fee,school_subscription.subscription_fee as subscription_fee,school_subscription.payment_type as payment_type';
			$QueryObj->Where="where school.id='".mysql_real_escape_string($id)."' and school.id=school_subscription.school_id  ORDER BY school.id desc";
	 
			return $record=$QueryObj->DisplayOne();
    } 
  
      #get single school with plan
     function getSchoolWithPlanForWebgarh(){
			$record='';
                    $query1="SELECT school.id,school.school_name,school.address1,school.address2,school.create_date,school.is_active,webgarh_school.monthly_fee as webgarh_fee,webgarh_school.is_paid as billing,webgarh_school.id as webgarh_id FROM school,webgarh_school where school.is_deleted=0 and webgarh_school.school_id=school.id order by webgarh_school.signup_date desc"; 
                                    
                    $query2="SELECT school_subscription.school_id, school_subscription.subscription_id as subscription_id,school_subscription.id as subscription_record_id,school_subscription.is_active as subscription_active,school_subscription.set_up_fee as set_up_fee,school_subscription.subscription_fee as subscription_fee,school_subscription.payment_type as payment_type FROM school_subscription where id!=0" ;
                   
                    $query3="SELECT student.school_id as active_school_id,count(*) as active_student FROM student where is_active=1 group by school_id";
                    
                    $query4="SELECT student.school_id as in_active_school_id,count(*) as in_active_student FROM student where is_active=0 group by school_id";
                     
                    $query5="SELECT * FROM (".$query1.") as t1 left join (".$query2.") as t2 ON t1.id = t2.school_id";
                    
                    $query6="SELECT * FROM (".$query5.") as t3 left join (".$query3.") as t4 ON t3.id = t4.active_school_id";
                    
                    $final_query="SELECT * FROM (".$query6.") as t5 left join (".$query4.") as t6 ON t5.id = t6.in_active_school_id";
             
                    $QueryObj = new query();
                    $QueryObj->ExecuteQuery($final_query);
				
						if($QueryObj->GetNumRows()): 
                                        while($object =$QueryObj->GetObjectFromRecord()):
                                            $record[]=$object;
                                        endwhile;                                       
				 
                        endif;
                 return $record;
               
    }  
  
    function endbleDisable($id, $status){
        $this->Data['id']=$id;
        $this->Data['is_paid']=$status;
        $this->Update();
    }
	
	
	
	 function AddSchool($school_id, $amount){
				$this->Data['school_id']=$school_id;
				$this->Data['monthly_fee']=$amount; 
				$this->Data['signup_date']=date('Y-m-d');
				$this->Data['on_date']=date('Y-m-d');
				$this->Insert();	
	}
	
	 function get_total_billing_school(){
			$QueryObj = new query('webgarh_school,school');
			$QueryObj->Field=('count(*) as record');
			$QueryObj->Where="where webgarh_school.is_paid='1' and webgarh_school.school_id=school.id and school.is_deleted='0'";
			$rec=$QueryObj->DisplayOne();
			if($rec):
				return $rec->record;
			else:
				return '0';
			endif;
	 }
	
}


class webgarhPaymentSchool extends cwebc {
    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='id'){
        parent::__construct('webgarh_school_fee');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id','amount', 'month', 'year','on_date', 'fee_made');

	}

    /*
     * Get school by id
     */
    function getSchool($id){
        return $this->_getObject('school', $id);
    }
    
    function getAccountPayment($month,$year){
			$rec=array('total'=>'','amount','monthly_fee'=>'');
            $this->Field=('SUM(amount) as amount,count(*) as record,amount as monthly_fee');
			if(!empty($month) && !empty($month)):
				$this->Where="where month='".$month."' AND year='".$year."'";
			elseif(!empty($month)):
				$this->Where="where month='".$month."' ";
			elseif(!empty($year)):
				$this->Where="where year='".$year."' ";
			endif;		
			
            $record=$this->DisplayOne();
			
            if($record):
                $rec['amount']=$record->amount;
				$rec['total_rec']=$record->record;
				$rec['monthly_fee']=$record->monthly_fee;
            endif;
				return $rec;		
    }
	
	
    function AddMonthlyEntries($month,$year){
		$QueryObj = new query('webgarh_school,school');
		$QueryObj->Field=('webgarh_school.*,school.id as school_table_id');
		$QueryObj->Where="where webgarh_school.is_paid='1' and webgarh_school.school_id=school.id and school.is_deleted='0'";
		$record=$QueryObj->ListOfAllRecords('object');
		
		if(!empty($record)):
			foreach($record as $key=>$val):
				$QueryIns = new query('webgarh_school_fee');
				$QueryIns->Data['month']=$month;
				$QueryIns->Data['year']=$year; 
				$QueryIns->Data['amount']=$val->monthly_fee;
				$QueryIns->Data['on_date']=date('Y-m-d');
				$QueryIns->Insert();
			endforeach;
		endif;
		
		return true;	
    }	
	
	function DeleteMonthlyEntries($month,$year){
		$QueryObj = new query('webgarh_school_fee');
		$QueryObj->Where="where month='".$month."' and year='".$year."'";
		$QueryObj->Delete_where();
		return true;
	}	
}


?>