<?php

class examination extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_examination');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'session_id', 'section', 'teacher_id', 'title', 'exam_type', 'subject_id', 'minimum_marks', 'maximum_marks', 'date_of_examination', 'time_of_examination', 'on_date', 'update_date');
    }

    /*
     * Create new exam list or update existing theme
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get exam list by id
     */

    function getRecord($id) {
        return $this->_getObject('student_examination', $id);
    }

    /*
     * Get List of all exam list in array
     */

    function listAll($school_id, $session_id = 0, $result_type = 'object') {

        $query = new query('student_examination,staff,subject_master,session');
        $query->Field = ('student_examination.id as id,student_examination.school_id as school_id, student_examination.title as title, student_examination.section as section, student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks as maximum_marks,student_examination.date_of_examination as date, student_examination.time_of_examination as time,staff.title as staff_title,staff.first_name as first_name,staff.last_name as last_name,subject_master.name as subject,session.title as session_name');
        if ($session_id):
            $query->Where = "where student_examination.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination.session_id='" . mysql_real_escape_string($session_id) . "' and student_examination.teacher_id=staff.id and student_examination.subject_id=subject_master.id and student_examination.session_id=session.id ORDER BY student_examination.date_of_examination desc";
        else:
            $query->Where = "where student_examination.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination.teacher_id=staff.id and student_examination.subject_id=subject_master.id and student_examination.session_id=session.id ORDER BY student_examination.date_of_examination desc";
        endif;
        return $query->ListOfAllRecords('object');
    }

    /*
     * Get List of all exam list in array
     */

    function listAllWithSection($school_id, $session_id, $section = 0, $result_type = 'object') {

        $query = new query('student_examination,staff,subject_master,session');
        $query->Field = ('student_examination.id as id,student_examination.school_id as school_id, student_examination.title as title, student_examination.section as section, student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks as maximum_marks,student_examination.date_of_examination as date, student_examination.time_of_examination as time,staff.title as staff_title,staff.first_name as first_name,staff.last_name as last_name,subject_master.name as subject,session.title as session_name');
        if ($section):
            $query->Where = "where student_examination.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination.session_id='" . mysql_real_escape_string($session_id) . "' and student_examination.section='" . mysql_real_escape_string($section) . "' and student_examination.teacher_id=staff.id and student_examination.subject_id=subject_master.id and student_examination.session_id=session.id ORDER BY student_examination.date_of_examination desc";
        else:
            $query->Where = "where student_examination.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination.session_id='" . mysql_real_escape_string($session_id) . "' and student_examination.teacher_id=staff.id and student_examination.subject_id=subject_master.id and student_examination.session_id=session.id ORDER BY student_examination.date_of_examination desc";
        endif;
        return $query->ListOfAllRecords('object');
    }

    function getExamNames($exam_ids, $student) {

        $query = new query('student_examination,students_examination_rel,subject_master');
        $query->Field = ('subject_master.name as title');
        $query->Where = "where student_examination.id IN (" . $exam_ids . ") and students_examination_rel.student_id='" . $student . "' and subject_master.id=student_examination.subject_id and students_examination_rel.exam_id=student_examination.id";
        $record = $query->ListOfAllRecords('object');
        if (!empty($record)):
            foreach ($record as $key => $val):
                $rec[] = $val->title;
            endforeach;
            return implode(',', $rec);
        else:
            return "";
        endif;
    }

    function getExamSession($school_id, $show_active = 0, $result_type = 'object') {

        $query = new query('student_examination,session');
        $query->Field = ('session.id as session_id,session.title as session_name');
        $query->Where = "where student_examination.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination.session_id=session.id GROUP BY session_id ORDER BY student_examination.date_of_examination desc";
        return $query->ListOfAllRecords('object');
    }

#exam list with slected session & section

    function listAllSessionSection($school_id, $session, $section, $show_active = 0, $result_type = 'object') {

        $query = new query('student_examination,staff,subject_master');
        $query->Field = ('student_examination.id as id,student_examination.school_id as school_id, student_examination.title as title,student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks as maximum_marks,student_examination.date_of_examination as date, student_examination.time_of_examination as time,staff.title as staff_title,staff.first_name as first_name,staff.last_name as last_name,subject_master.name as subject');
        $query->Where = "where student_examination.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination.session_id='" . mysql_real_escape_string($session) . "' and student_examination.section='" . mysql_real_escape_string($section) . "' and student_examination.teacher_id=staff.id and student_examination.subject_id=subject_master.id ORDER BY student_examination.date_of_examination desc";
        return $query->ListOfAllRecords('object');
    }

#exam list with slected session & section

    function listAllSessionSectionFromDates($school_id, $session, $section, $from, $to) {

        $query = new query('student_examination,staff,subject_master');
        $query->Field = ('student_examination.id as id,student_examination.school_id as school_id, student_examination.title as title,student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks as maximum_marks,student_examination.date_of_examination as date, student_examination.time_of_examination as time,staff.title as staff_title,staff.first_name as first_name,staff.last_name as last_name,subject_master.name as subject');
        $query->Where = "where student_examination.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination.session_id='" . mysql_real_escape_string($session) . "' and student_examination.section='" . mysql_real_escape_string($section) . "' 
                                    AND DATE(student_examination.date_of_examination) >= CAST('" . $from . "' AS DATE ) 
                                    AND DATE(student_examination.date_of_examination) <= CAST('" . $to . "' AS DATE ) and student_examination.teacher_id=staff.id and student_examination.subject_id=subject_master.id ORDER BY student_examination.date_of_examination desc";
        return $query->ListOfAllRecords('object');
    }

#exam list with slected session & section

    function listAllSessionSectionExam($session, $section) {

        $query = new query('student_examination,staff,subject_master');
        $query->Field = ('student_examination.id as id,student_examination.school_id as school_id, student_examination.title as title,student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks as maximum_marks,student_examination.date_of_examination as date, student_examination.time_of_examination as time,staff.title as staff_title,staff.first_name as first_name,staff.last_name as last_name,subject_master.name as subject');
        $query->Where = "where student_examination.session_id='" . mysql_real_escape_string($session) . "' and student_examination.section='" . mysql_real_escape_string($section) . "' and student_examination.teacher_id=staff.id and student_examination.subject_id=subject_master.id ORDER BY student_examination.date_of_examination desc";
        return $query->ListOfAllRecords('object');
    }

#Get List of all exam result

    function examination_result($id) {

        $query = new query('student,student_examination,students_examination_rel,student_family_details,subject_master');
        $query->Field = ('student.id as student_id, student.reg_id as reg_id,student.first_name as first_name,student.last_name as last_name, students_examination_rel.roll_no as roll_no,subject_master.name as subject, students_examination_rel.marks_obtained as marks_obtained, student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where student_examination.id='" . mysql_real_escape_string($id) . "' and student_examination.id=students_examination_rel.exam_id and student_examination.subject_id=subject_master.id and students_examination_rel.student_id=student.id and student_family_details.student_id=student.id and student.is_active='1' GROUP BY student.id ORDER BY student.id asc";
        return $query->ListOfAllRecords('object');
    }

#Get List of all exam result

    function examinationResultReport($id, $session, $section) {
        $query = new query('student,student_examination,students_examination_rel,student_family_details,subject_master');
        $query->Field = ('student.id as student_id, student.reg_id as reg_id,student.first_name as first_name,student.last_name as last_name, students_examination_rel.roll_no as roll_no,subject_master.name as subject, students_examination_rel.marks_obtained as marks_obtained, student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where student_examination.id='" . mysql_real_escape_string($id) . "' and student_examination.id=students_examination_rel.exam_id and student_examination.subject_id=subject_master.id and students_examination_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student.id asc";
        $record = $query->ListOfAllRecords('object');
        if (!empty($record)):
            $session = get_object('session', $session);
            $filename = "examination_report.csv";

            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;filename=$filename");

            echo "Examination Report" . "\n\n";
            echo "Session Name" . "\t" . $session->title . "\n";
            if (!empty($section)):
                echo "Section" . "\t" . $section . "\n";
            endif;
            echo "\n";
            echo 'Roll No.' . "\t" . 'Student Name' . "\t" . 'Father Name' . "\t" . 'Subject' . "\t" . 'Minimum Marks' . "\t" . 'Marks Obtained' . "\t" . 'Maximum Marks' . "\n";

            foreach ($record as $key => $value):
                echo $value->roll_no . "\t" . ucfirst($value->first_name) . " " . $value->last_name . "\t" . $value->father_name . "\t" . $value->subject . "\t" . $value->minimum_marks . "\t" . $value->marks_obtained . "\t" . $value->maximum_marks . "\t" . "\t\n";
            endforeach;
            exit;
        endif;
    }

#Get single student exam result

    function single_student_examination_result($id, $st_id) {

        $query = new query('student,student_examination,students_examination_rel');
        $query->Field = ('student.id as student_id, student.reg_id as reg_id,student.first_name as first_name,student.last_name as last_name, students_examination_rel.roll_no as roll_no, students_examination_rel.marks_obtained as marks_obtained, student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks');
        $query->Where = "where student.id='" . mysql_real_escape_string($st_id) . "' and student_examination.id='" . mysql_real_escape_string($id) . "' and student_examination.id=students_examination_rel.exam_id and students_examination_rel.student_id=student.id GROUP BY student.id ORDER BY student.id asc";
        return $query->DisplayOne();
    }

#Get single student exam result

    function single_student_all_examination_result($exam_ids, $st_id) {

        $query = new query('student,student_examination,students_examination_rel,subject_master');
        $query->Field = ('student.id as student_id, student.reg_id as reg_id,student.first_name as first_name,student.last_name as last_name, students_examination_rel.roll_no as roll_no,student_examination.id as exam_id,student_examination.title, students_examination_rel.marks_obtained as marks_obtained, student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks,subject_master.id as subject_id,subject_master.name as subject');
        $query->Where = "where student.id='" . mysql_real_escape_string($st_id) . "' and student_examination.id IN(" . mysql_real_escape_string($exam_ids) . ") and student_examination.id=students_examination_rel.exam_id and subject_master.id=student_examination.subject_id and students_examination_rel.student_id=student.id ORDER BY student.id asc";
        return $query->ListOfAllRecords('object');
    }

#Get single student exam result

    function studentAllExam($student_id, $session_id = 0) {

        $query = new query('student_examination,students_examination_rel,subject_master,session');
        $query->Field = ('session.title as session_name,student_examination.section,students_examination_rel.student_id,students_examination_rel.roll_no as roll_no,student_examination.title,student_examination.date_of_examination ,students_examination_rel.marks_obtained as marks_obtained, student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks,subject_master.name as subject');
        if ($session_id):
            $query->Where = "where students_examination_rel.student_id='" . mysql_real_escape_string($student_id) . "' and student_examination.session_id='" . mysql_real_escape_string($session_id) . "' and student_examination.id=students_examination_rel.exam_id and subject_master.id=student_examination.subject_id and session.id=student_examination.session_id ORDER BY student_examination.date_of_examination desc";
        else:
            $query->Where = "where students_examination_rel.student_id='" . mysql_real_escape_string($student_id) . "' and student_examination.id=students_examination_rel.exam_id and subject_master.id=student_examination.subject_id and session.id=student_examination.session_id ORDER BY student_examination.date_of_examination desc";
        endif;
        return $query->ListOfAllRecords('object');
    }

    /* Get List section Students */

    function sectionSessionStudentsExam($id, $section, $exam_id) {
        /*
          $query=new query('student,session_students_rel,student_examination');
          $query->Field=('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks as maximum_marks');
          $query->Where="where session_students_rel.session_id='".mysql_real_escape_string($id)."' and session_students_rel.section='".mysql_real_escape_string($section)."' and session_students_rel.student_id=student.id and student_examination.session_id=session_students_rel.session_id and student_examination.section=session_students_rel.section ORDER BY student.id asc";
         */
        $query = new query('student,session_students_rel,student_examination,student_family_details');
        $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session_students_rel.roll_no as roll_no,student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks as maximum_marks,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and student_examination.id='" . mysql_real_escape_string($exam_id) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student.id asc";
        return $query->ListOfAllRecords('object');
    }

    /*
     * delete a exam list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /*
     * Update exam list position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    function examination_titles($ids) {
        $records = array();
        $this->Field = ('title');
        $this->Where = "where id IN(" . $ids . ")  ORDER BY id asc";
        $rec = $this->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = $value->title;
            endforeach;
        endif;
        return $records;
    }

    function groupExaminations($ids) {
        $ObjUpdate = new query('student_examination,subject_master');
        $ObjUpdate->Field = ('student_examination.*,subject_master.name as subject');
        $ObjUpdate->Where = "where student_examination.id IN(" . $ids . ") and student_examination.subject_id=subject_master.id ORDER BY student_examination.id asc";
        return $ObjUpdate->ListOfAllRecords('object');
    }

    function getUniqueExamDetails($exam_ids) {
        $this->Field = "DISTINCT title";
        $this->Where = "where id IN ($exam_ids)";
        return $this->ListOfAllRecords('object');
    }

    function getExamDetails($exam_ids) {
        $this->Where = "where id IN ($exam_ids)";
        return $this->ListOfAllRecords('object');
    }

    public static function getMaxMarks($exam_title, $school_id, $session_id, $section) {
        $obj = new examination;
        $obj->Field = "MAX(maximum_marks) as max_marks";
        $obj->Where = "where title='$exam_title' and school_id='$school_id' and session_id='$session_id' and section='$section'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data->max_marks : '';
    }

    public static function getMarksObtained($subject_id, $exam_type, $student_id, $session_id, $section) {
        $query = new query('student_examination,students_examination_rel');
        $query->Field = "student_examination.maximum_marks as max_marks,students_examination_rel.marks_obtained as marks";
        $query->Where = "where student_examination.session_id='$session_id' and student_examination.section='$section' and student_examination.subject_id='$subject_id' and student_examination.exam_type='$exam_type' and student_examination.id=students_examination_rel.exam_id and students_examination_rel.student_id='$student_id'";
        $data = $query->DisplayOne();
        $temp = new stdClass();
        $temp->marks = 0;
        $temp->max_marks = 0;
        return is_object($data) ? $data : $temp;
    }

    public static function getTotalMarksObtained($exam_title, $student_id, $session_id, $section) {
        $query = new query('student_examination,students_examination_rel');
        $query->Field = "students_examination_rel.marks_obtained as marks";
        $query->Where = "where student_examination.session_id='$session_id' and student_examination.section='$section' and student_examination.title='$exam_title' and student_examination.id=students_examination_rel.exam_id and students_examination_rel.student_id='$student_id'";
//        $query->print=1;
        return $query->ListOfAllRecords();
    }

    function getSubjectMarks($exam_ids, $student) {
        $query = new query('student_examination,students_examination_rel,subject_master');
        $query->Field = ('student_examination.id as exam_id,student_examination.title as exam_title,student_examination.subject_id as subject_id, subject_master.name as title,students_examination_rel.marks_obtained as marks_obtained');
        $query->Where = "where student_examination.id IN (" . $exam_ids . ") and students_examination_rel.student_id='" . $student . "' and subject_master.id=student_examination.subject_id and students_examination_rel.exam_id=student_examination.id";
//$query->print=1;
        $record = $query->ListOfAllRecords('object');
        return $record;
    }

    public static function getExamniationGrade($student_id, $exam_id, $min_marks = 0, $max_marks = 0, $subject_id, $school_id) {
        $query = new query('student_examination');
        $query->Where = "where id='$exam_id' and subject_id='$subject_id'";
        $data = $query->DisplayOne();
        if (is_object($data)) {
            $query = new query('students_examination_rel');
            $query->Field = "marks_obtained";
            $query->Where = "where student_id='$student_id' and exam_id='$exam_id'";
            $data = $query->DisplayOne();
            $obtain = is_object($data) ? $data->marks_obtained : '--';
            $percentage = number_format(($obtain / $max_marks) * 100, 2) . "%";

            $QueryGrOb = new examGrade();
            $all_grade = $QueryGrOb->listAll($school_id);
            $grade = '--';
            if (!empty($all_grade)):
                foreach ($all_grade as $kg => $kv):
                    if (($percentage >= $kv->minimum) && ($percentage < $kv->maximum)):
                        $grade = $kv->title;
                    endif;
                endforeach;
            else:
                $grade = '--';
            endif;
            return $grade;
        } else {
            return '--';
        }
    }

    public static function claculateGrade($marks_obtained, $max_marks, $school_id) {
        $percentage = number_format(($marks_obtained / $max_marks) * 100, 2) . "%";
        $QueryGrOb = new examGrade();
        $all_grade = $QueryGrOb->listAll($school_id);
        $grade = '--';
        if (!empty($all_grade)):
            foreach ($all_grade as $kg => $kv):
                if (($percentage >= $kv->minimum) && ($percentage < $kv->maximum)):
                    $grade = $kv->title;
                endif;
            endforeach;
        else:
            $grade = '--';
        endif;
        return $grade;
    }

    function getTotalSubjects($session_id, $section) {
        $this->Field = "DISTINCT(subject_id)";
        $this->Where = "where session_id='$session_id' and section='$section'";
//        $this->print = 1;
        $data = $this->ListOfAllRecords('object');
        $subject_ids = array();
        foreach ($data as $subject_id) {
            $subject_ids[] = $subject_id->subject_id;
        }
        if (!empty($subject_ids)) {
            $sub_ids = implode(',', $subject_ids);
            $query = new query('subject_master');
            $query->Where = "where id IN($sub_ids)";
            return $query->ListOfAllRecords('object');
        } else {
            return '';
        }
    }

    public static function calculateTotal($exam_type, $student_id, $session_id, $section, $default_percent) {
        $obj = new examination;
        $obj->Field = "DISTINCT(subject_id)";
        $obj->Where = "where session_id='$session_id' and section='$section'";
        $data_sub = $obj->ListOfAllRecords();
        $subject_ids = array();
        foreach ($data_sub as $subject_id) {
            $subject_ids[] = $subject_id['subject_id'];
        }
        $sub_ids = implode(',', $subject_ids);
        $query = new query('student_examination,students_examination_rel');
        $query->Field = "student_examination.maximum_marks as max_marks,students_examination_rel.marks_obtained as marks";
        $query->Where = "where student_examination.session_id='$session_id' and student_examination.section='$section' and student_examination.subject_id IN(" . $sub_ids . ") and student_examination.exam_type='$exam_type' and student_examination.id=students_examination_rel.exam_id and students_examination_rel.student_id='$student_id'";
        $data = $query->ListOfAllRecords('object');
        $count = 0;
        foreach ($data as $result) {
            $marks_percent = number_format(($result->marks * 100 / $result->max_marks), 1);
            $total_result = ($default_percent * $marks_percent / 100);
            $count+= $total_result;
        }
        return $count;
    }

    function listAll_for_staff($login_staff_id, $school_id, $session_id = 0, $result_type = 'object') {
        $query = new query('student_examination,staff,subject_master,session');
        $query->Field = ('student_examination.id as id,student_examination.school_id as school_id, student_examination.title as title, student_examination.section as section, student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks as maximum_marks,student_examination.date_of_examination as date, student_examination.time_of_examination as time,staff.title as staff_title,staff.first_name as first_name,staff.last_name as last_name,subject_master.name as subject,session.title as session_name');
        if ($session_id) {
            $query->Where = "where student_examination.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination.session_id='" . mysql_real_escape_string($session_id) . "' and student_examination.teacher_id=staff.id and student_examination.subject_id=subject_master.id and student_examination.session_id=session.id  AND `staff`.`id` = '$login_staff_id'  ORDER BY student_examination.date_of_examination desc";
        } else {
            $query->Where = "where student_examination.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination.teacher_id=staff.id and student_examination.subject_id=subject_master.id and student_examination.session_id=session.id AND `staff`.`id` = '$login_staff_id' ORDER BY student_examination.date_of_examination desc";
        }
        return $query->ListOfAllRecords('object');
    }

    function examination_result_student($id) {

        $query = new query('student,student_examination,students_examination_rel,student_family_details,subject_master');
        $query->Field = ('student.id as student_id, student.reg_id as reg_id,student.first_name as first_name,student.last_name as last_name, students_examination_rel.roll_no as roll_no,subject_master.name as subject, students_examination_rel.marks_obtained as marks_obtained, student_examination.minimum_marks as minimum_marks,student_examination.maximum_marks,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where student_examination.id='" . mysql_real_escape_string($id) . "' and student_examination.id=students_examination_rel.exam_id and student_examination.subject_id=subject_master.id and students_examination_rel.student_id=student.id and student_family_details.student_id=student.id and student.is_active='1' GROUP BY student.id ORDER BY student.id asc";
        return $query->ListOfAllRecords('object');
    }

    function list_all_student_examination() {
        return $this->ListOfAllRecords();
    }

}

class examinationMarks extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('students_examination_rel');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'exam_id', 'roll_no', 'student_id', 'marks_obtained', 'on_date', 'update_date');
    }

    /*
     * Create new exam result list or update existing theme
     */

    function saveData($POST) {
        if (!empty($POST)):
            foreach ($POST as $key => $value):
                $ObjUpdate = new query('students_examination_rel');
                $ObjUpdate->Where = ("where student_id='" . $key . "' and exam_id=" . $value['exam_id']);
                $check = $ObjUpdate->DisplayOne();

                $ObjQuery = new query('students_examination_rel');
                $ObjQuery->Data['exam_id'] = $value['exam_id'];
                $ObjQuery->Data['marks_obtained'] = $value['marks_obtained'];
                $ObjQuery->Data['roll_no'] = $value['roll_no'];
                $ObjQuery->Data['section'] = $value['section'];
                $ObjQuery->Data['student_id'] = $key;
                if ($check):
                    $ObjQuery->Data['id'] = $check->id;
                    $ObjQuery->Data['update_date'] = date('Y-m-d');
                    $ObjQuery->Update();
                else:
                    $ObjQuery->Data['on_date'] = date('Y-m-d');
                    $ObjQuery->Insert();
                endif;
            endforeach;
        endif;
        return true;
    }

    function updateData($POST) {
        if (!empty($POST)):
            foreach ($POST as $key => $value):

                $ObjUpdate = new query('students_examination_rel');
                $ObjUpdate->Where = ("where student_id='" . $key . "' and exam_id=" . $value['exam_id']);
                $check = $ObjUpdate->DisplayOne();

                $ObjQuery = new query('students_examination_rel');
                $ObjQuery->Data['exam_id'] = $value['exam_id'];
                $ObjQuery->Data['marks_obtained'] = $value['marks_obtained'];
                $ObjQuery->Data['roll_no'] = $value['roll_no'];
                $ObjQuery->Data['student_id'] = $key;
                if ($check):
                    $ObjQuery->Data['id'] = $check->id;
                    $ObjQuery->Data['update_date'] = date('Y-m-d');
                    $ObjQuery->Update();
                else:
                    $ObjQuery->Data['on_date'] = date('Y-m-d');
                    $ObjQuery->Insert();
                endif;


            endforeach;
        endif;
        return true;
    }

#get sutdent marks

    function get_student_exam_marks($exam_id, $student_id) {

        $this->Where = "where exam_id='" . mysql_real_escape_string($exam_id) . "' and student_id='" . mysql_real_escape_string($student_id) . "'  ORDER BY id asc limit 0,1";
        return $check = $this->DisplayOne();
    }

#get sutdent marks

    function get_student_exam_marks_in_multpale($exam_id, $student_id) {
        $marks = array('total' => '', 'obtain' => '');
        $query = new query('students_examination_rel,student_examination');
        $query->Field = ('students_examination_rel.*,student_examination.minimum_marks,student_examination.maximum_marks');
        $query->Where = "where students_examination_rel.exam_id IN(" . mysql_real_escape_string($exam_id) . ") and students_examination_rel.student_id='" . mysql_real_escape_string($student_id) . "' and students_examination_rel.exam_id=student_examination.id ORDER BY students_examination_rel.id asc";
        $record = $query->ListOfAllRecords('object');
        if (!empty($record)):
            foreach ($record as $key => $val):
                $marks['total'] = $marks['total'] + $val->maximum_marks;
                $marks['obtain'] = $marks['obtain'] + $val->marks_obtained;
            endforeach;
        endif;

        return $marks;
    }

#get sutdent marks

    function getStudentResult($exam_id, $student_id) {
        $query = new query('students_examination_rel,student_examination');
        $query->Field = ('students_examination_rel.*,student_examination.minimum_marks,student_examination.maximum_marks');
        $query->Where = "where students_examination_rel.exam_id='" . mysql_real_escape_string($exam_id) . "' and students_examination_rel.student_id='" . mysql_real_escape_string($student_id) . "' and students_examination_rel.exam_id=student_examination.id ORDER BY students_examination_rel.id asc limit 0,1";
        return $check = $query->DisplayOne();
    }

#get sutdent marks

    function getExamStudents($exam_id) {
        $query = new query('students_examination_rel,student,session_students_rel,student_family_details');
        $query->Field = ('session_students_rel.id as rel_id,session_students_rel.session_id as session_id,session_students_rel.roll_no as roll_no, session_students_rel.section as section,session_students_rel.concession,session_students_rel.concession_type,students_examination_rel.exam_id,student.id,student.first_name, student.last_name, student.sex,student.email, student.phone,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where students_examination_rel.exam_id IN(" . mysql_real_escape_string($exam_id) . ") and students_examination_rel.student_id=student.id and session_students_rel.student_id=student.id and session_students_rel.roll_no=students_examination_rel.roll_no and student.is_active=1 and student.is_deleted=0 and student_family_details.student_id=student.id GROUP BY student.id ORDER BY session_students_rel.section asc";
        return $query->ListOfAllRecords('object');
    }

    /*
     * Get exam result list by id
     */

    function getRecord($id) {
        return $this->_getObject('students_examination_rel', $id);
    }

    /*
     * Get List of all students_examination_rel list in array
     */

    function listAll($show_active = 0, $result_type = 'object') {
//$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active)
            $this->Where = "where is_deleted='0' and is_active='1'  ORDER BY position asc";
        else
            $this->Where = "where is_deleted='0' ORDER BY position asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    function getExamResultText($Result) {
        $result_data = '';
        $minimum = '0';
        $obtain = '0';
        $maximum = '0';
        $sr = '1';
        if (!empty($Result)):
            $result_data.="<table style='width: 100%; text-align: center; line-height: 40px;'>
                                                    <tr><th style='text-align:left;width:10%;border-bottom:1px solid;' >Sr. No.</th><th style='text-align: left; width: 30%;border-bottom:1px solid;'>Subject Name</th><th style='width:20%;border-bottom:1px solid;'>Passing Marks</th><th style='width:20%;border-bottom:1px solid;'>Minimum Obtained</th><th style='width:20%;border-bottom:1px solid;'>Maximum Marks</th></tr>
                                                    ";
            foreach ($Result as $r_k => $r_v):
                $result_data.="<tr><td style='text-align:left;width:10%;' >" . $sr . ".</td><td style='text-align: left; width: 30%;'>" . $r_v->subject . "</td><td style='width:20%;'>" . $r_v->minimum_marks . "</td><td style='width:20%;'>" . $r_v->marks_obtained . "</td><td style='width:20%;'>" . $r_v->maximum_marks . "</td></tr>";

                $minimum = $minimum + $r_v->minimum_marks;
                $obtain = $obtain + $r_v->marks_obtained;
                $maximum = $maximum + $r_v->maximum_marks;
                $sr++;
            endforeach;
            $result_data.="</table>";
        endif;
        return $result_data;
    }

    function getExamResultTextMSG($Result) {
        $result_data = '';
        $minimum = '0';
        $obtain = '0';
        $maximum = '0';
        $sr = '1';
        if (!empty($Result)):

            foreach ($Result as $r_k => $r_v):
                $result_data.=$r_v->subject . "=" . $r_v->marks_obtained . "\n";
            endforeach;
            $result_data.="</table>";
        endif;
        return $result_data;
    }

    /*
     * delete a students_examination_rel list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /*
     * Update students_examination_rel list position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

#delete all result of exam

    function deleteExamResults($exam_id) {
        $this->Where = ("where exam_id='" . mysql_real_escape_string($exam_id) . "'");

        $this->Delete_where();
    }

}

class examinationGroup extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_examination_group');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'session_id', 'section', 'exam_id', 'title', 'send_sms', 'intimation_sms', 'from_date', 'to_date', 'remarks', 'term_exam', 'on_date', 'update_date');
    }

    /*
     * Create new group exam  list or update existing theme
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (!empty($POST['exam'])):
            $this->Data['exam_id'] = implode(',', $POST['exam']);
        endif;
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get List of all exam group list in array
     */

    function ListAllGroups($school_id, $limit = FALSE) {
        $query = new query('student_examination_group,session');
        $query->Field = ('student_examination_group.*, session.title as session_name');
        $query->Where = "where student_examination_group.school_id='$school_id' and student_examination_group.session_id=session.id";
        return $query->ListOfAllRecords('object');
    }

    function ListYearlyDMCGroups($school_id, $session_id, $section) {
        $this->Where = "where school_id='$school_id' and session_id='$session_id' and section='$section' order by id asc limit 2";
        return $this->ListOfAllRecords('object');
    }

    function listAll($school_id, $session_id = 0, $result_type = 'object') {

        $query = new query('student_examination_group,session');
        $query->Field = ('student_examination_group.*, session.title as session_name');
        if ($session_id):
            $query->Where = "where student_examination_group.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination_group.session_id='" . mysql_real_escape_string($session_id) . "' and student_examination_group.session_id=session.id ORDER BY student_examination_group.id desc";
        else:
            $query->Where = "where student_examination_group.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination_group.session_id=session.id ORDER BY student_examination_group.id desc";
        endif;
        return $query->ListOfAllRecords('object');
    }

    function getGroupExamSession($school_id, $session_id = 0, $result_type = 'object') {

        $query = new query('student_examination_group,session');
        $query->Field = ('student_examination_group.session_id as session_id, session.title as session_name');
        $query->Where = "where student_examination_group.school_id='" . mysql_real_escape_string($school_id) . "' and student_examination_group.session_id=session.id GROUP BY student_examination_group.session_id ORDER BY student_examination_group.id desc";
        return $query->ListOfAllRecords('object');
    }

    function getSelectedSubjectGroup($session_id, $ct_sec, $subject) {
        $record_data = array('record' => '', 'terms' => '');
        $exams = array();
        $term_title = array();
        $query = new query('student_examination_group,student_examination,subject_master');
        $query->Field = ('student_examination_group.*,student_examination.id as exam,student_examination.subject_id,subject_master.name as subject_name');
        $query->Where = "where student_examination_group.session_id='" . mysql_real_escape_string($session_id) . "' and student_examination_group.term_exam='1' and student_examination_group.section='" . mysql_real_escape_string($ct_sec) . "' and student_examination.id IN(student_examination_group.exam_id) and subject_master.id=student_examination.subject_id and student_examination.subject_id='" . $subject . "' ORDER BY student_examination_group.id desc";
        $result = $query->ListOfAllRecords('object');
        if (!empty($result)):
            foreach ($result as $key => $value):
                $exams[] = $value->exam;
                $term_title[$value->exam] = $value->title;
            endforeach;
        endif;

        $exam_id = implode(',', $exams);

        if (!empty($exam_id)):
#Get exam Students result
            $QueryOb = new examinationMarks();
            $record = $QueryOb->getExamStudents($exam_id);

            if (!empty($record)):
                $record_data['record'] = $record;
                $record_data['terms'] = $term_title;
            endif;
        endif;
        return $record_data;
    }

    function getGroupExamSessionSection($session_id, $section) {
        $this->Where = "where session_id='" . $session_id . "' and section='" . $section . "' order by id desc";
        return $this->ListOfAllRecords('object');
    }

    function getSubjectByExams($exam_ids) {
        $query = new query('student_examination,subject_master');
        $query->Field = ('subject_master.*,student_examination.id as exam,student_examination.title as exam_title');
        $query->Where = "where student_examination.id IN(" . $exam_ids . ") and subject_master.id=student_examination.subject_id GROUP BY subject_master.id ORDER BY subject_master.id desc";
//$query->print=1;
        return $result = $query->ListOfAllRecords('object');
    }

    function getTermExamSessionSection($session_id, $section) {
        $this->Where = "where session_id='" . $session_id . "' and section='" . $section . "' and term_exam='1' order by id desc";
        return $this->ListOfAllRecords('object');
    }

    function getRecord($id) {
        return $this->_getObject('student_examination_group', $id);
    }

    /*
     * delete a exam group list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

class examGrade extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'desc', $orderby = 'id') {
        parent::__construct('exam_grade');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'title', 'minimum', 'maximum', 'remarks', 'on_date', 'update_date');
    }

    /*
     * Create new examGrade
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            $this->Update();
            return $this->Data['id'];
        } else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /* Get examGrade by id */

    function getRecord($id) {
        return $this->_getObject('exam_grade', $id);
    }

    /* Get List of all exam_grade in array */

    function listAll($school_id) {
        $this->Where = "where school_id='" . $school_id . "' ORDER BY id desc";

        return $this->ListOfAllRecords('object');
    }

    /* delete a exam_grade by id */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

class students_examination_rel extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'desc', $orderby = 'id') {
        parent::__construct('students_examination_rel');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'exam_id', 'roll_no', 'student_id', 'mark_obtained', 'on_date', 'update_date', 'section');
    }


    function examination_rel($student_login_id) {
        $this->Where = "JOIN `student_examination` ON `students_examination_rel`. `exam_id` = `student_examination`.`id` WHERE `students_examination_rel`.`student_id` = '$student_login_id'";
        return $this->ListOfAllRecords();
    }

}

?>