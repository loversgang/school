<?php

class homework extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('homework');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveHomeWork($POST, $login_staff_id, $remove_file = '') {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        $filename = rand() . $_FILES['file']['name'];
        if ($_FILES['file']['error'] == 0) {
            move_uploaded_file($_FILES['file']['tmp_name'], DIR_FS_SITE_UPLOAD . 'file/homework/' . $filename);
            $this->Data['file'] = $filename;
        }
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($_FILES['file']['error'] == 0) {
                unlink(DIR_FS_SITE_UPLOAD . 'file/homework/' . $remove_file);
            }
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['staff_id'] = $login_staff_id;
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function saveMark($id, $mark_value) {
        $this->Data['id'] = $id;
        $this->Data['mark'] = $mark_value;
        $this->Update();
    }

    function listsHomeWork($id = 0) {
        if ($id > 0) {
            $this->Where = "WHERE `id` = '$id'";
            return $this->DisplayOne();
        } else {
            $this->Where = "WHERE `staff_id` = '" . $_SESSION['admin_session_secure']['user_id'] . "' ORDER BY `id` DESC ";
            return $this->ListOfAllRecords();
        }
    }

    function listsHomeWorkStudent($session_id) {
        $this->Where = "WHERE `class` = '$session_id' ORDER BY `id` DESC ";
        return $this->ListOfAllRecords();
    }

    function listsHomeWorkStudent_with_section($session_id, $sec) {
        $this->Where = "WHERE `class` = '$session_id' AND `section` = '$sec' ORDER BY `id` DESC ";
        return $this->ListOfAllRecords();
    }

    /* Get Staff Staff by id */

    function getClass($id) {
        return $this->_getObject('staff', $id);
    }

    function deleteHomeWork($id) {
        $this->Where = "WHERE `id` = '$id'";
        $this->Delete_where();
    }

    function removeImg($id) {
        $this->Data['id'] = $id;
        $this->Data['file'] = '';
        $this->Update();
    }

}

class reports extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('report');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function check_assingnment_student_id_exist($assignment_id, $student_id) {
        $this->Where = "WHERE `assignment_id` = '$assignment_id' AND `student_id` = '$student_id'";
        return $this->DisplayOne();
    }

    function saveReports($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function all_reports($id) {
        $this->Where = "WHERE `assignment_id` = '$id'";
        return $this->ListOfAllRecords();
    }

    function delete_using_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    public static function get_status_grade_using_assignment_student_id($assignment_id, $student_id) {
        $query = new reports();
        $query->Where = "WHERE `assignment_id` = '$assignment_id' AND `student_id` = '$student_id'";
        return $query->DisplayOne();
    }

}

?>