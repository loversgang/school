<?php

class attendance extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('student_attendance');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'roll_no', 'section', 'student_id', 'session_id', 'date', 'status', 'on_date');
    }

    /*
     * Create new attendance list or update existing theme
     */

    function saveData($POST) {
        $Attendance_array = array(
            "P" => "Present",
            "A" => "Absent",
            "L" => "Leave",
            "H" => "Holiday"
        );

        if (!empty($POST)):
            foreach ($POST as $key => $value):
                #Check the attendance on this date
                $QueryCh = new query('student_attendance');
                $QueryCh->Where = "where session_id='" . $value['session_id'] . "' and student_id='" . $value['student_id'] . "' and date='" . $value['date'] . "'";
                $check = $QueryCh->DisplayOne();


                $ObjQuery = new query('student_attendance');
                $ObjQuery->Data['session_id'] = $value['session_id'];
                $ObjQuery->Data['student_id'] = $value['student_id'];
                $ObjQuery->Data['roll_no'] = $value['roll_no'];
                $ObjQuery->Data['section'] = $value['section'];
                $ObjQuery->Data['status'] = $value['status'];
                $ObjQuery->Data['date'] = $value['date'];
                if (!$check):
                    $ObjQuery->Data['on_date'] = date('Y-m-d');
                    $ObjQuery->Insert();

                    $QueryPr = new query('student_family_details');
                    $QueryPr->Where = "where student_id='" . $value['student_id'] . "'";
                    $parents = $QueryPr->DisplayOne();

                    $s_info = get_object('student', $value['student_id']);
                    $school = get_object('school', $s_info->school_id);

                else:
                    $ObjQuery->Data['id'] = $check->id;
                    $ObjQuery->Data['on_date'] = date('Y-m-d');
                    $ObjQuery->Update();
                endif;
            endforeach;
        endif;
        return true;
    }

    function getHalfAttendance($session_id, $from, $to) {
        $total_sunday = getTotalSunday($from, $to);
        $total_days = getTotalDays($from, $to);
        $working_days = $total_days - $total_sunday;

        $QueryObj = new studentSession();
        $records = $QueryObj->getAllSessionStudent($session_id);
        if (!empty($records)):
            foreach ($records as $key => $value):
                #get Student Absent Now for previous date
                $QurAtten = new attendance();
                $Absent = $QurAtten->getStudentAbsent($session_id, $value->section, $value->id, $from, $to);
                if ($Absent != '0'):
                    $per = (($Absent / $working_days) * 100);
                    if ($per >= '50'):
                    else:
                        unset($records[$key]);
                    endif;
                else:
                    unset($records[$key]);
                endif;

            endforeach;
        endif;
        return $records;
    }

    function getClassWiseAttendance($school, $from, $to, $sess_int) {
        $val = explode(',', $sess_int);
        $start = $val[0];
        $end = $val[1];
        $obj = new query('session');
        $obj->Field = ('id');
        $obj->Where = "where start_date='$start' and end_date='$end'";
        $data = $obj->ListOfAllRecords();
        $session_ids_array = array();
        foreach ($data as $sess_data) {
            $session_ids_array[] = $sess_data['id'];
        }
        $session_ids = implode(',', $session_ids_array);
        $record_array = array();
        $total_sunday = getTotalSunday($from, $to);
        $total_days = date('d', strtotime($to));

        #school holiday
        $M_obj = new schoolHolidays();
        $M_holiday = $M_obj->checkMonthlyHoliday($school, date('m', strtotime($from)), date('Y', strtotime($from)));

        #Total Working Days
        $working_days = $total_days - $total_sunday - $M_holiday;

        #get_all session 
        $QuerySession = new session();
        $records = $QuerySession->getSessionIntervalData($session_ids);
        //return pr($records);
        if (!empty($records)):
            foreach ($records as $key => $value):
                #get Total Student in session           
                $QueryObj = new studentSession();
                $total_rec = $QueryObj->getAllSessionStudent($value->id);


                #get Student attendance
                $QurAtten = new attendance();
                $status = $QurAtten->getTotalSessionStudentAttendance($value->id, $from, $to);

                $record_array[] = array('session_id' => $value->id, 'session_name' => $value->title, 'total_student' => count($total_rec), 'absent' => $status['A'], 'present' => $status['P'], 'leave' => $status['L'], 'total_days' => $working_days);

            endforeach;
        endif;

        return $record_array;
    }

    function get_current_session_attendance($id) {
        $record = array();
        $query1 = "SELECT session.id AS id,course_id AS course_id, session.title, session.start_date, session.end_date FROM session 
                            where session.school_id='" . mysql_real_escape_string($id) . "' AND session.is_active='1' AND DATE(session.start_date) <= CAST('" . date('Y-m-d') . "' AS DATE ) AND DATE(session.end_date) >= CAST('" . date('Y-m-d') . "' AS DATE) GROUP BY session.id ORDER BY session.position asc";

        $query2 = "SELECT session_id,count(session_students_rel.student_id) as total_student FROM `session_students_rel`,`student` 
                            WHERE session_students_rel.id!='0' and student.is_active='1' and student.is_deleted='0' and student.id=session_students_rel.student_id GROUP BY session_students_rel.session_id";


        $q1 = "SELECT * FROM (" . $query1 . ") as t1 left join (" . $query2 . ") as t2 ON t1.id = t2.session_id ";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($q1);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):

                $queryOb = new query('student_attendance');
                $queryOb->Field = ("count(*) as count,status,session_id");
                $queryOb->Where = "where session_id='" . $object->id . "' and  date='" . date('Y-m-d') . "' and status!='H' and status='P' group by status";
                $status = $queryOb->DisplayOne();
                if (is_object($status)):
                    $present = $status->count;
                else:
                    $present = '0';
                endif;

                $queryOb1 = new query('student_attendance');
                $queryOb1->Field = ("count(*) as count,status,session_id");
                $queryOb1->Where = "where session_id='" . $object->id . "' and  date='" . date('Y-m-d') . "' and status!='H' and status='A' group by status";
                $status1 = $queryOb1->DisplayOne();
                if (is_object($status1)):
                    $absent = $status1->count;
                else:
                    $absent = '0';
                endif;

                $queryOb2 = new query('student_attendance');
                $queryOb2->Field = ("count(*) as count,status,session_id");
                $queryOb2->Where = "where session_id='" . $object->id . "' and  date='" . date('Y-m-d') . "' and status!='H' and status='L' group by status";
                $status2 = $queryOb2->DisplayOne();
                if (is_object($status2)):
                    $leave = $status2->count;
                else:
                    $leave = '0';
                endif;

                if (!empty($object->total_student)):
                    $total_stu = $object->total_student;
                else:
                    $total_stu = '0';
                endif;

                $record[] = array('session_id' => $object->session_id, 'course_id' => $object->course_id, 'title' => $object->title, 'total_student' => $total_stu, 'present' => $present, 'absent' => $absent, 'leave' => $leave);
            endwhile;
        endif;

        return $record;
    }

    /*
     * Get attendance list by id
     */

    function getRecord($id) {
        return $this->_getObject('student_attendance', $id);
    }

    /*
     * Get List of all attendance list in array
     */

    function listAll($show_active = 0, $result_type = 'object') {
        if ($show_active)
            $this->Where = "where is_deleted='0' and is_active='1'  ORDER BY id asc";
        else
            $this->Where = "where is_deleted='0' ORDER BY positiidon asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    function checkMinDateAttendace($session_id, $section) {
        #Check the attendance on this date
        $this->Field = ("min(date) as min_date");
        $this->Where = "where session_id='" . $session_id . "' and section='" . $section . "'";
        $rec = $this->DisplayOne();
        return $rec->min_date;
    }

    function checkMaxDateAttendace($session_id, $section) {
        #Check the attendance on this date
        $this->Field = ("max(date) as max_date");
        $this->Where = "where session_id='" . $session_id . "' and section='" . $section . "'";
        $rec = $this->DisplayOne();
        return $rec->max_date;
    }

    function checkCurrentDateAttendace($session_id, $ct_sect, $date) {
        #Check the attendance on this date
        $QueryCh = new query('student_attendance');
        $QueryCh->Where = "where session_id='" . $session_id . "' and section='" . $ct_sect . "' and date='" . $date . "'";
        return $QueryCh->DisplayOne();
    }

    function checkMonthYearAttendace($student_id, $month, $year) {
        $status = array();
        #Check the attendance on this date
        $this->Field = ('count(*) as count,status');
        $this->Where = "where student_id='" . $student_id . "' and month(date)='" . date($month) . "' and YEAR(date)='" . date($year) . "' group by status";
        $this->DisplayAll();
        while ($object = $this->GetObjectFromRecord()):
            $status[$object->status] = $object->count;
        endwhile;
        return $status;
    }

    function checkSessionSectionMonthYearAttendace($session_id, $section, $student_id, $month, $year) {
        $status = array();
        #Check the attendance on this date
        $this->Field = ('count(*) as count,status');
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student_id) . "' and month(date)='" . date(mysql_real_escape_string($month)) . "' and YEAR(date)='" . date(mysql_real_escape_string($year)) . "' and status!='H' group by status";
        $this->DisplayAll();
        while ($object = $this->GetObjectFromRecord()):
            $status[$object->status] = $object->count;
        endwhile;
        return $status;
    }

    function checkSessionStudentAttendace($session_id, $section, $student_id) {
        $status = array();
        #Check the attendance on this date
        $this->Field = ('count(*) as count,status');
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student_id) . "' AND  status!='H' group by status";
        $this->DisplayAll();
        while ($object = $this->GetObjectFromRecord()):
            $status[$object->status] = $object->count;
        endwhile;
        return $status;
    }

    function getStudentAbsent($session_id, $section, $student_id, $from, $to) {
        $status = array();
        #Check the attendance on this date
        $this->Field = ('count(*) as count,status');
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student_id) . "' 
                        AND DATE(date) >= CAST('" . $from . "' AS DATE) AND DATE(date) <= CAST('" . $to . "' AS DATE) and status='A' group by status";
        $rec = $this->DisplayOne();
        if ($rec):
            return $rec->count;
        else:
            return '0';
        endif;
    }

    function getTotalSessionStudentAttendance($session_id, $from, $to) {
        $status = array('A' => '0', 'L' => '0', 'P' => '0');
        #Check the attendance on this date
        $this->Field = ('count(status) as count,status,session_id');
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and (student_id!='0' OR  student_id!='') and date>='" . date(mysql_real_escape_string($from)) . "' and date<='" . date(mysql_real_escape_string($to)) . "' and status !='H' group by status";
        $record = $this->ListOfAllRecords('object');
        if (!empty($record)):
            foreach ($record as $key => $value):
                $status[$value->status] = $value->count;
            endforeach;
        endif;
        return $status;
    }

    function getStudentAbsentDetail($session_id, $section, $student_id, $from, $to) {
        $status = array();
        #Check the attendance on this date
        $this->Field = ('date');
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student_id) . "' and date>='" . date(mysql_real_escape_string($from)) . "' and date<='" . date(mysql_real_escape_string($to)) . "' and status='A' order by date desc";
        return $this->ListOfAllRecords('object');
    }

    function get_today_attendance($session_id, $section, $student_id, $date) {
        $status = array();
        #Check the attendance on this date
        $this->Field = ('status');
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student_id) . "' and date='" . date(mysql_real_escape_string($date)) . "' order by date desc";

        $rec = $this->DisplayOne();
        if ($rec):
            return $rec->status;
        else:
            return '-';
        endif;
    }

    /*
     * delete a attendance list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    // Get Total Attendance Of A Student
    public static function getTotalAttendanceOfStudent($student_id, $class_id, $section) {
        $obj = new attendance;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where session_id='$class_id' and student_id='$student_id' and section='$section' and status='P'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

    public static function getTotalWorkingDays($section, $class_id, $student_id) {
        $obj = new attendance;
        $obj->Field = "COUNT(*) as count";
        $obj->Where = "where session_id='$class_id' and student_id='$student_id' and section='$section'";
        $data = $obj->DisplayOne();
        return $data->count;
    }

}

?>