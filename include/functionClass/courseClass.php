<?php


class course extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;


    function __construct($order='asc', $orderby='position'){
        parent::__construct('school_course');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'school_id','course_name','description','position','is_deleted', 'is_active');

	}

    /*
     * Create new course or update existing theme
     */
    function saveData($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
       
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              return $this->Data['id'];
        }
        else{   
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get course by id
     */
    function getRecord($id){
        return $this->_getObject('school_course', $id);
    }

    
    /*
     * Get List of all course in array
     */
    function listAll($school_id,$show_active=0, $result_type='object'){
		
		if($show_active)
			$this->Where="where is_deleted='0' and school_id='".$school_id."' and is_active='1'  ORDER BY position asc";
		else
			$this->Where="where is_deleted='0' and school_id='".$school_id."' ORDER BY position asc";
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }

  

    /*
     * delete a course by id
     */
    function deleteRecord($id){ 
        $this->id=$id;
            return $this->Delete();
    }

    /*
     * Update course position
     */
    function updatePosition($position, $id){
        $this->Data['id']=$id;
        $this->Data['position']=($position!='')?$position:0;
        $this->Update();
    }

    

   
}
?>