<?php

class remarks extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('remarks');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveRemarks($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function remarks($staff_id) {
        $this->Where = "WHERE `staff_id` = '$staff_id'";
        return $this->ListOfAllRecords('object');
    }

    function remarks_login_student($student_id) {
        $this->Where = "WHERE `student_id` = '$student_id'";
        return $this->ListOfAllRecords('object');
    }

    function delete_remark($id) {
        $this->Where = "WHERE `id` = '$id'";
        $this->Delete_where();
    }

    function remark_using_id($id) {
        $this->Where = "WHERE `id` = '$id'";
        return $this->DisplayOne();
    }

}

?>