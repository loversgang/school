<?php

class subject extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('subject_master');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'name', 'type', 'position', 'is_deleted', 'is_active');
    }

    /*
     * Create new subject list or update existing theme
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get subject list by id
     */

    function getRecord($id) {
        return $this->_getObject('subject_master', $id);
    }

    /*
     * Get List of all subject list in array
     */

    function listAll($school_id, $show_active = 0, $result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active)
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and  is_deleted='0' and is_active='1'  ORDER BY type asc";
        else
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_deleted='0' ORDER BY type asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    function ComplserySubjectList($school_id, $result_type = 'object') {

        $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_active='1' and type='Compulsory' ORDER BY name asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    function ElectiveSubjectList($school_id, $result_type = 'object') {

        $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_active='1' and type='Elective' ORDER BY name asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    #Get Session Subjects

    function listSessionSubject($list1, $list2, $show_active = 0, $result_type = 'object') {
        if (empty($list1)) {
            $list1 = '0';
        }
        if (empty($list2)) {
            $list2 = '0';
        }

        if ($show_active)
            $this->Where = "where is_deleted='0' and is_active='1' and id IN(" . $list1 . ") OR id IN(" . $list2 . ") ORDER BY position asc";
        else
            $this->Where = "where is_deleted='0' and id IN(" . $list1 . ") OR id IN(" . $list2 . ") ORDER BY position asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    #Get Session Subjects

    function listSessionSelectedSubjects($list1, $show_active = 0, $result_type = 'object') {
        if (empty($list1)) {
            $list1 = '0';
        }

        if ($show_active)
            $this->Where = "where is_deleted='0' and is_active='1' and id IN(" . $list1 . ") ORDER BY position asc";
        else
            $this->Where = "where is_deleted='0' and id IN(" . $list1 . ") ORDER BY position asc";

        return $this->ListOfAllRecords('object');
    }

    #Get Session Subjects

    function GetSessionSubjects($school_id, $list) {
        if (empty($list)) {
            $list = '0';
        }
        $this->Where = "where is_active='1' and school_id='$school_id' and id IN(" . $list . ") ORDER BY position asc";
        return $this->ListOfAllRecords('object');
    }

    #Get Session Subjects

    function GetSessionSubjectsInString($list) {
        $res = array();
        if (empty($list)) {
            $list = '0';
        }
        $this->Where = "where is_active='1' and id IN(" . $list . ") ORDER BY position asc";
        $record = $this->ListOfAllRecords('object');
        if (!empty($record)):
            foreach ($record as $key => $value):
                $res[] = $value->name;
            endforeach;
            return implode(',', $res);
        else:
            return '';
        endif;
    }

    #Get Session Subjects

    function GetSessionSubjectsInArray($list) {
        $res = array();
        if (empty($list)) {
            $list = '0';
        }
        $this->Where = "where is_active='1' and id IN(" . $list . ") ORDER BY position asc";
        $record = $this->ListOfAllRecords('object');
        if (!empty($record)):
            foreach ($record as $key => $value):
                $res[] = $value->name;
            endforeach;

        endif;
        return $res;
    }

    /*
     * delete a subject list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /*
     * Update subject list position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

}

?>