<?php

class eventCat extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('school_events_cat');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'title', 'description', 'is_active');
    }

    /*
     * Create new Event Cat list or update existing theme
     */

    function saveEventCat($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Event Cat list by id
     */

    function getRecord($id) {
        return $this->_getObject('school_events_cat', $id);
    }

    // List All cats
    function listAllCats() {
        return $this->ListOfAllRecords('object');
    }

}

class event extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('school_events');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'session_year', 'session_id', 'section', 'event_cat', 'event', 'description', 'is_active', 'on_date', 'date_add', 'date_upd');
    }

    /*
     * Create new Event list or update existing theme
     */

    function saveEvent($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['date_upd'] = date('d-m-Y');
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_add'] = date('d-m-Y');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Event Cat list by id
     */

    function getRecord($id) {
        return $this->_getObject('school_events', $id);
    }

    // List All cats
    function listAllEvents() {
        return $this->ListOfAllRecords('object');
    }

    // Get Events By Session ID
    function getEventsBySessionId($session_id) {
        $current_date = date('Y-m-d');
        $this->Where = "Where (session_id='$session_id' or session_id='0') and is_active='1' AND CAST(on_date as DATE) >= CAST('$current_date' as DATE)";
        return $this->ListOfAllRecords('object');
    }

    // Get School Events

    function getSchoolEvents() {
        $current_date = date('Y-m-d');
        $this->Where = "Where `session_id`='0' AND `is_active`='1' AND `on_date` >= '$current_date'";
        return $this->ListOfAllRecords('object');
    }

}
