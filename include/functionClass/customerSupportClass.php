<?php

/*
 * Customer Support Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class customer_support extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     * 
     */

    function __construct($order='desc', $orderby='id') {
        parent::__construct('customer_support');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id','message_to','message_from','message','date','ip_address','is_read','is_deleted');
        
    }

    /*
     * Create new page or update existing page
     */

    function saveMessage($to,$from,$message) {
       
        $this->Data['message_to']=$to;
        $this->Data['message_from']=$from;
        $this->Data['message']=$message;
        $this->Insert();
        return $this->GetMaxId();
    }

    /*
     * Get page by id
     */

    function getMessage($id) {
        return $this->_getObject('customer_support', $id);
    }
 
    
    
    
    
    /*
     * Get List of all imbox messages in array
     */

    function listInboxMessages($id,$result_type='object') {

        $id= mysql_real_escape_string($id);
        $this->Where = "where is_deleted='0' AND  message_to='$id' ORDER BY date desc,id desc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }
    
    /*
     * Get List of all sent messages in array
     */

    function listSentMessages($id,$result_type='object') {

        $id= mysql_real_escape_string($id); 
        $this->Where = "where is_deleted='0' AND  message_from='$id' ORDER BY date desc,id desc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    
    function getUnreadMessagess($id) {
            $id= mysql_real_escape_string($id); 
            $this->Where = "where is_deleted='0' AND  message_to='$id'  AND is_read='0' ORDER BY date desc,id desc";
    
            return $this->ListOfAllRecords('object');
    }
    
    function countUnreadMessages($id) {
        
            $id= mysql_real_escape_string($id); 
            $contacts=array();
     
            $this->Where = "where is_deleted='0' AND  message_to='$id'  AND is_read='0' ORDER BY date desc,id desc";
    
            $contacts= $this->ListOfAllRecords('object');
            
            return(count($contacts));
    }
    
    
    function getLimitedMessages($limit,$id) {
            $id= mysql_real_escape_string($id); 
            $this->Where = "where is_deleted='0' AND  message_to='$id'  ORDER BY date desc,id desc LIMIT 0,$limit";
    
            return $this->ListOfAllRecords('object');
    }
    
    
    
    function setmessageRead($id){
        $id= mysql_real_escape_string($id);
        $this->Data['is_read']='1';
        $this->Data['id']=$id;

        $this->Update();
        
        
    }
    
    
    
    /*
     * delete a page by id
     */

    function deleteMessage($id) {
        $this->id = $id;
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }
    
     /*
     * Fetch all deleted messages - which have "is_deleted" set to "1"
     */
    /*
    function getThrash(){
          $this->Where="where user_id='$this->user_id' AND is_deleted='1'";
          $this->DisplayAll();
    }
*/
 
}

?>