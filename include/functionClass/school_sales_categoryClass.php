<?php

class school_sales_category extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('school_sales_category');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'title', 'description', 'is_active', 'is_deleted');
    }

    /*
     * Create new school_sales or update existing theme
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    // List of all schools sales
    function listschool_sales() {
        return $this->ListOfAllRecords();
    }

    // check id exists  

    public static function checkId($id) {
        $query = new school_sales_category;
        $query->Where = "WHERE `id` = '$id'";
        return $query->DisplayOne();
    }

    function saveImage($post) {
        $this->Data = $this->_makeData($post, $this->requiredVars);
        $rand = rand(0, 99999999);
        $image_obj = new imageManipulation();

        if ($image_obj->upload_photo('school_sales', $_FILES['file'], $rand)):
            $this->Data['file'] = $image_obj->makeFileName($_FILES['file']['name'], $rand);
        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                $max_id = $Data['id'];
        }
        else {
            $this->Data['date_added'] = date('Y-m-d');
            $this->Insert();
            $max_id = $this->GetMaxId();
        }
        return $max_id;
    }

}

?>