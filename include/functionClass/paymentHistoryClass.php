<?php

class paymentHistory extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='desc', $orderby='payment_date'){
        parent::__construct('payment_history');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'school_id','school_subscription_id', 'set_up_fee','subscription_price','amount', 'payment_type','payment_type_int','payment_date','next_payment_date' ,'transaction_id','payment_date', 'ip_address','remarks','on_date','admin_id','update_date');

	}

    /*
     * Create new Payment
     */
    function saveData($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['amount']=(str_replace(',', '', $POST['set_up_fee'])+str_replace(',', '', $POST['subscription_price']));
        $this->Data['set_up_fee']=str_replace(',', '', $POST['set_up_fee']);
        $this->Data['subscription_price']=str_replace(',', '', $POST['subscription_price']);

        $this->Data['ip_address']=$_SERVER['REMOTE_ADDR'];
        $this->Data['next_payment_date']=date('Y-m-d',strtotime($POST['payment_date'].'+'.$POST['payment_type_int'].' months'));   
        if(isset($this->Data['id']) && $this->Data['id']!=''){           
            $this->Data['update_date']=date('Y-m-d');
            if($this->Update())
              return $this->Data['id'];
        }
        else{ 
            $this->Data['on_date']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Payment by id
     */
    function getPaymentRecord($id){
        return $this->_getObject('payment_history', $id);
    }

    
    /*
     * Get List of all Payment in array
     */
    function listAll($school=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($school)
			$this->Where="where school_id='".$school."' ORDER BY id desc";
		else
			$this->Where="where id!='0' ORDER BY id desc";
		if($result_type=='object')
			return $this->DisplayAll();
                
		else
			return $this->ListOfAllRecords('object');
    }

    function get_total_school_payments($school){
            $this->Field=('SUM(amount) as amount');
            $this->Where="where school_id='".$school."' ";
            $record=$this->DisplayOne();
            if($record):
                return $record->amount;
            else:
                return '0';
            endif;
     } 
     
     
    /*
     * delete a Payment by id
     */
    function deleteRecord($id){
        $this->id=$id;        
            return $this->Delete();
    }

    
    function get_current_year_school_peyments(){    
        $record=array();
                    $query1="SELECT school.id, school.school_name from school 
                            where school.is_active='1' and school.is_deleted='0'"; 
                                    
                    $query2="SELECT payment_history.school_id,payment_history.payment_date,payment_history.amount,sum(payment_history.amount) AS total from
                              payment_history WHERE '".date('Y')."'=YEAR(payment_history.payment_date) GROUP BY payment_history.school_id" ;
                    
                    $new_query="SELECT * FROM (".$query1.") as t1 left join (".$query2.") as t2 ON t1.id = t2.school_id ";        
 
                     
                    $QueryObj = new query();
                    $QueryObj->ExecuteQuery($new_query);
				if($QueryObj->GetNumRows()): 
                                        while($object =$QueryObj->GetObjectFromRecord()):
                                                $record[]=$object;
                                        endwhile;
                                endif;
          return $record;        
 
            }     
            
     function get_current_year_school_peyments_with_afilate($list){    
        $record=array();
       if(empty($list)):
           $list='0';
       endif;
                    $query1="SELECT school.id, school.school_name from school 
                            where school.is_active='1' and school.is_deleted='0' and school.id IN(".mysql_real_escape_string($list).")"; 
                               
                    $query2="SELECT payment_history.school_id,payment_history.payment_date,payment_history.amount,sum(payment_history.amount) AS total from
                              payment_history WHERE '".date('Y')."'=YEAR(payment_history.payment_date) GROUP BY payment_history.school_id" ;
                    
                    $new_query="SELECT * FROM (".$query1.") as t1 left join (".$query2.") as t2 ON t1.id = t2.school_id ";        
 
                     
                    $QueryObj = new query();
                    $QueryObj->ExecuteQuery($new_query);
				if($QueryObj->GetNumRows()): 
                                        while($object =$QueryObj->GetObjectFromRecord()):
                                                $record[]=$object;
                                        endwhile;
                                endif;
          return $record;        
 
            }           
   
}
?>