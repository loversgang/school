<?php

class loan extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('loan');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'staff_id', 'amount', 'type', 'installment', 'total_installments', 'start_time', 'start_year', 'installments_paid');
    }

    function saveLoan($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function list_loans($staff_id) {
        $this->Where = "WHERE `staff_id` = '$staff_id'";
        return $this->ListOfAllRecords('object');
    }

    function list_loans_not_paid($staff_id, $start_year) {
        $this->Where = "WHERE `staff_id` = '$staff_id' AND `installments_paid` < `total_installments` AND '$start_year' >= `start_year`";
        return $this->ListOfAllRecords('object');
    }

    function list_loan($id) {
        $this->Where = "WHERE `id` = '$id'";
        return $this->DisplayOne();
    }

    function Delete_using_id($id) {
        $this->id = $id;
        $this->Delete();
    }

    function installments_paid($installments_paid, $loan_id) {
        $query = new loan;
        $query->Data['installments_paid'] = $installments_paid + 1;
        $query->Data['id'] = $loan_id;
        $query->Update();
    }

}

?>