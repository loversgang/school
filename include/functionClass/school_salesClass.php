<?php

class school_sales extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('school_sales');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveData($POST, $remove_file = '') {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';
        if ($_FILES['file']['error'] == 0) {
            if ($_FILES['file']['type'] == 'image/jpg' || $_FILES['file']['type'] == 'image/jpeg' || $_FILES['file']['type'] == 'image/png') {
                $name = $_FILES['file']['name'];
                $tmp_name = $_FILES['file']['tmp_name'];
                $filename = rand() . $name;
                $target_dir = DIR_FS_SITE_UPLOAD . 'file/school_sales/';
                $target_file = $target_dir . $filename;
                move_uploaded_file($tmp_name, $target_file);
                $this->Data['file'] = $filename;
            }
        }
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            unlink(DIR_FS_SITE_UPLOAD . 'file/school_sales/' . $remove_file);
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    // List of all schools sales
    function listschool_sales() {
        return $this->ListOfAllRecords();
    }

    // check id exists  

    public static function checkId($id) {
        $query = new school_sales;
        $query->Where = "WHERE `id` = '$id'";
        return $query->DisplayOne();
    }

    function saveImage($post) {
        $this->Data = $this->_makeData($post, $this->requiredVars);
        $rand = rand(0, 99999999);
        $image_obj = new imageManipulation();

        if ($image_obj->upload_photo('school_sales', $_FILES['file'], $rand)):
            $this->Data['file'] = $image_obj->makeFileName($_FILES['file']['name'], $rand);
        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                $max_id = $Data['id'];
        }
        else {
            $this->Data['date_added'] = date('Y-m-d');
            $this->Insert();
            $max_id = $this->GetMaxId();
        }
        return $max_id;
    }

    function school_sales_category_data() {
        $this->TableName = "school_sales_category";
        $this->Where = "WHERE `is_active` = 1  ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function school_sales_category_data_using_id($id) {
        $this->Field = "`school_sales`.`sales_cat_id`, `school_sales_category`.`title`";
        $this->Where = "LEFT JOIN `school_sales_category` ON `school_sales_category`.`id` = `school_sales`.`sales_cat_id` WHERE `school_sales`.`id` = '$id'";
        return $this->DisplayOne();
    }

    function school_sales_category_data_using_without_id() {
        $this->Field = "`school_sales`.`stock_available`,`school_sales`.`stock_left`,`school_sales`.`stock`, `school_sales`.`description`, `school_sales`.`id`, `school_sales`.`name`, `school_sales`.`price`, `school_sales`.`file`, `school_sales_category`.`title`";
        $this->Where = "LEFT JOIN `school_sales_category` ON `school_sales_category`.`id` = `school_sales`.`sales_cat_id`  GROUP BY `school_sales`.`id` ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function getSchoolSalesCategory() {
        $this->TableName = "school_sales_category";
        $this->Where = "WHERE `is_active` = 1 AND `is_deleted` = 0";
        return $this->ListOfAllRecords();
    }

//    function delete_school_sales($id) {
//        $this->Where = "WHERE `id` = '$id'";
//        $list = $this->DisplayOne();
//        $delete = $this->Delete_where();
//        if ($delete) {
//            unlink(DIR_FS_SITE_UPLOAD . 'file/school_sales/' . $list->file);
//        }
//    }
    function delete_school_sales($id, $removed_file) {
        $this->Where = "WHERE `id` = '$id'";
        $delete = $this->Delete_where();
        if ($delete) {
            unlink(DIR_FS_SITE_UPLOAD . 'file/school_sales/' . $removed_file);
        }
    }

    function sales_using_sales_cat_id($sales_cat_id) {
        $this->Where = "WHERE `sales_cat_id` = '$sales_cat_id'";
        return $this->ListOfAllRecords();
    }

}

?>