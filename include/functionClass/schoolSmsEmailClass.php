<?php

class schoolSmsEmailHistory extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='id'){
        parent::__construct('school_sms_email_history');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'school_id','student_id','type','sms_type','from_number','to_number','sms_text','on_date','sms_send_status','sms_send_date','to_email','from_email','email_type','email_subject','email_school_name','email_text','email_send_status','email_send_date');
	}

    /* Create new School SMS Email */
    function saveData($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              return $this->Data['id'];
        }
        else{  
            $this->Data['on_date']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get School SMS by id
     */
    function getRecord($id){
        return $this->_getObject('school_sms_email_history', $id);
    }

    function listAll($school=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($school)
			$this->Where="where school_id='".$school."' ORDER BY id asc";
		else
			$this->Where="where id!='0' ORDER BY id asc";
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('array');
    }
    
    function listAllToday(){
		$this->Where="where id!='0' and `on_date`='".date('Y-m-d')."' ORDER BY id asc";
                return $this->ListOfAllRecords('object');
    }    
    
    function getSchoolSmsArray($school=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
                        $records=array();
			$this->Where="where school_id='".$school."' ORDER BY id asc";

			$this->DisplayAll();
				if($this->GetNumRows()): 
                                    while($Obj=$this->GetObjectFromRecord()): 				
                                                            $records[]=$Obj->type;		
                                    endwhile;
                                endif;
                         return $records;       
    }
    

    /* delete a School SMS by id */
    function deleteRecord($id){
        $this->id=$id;        
            return $this->Delete();
    }
      
}


?>