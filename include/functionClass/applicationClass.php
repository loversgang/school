<?php

class application extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('application');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'staff_id', 'student_id', 'send_to', 'subject', 'message', 'on_date', 'leave_type_id', 'status');
    }

//    Create new course or update existing theme

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function listApplication($staff_id) {
        $this->Where = "WHERE `staff_id` = '$staff_id' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function listApplicationStudent($staff_id) {
        $this->Where = "WHERE `student_id` = '$staff_id' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    public static function checkId($id) {
        $query = new circulars;
        $query->Where = "WHERE `id` = '$id'";
        return $query->DisplayOne();
    }

//    Get Lists of all Student member Applications

    function listStudentApplications() {
        $this->Where = "WHERE `student_id` != 0";
        return $this->ListOfAllRecords();
    }

    // Get Lists of all Staff member Applications

    function listStaffApplications() {
        $this->Where = "WHERE `staff_id` != 0";
        return $this->ListOfAllRecords();
    }

    // Get Student Information By Student Id


    public static function studentInformation($id) {
        $query = new application;
        $query->TableName = 'student';
        $query->Where = "WHERE `id` = '$id'";
        return $query->DisplayOne();
    }

    // Get Staff Information By Staff Id

    public static function staffInformation($id) {
        $query = new application;
        $query->TableName = 'staff';
        $query->Where = "WHERE `id` = '$id'";
        return $query->DisplayOne();
    }

    // Delete Student Application By Id

    function deleteStudentApplicationById($id) {
        $this->id = $id;
        $this->Delete();
    }

    function deleteStaffApplicationById($id) {
        $this->id = $id;
        $this->Delete();
    }

    public static function set_nofication_read($id) {
        $query = new application;
        $query->Data['status'] = 1;
        $query->Where = "WHERE `id` = '$id'";
        $query->UpdateCustom();
    }

    public static function countStaffApplication($type = '') {
        $query = new application;
        $query->Field = "COUNT(*) as count";
        if ($type == 'student') {
            $query->Where = "WHERE `student_id` != 0 AND `status` = 0";
            $data = $query->DisplayOne();
            return $data->count;
        } else if ($type == 'staff') {
            $query->Where = "WHERE `staff_id` != 0 AND `status` = 0";
            $data = $query->DisplayOne();
            return $data->count;
        } else {
            $query->Where = "WHERE `status` = 0";
            $data = $query->DisplayOne();
            return $data->count;
        }
    }

}

?>