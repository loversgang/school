<?php
/*
 * Service Module Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class service extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('user_service');
	$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id','user_id', 'name','date_added', 'urlname', 'date_show','short_description', 'long_description', 'position','meta_name', 'meta_keyword', 'meta_description',  'is_deleted', 'image', 'is_active','last_update_dt');

	}

    /*
     * Create new service or update existing theme
     */
    function saveService($POST,$user_id){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        if($this->Data['meta_name']==''){
            $this->Data['meta_name']=$this->Data['name'];
        }

        if($this->Data['meta_keyword']==''){
            $this->Data['meta_keyword']=$this->Data['name'];
        }

        if($this->Data['meta_description']==''){
            $this->Data['meta_description']=$this->Data['name'];
        }

        if($this->Data['urlname']==''){
            $this->Data['urlname']=$this->_sanitize($this->Data['name']);
        }

        $this->Data['user_id']=$user_id;
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';


        $rand=rand(0, 99999999);
        $image_obj=new imageManipulation();
       
        if($image_obj->upload_photo('service', $_FILES['image'], $rand)):
                $this->Data['image']=$image_obj->makeFileName($_FILES['image']['name'], $rand);
        endif;
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){


            if($this->Update())
              return $this->Data['id'];
        }
        else{
            $this->Data['date_added']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get service by id
     */
    function getService($id){
        return $this->_getObject('user_service', $id);
    }


    /*
     * Get service by id
     */
    function getServiceByID($id,$show_active=1){
        if($show_active)
            $this->Where="where id='$id' AND user_id='$this->user_id' AND is_deleted='0' AND is_active='1'";
        else
            $this->Where="where id='$id' AND user_id='$this->user_id' AND is_deleted='0'";
        return $this->DisplayOne();
    }
    
    
    
    
    /*
     * Get List of all service in array
     */
    function listServices($show_active=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($show_active)
			$this->Where="where user_id='$this->user_id' AND is_deleted='0' AND is_active='1'  ORDER BY position asc";
		else
			$this->Where="where user_id='$this->user_id' AND is_deleted='0'  ORDER BY position asc";

		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }

    function listLimitedActiveServices($result_type='array',$limit=5){
		if($limit)
		     $this->Where="where user_id='$this->user_id' AND is_deleted='0' AND is_active='1'  ORDER BY position asc LIMIT 0,$limit";
		else
                     $this->Where="where user_id='$this->user_id' AND is_deleted='0' AND is_active='1'  ORDER BY position asc ";
                
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }
    
  

    /*
     * delete a theme by id
     */
    function deleteService($id){
        $this->id=$id;
        if(SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

    /*
     * Update theme position
     */

    function updatePosition($position, $id){
        $this->Data['id']=$id;
        $this->Data['position']=($position!='')?$position:0;
        $this->Update();
    }

     function setUserId($id){
        $this->user_id=mysql_real_escape_string($id);
    }
    
    /*
     * Fetch all deleted events - which have "is_deleted" set to "1"
     */
    function getThrash(){
          $this->Where="where user_id='$this->user_id' AND is_deleted='1'";
          $this->DisplayAll();
    }

   
}
?>