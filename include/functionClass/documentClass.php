<?php

class document extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('document_master');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'type', 'name', 'urlname', 'short_description', 'format', 'create_date', 'is_deleted', 'is_active');
    }

    /*
     * Create new document or update existing theme
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['create_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get document by id
     */

    function getRecord($id) {
        return $this->_getObject('document_master', $id);
    }

    /*
     * Get List of all document in array
     */

    function listAll($show_active = 0, $result_type = 'object') {

        if ($show_active)
            $this->Where = "where is_deleted='0' and is_active='1'  ORDER BY id asc";
        else
            $this->Where = "where is_deleted='0' ORDER BY position asc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    function listAllWithName($show_active = 0) {
        $query = new query('document_master,document_master_cat');
        $query->Field = ('document_master.*,document_master_cat.name as doc_type');
        if ($show_active):
            $query->Where = "where document_master.is_active='1' and document_master.is_deleted='0' and document_master.type=document_master_cat.id ORDER BY document_master.name asc";
        else:
            $query->Where = "where document_master.type=document_master_cat.id ORDER BY document_master.name asc";
        endif;
        return $query->ListOfAllRecords('object');
    }

    function listUnderCatName($show_active = 0) {
        $result = array();
        ;
        $query = new query('document_master_cat');
        $query->Where = "where id!='0'order by name asc";
        $query->DisplayAll();
        if ($query->GetNumRows()):
            while ($Obj = $query->GetObjectFromRecord()):
                $record = array();
                $query_new = new query('document_master');
                $query_new->Field = ('id,name,create_date');
                $query_new->Where = "where type='" . $Obj->id . "' and is_active='1' and is_deleted='0' order by name asc";
                $record = $query_new->ListOfAllRecords('object');
                $result[$Obj->name] = $record;
            endwhile;
        endif;

        return $result;

        $query = new query('document_master,document_master_cat');
        $query->Field = ('document_master.*,document_master_cat.name as doc_type');
        if ($show_active):
            $query->Where = "where document_master.is_active='1' and document_master.is_deleted='0' and document_master.type=document_master_cat.id ORDER BY document_master.name asc";
        else:
            $query->Where = "where document_master.type=document_master_cat.id ORDER BY document_master.name asc";
        endif;
        $query->print = 1;
        return $query->ListOfAllRecords('object');
    }

    /*
     * delete a document by id
     */

    function deleteRecord($id) {
        echo $id;
        $this->id = $id;
        return $this->Delete();
    }

    /*
     * Update document position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

}

class SchoolDocumentTemplate extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_document_template');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'document_id', 'on_date');
    }

    /* Create new document rel */

    function saveData($POST, $school) {
        if (!empty($POST)):

            foreach ($POST as $key => $value):
                $newQuery = new query('school_document_template');
                $newQuery->Data['school_id'] = $school;
                $newQuery->Data['document_id'] = $value;
                $newQuery->Data['on_date'] = date('Y-m-d');
                $newQuery->Insert();
            endforeach;
        endif;
        return true;
    }

    function listAll($school = 0, $result_type = 'object') {
        $query = new query('document_master,school_document_template');
        $query->Field = ('document_master.*');
        $query->Where = "where school_document_template.school_id='" . mysql_real_escape_string($school_id) . "' and school_document_template.document_id=document_master.id ORDER BY document_master.name asc";
        return $query->ListOfAllRecords('object');
    }

    function get_school_single_document($school_id = 0, $result_type) {
        $query = new query('document_master,school_document_template');
        $query->Field = ('document_master.id,document_master.type');
        $query->Where = "where school_document_template.school_id='" . mysql_real_escape_string($school_id) . "' and document_master.type='" . mysql_real_escape_string($result_type) . "' and school_document_template.document_id=document_master.id ORDER BY document_master.name asc";
        //$query->print=1;
        return $query->DisplayOne();
    }

    function getSchoolDocumentTemplate($school = 0, $result_type = 'object') {
        $records = array();
        $this->Where = "where school_id='" . $school . "' ORDER BY id asc";

        $this->DisplayAll();
        if ($this->GetNumRows()):
            while ($Obj = $this->GetObjectFromRecord()):
                $records[] = $Obj->document_id;
            endwhile;
        endif;
        return $records;
    }

    /* delete a School SMS by id */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /* delete a School SMS by id */

    function deleteSchoolRecord($school) {
        /* Check for previous value */
        $newQuery = new query('school_document_template');
        $newQuery->Where = "where school_id='" . $school . "'";
        $newQuery->Delete_where();
        return;
    }

}

#Get Dccument Master Category

class DocumentMasterCategory extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('document_master_cat');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'remarks');
    }

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function listAll($result_type = 'object') {

        $this->Where = "where id!='0' ORDER BY id asc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

}

#school document History

class SchoolDocumentPrintHistory extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_document_print_history');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'student_id', 'staff_id', 'doc_type', 'session_id', 'section', 'document_id', 'document_text', 'on_date');
    }

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getLastUpdateDoc($school, $session, $section, $doc, $student) {
        $this->Where = "where school_id='" . $school . "' and session_id='" . $session . "' and section='" . $section . "' and document_id='" . $doc . "' and student_id='" . $student . "' and document_text is NOT NULL order by on_date desc";
        return $this->DisplayOne();
    }

    function getLastPrintDoc($school, $session, $section, $doc, $student) {
        $this->Where = "where school_id='" . $school . "' and session_id='" . $session . "' and section='" . $section . "' and document_id='" . $doc . "' and student_id='" . $student . "' order by on_date desc";
        //$this->print=1;
        return $this->DisplayOne();
    }

    function listAll($school_id = 0, $result_type = 'object') {
        $query = new query('document_master,school_document_print_history');
        $query->Field = ('document_master.name,document_master.type,school_document_print_history.on_date');
        $query->Where = "where school_document_print_history.school_id='" . mysql_real_escape_string($school_id) . "' and school_document_print_history.document_id=document_master.id ORDER BY school_document_print_history.on_date desc";
        return $query->ListOfAllRecords('object');
    }

    function listPrintingdocs($school_id, $student_id, $session = 0, $section = 0) {
        $query = new query('document_master,school_document_print_history,session,document_master_cat');
        $query->Field = ('document_master_cat.name,document_master.type,school_document_print_history.id as his_id,school_document_print_history.on_date,school_document_print_history.section,session.title as session_name');
        if (!empty($session) && !empty($section)):
            $query->Where = "where school_document_print_history.student_id='" . mysql_real_escape_string($student_id) . "' and school_document_print_history.school_id='" . mysql_real_escape_string($school_id) . "' and school_document_print_history.session_id='" . mysql_real_escape_string($session) . "' and school_document_print_history.section='" . mysql_real_escape_string($section) . "' and document_master_cat.id=document_master.type and school_document_print_history.document_id=document_master.id and school_document_print_history.session_id=session.id ORDER BY school_document_print_history.on_date desc";
        elseif (!empty($session)):
            $query->Where = "where school_document_print_history.student_id='" . mysql_real_escape_string($student_id) . "' and school_document_print_history.school_id='" . mysql_real_escape_string($school_id) . "' and school_document_print_history.session_id='" . mysql_real_escape_string($session) . "' and document_master_cat.id=document_master.type and school_document_print_history.document_id=document_master.id and school_document_print_history.session_id=session.id ORDER BY school_document_print_history.on_date desc";
        else:
            $query->Where = "where school_document_print_history.student_id='" . mysql_real_escape_string($student_id) . "' and school_document_print_history.school_id='" . mysql_real_escape_string($school_id) . "' and document_master_cat.id=document_master.type and school_document_print_history.document_id=document_master.id and school_document_print_history.session_id=session.id ORDER BY school_document_print_history.on_date desc";
        endif;

        return $query->ListOfAllRecords('object');
    }

    /* delete a School print by id */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /* delete a School SMS by id */

    function deleteSchoolRecord($school) {
        /* Check for previous value */
        $newQuery = new query('school_document_print_history');
        $newQuery->Where = "where school_id='" . $school . "'";
        $newQuery->Delete_where();
        return;
    }

}

class SchoolDocumentIndicators extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_document_indicators');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'title', 'sem', 'type');
    }

    function saveIndicators($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function listIndicators($school_id, $sem = '', $type = '') {
        if ($sem != '') {
            $this->Where = "where school_id='$school_id' and sem='$sem' and type='$type'";
        } else {
            $this->Where = "where school_id='$school_id'";
        }
        return $this->ListOfAllRecords('object');
    }

    /* Get Tndicator by id */

    function getRecord($id) {
        return $this->_getObject('school_document_indicators', $id);
    }

}

?>