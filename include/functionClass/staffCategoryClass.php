<?php

class staffCategory extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('staff_category_master');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'school_id','name', 'urlname', 'description', 'position','is_deleted', 'is_active');

	}

    /*
     * Create new Staff Category or update existing theme
     */
    function saveData($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        if(isset($this->Data['urlname']) && $this->Data['urlname']==''):
             $this->Data['urlname']=$this->_sanitize($this->Data['name']);
        elseif(!isset($this->Data['urlname'])):
             $this->Data['urlname']=$this->_sanitize($this->Data['name']);
        endif;
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';

        if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              return $this->Data['id'];
        }
        else{            
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get Staff Category by id
     */
    function getStaffCategory($id){
        return $this->_getObject('staff_category_master', $id);
    }

    
    /*
     * Get List of all Staff Category in array
     */
    function listAll($school_id,$show_active=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($show_active)
			$this->Where="where school_id='".mysql_real_escape_string($school_id)."' and is_deleted='0' and is_active='1'  ORDER BY position asc";
		else
			$this->Where="where school_id='".mysql_real_escape_string($school_id)."' and is_deleted='0' ORDER BY position asc";
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }

  

    /*
     * delete a Staff Category by id
     */
    function deleteRecord($id){
           $this->id=mysql_real_escape_string($id);
            return $this->Delete();
    }

    /*
     * Update Staff Category position
     */
    function updatePosition($position, $id){
        $this->Data['id']=$id;
        $this->Data['position']=($position!='')?$position:0;
        $this->Update();
    }

}
?>