<?php

class schoolSubscription extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='id'){
        parent::__construct('school_subscription');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'school_id','subscription_id','create_date','for_days','set_up_fee','subscription_fee','payment_type', 'is_deleted','is_active');

	}

    /*
     * Create new school subscription or update existing school
     */
    function saveData($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active']=1;
        
        if(isset($this->Data['id']) && $this->Data['id']!=''){
            $this->Data['update_date']=date('Y-m-d');
            if($this->Update())
              return $this->Data['id'];
        }
        else{  
            $this->Data['create_date']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get school subscription by id
     */
    function getSchoolSubscription($id){
        return $this->_getObject('school_subscription', $id);
    }

    
    /*
     * Get List of all school subscription in array
     */
    function listAll($show_active=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($show_active)
			$this->Where="where is_deleted='0' and is_active='1'  ORDER BY position asc";
		else
			$this->Where="where is_deleted='0' ORDER BY position asc";
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }

  

    /*
     * delete a school subscription by id
     */
    function deleteRecord($id){
        $this->id=$id;
            return $this->Delete();
    }

     function delete_where_school($id){
            $QueryObj= new query('school_subscription');
            $QueryObj->Where="where school_id='".mysql_real_escape_string($id)."'";
           
            $QueryObj->Delete_where();
            return true;

    }
    
   
}
?>