<?php

class schoolSetting extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='id'){
        parent::__construct('school_setting');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'school_id','reg_id_prefix','is_sms_allowed','is_email_allowed', 'max_courses_allowed','max_sections_per_session','max_teacher_allowed', 'max_student_per_session','max_sms_limit','max_email_limit','payment_reminder','receipt_prefix','receipt_start_no');
	}

    /* Create new Settings */
    function saveData($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              return $this->Data['id'];
        }
        else{            
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get School Settings by id
     */
    function getPaymentRecord($id){
        return $this->_getObject('school_setting', $id);
    }


    function listAll($school=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($school)
			$this->Where="where school_id='".$school."' ORDER BY id asc";
		else
			$this->Where="where id!='0' ORDER BY id asc";
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }
    
    /* get School Setting by schoo id */
    function getSchoolSetting($school_id){
			$this->Where="where school_id='".$school_id."' ORDER BY id asc";
			return $this->DisplayOne();		
    }
    
    /* */
    function checkSchoolPrefix($school_id,$name){
			$this->Where="where school_id!='".$school_id."' and reg_id_prefix='".$name."' ORDER BY id asc";
			return $this->DisplayOne();		
    }    

    /* delete a School Setting by id */
    function deleteRecord($id){
        $this->id=$id;        
            return $this->Delete();
    }


   
}
?>