<?php

class staff extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('staff');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'title', 'first_name', 'last_name', 'sex', 'marital', 'designation', 'permanent_address1', 'permanent_address2', 'permanent_city', 'permanent_state', 'permanent_country', 'permanent_zip', 'correspondence_address1', 'correspondence_address2', 'correspondence_city', 'correspondence_state', 'correspondence_country', 'correspondence_zip', 'email', 'date_of_birth', 'phone', 'mobile', 'photo', 'salary', 'subject', 'join_date', 'quit_date', 'is_quit', 'staff_category', 'is_deleted', 'is_active', 'staff_login_id', 'password', 'msg_notif_email', 'earning_heads', 'deduction_heads');
    }

    /* Create new Staff or update existing  */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['date_of_birth'] = date('Y-m-d', strtotime($this->Data['date_of_birth']));
        $this->Data['join_date'] = date('Y-m-d', strtotime($this->Data['join_date']));
        if (isset($this->Data['quit_date']) && !empty($this->Data['quit_date'])):
            $this->Data['quit_date'] = date('Y-m-d', strtotime($this->Data['quit_date']));
        else:
            $this->Data['quit_date'] = '';
        endif;

        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';
        $this->Data['is_quit'] = isset($this->Data['is_quit']) ? '1' : '0';
        if ($_FILES['image']):
            $rand = rand(0, 99999999);
            $image_obj = new imageManipulation();

            if ($image_obj->upload_photo('staff', $_FILES['image'], $rand)):
                $this->Data['photo'] = $image_obj->makeFileName($_FILES['image']['name'], $rand);
            endif;
        endif;

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /* Get Staff Staff by id */

    function getStaff($id) {
        return $this->_getObject('staff', $id);
    }

    function getStaff_detail($id) {
        $query = new query('staff,staff_category_master,staff_designation_master,school');
        $query->Field = ('staff.id,staff.school_id,staff.title,staff.first_name,staff.last_name,staff.sex,staff.marital,staff.permanent_address1,staff.permanent_address2,staff.permanent_city,staff.permanent_state,staff.permanent_country,staff.permanent_zip,staff.correspondence_address1,staff.correspondence_address2,staff.correspondence_city,staff.correspondence_state,staff.correspondence_country,staff.correspondence_zip,staff.email,staff.date_of_birth,staff.phone,staff.mobile,staff.photo,staff.salary,staff.subject,staff.join_date,staff.quit_date,staff.is_active,staff.is_deleted,staff_category_master.name as staff_category,staff_designation_master.name as designation,school.school_name,school.logo as school_logo');
        $query->Where = "where staff.id='" . mysql_real_escape_string($id) . "' and staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id and staff.school_id=school.id ORDER BY staff.id asc";

        return $query->DisplayOne();
    }

    function deleteImage($id) {
        $this->Data['photo'] = '';
        $this->Data['id'] = $id;

        return $this->Update();
    }

    /* Get List of all Staff  */

    function getSchoolStaff($school_id, $show_active = 0, $result_type = 'object') {
        if ($show_active)
            $this->Where = "where is_deleted='0' and school_id='" . mysql_real_escape_string($school_id) . "' and is_active='1'  ORDER BY id asc";
        else
            $this->Where = "where is_deleted='0' and school_id='" . mysql_real_escape_string($school_id) . "' ORDER BY id asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    function getStaffList($school_id) {
        $this->Where = "where is_active='1' and is_quit='0'";
        return $this->ListOfAllRecords('object');
    }

    /* Get List of all Staff  */

    function getSchoolStaff_detail($school_id, $show_active = 0, $result_type = 'object') {
        $query = new query('staff,staff_category_master,staff_designation_master');
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff.sex,staff.marital,staff.permanent_address1,staff.permanent_address2,staff.permanent_city,staff.permanent_state,staff.permanent_country,staff.permanent_zip,staff.correspondence_address1,staff.correspondence_address2,staff.correspondence_city,staff.correspondence_state,staff.correspondence_country,staff.correspondence_zip,staff.email,staff.date_of_birth,staff.phone,staff.mobile,staff.photo,staff.salary,staff.subject,staff.join_date,staff.quit_date,staff.is_active,staff.is_deleted,staff_category_master.name as staff_category,staff_designation_master.name as designation');
        if ($show_active):
            $query->Where = "where staff.is_deleted='0' and staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.is_active='1' and staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id ORDER BY staff.id asc";
        else:
            $query->Where = "where staff.is_deleted='0' and staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id ORDER BY staff.id asc";
        endif;
        return $query->ListOfAllRecords('object');
    }

    /* Get List of all Staff  */

    function getSchoolStaffDetailActiveDeactive($school_id, $show_active = 0) {
        $query = new query('staff,staff_category_master,staff_designation_master');
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff.sex,staff.marital,staff.permanent_address1,staff.permanent_address2,staff.permanent_city,staff.permanent_state,staff.permanent_country,staff.permanent_zip,staff.correspondence_address1,staff.correspondence_address2,staff.correspondence_city,staff.correspondence_state,staff.correspondence_country,staff.correspondence_zip,staff.email,staff.date_of_birth,staff.phone,staff.mobile,staff.photo,staff.salary,staff.subject,staff.join_date,staff.quit_date,staff.is_active,staff.is_deleted,staff.staff_login_id,staff.password,staff_category_master.name as staff_category,staff_designation_master.name as designation');
        if ($show_active):
            $query->Where = "where staff.is_deleted='0' and staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.is_active='1' AND staff.is_quit='0' AND staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id ORDER BY staff.id asc";
        else:
            $query->Where = "where staff.is_deleted='0' and staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.is_active='0' AND staff.is_quit='0' AND staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id ORDER BY staff.id asc";
        endif;

        return $query->ListOfAllRecords('object');
    }

    /* Get List of all Staff  */

    function getSchoolOldStaffDetail($school_id, $show_active = 0) {
        $query = new query('staff,staff_category_master,staff_designation_master');
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff.sex,staff.marital,staff.permanent_address1,staff.permanent_address2,staff.permanent_city,staff.permanent_state,staff.permanent_country,staff.permanent_zip,staff.correspondence_address1,staff.correspondence_address2,staff.correspondence_city,staff.correspondence_state,staff.correspondence_country,staff.correspondence_zip,staff.email,staff.date_of_birth,staff.phone,staff.mobile,staff.photo,staff.salary,staff.subject,staff.join_date,staff.quit_date,staff.is_active,staff.is_deleted,staff_category_master.name as staff_category,staff_designation_master.name as designation');
        if ($show_active):
            $query->Where = "where staff.is_deleted='0' and staff.school_id='" . mysql_real_escape_string($school_id) . "' AND staff.is_quit='1' AND staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id ORDER BY staff.id asc";
        else:
            $query->Where = "where staff.is_deleted='0' and staff.school_id='" . mysql_real_escape_string($school_id) . "' AND staff.is_quit='1' AND staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id ORDER BY staff.id asc";
        endif;

        return $query->ListOfAllRecords('object');
    }

    /* Get List of all Staff  */

    function getSchoolStaffWithCategory($school_id, $cat = 0) {
        $query = new query('staff,staff_category_master,staff_designation_master');
        $query->Field = ('staff.*,staff_category_master.name as staff_category,staff_designation_master.name as designation');
        if ($cat):
            $query->Where = "where staff.is_deleted='0' and staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.staff_category='" . mysql_real_escape_string($cat) . "' and staff.is_active='1' and staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id ORDER BY staff.id asc";
        else:
            $query->Where = "where staff.is_deleted='0' and staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.is_active='1' and staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id ORDER BY staff.id asc";
        endif;
        return $query->ListOfAllRecords('object');
    }

    /* Get List of all Staff  */

    function getSchoolStaffFront($school_id, $p, $max_records) {
        $query = new query('staff,staff_category_master,staff_designation_master');
        $query->AllowPaging = 1;
        $query->PageNo = $p;
        $query->PageSize = $max_records;
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff.sex,staff.marital,staff.permanent_address1,staff.permanent_address2,staff.permanent_city,staff.permanent_state,staff.permanent_country,staff.permanent_zip,staff.correspondence_address1,staff.correspondence_address2,staff.correspondence_city,staff.correspondence_state,staff.correspondence_country,staff.correspondence_zip,staff.email,staff.date_of_birth,staff.phone,staff.mobile,staff.photo,staff.salary,staff.subject,staff.join_date,staff.quit_date,staff.is_active,staff.is_deleted,staff_category_master.name as staff_category,staff_designation_master.name as designation');
        $query->Where = "where staff.is_deleted='0' and staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.is_active='1' and staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id ORDER BY staff_category_master.name asc";

        $staff_array = $query->ListOfAllRecords('object');
        $staff['staff'] = $staff_array;
        $staff['TotalRecords'] = $query->TotalRecords;
        $staff['TotalPages'] = $query->TotalPages;
        return $staff;
    }

    function getSchoolStaffFrontRandomly($school_id, $total) {
        $query = new query('staff,staff_category_master,staff_designation_master');
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff.sex,staff.marital,staff.permanent_address1,staff.permanent_address2,staff.permanent_city,staff.permanent_state,staff.permanent_country,staff.permanent_zip,staff.correspondence_address1,staff.correspondence_address2,staff.correspondence_city,staff.correspondence_state,staff.correspondence_country,staff.correspondence_zip,staff.email,staff.date_of_birth,staff.phone,staff.mobile,staff.photo,staff.salary,staff.subject,staff.join_date,staff.quit_date,staff.is_active,staff.is_deleted,staff_category_master.name as staff_category,staff_designation_master.name as designation');
        $query->Where = "where staff.is_deleted='0' and staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.is_active='1' and staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id ORDER BY rand() LIMIT 0," . $total . "";
        $staff_array = $query->ListOfAllRecords('object');
        return $staff_array;
    }

    function getStaffDetail($school_id, $staff_id) {
        $query = new query('staff,staff_category_master,staff_designation_master');
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff.sex,staff.marital,staff.permanent_address1,staff.permanent_address2,staff.permanent_city,staff.permanent_state,staff.permanent_country,staff.permanent_zip,staff.correspondence_address1,staff.correspondence_address2,staff.correspondence_city,staff.correspondence_state,staff.correspondence_country,staff.correspondence_zip,staff.email,staff.date_of_birth,staff.phone,staff.mobile,staff.photo,staff.salary,staff.subject,staff.join_date,staff.quit_date,staff.is_active,staff.is_deleted,staff_category_master.name as staff_category,staff_designation_master.name as designation');
        $query->Where = "where staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.id='" . mysql_real_escape_string($staff_id) . "' and staff.designation=staff_designation_master.id and staff.staff_category=staff_category_master.id";
        $staff = $query->DisplayOne();
        return $staff;
    }

    /* Get List of all Staff  */

    function getCatStaff($school, $id) {
        $query = new query('staff,staff_category_master');
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff.sex,staff.marital,staff.permanent_address1,staff.permanent_address2,staff.permanent_city,staff.permanent_state,staff.permanent_country,staff.permanent_zip,staff.correspondence_address1,staff.correspondence_address2,staff.correspondence_city,staff.correspondence_state,staff.correspondence_country,staff.correspondence_zip,staff.email,staff.date_of_birth,staff.phone,staff.mobile,staff.photo,staff.salary,staff.subject,staff.join_date,staff.quit_date,staff.is_active,staff.is_deleted,staff_category_master.name as staff_category');

        $query->Where = "where staff.is_deleted='0' and staff.is_active='1' AND staff.is_quit='0' and staff.school_id='" . mysql_real_escape_string($school) . "' and staff.staff_category='" . mysql_real_escape_string($id) . "' and staff.staff_category=staff_category_master.id ORDER BY staff.id asc";

        return $query->ListOfAllRecords('object');
    }

    /* delete a Staff by id   */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    function getLastDatePrinting($school_id, $staff_id, $doc_id) {
        $query = new query('school_document_print_history');
        $query->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and staff_id='" . mysql_real_escape_string($staff_id) . "' and document_id='" . mysql_real_escape_string($doc_id) . "' order by on_date desc";
        $rec = $query->DisplayOne();
        if (is_object($rec)):
            return $rec->on_date;
        else:
            return false;
        endif;
    }

    function getLoginStaffInformation($login_user_id) {
        $this->Where = "WHERE `id`= '" . $login_user_id . "'";
        return $this->DisplayOne();
    }

    // Staff basic information

    function staffBasicInformation($staff_id) {
        $this->Where = "WHERE `id`= '" . $staff_id . "'";
        $s_info = $this->DisplayOne();
        return $s_info;
    }

    // staff Designation using designation(designation is id stored in staff table)

    public static function staffDesignationByDesignation($designation) {
        $query = new staff;
        $query->TableName = "staff_designation_master";
        $query->Where = "WHERE `id` = '$designation'";
        return $query->DisplayOne();
    }

    function notificationEnabaleDisable($id) {
        if (isset($_POST['msg_notif_email'])) {
            $this->Data['msg_notif_email'] = 1;
        } else {
            $this->Data['msg_notif_email'] = 0;
        }
        $this->Where = "WHERE `id` = '$id'";
        $this->UpdateCustom();
    }

    function sendEmailIfNotificationEnable($id) {
        $this->Where = "WHERE `msg_notif_email` = 1 AND `id` = '$id'";
        return $this->DisplayOne();
    }

    function getStaffDOB($school_id) {
        $current_month = date('m');
        $this->Where = "WHERE MONTH(`date_of_birth`) BETWEEN $current_month AND 12 ORDER BY DAY(date_of_birth) ASC";
        return $this->ListOfAllRecords();
    }

}

/* Staff Experience Class */

class staffExperience extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('staff_experience');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'staff_id', 'job_title', 'designation', 'from_date', 'to_date', 'department');
    }

    /* Create new Staff Experience or update existing  */

    function saveData($POST, $staff_id) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (!empty($POST)):
            foreach ($POST as $key => $value):

                if (!empty($value['department']) && !empty($value['designation']) && !empty($value['from_date']) && !empty($value['to_date'])):
                    $ObjQuery = new query('staff_experience');
                    $ObjQuery->Data['department'] = $value['department'];
                    $ObjQuery->Data['designation'] = $value['designation'];
                    $ObjQuery->Data['from_date'] = date('Y-m-d', strtotime($value['from_date']));
                    $ObjQuery->Data['to_date'] = date('Y-m-d', strtotime($value['to_date']));
                    $ObjQuery->Data['staff_id'] = $staff_id;
                    if (isset($value['id'])):
                        $ObjQuery->Data['id'] = $value['id'];
                        $ObjQuery->Update();
                    else:
                        $ObjQuery->Insert();
                    endif;
                endif;
            endforeach;
        endif;
        return true;
    }

    /* Get Staff Staff Experience by id */

    function getStaffExperience($id) {
        return $this->_getObject('staff_experience', $id);
    }

    /* Get List of all Staff  Experience */

    function getStaffExp($staff_id, $result_type = 'object') {

        $this->Where = "where staff_id='" . mysql_real_escape_string($staff_id) . "' ORDER BY id asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* delete a Staff by id Experience */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

/* Staff Qualification Class */

class staffQualification extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('staff_qualification');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'staff_id', 'qualification', 'institute', 'year_of_passing', 'percentage');
    }

    /* Create new Staff Qualification or update existing  */

    function saveData($POST, $staff_id) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (!empty($POST)):
            foreach ($POST as $key => $value):

                if (!empty($value['qualification']) && !empty($value['institute']) && !empty($value['percentage']) && !empty($value['year_of_passing'])):
                    $ObjQuery = new query('staff_qualification');
                    $ObjQuery->Data['qualification'] = $value['qualification'];
                    $ObjQuery->Data['institute'] = $value['institute'];
                    $ObjQuery->Data['percentage'] = $value['percentage'];
                    $ObjQuery->Data['year_of_passing'] = $value['year_of_passing'];
                    $ObjQuery->Data['staff_id'] = $staff_id;
                    if (isset($value['id'])):
                        $ObjQuery->Data['id'] = $value['id'];
                        $ObjQuery->Update();
                    else:
                        $ObjQuery->Insert();
                    endif;

                endif;
            endforeach;
        endif;
        return true;
    }

    /* Get Staff Staff Qualification by id */

    function getQualification($id) {
        return $this->_getObject('staff_qualification', $id);
    }

    /* Get List of all Staff  Qualification */

    function getStaffQualification($staff_id, $result_type = 'object') {

        $this->Where = "where staff_id='" . mysql_real_escape_string($staff_id) . "' ORDER BY id asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* delete a Staff by id Qualification */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    // get staff qualification

    function getStaffQualification2($id) {
        $this->Where = "WHERE `staff_id`= '" . $id . "'";
        $s_qualification = $this->DisplayOne();
        return $s_qualification;
    }

}

/* Staff Documents Class */

class staffDocument extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('staff_document');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'staff_id', 'document_title', 'file');
    }

    /* Create new Staff Documents or update existing  */

    function saveData($POST, $staff_id) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (!empty($POST)):
            foreach ($POST as $key => $value):
                if (!empty($value['document_title'])):
                    /* Make Array to file upload */
                    $_FILES['document'] = array();
                    $_FILES['document'] = array('name' => $_FILES['file']['name'][$key], 'type' => $_FILES['file']['type'][$key], 'tmp_name' => $_FILES['file']['tmp_name'][$key], 'error' => $_FILES['file']['error'][$key], 'size' => $_FILES['file']['size'][$key]);

                    $rand = rand(0, 99999999);
                    $file_obj = new file();
                    if ($file_obj->uploadDocument('document', $_FILES['document'], $rand)):
                        $ObjQuery = new query('staff_document');
                        $ObjQuery->Data['file'] = $this->_sanitize($file_obj->makeFileName($_FILES['document']['name'], $rand));
                        $ObjQuery->Data['document_title'] = $value['document_title'];
                        $ObjQuery->Data['staff_id'] = $staff_id;
                        $ObjQuery->Insert();
                    endif;
                endif;
            endforeach;
        endif;
        return true;
    }

    /* Get Staff Staff Documents by id */

    function getSDocument($id) {
        return $this->_getObject('staff_document', $id);
    }

    /* Get List of all Staff  Qualification */

    function getStaffDocument($school_id, $result_type = 'object') {

        $this->Where = "where staff_id='" . mysql_real_escape_string($school_id) . "' ORDER BY id asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* delete a Staff by id Documents */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

# Staff Salary

class staffSalary extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('staff_salary');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    /*
     * Create new staff salary list 
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
//        $this->Data['amount'] = str_replace(',', '', $this->Data['amount']);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function check_for_the_month_salary($date, $staff_id, $id = '0') {
        if ($id)
            $this->Where = "where id!='" . mysql_real_escape_string($id) . "' and staff_id='" . mysql_real_escape_string($staff_id) . "' and month(payment_date)=month('" . mysql_real_escape_string($date) . "') and year(payment_date)=year('" . mysql_real_escape_string($date) . "') order by id desc";
        else
            $this->Where = "where staff_id='" . mysql_real_escape_string($staff_id) . "' and month(payment_date)=month('" . mysql_real_escape_string($date) . "') and year(payment_date)=year('" . mysql_real_escape_string($date) . "') order by id desc";
        return $this->DisplayOne();
    }

    function getRecord($id) {
        $query = new query('staff,staff_salary,staff_category_master');
        $query->Field = ('staff.id as staff_id,staff.title,staff.first_name,staff.last_name,staff.sex,staff_category_master.id as cat_id,staff_category_master.name as category,staff_salary.*');

        $query->Where = "where staff_salary.id='" . mysql_real_escape_string($id) . "' and staff.id=staff_salary.staff_id and staff.staff_category=staff_category_master.id ORDER BY staff_salary.payment_date desc";

        return $query->DisplayOne();
    }

    function getMaxReciept($school_id) {
        $last = '0';
        $this->Field = ('MAX(receipt_no) as last');
        $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' ";
        $max = $this->DisplayOne();
        if (!empty($max->last)):
            $last = $max->last + 1;
        else:
            $last = ($last + 1);
        endif;
        return $last;
    }

    /* Get List of all Staff  */

    function ListAll($school_id) {
        $query = new query('staff,staff_salary,staff_category_master');
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff.sex,staff_category_master.name as category,staff_salary.amount,staff_salary.payment_date,staff_salary.id as payment_id');

        $query->Where = "where staff_salary.school_id='" . mysql_real_escape_string($school_id) . "' and staff.id=staff_salary.staff_id and staff.staff_category=staff_category_master.id ORDER BY staff_salary.payment_date desc";

        return $query->ListOfAllRecords('object');
    }

    function getSingleSraffSalary($school_id, $id) {
        $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and staff_id='" . mysql_real_escape_string($id) . "' order by id desc";

        return $this->ListOfAllRecords('object');
    }

    /*
     * delete a Payment by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    function SalaryReport($school_id, $from, $to, $cat = 0) {

        $query = new query('staff,staff_salary,staff_category_master');
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff_category_master.name as category,staff_salary.amount,staff_salary.payment_date,staff_salary.remarks,staff_salary.id as payment_id');

        if ($cat):
            $query->Where = "where staff_salary.school_id='" . mysql_real_escape_string($school_id) . "' and staff_salary.payment_date>='" . mysql_real_escape_string($from) . "' and staff_salary.payment_date<='" . mysql_real_escape_string($to) . "' and staff.staff_category='" . mysql_real_escape_string($cat) . "' and staff.id=staff_salary.staff_id and staff.staff_category=staff_category_master.id ORDER BY staff_salary.payment_date desc";
        else:
            $query->Where = "where staff_salary.school_id='" . mysql_real_escape_string($school_id) . "' and staff_salary.payment_date>='" . mysql_real_escape_string($from) . "' and staff_salary.payment_date<='" . mysql_real_escape_string($to) . "' and staff.id=staff_salary.staff_id and staff.staff_category=staff_category_master.id ORDER BY staff_salary.payment_date desc";
        endif;

        return $query->ListOfAllRecords('object');
    }

    function SalaryReportDownload($school_id, $from, $to, $cat = 0) {
        $record = array();
        $orders_arr = array();
        $query = new query('staff,staff_salary,staff_category_master');
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff_category_master.name as category,staff_salary.amount,staff_salary.payment_date,staff_salary.remarks,staff_salary.id as payment_id');

        if ($cat):
            $query->Where = "where staff_salary.school_id='" . mysql_real_escape_string($school_id) . "' and staff_salary.payment_date>='" . mysql_real_escape_string($from) . "' and staff_salary.payment_date<='" . mysql_real_escape_string($to) . "' and staff.staff_category='" . mysql_real_escape_string($cat) . "' and staff.id=staff_salary.staff_id and staff.staff_category=staff_category_master.id ORDER BY staff_salary.payment_date desc";
        else:
            $query->Where = "where staff_salary.school_id='" . mysql_real_escape_string($school_id) . "' and staff_salary.payment_date>='" . mysql_real_escape_string($from) . "' and staff_salary.payment_date<='" . mysql_real_escape_string($to) . "' and staff.id=staff_salary.staff_id and staff.staff_category=staff_category_master.id ORDER BY staff_salary.payment_date desc";
        endif;
        $record = $query->ListOfAllRecords('object');

        if (!empty($record)): $sr = 1;
            $total = '0';
            $filename = "SalaryReport.csv";

            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;filename=$filename");

            if ($cat): $category = get_object('staff_category_master', $cat);
                echo "Category Name " . "\t" . $category->name . "\n";
            endif;
            echo "From Date" . "\t" . $from . "\n";
            echo "To Date " . "\t" . $to . "\n \n";
            echo 'Sr. No.' . "\t" . 'Staff Name' . "\t" . 'Category' . "\t" . 'Date' . "\t" . 'Amount' . "\n";

            foreach ($record as $key => $value):
                echo $sr . ".\t" . $value->title . ' ' . $value->first_name . ' ' . $value->last_name . ' ' . "\t" . $value->category . "\t" . $value->payment_date . "\t" . number_format($value->amount, 2) . "\t\n";
                $total = $total + $value->amount;
                $sr++;
            endforeach;
            echo '' . "\t" . '' . "\t" . '' . "\t" . 'Total' . "\t" . '' . number_format($total, 2) . "\n";
            exit;
        endif;
    }

    function SalaryReportMonthWise($school_id, $month, $s_report_type, $cat = 0) {
        $date = explode('-', $month);
        $month = $date['0'];
        $year = $date['1'];
        if ($s_report_type == 'recieve'):
            $query = new query('staff,staff_salary,staff_category_master');

            if ($cat):
                $query->Field = ('staff.*,staff_category_master.name as category,staff_salary.amount,staff_salary.deduction,staff_salary.payment_date,staff_salary.remarks,staff_salary.id as payment_id');
                $query->Where = "where staff_salary.school_id='" . mysql_real_escape_string($school_id) . "' 
                            AND MONTH(staff_salary.month_year)= '" . $month . "'
                            AND YEAR(staff_salary.month_year) = '" . $year . "'                                
                            AND staff.id=staff_salary.staff_id and staff.staff_category='" . mysql_real_escape_string($cat) . "' and staff.staff_category=staff_category_master.id ORDER BY staff_salary.payment_date desc";
            else:
                $query->Field = ('staff.*,staff_category_master.name as category,staff_salary.amount,staff_salary.deduction,staff_salary.payment_date,staff_salary.remarks,staff_salary.id as payment_id');
                $query->Where = "where staff_salary.school_id='" . mysql_real_escape_string($school_id) . "' 
                            AND MONTH(staff_salary.month_year)= '" . $month . "'
                            AND YEAR(staff_salary.month_year) = '" . $year . "'                                
                            AND staff.id=staff_salary.staff_id and staff.staff_category=staff_category_master.id ORDER BY staff_salary.payment_date desc";
            endif;
            return $query->ListOfAllRecords('object');
        else:
            $query = new query('staff,staff_category_master');

            if ($cat):
                $query->Field = ('staff.*,staff_category_master.name as category');
                $query->Where = "where staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.staff_category='" . mysql_real_escape_string($cat) . "' 
                            AND staff.id NOT IN(SELECT staff_salary.staff_id FROM staff_salary where MONTH(staff_salary.month_year)= '" . $month . "' AND YEAR(staff_salary.month_year) = '" . $year . "' GROUP BY staff_salary.staff_id) and staff.staff_category=staff_category_master.id and staff.is_active='1' ORDER BY staff.id desc";

            else:
                $query->Field = ('staff.*,staff_category_master.name as category');
                $query->Where = "where staff.school_id='" . mysql_real_escape_string($school_id) . "' 
                            AND staff.id NOT IN(SELECT staff_salary.staff_id FROM staff_salary where MONTH(staff_salary.month_year)= '" . $month . "' AND YEAR(staff_salary.month_year) = '" . $year . "' GROUP BY staff_salary.staff_id) and staff.staff_category=staff_category_master.id and staff.is_active='1' ORDER BY staff.id desc";
            endif;
            return $query->ListOfAllRecords('object');
        endif;
    }

    function SalaryReportMonthWiseDownload($school_id, $monthYear, $s_report_type, $cat, $report_array) {
        $date = explode('-', $monthYear);
        $month = $date['0'];
        $year = $date['1'];
        if ($s_report_type == 'recieve'):
            $query = new query('staff,staff_salary,staff_category_master');

            if ($cat):
                $query->Field = ('staff.*,staff_category_master.name as category,staff_salary.amount,staff_salary.payment_date,staff_salary.remarks,staff_salary.id as payment_id');
                $query->Where = "where staff_salary.school_id='" . mysql_real_escape_string($school_id) . "' 
                            AND MONTH(staff_salary.payment_date )= '" . $month . "'
                            AND YEAR(staff_salary.payment_date ) = '" . $year . "'                                
                            AND staff.id=staff_salary.staff_id and staff.staff_category='" . mysql_real_escape_string($cat) . "' and staff.staff_category=staff_category_master.id ORDER BY staff_salary.payment_date desc";
            else:
                $query->Field = ('staff.*,staff_category_master.name as category,staff_salary.amount,staff_salary.payment_date,staff_salary.remarks,staff_salary.id as payment_id');
                $query->Where = "where staff_salary.school_id='" . mysql_real_escape_string($school_id) . "' 
                            AND MONTH(staff_salary.payment_date )= '" . $month . "'
                            AND YEAR(staff_salary.payment_date ) = '" . $year . "'                                
                            AND staff.id=staff_salary.staff_id and staff.staff_category=staff_category_master.id ORDER BY staff_salary.payment_date desc";
            endif;
            $record = $query->ListOfAllRecords('object');
        else:
            $query = new query('staff,staff_category_master');

            if ($cat):
                $query->Field = ('staff.*,staff_category_master.name as category');
                $query->Where = "where staff.school_id='" . mysql_real_escape_string($school_id) . "' and staff.staff_category='" . mysql_real_escape_string($cat) . "' 
                            AND staff.id NOT IN(SELECT staff_salary.staff_id FROM staff_salary where MONTH(staff_salary.payment_date )= '" . $month . "' AND YEAR(staff_salary.payment_date ) = '" . $year . "' GROUP BY staff_salary.staff_id) and staff.staff_category=staff_category_master.id and staff.is_active='1' ORDER BY staff.id desc";

            else:
                $query->Field = ('staff.*,staff_category_master.name as category');
                $query->Where = "where staff.school_id='" . mysql_real_escape_string($school_id) . "' 
                            AND staff.id NOT IN(SELECT staff_salary.staff_id FROM staff_salary where MONTH(staff_salary.payment_date )= '" . $month . "' AND YEAR(staff_salary.payment_date ) = '" . $year . "' GROUP BY staff_salary.staff_id) and staff.staff_category=staff_category_master.id and staff.is_active='1' ORDER BY staff.id desc";
            endif;
            $record = $query->ListOfAllRecords('object');
        endif;

        if (!empty($record)):
            if ($s_report_type == 'recieve'):
                $filename = $s_report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "Statement of " . $report_array[$s_report_type] . "\n\n";
                if (!empty($cat)):
                    $category = get_object('staff_category_master', $cat);
                    echo "Staff Category" . "\t" . $category->name . "\n";
                endif;
                if (!empty($monthYear)):
                    echo "Month And Year" . "\t" . date('M, Y', strtotime("01-" . $monthYear)) . "\n";
                endif;
                echo "\n";
                echo 'Sr. No.' . "\t" . 'Staff Name' . "\t" . 'Staff Category' . "\t" . 'Date' . "\t" . 'Amount' . "\n";
                $sr = '1';
                foreach ($record as $key => $value):
                    echo $sr . "\t" . $value->title . ' ' . ucfirst($value->first_name) . ' ' . $value->last_name . "\t" . $value->category . "\t" . $value->payment_date . "\t" . number_format($value->amount, 2) . "\t\n";
                    $sr++;
                endforeach;
                exit;
            else:
                $filename = $s_report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "Statement of " . $report_array[$s_report_type] . "\n\n";
                if (!empty($cat)):
                    $category = get_object('staff_category_master', $cat);
                    echo "Staff Category" . "\t" . $category->name . "\n";
                endif;
                if (!empty($monthYear)):
                    echo "Month And Year" . "\t" . date('M, Y', strtotime("01-" . $monthYear)) . "\n";
                endif;
                echo "\n";
                echo 'Sr. No.' . "\t" . 'Staff Name' . "\t" . 'Staff Category' . "\n";
                $sr = '1';
                foreach ($record as $key => $value):
                    echo $sr . "\t" . $value->title . ' ' . ucfirst($value->first_name) . ' ' . $value->last_name . "\t" . $value->category . "\t\n";
                    $sr++;
                endforeach;
                exit;
            endif;
        endif;
    }

    function SumSalaryReport($school_id, $from, $to) {

        $query = new query('staff_salary');
        $query->Field = ('SUM(staff_salary.amount) as amount');
        $query->Where = "where staff_salary.school_id='" . mysql_real_escape_string($school_id) . "' and staff_salary.payment_date>='" . mysql_real_escape_string($from) . "' and staff_salary.payment_date<='" . mysql_real_escape_string($to) . "'";
        return $query->ListOfAllRecords('object');
    }

    function getStaffSalaryRecords($id) {
        $this->Where = "WHERE `staff_id` = '" . $id . "'";
        return $this->ListOfAllRecords();
    }

    function getStaffSalaryRecordsById($id, $staff_id) {
        $this->Where = "WHERE `id` = '$id' AND `staff_id` = '" . $staff_id . "'";
        return $this->DisplayOne();
    }

}

# Staff Salary

class staffAttendance extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('staff_attendance');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'staff_id', 'date', 'status', 'on_date', 'update_date');
    }

    // Create new staff salary list 

    function saveAttendance($attendance) {
        #add attendance 
        if (!empty($attendance)):
            foreach ($attendance as $key => $value):
                #firstly check attendance
                $Query = new query('staff_attendance');
                $Query->Where = "where staff_id='" . $value['staff_id'] . "' and date='" . $value['date'] . "'";
                $rec = $Query->DisplayOne();

                if (is_object($rec)):
                    $Query = new query('staff_attendance');
                    $Query->Where = "where staff_id='" . $value['staff_id'] . "' and date='" . $value['date'] . "'";
                    $Query->Delete_where();

                    $Obj = new query('staff_attendance');
                    $Obj->Data['staff_id'] = $value['staff_id'];
                    $Obj->Data['date'] = $value['date'];
                    $Obj->Data['status'] = $value['status'];
                    $Obj->Data['on_date'] = date('Y-m-d');
                    $Obj->Insert();
                else:
                    $Obj = new query('staff_attendance');
                    $Obj->Data['staff_id'] = $value['staff_id'];
                    $Obj->Data['date'] = $value['date'];
                    $Obj->Data['status'] = $value['status'];
                    $Obj->Data['on_date'] = date('Y-m-d');
                    $Obj->Insert();
                endif;
            endforeach;
        endif;
        return true;
    }

    function GetStaffAttendanceSingleDay($staff_id, $date) {
        #firstly check attendance
        $Query = new query('staff_attendance');
        $Query->Where = "where staff_id='" . $staff_id . "' and date='" . $date . "'";
        $rec = $Query->DisplayOne();
        if (is_object($rec)):
            return $rec->status;
        else:
            return '';
        endif;
    }

    function GetStaffAttendace($staff_id, $from, $to) {
        $status = array();
        #Check the attendance on this date
        $this->Field = ('count(*) as count,status');
        $this->Where = "where staff_id='" . mysql_real_escape_string($staff_id) . "' 
                    AND DATE(date ) <= CAST('" . $to . "' AS DATE) 
                    AND DATE(date) >= CAST('" . $from . "' AS DATE) group by status";
        $this->DisplayAll();
        while ($object = $this->GetObjectFromRecord()):
            $status[$object->status] = $object->count;
        endwhile;
        return $status;
    }

}

class staffPayHeads extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('staff_pay_rule_heads');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'type', 'title', 'earning_deduction', 'is_active', 'add_date');
    }

    // Create New Pay Head
    function savePayHead($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_add'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    // List Pay Rules
    function listPayHeads() {
        return $this->ListOfAllRecords('object');
    }

    // Get Pay Head Detail By Id
    function getPayHead($id) {
        return $this->getObject($id);
    }

    // Get Pay Rules By Type
    function getPayRuleByType($type) {
        $this->Where = "WHERE type='$type'";
        return $this->ListOfAllRecords('object');
    }

    // Get Field Detail
    public static function getFieldDetail($id, $field) {
        $obj = new staffPayHeads;
        $obj->Field = "$field";
        $obj->Where = "where id='$id'";
        $data = $obj->DisplayOne();
        if (is_object($data)) {
            return $data->$field;
        }
    }

    function all_earnings() {
        $this->Where = "WHERE `type` = 'earning'";
        $data = $this->ListOfAllRecords();
        $e_list = array();
        foreach ($data as $earning) {
            $e_list[] = $earning['title'];
        }
        return $e_list;
    }

    function all_deductions() {
        $this->Where = "WHERE `type` = 'deduction'";
        $data = $this->ListOfAllRecords();
        $d_list = array();
        foreach ($data as $deduction) {
            $d_list[] = $deduction['earning_deduction'];
        }
        return $d_list;
    }

}

class bank extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('bank');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveBank($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function banks($id = 0) {
        if ($id > 0) {
            $this->Where = "WHERE `id` = '$id'";
            return $this->DisplayOne();
        } else {
            return $this->ListOfAllRecords();
        }
    }

    function delete_bank($id) {
        $this->Where = "WHERE `id` = '$id'";
        $this->Delete_where();
    }

}

class payment_method extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('payment_method');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function savePaymentMethod($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function staff_exist($staff_id) {
        $this->Where = "WHERE `staff_id` = '$staff_id'";
        return $this->DisplayOne();
    }

}
