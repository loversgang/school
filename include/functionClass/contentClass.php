<?php


class content extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order='asc', $orderby='position'){
        parent::__construct('content');
	$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id','school_id','name','call_id','description','urlname','meta_name','meta_keyword','meta_description','position');
    }

    /*
     * Create new content or update existing content
     */
    function saveData($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);

        if($this->Data['meta_name']==''){
            $this->Data['meta_name']=$this->Data['name'];
        }

        if($this->Data['meta_keyword']==''){
            $this->Data['meta_keyword']=$this->Data['name'];
        }

        if($this->Data['meta_description']==''){
            $this->Data['meta_description']=$this->Data['name'];
        }

        if($this->Data['urlname']==''){
            $this->Data['urlname']=$this->_sanitize($this->Data['urlname']);
        }

        if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              $max_id=$this->Data['id'];
        }
        else{
            $this->Insert();
            $max_id=$this->GetMaxId();
        }
		
        $quryObj=new content();
        $object=$quryObj->getPage($max_id);
        if($object){            
          $Query = new query('content');  
          $Query->Data['id']=$max_id;
          $Query->Data['urlname']=$Query->_sanitize($object->name.'-'.$max_id);          
          $Query->Update();
        }
        return $max_id;		
    }

    
    function saveNeccessaryPage($PAGES,$school_id){
        
    if(!empty($PAGES)): 
        foreach($PAGES AS $KEY=>$VALUE):
       
             $CheckObj=new query('content');  
             $CheckObj->Where="Where call_id='".$KEY."-".$school_id."' AND school_id='".$school_id."'";
             $page=$CheckObj->DisplayOne();
             if(is_object($page)):
             else:    
                $QueryNew = new query('content');  
                $QueryNew->Data['name']=$VALUE;
                $QueryNew->Data['meta_name']=$VALUE;
                $QueryNew->Data['call_id']=$KEY.'-'.$school_id;
                $QueryNew->Data['school_id']=$school_id;
                $QueryNew->Data['meta_keyword']=$VALUE;     
                $QueryNew->Data['meta_description']=$VALUE;        
                $QueryNew->Data['urlname']=$this->_sanitize($VALUE);

                $QueryNew->Insert();
                $max_id=$QueryNew->GetMaxId();

                $quryObj=new content();
                $object=$quryObj->getPage($max_id);
                if($object){            
                  $Query = new query('content');  
                  $Query->Data['id']=$max_id;
                  $Query->Data['urlname']=$Query->_sanitize($object->name.'-'.$max_id);          
                  $Query->Update();
                }
             endif;   
        endforeach;    
    endif;    	
    }    
    
    /*
     * Get Page by id
     */
    function getPage($id){
        return $this->_getObject('content', $id);
    }

    
    function getSchoolPages($school_id){
        
        $this->Where='Where school_id="'.mysql_real_escape_string($school_id).'"';
        return $this->DisplayAll();
       
    }
    
    function get_page_by_call_id($call_id){
        
        $this->Where='Where call_id="'.mysql_real_escape_string($call_id).'"';
        return $this->DisplayOne();
       
    }    
    
    
 /*check if page exists for user*/
    function getSchoolSinglePages($id,$school_id){        
            $this->Where="Where id='".mysql_real_escape_string($id)."' AND school_id='".$school_id."'";
            $page=$this->DisplayOne();
            if($this->GetNumRows()>0):
                return $page;/*page exists*/
            else:
                return false;/*page does not exits*/
            endif;
      
    }
    
    
    function setUserId($id){
        $this->user_id=mysql_real_escape_string($id);
    }  
    
    function deleteRecord($id){ 
        $this->id=$id;
            return $this->Delete();
    }
 

      
}
?>