<?php
/*
 * Category Module Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class category extends cwebc {
    
    function __construct() {
        parent::__construct('category');
        $this->requiredVars=array('id','name','meta_name','urlname', 'meta_keyword', 'meta_description', 'position',  'is_deleted', 'is_active');
     }
          
     /*Functions for front*/
      function getAllCategories(){
		
		$this->Where="where is_deleted='0' and is_active='1' order by name asc";    
		return $this->ListOfAllRecords('object');        
     }
     
     /*Get all  categories*/ 
     function getAllCategoriesName(){
            $this->Field="id,name";
            $this->Where="where is_deleted='0' and is_active='1' order by $this->orderby $this->order";    
            return $this->ListOfAllRecords('object');        
     } 
     
    function ListCategories(){
        $this->Where="where  is_deleted='0' order by $this->orderby $this->order";
        $this->DisplayAll();     
     }
    
     function get_category_name($id)
     {          
                $this->Where="where id='".mysql_real_escape_string($id)."'";  
                $cat= $this->DisplayOne();
                return $cat->name;
     }   
     
     function get_category_obj($id)
     {          
                $this->Where="where id='".mysql_real_escape_string($id)."'";  
                $cat= $this->DisplayOne();
                return $cat;
     }   
     
      /*used in both front and admin for showing categories*/
    function get_all_categories_name()
     {
               $all_cat=array();
                $this->Field="id,name";
		$this->Where="where is_deleted='0' AND is_active='1' order by $this->orderby $this->order";
                $all_catt=$this->ListOfAllRecords();
                 foreach($all_catt as $kkk=>$vvv):
                  $all_cat[$vvv['id']]=$vvv['name'];
                 endforeach;
                return $all_cat;

     }

       /*used in both front and admin for showing categories*/
    function get_all_categories_name_of_cat($cats_id)
     {
                $all_cat=array();
                $this->Field="id,name";
		$this->Where="where is_deleted='0' AND is_active='1' AND id IN($cats_id) order by $this->orderby $this->order";
                $all_catt=$this->ListOfAllRecords();
                if(count($all_catt)):
                     foreach($all_catt as $kkk=>$vvv):
                      $all_cat[$vvv['id']]=$vvv['name'];
                     endforeach;
                endif;
                return $all_cat;

     }

     
}
