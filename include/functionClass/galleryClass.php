<?php


class gallery extends cwebc{
    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order='asc', $orderby='position'){
        $this->InitilizeSQL();
        $this->orderby=$orderby;
        $this->order=$order;
        parent::__construct('gallery');
        //$this->TableName='content';
        $this->requiredVars=array('id','school_id', 'image', 'position','urlname', 'caption', 'is_deleted', 'is_active', 'link', 'date_added');
    }



    function saveImage($post){
        $this->Data=$this->_makeData($post, $this->requiredVars);

        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';

        $rand=rand(0, 99999999);
        $image_obj=new imageManipulation();
       
        if($image_obj->upload_photo('gallery', $_FILES['image'], $rand)):
                $this->Data['image']=$image_obj->makeFileName($_FILES['image']['name'], $rand);
        endif;

        if(isset($this->Data['id']) && $this->Data['id']!=''){

            if($this->Update())
              $max_id=$Data['id'];
        }
        else{
            $this->Data['date_added']=date('Y-m-d');
            $this->Insert();
            $max_id=$this->GetMaxId();
        }
         
        $quryObj=new gallery();
         $object=$quryObj->getImage($max_id);
        if($object){            
          $Query = new query('gallery');  
          $Query->Data['id']=$max_id;
          $Query->Data['urlname']=$Query->_sanitize($object->caption.'-'.$max_id);          
          $Query->Update();
        }
        return $max_id;	       
        
    }


    function getImage($id){

         return $this->_getObject('gallery', mysql_real_escape_string($id));
    }



    function listSchoolGalleryImages($school_id,$show_active=0, $result_type='object'){
        if($show_active)
          $this->Where="where school_id='".$school_id."' AND is_deleted='0' AND is_active='1' order by $this->orderby $this->order";
        else
          $this->Where="where school_id='".$school_id."' AND is_deleted='0' order by $this->orderby $this->order";
        if($result_type=='object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }
    
  function getGalleries($school_id,$p,$max_records)
    {    
            $this->Where="where school_id='".$school_id."' and is_active='1' and is_deleted='0' ORDER BY position";
            $this->AllowPaging=1;
            $this->PageNo=$p;
            $this->PageSize=$max_records; 
            $gallery_array=$this->ListOfAllRecords('object');
            $gallery['gallery']=$gallery_array;
            $gallery['TotalRecords']=$this->TotalRecords;
            $gallery['TotalPages']=$this->TotalPages;   
            return $gallery;          
    }    

    function deleteImage($id){
        $this->id=mysql_real_escape_string($id);
            return $this->Delete();
    }

      function deleteOnlyImage($id){
        $this->Data['image']='';
        $this->Data['id']=$id;
        
          return $this->Update();
    } 
    
     /*
     * Fetch all deleted events - which have "is_deleted" set to "1"
     */
    function getThrash(){
          $this->Where="where user_id='$this->user_id' AND is_deleted='1'";
          $this->DisplayAll();
    }
    
    
    /*
     * Get image by id
     */
    function getSingleImage($id){
        return $this->_getObject('gallery', $id);
    }
    
    /*change status of image*/
    function changeStatus($id){
        $image_obj=$this->getImageByID($id,0);
      
        if(is_object($image_obj)):
            $query_obj=new gallery();
            $query_obj->Data['is_active']=$image_obj->is_active==1?0:1;
            $query_obj->Data['id']=$id;
         
            $query_obj->Update();
        else:
            return false;
        endif;
        
        
    }
    
    
   
}
?>