<?php

class vehicle extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_vehicle');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'model', 'make', 'year', 'type', 'vehicle_number', 'color', 'vendor', 'permit', 'permit_expiry', 'possible_rounds', 'max_seating_capacity', 'purchase_date', 'seating_capacity', 'driver_id', 'driver_licence_expiry', 'driver_photo', 'conductor_id', 'conductor_photo', 'round_routes', 'on_date', 'update_date', 'is_deleted', 'is_active', 'insurance', 'insurance_expiry');
    }

#Create new vehicle list or update existing vehicle

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                $this->Data['update_date'] = date('Y-m-d');
            }
            return $this->Data['id'];
        } else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

#Get vehicle list by id

    function getRecord($id) {
        return $this->_getObject('school_vehicle', $id);
    }

#Get List of all vehicle list

    function listAll($school_id, $show_active = 0, $result_type = 'object') {
//$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active) {
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_deleted='0' and is_active='1'  ORDER BY id asc";
        } else {
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_deleted='0' ORDER BY id asc";
        }
        if ($result_type == 'object') {
            return $this->DisplayAll();
        } else {
            return $this->ListOfAllRecords('object');
        }
    }

    function getIstVehicle($school_id) {
        $this->Where = "where school_id='$school_id' limit 1";
        return $this->DisplayOne();
    }

    function listAllVehicles($school_id) {
        $this->Where = "where school_id='$school_id'";
        return $this->ListOfAllRecords('object');
    }

    function getPermitInsuranceExpiry($school_id) {
        $result = array('permit' => '', 'insurance' => '', 'total' => '0');
        $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_deleted='0' and is_active='1'  ORDER BY id asc";
        $record = $this->ListOfAllRecords('object');

        if (!empty($record)):
            foreach ($record as $key => $value):
                #check permit expiry
                if (strtotime($value->permit_expiry) < strtotime(date('Y-m-d'))):
                    $result['permit'][] = $value;
                    $result['total'] = $result['total'] + 1;
                endif;

                #check permit expiry
                if (strtotime($value->insurance_expiry) < strtotime(date('Y-m-d'))):
                    $result['insurance'][] = $value;
                    $result['total'] = $result['total'] + 1;
                endif;
            endforeach;
        endif;
        return $result;
    }

    function listAllReport($school_id, $show_active = 0, $v_report_type, $report_array) {
//$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active):
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_deleted='0' and is_active='1'  ORDER BY id asc";
        else:
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_deleted='0' ORDER BY id asc";
        endif;
        $record = $this->ListOfAllRecords('object');
        if (!empty($record)):
            if ($v_report_type == 'vehicle_info'):
                $filename = $v_report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "Statement of " . $report_array[$v_report_type] . "\n\n";
                echo 'Sr. No.' . "\t" . 'Model' . "\t" . 'Company' . "\t" . 'Vehicle Number' . "\t" . 'Color' . "\t" . 'Vehicle Type' . "\t" . 'Purchase Date' . "\n";
                $sr = '1';
                foreach ($record as $key => $value):
                    echo $sr . ".\t" . $value->model . "\t" . $value->make . "\t" . $value->vehicle_number . "\t" . $value->color . "\t" . $value->type . "\t" . $value->purchase_date . "\t\n";
                    $sr++;
                endforeach;
                exit;
            elseif ($v_report_type == 'vendor'):
                $filename = $v_report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "Statement of " . $report_array[$v_report_type] . "\n\n";
                echo 'Sr. No.' . "\t" . 'Model' . "\t" . 'Company' . "\t" . 'Vehicle Number' . "\t" . 'Vendor OR Owned' . "\n";
                $sr = '1';
                foreach ($record as $key => $value):
                    echo $sr . ".\t" . $value->model . "\t" . $value->make . "\t" . $value->vehicle_number . "\t";
                    if ($value->vendor == ""): echo "Owned";
                    else: echo $value->vendor;
                    endif;
                    echo "\t\n";
                    $sr++;
                endforeach;
                exit;
            elseif ($v_report_type == 'vendor_info'):
                $filename = $v_report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "Statement of " . $report_array[$v_report_type] . "\n\n";
                echo 'Sr. No.' . "\t" . 'Model' . "\t" . 'Company' . "\t" . 'Vehicle Number' . "\t" . 'Vendor Information' . "\n";
                $sr = '1';
                foreach ($record as $key => $value):
                    if ($value->vendor != 'Owned'):
                        echo $sr . ".\t" . $value->model . "\t" . $value->make . "\t" . $value->vehicle_number . "\t";
                        echo $value->vendor;
                        echo "\t\n";
                        $sr++;
                    endif;
                endforeach;
                exit;
            elseif ($v_report_type == 'vendor_reg'):
                $filename = $v_report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "Statement of " . $report_array[$v_report_type] . "\n\n";
                echo 'Sr. No.' . "\t" . 'Model' . "\t" . 'Company' . "\t" . 'Vehicle Number' . "\t" . 'Seating Capacity' . "\t" . 'Insurance' . "\n";
                $sr = '1';
                foreach ($record as $key => $value):
                    echo $sr . ".\t" . $value->model . "\t" . $value->make . "\t" . $value->vehicle_number . "\t" . $value->seating_capacity . "\t" . $value->insurance . "\t\n";
                    $sr++;
                endforeach;
                exit;
            endif;

        endif;
    }

#delete a vehicle list by id

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    function deleteImage($id, $type) {
        $this->Data['id'] = $id;
        $this->Data[$type] = '';
        return $this->Update();
    }

}

#Vehicle Student rel class

class vehicleStudent extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_vehicle_student');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'vehicle_id', 'student_id', 'route_id', 'stoppage_id', 'round', 'amount', 'session_id', 'section', 'on_date', 'update_date', 'from_date', 'to_date');
    }

    /*
     * Create new student vehicle rel list or update existing 
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                $this->Data['update_date'] = date('Y-m-d');
            }
            return $this->Data['id'];
        } else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

// List All Vehicle Students
    function ListAllVehicleStudents($vehicle_id) {
        $date = date('Y-m-d');
        $query = new query('student,school_vehicle_student,student_family_details');
        $query->Field = ('student.id, student.reg_id,student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,school_vehicle_student.section as section,school_vehicle_student.id as vehicle_student_id,school_vehicle_student.amount,school_vehicle_student.from_date,school_vehicle_student.to_date,school_vehicle_student.route_id,school_vehicle_student.stoppage_id,school_vehicle_student.round,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where school_vehicle_student.vehicle_id='" . mysql_real_escape_string($vehicle_id) . "' and school_vehicle_student.student_id=student.id and school_vehicle_student.to_date > '$date' and student_family_details.student_id=student.id ORDER BY student.id asc";
        //$query->print=1;
        return $query->ListOfAllRecords('object');
    }

    /*
     * Get student vehicle rel list by id
     */

    function getRecord($id) {
        return $this->_getObject('school_vehicle_student', $id);
    }

    /*
     * Get List of all student vehicle rel list in array
     */

    function listAll($show_active = 0, $result_type = 'object') {
//$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active)
            $this->Where = "where id!='0' ORDER BY id asc";
        else
            $this->Where = "where id!='0' ORDER BY id asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

#get vehicle student 

    function vehicleStudentIdArray($vehicle_id) {
        $records = array();
        $this->Where = "where vehicle_id='" . mysql_real_escape_string($vehicle_id) . "' order by id";
        $rec = $this->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = $value->student_id;
            endforeach;
        endif;
        return $records;
    }

# delete single student vehicle

    function deleteData($vehicle_id, $student_id) {
        $records = array();
        $this->Where = "where vehicle_id='" . mysql_real_escape_string($vehicle_id) . "' and student_id='" . mysql_real_escape_string($student_id) . "'";
        $this->Delete_where();
        return true;
    }

#check student vehicle

    function checkVehicleStudent($vehicle_id, $session_id, $section, $student_id) {
        $records = array();
        $this->Where = "where vehicle_id='" . mysql_real_escape_string($vehicle_id) . "' and session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student_id) . "'";
        return $this->DisplayOne();
    }

#check student vehicle

    function checkVehicleStudentFee($session_id, $section, $student_id) {
        $records = array();
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student_id) . "' and (amount!='' OR NOT NULL)";
        $rec = $this->DisplayOne();
        if (is_object($rec)):
            return $rec->amount;
        else:
            return '';
        endif;
    }

#check student checkVehicleStudentFeeInInterval

    function checkVehicleStudentFeeInInterval($session_id, $section, $student_id, $from, $to) {
        $from = date('Y-m-d');
        $records = array();
        /*
          $this->Where="where session_id='".mysql_real_escape_string($session_id)."' and section='".mysql_real_escape_string($section)."'
          AND ( CAST('".$from."' AS DATE) between DATE(from_date) AND DATE(to_date) OR CAST('".$to."' AS DATE) between DATE(from_date) AND DATE(to_date))
          and student_id='".mysql_real_escape_string($student_id)."' and (amount!='' OR NOT NULL)";
         */
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "'  
                    AND ( CAST('" . $from . "' AS DATE) between DATE(from_date) AND DATE(to_date) OR CAST('" . $to . "' AS DATE) between DATE(from_date) AND DATE(to_date)) 
                    and student_id='" . mysql_real_escape_string($student_id) . "' and (amount!='' OR NOT NULL) order by id desc";

        $rec = $this->DisplayOne();

        if (is_object($rec)):
            return $rec->amount;
        else:
            return '';
        endif;
    }

#check student checkVehicleStudentFeeInInterval

    function checkOtherVehicleAssign($session_id, $section, $student_id, $vehicle_id) {
        $records = array();
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' 
                    AND DATE(from_date )<= CAST('" . date('Y-m-d') . "' AS DATE) AND DATE(to_date)>= CAST('" . date('Y-m-d') . "' AS DATE )
                    AND student_id='" . mysql_real_escape_string($student_id) . "' and vehicle_id!='" . $vehicle_id . "'  and (amount!='' OR NOT NULL)";
        $rec = $this->DisplayOne();
        if (is_object($rec)):
            return $rec;
        else:
            return false;
        endif;
    }

#check student vehicle assign or not

    function checkVehicleStudentAssign($session_id, $section, $student_id) {
        $query = new query("school_vehicle_student,school_vehicle");
        $query->Field = ("school_vehicle.*,school_vehicle_student.id as assign_id,school_vehicle_student.from_date,school_vehicle_student.to_date,school_vehicle_student.vehicle_id,school_vehicle_student.student_id,school_vehicle_student.session_id,school_vehicle_student.section,school_vehicle_student.amount");
        $query->Where = "where school_vehicle_student.session_id='" . mysql_real_escape_string($session_id) . "' 
                                AND DATE(school_vehicle_student.from_date )<= CAST('" . date('Y-m-d') . "' AS DATE) AND DATE(school_vehicle_student.to_date)>= CAST('" . date('Y-m-d') . "' AS DATE )                    
                                AND school_vehicle_student.section='" . mysql_real_escape_string($section) . "' and school_vehicle_student.student_id='" . mysql_real_escape_string($student_id) . "' and school_vehicle_student.vehicle_id=school_vehicle.id";
        return $query->DisplayOne();
    }

#check student vehicle assign or not

    function checkVehicleStudentAssignCurrentVehicle($session_id, $section, $student_id, $vehicle_id) {
        $query = new query("school_vehicle_student,school_vehicle");
        $query->Field = ("school_vehicle.*,school_vehicle_student.id as assign_id,school_vehicle_student.from_date,school_vehicle_student.to_date,school_vehicle_student.vehicle_id,school_vehicle_student.student_id,school_vehicle_student.session_id,school_vehicle_student.section,school_vehicle_student.amount");
        $query->Where = "where school_vehicle_student.session_id='" . mysql_real_escape_string($session_id) . "' 
                                AND DATE(school_vehicle_student.from_date )<= CAST('" . date('Y-m-d') . "' AS DATE) AND DATE(school_vehicle_student.to_date)>= CAST('" . date('Y-m-d') . "' AS DATE )                    
                                AND school_vehicle_student.section='" . mysql_real_escape_string($section) . "' and school_vehicle_student.student_id='" . mysql_real_escape_string($student_id) . "' and school_vehicle_student.vehicle_id=school_vehicle.id";
        return $query->DisplayOne();
    }

    function getSessionList($id) {
        $query = new query("session,school_vehicle_student");
        $query->Field = ("session.id ,session.title,school_vehicle_student.vehicle_id");
        $query->Where = "where school_vehicle_student.vehicle_id='" . mysql_real_escape_string($id) . "' and session.is_active='1' and session.id=school_vehicle_student.session_id group by school_vehicle_student.session_id order by session.id";

        return $query->ListOfAllRecords('object');
    }

    function getSessionsection($id) {
        $query = new query("school_vehicle_student");
        $query->Field = ("school_vehicle_student.session_id,school_vehicle_student.section");
        $query->Where = "where school_vehicle_student.session_id='" . mysql_real_escape_string($id) . "' group by school_vehicle_student.section";

        return $query->ListOfAllRecords('object');
    }

# delete all students student 

    function DeleteStudentFromVehicle($vehicle_id) {
        $records = array();
        $this->Where = "where vehicle_id='" . mysql_real_escape_string($vehicle_id) . "'";
        $this->Delete_where();
        return true;
    }

    function GetStudentWithVehicleID($vehicle_id, $student_id) {
        $this->Where = "where vehicle_id='" . mysql_real_escape_string($vehicle_id) . "' and student_id='" . mysql_real_escape_string($student_id) . "'";
        return $this->DisplayOne();
    }

#stop_vehicle

    function stop_vehicle($id) {
        $current = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        $this->Data['to_date'] = $current;
        $this->Data['id'] = $id;
        $this->Update();
        return true;
    }

#stop_vehicle

    function start_vehicle($session_id, $section, $student_id, $vehicle_id, $route_id, $stoppage_id, $round, $amount) {
        $current = date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'));
        $session = get_object('session', $session_id);

        $query = new query('school_vehicle_student');
        $query->Data['from_date'] = date('Y-m-d');
        $query->Data['session_id'] = $session_id;
        $query->Data['section'] = $section;
        $query->Data['to_date'] = $session->end_date;
        $query->Data['student_id'] = $student_id;
        $query->Data['vehicle_id'] = $vehicle_id;
        $query->Data['route_id'] = $route_id;
        $query->Data['stoppage_id'] = $stoppage_id;
        $query->Data['round'] = $round;
        $query->Data['amount'] = $amount;
        $query->Data['on_date'] = date('Y-m-d');
        $query->Insert();
        return true;
    }

    function GetTotleStudentOnVehicleID($vehicle_id) {
        $this->Where = "where vehicle_id='" . mysql_real_escape_string($vehicle_id) . "'";
        $this->DisplayAll();
        return $this->GetNumRows();
    }

# add multiple students student 

    function AddMultipleStudentIntoVehicle($POST, $vehicle_id, $session_id, $section) {
        $session_info = get_object('session', $session_id);

        if (!empty($POST)):
            foreach ($POST as $key => $value):
                $QueryCheck = new query('school_vehicle_student');
                $QueryCheck->Where = "where vehicle_id='" . mysql_real_escape_string($vehicle_id) . "' and student_id='" . mysql_real_escape_string($key) . "'";
                $check = $QueryCheck->DisplayOne();
                $vehicle = get_object('school_vehicle', $vehicle_id);

                if (!$check):
                    $ObjQuery = new query('school_vehicle_student');
                    $ObjQuery->Data['vehicle_id'] = $vehicle_id;
                    $ObjQuery->Data['amount'] = $vehicle->default_student_fee;
                    $ObjQuery->Data['student_id'] = $key;
                    $ObjQuery->Data['session_id'] = $session_id;
                    $ObjQuery->Data['from_date'] = date('Y-m-d');
                    $ObjQuery->Data['to_date'] = $session_info->end_date;
                    $ObjQuery->Data['section'] = $section;
                    $ObjQuery->Data['on_date'] = date('Y-m-d');
                    $ObjQuery->Insert();
                endif;
            endforeach;
        endif;
        return true;
    }

# add multiple students student 

    function AddSingleStudentIntoVehicle($vehicle_id, $session_id, $section, $student_id, $amount) {

        $QueryCheck = new query('school_vehicle_student');
        $QueryCheck->Where = "where vehicle_id='" . mysql_real_escape_string($vehicle_id) . "' and session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and student_id='" . mysql_real_escape_string($student_id) . "'";
        $check = $QueryCheck->DisplayOne();
        $vehicle = get_object('school_vehicle', $vehicle_id);
        $session_info = get_object('session', $session_id);

        if (!$check):
            $ObjQuery = new query('school_vehicle_student');
            $ObjQuery->Data['vehicle_id'] = $vehicle_id;
            $ObjQuery->Data['amount'] = $amount;
            $ObjQuery->Data['from_date'] = $session_info->start_date;
            $ObjQuery->Data['to_date'] = $session_info->end_date;
            ;
            $ObjQuery->Data['student_id'] = $student_id;
            $ObjQuery->Data['session_id'] = $session_id;
            $ObjQuery->Data['section'] = $section;
            $ObjQuery->Data['on_date'] = date('Y-m-d');

            $ObjQuery->Insert();
        endif;

        return true;
    }

    public static function chkStudentAssigned($student_id) {
        $date = date('Y-m-d');
        $obj = new vehicleStudent;
        $obj->Where = "where student_id='$student_id' and to_date > '$date'";
        $data = $obj->DisplayOne();
        return is_object($data) ? $data : '';
    }

    public static function getStudentRoot($student_id) {
        $date = date('Y-m-d');
        $obj = new vehicleStudent;
        $obj->Where = "where student_id='$student_id' and to_date > '$date'";
        return $obj->DisplayOne();
    }

    public static function getStudentVehicleDetails($vehicle_student_id) {
        $obj = new vehicleStudent;
        $obj->Where = "where id='$vehicle_student_id'";
        return $obj->DisplayOne();
    }

    /*
     * delete a student Session rel list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /* Get List sessionStudents if session id */

    function vehicleStudentsWithVehicles($school_id, $id) {
        if ($id):
            $query = new query('student,school_vehicle_student,student_family_details');
            $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
            $query->Where = "where school_vehicle_student.vehicle_id='" . mysql_real_escape_string($id) . "' and school_vehicle_student.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student.id asc";
            return $query->ListOfAllRecords('object');
        else:
            $query = new query('student,student_family_details');
            $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
            $query->Where = "where student.school_id='" . mysql_real_escape_string($school_id) . "' and student.is_deleted='0' and student.is_active='1' and student_family_details.student_id=student.id GROUP BY student.id ORDER BY id asc";
            return $query->ListOfAllRecords('object');
        endif;
    }

    /* Get List vehicleStudents */

    function vehicleStudents($id) {
        $query = new query('student,school_vehicle_student,session,student_family_details');
        $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session.title as session_name,school_vehicle_student.section as section,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where school_vehicle_student.vehicle_id='" . mysql_real_escape_string($id) . "' and school_vehicle_student.session_id=session.id and school_vehicle_student.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student.id asc";

        return $query->ListOfAllRecords('object');
    }

    /* Get List vehicleStudents */

    function vehicleStudentsWithSession($id, $session_id, $section) {
        $query = new query('student,school_vehicle_student,session,student_family_details');
        $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session.title as session_name,school_vehicle_student.section as section,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where school_vehicle_student.vehicle_id='" . mysql_real_escape_string($id) . "' and school_vehicle_student.session_id='" . mysql_real_escape_string($session_id) . "' and school_vehicle_student.section='" . mysql_real_escape_string($section) . "' and school_vehicle_student.session_id=session.id and school_vehicle_student.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student.id asc";

        return $query->ListOfAllRecords('object');
    }

    /* Get List vehicleStudents */

    function vehicleStudentsWithDetail($id, $session_id, $section) {
        $date = date('Y-m-d');
        $query = new query('student,school_vehicle_student,student_family_details');
        $query->Field = ('student.id, student.reg_id,student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,school_vehicle_student.section as section,school_vehicle_student.id as vehicle_student_id,school_vehicle_student.amount,school_vehicle_student.from_date,school_vehicle_student.to_date,school_vehicle_student.route_id,school_vehicle_student.stoppage_id,school_vehicle_student.round,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        if ($section == '0') {
            $query->Where = "where school_vehicle_student.vehicle_id='" . mysql_real_escape_string($id) . "' and school_vehicle_student.session_id='" . mysql_real_escape_string($session_id) . "' and school_vehicle_student.student_id=student.id and school_vehicle_student.to_date > '$date' and student_family_details.student_id=student.id ORDER BY student.id asc";
        } else {
            $query->Where = "where school_vehicle_student.vehicle_id='" . mysql_real_escape_string($id) . "' and school_vehicle_student.session_id='" . mysql_real_escape_string($session_id) . "' and school_vehicle_student.section='" . mysql_real_escape_string($section) . "' and school_vehicle_student.to_date > '$date' and school_vehicle_student.student_id=student.id and student_family_details.student_id=student.id ORDER BY student.id asc";
        }
        return $query->ListOfAllRecords('object');
    }

    /* Get List vehicleStudents */

    function sessionSectionVehicles($session_id, $section) {
        $query = new query('school_vehicle,school_vehicle_student,staff');
        $query->Field = ('school_vehicle.id,school_vehicle.model,school_vehicle.make,school_vehicle.year,school_vehicle.type,school_vehicle.vehicle_number,school_vehicle.color,school_vehicle.vendor,school_vehicle.purchase_date,school_vehicle.seating_capacity,staff.title,staff.first_name,staff.last_name,school_vehicle_student.section');
        $query->Where = "where school_vehicle_student.session_id='" . mysql_real_escape_string($session_id) . "' and school_vehicle_student.section='" . mysql_real_escape_string($section) . "' and school_vehicle.id=school_vehicle_student.vehicle_id and school_vehicle.driver_id=staff.id GROUP BY school_vehicle.id";
        return $query->ListOfAllRecords('object');
    }

}

class vehicle_route extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_vehicle_route');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'route_from', 'route_to', 'is_active');
    }

// Save Routes
    function saveVehicleRoute($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

// Get Route Detail
    function getRecord($id) {
        return $this->_getObject('school_vehicle_route', $id);
    }

// List Routes
    function listVehicleRoute($school_id) {
        $this->Where = "where school_id='$school_id'";
        return $this->ListOfAllRecords('object');
    }

// Get Vehicle Routes
    public static function getVehicleRoutes($school_id, $routes) {
        if (!$routes) {
            $routes = '""';
        }
        $obj = new vehicle_route;
        $obj->Where = "where school_id='$school_id' and id IN($routes)";
        return $obj->ListOfAllRecords('object');
    }

}

class vehicle_stoppages extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_vehicle_stoppages');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'route_id', 'stoppage', 'pick_time', 'drop_time', 'amount', 'is_active');
    }

    function saveStoppages($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

// Get Stoppage Detail
    function getRecord($id) {
        return $this->_getObject('school_vehicle_stoppages', $id);
    }

    function listStoppages($route_id) {
        $this->Where = "where route_id='$route_id'";
        return $this->ListOfAllRecords('object');
    }

}

class vehicle_expenses extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_vehicle_expense');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'vehicle_id', 'name', 'date', 'amount', 'is_active');
    }

    function saveVehicleExpense($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

// Get Stoppage Detail
    function getRecord($id) {
        return $this->_getObject('school_vehicle_expense', $id);
    }

    function listVehicleExpenses($vehicle_id) {
        $this->Where = "where vehicle_id='$vehicle_id'";
        return $this->ListOfAllRecords('object');
    }

}

class vehicle_docs extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_vehicle_docs');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'vehicle_id', 'title', 'doc', 'is_active', 'date_add');
    }

    function saveVehicleDoc($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

// Get Stoppage Detail
    function getRecord($id) {
        return $this->_getObject('school_vehicle_docs', $id);
    }

    function listVehicleDocs($vehicle_id) {
        $this->Where = "where vehicle_id='$vehicle_id'";
        return $this->ListOfAllRecords('object');
    }

}

class vehicle_expense_docs extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_vehicle_expense_docs');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'vehicle_id', 'expense_id', 'title', 'doc', 'is_active', 'date_add');
    }

    function saveVehicleExpenseDoc($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

// Get Stoppage Detail
    function getRecord($id) {
        return $this->_getObject('school_vehicle_expense_docs', $id);
    }

    function listVehicleExpenseDocs($expense_id) {
        $this->Where = "where expense_id='$expense_id'";
        return $this->ListOfAllRecords('object');
    }

}
