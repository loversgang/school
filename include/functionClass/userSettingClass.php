<?php
/*
 * User Settings Class - 
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *   
 */

class user_setting extends cwebc {
    
    function __construct() {
        parent::__construct('user_setting');
        $this->requiredVars=array('id','key','value','name','title','type','user_id');
	
        }   
        
    
        
    function saveUserSettings($user_id){
      
        global $user_settings;
        
        if(is_array($user_settings)):
            
             foreach($user_settings as $k=>$setting):
            
                    $this->Data['`key`']=$setting['key'];
                    $this->Data['value']=$setting['value'];
                    $this->Data['name']=$setting['name'];
                    $this->Data['title']=$setting['title'];
                    $this->Data['type']=$setting['type'];
                    $this->Data['user_id']=$user_id;
                    $this->Insert();
            
            
             endforeach;
        
        else:
            return false;
        endif;
       
        
    }
    
    
    
    
    
  }
    