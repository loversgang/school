<?php

class stock extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('stock');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveStock($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function stocks() {
        return $this->ListOfAllRecords();
    }

}

?>