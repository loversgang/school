<?php

class complaints extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('complaints');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveComplaints($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function lists_complaints($id = 0) {
        if ($id > 0) {
            $this->Where = "WHERE `id` = '$id'";
            return $this->DisplayOne();
        } else {
            return $this->ListOfAllRecords();
        }
    }

    function lists_complaints_posted_student($login_student_id) {
        $this->Where = "WHERE `student_id` = '$login_student_id'";
        return $this->ListOfAllRecords();
    }

    function delete_complaint($id) {
        $this->id = $id;
        $this->Delete();
    }

    function update_feedback($feedback, $id) {
        $this->Data['feedback'] = $feedback;
        $this->Data['feedback_date'] = date('d M Y');
        $this->Data['id'] = $id;
        $this->Update();
    }

    function check_correct_data($complaint_id, $student_id) {
        $this->Where = "WHERE `id` = '$complaint_id' AND `student_id` = '$student_id'";
        return $this->DisplayOne();
    }

    function complaints_using_complaint_id($complaint_id) {
        $this->Where = "WHERE `id` = '$complaint_id'";
        return $this->ListOfAllRecords();
    }

    function complaint_solved($complaint_id, $action) {
        if ($action == 'resolve') {
            $this->Data['status'] = 1;
        } else {
            $this->Data['status'] = 0;
        }
        $this->Data['id'] = $complaint_id;
        $this->Update();
    }

}

class complaints_feedback extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('complaint_feedback');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function saveComplaints_feedback($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function all_complaints_feeback($id, $login_student_id) {
        $this->Where = "WHERE `complaint_id` = '$id' AND `student_id` = '$login_student_id' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

    function check_correct_data($complaint_id, $student_id) {
        $this->Where = "WHERE `complaint_id` = '$complaint_id' AND `student_id` = '$student_id'";
        return $this->DisplayOne();
    }

}

class referers extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('referers');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function save_referers($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function get_staff() {
        $this->TableName = "staff";
        $data = $this->ListOfAllRecords();
        foreach ($data as $key => $staff_info) {
            $staff[$staff_info['id']] = $staff_info['title'] . ' ' . $staff_info['first_name'] . ' ' . $staff_info['last_name'];
        }
        return $staff;
    }

    function referers($staff_id) {
        $this->Where = "WHERE `staff_id` = '$staff_id'";
        return $this->ListOfAllRecords();
    }

    function all_referers($complaint_id) {
        $this->Where = "WHERE `complaint_id` = '$complaint_id'";
        return $this->ListOfAllRecords();
    }

}

class referers_messages extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('referers_messages');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = $this->table_fields();
    }

    function save_referers_messages($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {

            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function all_referers_message($id, $staff_id) {
        $this->Where = "WHERE `complaint_id` = '$id' AND `staff_id` = '$staff_id' ORDER BY `id` DESC";
        return $this->ListOfAllRecords();
    }

}

?>