<?php
/*
 * FileHandling Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class file  {


            private $FileMimeTypes;
            private $FileImportMimeTypes;
            private $AudioMimeTypes;

            function __construct() {
                     global $conf_allowed_import_file_mime_type;
                     global $conf_allowed_file_mime_type;
                     global $conf_allowed_audio_mime_type;
                     $this->FileMimeTypes=$conf_allowed_file_mime_type;
                     $this->FileImportMimeTypes=$conf_allowed_import_file_mime_type;
                     $this->AudioMimeTypes=$conf_allowed_audio_mime_type;
                     $this->errorMsg= new errorManipulation();
                }


            function uploadDocument($to_upload_path, $file, $random='')
                    {

                            $upload_path=DIR_FS_SITE_UPLOAD.'file/';
                            $upload_path.=$to_upload_path.'/';

                           //echo $upload_path;exit;

                            $allowed_mime_types=$this->FileMimeTypes;
                          
                            if($file['error']==0):
                                    if(in_array($file['type'], $allowed_mime_types)):

                                            $filename=$this->makeFileName($file['name'], $random);

                                            if(move_uploaded_file($file['tmp_name'], $upload_path.$filename)):
                                                    return 1;
                                            else:
                                                    $this->errorMsg->errorAdd("unable to upload!!");
                                                    return false;
                                            endif;
                                    else:

                                            $this->errorMsg->errorAdd($file['name']." File Has Wrong file format.");

                                            return false;
                                    endif;
                            else:
                                    $error=$this->file_upload_error_codes($file['error']);
                                    //$this->errorMsg->errorAdd($error);
                                    return false;
                            endif;
                            return 1;
                    }

              function uploadImportDocument($to_upload_path, $file, $random='')
                    {

                            $upload_path=DIR_FS_SITE_UPLOAD.'file/';
                            $upload_path.=$to_upload_path.'/';

                           // echo $upload_path;exit;

                            $allowed_mime_types=$this->FileImportMimeTypes;
                            if($file['error']==0):
                                    if(in_array($file['type'], $allowed_mime_types)):

                                            $filename=$this->makeFileName($file['name'], $random);
                                            if(move_uploaded_file($file['tmp_name'], $upload_path.$filename)):
                                                    return 1;
                                            else:
                                                    $this->errorMsg->errorAdd("unable to upload!!");
                                                    return false;
                                            endif;
                                    else:

                                            $this->errorMsg->errorAdd("Wrong file format.");

                                            return false;
                                    endif;
                            else:
                                    $error=$this->file_upload_error_codes($file['error']);
                                    //$this->errorMsg->errorAdd($error);
                                    return false;
                            endif;
                            return 1;
                    }

              function uploadAudio($to_upload_path, $file, $random='')
                    {

                            $upload_path=DIR_FS_SITE_UPLOAD.'audio/';
                            $upload_path.=$to_upload_path.'/';

                            $allowed_mime_types=$this->AudioMimeTypes;

                            if($file['error']==0):
                                    if(in_array($file['type'], $allowed_mime_types)):
                                            $filename=$this->makeFileName($file['name'], $random);
                                            if(move_uploaded_file($file['tmp_name'], $upload_path.$filename)):
                                                    return 1;
                                            else:
                                                    $this->errorMsg->errorAdd("unable to upload!!");
                                                    return false;
                                            endif;
                                    else:
                                            $this->errorMsg->errorAdd("Wrong file format.");
                                            return false;
                                    endif;
                            else:
                                    $error=$this->file_upload_error_codes($file['error']);
                                    //$this->errorMsg->errorAdd($error);
                                    return false;
                            endif;
                            return 1;
                    }


            function file_upload_error_codes($code)
            {
                    switch ($code):
                            case '1':#UPLOAD_ERR_INI_SIZE
                                    return 'File size limit exceeds. Max file size: 2MB.';
                                    break;
                            case '2': #UPLOAD_ERR_FORM_SIZE
                                    return 'Max file size limit set in page has crossed.';
                                    break;
                            case '3': #UPLOAD_ERR_PARTIAL
                                    return 'File was only partially uploaded';
                                    break;
                            case '4': #UPLOAD_ERR_NO_FILE
                                     return 'No file was uploaded';
                                    break;
                            default: return 'No Message Found!';
                    endswitch;
            }

            function download_file($file)
                    {
                            if (!is_file($file))
                                                            {
                                                                     die("<b>404 File not found!</b>");
                                                            }


                                      //Gather relevent info about file
                                    $len = filesize($file);
                                    $filename = basename($file);
                                    $file_extension = strtolower(substr(strrchr($filename,"."),1));

                                    //This will set the Content-Type to the appropriate setting for the file
                                    switch( $file_extension )
                                             {
                                            case "pdf": $ctype="application/pdf"; break;
                                                    case "exe": $ctype="application/octet-stream"; break;
                                                    case "zip": $ctype="application/zip"; break;
                                                    case "doc": $ctype="application/msword"; break;
                                                    case "docx": $ctype="application/vnd.openxmlformats-officedocument.wordprocessingml.document";break;
                                                    case "xls": $ctype="application/vnd.ms-excel"; break;
                                                    case "xlsx": $ctype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; break;
                                                    case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
                                                    case "pptx": $ctype="application/vnd.openxmlformats-officedocument.presentationml.presentation"; break;
                                                    case "gif": $ctype="image/gif"; break;
                                                    case "png": $ctype="image/png"; break;
                                                    case "jpeg":
                                                    case "jpg": $ctype="image/jpg"; break;
                                                    case "mp3": $ctype="audio/mpeg"; break;
                                                    case "wav": $ctype="audio/x-wav"; break;
                                                    case "mpeg":
                                                    case "mpg":
                                                    case "mpe": $ctype="video/mpeg"; break;
                                                    case "mov": $ctype="video/quicktime"; break;
                                                    case "avi": $ctype="video/x-msvideo"; break;

                                                    //The following are for extensions that shouldn't be downloaded (sensitive stuff, like php files)
                                                    case "php":
                                                    case "htm":
                                                    case "html":
                                                    case "txt":die("<b>Cannot be used for ". $file_extension ." files!</b>"); break;

                                                    default: $ctype="application/force-download";
                                            }
                                            ob_clean();
                                            //Begin writing headers
                                            header("Pragma: public");
                                            header("Expires: 0");
                                            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                                            header("Cache-Control: public");
                                            header("Content-Description: File Transfer");

                                            //Use the switch-generated Content-Type
                                            header("Content-Type: $ctype");

                                            //Force the download
                                            $header="Content-Disposition: attachment; filename=".$filename.";";
                                            header($header );
                                            header("Content-Transfer-Encoding: binary");
                                            header("Content-Length: ".$len);
                                            @readfile($file);
                                            exit;
                            }

            function backupFile($backup_file,$MemberArray)
                    {

                            $fp = fopen($backup_file, "w");
                            foreach ($MemberArray as $Value)
                                 fputs($fp,$Value."\n");

                            fclose($fp);
                    }

//            function makeFileName($name, $id)
//            {
//                    $file_name_parts=explode('.', $name);
//                    $file_name_parts['0'].=$id;
//                    $file=$file_name_parts['0'].'.'.$file_name_parts['1'];
//                    return sanitize($file);
//            }
              function makeFileName($name, $id)
                {
                        $file_name_parts=pathinfo($name);
                        $file_name_parts['filename'].=$id;
                        $file=$file_name_parts['filename'].'.'.$file_name_parts['extension'];
                        return sanitize($file);
                }

            function human_filesize($bytes, $decimals = 2) {
              $sz = 'BKMGTP';
              $factor = floor((strlen($bytes) - 1) / 3);
              return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
            }


            function get_file_size($filename, $size_in = 'MB')
            {
                    $size_in_bytes = filesize($filename);

                    // Precision: decimals at the end for each type of size

                    if($size_in == 'B')
                    {
                    $size = $size_in_bytes;
                    $precision = 0;
                    }
                    elseif($size_in == 'KB')
                    {
                    $size = (($size_in_bytes / 1024));
                    $precision = 2;
                    }
                    elseif($size_in == 'MB')
                    {
                    $size = (($size_in_bytes / 1024) / 1024);
                    $precision = 2;
                    }
                    elseif($size_in == 'GB')
                    {
                    $size = (($size_in_bytes / 1024) / 1024) / 1024;
                    $precision = 2;
                    }

                    $size = round($size, $precision);

                    return $size.' '.$size_in;
            }

    /*function to delete a directory, if directory containing files*/
    function delete_directory($dirname) {
                 if (is_dir($dirname))
                   $dir_handle = opendir($dirname);
                 if (!(isset($dir_handle) && $dir_handle))
                      return false;
                 while($file = readdir($dir_handle)) {
                       if ($file != "." && $file != "..") {
                            if (!is_dir($dirname."/".$file))
                                 unlink($dirname."/".$file);
                            else
                                 $this->delete_directory($dirname.'/'.$file);
                       }
                 }
                 closedir($dir_handle);
                 rmdir($dirname);
                 return true;
    }

    
    
    /**
     * Copy a file, or recursively copy a folder and its contents
     * @param       string   $source    Source path
     * @param       string   $dest      Destination path
     * @param       string   $permissions New folder creation permissions
     * @return      bool     Returns true on success, false on failure
     */
    function folderCopy($source, $dest, $permissions = 0755)
    {
        // Check for symlinks
        if (is_link($source)) {
            return symlink(readlink($source), $dest);
        }

        // Simple copy for a file
        if (is_file($source)) {
            return copy($source, $dest);
        }

        // Make destination directory
        if (!is_dir($dest)) {
            mkdir($dest, $permissions);
            chmod($dest,$permissions);  // octal; correct value of mode
        }

        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            $this->folderCopy("$source/$entry", "$dest/$entry");
        }

        // Clean up
        $dir->close();
        return true;
    }

    
    
    
    
    

    function clear_cache_of_module($module_name){

        global $module_cache_folder;

        $module_cache_folders=array();
        if(isset($module_cache_folder[$module_name])):

            $module_cache_folders=$module_cache_folder[$module_name];

            foreach($module_cache_folders as $k=>$module):

                /*delete from cache folder*/
                $module_cache_path=DIR_FS_SITE_SMARTY_CACHE. CURRENT_THEME. '/'.$module;
                $this->delete_directory($module_cache_path);

                /*delete from compiled folder*/
                $module_compiled_path=DIR_FS_SITE_SMARTY_COMPILED. CURRENT_THEME. '/'.$module;
                $this->delete_directory($module_compiled_path);
            endforeach;

        endif;


    }

}





?>