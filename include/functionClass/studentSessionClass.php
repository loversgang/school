<?php

class studentSession extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('session_students_rel');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'session_id', 'roll_no', 'student_id', 'section', 'concession', 'concession_type', 'amount', 'start_concession', 'on_date', 'update_date');
    }

    /*
     * Create new student Session rel list or update existing theme
     */

    function saveData($POST) {

        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get student Session rel list by id
     */

    function getRecord($id) {
        return $this->_getObject('session_students_rel', $id);
    }

    function getConsessionAmount($session, $section, $student) {
        $this->Where = "where session_id='" . $session . "' and section='" . $section . "' and student_id='" . $student . "' ORDER BY id asc";
        $rec = $this->DisplayOne();
        if (is_object($rec)):
            return $rec->concession_type;
        else:
            return '0';
        endif;
    }

    // Get Concession
    function getConcession($session_id, $section, $student_id) {
        $this->Field = "concession as title,concession_type as type,CONCAT('-',amount) as amount";
        $this->Where = "where session_id='$session_id' and section='$section' and student_id='$student_id' and start_concession='1'";
        return $this->DisplayOne();
    }

    /*
     * Get List of all student Session rel list in array
     */

    function listAll($show_active = 0, $result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active) {
            $this->Where = "where id!='0' ORDER BY id asc";
        } else {
            $this->Where = "where id!='0' ORDER BY id asc";
        }
        if ($result_type == 'object') {
            return $this->DisplayAll();
        } else {
            return $this->ListOfAllRecords('object');
        }
    }

    #get session student 

    function sessionStudentIdArray($session_id) {

        $records = array();
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' order by id";
        $rec = $this->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = $value->student_id;
            endforeach;
        endif;

        return $records;
    }

    // Get Section Students
    function getSectionStudents($session_id, $section) {
        $this->Field = "student_id";
        $this->Where = "where session_id='$session_id' and section='$section' group by student_id order by id";
        $data = $this->ListOfAllRecords('object');
        $data_array = array();
        foreach ($data as $student_id) {
            $data_array[] = $student_id->student_id;
        }
        $sids = implode(',', $data_array);
        $obj = new query('student');
        $obj->Field = "first_name,last_name,sex,date_of_birth,photo";
        $obj->Where = "WHERE id IN ($sids)";
        return $obj->ListOfAllRecords('object');
    }

    // Get Section Students Birthday
    function getSectionStudentsBirthday($session_id, $section) {
        $current_month = date('m');
        $this->Field = "student_id";
        $this->Where = "where session_id='$session_id' and section='$section' group by student_id order by id";
        $data = $this->ListOfAllRecords('object');
        $data_array = array();
        foreach ($data as $student_id) {
            $data_array[] = $student_id->student_id;
        }
        $sids = implode(',', $data_array);
        $obj = new query('student');
        $obj->Field = "first_name,last_name,sex,date_of_birth,photo";
        $obj->Where = "WHERE id IN ($sids) and (MONTH(date_of_birth) BETWEEN $current_month AND $current_month) order by DAY(date_of_birth) asc";
        return $obj->ListOfAllRecords('object');
    }

    // Get Section Teachers Birthday
    function getSectionTeachersBirthday($session_id, $section) {
        $current_month = date('m');
        $obj = new query('school_time_table');
        $obj->Field = "staff_id";
        $obj->Where = "where session_id='$session_id' and section='$section' group by staff_id order by id";
        $data = $obj->ListOfAllRecords('object');
        $data_array = array();
        foreach ($data as $staff_id) {
            $data_array[] = $staff_id->staff_id;
        }
        $sids = implode(',', $data_array);
        $obj2 = new query('staff');
        $obj2->Field = "title,first_name,last_name,sex,date_of_birth,photo";
        $obj2->Where = "WHERE id IN ($sids) and (MONTH(date_of_birth) BETWEEN $current_month AND $current_month) order by DAY(date_of_birth) asc";
        return $obj2->ListOfAllRecords('object');
    }

    # delete single student 

    function deleteData($session_id, $student_id) {
        $records = array();
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and student_id='" . mysql_real_escape_string($student_id) . "'";
        $this->Delete_where();
        return true;
    }

    # check single student 

    function checkStudentData($session_id, $student_id) {
        $records = array();
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and student_id='" . mysql_real_escape_string($student_id) . "'";
        $this->DisplayOne();
    }

    # check single student 

    function getBalanceDetail($session_id, $student_id, $payment_date) {

        $query = new session_interval();
        $rec = $query->getIntervalByDate($session_id, $payment_date);
        if (is_object($rec)):
            $query = new studentFee();
            return $query->getBalanceAmount($session_id, $rec->id, $student_id);
        else:
            return '0';
        endif;
    }

    # delete all students student 

    function DeleteStudentFromSession($session_id) {
        $records = array();
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "'";
        $this->Delete_where();
        return true;
    }

    function GetStudentWithSessionID($session_id, $student_id) {

        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and student_id='" . mysql_real_escape_string($student_id) . "'";
        return $this->DisplayOne();
    }

    # add multiple students student 

    function AddMultipleStudentIntoSession($POST, $session_id, $id) {
        $Master_Section = array(
            "A" => "B",
            "B" => "C",
            "C" => "D",
            "D" => "E",
            "E" => "F",
            "F" => "G",
            "G" => "H"
        );
        if (!empty($POST)):
            foreach ($POST as $key => $value):

                $CheckSessStudent = new studentSession();
                $check = $CheckSessStudent->GetStudentWithSessionID($session_id, $key);

                if (!is_object($check)):
                    $POST['session_id'] = $session_id;
                    $POST['student_id'] = $key;

                    #count the last section student
                    $QueryCountSt = new studentSession();
                    $section_wise = $QueryCountSt->getSessionStudentBySection($session_id);

                    #session info
                    $session = get_object('session', $session_id);

                    # set the section and roll no
                    if (count($section_wise) >= 1):
                        $last_section = $section_wise[(count($section_wise) - 1)]['section'];
                        $last_section_students = $section_wise[(count($section_wise) - 1)]['count'];
                    elseif (count($section_wise) == '1'):
                        $last_section = $section_wise[(count($section_wise) - 1)]['section'];
                        $last_section_students = $section_wise[(count($section_wise) - 1)]['count'];
                    endif;

                    if (empty($last_section)) {
                        $last_section = 'A';
                    }
                    if (empty($last_section_students)) {
                        $last_section_students = '0';
                    }

                    #Get Max Roll No
                    $QuerylastRoll = new studentSession();
                    $last_section_roll_no = $QuerylastRoll->getSessionSectionMaxRollNo($POST['session_id'], $last_section);

                    if ($last_section_students >= $session->max_sections_students):
                        $POST['section'] = $Master_Section[$last_section];
                        $POST['roll_no'] = '1';
                    else:
                        $POST['section'] = $last_section;
                        $POST['roll_no'] = ($last_section_roll_no + 1);
                    endif;

                    $new_sect = get_charcter_value($POST['section']);

                    # if meximum limit is over
                    if ($new_sect > $session->total_sections_allowed):
                        $admin_user = new admin_session();
                        $admin_user->set_error();
                        $admin_user->set_pass_msg(MSG_SESSION_STUDENT_LIMIT);
                        Redirect(make_admin_url('session', 'previous', 'previous&ct_session=' . $session_id . '&id=' . $id));
                    endif;

                    #Get all session students in array
                    $SessFeeObj = new session_fee_type();
                    $SessFeeHeads = $SessFeeObj->sessionPayHeads($POST['session_id'], 'array');

                    #Add All Session Fee
                    $SessFeeAdd = new studentSessionInterval();
                    $SessFeeAdd->AddCompleteSessionFee($SessFeeHeads, $POST);

                    #Add student to session	
                    $QuerySess = new studentSession();
                    $QuerySess->saveData($POST);

                    #check Previous session of student
                    $PreSessStudent = new studentSession();
                    $PrevSession = $PreSessStudent->GetStudentWithSessionID($id, $key);

                    if (is_object($PrevSession)):
                        $QueryVeh = new vehicleStudent();
                        $vehicle = $QueryVeh->checkVehicleStudentAssign($PrevSession->session_id, $PrevSession->section, $POST['student_id']);
                        if (is_object($vehicle)):
                            #add vehicle for the new session 
                            $QueryVehAdd = new vehicleStudent();
                            $QueryVehAdd->AddSingleStudentIntoVehicle($vehicle->vehicle_id, $POST['session_id'], $POST['section'], $POST['student_id'], $vehicle->amount);
                        endif;
                    endif;
                endif;
            endforeach;
        endif;
        return true;
    }

    function GetCoundOFSessionStudent($session) {

        $query = new query('student,session_students_rel');
        $query->Field = ('count(session_students_rel.student_id) as count, session_students_rel.session_id');
        $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($session) . "' and session_students_rel.student_id=student.id and session_students_rel.student_id!='0' GROUP BY session_students_rel.session_id";
        $rec = $query->DisplayOne();
        if (is_object($rec)):
            return $rec->count;
        else:
            return '0';
        endif;
    }

    function getAllConcessionWiseAmount($session_id, $section) {
        $record = array();
        $Query = new query('session_students_rel');
        $Query->Field = ('concession,SUM(concession_type) as amount');
        $Query->Where = "where session_id='" . $session_id . "' and section='" . $section . "' and concession!='NULL' and concession!='None' GROUP BY concession";
        $rec = $Query->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $record[$value->concession] = $value->amount;
            endforeach;
        endif;

        return $record;
    }

    # add multiple students student 

    function AddMultipleStudentFromList($POST, $session_id) {
        $Master_Section = array(
            "A" => "B",
            "B" => "C",
            "C" => "D",
            "D" => "E",
            "E" => "F",
            "F" => "G",
            "G" => "H"
        );
        if (!empty($POST)):
            foreach ($POST as $key => $value):

                $CheckSessStudent = new studentSession();
                $check = $CheckSessStudent->GetStudentWithSessionID($session_id, $key);

                if (!is_object($check)):
                    $POST['session_id'] = $session_id;
                    $POST['student_id'] = $key;

                    #count the last section student
                    $QueryCountSt = new studentSession();
                    $section_wise = $QueryCountSt->getSessionStudentBySection($session_id);

                    #session info
                    $session = get_object('session', $session_id);

                    # set the section and roll no
                    if (count($section_wise) >= 1):
                        $last_section = $section_wise[(count($section_wise) - 1)]['section'];
                        $last_section_students = $section_wise[(count($section_wise) - 1)]['count'];
                    elseif (count($section_wise) == '1'):
                        $last_section = $section_wise[(count($section_wise) - 1)]['section'];
                        $last_section_students = $section_wise[(count($section_wise) - 1)]['count'];
                    endif;

                    if (empty($last_section)) {
                        $last_section = 'A';
                    }
                    if (empty($last_section_students)) {
                        $last_section_students = '0';
                    }

                    #Get Max Roll No
                    $QuerylastRoll = new studentSession();
                    $last_section_roll_no = $QuerylastRoll->getSessionSectionMaxRollNo($POST['session_id'], $last_section);

                    if ($last_section_students >= $session->max_sections_students):
                        $POST['section'] = $Master_Section[$last_section];
                        $POST['roll_no'] = '1';
                    else:
                        $POST['section'] = $last_section;
                        $POST['roll_no'] = ($last_section_roll_no + 1);
                    endif;

                    $new_sect = get_charcter_value($POST['section']);

                    # if meximum limit is over
                    if ($new_sect > $session->total_sections_allowed):
                        $admin_user = new admin_session();
                        $admin_user->set_error();
                        $admin_user->set_pass_msg(MSG_SESSION_STUDENT_LIMIT);
                        Redirect(make_admin_url('session', 'other', 'other&ct_session=' . $session_id));
                    endif;
                    #Get all session students in array
                    $SessFeeObj = new session_fee_type();
                    $SessFeeHeads = $SessFeeObj->sessionPayHeads($POST['session_id'], 'array');

                    #Add All Session Fee
                    $SessFeeAdd = new studentSessionInterval();
                    $SessFeeAdd->AddCompleteSessionFee($SessFeeHeads, $POST);


                    #Add student to session	
                    $QuerySess = new studentSession();
                    $QuerySess->saveData($POST);
                endif;
            endforeach;
        endif;
        return true;
    }

    /*
     * delete a student Session rel list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    /* Get List session of student */

    function getStudentSessionList($student_id) {
        $query = new query('session_students_rel,session');
        $query->Field = ('session_students_rel.student_id as student_id,session_students_rel.section as section,session.id as session_id,session.title as session_name');
        $query->Where = "where session_students_rel.student_id='" . mysql_real_escape_string($student_id) . "' and session_students_rel.session_id=session.id ORDER BY session.year desc";
        return $query->ListOfAllRecords('object');
    }

    /* Get List session of student */

    function getStudentPreviousSessionFeePending($student_id, $current_session) {

        $sessionDet = get_object('session', $current_session);
        $res = array();
        $query = new query('session_students_rel,session');
        $query->Field = ('session_students_rel.student_id as student_id,session_students_rel.section as section,session.id as session_id,session.title as session_name,session.start_date,session.end_date');
        $query->Where = "where session_students_rel.student_id='" . mysql_real_escape_string($student_id) . "' AND DATE(session.end_date)<= CAST('" . $sessionDet->end_date . "' AS DATE) AND session.id!='" . $current_session . "' and session_students_rel.session_id=session.id ORDER BY session.year desc";
//$query->print=1;
        $rec = $query->ListOfAllRecords('object');

        if (!empty($rec)):
            foreach ($rec as $key => $val):
                $QueryObj = new studentSessionInterval();
                $bal = $QueryObj->getOnlyPreviousSessionBalance($val->session_id, $student_id);

                if (!empty($bal)):
                    $bal['section'] = $val->section;
                    $res[date('Y', strtotime($val->start_date)) . '-' . date('y', strtotime($val->end_date))] = $bal;
                endif;
            endforeach;
        endif;
        return $res;
    }

    /* Get Class Name */

    function getClassNameBySession($session_id) {
        $query = new query('school_course,session');
        $query->Field = ('school_course.course_name');
        $query->Where = "where session.id='" . mysql_real_escape_string($session_id) . "' and school_course.id=session.course_id ";
        $rec = $query->DisplayOne();
        if ($rec):
            return $rec->course_name;
        else:
            return '';
        endif;
    }

    /* Get List session of student */

    function getAllSessionStudent($session_id) {
        $query = new query('student,session_students_rel,session,student_family_details');
        $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session.title as session_name,session_students_rel.session_id as session_id,session_students_rel.roll_no as roll_no, session_students_rel.section as section,student_family_details.father_name,student_family_details.mother_name,student_family_details.email as parents_email,student_family_details.mobile as parents_phone');
        $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($session_id) . "' and (session_students_rel.student_id!='0' || session_students_rel.student_id!='') and session_students_rel.student_id=student.id and session_students_rel.session_id=session.id and student_family_details.student_id=student.id  GROUP BY student.id ORDER BY session_students_rel.roll_no asc";
        return $query->ListOfAllRecords('object');
    }

    /* Get List sessionStudents if session id */

    function sessionStudentsWithSessionOrSimple($school_id, $id, $section = '0') {
        if ($id):

            $query = new query('student,session_students_rel,session,student_family_details');
            $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session.title as session_name,session_students_rel.session_id as session_id,session_students_rel.roll_no as roll_no, session_students_rel.section as section,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
            if ($section):
                $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and session_students_rel.student_id=student.id and session_students_rel.session_id=session.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY session_students_rel.roll_no asc";
            else:
                $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.student_id=student.id and session_students_rel.session_id=session.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY session_students_rel.roll_no asc";
            endif;
            //$query->print=1;
            return $query->ListOfAllRecords('object');

        endif;
    }

    /* Get List sessionStudents if session id */

    function AllSchoolStudent($school_id) {

        $query = new query('student,student_family_details');
        $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where student.school_id='" . mysql_real_escape_string($school_id) . "' and student.is_active=1 and student.is_deleted=0 and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student.id asc";
        return $query->ListOfAllRecords('object');
    }

    function SingleStudentsWithSession($student_id, $session_id, $section = 'A') {
        if ($student_id):
            $query = new query('student,session_students_rel,session,student_family_details');
            $query->Field = ('student.id, student.reg_id, student.date_of_birth,student.date_of_admission,student.category,student.cast,student.religion, student.course_of_admission,student.photo, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session.year as session,session.title as session_name,session.frequency,session_students_rel.session_id as session_id,session_students_rel.id as rel_id,session_students_rel.roll_no as roll_no, session_students_rel.section as section,session_students_rel.concession,session_students_rel.concession_type,session_students_rel.amount,session_students_rel.start_concession,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
            $query->Where = "where student.id='" . mysql_real_escape_string($student_id) . "' and session_students_rel.session_id='" . mysql_real_escape_string($session_id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and session_students_rel.student_id=student.id and session_students_rel.session_id=session.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY session_students_rel.roll_no asc";
            // $query->print=1;
            return $query->DisplayOne();

        endif;
    }

    /* Get List sessionStudents */

    function sessionStudents($id) {
        $query = new query('student,session_students_rel,student_family_details,student_address');
        $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.photo, student.date_of_birth, student.email, student.phone,session_students_rel.session_id as session_id,session_students_rel.roll_no as roll_no,session_students_rel.section as section,session_students_rel.id as rel_id,session_students_rel.concession,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income,student_address.address1,student_address.address2,student_address.city,student_address.state');
        $query->Where = "where student.is_active='1' and session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.student_id=student.id and student_address.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY session_students_rel.roll_no asc";
        return $query->ListOfAllRecords('object');
    }

    /* Get Current Year Students In Array for autocomplete */

    function findCurrentYearStudentsByRegId($school_id, $reg_id) {
        $query = new query('student,session_students_rel,session,session_interval,student_family_details');

        $query->Field = ('student.id,student.id as student_id,student.reg_id as reg_id,session_interval.id as interval_id, student.first_name,student.last_name,student.sex,student.phone,session.id as session_id,session.title as session_name,session_students_rel.section as section, session_students_rel.roll_no as roll_no,session.title as session_name,student_family_details.father_name');

        $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' 
                        AND session.id=session_students_rel.session_id
						AND DATE( session.start_date ) <= CAST('" . date('Y-m-d') . "' AS DATE ) 
                        AND DATE( session.end_date ) >= CAST('" . date('Y-m-d') . "' AS DATE ) 
                        AND CAST('" . date('Y-m-d') . "' AS DATE) >= DATE(session_interval.from_date) 
                        AND CAST('" . date('Y-m-d') . "' AS DATE) <= DATE(session_interval.to_date )						
						AND student.reg_id='$reg_id' 
						AND student_family_details.student_id=student.id
						AND session_interval.session_id=session.id
						AND session_students_rel.student_id=student.id AND session_students_rel.session_id=session.id 
						GROUP BY session.id ORDER BY student.id asc";
        $data = $query->DisplayOne();
        return is_object($data) ? $data : '';
    }

    /* Get Current Year Students In Array for autocomplete */

    function findActiveSessionStudents($school_id) {
        $result = '';
        $query = new query('student,session_students_rel,session,student_family_details');

        $query->Field = ('student.id,student.id as student_id,student.reg_id as reg_id, student.first_name,student.last_name,student.sex,student.phone,session.id as session_id,session.title as session_name,student_family_details.mobile');

        $query->Where = "where session.school_id='" . mysql_real_escape_string($school_id) . "' 
                        AND session.id=session_students_rel.session_id
						AND DATE( session.start_date ) <= CAST('" . date('Y-m-d') . "' AS DATE ) 
                        AND DATE( session.end_date ) >= CAST('" . date('Y-m-d') . "' AS DATE ) 
						AND student_family_details.student_id=student.id
						AND session_students_rel.student_id=student.id AND session_students_rel.session_id=session.id 
						GROUP BY student.id ORDER BY student.id asc";
        return $query->ListOfAllRecords('object');
    }

    /* Get List section Students */

    function sectionSessionStudents($id, $section) {

        if ($section) {
            $query = new query('student,session_students_rel,student_family_details');
            $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,student.student_login_id,student.password,session_students_rel.roll_no as roll_no,session_students_rel.section,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
            $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id and student.is_active='1' GROUP BY student.id ORDER BY student.id asc";
            // $query->print=1;
            return $query->ListOfAllRecords('object');
        } else {
            $query = new query('student,session_students_rel,student_family_details');
            $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,student.student_login_id,student.password,session_students_rel.roll_no as roll_no,session_students_rel.section,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
            $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id and student.is_active='1' GROUP BY student.id ORDER BY student.id asc";
            // $query->print=1;
            return $query->ListOfAllRecords('object');
        }
    }

    /* Get List section Students */

    function SelectedSessionStudents($sess_int) {
        $val = explode(',', $sess_int);
        $start = $val[0];
        $end = $val[1];
        $obj = new query('session');
        $obj->Field = ('id');
        $obj->Where = "where start_date='$start' and end_date='$end'";
        $data = $obj->ListOfAllRecords();
        $session_ids_array = array();
        foreach ($data as $sess_data) {
            $session_ids_array[] = $sess_data['id'];
        }
        $session_ids = implode(',', $session_ids_array);
        $query = new query('student,session_students_rel,student_family_details');
        $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session_students_rel.roll_no as roll_no,session_students_rel.section,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where session_students_rel.session_id IN($session_ids) and session_students_rel.student_id=student.id and student_family_details.student_id=student.id and student.is_active='1' GROUP BY student.id ORDER BY student.id asc";
        return $query->ListOfAllRecords('object');
    }

    function getIntervalDuePendingFee($id, $section) {
        $result = array();
        $query = new query('student,session_students_rel,student_family_details');
        $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session_students_rel.roll_no as roll_no,session_students_rel.section,student_family_details.father_name,student_family_details.mother_name,student_family_details.mobile as family_phone,student_family_details.household_annual_income');
        if ($section) {
            $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student.id asc";
        } else {
            $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student.id asc";
        }
        $reocrd = $query->ListOfAllRecords('object');

        $query12 = new query('session_interval');
        $query12->Where = "where session_id='" . $id . "' ORDER BY from_date asc";
        $intervals = $query12->ListOfAllRecords('object');

        $current_date = date('Y-m-d');
        $current = '0';
        $int_id = array();

        if (!empty($intervals)):
            foreach ($intervals as $ik => $iv):
                if ((strtotime($current_date) >= strtotime($iv->from_date)) && (strtotime($current_date) <= strtotime($iv->to_date))):
                    $current = '1';
                endif;
                $int_id[] = $iv->id;
                if ($current == '1'):
                    break;
                endif;
            endforeach;
        endif;
        $all_int = implode(',', $int_id);

        if (empty($all_int)):
            $all_int = '0';
        endif;


        if (!empty($reocrd)):
            foreach ($reocrd as $key => $value):
                $queryNew = new query('student_session_interval');
                $queryNew->Field = ('id,SUM(total) as total,SUM(paid) as paid');
                $queryNew->Where = "where session_id='" . mysql_real_escape_string($id) . "' and student_id='" . $value->id . "' and section='" . mysql_real_escape_string($section) . "' AND interval_id IN(" . $all_int . ")";

                $rec = $queryNew->DisplayOne();
                $value->total = $rec->total;
                $value->paid = $rec->paid;
                if (!empty($value->total)):
                    if ($value->paid < $value->total):
                        $result[] = $value;
                    endif;
                else:
                    $result[] = $value;
                endif;
            endforeach;
        endif;
        return $result;
    }

    /* Get List section Students */

    function sectionSessionStudentsDownload($school, $id, $section, $monthYear) {
        $query = new query('student,session_students_rel,student_family_details');
        $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session_students_rel.roll_no as roll_no,session_students_rel.section,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student.id asc";

        $record = $query->ListOfAllRecords('object');
        if (!empty($record)):
            #count working days
            $working_days = '';
            if (!empty($monthYear)):
                $first_date_of_month = date('Y-m-d', strtotime("01-" . $monthYear));
                $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date("01-" . $monthYear))));

                $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
                $total_month_days = (date('d', strtotime($last_date_of_month)));
                $working_days = $total_month_days - $total_sunday;
            endif;

            if (!empty($monthYear)):
                $pos = strrpos($monthYear, "-");
                if ($pos == true):
                    $m_y = explode('-', $monthYear);
                    $date = $m_y['0'];
                    $year = $m_y['1'];
                endif;
            endif;
            $session = get_object('session', $id);
            $filename = "attendance.csv";

            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;filename=$filename");

            echo "STATEMENT OF MONTHLY ATTENDANCE REPORT" . "\n\n";
            echo "Session Name" . "\t" . $session->title . "\n";
            if (!empty($section)):
                echo "Section" . "\t" . $section . "\n";
            endif;
            if (!empty($monthYear)):
                echo "Month And Year" . "\t" . date('M, Y', strtotime("01-" . $monthYear)) . "\n";
            endif;
            echo "\n";
            echo 'SR. NO.' . "\t" . 'ROLL NO.' . "\t" . 'SECTION' . "\t" . 'STUDENT NAME' . "\t" . 'FATHER NAME' . "\t" . 'PRESENT' . "\t" . 'ABSENT' . "\t" . 'LEAVE' . "\t" . 'WORKING DAYS' . "\t" . '%AGE' . "\n";
            $SR = '1';
            foreach ($record as $key => $value):
                $Q_obj = new attendance();
                $r_atten = $Q_obj->checkSessionSectionMonthYearAttendace($id, $section, $value->id, $date, $year);
                $t_attend = array_sum($r_atten);

                $M_obj = new schoolHolidays();
                $M_holiday = $M_obj->checkMonthlyHoliday($school, $date, $year);

                echo $SR . "\t" . $value->roll_no . "\t" . $value->section . "\t" . ucfirst($value->first_name) . " " . $value->last_name . "\t" . $value->father_name . "\t";
                if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $present = $r_atten['P'];
                else: echo $present = '0';
                endif;
                echo "\t";
                if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
                else: echo '0';
                endif;
                echo "\t";
                if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
                else: echo '0';
                endif;
                echo "\t" . ($working_days - $M_holiday);
                echo "\t";
                echo number_format(($present / ($working_days - $M_holiday)) * 100, 2) . "%";
                echo "\t\n";
                $SR++;
            endforeach;
            exit;
        endif;
    }

    /* Get List section Students with fee info */

    function sectionSessionStudentsWithFee($id, $section, $month, $year) {
        $record = array();
        $query1 = "SELECT student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session_students_rel.roll_no as roll_no,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income FROM student,session_students_rel,student_family_details 
                            where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id";

        $query2 = "SELECT student_fee.id as student_fee_id,student_fee.student_id as student_id,student_fee.payment_date,student_fee.absent_fine,student_fee.late_fee,student_fee.month as month,student_fee.total_amount FROM
                              student_fee WHERE student_fee.session_id='" . mysql_real_escape_string($id) . "' and student_fee.section='" . mysql_real_escape_string($section) . "' and student_fee.month='" . mysql_real_escape_string($month) . "' and student_fee.current_year='" . mysql_real_escape_string($year) . "'";

        $new_query = "SELECT * FROM (" . $query1 . ") as t1 left join (" . $query2 . ") as t2 ON t1.id = t2.student_id ";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($new_query);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):
                $record[] = $object;
            endwhile;

        endif;
        return $record;
    }

    /* Get List section Students fee dawnload */

    function sectionSessionStudentsFeeDownload($school, $session_id, $ct_sec, $interval_id) {

        #Get interval detail
        $interval_detail = get_object('session_interval', $interval_id);

        #Get Section Students
        $QueryOb = new studentSession();
        $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);

        if (!empty($record)):
            $session = get_object('session', $session_id);

            #Get Session Fee Heads
            $QuerySess = new session_fee_type();
            $Sesspay_heads = $QuerySess->sessionPayHeadsWithKey($session_id);



            $filename = "FeeReport.csv";

            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;filename=$filename");

            echo "CHECK LIST DETAIL OF STUDENTS FEE REPORTS" . "\n\n";
            echo "Session Name" . "\t" . $session->title . "\n";
            if (!empty($ct_sec)):
                echo "Section" . "\t" . $ct_sec . "\n";
            endif;

            echo "Month And Year" . "\t";
            echo date('M,Y', strtotime($interval_detail->from_date)) . " - " . date('M,Y', strtotime($interval_detail->to_date));
            echo "\n";
            echo "\n";

            echo 'REG. NO.' . "\t" . 'STUDENT NAME' . "\t";
            if (!empty($Sesspay_heads)):
                foreach ($Sesspay_heads as $kk => $vv):
                    echo ucwords($vv['title']) . "(" . $vv['type'] . ")" . "\t";
                endforeach;
            endif;
            echo 'Late Fee Fine' . "\t" . 'Attendance Fine' . "\t" . 'Vehicle Fee' . "\t" . 'Concession' . "\t" . 'PREVIOUS AMOUNT' . "\t" . 'RECEIVABLES AMOUNT' . "\t" . 'RECEIVING AMOUNT' . "\t" . 'BALANCE AMOUNT' . "\t\n";
            $sr = '1';
            $sum = '';
            $pre_sum = '';
            $rec_sum = '';
            $paid_sum = '';
            $total_sum = '';
            $balance = '';
            foreach ($record as $key => $stu):
                #Get interval detail
                $SessIntOb = new studentSessionInterval();
                $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sec, $stu->id, $interval_id);

                #get Pending Fee for previous month
                $SessObPre = new studentSessionInterval();
                $pending_fee = $SessObPre->getPreviousBalanceFee($session_id, $ct_sec, $stu->id, $interval_id);

                echo $stu->reg_id . "\t" . ucfirst($stu->first_name) . " " . $stu->last_name . "\t";

                if (!empty($Sesspay_heads)): $srr = '2';
                    foreach ($Sesspay_heads as $kk => $vv):
                        #Get student payheads students
                        $SessStuFee = new session_student_fee();
                        $head = $SessStuFee->GetStudentPayHeadsAmount($session_id, $stu->id, $vv['fee_id']);

                        if (is_object($head)):
                            if ($head->type == 'Regular'):
                                echo number_format($head->amount, 2);
                                echo "\t";
                            elseif ($interval_detail->interval_position == '1'):
                                echo number_format($head->amount, 2);
                                echo "\t";
                            else:
                                echo "0";
                                echo "\t";
                            endif;
                        else:
                            echo "0";
                            echo "\t";
                        endif;
                    endforeach;
                endif;

                if (is_object($student_interval_detail)): echo $student_interval_detail->late_fee;
                    echo "\t";
                else: echo '0';
                    echo "\t";
                endif;
                if (is_object($student_interval_detail)): echo $student_interval_detail->absent_fine;
                    echo "\t";
                else: echo '0';
                    echo "\t";
                endif;
                if (is_object($student_interval_detail)): echo $student_interval_detail->vehicle_fee;
                    echo "\t";
                else: echo '0';
                    echo "\t";
                endif;
                if (is_object($student_interval_detail)): echo $student_interval_detail->concession;
                    echo "\t";
                else: echo '0';
                    echo "\t";
                endif;

                if (isset($pending_fee)): echo $pending = $pending_fee;
                    echo "\t";
                else: echo $pending = '0';
                    echo "\t";
                endif;
                if (is_object($student_interval_detail)): echo $rc = $student_interval_detail->total;
                    echo "\t";
                else: echo $rc = '0';
                    echo "\t";
                endif;
                if (is_object($student_interval_detail)): echo $pd = $student_interval_detail->paid;
                    echo "\t";
                else: echo $pd = '0';
                    echo "\t";
                endif;
                if (is_object($student_interval_detail)): echo $bl = ($pending_fee + ($student_interval_detail->total - $student_interval_detail->paid));
                    echo "\t";
                else: echo $bl = '0';
                    echo "\t";
                endif;

                $pre_sum = $pre_sum + $pending;
                $rec_sum = $rec_sum + $rc;
                $paid_sum = $paid_sum + $pd;
                $balance = $balance + $bl;
                echo "\t\n";
            endforeach;

            echo "\t\t\t";
            if (!empty($Sesspay_heads)):
                foreach ($Sesspay_heads as $kk => $vv):
                    echo "\t";
                endforeach;
            endif;
            echo "\t\t\ TOTAL \t";
            echo $pre_sum;
            echo "\t";
            echo $rec_sum;
            echo "\t";
            echo $paid_sum;
            echo "\t";
            echo $balance . "\t\n";

            exit;
        endif;
    }

    /* Get List section Students with fee info */

    function SessionStudentsWithFee($id, $section) {
        $record = array();
        $query1 = "SELECT student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session_students_rel.roll_no as roll_no,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income FROM student,session_students_rel,student_family_details 
                            where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id";

        $query2 = "SELECT session_student_fee.student_id,SUM(session_student_fee.amount) as total_fee FROM session_student_fee
                            where session_student_fee.session_id='" . mysql_real_escape_string($id) . "' GROUP BY session_student_fee.student_id";

        $query3 = "SELECT student_fee.student_id as s_id, SUM(student_fee.total_amount) as paid
                            FROM student_fee WHERE student_fee.session_id='" . mysql_real_escape_string($id) . "' and student_fee.section='" . mysql_real_escape_string($section) . "'  GROUP BY student_fee.student_id";

        $new_query = "SELECT * FROM (" . $query1 . ") as t1 left join (" . $query2 . ") as t2 ON t1.id = t2.student_id ";

        $final_query = "SELECT * FROM (" . $new_query . ") as t3 left join (" . $query3 . ") as t4 ON t3.id = t4.s_id";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($final_query);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):
                $record[] = $object;
            endwhile;

        endif;
        return $record;
    }

    /* Get List section Students */

    function sectionSessionStudentsWithAttendance($id, $section, $date) {

        $record = array();
        $query1 = "SELECT student.*,session_students_rel.roll_no as roll_no,session_students_rel.section,student_family_details.father_name,student_family_details.mother_name,
                        student_family_details.household_annual_income FROM student,session_students_rel,student_family_details 
                        where session_students_rel.session_id='" . mysql_real_escape_string($id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY student.id asc";


        $query2 = "SELECT student_attendance.student_id as student_id,student_attendance.date as today,student_attendance.status as attendance,student_attendance.roll_no as atten_roll_no FROM student_attendance 
                            where student_attendance.session_id='" . mysql_real_escape_string($id) . "' and student_attendance.section='" . mysql_real_escape_string($section) . "' 
                            and student_attendance.date='" . mysql_real_escape_string($date) . "' GROUP BY student_attendance.id ORDER BY student_attendance.student_id asc";

        $new_query = "SELECT * FROM (" . $query1 . ") as t1 left join (" . $query2 . ") as t2 ON t1.id = t2.student_id ";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($new_query);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):
                $record[] = $object;
            endwhile;

        endif;
        return $record;
    }

    /* Get List section Students */

    function sectionSessionSingleStudentsWithAttendance($student_id) {

        $query = new query('student,session_students_rel,student_family_details');
        $query->Field = ('student.id, student.reg_id, student.date_of_admission, student.course_of_admission, student.first_name, student.last_name, student.sex, student.date_of_birth, student.email, student.phone,session_students_rel.roll_no as roll_no,student_family_details.father_name,student_family_details.mother_name,student_family_details.household_annual_income');
        $query->Where = "where student.id='" . mysql_real_escape_string($student_id) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id";
        return $query->ListOfAllRecords('object');
    }

    function checkStudentSection($session_id, $student_id) {

        $this->Field = ('section');
        $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' and student_id='" . mysql_real_escape_string($student_id) . "'  ORDER BY id asc";
        $reslul = $this->DisplayOne();
        if ($reslul):
            return $reslul->section;
        else:
            return false;
        endif;
    }

    function listAllSessionSection($session_id, $show_active = 0, $result_type = 'object') {
        $this->Field = ('section');
        if ($show_active):
            $this->Where = "where id!='0' and session_id='" . mysql_real_escape_string($session_id) . "' GROUP BY section ORDER BY id asc";
        else:
            $this->Where = "where id!='0' and session_id='" . mysql_real_escape_string($session_id) . "' GROUP BY section ORDER BY id asc";
        endif;
        if ($result_type == 'object') {
            return $this->DisplayAll();
        } else {
            return $this->ListOfAllRecords('object');
        }
    }

    function getSessionStudentBySection($session_id) {
        $this->Field = ('session_id,section,MAX(roll_no) as roll_no,count(*) as count,MAX(roll_no) as max_roll');
        $this->Where = "where id!='0' and session_id='" . mysql_real_escape_string($session_id) . "' and student_id!='0' GROUP BY section";
        return $this->ListOfAllRecords('array');
    }

    function getSessionSectionMaxRollNo($session_id, $section) {
        $this->Field = ('MAX(roll_no) as max_roll');
        $this->Where = "where id!='0' and session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' GROUP BY section ORDER BY roll_no desc limit 1";
        $res = $this->DisplayOne();
        if ($res):
            return $res->max_roll;
        else:
            return '0';
        endif;
    }

    #check roll no for this section  

    function checkRollnoForSection($session_id, $section, $roll_no, $id) {
        $this->Where = "where id!='" . mysql_real_escape_string($id) . "' and session_id='" . mysql_real_escape_string($session_id) . "' and section='" . mysql_real_escape_string($section) . "' and roll_no='" . mysql_real_escape_string($roll_no) . "' limit 1";
        return $this->DisplayOne();
    }

    #count the sex in the section class

    function countSexInSection($session_id, $section, $sex) {
        $query = new query('student,session_students_rel');
        $query->Field = ('student.id, student.reg_id, student.sex,count(*) as count');
        $query->Where = "where session_students_rel.session_id='" . mysql_real_escape_string($session_id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and student.sex='" . mysql_real_escape_string($sex) . "' and session_students_rel.student_id=student.id limit 1";

        $record = $query->DisplayOne();
        if ($record):
            return $record->count;
        else:
            return '0';
        endif;
    }

    #Get the  report document

    function get_various_student_report($school, $session_id, $section, $report_type) {
        $record = array();
        if ($report_type == 'admission_detail' || $report_type == 'student_type' || $report_type == 'contact_detail' || $report_type == 'category_detail' || $report_type == 'subject_detail' || $report_type == 'subject_detail' || $report_type == 'withdrawal_detail' || $report_type == 'address_label'):
            $query = new query('student,session_students_rel,student_family_details');
            $query->Field = ('student.*,session_students_rel.session_id as session_id,session_students_rel.roll_no as roll_no,session_students_rel.section as section,session_students_rel.id as rel_id,session_students_rel.concession,session_students_rel.concession_type,session_students_rel.amount,student_family_details.father_name,student_family_details.mother_name,student_family_details.father_occupation,student_family_details.mother_occupation,student_family_details.household_annual_income');
            if (empty($section)):
                $query->Where = "where student.school_id='" . mysql_real_escape_string($school) . "' and session_students_rel.session_id='" . mysql_real_escape_string($session_id) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id and student.is_active='1' GROUP BY student.id ORDER BY session_students_rel.roll_no asc";
            else:
                $query->Where = "where student.school_id='" . mysql_real_escape_string($school) . "' and session_students_rel.session_id='" . mysql_real_escape_string($session_id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id and student.is_active='1' GROUP BY student.id ORDER BY session_students_rel.roll_no asc";
            endif;
            $record = $query->ListOfAllRecords('object');
        endif;
        return $record;
    }

    function download_report_various_student_report($school, $session_id, $section, $report_type, $report_array) {
        $record = array();
        if ($report_type == 'admission_detail' || $report_type == 'student_type' || $report_type == 'contact_detail' || $report_type == 'category_detail' || $report_type == 'subject_detail' || $report_type == 'subject_detail' || $report_type == 'withdrawal_detail' || $report_type == 'address_label'):
            $query = new query('student,session_students_rel,student_family_details');
            $query->Field = ('student.*,session_students_rel.session_id as session_id,session_students_rel.roll_no as roll_no,session_students_rel.section as section,session_students_rel.id as rel_id,session_students_rel.concession as concession,session_students_rel.concession_type as concession_type,session_students_rel.concession,student_family_details.father_name,student_family_details.mother_name,student_family_details.father_occupation,student_family_details.mother_occupation,student_family_details.household_annual_income');
            if (empty($section)):
                $query->Where = "where student.school_id='" . mysql_real_escape_string($school) . "' and session_students_rel.session_id='" . mysql_real_escape_string($session_id) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY session_students_rel.roll_no asc";
            else:
                $query->Where = "where student.school_id='" . mysql_real_escape_string($school) . "' and session_students_rel.session_id='" . mysql_real_escape_string($session_id) . "' and session_students_rel.section='" . mysql_real_escape_string($section) . "' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id GROUP BY student.id ORDER BY session_students_rel.roll_no asc";
            endif;
            $record = $query->ListOfAllRecords('object');
        endif;

        if ($report_type == 'category_detail' && !empty($record)):
            foreach ($record as $key => $value):
                if (($value->category == '') || ($value->category == 'None') || ($value->category == 'GEN')):
                    unset($record[$key]);
                endif;
            endforeach;
        endif;

        if (!empty($record)):
            if ($report_type == 'admission_detail'):
                $session = get_object('session', $session_id);
                $filename = $report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "CHECK LIST DETAIL OF ADMISSION DETAIL & DOB\n\n";

                echo "Session Name" . "\t" . $session->title . "\n";
                if (!empty($section)):
                    echo "Section" . "\t" . $section . "\n";
                endif;
                echo "\n";
                echo 'Sr. No.' . "\t" . 'DATE OF ADMISSION' . "\t" . 'ADMISSION NO.' . "\t" . 'ROLL NO.' . "\t" . 'STUDENT NAME' . "\t" . 'FATHER NAME' . "\t" . 'MOTHER NAME' . "\t" . 'ADDRESS' . "\t" . 'DATE OF BIRTH' . "\n";
                $sr = '1';
                foreach ($record as $key => $value):
                    $QueryP = new studentAddress();
                    $s_p_ad = $QueryP->getStudentAddressByType($value->id, 'permanent');
                    echo $sr . "\t" . date('d M Y', strtotime($value->date_of_admission)) . "\t" . $value->reg_id . "\t" . $value->roll_no . "\t" . ucfirst($value->first_name) . " " . $value->last_name . "\t" . $value->father_name . "\t" . $value->mother_name . "\t";
                    if (is_object($s_p_ad)):
                        echo $s_p_ad->address1 . ", " . $s_p_ad->city . ", " . $s_p_ad->tehsil . ", " . $s_p_ad->district . ", " . $s_p_ad->state . ", " . $s_p_ad->zip . "\t";
                    endif;
                    echo date('d M Y', strtotime($value->date_of_birth)) . "\t\n";
                    $sr++;
                endforeach;
                exit;
            elseif ($report_type == 'withdrawal_detail'):
                $session = get_object('session', $session_id);
                $filename = $report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "ADMISSION & WITHDRAWAL REGISTER OF THE STUDENTS\n\n";

                echo "Session Name" . "\t" . $session->title . "\n";
                if (!empty($section)):
                    echo "Section" . "\t" . $section . "\n";
                endif;
                echo "\n";
                echo 'Sr. No.' . "\t" . 'DATE OF ADMISSION' . "\t" . 'ADMISSION NO.' . "\t" . 'ROLL NO.' . "\t" . 'STUDENT NAME' . "\t" . 'DATE OF BIRTH' . "\t" . 'FATHER NAME' . "\t" . 'MOTHER NAME' . "\t" . 'FATHER OCCUPATION' . "\t" . 'CATEGORY' . "\t" . 'RESIDENCE OF ADDRESS' . "\t\n";
                $sr = '1';
                foreach ($record as $key => $value):
                    $QueryP = new studentAddress();
                    $s_p_ad = $QueryP->getStudentAddressByType($value->id, 'permanent');
                    echo $sr . "\t" . date('d M Y', strtotime($value->date_of_admission)) . "\t" . $value->reg_id . "\t" . $value->roll_no . "\t" . ucfirst($value->first_name) . " " . $value->last_name . "\t" . date('d M Y', strtotime($value->date_of_birth)) . "\t" . $value->father_name . "\t" . $value->mother_name . "\t" . $value->father_occupation . "\t" . $value->category . "\t";
                    if (is_object($s_p_ad)):
                        echo $s_p_ad->address1 . ", " . $s_p_ad->city . ", " . $s_p_ad->state . ", " . $s_p_ad->zip . "\t\n";
                    else:
                        echo "\t\n";
                    endif;

                    $sr++;
                endforeach;
                exit;
            elseif ($report_type == 'category_detail'):
                $session = get_object('session', $session_id);
                $filename = $report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "CHECK LIST DETAIL STATEMENT OF SC,ST & OBC STUDENTS\n\n";
                echo "Session Name" . "\t" . $session->title . "\n";
                if (!empty($section)):
                    echo "Section" . "\t" . $section . "\n";
                endif;
                echo "\n";
                echo 'Sr. No.' . "\t" . 'DATE OF ADMISSION' . "\t" . 'ADMISSION NO.' . "\t" . 'ROLL NO.' . "\t" . 'STUDENT NAME' . "\t" . 'FATHER NAME' . "\t" . 'MOTHER NAME' . "\t" . 'ADDRESS' . "\t" . 'CATEGORY' . "\t" . "\t\n";
                $sr = '1';
                foreach ($record as $key => $value):
                    $QueryP = new studentAddress();
                    $s_p_ad = $QueryP->getStudentAddressByType($value->id, 'permanent');
                    echo $sr . "\t" . date('d M Y', strtotime($value->date_of_admission)) . "\t" . $value->reg_id . "\t" . $value->roll_no . "\t" . ucfirst($value->first_name) . " " . $value->last_name . "\t" . $value->father_name . "\t" . $value->mother_name . "\t";

                    if (is_object($s_p_ad)):
                        echo $s_p_ad->address1 . "," . $s_p_ad->city . ", " . $s_p_ad->state . ", " . $s_p_ad->zip . "\t";
                    else:
                        echo "\t";
                    endif;
                    echo $value->category . "\t\n";
                    $sr++;
                endforeach;
                exit;
            elseif ($report_type == 'subject_detail'):
                $session = get_object('session', $session_id);

                $QueryStComSub = new subject();
                $cmpSub = $QueryStComSub->GetSessionSubjectsInArray($session->compulsory_subjects);

                #get the student subject list if enter previous
                $QueryStELcSubb = new subject();
                $elcSub = $QueryStELcSubb->GetSessionSubjectsInArray($session->elective_subjects);

                $filename = $report_type . ".csv";
                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "CHECK LIST OF REGISTERED CANDIDATE WITH SUBJECT\n\n";
                echo "Session Name" . "\t" . $session->title . "\n";
                if (!empty($section)):
                    echo "Section" . "\t" . $section . "\n";
                endif;
                echo "\n";
                echo 'REG. NO.' . "\t" . 'ROLL NO.' . "\t" . 'STUDENT NAME' . "\t" . 'FATHER NAME' . "\t" . 'MOTHER NAME' . "\t" . 'CATEGORY' . "\t" . 'RELIGION' . "\t" . 'GENDER' . "\t";
                if (!empty($cmpSub)):
                    foreach ($cmpSub as $key => $val):
                        echo "SUB " . ($key + 1) . "\t";
                    endforeach;
                endif;
                if (!empty($elcSub)):
                    foreach ($elcSub as $Ekey => $Eval):
                        echo "ELE SUB " . ($Ekey + 1) . "\t";
                    endforeach;
                endif;
                $ttl_elc = count($elcSub);
                echo "\n";

                foreach ($record as $key => $value):
                    $QueryStElcSub = new studentSubject();
                    $StelcSub = $QueryStElcSub->getStudentSubjectTitleInArray($session_id, $value->id, 'Elective');

                    echo $value->reg_id . "\t" . $value->roll_no . "\t" . ucfirst($value->first_name) . " " . $value->last_name . "\t" . $value->father_name . "\t" . $value->mother_name . "\t" . $value->category . "\t" . $value->religion . "\t" . $value->sex . "\t";

                    if (!empty($cmpSub)):
                        foreach ($cmpSub as $Kkey => $Vval):
                            echo $Vval . "\t";
                        endforeach;
                    endif;

                    if (!empty($StelcSub)): $el_st = '1';
                        foreach ($StelcSub as $Skey => $Sval):
                            echo $Sval . "\t";
                            $el_st++;
                        endforeach;
                        while ($el_st <= $ttl_elc):
                            echo "\t";
                            $el_st++;
                        endwhile;
                    else: $el_st = '1';
                        while ($el_st <= $ttl_elc):
                            echo "\t";
                            $el_st++;
                        endwhile;
                    endif;
                    echo "\n";
                endforeach;
                exit;
            elseif ($report_type == 'contact_detail'):
                $session = get_object('session', $session_id);
                $filename = $report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "Statement of " . $report_array[$report_type] . "\n\n";
                echo "Session Name" . "\t" . $session->title . "\n";
                if (!empty($section)):
                    echo "Section" . "\t" . $section . "\n";
                endif;
                echo "\n";
                echo 'Reg ID' . "\t" . 'ROLL No.' . "\t" . 'Student Name' . "\t" . 'Contact No.' . "\t" . 'Permanent Address' . "\t" . "\n";
                foreach ($record as $key => $value):
                    #get student permanent address
                    $QueryP = new studentAddress();
                    $s_p_ad = $QueryP->getStudentAddressByType($value->id, 'permanent');

                    #get student correspondence address
                    $QueryC = new studentAddress();
                    $s_c_ad = $QueryC->getStudentAddressByType($value->id, 'correspondence');

                    echo $value->reg_id . "\t" . $value->roll_no . "\t" . ucfirst($value->first_name) . " " . $value->last_name . "\t" . $value->phone . "\t";

                    if (is_object($s_p_ad)):
                        echo $s_p_ad->address1 . ", " . $s_p_ad->city . ", " . $s_p_ad->state . ", " . $s_p_ad->zip;
                    endif;
                    echo "\t";

                    echo "\t\n";
                endforeach;
                exit;
            elseif ($report_type == 'student_type'):
                $session = get_object('session', $session_id);
                $filename = $report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "Statement of " . $report_array[$report_type] . "\n\n";
                echo "Session Name" . "\t" . $session->title . "\n";
                if (!empty($section)):
                    echo "Section" . "\t" . $section . "\n";
                endif;
                echo "\n";
                echo 'Reg ID' . "\t" . 'ROLL No.' . "\t" . 'Student Name' . "\t" . 'Father Name' . "\t" . 'Student Granted' . "\t" . 'Concession' . "\n";
                foreach ($record as $key => $value):
                    echo $value->reg_id . "\t" . $value->roll_no . "\t" . ucfirst($value->first_name) . " " . $value->last_name . "\t" . $value->father_name . "\t";
                    if (empty($value->concession)):echo "None";
                    else: echo $value->concession;
                    endif;
                    echo "\t" . $value->concession_type . "\t\n";
                endforeach;
                exit;
            elseif ($report_type == 'address_label'):
                $session = get_object('session', $session_id);
                $filename = $report_type . ".csv";

                header("Content-type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$filename");

                echo "Statement of " . $report_array[$report_type] . "\n\n";
                echo "Session Name" . "\t" . $session->title . "\n";
                if (!empty($section)):
                    echo "Section" . "\t" . $section . "\n";
                endif;
                echo "\n";
                echo 'Sr. No.' . "\t" . 'Address' . "\n";
                $sr = '1';
                foreach ($record as $key => $value):
                    #get student permanent address
                    $QueryP = new studentAddress();
                    $s_p_ad = $QueryP->getStudentAddressByType($value->id, 'permanent');

                    echo $sr . "\t";
                    echo ucfirst($value->first_name) . " " . $value->last_name . ", S/o. | D/o. " . $value->father_name . ", ";
                    if (is_object($s_p_ad)):
                        echo $s_p_ad->address1 . "," . $s_p_ad->city . ", " . $s_p_ad->state . ", " . $s_p_ad->zip;
                    endif;
                    echo "\t\n";
                    $sr++;
                endforeach;
                exit;
            endif;
        else:
            return false;
        endif;
    }

    # check single student 

    function checkStudentInSession($student_id) {
        $query = new query('session_students_rel');
        $query->Where = "where student_id='" . mysql_real_escape_string($student_id) . "'";
        return $query->DisplayOne();
    }

    # check single student 

    function checkDuplicateRecord($session, $section, $roll_no) {
        $query = new query('session_students_rel');
        $query->Where = "where session_id='" . mysql_real_escape_string($session) . "' AND section='" . mysql_real_escape_string($section) . "' AND roll_no='" . mysql_real_escape_string($roll_no) . "'";
        return $query->DisplayOne();
    }

    #get_current_session_with_students   

    function get_current_session_with_students($id) {
        $record = array();
        $Query = "SELECT session.id AS id, session.title, session.start_date, session.end_date, COUNT( session_students_rel.session_id ) AS count
                    FROM session 
                    LEFT JOIN session_students_rel ON session.id = session_students_rel.session_id
                    WHERE session.school_id =  '" . $id . "'
                    AND DATE( session.start_date ) <= CAST('" . date('Y-m-d') . "' AS DATE ) 
                    AND DATE( session.end_date ) >= CAST('" . date('Y-m-d') . "' AS DATE ) 
                    GROUP BY session.id";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($Query);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):
                $record[] = $object;
            endwhile;
        endif;
        return $record;
    }

    public static function getTotalStudentCount($school_id, $field = '') {
        $obj = new query('session');
        $obj->Field = ('id');
        $obj->Where = "WHERE session.school_id='" . mysql_real_escape_string($school_id) . "' AND session.is_active='1' AND DATE(session.start_date) <= CAST('" . date('Y-m-d') . "' AS DATE ) AND DATE(session.end_date) >= CAST('" . date('Y-m-d') . "' AS DATE)";
        $data = $obj->ListOfAllRecords();
        $session_ids_array = array();
        foreach ($data as $sess_ids) {
            $session_ids_array[] = $sess_ids['id'];
        }
        $session_ids = implode(',', $session_ids_array);

//        $query = new query('student,session_students_rel,student_family_details');
        $query = new query('student');
        if ($field != '') {
//            $query->Where = "where session_students_rel.session_id IN($session_ids) and student.sex='$field' and session_students_rel.student_id=student.id and student_family_details.student_id=student.id and student.is_active='1' AND student.is_deleted='0' GROUP BY student.id ORDER BY student.id asc";
            $query->Where = "JOIN `session_students_rel` ON `student`.`id` = `session_students_rel`.`student_id` JOIN `student_family_details` ON `student_family_details`.`student_id`=`student`.`id` WHERE `student`.`sex` = '" . $field . "' AND `student`.`is_active` = '1' AND `student`.`is_deleted` = '0' AND `session_students_rel`.`session_id` IN($session_ids) GROUP BY `student`.`id` ORDER BY student.id ASC";
        } else {
            $query->Where = "where session_students_rel.session_id IN($session_ids) and session_students_rel.student_id=student.id and student_family_details.student_id=student.id and student.is_active='1' AND student.is_deleted='0' GROUP BY student.id ORDER BY student.id asc";
        }
        $data_count = $query->ListOfAllRecords();
        return count($data_count);
    }

    function get_current_session_with_students_with_boys_girls($id) {
        $record = array();
        $query1 = "SELECT session.id AS id,course_id AS course_id, session.title, session.start_date, session.end_date FROM session 
                            where session.school_id='" . mysql_real_escape_string($id) . "' AND session.is_active='1' AND DATE(session.start_date) <= CAST('" . date('Y-m-d') . "' AS DATE ) AND DATE(session.end_date) >= CAST('" . date('Y-m-d') . "' AS DATE) GROUP BY session.id ORDER BY session.position asc";

        $query2 = "SELECT session_id,count(session_students_rel.student_id) as total_student FROM `session_students_rel`,`student` 
                            WHERE session_students_rel.id!='0' and student.is_active='1' and student.is_deleted='0' and student.id=session_students_rel.student_id GROUP BY session_students_rel.session_id";

        $query3 = "SELECT session_students_rel.session_id as male_session, COUNT(student.sex) AS male FROM  `session_students_rel`,`student`
                             WHERE student.is_active='1' AND student.is_deleted='0' AND student.sex='Male' AND student.id=session_students_rel.student_id GROUP BY  `session_id` ";

        $query4 = "SELECT session_students_rel.session_id as female_session, COUNT(student.sex ) AS female FROM  `session_students_rel`,`student`
                             WHERE student.is_active='1' AND student.is_deleted='0' AND student.sex='Female' AND student.id=session_students_rel.student_id GROUP BY  `session_id` ";


        $q1 = "SELECT * FROM (" . $query1 . ") as t1 left join (" . $query2 . ") as t2 ON t1.id = t2.session_id ";
        $q2 = "SELECT * FROM (" . $q1 . ") as t3 left join (" . $query3 . ") as t4 ON t3.id = t4.male_session ";
        $q3 = "SELECT * FROM (" . $q2 . ") as t5 left join (" . $query4 . ") as t6 ON t5.id = t6.female_session ";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($q3);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):
                $record[] = $object;
            endwhile;
        endif;
        return $record;
    }

    function get_current_session_fee_record_with_balance_and_paid($id) {
        $record = array();
        $record_list = array();
        $query1 = "SELECT session.id AS id,session.course_id AS course_id, session.title, session.start_date, session.end_date, session.frequency,session_interval.id as interval_id FROM session,session_interval 
                            where session.school_id='" . mysql_real_escape_string($id) . "' AND session.is_active='1' AND DATE(session.start_date) <= CAST('" . date('Y-m-d') . "' AS DATE ) AND DATE(session.end_date) >= CAST('" . date('Y-m-d') . "' AS DATE) AND DATE(session_interval.from_date) <= CAST('" . date('Y-m-d') . "' AS DATE ) AND DATE(session_interval.to_date) >= CAST('" . date('Y-m-d') . "' AS DATE) AND session_interval.session_id=session.id GROUP BY session.id ORDER BY session.position asc";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($query1);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):
                $ObjQn = new studentSessionInterval();
                $pending = $ObjQn->getPreviousSessionBalance($object->id, $object->interval_id);

                $record[] = $object;
                $record_list[] = array('id' => $object->id, 'course_id' => $object->course_id, 'title' => $object->title, 'title' => $object->title, 'start_date' => $object->start_date, 'end_date' => $object->end_date, 'total_paid' => $pending['paid'], 'total_fee' => $pending['total']);
            endwhile;
        endif;

        return $record_list;
    }

    function get_current_session_fee_record($id) {

        $record = array();
        //$query=new query('student,session_students_rel');
        $QueryObj = new query('session');
        $QueryObj->Field = ('session.id,session.title,session.start_date,session.end_date');
        $QueryObj->Where = "where session.school_id='" . mysql_real_escape_string($id) . "' AND DATE(session.start_date ) <= CAST('" . date('Y-m-d') . "' AS DATE )  AND DATE(session.end_date ) >= CAST('" . date('Y-m-d') . "' AS DATE )";
        $All_rec = $QueryObj->ListOfAllRecords('object');
        if (!empty($All_rec)):
            foreach ($All_rec as $kk => $object):
                $ct_rec = '';
                $SubQueryObj = new query('student_fee');
                $SubQueryObj->Field = ('student_fee.id,sum(student_fee.total_amount) as amount');
                $SubQueryObj->Where = "where student_fee.session_id='" . mysql_real_escape_string($object->id) . "' group by student_fee.session_id";

                $ct_rec = $SubQueryObj->DisplayOne();
                if (empty($ct_rec->amount)) {
                    $ct_rec->amount = '0';
                }
                $record[] = array('session_id' => $object->id, 'title' => $object->title, 'amount' => $ct_rec->amount);
            endforeach;
        endif;
        return $record;
    }

    function getStudentCurrentSession($school, $id, $act = FALSE) {
        $QueryObj = new query('session,session_students_rel,student');
        $QueryObj->Field = ('session.id,session.title,session.start_date,session.end_date,session_students_rel.session_id,session_students_rel.student_id,student.id as s_id');
        $QueryObj->Where = "where session.school_id='" . mysql_real_escape_string($school) . "' AND student.id='" . mysql_real_escape_string($id) . "' AND DATE(session.start_date ) <= CAST('" . date('Y-m-d') . "' AS DATE )  AND DATE(session.end_date ) >= CAST('" . date('Y-m-d') . "' AS DATE ) and session_students_rel.session_id=session.id and session_students_rel.student_id=student.id";
        //$QueryObj->print=1;
        $rec = $QueryObj->DisplayOne();
        if ($act == TRUE) {
            if (is_object($rec)):
                return $rec->id;
            else:
                return 'none';
            endif;
        } elseif ($rec) {
            return $rec->title;
        } else {
            return 'None';
        }
    }

    function getStudentCurrentSessionInfo($school, $id) {
        $QueryObj = new query('session,session_students_rel,student');
        $QueryObj->Field = ('session.id,session.title,session.start_date,session.end_date,session_students_rel.id as rel_id,session_students_rel.session_id,session_students_rel.section,session_students_rel.concession,session_students_rel.amount,session_students_rel.start_concession,session_students_rel.concession_type,session_students_rel.student_id,student.id as s_id');
        $QueryObj->Where = "where session.school_id='" . mysql_real_escape_string($school) . "' AND student.id='" . mysql_real_escape_string($id) . "' AND DATE(session.start_date ) <= CAST('" . date('Y-m-d') . "' AS DATE )  AND DATE(session.end_date ) >= CAST('" . date('Y-m-d') . "' AS DATE ) and session_students_rel.session_id=session.id and session_students_rel.student_id=student.id";

        $rec = $QueryObj->DisplayOne();
        if ($rec):
            return $rec;
        else:
            return '';
        endif;
    }

    function getStudentCurrentSessionId($school, $id) {
        $QueryObj = new query('session,session_students_rel,student');
        $QueryObj->Field = ('session.id,session.title,session.start_date,session.end_date,session_students_rel.session_id,session_students_rel.student_id,student.id as s_id');
        $QueryObj->Where = "where session.school_id='" . mysql_real_escape_string($school) . "' AND student.id='" . mysql_real_escape_string($id) . "' AND DATE(session.start_date ) <= CAST('" . date('Y-m-d') . "' AS DATE )  AND DATE(session.end_date ) >= CAST('" . date('Y-m-d') . "' AS DATE ) and session_students_rel.session_id=session.id and session_students_rel.student_id=student.id";

        $rec = $QueryObj->DisplayOne();
        if ($rec):
            return $rec->id;
        else:
            return 'None';
        endif;
        return $record;
    }

    function get_student_id_using_session_id($session_id, $sec) {
        $this->Where = "WHERE `session_id` = '$session_id' AND `section` = '$sec' ORDER BY `roll_no` ASC";
        return $this->ListOfAllRecords();
    }

    function get_student_id_using_session_id_no_section($session_id) {
        $this->Where = "WHERE `session_id` = '$session_id' ORDER BY `roll_no` ASC";
        return $this->ListOfAllRecords();
    }

    function get_login_student_section($login_student_id) {
        $this->Where = "WHERE `student_id` = '$login_student_id'";
        return $this->DisplayOne();
    }

}

# Session Fee Type

class session_student_fee extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('session_student_fee');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'session_id', 'student_id', 'fee_head', 'amount', 'on_date');
    }

    /*
     * Create new session_fee_type list or update existing theme
     */

    function saveData($POST, $session_id, $session_heads) {

        if (!empty($POST)):

            foreach ($POST as $key => $value):

                #delete the previous entry 
                $ObjDelQuery = new query('session_student_fee');
                $ObjDelQuery->Where = "where student_id='" . $key . "' and session_id='" . $session_id . "'";
                $rec = $ObjDelQuery->DisplayOne();

                if (empty($rec)):
                    if (!empty($session_heads)):
                        foreach ($session_heads as $head_key => $head_value):
                            $ObjQuery = new query('session_student_fee');
                            $ObjQuery->Data['session_id'] = $session_id;
                            $ObjQuery->Data['student_id'] = $key;
                            $ObjQuery->Data['fee_head'] = $head_value['fee_id'];
                            $ObjQuery->Data['amount'] = $head_value['amount'];
                            //$ObjQuery->print=1;
                            $ObjQuery->Data['on_date'] = date('Y-m-d');
                            $ObjQuery->Insert();
                        endforeach;
                    endif;
                endif;
            endforeach;
        endif;
        return true;
    }

    function saveSingleStudentData($student_id, $session_id, $session_heads) {
        if (!empty($session_heads)):
            $ObjDelQuery = new query('session_student_fee');
            $ObjDelQuery->Where = "where student_id='" . $student_id . "' and session_id='" . $session_id . "'";
            $ObjDelQuery->Delete_where();

            foreach ($session_heads as $head_key => $head_value):
                $ObjQuery = new query('session_student_fee');
                $ObjQuery->Data['session_id'] = $session_id;
                $ObjQuery->Data['student_id'] = $student_id;
                $ObjQuery->Data['fee_head'] = $head_value['title'];
                $ObjQuery->Data['amount'] = $head_value['fee'];
                //$ObjQuery->print=1;
                $ObjQuery->Data['on_date'] = date('Y-m-d');
                $ObjQuery->Insert();
            endforeach;
        endif;

        return true;
    }

    function removeSessionStudentFee($session_id, $student_id) {
        $ObjDelQuery = new query('session_student_fee');
        $ObjDelQuery->Where = "where student_id='" . $student_id . "' and session_id='" . $session_id . "'";
        $ObjDelQuery->Delete_where();

        return true;
    }

    function getSingleStudentFeeHeadValue($session_id, $student_id, $head) {
        $ObjDelQuery = new query('session_student_fee');
        $ObjDelQuery->Where = "where student_id='" . $student_id . "' and session_id='" . $session_id . "' and fee_head='" . $head . "'";
        $rec = $ObjDelQuery->DisplayOne();
        if ($rec):
            return $rec->amount;
        else:
            return '0';
        endif;

        return true;
    }

    function GetStudentPayHeads($session_id, $student_id, $result_type = 'object') {
        $Query = new query('fee_type_master,session_student_fee');
        $Query->Field = "session_student_fee.student_id as student_id,session_student_fee.session_id as session_id,session_student_fee.fee_head,session_student_fee.amount as amount,fee_type_master.id as fee_id,fee_type_master.name as title,fee_type_master.type";
        $Query->Where = "where fee_type_master.id=session_student_fee.fee_head and fee_type_master.is_active='1' and session_student_fee.student_id='" . mysql_real_escape_string($student_id) . "' and session_student_fee.session_id='" . mysql_real_escape_string($session_id) . "' ORDER BY fee_type_master.position";
        if ($result_type == 'object'):
            return $Query->DisplayAll();
        elseif ($result_type == 'array'):
            return $Query->ListOfAllRecords('array');
        else:
            return $Query->ListOfAllRecords('object');
        endif;
    }

    function GetStudentPayHeadsAmount($session_id, $student_id, $head_id) {
        $Query = new query('fee_type_master,session_student_fee');
        $Query->Field = "session_student_fee.amount as amount,fee_type_master.type";
        $Query->Where = "where fee_type_master.id=session_student_fee.fee_head and fee_type_master.is_active='1' and session_student_fee.student_id='" . mysql_real_escape_string($student_id) . "' and session_student_fee.fee_head='" . mysql_real_escape_string($head_id) . "' and session_student_fee.session_id='" . mysql_real_escape_string($session_id) . "' ORDER BY fee_type_master.position";
        $rec = $Query->DisplayOne();
        if (is_object($rec)):
            return $rec;
        else:
            return '';
        endif;
    }

    function GetStudentPayHeadsWithKeys($session_id, $student_id) {
        $Query = new query('fee_type_master,session_student_fee');
        $Query->Field = "session_student_fee.student_id as student_id,session_student_fee.session_id as session_id,session_student_fee.fee_head,session_student_fee.amount as amount,fee_type_master.id as fee_id,fee_type_master.name as title,fee_type_master.type";
        $Query->Where = "where fee_type_master.id=session_student_fee.fee_head and fee_type_master.is_active='1' and session_student_fee.student_id='" . mysql_real_escape_string($student_id) . "' and session_student_fee.session_id='" . mysql_real_escape_string($session_id) . "' ORDER BY fee_type_master.position";

        $rec = $Query->ListOfAllRecords('array');
        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[$value['fee_id']] = $value;
            endforeach;
        endif;
        return $records;
    }

}

?>
