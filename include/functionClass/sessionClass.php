<?php

class session extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('session');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'course_id', 'title', 'start_date', 'end_date', 'year', 'incharge', 'total_compulsory_subject', 'total_elective_subject', 'max_boy', 'max_girl', 'max_sections_students', 'compulsory_subjects', 'elective_subjects', 'total_sections_allowed', 'late_fee_days', 'absent_fine', 'late_fee_rate', 'on_date', 'is_active', 'update_date', 'interval_of_fee', 'frequency', 'position');
    }

    /*
     * Create new session list or update existing theme
     */

    function saveData($POST) {
        //print_r($POST); exit;

        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['start_date'] = date('Y-m-d', strtotime($POST['start_date']));
        $this->Data['end_date'] = date('Y-m-d', strtotime($POST['end_date']));

        $this->Data['total_compulsory_subject'] = count($POST['compulsory_subjects']);
        $this->Data['total_elective_subject'] = count($POST['elective_subjects']);
        $this->Data['compulsory_subjects'] = implode(',', ($POST['compulsory_subjects']));
        $this->Data['elective_subjects'] = implode(',', $POST['elective_subjects']);

        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';
        $this->Data['start_date'] = date('Y-m-d', strtotime($this->Data['start_date']));
        $this->Data['end_date'] = date('Y-m-d', strtotime($this->Data['end_date']));

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get fee list by id
     */

    function getRecord($id) {
        return $this->_getObject('session', $id);
    }

    public static function getSessionDetails($id) {
        $obj = new session;
        $obj->Where = "where id=$id";
        return $obj->DisplayOne();
    }

    // Get session subjects
    function getSessionSubjects($session_id) {
        $obj = new query('session');
        $obj->Where = "where id='$session_id'";
        $data = $obj->DisplayOne();
        $subjects = $data->compulsory_subjects;
        $obj1 = new query('subject_master');
        $obj1->Where = "where id IN($subjects)";
        $sdata = $obj1->ListOfAllRecords('object');
        return $sdata;
    }

    // Get Session Details
    function getSessionIntervalData($session_ids) {
        $this->Where = "where id IN($session_ids)";
        return $this->ListOfAllRecords('object');
    }

    /*
     * Get List of all session list in array
     */

    function listAll($school_id, $show_active = 0, $result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active)
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_active='1' ORDER BY id desc";
        else
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' ORDER id desc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /*
     * Get List of all session list in array
     */

    function listOfYearSession($school_id, $ses_int, $object_type = '') {
        $val = explode('-', $ses_int);
        $to = $val['0'];
        $start = $val['1'];
        $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' AND YEAR(start_date)='" . $start . "' AND YEAR(end_date)='" . $to . "' ORDER BY position asc,id desc";
        if ($object_type == 'object') {
            return $this->ListOfAllRecords('object');
        } else {
            return $this->DisplayAll();
        }
    }

    /*
     * Get List of all session list in array
     */

    function listSchoolSessByRange($school_id, $show_active = 0, $result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active)
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_active='1' ORDER BY position asc,id desc";
        else
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' ORDER BY position asc,id desc";
        $this->print = 1;
        return $this->ListOfAllRecords('object');
    }

    function listOfYearSess($school_id, $show_active = 0) {
        $record = array();

        $this->Field = " id,school_id,start_date,end_date";
        if ($show_active) {
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_active='1' GROUP BY `end_date` ORDER BY `end_date` desc";
        } else {
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' GROUP BY `end_date` ORDER BY `end_date` desc";
        }

        $rec = $this->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $val):
                $record[] = array(
                    'start_date' => $val->start_date,
                    'end_date' => $val->end_date,
                    'from' => date('Y', strtotime($val->end_date)),
                    'to' => date('Y', strtotime($val->start_date))
                );
            endforeach;
        endif;
        return $record;
    }

    #all session of 

    function listAllCurrent($school_id, $show_active = 0, $result_type = 'object') {

        if ($show_active)
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and is_active='1' ORDER BY year desc";
        else
            $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' ORDER BY year desc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

#all session of struck off students

    function listAllCurrentSessionStruckOff($school_id, $show_active = 0, $session_int, $result_type = 'object') {
        $val = explode('-', $session_int);
        $to = $val['0'];
        $start = $val['1'];
        $query = new query('session,stuck_off_list');
        $query->Field = "session.*,stuck_off_list.session_id";
        if ($show_active) {
            $query->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and YEAR(session.start_date)='" . $start . "' AND YEAR(session.end_date)='" . $to . "' and session.is_active='1' and session.id=stuck_off_list.session_id and stuck_off_list.stuck_off='1' and stuck_off_list.revert='0' group by session.id ORDER BY session.year desc";
        } else {
            $query->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and YEAR(session.start_date)='" . $start . "' AND YEAR(session.end_date)='" . $to . "' and stuck_off_list.stuck_off='1' and stuck_off_list.revert='0' group by session.id ORDER BY session.year desc";
        }
        if ($result_type == 'object') {
            return $query->DisplayAll();
        } else {
            return $query->ListOfAllRecords('object');
        }
    }
    function listAllCurrentSessionStruckOff2($school_id, $show_active = 0, $session_int, $result_type = 'object') {
        $val = explode('-', $session_int);
        $to = $val['0'];
        $start = $val['1'];
        $query = new query('session,stuck_off_list');
        $query->Field = "session.*,stuck_off_list.session_id";
        if ($show_active) {
            $query->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and YEAR(session.start_date)='" . $start . "' AND YEAR(session.end_date)='" . $to . "' and session.is_active='1' and session.id=stuck_off_list.session_id and stuck_off_list.stuck_off='1' and stuck_off_list.revert='0' group by session.id ORDER BY session.year desc";
        } else {
            $query->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' and YEAR(session.start_date)='" . $start . "' AND YEAR(session.end_date)='" . $to . "' and stuck_off_list.stuck_off='1' and stuck_off_list.revert='0' group by session.id ORDER BY session.year desc";
        }
        $query->print = 1;
        if ($result_type == 'object') {
            return $query->DisplayAll();
        } else {
            return $query->ListOfAllRecords('object');
        }
    }

    #all session of 

    function listAllOnlyCurrent($school_id, $result_type = 'object', $sess_int) {

        $val = explode('-', $sess_int);
        $to = $val['0'];
        $start = $val['1'];
        $this->Where = "where school_id='" . mysql_real_escape_string($school_id) . "' AND YEAR(start_date)='" . $start . "' AND YEAR(end_date)='" . $to . "' ORDER BY position asc,id desc";

        return $this->DisplayAll();
    }

    /*
      #all session of
      function listAllPendingFeeCurrentSession($school_id){
      $result=array();
      $this->Where="where school_id='".mysql_real_escape_string($school_id)."' AND is_active='1' AND DATE(start_date) <= CAST('".date('Y-m-d')."' AS DATE ) AND DATE(end_date) >= CAST('".date('Y-m-d')."' AS DATE) GROUP BY id order by id desc";
      $record=$this->ListOfAllRecords('object');
      if(!empty($record)):
      foreach($record as $key=>$value):
      #get Session last Interval
      $QueryObj=new session_interval();
      $last=$QueryObj->getCurrentLastIntervalId($value->id);
      if($last):
      #check this interval save in student session fee
      $QueryObjInt=new studentSessionInterval();
      $entry=$QueryObjInt->checkIntervaFeeId($last,$value->id);
      if(is_object($entry)):

      else:
      $result[]=$value;
      endif;
      endif;
      endforeach;
      endif;
      return $result;
      }
     */
    /*
     * delete a session list by id
     */

    function deleteRecord($id) {

        $this->id = $id;
        return $this->Delete();
    }

    /*
     * Update session list position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    function getMonthNames($date1, $date2) {
        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        $my = date('mY', $time2);

        $months = array(date('m-Y', $time1));

        while ($time1 < $time2) {
            $time1 = strtotime(date('Y-m-d', $time1) . ' +1 days');

            $months[] = date('m-Y', $time1);
        }
        $months[] = date('m-Y', $time2);
        $months = array_unique($months);

        return $months;
    }

    function LateFee($session_id, $ct_sect, $student, $from, $to) {
        $session = get_object('session', $session_id);
        $time1 = strtotime($from);
        $time2 = strtotime($to);

        $to = date('Y-m-d', strtotime(date('Y-m-d', strtotime($from)) . ' +' . ($session->late_fee_days - 1) . ' days'));

        $QueryObj = new query('student_fee');
        $QueryObj->Field = ("total_amount");
        $QueryObj->Where = "where session_id='" . $session_id . "' and student_id='" . $student . "' and DATE(payment_date) >= CAST('" . $from . "' AS DATE) and DATE(payment_date) <= CAST('" . $to . "' AS DATE)";
        $rec = $QueryObj->DisplayOne();

        if (is_object($rec)):
            return '0';
        elseif (strtotime(date('Y-m-d')) > strtotime($to)):
            return $session->late_fee_rate;
        else:
            return '0';
        endif;
    }

    function listOfLateFee($session_id, $ct_sect, $student, $start_date, $end_date, $frequency, $late_days) {
        $time1 = strtotime($start_date);
        $time2 = strtotime($end_date);


        if ($frequency == '12'):
            $frequency = '1';
        elseif ($frequency == '6'):
            $frequency = '2';
        elseif ($frequency == '4'):
            $frequency = '3';
        elseif ($frequency == '1'):
            $frequency = '12';
        endif;


        $interval = array();
        while ($time1 <= $time2) {
            $new = strtotime(date('Y-m-d', $time1) . ' +' . $frequency . ' months');
            if ($new >= $time2):
                $new = $time2;
            endif;

            $interval[] = array(date('Y-m-d', $time1) => date('Y-m-d', $new));
            $time1 = strtotime(date('Y-m-d', $time1) . ' +' . $frequency . ' months');
        }

        $interval = array_reverse($interval);

        if (!empty($interval)):
            foreach ($interval as $key => $value):
                if (!empty($value)):
                    foreach ($value as $key_key => $value_val):
                        $check_val = date('Y-m-d', strtotime(date('Y-m-d', strtotime($key_key)) . ' +' . $late_days . ' days'));
                        $QueryObj = new query('student_fee');
                        $QueryObj->Field = ("total_amount");
                        $QueryObj->Where = "where session_id='" . $session_id . "' and section='" . $ct_sect . "' and student_id='" . $student . "' and DATE(payment_date) >= CAST('" . $key_key . "' AS DATE) and DATE(payment_date) <= CAST('" . $check_val . "' AS DATE)";
                        $rec = $QueryObj->DisplayOne();
                    endforeach;
                endif;
                // echo date('Y-m-d',strtotime($check_val))."--".date('Y-m-d',strtotime($check_val));
                if (is_object($rec)):
                    unset($interval[$key]);
                elseif (strtotime($check_val) > strtotime(date('Y-m-d'))):
                    unset($interval[$key]);
                endif;
            endforeach;
        endif;

        return $interval;
    }

    function getPendingAmount($pending) {
        $total = '';
        if (!empty($pending)):
            foreach ($pending as $key => $value):
                $total = $total + $value['amount'];
            endforeach;
        endif;
        return $total;
    }

    function GetFeeInterval($start_date, $end_date, $frequency) {
        $time1 = strtotime($start_date);
        $time2 = strtotime($end_date);

        $interval = array();
        while ($time1 <= $time2) {
            $new = strtotime(date('Y-m-d', $time1) . ' +' . $frequency . ' months');
            if ($new >= $time2):
                $new = $time2;
            endif;

            if ($new == $time2):
                $interval[] = array('from' => date('Y-m-d', $time1), 'to' => date('Y-m-d', strtotime(date('Y-m-d', $new) . ' +0 days')));
            else:
                $interval[] = array('from' => date('Y-m-d', $time1), 'to' => date('Y-m-d', strtotime(date('Y-m-d', $new) . ' -1 days')));
            endif;

            $time1 = strtotime(date('Y-m-d', $time1) . ' +' . $frequency . ' months');
        }

        return $interval;
    }

}

# Session Fee Type

class session_interval extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('session_interval');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'session_id', 'from_date', 'to_date', 'interval_position', 'on_date', 'update_date');
    }

    function saveIntervals($interval, $session_id) {
        $queryObj = new query('session_interval');
        $queryObj->Where = "where session_id='" . $session_id . "' order by id asc";
        $record = $queryObj->ListOfAllRecords('object');
        $sr = '1';
        if (!empty($record)):
            foreach ($record as $r_key => $r_val):
                if (array_key_exists($r_key, $interval)):
                    $ObjQuery = new query('session_interval');
                    $ObjQuery->Data['session_id'] = $session_id;
                    $ObjQuery->Data['from_date'] = $interval[$r_key]['from'];
                    $ObjQuery->Data['to_date'] = $interval[$r_key]['to'];
                    $ObjQuery->Data['update_date'] = date('Y-m-d');
                    $ObjQuery->Data['id'] = $r_val->id;
                    if ($sr == '1'):
                        $ObjQuery->Data['interval_position'] = '1';
                    endif;
                    $ObjQuery->Update();
                else:
                    $ObjQuery = new query('session_interval');
                    $ObjQuery->Where = "where id='" . $r_val->id . "'";
                    $ObjQuery->Delete_where();
                endif;
                unset($interval[$r_key]);
                $sr++;
            endforeach;
            if (!empty($interval)):
                foreach ($interval as $r_key => $r_val):
                    $ObjQuery = new query('session_interval');
                    $ObjQuery->Data['session_id'] = $session_id;
                    $ObjQuery->Data['from_date'] = $interval[$r_key]['from'];
                    $ObjQuery->Data['to_date'] = $interval[$r_key]['to'];
                    $ObjQuery->Data['on_date'] = date('Y-m-d');
                    if ($sr == '1'):
                        $ObjQuery->Data['interval_position'] = '1';
                    endif;
                    $ObjQuery->Insert();
                    $sr++;
                endforeach;
            endif;
        else:
            if (!empty($interval)):
                foreach ($interval as $r_key => $r_val):
                    $ObjQuery = new query('session_interval');
                    $ObjQuery->Data['session_id'] = $session_id;
                    $ObjQuery->Data['from_date'] = $interval[$r_key]['from'];
                    $ObjQuery->Data['to_date'] = $interval[$r_key]['to'];
                    $ObjQuery->Data['on_date'] = date('Y-m-d');
                    if ($sr == '1'):
                        $ObjQuery->Data['interval_position'] = '1';
                    endif;
                    $ObjQuery->Insert();
                    $sr++;
                endforeach;
            endif;
        endif;
        return true;
    }

    function listOfInterval($session_id) {
        $this->Where = "where session_id='" . $session_id . "' ORDER BY id asc";
        return $this->ListOfAllRecords('object');
    }

    function listOfCurrentAndFutureInterval($session_id) {
        $this->Where = "where session_id='" . $session_id . "'  AND DATE(to_date ) >= CAST('" . date('Y-m-d') . "' AS DATE)  ORDER BY id asc";
        return $this->ListOfAllRecords('object');
    }

    function getIntervalByDate($session_id, $date) {
        $this->Where = "where session_id='" . $session_id . "'   AND CAST('" . $date . "' AS DATE) <=DATE(to_date) ORDER BY id asc limit 0,1";

        return $this->DisplayOne();
    }

    function getCurrentLastIntervalId($session_id) {
        $current_date = date('Y-m-d');
        $current = '0';
        $last_id = '';
        #Get Session Interval In array
        $SessIntOb = new session_interval();
        $interval = $SessIntOb->listOfIntervalInArray($session_id);
        if (!empty($interval)):
            foreach ($interval as $ik => $iv):
                if ((strtotime($current_date) >= strtotime($iv->from_date)) && (strtotime($current_date) <= strtotime($iv->to_date))):
                    $current = '1';
                endif;
                $last_id = $iv->id;
                if ($current == '1'):
                    break;
                endif;
            endforeach;
        endif;
        return $last_id;
    }

    function listOfIntervalInArray($session_id) {
        $record = array();
        $this->Where = "where session_id='" . $session_id . "' ORDER BY id asc";
        $rec = $this->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $rk => $rv):
                $record[] = $rv;
            endforeach;
        endif;
        return $record;
    }

}

# Session Fee Type

class session_fee_type extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('session_fee_type');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'session_id', 'fee_head', 'amount', 'on_date', 'update_date');
    }

    /*
     * Create new session_fee_type list or update existing theme
     */

    function saveData($POST, $session_id) {
        if (!empty($POST)):
            foreach ($POST as $key => $value):
                if (!empty($value['title']) && !empty($value['fee'])):
                    $ObjQuery = new query('session_fee_type');
                    $ObjQuery->Data['session_id'] = $session_id;
                    $ObjQuery->Data['fee_head'] = $value['title'];
                    $ObjQuery->Data['amount'] = $value['fee'];
                    if (isset($value['id'])):
                        $ObjQuery->Data['update_date'] = date('Y-m-d');
                        $ObjQuery->Data['id'] = $value['id'];
                        $ObjQuery->Update();
                    else:
                        $ObjQuery->Data['on_date'] = date('Y-m-d');
                        $ObjQuery->Insert();
                    endif;
                endif;
            endforeach;
        endif;
        return true;
    }

    /*
     * Get session_fee_type list by id
     */

    function getRecord($id) {
        return $this->_getObject('session_fee_type', $id);
    }

    /*
     * Get List of all session_fee_type list in array
     */

    function listAll($show_active = 0, $result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active)
            $this->Where = "where is_deleted='0' and is_active='1'  ORDER BY position asc";
        else
            $this->Where = "where is_deleted='0' ORDER BY position asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    #get session payheads

    function sessionPayHeads($session_id, $result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        $Query = new query('fee_type_master,session_fee_type');
        $Query->Field = "session_fee_type.*,fee_type_master.id as fee_id,fee_type_master.name as title,fee_type_master.type";
        $Query->Where = "where fee_type_master.id=session_fee_type.fee_head and fee_type_master.is_active='1' and session_fee_type.session_id='" . mysql_real_escape_string($session_id) . "' ORDER BY fee_type_master.position";
        if ($result_type == 'object'):
            return $Query->DisplayAll();
        elseif ($result_type == 'array'):
            return $Query->ListOfAllRecords('array');
        else:
            return $Query->ListOfAllRecords('object');
        endif;
    }

    #get session payheads

    function sessionPayHeadsWithKey($session_id) {
        $records = array();
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        $Query = new query('fee_type_master,session_fee_type');
        $Query->Field = "session_fee_type.*,fee_type_master.id as fee_id,fee_type_master.name as title,fee_type_master.type";
        $Query->Where = "where fee_type_master.id=session_fee_type.fee_head and fee_type_master.is_active='1' and session_fee_type.session_id='" . mysql_real_escape_string($session_id) . "' ORDER BY fee_type_master.position";

        $rec = $Query->ListOfAllRecords('array');
        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $value['amount'] = '0';
                $records[$value['fee_id']] = $value;
            endforeach;
        endif;
        return $records;
    }

    #get session payheads

    function sessionPayHeadsIdInArray($session_id) {

        $records = array();
        $Query = new query('fee_type_master,session_fee_type');
        $Query->Field = "session_fee_type.*,fee_type_master.name as title";
        $Query->Where = "where fee_type_master.id=session_fee_type.fee_head and fee_type_master.is_active='1' and session_fee_type.session_id='" . mysql_real_escape_string($session_id) . "'";
        $rec = $Query->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = $value->fee_head;
            endforeach;
        endif;
        return $records;
    }

    #get session payheads

    function sessionPayHeadsNameArray($session_id) {

        $records = array();
        $Query = new query('fee_type_master,session_fee_type');
        $Query->Field = "session_fee_type.*,fee_type_master.name as title";
        $Query->Where = "where fee_type_master.id=session_fee_type.fee_head and fee_type_master.is_active='1' and session_fee_type.session_id='" . mysql_real_escape_string($session_id) . "'";
        $rec = $Query->ListOfAllRecords('object');
        if (!empty($rec)):
            foreach ($rec as $key => $value):
                $records[] = ucfirst($value->title);
            endforeach;
        endif;
        return $records;
    }

    /*
     * delete a session_fee_type list by id
     */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    function DeleteSessionFeeHeads($session_id) {
        $Query = new query('session_fee_type');
        $Query->Where = "where session_id='" . mysql_real_escape_string($session_id) . "'";
        $Query->Delete_where();
        return true;
    }

    /*
     * Update session_fee_type list position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

}

# Session Fee Type

class session_section_staff extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('session_section_incharge');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'session_id', 'section', 'staff_id', 'on_date');
    }

    /*
     * Create new session_fee_type list or update existing theme
     */

    function saveData($POST, $session_id) {
        if (!empty($POST)):
            foreach ($POST['incharge'] as $key => $value):
                $ObjQuery = new query('session_section_incharge');
                $ObjQuery->Data['session_id'] = $session_id;
                $ObjQuery->Data['staff_id'] = $value['id'];
                $ObjQuery->Data['section'] = $value['section'];
                $ObjQuery->Data['on_date'] = date('Y-m-d');
                $ObjQuery->Insert();
            endforeach;
        endif;
        return true;
    }

    function getRecord($id) {
        return $this->_getObject('session_section_incharge', $id);
    }

    function listAllSessionSection($session_id, $limit = '0') {
        if ($limit):
            $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' order by section limit 0," . $limit;
        else:
            $this->Where = "where session_id='" . mysql_real_escape_string($session_id) . "' order by section";
        endif;
        return $this->ListOfAllRecords('object');
    }

    /* Get List of all Staff  */

    function ListOfSection($session_id) {
        $query = new query('staff,session_section_incharge');
        $query->Field = ('staff.id,staff.title,staff.first_name,staff.last_name,staff.sex,session_section_incharge.section');

        $query->Where = "where session_section_incharge.session_id='" . mysql_real_escape_string($session_id) . "' and staff.id=session_section_incharge.staff_id ORDER BY session_section_incharge.section asc";

        return $query->ListOfAllRecords('object');
    }

    function DeleteSessionSessionRecord($session_id) {
        $Query = new query('session_section_incharge');
        $Query->Where = "where session_id='" . mysql_real_escape_string($session_id) . "'";
        $Query->Delete_where();
        return true;
    }

    /*
     * Update session_fee_type list position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

}

?>