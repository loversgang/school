<?php

class school extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('school');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'client_id', 'school_name', 'urlname', 'address1', 'address2', 'city', 'district', 'state', 'country', 'website_url', 'phone1_contact_name', 'phone1', 'phone2_contact_name', 'phone2', 'fax', 'school_head_title', 'school_head_name', 'school_head_phone', 'school_head_email', 'email_address', 'school_phone', 'username', 'password', 'logo', 'create_date', 'update_date', 'position', 'ip_address', 'affiliation_code', 'affiliation_board', 'established_year', 'school_type', 'is_deleted', 'is_active', 'is_website', 'theme', 'school_time', 'facebook', 'twitter', 'google');
    }

    /*
     * Create new school or update existing school
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active'] = isset($this->Data['is_active']) ? '1' : '0';

        $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];

        if (!empty($this->Data['password'])):
            $this->Data['password'] = encrypt_decrypt('encrypt', $this->Data['password']);
        endif;


        if ($_FILES['image']):
            $rand = rand(0, 99999999);
            $image_obj = new imageManipulation();

            if ($image_obj->upload_photo('school', $_FILES['image'], $rand)):
                $this->Data['logo'] = $image_obj->makeFileName($_FILES['image']['name'], $rand);
            endif;
        endif;


        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            if ($this->Update()) {
                $max_id = $this->Data['id'];
            }
        } else {
            $this->Data['create_date'] = date('Y-m-d');
            $this->Insert();
            $max_id = $this->GetMaxId();
        }

        $quryObj = new school();
        $object = $quryObj->getSchool($max_id);
        if ($object) {
            $Query = new query('school');
            $Query->Data['id'] = $max_id;
            $Query->Data['urlname'] = $Query->_sanitize($object->school_name . '-' . $max_id);
            $Query->Update();
        }
        return $max_id;
    }

    #check User Name

    function check_user_name_exists($username) {
        $this->Where = "where username='" . mysql_real_escape_string($username) . "'";
        $user = $this->DisplayOne();
        if ($user && is_object($user)):

            return $user;
        else:
            return false;
        endif;
    }

    /*
     * Get school by id
     */

    function getSchool($id) {
        return $this->_getObject('school', $id);
    }

    #get single school with plan

    function getSchoolWithPlan($id) {
        $QueryObj = new query('school, school_subscription');
        $QueryObj->Field = 'school.*,school_subscription.subscription_id as subscription_id,school_subscription.id as subscription_record_id,school_subscription.is_active as subscription_active,school_subscription.set_up_fee as set_up_fee,school_subscription.subscription_fee as subscription_fee,school_subscription.payment_type as payment_type';
        $QueryObj->Where = "where school.id='" . mysql_real_escape_string($id) . "' and school.id=school_subscription.school_id  ORDER BY school.id desc";

        return $record = $QueryObj->DisplayOne();
    }

    #get single school with plan

    function list_for_pending_fee() {

        $record = array();
        $query1 = "SELECT id,client_id,address1,school_name FROM `school` WHERE is_active='1' and is_deleted='0'";


        $query2 = "Select * from (SELECT school_id,id,payment_date,next_payment_date  FROM `payment_history`  ORDER BY id desc) as q1 GROUP BY school_id";

        $new_query = "SELECT * FROM (" . $query1 . ") as t1 left join (" . $query2 . ") as t2 ON t1.id = t2.school_id where t2.next_payment_date IS NULL OR  t2.next_payment_date<'" . date('Y-m-d') . "'";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($new_query);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):
                $record[] = $object;
            endwhile;

        endif;
        return $record;
    }

    #get single school with plan

    function list_for_pending_fee_with_afilate($list) {
        if (empty($list)):
            $list = '0';
        endif;
        $record = array();
        $query1 = "SELECT id,client_id,address1,school_name FROM `school` WHERE is_active='1' and is_deleted='0' and id IN(" . mysql_real_escape_string($list) . ")";


        $query2 = "Select * from (SELECT school_id,id,payment_date,next_payment_date FROM `payment_history`  ORDER BY id desc) as q1 GROUP BY school_id";

        $new_query = "SELECT * FROM (" . $query1 . ") as t1 left join (" . $query2 . ") as t2 ON t1.id = t2.school_id where t2.next_payment_date IS NULL OR  t2.next_payment_date<'" . date('Y-m-d') . "'";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($new_query);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):
                $record[] = $object;
            endwhile;

        endif;
        return $record;
    }

    function list_for_terminated() {
        $this->Field = 'id,client_id,school_name,address1';
        $this->Where = "where is_active='0' OR is_deleted='1' ORDER BY id asc";

        return $this->ListOfAllRecords('object');
    }

    function list_for_terminated_with_afilate($list) {
        $this->Field = 'id,client_id,school_name,address1';
        $this->Where = "where is_active='0' OR is_deleted='1' and id IN(" . mysql_real_escape_string($list) . ") ORDER BY id asc";

        return $this->ListOfAllRecords('object');
    }

    #get get_last_unique_id

    function get_last_unique_id() {
        $this->Field = 'max(client_id) as max';
        $record = $this->DisplayOne();
        if ($record):
            return ($record->max + 1);
        else:
            return '100001';
        endif;
    }

    #get all active school with plan

    function getAllSchoolWithPlan($id) {
        $QueryObj = new query('school, school_subscription');
        $QueryObj->Field = 'school.*,school_subscription.subscription_id as subscription_id,school_subscription.id as subscription_record_id,school_subscription.is_active as subscription_active,school_subscription.set_up_fee as set_up_fee,school_subscription.subscription_fee as subscription_fee,school_subscription.payment_type as payment_type';
        $QueryObj->Where = "where school.is_active='1' and school.is_deleted='0' and school.id=school_subscription.school_id  ORDER BY school.id desc";
        return $record = $QueryObj->DisplayOne();
    }

    #get all active school with plan

    function getAllSchoolDetail($id) {
        $QueryObj = new query('school, school_subscription,school_type');
        $QueryObj->Field = 'school.id,school.client_id,school.school_name,school.school_name,school.address1,
                    school.address2,school.city,school.state,school.country,school.district,school.website_url,school.school_phone,
                    school.phone1,school.phone2,school.school_head_name,school.school_head_email,school.email_address,school.school_time,school.facebook,school.twitter,school.google,
                    school.logo,school.affiliation_code,school.affiliation_board,school.established_year,school_type.name as school_type,
                   school_subscription.set_up_fee as set_up_fee,school_subscription.subscription_fee as subscription_fee,school_subscription.payment_type as payment_type';
        $QueryObj->Where = "where school.is_active='1' and school.is_deleted='0' and school.id='" . mysql_real_escape_string($id) . "' and school.id=school_subscription.school_id and school_type.id=school.school_type ORDER BY school.id desc";
        return $record = $QueryObj->DisplayOne();
    }

    #getschool Theme Name

    function getSchoolTheme($id) {
        $QueryObj = new query('school, theme');
        $QueryObj->Field = 'theme.folder_name';
        $QueryObj->Where = "where school.is_active='1' and school.is_deleted='0' and school.is_website='1' and school.theme=theme.id  ORDER BY school.id desc";

        $rec = $QueryObj->DisplayOne();
        if ($rec):
            return $rec->folder_name;
        else:
            return false;
        endif;
    }

    /** Get List of all school in array */
    function listAll($show_active = 0, $result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active) {
            $this->Where = "where is_deleted='0' and is_active='1'  ORDER BY position asc";
        } else {
            $this->Where = "where is_deleted='0' ORDER BY position asc";
        }
        if ($result_type == 'object') {
            return $this->DisplayAll();
        } else {
            return $this->ListOfAllRecords('object');
        }
    }

    #get single school with plan

    function getSchoolWithPlanForWebgarh() {
        $query1 = "SELECT school.id,school.school_name,school.address1,school.address2,school.create_date,school.is_active FROM school where is_deleted=0";

        $query2 = "SELECT school_subscription.school_id, school_subscription.subscription_id as subscription_id,school_subscription.id as subscription_record_id,school_subscription.is_active as subscription_active,school_subscription.set_up_fee as set_up_fee,school_subscription.subscription_fee as subscription_fee,school_subscription.payment_type as payment_type FROM school_subscription where id!=0";

        $query3 = "SELECT student.school_id as active_school_id,count(*) as active_student FROM student where is_active=1 group by school_id";

        $query4 = "SELECT student.school_id as in_active_school_id,count(*) as in_active_student FROM student where is_active=0 group by school_id";

        $query5 = "SELECT * FROM (" . $query1 . ") as t1 left join (" . $query2 . ") as t2 ON t1.id = t2.school_id";

        $query6 = "SELECT * FROM (" . $query5 . ") as t3 left join (" . $query3 . ") as t4 ON t3.id = t4.active_school_id";

        $final_query = "SELECT * FROM (" . $query6 . ") as t5 left join (" . $query4 . ") as t6 ON t5.id = t6.in_active_school_id";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($final_query);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):
                $record[] = $object;
            endwhile;

        endif;
        return $record;
    }

    function listAllWithAfilate($list = 0, $show_active = 0, $result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active):
            if ($list):
                $this->Where = "where is_deleted='0' and is_active='1' and id IN(" . mysql_real_escape_string($list) . ")  ORDER BY position asc";
            else:
                $this->Where = "where is_deleted='0' and is_active='1'  ORDER BY position asc";
            endif;
        else:
            if ($list):
                $this->Where = "where is_deleted='0' and id IN(" . mysql_real_escape_string($list) . ")  ORDER BY position asc";
            else:
                $this->Where = "where is_deleted='0' ORDER BY position asc";
            endif;
        endif;
        if ($result_type == 'object') {
            return $this->DisplayAll();
        } else {
            return $this->ListOfAllRecords('object');
        }
    }

    function getAllMonths($date1, $date2) {
        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        $my = date('mY', $time2);

        $months = array(date('F Y', $time1));

        while ($time1 < $time2) {
            $time1 = strtotime(date('Y-m-d', $time1) . ' +1 days');

            $months[] = date('F Y', $time1);
        }

        $months[] = date('F Y', $time2);
        $months = array_unique($months);
        return count($months);
    }

    #total payments

    function getSchoolNoPayment($date1, $date2, $type = '1') {

        #get total months
        $total_month = $this->getAllMonths($date1, $date2);

        $noOfPayment = ceil($total_month / $type);

        return $noOfPayment;
    }

    function getSchoolCurrentPaymentDate($date1, $date2, $type = '1') {

        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        $my = date('mY', $time2);

        $months = array(date('Y-m-d', $time1));
        while ($time1 < $time2) {
            $time_first = $time1;
            $time1 = strtotime(date('Y-m-d', $time1) . '+' . $type . ' months');
            $months[date('Y-m-d', $time_first)] = date('Y-m-d', $time1);
        }

        $months = array_unique($months);
        if (count($months)):
            foreach ($months as $m_key => $m_value):
                if (strtotime(date('Y-m-d')) >= strtotime($m_value . '-15 days') && strtotime(date('Y-m-d')) <= strtotime($m_value)):
                    return $m_value;
                    break;
                endif;
            endforeach;
        endif;

        return false;
    }

    function getThrash($result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);

        $this->Where = "where is_deleted='1' ORDER BY position asc";
        if ($result_type == 'object') {
            return $this->DisplayAll();
        } else {
            return $this->ListOfAllRecords('object');
        }
    }

    function getThrashWithAfilate($list = 0, $result_type = 'object') {
        if ($list):
            $this->Where = "where is_deleted='1' and id IN(" . mysql_real_escape_string($list) . ")  ORDER BY position asc";
        else:
            $this->Where = "where is_deleted='1' ORDER BY position asc";
        endif;


        if ($result_type == 'object') {
            return $this->DisplayAll();
        } else {
            return $this->ListOfAllRecords('object');
        }
    }

    /*
     * delete a school by id
     */

    function deleteRecord($id) {
        $this->id = mysql_real_escape_string($id);
        if (SOFT_DELETE) {
            return $this->SoftDelete();
        } else {
            return $this->Delete();
        }
    }

    function deleteImage($id) {
        $this->Data['logo'] = '';
        $this->Data['id'] = $id;

        return $this->Update();
    }

    /*
     * Update school position
     */

    function updatePosition($position, $id) {
        $this->Data['id'] = $id;
        $this->Data['position'] = ($position != '') ? $position : 0;
        $this->Update();
    }

    function FullReportDownload($CatExpRecord, $SalRecord, $CatIncRecord, $FeeRecord, $from, $to) {
        $total = '0';
        $exptotal = '0';
        $inctotal = '0';
        $filename = "FullReport.csv";

        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=$filename");

        echo "From Date" . "\t" . $from . "\n";
        echo "To Date " . "\t" . $to . "\n \n";
        echo 'Expenses' . "\t" . '' . "\t" . '' . "\t" . 'Incomes' . "\t" . '' . "\t" . '' . "\t\n";
        if (count($CatExpRecord) >= count($CatIncRecord)):
            $start = $CatExpRecord;
            $last = $CatIncRecord;
            foreach ($start as $key => $value):
                echo $value->category_name . "\t" . number_format($value->amount, 2) . "\t" . '' . "\t";
                $exptotal = $exptotal + $value->amount;
                if (array_key_exists($key, $last)):
                    echo $last[$key]->category_name . "\t" . number_format($last[$key]->amount, 2) . "\t\n";
                    $inctotal = $inctotal + $last[$key]->amount;
                else:
                    echo "\n";
                endif;
            endforeach;

        else:
            foreach ($CatIncRecord as $key => $value):
                if (array_key_exists($key, $CatExpRecord)):
                    echo $CatExpRecord[$key]->category_name . "\t" . number_format($CatExpRecord[$key]->amount, 2) . "\t";
                    $exptotal = $exptotal + $CatExpRecord[$key]->amount;
                else:
                    echo "\t" . '' . "\t";
                    ;
                endif;
                echo $value->category_name . "\t" . number_format($value->amount, 2) . "\t\n";
                $inctotal = $inctotal + $value->amount;
            endforeach;
        endif;



        echo '' . "\t" . number_format($exptotal, 2) . "\t" . '' . "\t" . '' . "\t" . number_format($inctotal, 2) . "\t" . '' . "\t\n";
        echo 'Salaries' . "\t" . number_format($SalRecord['0']->amount, 2) . "\t" . '' . "\t" . 'Student Fee' . "\t" . number_format($FeeRecord['0']->amount, 2) . "\t" . '' . "\t\n\n";

        $exptotal = $exptotal + $SalRecord['0']->amount;
        $inctotal = $inctotal + $FeeRecord['0']->amount;

        echo 'Total' . "\t" . number_format($exptotal, 2) . "\t" . '' . "\t" . 'Total' . "\t" . number_format($inctotal, 2) . "\t" . '' . "\t\n";
        echo '' . "\t" . '' . "\t" . '' . "\t" . 'Net Income' . "\t" . number_format($inctotal - $exptotal, 2) . "\t" . '' . "\t\n";
        exit;
    }

    /* Get All current session student */

    function GetActiveSchoolSessionStudents($id = 0) {
        $QueryObj = new query('school,session,session_students_rel,student');
        $QueryObj->Field = ('school.id as school_id,session.id as session_id,session.title,session.start_date,session.end_date,session_students_rel.student_id,student.first_name,student.last_name,student.date_of_birth,student.email,student.phone');
        if ($id):
            $QueryObj->Where = "where school.is_active='1' AND school.id='" . $id . "' AND session.school_id=school.id AND DATE(session.start_date ) <= CAST('" . date('Y-m-d') . "' AS DATE )  AND DATE(session.end_date ) >= CAST('" . date('Y-m-d') . "' AS DATE ) AND session_students_rel.session_id=session.id AND student.id=session_students_rel.student_id GROUP BY session_students_rel.student_id";
        else:
            $QueryObj->Where = "where school.is_active='1' AND session.school_id=school.id AND DATE(session.start_date ) <= CAST('" . date('Y-m-d') . "' AS DATE )  AND DATE(session.end_date ) >= CAST('" . date('Y-m-d') . "' AS DATE ) AND session_students_rel.session_id=session.id AND student.id=session_students_rel.student_id GROUP BY session_students_rel.student_id";
        endif;
        return $QueryObj->ListOfAllRecords('object');
    }

    /* Get All current session student where birthday on tomarrow */

    function GetActiveSchoolSessionStudentsForBirthday() {
        $QueryObj = new query('school,session,session_students_rel,student');
        $QueryObj->Field = ('school.id as school_id,session.id as session_id,session.title,session.start_date,session.end_date,session_students_rel.student_id,student.first_name,student.last_name,student.date_of_birth,student.email,student.phone');
        $QueryObj->Where = "where school.is_active='1' AND session.school_id=school.id AND DATE(session.start_date ) <= CAST('" . date('Y-m-d') . "' AS DATE )  AND DATE(session.end_date ) >= CAST('" . date('Y-m-d') . "' AS DATE ) AND session_students_rel.session_id=session.id AND student.id=session_students_rel.student_id AND MONTH(student.date_of_birth)='" . date('m') . "' AND DAY(student.date_of_birth)='" . date('d') . "' AND student.quit='0' GROUP BY session_students_rel.student_id";
        return $QueryObj->ListOfAllRecords('object');
    }

    function deleteCompleteSchool($school_id) {
        if (!empty($school_id)):
            #delete All content pages for website And Website Folder
            $QueryObj = new query('content');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();
            if (is_dir(DIR_FS_SITE_PUBLIC . 'site/' . $school_id)):
                unlink(DIR_FS_SITE_PUBLIC . '/site/' . $school_id . '/index.php');
                unlink(DIR_FS_SITE_PUBLIC . 'site/' . $school_id . '/.htaccess');
                unlink(DIR_FS_SITE_PUBLIC . 'site/' . $school_id . '/error_log');
                @rmdir(DIR_FS_SITE_PUBLIC . 'site/' . $school_id);
            endif;

            #delete All fee_type_master
            $QueryObj = new query('fee_type_master');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All Enquires
            $QueryObj = new query('enquiry');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All notice_board
            $QueryObj = new query('notice_board');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All payment_history for this school
            $QueryObj = new query('payment_history');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_course
            $QueryObj = new query('school_course');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_document_print_history
            $QueryObj = new query('school_document_print_history');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_document_template
            $QueryObj = new query('school_document_template');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_email
            $QueryObj = new query('school_email');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_email_counter
            $QueryObj = new query('school_email_counter');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_expense
            $QueryObj = new query('school_expense');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_expense_cat
            $QueryObj = new query('school_expense_cat');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_holiday
            $QueryObj = new query('school_holiday');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_income
            $QueryObj = new query('school_income');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_income_cat
            $QueryObj = new query('school_income_cat');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_setting
            $QueryObj = new query('school_setting');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_sms
            $QueryObj = new query('school_sms');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_sms_counter
            $QueryObj = new query('school_sms_counter');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_sms_email_history
            $QueryObj = new query('school_sms_email_history');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_sms_sent_count
            $QueryObj = new query('school_sms_sent_count');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All school_subscription
            $QueryObj = new query('school_subscription');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All staff_category_master
            $QueryObj = new query('staff_category_master');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All staff_designation_master
            $QueryObj = new query('staff_designation_master');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All subject_master
            $QueryObj = new query('subject_master');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All user
            $QueryObj = new query('user');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All student_examination_group
            $QueryObj = new query('student_examination_group');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #delete All exam_grade
            $QueryObj = new query('exam_grade');
            $QueryObj->Where = "where school_id='" . $school_id . "'";
            $QueryObj->Delete_where();

            #Now Get All Session By School_id
            $QObj = new query('session');
            $QObj->Where = "where school_id='" . $school_id . "'";
            $sess_rec = $QObj->ListOfAllRecords('object');

            if (!empty($sess_rec)):
                foreach ($sess_rec as $key => $val):

                    #Now delete this session_fee_type
                    $QueryObj = new query('session_fee_type');
                    $QueryObj->Where = "where session_id='" . $val->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this session_interval
                    $QueryObj = new query('session_interval');
                    $QueryObj->Where = "where session_id='" . $val->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this session_section_incharge
                    $QueryObj = new query('session_section_incharge');
                    $QueryObj->Where = "where session_id='" . $val->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this session_students_rel
                    $QueryObj = new query('session_students_rel');
                    $QueryObj->Where = "where session_id='" . $val->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this session_student_fee
                    $QueryObj = new query('session_student_fee');
                    $QueryObj->Where = "where session_id='" . $val->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this student_session_interval
                    $QueryObj = new query('student_session_interval');
                    $QueryObj->Where = "where session_id='" . $val->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this session
                    $QueryObj = new query('session');
                    $QueryObj->Where = "where id='" . $val->id . "'";
                    $QueryObj->Delete_where();
                endforeach;
            endif;


            #Now Get All staff By School_id
            $QObj1 = new query('staff');
            $QObj1->Where = "where school_id='" . $school_id . "'";
            $staff_rec = $QObj1->ListOfAllRecords('object');

            if (!empty($staff_rec)):
                foreach ($staff_rec as $key => $val):

                    #Now delete this staff_attendance
                    $QueryObj = new query('staff_attendance');
                    $QueryObj->Where = "where staff_id='" . $val->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this staff_experience
                    $QueryObj = new query('staff_experience');
                    $QueryObj->Where = "where staff_id='" . $val->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this staff_qualification
                    $QueryObj = new query('staff_qualification');
                    $QueryObj->Where = "where staff_id='" . $val->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this staff_salary
                    $QueryObj = new query('staff_salary');
                    $QueryObj->Where = "where staff_id='" . $val->id . "'";
                    $QueryObj->Delete_where();

                    #Now Get All staff_document By School_id
                    $QObj11 = new query('staff_document');
                    $QObj11->Where = "where staff_id='" . $val->id . "'";
                    $doc_rec = $QObj11->ListOfAllRecords('object');

                    if (!empty($doc_rec)):
                        foreach ($staff_rec as $key1 => $val1):
                            if (is_dir(DIR_FS_SITE_PUBLIC . 'upload/file/document/' . $val1->file)):
                                unlink(DIR_FS_SITE_PUBLIC . 'upload/file/document/' . $val1->file);
                            endif;

                            #Now delete this staff_document
                            $QueryObj = new query('staff_document');
                            $QueryObj->Where = "where id='" . $val1->id . "'";
                            $QueryObj->Delete_where();
                        endforeach;
                    endif;

                    #Now delete this staff
                    $QueryObj = new query('staff');
                    $QueryObj->Where = "where id='" . $val->id . "'";
                    $QueryObj->Delete_where();
                endforeach;
            endif;


            #Now Get All Vehicle By School_id
            $QObj12 = new query('school_vehicle');
            $QObj12->Where = "where school_id='" . $school_id . "'";
            $veh_rec = $QObj12->ListOfAllRecords('object');

            if (!empty($veh_rec)):
                foreach ($veh_rec as $key12 => $val12):

                    #Now delete this school_vehicle_student
                    $QueryObj = new query('school_vehicle_student');
                    $QueryObj->Where = "where vehicle_id	='" . $val12->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this school_vehicle
                    $QueryObj = new query('school_vehicle');
                    $QueryObj->Where = "where id='" . $val12->id . "'";
                    $QueryObj->Delete_where();
                endforeach;
            endif;


            #Now Get All Exam By School_id
            $QObj123 = new query('student_examination');
            $QObj123->Where = "where school_id='" . $school_id . "'";
            $exam_rec = $QObj123->ListOfAllRecords('object');

            if (!empty($exam_rec)):
                foreach ($exam_rec as $key123 => $val12):

                    #Now delete this students_examination_rel
                    $QueryObj = new query('students_examination_rel');
                    $QueryObj->Where = "where exam_id	='" . $val12->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this student_examination
                    $QueryObj = new query('student_examination');
                    $QueryObj->Where = "where id='" . $val12->id . "'";
                    $QueryObj->Delete_where();
                endforeach;
            endif;

            #Now Get All Gallery By School_id
            $QObj1234 = new query('gallery');
            $QObj1234->Where = "where school_id='" . $school_id . "'";
            $gal_rec = $QObj1234->ListOfAllRecords('object');

            if (!empty($gal_rec)):
                foreach ($gal_rec as $key1234 => $val1234):
                    #delete all images from folders
                    $im_obj = new imageManipulation();
                    $im_obj->DeleteImagesFromAllFolders('gallery', $val1234->image);

                    #Now delete this gallery
                    $QueryObj = new query('gallery');
                    $QueryObj->Where = "where id='" . $val1234->id . "'";
                    $QueryObj->Delete_where();
                endforeach;
            endif;


            #Now Get All Student By School_id
            $QObjStu = new query('student');
            $QObjStu->Where = "where school_id='" . $school_id . "'";
            $stu_rec = $QObjStu->ListOfAllRecords('object');

            if (!empty($stu_rec)):
                foreach ($stu_rec as $key12345 => $val12345):

                    #Now delete this student_address
                    $QueryObj = new query('student_address');
                    $QueryObj->Where = "where student_id='" . $val12345->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this student_attendance
                    $QueryObj = new query('student_attendance');
                    $QueryObj->Where = "where student_id='" . $val12345->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this student_previous_school
                    $QueryObj = new query('student_previous_school');
                    $QueryObj->Where = "where student_id='" . $val12345->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this student_sibling_stuty
                    $QueryObj = new query('student_sibling_stuty');
                    $QueryObj->Where = "where student_id='" . $val12345->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this student_subject
                    $QueryObj = new query('student_subject');
                    $QueryObj->Where = "where student_id='" . $val12345->id . "'";
                    $QueryObj->Delete_where();

                    #Now delete this student_fee
                    $QueryObj = new query('student_fee');
                    $QueryObj->Where = "where student_id='" . $val12345->id . "'";
                    $QueryObj->Delete_where();

                    #Now Get All student_fee By School_id
                    $QObjStuFee = new query('student_fee');
                    $QObjStuFee->Where = "where student_id='" . $val12345->id . "'";
                    $stuFee_rec = $QObjStuFee->ListOfAllRecords('object');
                    if (!empty($stuFee_rec)):
                        foreach ($stuFee_rec as $key123456 => $val123456):

                            #Now delete this student_fee_pay_heads
                            $QueryObj = new query('student_fee_pay_heads');
                            $QueryObj->Where = "where student_fee_id='" . $val123456->id . "'";
                            $QueryObj->Delete_where();

                            #Now delete this student_fee_record
                            $QueryObj = new query('student_fee_record');
                            $QueryObj->Where = "where student_fee_id='" . $val123456->id . "'";
                            $QueryObj->Delete_where();

                            #Now delete this student_fee
                            $QueryObj = new query('student_fee');
                            $QueryObj->Where = "where id='" . $val123456->id . "'";
                            $QueryObj->Delete_where();
                        endforeach;
                    endif;

                    #Now Get All student_family_details By School_id
                    $QObj123467 = new query('student_family_details');
                    $QObj123467->Where = "where student_id='" . $val12345->id . "'";
                    $fam_rec = $QObj123467->ListOfAllRecords('object');

                    if (!empty($fam_rec)):
                        foreach ($fam_rec as $key123467 => $val1234567):
                            #delete all images from folders
                            $im_obj = new imageManipulation();
                            $im_obj->DeleteImagesFromAllFolders('student_family', $val1234567->family_photo);

                            #Now delete this student_family_details
                            $QueryObj = new query('student_family_details');
                            $QueryObj->Where = "where id='" . $val1234567->id . "'";
                            $QueryObj->Delete_where();
                        endforeach;
                    endif;

                    #Now Get All student_document By School_id
                    $QObjDoc = new query('student_document');
                    $QObjDoc->Where = "where student_id='" . $val12345->id . "'";
                    $doc_rec1 = $QObjDoc->ListOfAllRecords('object');

                    if (!empty($doc_rec1)):
                        foreach ($doc_rec1 as $key1S => $val1S):
                            if (is_dir(DIR_FS_SITE_PUBLIC . 'upload/file/student_document/' . $val1S->file)):
                                unlink(DIR_FS_SITE_PUBLIC . 'upload/file/student_document/' . $val1S->file);
                            endif;

                            #Now delete this student_document
                            $QueryObj = new query('student_document');
                            $QueryObj->Where = "where id='" . $val1S->id . "'";
                            $QueryObj->Delete_where();
                        endforeach;
                    endif;


                    #delete all images from folders
                    $im_obj = new imageManipulation();
                    $im_obj->DeleteImagesFromAllFolders('student', $val12345->photo);

                    #Now delete this student
                    $QueryObj = new query('student');
                    $QueryObj->Where = "where id='" . $val12345->id . "'";
                    $QueryObj->Delete_where();
                endforeach;
            endif;

            $school = get_object('school', $school_id);

            #delete all images from folders
            $im_obj = new imageManipulation();
            $im_obj->DeleteImagesFromAllFolders('school', $school->logo);

            #Now delete this school
            $QueryObj = new query('school');
            $QueryObj->Where = "where id='" . $school_id . "'";
            $QueryObj->Delete_where();

        #end Function	
        endif;
    }

}

#School Types

class schoolType extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_type');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'name', 'remarks', 'on_date', 'update_date');
    }

    /*
     * Create Student Fee  list or update existing theme
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update())
                return $this->Data['id'];
        }
        else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getRecord($id) {
        return $this->_getObject('school_type', $id);
    }

    /*
     * Get List of all subject list in array
     */

    function listAll($show_active = 0, $result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);
        if ($show_active)
            $this->Where = "where is_deleted='0' and is_active='1'  ORDER BY position asc";
        else
            $this->Where = "where is_deleted='0' ORDER BY position asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /*
     * delete a Student Fee list by id
     */

    function getThrash($result_type = 'object') {
        //$this->enablePaging($allowPaging, $pageNo, $pageSize);

        $this->Where = "where is_deleted='1' ORDER BY position asc";
        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /*
     * delete a school by id
     */

    function deleteRecord($id) {
        $this->id = mysql_real_escape_string($id);
        if (SOFT_DELETE)
            return $this->SoftDelete();
        else
            return $this->Delete();
    }

}

class schoolHolidays extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'desc', $orderby = 'id') {
        parent::__construct('school_holiday');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'title', 'type', 'holiday_date', 'ip_address', 'remarks', 'alert', 'on_date', 'update_date');
    }

    /*
     * Create new schoolExpense
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->Data['alert'] = isset($this->Data['alert']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            $this->Update();
            return $this->Data['id'];
        } else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /* Get schoolExpense by id */

    function getRecord($id) {
        return $this->_getObject('school_holiday', $id);
    }

    /* Get List of all schoolExpense in array */

    function listAll($school_id, $show_active = 0, $result_type = 'object') {
        if ($show_active) {
            $this->Where = "where school_id='" . $school_id . "' ORDER BY id desc";
        } else {
            $this->Where = "where school_id='" . $school_id . "' ORDER BY id desc";
        }

        if ($result_type == 'object') {
            return $this->DisplayAll();
        } else {
            return $this->ListOfAllRecords('object');
        }
    }

    /* delete a schoolExpense by id */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    function checkMonthlyHoliday($school, $month, $year) {
        #Check the attendance on this date
        $this->Field = ('count(*) as count');
        $this->Where = "where school_id='" . mysql_real_escape_string($school) . "' and month(holiday_date)='" . date(mysql_real_escape_string($month)) . "' and YEAR(holiday_date)='" . date(mysql_real_escape_string($year)) . "' group by month(holiday_date)";
        $rec = $this->DisplayOne();
        if (is_object($rec)):
            return $rec->count;
        else:
            return '0';
        endif;
    }

    function checkHoliday($school, $date) {
        #Check the attendance on this date
        $this->Where = "where school_id='" . mysql_real_escape_string($school) . "' and holiday_date='" . mysql_real_escape_string($date) . "'";
        $rec = $this->DisplayOne();
        if (is_object($rec)):
            return "Holiday";
        else:
            return false;
        endif;
    }

}

class schoolEnquiry extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'desc', $orderby = 'id') {
        parent::__construct('enquiry');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'name', 'father_name', 'course', 'phone', 'email', 'detail', 'on_date', 'address', 'city', 'state', 'country', 'post_code', 'status', 'ref_name', 'reference_detail', 'msg_send', 'msg_date');
    }

    /*
     * Create new schoolEnquiry
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Update();
            return $this->Data['id'];
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /* Get schoolEnquiry by id */

    function getRecord($id) {
        return $this->_getObject('enquiry', $id);
    }

    /* Get List of all enquiry in array */

    function listAll($school_id, $show_active = 0, $result_type = 'object') {
        if ($show_active)
            $this->Where = "where school_id='" . $school_id . "' ORDER BY id desc";
        else
            $this->Where = "where school_id='" . $school_id . "' ORDER BY id desc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* Get List of all enquiry in array */

    function listAllActiveConfirm($school_id, $result_type = 'object') {
        $this->Where = "where school_id='" . $school_id . "' AND status!='dead' ORDER BY id desc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* Get List of all enquiry in array */

    function listAllType($school_id, $type, $result_type = 'object') {
        $this->Where = "where school_id='" . $school_id . "' AND status='" . $type . "' ORDER BY id desc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* Get List of all enquiry in array */

    function listAllDead($school_id, $result_type = 'object') {
        $this->Where = "where school_id='" . $school_id . "' AND status='dead' ORDER BY id desc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* Get List of all live enquiry in array */

    function listAllLive($school_id, $result_type = 'object') {
        $this->Where = "where school_id='" . $school_id . "' AND status='live' ORDER BY id desc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* Get List of all confirm enquiry in array */

    function listAllConfirm($school_id, $result_type = 'object') {
        $this->Where = "where school_id='" . $school_id . "' AND status='confirm' ORDER BY id desc";

        if ($result_type == 'object')
            return $this->DisplayAll();
        else
            return $this->ListOfAllRecords('object');
    }

    /* delete a enquiry by id */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

?>