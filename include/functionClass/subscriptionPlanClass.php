<?php

class subscriptionPlan extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /*
     *
     */
    function __construct($order='asc', $orderby='position'){
        parent::__construct('subscription_plan');
		$this->orderby=$orderby;
        $this->order=$order;
        $this->requiredVars=array('id', 'name', 'set_up_fee','subscription_fee', 'creation_date', 'update_date','is_deleted', 'is_active','type');

	}

    /*
     * Create new subscriptionPlan or update existing theme
     */
    function saveData($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';

        if(isset($this->Data['id']) && $this->Data['id']!=''){
            $this->Data['update_date']=date('Y-m-d');
            if($this->Update())
              return $this->Data['id'];
        }
        else{  
            $this->Data['creation_date']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /*
     * Get subscriptionPlan by id
     */
    function getSubscriptionPlan($id){
        return $this->_getObject('subscription_plan', $id);
    }

    
    /*
     * Get List of all subscriptionPlan in array
     */
    function listAll($show_active=0, $result_type='object'){
		//$this->enablePaging($allowPaging, $pageNo, $pageSize);
		if($show_active)
			$this->Where="where is_deleted='0' and is_active='1'  ORDER BY position asc";
		else
			$this->Where="where is_deleted='0' ORDER BY position asc";
		if($result_type=='object')
			return $this->DisplayAll();
		else
			return $this->ListOfAllRecords('object');
    }

  

    /*
     * delete a subscriptionPlan by id
     */
    function deleteRecord($id){
        $this->id=$id;
            return $this->Delete();
    }

    /*
     * Update subscriptionPlan position
     */
    function updatePosition($position, $id){
        $this->Data['id']=$id;
        $this->Data['position']=($position!='')?$position:0;
        $this->Update();
    }

    

   
}
?>