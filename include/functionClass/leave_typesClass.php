<?php

class leave_types extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'position') {
        parent::__construct('leave_types');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'type', 'status');
    }

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['status'] = isset($this->Data['status']) ? '1' : '0';
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function lists($id = 0) {
        if ($id > 0) {
            $this->Where = "WHERE `id` = '$id'";
            return $this->DisplayOne();
        } else {
            $this->Where = "ORDER BY `id` DESC";
            return $this->ListOfAllRecords();
        }
    }

    function deleteType($id) {
        $this->id = $id;
        $this->Delete();
    }

}

?>