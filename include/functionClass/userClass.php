<?php

class user extends cwebc {
    
    function __construct() {
        parent::__construct('user');
        $this->requiredVars=array('id','school_id','firstname','lastname','username','password','ip_address','last_visit','total_visit','is_active','is_deleted','user_type','address1','address2','phone','email','on_date','user_type');

        }   
        
      /*
     * Get List of all users in array
     */
    function listUsers($school_id,$is_active=0){
		if($is_active):
			$this->Where="where school_id='".$school_id."' and is_active='1'";
		else:
			$this->Where="where school_id='".$school_id."'";
		endif;	
        $this->DisplayAll();        
    }   

     /*Create new User */
    function saveUser($POST){
        $this->Data=$this->_makeData($POST, $this->requiredVars);
        $this->Data['is_active']=isset($this->Data['is_active'])?'1':'0';
        if(!empty($this->Data['password'])):
                $this->Data['password']=encrypt_decrypt('encrypt',$this->Data['password']);
        endif;         
		if(isset($this->Data['id']) && $this->Data['id']!=''){
            if($this->Update())
              return $this->Data['id'];
        }
        else{
            $this->Data['on_date']=date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }
        
  
    
    function getUser($id){
        return $this->_getObject('user', $id);
    }
    
     
	#check User Name
   function check_user_name_exists($username){
        $this->Where="where username='".mysql_real_escape_string($username)."'";
        $user=$this->DisplayOne(); 
        if($user && is_object($user)): 
           
             return $user; 
        else: 
             return false;
        endif;
   } 
   
 	#check User Name
   function check_user_name_exists_But_Not_this($username,$id){
        $this->Where="where username='".mysql_real_escape_string($username)."' and id!='".$id."'";
        $user=$this->DisplayOne(); 
        if($user && is_object($user)): 
           
             return $user; 
        else: 
             return false;
        endif;
   }   
   

    /*
     * check admin user with name in update case
     */
    function checkUsersWithID($username,$password){
        $this->Where="where is_active='1' and username='".mysql_real_escape_string($username)."' and password='".mysql_real_escape_string($password)."'";  
        return $rec=$this->DisplayOne();        
    }

    /* delete a Staff by id   */
    function deleteRecord($id){
        $this->id=$id;
            return $this->Delete();
    }
    
  }
    