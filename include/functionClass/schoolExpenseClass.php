<?php

class schoolExpense extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'desc', $orderby = 'id') {
        parent::__construct('school_expense');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'title', 'category', 'amount', 'ip_address', 'remarks', 'on_date', 'update_date');
    }

    /*
     * Create new schoolExpense
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $this->Data['amount'] = str_replace(',', '', $POST['amount']);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            $this->Update();
            return $this->Data['id'];
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /* Get schoolExpense by id */

    function getRecord($id) {
        $query = new query('school_expense,school_expense_cat');
        $query->Field = ('school_expense.*,school_expense_cat.id as category');
        $query->Where = "where school_expense.id='" . mysql_real_escape_string($id) . "' and school_expense.category=school_expense_cat.id ORDER BY school_expense.on_date desc";
        return $query->DisplayOne();
    }

    /* Get lsit of exp */

    function ExpenseReport($school_id, $from, $to, $cat = 0) {
        $query = new query('school_expense,school_expense_cat');
        $query->Field = ('school_expense.*,school_expense_cat.title as category');
        if ($cat):
            $query->Where = "where school_expense.school_id='" . mysql_real_escape_string($school_id) . "' and school_expense.on_date>='" . mysql_real_escape_string($from) . "' and school_expense.on_date<='" . mysql_real_escape_string($to) . "' and school_expense.category='" . mysql_real_escape_string($cat) . "' and school_expense.category=school_expense_cat.id ORDER BY school_expense.on_date desc";
        else:
            $query->Where = "where school_expense.school_id='" . mysql_real_escape_string($school_id) . "' and school_expense.on_date>='" . mysql_real_escape_string($from) . "' and school_expense.on_date<='" . mysql_real_escape_string($to) . "' and school_expense.category=school_expense_cat.id ORDER BY school_expense.on_date desc";
        endif;
        return $query->ListOfAllRecords('object');
    }

    function ExpenseReportDownload($school_id, $from, $to, $cat = 0) {
        $record = array();
        $orders_arr = array();
        $query = new query('school_expense,school_expense_cat');
        $query->Field = ('school_expense.*,school_expense_cat.title as category');
        if ($cat):
            $query->Where = "where school_expense.school_id='" . mysql_real_escape_string($school_id) . "' and school_expense.on_date>='" . mysql_real_escape_string($from) . "' and school_expense.on_date<='" . mysql_real_escape_string($to) . "' and school_expense.category='" . mysql_real_escape_string($cat) . "' and school_expense.category=school_expense_cat.id ORDER BY school_expense.on_date desc";
        else:
            $query->Where = "where school_expense.school_id='" . mysql_real_escape_string($school_id) . "' and school_expense.on_date>='" . mysql_real_escape_string($from) . "' and school_expense.on_date<='" . mysql_real_escape_string($to) . "' and school_expense.category=school_expense_cat.id ORDER BY school_expense.on_date desc";
        endif;
        $record = $query->ListOfAllRecords('object');

        if (!empty($record)): $sr = 1;
            $total = '0';
            $filename = "ExpenseReport.csv";

            header("Content-type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;filename=$filename");

            if ($cat): $category = get_object('school_expense_cat', $cat);
                echo "Category Name " . "\t" . $category->title . "\n";
            endif;
            echo "From Date" . "\t" . $from . "\n";
            echo "To Date " . "\t" . $to . "\n \n";
            echo 'Sr. No.' . "\t" . 'Title' . "\t" . "\t" . 'Description' . "\t" . 'Category' . "\t" . 'Date' . "\t" . 'Amount' . "\n";

            foreach ($record as $key => $value):
                echo $sr . ".\t" . $value->title . "\t" . $value->remarks . "\t" . $value->category . "\t" . $value->on_date . "\t" . number_format($value->amount, 2) . "\t\n";
                $total = $total + $value->amount;
                $sr++;
            endforeach;
            echo '' . "\t" . '' . "\t" . '' . "\t" . 'Total' . "\t" . '' . number_format($total, 2) . "\n";
            exit;
        endif;
    }

    function CatExpenseReport($school_id, $from, $to) {
        $record = array();
        $query1 = "SELECT id as cat_id,title as category_name FROM school_expense_cat
                            WHERE school_expense_cat.school_id='" . mysql_real_escape_string($school_id) . "'";


        $query2 = "SELECT category,sum(amount) as amount FROM school_expense
                              WHERE school_expense.school_id='" . mysql_real_escape_string($school_id) . "' and school_expense.on_date>='" . mysql_real_escape_string($from) . "' and school_expense.on_date<='" . mysql_real_escape_string($to) . "' GROUP BY category";

        $new_query = "SELECT * FROM (" . $query1 . ") as t1 left join (" . $query2 . ") as t2 ON t1.cat_id = t2.category";

        $QueryObj = new query();
        $QueryObj->ExecuteQuery($new_query);
        if ($QueryObj->GetNumRows()):
            while ($object = $QueryObj->GetObjectFromRecord()):
                $record[] = $object;
            endwhile;

        endif;
        return $record;
    }

    /* Get List of all schoolExpense in array */

    function listAll($school_id, $year = '', $cat = '') {
        $query = new query('school_expense,school_expense_cat');
        $query->Field = ('school_expense.*,school_expense_cat.title as category');
        if ($cat != '') {
            $query->Where = "where school_expense.school_id='" . mysql_real_escape_string($school_id) . "' and school_expense.category='$cat' and (school_expense.on_date BETWEEN '$year-01-01' and '$year-12-31') and school_expense.category=school_expense_cat.id ORDER BY school_expense.on_date desc";
        } else {
            $query->Where = "where school_expense.school_id='" . mysql_real_escape_string($school_id) . "' and school_expense.category=school_expense_cat.id and (school_expense.on_date BETWEEN '$year-01-01' and '$year-12-31') ORDER BY school_expense.on_date desc";
        }
        //$query->print=1;
        return $query->ListOfAllRecords('object');
    }

    /* delete a schoolExpense by id */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

    // List Expense Years
    function listExpenseYears($school_id) {
        $this->Field = "DISTINCT YEAR(on_date) AS year";
        $this->Where = "where school_id='$school_id' order by id desc";
        return $this->ListOfAllRecords('object');
    }

}

class schoolExpenseCategory extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'desc', $orderby = 'id') {
        parent::__construct('school_expense_cat');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'school_id', 'title', 'ip_address', 'remarks', 'on_date', 'update_date');
    }

    /*
     * Create new schoolExpense
     */

    function saveData($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            $this->Data['update_date'] = date('Y-m-d');
            $this->Update();
            return $this->Data['id'];
        } else {
            $this->Data['on_date'] = date('Y-m-d');
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    /* Get schoolExpense by id */

    function getRecord($id) {
        return $this->_getObject('school_expense_cat', $id);
    }

    /* Get List of all schoolExpense in array */

    function listAll($school_id, $show_active = 0, $result_type = 'object') {
        if ($show_active) {
            $this->Where = "where school_id='" . $school_id . "' ORDER BY id desc";
        } else {
            $this->Where = "where school_id='" . $school_id . "' ORDER BY id desc";
        }
        if ($result_type == 'object') {
            return $this->DisplayAll();
        } else {
            return $this->ListOfAllRecords('object');
        }
    }

    function listAllExpenseCat($school_id) {
        $this->Where = "where school_id='$school_id'";
        return $this->ListOfAllRecords('object');
    }

    /* delete a schoolExpense by id */

    function deleteRecord($id) {
        $this->id = $id;
        return $this->Delete();
    }

}

class expense_docs extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('school_expense_docs');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'expense_id', 'title', 'doc');
    }

    function saveExpenseDoc($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

// Get Stoppage Detail
    function getRecord($id) {
        return $this->_getObject('school_expense_docs', $id);
    }

    function listExpenseDocs($expense_id) {
        $this->Where = "where expense_id='$expense_id'";
        return $this->ListOfAllRecords('object');
    }

}

?>
