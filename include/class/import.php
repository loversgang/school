<?php
include(DIR_FS_SITE.'include/excel/excel_reader2.php');
class import extends Spreadsheet_Excel_Reader {

    var $essential_cols;
    var $general_validations;
    var $validate_attribute;
    var $header;
   
    var $image_data;
    var $attribute_data;
    var $product_data;
   
    
    function import($file=''){
        $this->essential_cols=array();
        $this->general_validations=array();
        $this->validate_attribute=array();
        $this->header=array();
        parent::Spreadsheet_Excel_Reader($file);
    }
     function getHeader(){
         $category=array();
         foreach($this->readline() as $k=>$v){
             $category[$k]=$v;
         }
         return $category;
    }
    function readLine($line=1){
        #read the first row & all columns | must be head
        $header=array();
        for($i=$line;$i<=$this->colcount();$i++){
             if($this->val(1,$i)!=''){
                $header[]=$this->val(1, $i);
             }
        }
        return $header;
    }

    function verifyEssentialCols(){
       foreach($this->essential_cols as $k=>$v){
            if(!in_array($v, $this->readline())){
                return false;
            }
        }
        return true;
     }

    function getData($i=2){
        $data=array();
        foreach($this->getHeader() as $k=>$v){
            $data[$v]=$this->val($i,$k+1);
        }
        return $data;
    }
};
?>