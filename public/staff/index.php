<?php
require_once("../../include/config_staff/config.php");
$include_fucntions = array('http', 'image_manipulation', 'users', 'email');
include_functions($include_fucntions);

/* check if already logged in */
if ($admin_user->is_logged_in()) {
    /* Check User type */
    if (isset($_SESSION['admin_session_secure']['login_type']) && $_SESSION['admin_session_secure']['login_type'] != 'staff') {

        $admin_user->set_error();
        $admin_user->set_pass_msg('Sorry, You are already logged in as ' . ucfirst($_SESSION['admin_session_secure']['login_type']) . '.');
        Redirect(DIR_WS_SITE . $_SESSION['admin_session_secure']['login_type'] . '/index.php');
    } else {

        redirect(make_admin_url('home', 'list', 'list'));
    }
}

/* check cookie */
if (isset($_COOKIE['staff'])) {
    $object = get_object('staff', $_COOKIE['staff']);
    if (is_object($object)) {
        if ($staff = validate_staff_account('staff', array('staff_login_id' => $object->staff_login_id, 'password' => $object->password), 0)) {
            $object->user_type = 'staff';
            $object->last_access = '';
            $object->allow_pages = '*';
            $object->username = $object->staff_login_id;
            $admin_user->set_admin_user_from_object($object);

            /* Set the login type */
            $_SESSION['admin_session_secure']['login_type'] = 'staff';

            Redirect1(DIR_WS_SITE . ADMIN_PUBLIC_FOLDER . "/control.php");
        }
    }
}
/* login user */
if (is_var_set_in_post('login')):
    if ($staff = validate_staff_account('staff', $_POST)):
        if ($staff && $staff->is_active == '1'):
            if (isset($_POST['remember'])) {
                setcookie('staff', $staff->id, time() + 60 * 60 * 24 * 30);
            }
            $staff->user_type = 'staff';
            $staff->last_access = '';
            $staff->allow_pages = '*';
            $staff->username = $staff->staff_login_id;
            $admin_user->set_admin_user_from_object($staff);

            /* Set the login type */
            $_SESSION['admin_session_secure']['login_type'] = 'staff';
            Redirect1(DIR_WS_SITE . ADMIN_PUBLIC_FOLDER . "/control.php");
        else:
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_LOGIN_INVALID_USERNAME_PASSWORD);
        //Redirect1(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/index.php');
        endif;
    elseif ($staff = validate_staff_account('staff', $_POST)):
        if ($staff && $staff->is_active == '1'):

            if (isset($_POST['remember'])) {
                setcookie('staff', $staff->id, time() + 60 * 60 * 24 * 30);
            }
            $staff->user_type = 'staff';
            $staff->last_access = '';
            $staff->allow_pages = '*';
            $staff->username = $staff->staff_login_id;
            $admin_user->set_admin_user_from_object($staff);

            /* Set the login type */
            $_SESSION['admin_session_secure']['login_type'] = 'staff';

            Redirect1(DIR_WS_SITE . ADMIN_PUBLIC_FOLDER . "/control.php");
        else:
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_LOGIN_INVALID_USERNAME_PASSWORD);
        //Redirect1(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/index.php');
        endif;
    else:
        $admin_user->set_error();
        $admin_user->set_pass_msg(MSG_LOGIN_INVALID_USERNAME_PASSWORD);
    //Redirect1(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/index.php');
    endif;
endif;

/* forgot_pass user */
if (is_var_set_in_post('forgot_pass')):
    $QueryObj = new query('school');
    $QueryObj->Where = "where school_head_email='" . $_POST['email'] . "'";
    $user = $QueryObj->DisplayOne();
    if ($QueryObj->GetNumRows()):
        $name = $user->username;
        $password = $user->password;
        $header = 'Forgot Password Request';
        $center_content = '';
        $subject = MAIN_SITE . ': Forgot Password Request';
        $footer = 'Best Regards, ' . "<br/>" . MAIN_SITE;
        $center_content.= "Dear " . ucfirst($user->username) . ",<br/>";
        $center_content.= "You have requested for your password for the site " . MAIN_SITE . "<br/><br/>";
        $center_content.= "Your Username is : " . $name . "<br/>";
        $center_content.= "Your Password is : " . $password . "<br/><br/>";
        $center_content.= "You may change your password for further security and remembrance" . "<br/><br/>";
        include_once(DIR_FS_SITE . 'include/email/general.php');
        $content = ob_get_contents();
        SendEmail($subject, $_POST['email'], ADMIN_EMAIL, MAIN_SITE, $content, BCC_EMAIL, 'html');
        $admin_user->set_pass_msg('Your Login Details has been successfully sent to your Email Id');
        Redirect(DIR_WS_SITE_ADMIN . 'index.php');
    else:
        $admin_user->set_error();
        $admin_user->set_pass_msg('Sorry, User with this Email Address does not exist!!');
        Redirect(DIR_WS_SITE_ADMIN . 'index.php');
    endif;
endif;
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>My Management Panel</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta name="Developers" content="WebGarh Solutions, Chandigarh"/>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" type="text/css" href="assets/template/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/template/css/freelancer.css"/>
        <link rel="stylesheet" type="text/css" href="assets/template/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="assets/template/css/sales.css"/>
        <link rel="stylesheet" type="text/css" href="assets/template/font-awesome/css/font-awesome.min.css"/>
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="assets/template/plugin/toastr/toastr.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/template/plugin/dataTable/dataTable.css"/>
        <link rel="stylesheet" type="text/css" href="assets/template/plugin/validation/validationEngine.jquery.css"/>
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo" style="color:white; font-weight:bold;font-size:20px">
            <?php /* echo SITE_NAME */ ?>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content" style="padding-top: 20px">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: #2c3e50;color: #fff;">
                                <div style='margin-bottom:10px;text-align:center;'>
                                    <h3 class="form-title" style='margin-bottom:0px;line-height:20px;'>Staff Login</h3>
                                    <img alt="ARSHSOFT" src="http://www.arshsoft.com//theme/schoolmanagement/graphic/img/logo.png">
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php display_message(1); ?>
                                <div class="alert alert-error hide">
                                    <button class="close" data-dismiss="alert"></button>
                                    <span>Enter any username and password.</span>
                                </div>
                                <form class="form-vertical " method="post" name="form_login" id="validation">
                                    <fieldset>
                                        <div class="form-group">
                                            <input class="form-control validate[required]" id="staff_login_id" placeholder="Login ID" name="staff_login_id" type="text">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control validate[required]" placeholder="Password" name="password" type="password" id="password" value="">
                                        </div>
                                        <div class="checkbox">
                                            <label>
                                                <input name="remember" type="checkbox" value="Remember Me"> Remember Me
                                            </label>
                                        </div>
                                        <input class="btn btn-lg btn-success btn-block" type="submit" value="Login" name="login">
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END LOGIN -->
        <script type="text/javascript" src="assets/template/js/jquery.js"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="assets/template/js/bootstrap.min.js"></script>
        <script src="assets/template/js/script.js"></script>
        <!-- Plugin JavaScript -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="assets/template/js/classie.js"></script>
        <script src="assets/template/js/cbpAnimatedHeader.js"></script>
        <!-- Contact Form JavaScript -->
        <script src="assets/template/js/jqBootstrapValidation.js"></script>
        <!-- Custom Theme JavaScript -->
        <script src="assets/template/js/freelancer.js"></script>
        <script src="assets/template/plugin/dataTable/dataTable.js"></script>
        <script src="assets/template/plugin/toastr/toastr.min.js"></script>
        <script src="assets/template/plugin/validation/jquery.validationEngine-en.js"></script>
        <script src="assets/template/plugin/validation/jquery.validationEngine.js"></script>
        <script>
            $(document).ready(function () {
                $("#validation").validationEngine({promptPosition: "topLeft"});
                $('#sample_2').DataTable({
                    "bLengthChange": false
                });
            });
        </script>
    </body>
    <!-- END BODY -->
</html>
