<?php
require_once("../../include/config_account/config.php");

$include_fucntions = array('http', 'image_manipulation', 'users', 'email');
include_functions($include_fucntions);

/* check if already logged in */
if ($admin_user->is_logged_in()) {
    /* Check User type */
    if (isset($_SESSION['admin_session_secure']['login_type']) && $_SESSION['admin_session_secure']['login_type'] != 'account') {

        $admin_user->set_error();
        $admin_user->set_pass_msg('Sorry, You are already logged in as ' . ucfirst($_SESSION['admin_session_secure']['login_type']) . '.');
        Redirect(DIR_WS_SITE . $_SESSION['admin_session_secure']['login_type'] . '/index.php');
    } else {

        redirect(make_admin_url('home', 'list', 'list'));
    }
}

/* check cookie */
if (isset($_COOKIE['account'])) {
    $object = get_object('school', $_COOKIE['account']);
    if (is_object($object)) {
        if ($user = validate_without_encript('school', array('username' => $object->username, 'password' => $object->password), 0)) {
            $admin_user->set_admin_user_from_object($object);
            update_last_access($user->id, 1);

            /* Set the login type */
            $_SESSION['admin_session_secure']['login_type'] = 'account';

            Redirect1(DIR_WS_SITE . ADMIN_PUBLIC_FOLDER . "/control.php");
        }
    }
}


/* login user */
if (is_var_set_in_post('login')):
    if ($user = validate_without_encript('school', $_POST)):
        if ($user && $user->is_active == '1'):
            if (isset($_POST['remember'])) {

                setcookie('account', $user->id, time() + 60 * 60 * 24 * 30);
            }
            $admin_user->set_admin_user_from_object($user);
            update_last_access($user->id, 1);

            /* Set the login type */
            $_SESSION['admin_session_secure']['login_type'] = 'account';

            Redirect1(DIR_WS_SITE . ADMIN_PUBLIC_FOLDER . "/control.php");
        else:
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_LOGIN_INVALID_USERNAME_PASSWORD);
        //Redirect1(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/index.php');
        endif;
    elseif ($user = validate_without_encript('user', $_POST)):
        if ($user && $user->is_active == '1'):
            #set id as the school id    
            $user->id = $user->school_id;

            if (isset($_POST['remember'])) {
                setcookie('account', $user->id, time() + 60 * 60 * 24 * 30);
            }

            update_last_access_user($user->id, 1);
            $admin_user->set_admin_user_from_object($user);

            /* Set the login type */
            $_SESSION['admin_session_secure']['login_type'] = 'account';

            Redirect1(DIR_WS_SITE . ADMIN_PUBLIC_FOLDER . "/control.php");
        else:
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_LOGIN_INVALID_USERNAME_PASSWORD);
        //Redirect1(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/index.php');
        endif;
    else:
        $admin_user->set_error();
        $admin_user->set_pass_msg(MSG_LOGIN_INVALID_USERNAME_PASSWORD);
    //Redirect1(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/index.php');
    endif;
endif;

/* forgot_pass user */
if (is_var_set_in_post('forgot_pass')):
    $QueryObj = new query('school');
    $QueryObj->Where = "where school_head_email='" . $_POST['email'] . "'";
    $user = $QueryObj->DisplayOne();
    if ($QueryObj->GetNumRows()):
        $name = $user->username;
        $password = $user->password;
        $header = 'Forgot Password Request';
        $center_content = '';
        $subject = MAIN_SITE . ': Forgot Password Request';
        $footer = 'Best Regards, ' . "<br/>" . MAIN_SITE;
        $center_content.= "Dear " . ucfirst($user->username) . ",<br/>";
        $center_content.= "You have requested for your password for the site " . MAIN_SITE . "<br/><br/>";
        $center_content.= "Your Username is : " . $name . "<br/>";
        $center_content.= "Your Password is : " . $password . "<br/><br/>";
        $center_content.= "You may change your password for further security and remembrance" . "<br/><br/>";
        include_once(DIR_FS_SITE . 'include/email/general.php');
        $content = ob_get_contents();
        SendEmail($subject, $_POST['email'], ADMIN_EMAIL, MAIN_SITE, $content, BCC_EMAIL, 'html');
        $admin_user->set_pass_msg('Your Login Details has been successfully sent to your Email Id');
        Redirect(DIR_WS_SITE_ADMIN . 'index.php');
    else:
        $admin_user->set_error();
        $admin_user->set_pass_msg('Sorry, User with this Email Address does not exist!!');
        Redirect(DIR_WS_SITE_ADMIN . 'index.php');
    endif;
endif;
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>My Management Panel</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta name="Developers" content="WebGarh Solutions, Chandigarh"/>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES for validation -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/validation/validationEngine.jquery.css" />
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo" style="color:white; font-weight:bold;font-size:20px">
            <?php /* echo SITE_NAME */ ?>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="form-vertical " method="post" name="form_login" id="validation">
                <div style='margin-bottom:10px;text-align:center;'>
                    <h3 class="form-title" style='margin-bottom:0px;line-height:20px;'>Management Panel By</h3>
                    <img alt="ARSHSOFT" src="http://www.arshsoft.com//theme/schoolmanagement/graphic/img/logo.png">
                </div>		  
                <div class='login-form'>
                    <?php display_message(1); ?>
                    <div class="alert alert-error hide">
                        <button class="close" data-dismiss="alert"></button>
                        <span>Enter any username and password.</span>
                    </div>

                    <div class="control-group">
                        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                        <label class="control-label visible-ie8 visible-ie9">Username</label>
                        <div class="controls">
                            <div class="input-icon left">
                                <i class="icon-user"></i>
                                <input class="m-wrap placeholder-no-fix validate[required]" type="text" id="username" name="username" placeholder="User Name">
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label visible-ie8 visible-ie9">Password</label>
                        <div class="controls">
                            <div class="input-icon left">
                                <i class="icon-lock"></i>
                                <input class="m-wrap placeholder-no-fix validate[required]" type="password" id="password" name="password" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <label class="checkbox">
                            <div class="" style='float:left;'><span><input type="checkbox" name="remember" value="1"></span></div> Remember me
                        </label>

                        <button type="submit" name="login" value="Login" class="btn blue pull-right" id="login_button">
                            Login <i class="m-icon-swapright m-icon-white"></i>
                        </button>            
                    </div>

                    <div style="margin-top:15px" class="forget-password">
                        <h4>Forgot your password ?</h4>
                        <p>
                            <a href="javascript:void(0)" class="" id="forget-password">click here to reset your password.</a>

                        </p>
                    </div>
                </div>	
                <!-- END LOGIN FORM -->        
                <!-- BEGIN FORGOT PASSWORD FORM -->
                <div class="forget-form">

                    <p>Enter your e-mail address below to reset your password.</p>
                    <div class="control-group">
                        <div class="controls">
                            <div class="input-icon left">
                                <i class="icon-envelope"></i>
                                <input class="m-wrap placeholder-no-fix validate[required,custom[email]]" type="text" id="email" name="email" placeholder="Email">
                            </div>

                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" id="back-btn" class="btn">
                            <i class="m-icon-swapleft"></i> Back
                        </button>

                        <button name="forgot_pass" type="submit" class="btn blue pull-right">
                            Submit <i class="m-icon-swapright m-icon-white"></i>
                        </button>            
                    </div>
                </div>
                <!-- END FORGOT PASSWORD FORM -->		
            </form>
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <!--<div class="copyright">
        <?php echo date('Y') ?> &copy; <?php echo SITE_NAME ?> - Admin Panel.
        </div>-->
        <!-- END COPYRIGHT -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
        <script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!--[if lt IE 9]>
        <script src="assets/plugins/excanvas.min.js"></script>
        <script src="assets/plugins/respond.min.js"></script>  
        <![endif]-->   
        <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
        <script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
        <script src="assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <!-- Validation engine -->
        <script src="assets/plugins/validation/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
        <script src="assets/plugins/validation/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
        <!-- END PAGE LEVEL PLUGINS -->

        <script src="assets/scripts/app.js" type="text/javascript"></script>
        <script src="assets/scripts/login-soft.js" type="text/javascript"></script> 
        <script src="assets/scripts/form-validation.js"></script> 
        <!-- END PAGE LEVEL SCRIPTS --> 
        <script>
            jQuery(document).ready(function () {
                App.init();
                Login.init();
                FormValidation.init();
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>