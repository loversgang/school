var Lock = function () {

    return {
        //main function to initiate the module
        init: function () {

             $.backstretch([
                        "assets/img/bg/12.jpg",
		        "assets/img/bg/10.jpg",		        
		        "assets/img/bg/11.jpg",
                        "assets/img/bg/4.jpg"
		        ], {
		          fade: 1000,
		          duration: 8000
		      });
        }

    };

}();