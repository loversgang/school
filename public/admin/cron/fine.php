<?php

set_time_limit(0);
/*  This Script to update the all attendance fine,late fee and vehicle fee */

require_once("../../../include/config_admin/config.php");

include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subscriptionPlanClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSubscriptionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/paymentHistoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSettingClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsEmailClass.php');

$include_fucntions = array('http', 'image_manipulation', 'users', 'email');
include_functions($include_fucntions);

$all_services = array('birthday', 'holiday', 'payment_reminder', 'enquiry');

#Get al the current session
$QSession = new query('session');
$QSession->Field = ("id,start_date,end_date,school_id,late_fee_days,absent_fine,late_fee_rate");
$QSession->Where = "where is_active='1' AND DATE(start_date) <= CAST('" . date('Y-m-d') . "' AS DATE ) AND DATE(end_date) >= CAST('" . date('Y-m-d') . "' AS DATE) order by id asc";
$CurrentSession = $QSession->ListOfAllRecords('object');

if (!empty($CurrentSession)):
    foreach ($CurrentSession as $s_key => $s_val):

        #get the current Interval of every session
        $QueryInt = new query('session_interval');
        $QueryInt->Field = ("id,from_date,to_date,interval_position");
        $QueryInt->Where = "where session_id='" . $s_val->id . "' AND DATE(from_date) <= CAST('" . date('Y-m-d') . "' AS DATE ) AND DATE(to_date) >= CAST('" . date('Y-m-d') . "' AS DATE) order by id asc";
        $CurrentInterval = $QueryInt->DisplayOne();

        if (is_object($CurrentInterval)):

            #Now Get All Student for this Session
            $QuSessStu = new studentSession();
            $AllStudent = $QuSessStu->sessionStudents($s_val->id);
            if (!empty($AllStudent)):
                foreach ($AllStudent as $st_key => $st_val):
                    #get Student Info
                    $QueryStu = new studentSession();
                    $student_info = $QueryStu->SingleStudentsWithSession($st_val->id, $s_val->id, $st_val->section);

                    #get Detail of the current session interval fee for every student 
                    $QueryStuInt = new query('student_session_interval');
                    $QueryStuInt->Where = "where session_id='" . $s_val->id . "' AND student_id='" . $student_info->id . "' and interval_id='" . $CurrentInterval->id . "' order by id asc";
                    $StudentInterval = $QueryStuInt->DisplayOne();

                    #concession
                    $concession = $student_info->amount;

                    #Get late fee 
                    $QueryLateFee = new session();
                    $LateFee = $QueryLateFee->LateFee($s_val->id, $st_val->section, $student_info->id, $CurrentInterval->from_date, $CurrentInterval->to_date);

                    #get Student vehicle fee
                    $QueryStuVehFee = new vehicleStudent();
                    $Vehicle_Fee = $QueryStuVehFee->checkVehicleStudentFeeInInterval($s_val->id, $st_val->section, $student_info->id, $CurrentInterval->from_date, $CurrentInterval->to_date);

                    #Get student payheads students
                    $SessStuFeeObj = new session_student_fee();
                    $pay_heads = $SessStuFeeObj->GetStudentPayHeads($s_val->id, $student_info->id, 'array');

                    $regular = '';
                    $other = '';
                    $regular_heads = array();
                    $other_heads = array();
                    $obj = new studentSession;
                    $check_concession = $obj->getConcession($session_id, $ct_sect, $student_id);
                    if ($check_concession) {
                        if ($check_concession->type == 'regular') {
                            $regular = $regular + $check_concession->amount;
                            $regular_heads[] = array('title' => $check_concession->title, 'type' => $check_concession->type, 'amount' => $check_concession->amount);
                        } else {
                            $other = $other + $check_concession->amount;
                            $other_heads[] = array('title' => $check_concession->title, 'type' => $check_concession->type, 'amount' => $check_concession->amount);
                        }
                    }
                    if (!empty($pay_heads)):
                        foreach ($pay_heads as $key => $value):
                            if ($value['type'] == 'Regular'):
                                $regular = $regular + $value['amount'];
                                $regular_heads[] = array('title' => $value['title'], 'type' => $value['type'], 'amount' => $value['amount']);
                            else:
                                $other = $other + $value['amount'];
                                $other_heads[] = array('title' => $value['title'], 'type' => $value['type'], 'amount' => $value['amount']);
                            endif;
                        endforeach;
                    endif;

                    $Absent = '0';
                    if ($CurrentInterval->interval_position == '1'):
                        $total = $regular + $other;
                        $heads = array_merge($other_heads, $regular_heads);
                    else:
                        $total = $regular;
                        if (empty($regular_heads)):
                            $regular_heads[] = array('title' => 'No Fee', 'type' => 'regular', 'amount' => '0');
                        endif;
                        $heads = $regular_heads;
                    endif;

                    #get the previous Interval dates to get the absent fine
                    $PreOuInt = new query('session_interval');
                    $PreOuInt->Field = ("id,from_date,to_date,interval_position");
                    $PreOuInt->Where = "where session_id='" . $s_val->id . "' AND DATE(to_date) < CAST('" . $CurrentInterval->from_date . "' AS DATE )  order by from_date desc";
                    $PreviousInterval = $PreOuInt->DisplayOne();

                    if (is_object($PreviousInterval)):
                        #get Student Absent for previous months
                        $QurAtten = new attendance();
                        $Absent = $QurAtten->getStudentAbsent($s_val->id, $st_val->section, $student_info->id, $PreviousInterval->from_date, $PreviousInterval->to_date);
                        $Absent_fine = $Absent * $s_val->absent_fine;
                    else:
                        $Absent_fine = '0';
                    endif;

                    if (is_object($StudentInterval)):
                        if (!empty($StudentInterval->other_fee)):
                            if ($StudentInterval->other_fee_type == '+'):
                                $total = $total + $StudentInterval->other_fee;
                            elseif ($StudentInterval->other_fee_type == '-'):
                                $total = $total - $StudentInterval->other_fee;
                            endif;
                        endif;

                        $ObjQuery = new query('student_session_interval');
                        $ObjQuery->Data['session_id'] = $s_val->id;
                        $ObjQuery->Data['student_id'] = $student_info->id;
                        $ObjQuery->Data['interval_id'] = $CurrentInterval->id;

                        $ObjQuery->Data['vehicle_fee'] = $Vehicle_Fee;
                        $ObjQuery->Data['late_fee'] = $LateFee;
                        $ObjQuery->Data['concession'] = $concession;
                        $ObjQuery->Data['absent_fine'] = $Absent_fine;
                        $ObjQuery->Data['total'] = (($total + $Vehicle_Fee + $LateFee + $Absent_fine) - $concession);

                        $ObjQuery->Data['heads'] = serialize($heads);
                        $ObjQuery->Data['id'] = $StudentInterval->id;
                        $ObjQuery->Data['by_cron'] = date('Y-m-d');
                        $ObjQuery->Update();
                    else:
                        $ObjQuery = new query('student_session_interval');
                        $ObjQuery->Data['session_id'] = $s_val->id;
                        $ObjQuery->Data['student_id'] = $student_info->id;
                        $ObjQuery->Data['interval_id'] = $CurrentInterval->id;

                        $ObjQuery->Data['vehicle_fee'] = $Vehicle_Fee;
                        $ObjQuery->Data['late_fee'] = $LateFee;
                        $ObjQuery->Data['concession'] = $concession;
                        $ObjQuery->Data['absent_fine'] = $Absent_fine;
                        $ObjQuery->Data['total'] = (($total + $Vehicle_Fee + $LateFee + $Absent_fine) - $concession);
                        $ObjQuery->Data['on_date'] = date('Y-m-d');
                        $ObjQuery->Data['heads'] = serialize($heads);
                        $ObjQuery->Data['by_cron'] = date('Y-m-d');
                        $ObjQuery->Insert();
                    endif;

                    #Single Fee Update	
                endforeach;
            endif;
        endif;
    endforeach;
endif;



echo 'Cron Job Done';
exit;
?>

