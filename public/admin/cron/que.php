<?php 
/*  This Script put all emails And Sms Into the Sms Email Que*/

require_once("../../../include/config_admin/config.php");

include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subscriptionPlanClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSubscriptionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/paymentHistoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSettingClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSmsEmailClass.php');

        $include_fucntions=array('http', 'image_manipulation','users','email');
		include_functions($include_fucntions);
		
		$all_services=array('birthday','holiday','payment_reminder','enquiry','fee_reminder');
		//$all_services=array('holiday');
		
		#for Birthday Wishes
		if(in_array('birthday',$all_services)):
			//Get all active session and there student
			$QuerySt= new school();
			$All_student=$QuerySt->GetActiveSchoolSessionStudentsForBirthday();
			if(!empty($All_student)):
				foreach($All_student as $Skey=>$s_info):
							$school=get_object('school',$s_info->school_id);	
							$QyerySchool=new schoolSetting();
							$school_setting=$QyerySchool->getSchoolSetting($school->id);	
						
							/* Get Parents Deatil*/
							$QueryPr = new query('student_family_details');
							$QueryPr->Where="where student_id='".$s_info->student_id."'";
							$parents=$QueryPr->DisplayOne(); 
					
							$earr1=array();
							$earr1['PARENTS']=ucfirst($parents->father_name)." & ".ucfirst($parents->mother_name);
							$earr1['NAME']=ucfirst($s_info->first_name).' '.$s_info->last_name;				
							$earr1['SITE_NAME']=$school->school_name;
							$earr1['PRINCIPAL_NAME']=$school->school_head_name;	
							$earr1['SCHOOL_NAME']=$school->school_name;	
							$earr1['CONTACT_PHONE']=$school->school_phone;
							$earr1['CONTACT_EMAIL']=$school->email_address;	
							
								if($school_setting->is_email_allowed=='1'):
										$QueryEmail = new schoolEmail();
										$Check_email=$QueryEmail->getTypeEmail($school->id,'birthday');										
											if(is_object($Check_email)):												
												$msg1=get_database_msg('8',$earr1);
												$subject=get_database_msg_subject('8',$earr1);												
												unset($_POST);
													$_POST['school_id']=$school->id; $_POST['email_type']='birthday'; $_POST['type']='email'; $_POST['on_date']=date('Y-m-d'); $_POST['email_school_name']=$school->school_name; $_POST['email_subject']=$subject; 
													$_POST['to_email']=$parents->email; $_POST['email_text']=$msg1; $_POST['student_id']=$s_info->student_id; $_POST['from_email']=$school->email_address;
													
													$QuerySave = new schoolSmsEmailHistory();												
													$QuerySave->saveData($_POST);	
											endif;
								endif;
								
								#Send and Add Entry to Sms counter if set in setting
								if($school_setting->is_sms_allowed=='1'):														
											$QuerySms = new schoolSms();
											$Check_sms=$QuerySms->getTypeSms($school->id,'birthday');										
												if(is_object($Check_sms)):													
														$msg=get_database_msg_only('15',$earr1);												
														unset($_POST);
														$_POST['school_id']=$school->id; $_POST['sms_type']='birthday'; $_POST['type']='sms'; $_POST['on_date']=date('Y-m-d');
														$_POST['to_number']=$parents->mobile; $_POST['sms_text']=$msg; $_POST['student_id']=$s_info->student_id; 
														
														$QuerySave1 = new schoolSmsEmailHistory();												
														$QuerySave1->saveData($_POST);	
															
												endif;
								endif;				
				endforeach;
			endif;
		endif;
		
		
		
		#for Next Day Holiday 
		if(in_array('holiday',$all_services)):
					
					$next_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 days'));		
                    $query=new query('school_holiday');
                    $query->Where="where alert='1' and holiday_date='".$next_date."' GROUP BY school_id";
					$holiday=$query->ListOfAllRecords('object');
					echo '<pre>'; print_r($holiday); exit;
					if(!empty($holiday)):
						foreach($holiday as $h_key=>$h_s_info):	 
							//Get all active session and there student
							$QuerySt= new school();
							$All_student=$QuerySt->GetActiveSchoolSessionStudents($h_s_info->school_id);	
							
							if(!empty($All_student)):
								foreach($All_student as $Skey=>$s_value):
											$school=get_object('school',$s_value->school_id);	
											$QyerySchool=new schoolSetting();
											$school_setting=$QyerySchool->getSchoolSetting($school->id);
											
											/* Get Parents Deatil*/
											$QueryPr = new query('student_family_details');
											$QueryPr->Where="where student_id='".$s_value->student_id."'";
											$parents=$QueryPr->DisplayOne(); 											
											
											$earr1=array();
											$earr1['NAME']=ucfirst($s_value->first_name).' '.$s_value->last_name;				
											$earr1['DATE']=$next_date;	
											$earr1['DATE_NEW'] = date('Y-m-d',strtotime($next_date.'+1 days'));	
											$earr1['DATE1'] = date('Y-m-d',strtotime($next_date.'+1 days'));												
											$earr1['HOLIDAY']=$h_s_info->title;	
											$earr1['SITE_NAME']=$school->school_name;
											$earr1['PRINCIPAL_NAME']=$school->school_head_name;	
											$earr1['SCHOOL_NAME']=$school->school_name;	
											$earr1['CONTACT_PHONE']=$school->school_phone;
											$earr1['CONTACT_EMAIL']=$school->email_address;												
														
											if($school_setting->is_email_allowed=='1'):	
												$QueryEmail = new schoolEmail();
												$Check_email=$QueryEmail->getTypeEmail($school->id,'holiday');	
													if(is_object($Check_email)):
														$msg1=get_database_msg('9',$earr1);
														$subject=get_database_msg_subject('9',$earr1);														
														unset($_POST);
															$_POST['school_id']=$school->id; $_POST['email_type']='holiday'; $_POST['type']='email'; $_POST['on_date']=date('Y-m-d'); $_POST['email_school_name']=$school->school_name; $_POST['email_subject']=$subject; 
															$_POST['to_email']=$parents->email; $_POST['email_text']=$msg1; $_POST['student_id']=$s_value->student_id; $_POST['from_email']=$school->email_address;
															
															$QuerySave = new schoolSmsEmailHistory();												
															$QuerySave->saveData($_POST);	
													endif;
											endif;	
											
											#Send and Add Entry to Sms counter if set in setting
											if($school_setting->is_sms_allowed=='1'):														
														$QuerySms = new schoolSms();
														$Check_sms=$QuerySms->getTypeSms($school->id,'holiday');										
															if(is_object($Check_sms)):																
																	$msg=get_database_msg_only('14',$earr1);												
																	unset($POST);
																	$POST['school_id']=$school->id; $_POST['sms_type']='holiday'; $POST['type']='sms'; $POST['on_date']=date('Y-m-d');
																	$POST['to_number']=$parents->mobile; $POST['sms_text']=$msg; $POST['student_id']=$s_value->student_id; 
																	
																	$QuerySave1 = new schoolSmsEmailHistory();												
																	$QuerySave1->saveData($_POST);
																																
															endif;
											endif;												
											
								endforeach;
							endif;		
						endforeach;				
					endif;
		endif;			

		
		#for Payment Reminder For Client
		if(in_array('payment_reminder',$all_services)):
		
                    $query=new query('school');
                    $query->Where="where is_active='1' and is_deleted='0'";
                    $reocrd=$query->ListOfAllRecords('object'); 
					if(!empty($reocrd)):
						
						foreach($reocrd as $key=>$school_info):									
								$Query = new school();
								$school=$Query->getSchoolWithPlan($school_info->id);
																
								$last_date = date('Y-m-d',strtotime(date('Y-m-d').'+1 year'));
							if(is_numeric($school->payment_type)):
								// Get Due payment up to 15 days left 			
								$QueryPay = new school();
								$due_payment=$QueryPay->getSchoolCurrentPaymentDate($school->create_date,$last_date,$school->payment_type);
								
								if(!empty($due_payment)):
										$earr1=array();
										$earr1['NAME']=ucfirst($school->school_head_name);	
										$earr1['SITE_NAME']=SITE_NAME;						
										$earr1['DATE']=$due_payment;														
										$earr1['SCHOOL']=$school->school_name;	
										$earr1['SALES_EMAIL']=SUPPORT_EMAIL;	
										
										$msg1=get_database_msg('10',$earr1);	
										unset($_POST);
											$_POST['school_id']=$school->id; $_POST['email_type']='payment_reminder'; $_POST['type']='email'; $_POST['on_date']=date('Y-m-d');
											$_POST['to_email']=$school->school_head_email; $_POST['email_text']=$msg1;$_POST['from_email']=ADMIN_EMAIL; 
											
											$QuerySave = new schoolSmsEmailHistory();												
											$QuerySave->saveData($_POST);											
								endif;
							endif;
						endforeach;
					endif;
		endif;	

		#for Enquiry Reminder For Client
		if(in_array('enquiry',$all_services)):
		
                    $query=new query('school');
                    $query->Where="where is_active='1' and is_deleted='0'";
                    $reocrd=$query->ListOfAllRecords('object'); 
					if(!empty($reocrd)):
						
						foreach($reocrd as $key=>$school_info):									
							$QueryEn = new schoolEnquiry();
							$enqury=$QueryEn->listAllActiveConfirm($school_info->id,'array');
								
							if(!empty($enqury)):
							
								foreach ($enqury as $ek=>$ev):								
									if($ev->msg_send<3):
										if(!empty($ev->phone)):	
												$earr1=array();
												$earr1['NAME']=ucfirst($ev->name);	
												$earr1['SITE_NAME']=$school_info->school_name;						
												$earr1['DATE']=$ev->on_date;														
												$earr1['PHONE']=$school_info->school_phone;
												$msg1=get_database_msg('3',$earr1);	
											
											if(empty($ev->msg_date)):
												#first Message Send
												unset($_POST);
													$_POST['school_id']=$school_info->id; $_POST['sms_type']='enquiry'; $_POST['type']='sms'; $_POST['on_date']=date('Y-m-d');
													$_POST['to_number']=$ev->phone; $_POST['sms_text']=$msg1; 
													$QuerySave = new schoolSmsEmailHistory();												
													$QuerySave->saveData($_POST);	
												unset($_POST);
													$_POST['id']=$ev->id; $_POST['msg_date']=date('Y-m-d'); $_POST['msg_send']='1';
													
													#Set Dead schoolEnquiry
													$QueryObj = new schoolEnquiry();
													$QueryObj->saveData($_POST);													
											else: 
												if($ev->msg_date=='1'): 
													#Second Message Send After 3 Days of last message
													$time1 = strtotime(date('Y-m-d', strtotime($ev->msg_date)).' +3 days');
													if($time1==strtotime(date('Y-m-d'))): 
													unset($_POST);
														$_POST['school_id']=$school_info->id; $_POST['sms_type']='enquiry'; $_POST['type']='sms'; $_POST['on_date']=date('Y-m-d');
														$_POST['to_number']=$ev->phone; $_POST['sms_text']=$msg1; 
														$QuerySave = new schoolSmsEmailHistory();												
														$QuerySave->saveData($_POST);	
													unset($_POST);
														$_POST['id']=$ev->id; $_POST['msg_date']=date('Y-m-d'); $_POST['msg_send']=$ev->msg_send+1;
														
														#Save school Enquiry
														$QueryObj = new schoolEnquiry();
														$QueryObj->saveData($_POST);
													endif;		
												elseif($ev->msg_date=='2'):
													#Third Message Send After 6 Days after last message
													$time1 = strtotime(date('Y-m-d', strtotime($ev->msg_date)).' +6 days');
													if($time1==strtotime(date('Y-m-d'))): 
													unset($_POST);
														$_POST['school_id']=$school_info->id; $_POST['sms_type']='enquiry'; $_POST['type']='sms'; $_POST['on_date']=date('Y-m-d');
														$_POST['to_number']=$ev->phone; $_POST['sms_text']=$msg1; 
														$QuerySave = new schoolSmsEmailHistory();												
														$QuerySave->saveData($_POST);	
													unset($_POST);
														$_POST['id']=$ev->id; $_POST['msg_date']=date('Y-m-d'); $_POST['msg_send']=$ev->msg_send+1;
														
														#Save school Enquiry
														$QueryObj = new schoolEnquiry();
														$QueryObj->saveData($_POST);													 
													endif;	
												endif;			
											endif;
										endif;		
									else:
										#After 9 days of last msg and total 21 days after reg 
										$time1 = strtotime(date('Y-m-d', strtotime($ev->msg_date)).' +9 days');
								
										if($time1==strtotime(date('Y-m-d'))): 								
										unset($_POST);
											$_POST['id']=$ev->id; $_POST['status']='dead';
											
											#Set Dead schoolEnquiry
											$QueryObj = new schoolEnquiry();
											$QueryObj->saveData($_POST);
										endif;		
									endif;
								endforeach;
							endif;
						endforeach;
					endif;
		endif;


		#for Fee Reminder For parent
		if(in_array('fee_reminder',$all_services)):		
		
				#Get al the current session
				$QSession=new query('session');
				$QSession->Field=("id,start_date,end_date,school_id,late_fee_days,absent_fine,late_fee_rate");
				$QSession->Where="where is_active='1' AND DATE(start_date) <= CAST('".date('Y-m-d')."' AS DATE ) AND DATE(end_date) >= CAST('".date('Y-m-d')."' AS DATE) order by id asc";
				$CurrentSession=$QSession->ListOfAllRecords('object');			
				
				if(!empty($CurrentSession)):
					foreach($CurrentSession as $s_key=>$s_val):
						
						#get the current Interval of every session
						$QueryInt=new query('session_interval');
						$QueryInt->Field=("id,from_date,to_date,interval_position");
						$QueryInt->Where="where session_id='".$s_val->id."' AND DATE(from_date) <= CAST('".date('Y-m-d')."' AS DATE ) AND DATE(to_date) >= CAST('".date('Y-m-d')."' AS DATE) order by id asc";
						$CurrentInterval=$QueryInt->DisplayOne();	
								
						if(is_object($CurrentInterval)):
						
							#now check current date for one on first day of the interval 
							if($CurrentInterval->from_date==date('Y-m-d')):	
							
								#Now Get All Student for this Session
								$QuSessStu=new studentSession();
								$AllStudent=$QuSessStu->sessionStudents($s_val->id);
									if(!empty($AllStudent)):
										foreach($AllStudent as $st_key=>$st_val):
											#get Student Info
											$QueryStu=new studentSession();
											$student=$QueryStu->SingleStudentsWithSession($st_val->id,$s_val->id,$st_val->section);								
											
											$school=get_object('school',$s_val->school_id);	
											$QyerySchool=new schoolSetting();
											$school_setting=$QyerySchool->getSchoolSetting($school->id);												
											
											$next_date = date('Y-m-d',strtotime(date('Y-m-d').'+'.$s_val->late_fee_days.' days'));
											
											/* Get Parents Deatil*/
											$QueryPr = new query('student_family_details');
											$QueryPr->Where="where student_id='".$student->id."'";
											$parents=$QueryPr->DisplayOne(); 
											
											#get Detail of the current session interval fee for every student 
											$QueryStuInt=new query('student_session_interval');
											$QueryStuInt->Where="where session_id='".$s_val->id."' AND student_id='".$student->id."' and interval_id='".$CurrentInterval->id."' order by id asc";
											$StudentInterval=$QueryStuInt->DisplayOne();
											
											#get total fee record 
											$QueryStu=new query('student_session_interval');
											$QueryStu->Field=("SUM(TOTAL) as total_fee,SUM(PAID) as paid");
											$QueryStu->Where="where session_id='".$s_val->id."' AND student_id='".$student->id."' order by id asc";
											$Fee_rec=$QueryStu->DisplayOne();											

											$earr1=array();
											$earr1['PARENTS']=ucfirst($parents->father_name)." & ".ucfirst($parents->mother_name);
											$earr1['NAME']=ucfirst($student->first_name).' '.$student->last_name;				
											$earr1['TOTAL_FEE']=number_format($Fee_rec->total_fee-$Fee_rec->paid,2);						
											$earr1['SITE_NAME']=$school->school_name;	
											$earr1['LAST_DATE']=$next_date;
											$earr1['PRINCIPAL_NAME']=$school->school_head_name;	
											$earr1['SCHOOL_NAME']=$school->school_name;	
											$earr1['CONTACT_PHONE']=$school->school_phone;
											$earr1['CONTACT_EMAIL']=$school->email_address;											
												
									
											
										if(is_object($parents)):	
											if($school_setting->is_email_allowed=='1'):	
												$QueryEmail = new schoolEmail();
												$Check_email=$QueryEmail->getTypeEmail($school->id,'fee_reminder');	
													if(is_object($Check_email)):
														$msg1=get_database_msg('19',$earr1);
														$subject=get_database_msg_subject('19',$earr1);														
														unset($POST);
															$POST['school_id']=$school->id; $POST['email_type']='fee_reminder'; $POST['type']='email'; $POST['on_date']=date('Y-m-d'); $POST['email_school_name']=$school->school_name; $POST['email_subject']=$subject; 
															$POST['to_email']=$parents->email; $POST['email_text']=$msg1; $POST['student_id']=$student->id; $POST['from_email']=$school->email_address;
															
															$QuerySave = new schoolSmsEmailHistory();												
															$QuerySave->saveData($POST);	
													endif;
											endif;	
							
											#Send and Add Entry to Sms counter if set in setting
											if($school_setting->is_sms_allowed=='1'):	
												$earr1['AMOUNT']=number_format($Fee_rec->total_fee-$Fee_rec->paid,2);											
														$QuerySms = new schoolSms();
														$Check_sms=$QuerySms->getTypeSms($school->id,'fee_reminder');										
															if(is_object($Check_sms)):																
																	$msg=get_database_msg_only('20',$earr1);												
																	unset($POST);
																	$POST['school_id']=$school->id; $POST['sms_type']='fee_reminder'; $POST['type']='sms'; $POST['on_date']=date('Y-m-d');
																	$POST['to_number']=$parents->mobile; $POST['sms_text']=$msg; $POST['student_id']=$student->id; 
																	
																	$QuerySave1 = new schoolSmsEmailHistory();												
																	$QuerySave1->saveData($POST);																
															endif;
											endif;
											
										endif;											
										endforeach;
									endif;
							endif;		
						endif;
					endforeach;
				endif;
		endif;
				
		echo 'Cron Job Done'; exit;
?>

