<?php	
        require_once("../../include/config_payment/config.php");
        include DIR_FS_SITE.'include/class/adminlog.php';
        include_once(DIR_FS_SITE.'include/functionClass/class.php');
        
        $include_fucntions=array('http','image_manipulation','calender','url_rewrite','email','video','users');
	include_functions($include_fucntions);
        
        /* check if already logged in */
        if($admin_user->is_logged_in()){
         
             /* Check User type */
            if(isset($_SESSION['admin_session_secure']['login_type']) && $_SESSION['admin_session_secure']['login_type']!='payment'){ 
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Sorry, You are already logged in as '.ucfirst($_SESSION['admin_session_secure']['login_type']).'.'); 
                    Redirect(DIR_WS_SITE.$_SESSION['admin_session_secure']['login_type'].'/index.php'); 
            }
           
        }
        
	if(!$admin_user->is_logged_in()):
		Redirect(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/index.php');
	endif;
        if($admin_user->get_permission() !='*'):
		$PageAccess = split("@@",$admin_user->get_permission());
	else:
		$PageAccess='*';
	endif;
        $Page =isset($_GET['Page'])?$_GET['Page']:"home";
	if($Page!='' && file_exists(DIR_FS_SITE.ADMIN_FOLDER.'/script/'.$Page.'.php')):
		if($PageAccess=='*' || in_array($Page, $PageAccess)):
			include(DIR_FS_SITE.ADMIN_FOLDER.'/script/'.$Page.'.php');
		endif;
	endif;
         
	include_once(DIR_FS_SITE.ADMIN_FOLDER.'/include/header.php');

        require_once(DIR_FS_SITE.ADMIN_FOLDER.'/include/'."left.php");
        
    
        /* Udpate admin log  - only update operations are logged*/
        if(count($_POST)){
            $adlog= new adminlog();
            $adlog->setMsg('Action '.$action.' performed on '. $id.' in '.$Page.' module');
        }
        
        
        
       
        
        if($Page !="")
        {
            if(file_exists(DIR_FS_SITE.ADMIN_FOLDER.'/form/'.$Page.".php")):
                    if($PageAccess=='*' || in_array($Page, $PageAccess)):
                            require_once(DIR_FS_SITE.ADMIN_FOLDER.'/form/'.$Page.".php");
                    else:
                            echo"<br><br><font size='3'><b>Welcome ".$admin_user->get_username()."!</b></font><br><br>";
                            echo "You do not have the permission to access this page.";
                    endif;
            else:
                     if($PageAccess=='*' || in_array($Page, $PageAccess)):
                            require_once(DIR_FS_SITE.ADMIN_FOLDER.'/form/default.php');
                     else:
                            echo "<font size='3'><br><br><b>Page is under construction....</b></font>";
                     endif;
            endif;
        }
   

require_once(DIR_FS_SITE.ADMIN_FOLDER.'/include/'."footer.php"); 


?>