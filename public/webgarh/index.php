<?php
        require_once("../../include/config_webgarh/config.php");

        $include_fucntions=array('http', 'image_manipulation','users');
		include_functions($include_fucntions);

			
		
        /* check if already logged in */
        if($admin_user->is_logged_in()){
		
             /* Check User type */
            if(isset($_SESSION['admin_session_secure']['login_type']) && $_SESSION['admin_session_secure']['login_type']!='webgarh'){ 
					
					//payment
					$admin_user->set_error();
                    $admin_user->set_pass_msg('Sorry, You are already logged in as '.ucfirst($_SESSION['admin_session_secure']['login_type']).'.'); 
                    Redirect(DIR_WS_SITE.$_SESSION['admin_session_secure']['login_type'].'/index.php'); 
            }
            else{	
			
                 redirect(make_admin_url('home', 'list', 'list')); 
            }        
            
          
        }  
           
        
         /* login user */
        if(is_var_set_in_post('login')):
            
		if($_POST['username']==WEBGARH_USER_NAME && $_POST['password']==WEBGARH_PASSWORD):
                  
                      
                            if(isset($_POST['remember'])){
                       
                                setcookie('webgarh',$user->id, time()+60*60*24*30);
                            }
                            
                            $user->user_id='1';
                            $user->allow_pages='*';
                            $user->user_type='admin';
                            $user->user_type='webgarh';
                            
                            $admin_user->set_admin_user_from_object($user);
                            //update_last_access($user->id, 1);
 
                            /* Set the login type */
                            $_SESSION['admin_session_secure']['login_type']='webgarh';
                        
                            Redirect1(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER."/control.php");
                      
		else:
                
            $admin_user->set_error();
			$admin_user->set_pass_msg(MSG_LOGIN_INVALID_USERNAME_PASSWORD);
                        
			//Redirect1(DIR_WS_SITE.ADMIN_PUBLIC_FOLDER.'/index.php');
		endif;
	endif;
 
      
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php echo SITE_NAME?> | Webgarh Control Panel
</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
        
        <!-- BEGIN PAGE LEVEL STYLES for validation -->
	
        <link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/validation/validationEngine.jquery.css" />
        
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
	<!-- BEGIN LOGO -->
	<div class="logo" style="color:white; font-weight:bold;font-size:20px">
		<br/><br/>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
                <form class="form-vertical login-form"  method="post" name="form_login" id="validation">
		
			<h3 class="form-title">Webgarh Control Panel</h3>
			 <div class="alert alert-error hide">
				<button class="close" data-dismiss="alert"></button>
				<span>Enter any username and password.</span>
			</div>
                       
                        <?php display_message(1); ?>
			<div class="control-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Username</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-user"></i>
                                                 <input class="m-wrap placeholder-no-fix validate[required]" type="text" id="username" name="username" placeholder="User Name" />
					
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-lock"></i>
                                                <input class="m-wrap placeholder-no-fix validate[required]" type="password" id="password" name="password" placeholder="Password"/>
						
					</div>
				</div>
			</div>
			<div class="form-actions">
				
				<button type="submit"  name="login"  value="Login" class="btn blue pull-right" id="login_button">
				    Login <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
                        <!--
			<div class="forget-password">
				<h4>Forgot your password ?</h4>
				<p>
					no worries, click <a href="javascript:void(0)" class="" id="forget-password">here</a>
					to reset your password.
				</p>
			</div>-->
			
		</form>
		<!-- END LOGIN FORM -->        
		<!-- BEGIN FORGOT PASSWORD FORM -->
		<form class="form-vertical forget-form" action="index.html">
			<h3 class="">Forget Password ?</h3>
			<p>Enter your e-mail address below to reset your password.</p>
			<div class="control-group">
				<div class="controls">
					<div class="input-icon left">
						<i class="icon-envelope"></i>
						<input class="m-wrap placeholder-no-fix" type="text" placeholder="Email" name="email" />
					</div>
				</div>
			</div>
			<div class="form-actions">
				<button type="button" id="back-btn" class="btn">
				<i class="m-icon-swapleft"></i> Back
				</button>
				<button type="submit" class="btn blue pull-right">
				Submit <i class="m-icon-swapright m-icon-white"></i>
				</button>            
			</div>
		</form>
		<!-- END FORGOT PASSWORD FORM -->
		
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright">
		<?php echo date('Y')?> &copy; <?php echo SITE_NAME?> - Admin Panel.
	</div>
	<!-- END COPYRIGHT -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
        
        <!-- Validation engine -->
        <script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/validation/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/validation/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/scripts/app.js" type="text/javascript"></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/scripts/login-soft.js" type="text/javascript"></script> 
         <script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/scripts/form-validation.js"></script> 
	<!-- END PAGE LEVEL SCRIPTS --> 
	<script>
		jQuery(document).ready(function() {     
		  App.init();
		  Login.init();
                  FormValidation.init();
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

