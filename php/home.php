<?php

include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/noticeBoardClass.php');

/*****************get navigation*********************************/
include_once(DIR_FS_SITE.'php/navigation.php');

/*****************Notice Board Contents*********************************/
$noteQuery=new noticeBoard();
$notice=$noteQuery->getSchoolNoticeBoardList($school->id,'1','array');

$smarty->assign_notnull('NOTICE',$notice,true);

/*******************seo information************************************/  

$content=add_metatags("Home","Home Page");

/******* End SEO information Section ***********************************/

$smarty->renderLayout();
?>
