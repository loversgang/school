<?php
include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');


/*****************get navigation*********************************/

include_once(DIR_FS_SITE.'php/navigation.php');

$p=isset($_GET['p'])?$_GET['p']:1;
$max_records='12';


	/*****************Staff Contents*********************************/

	$QueryStaff = new staff();
	$staff=$QueryStaff->getSchoolStaffFront($school->id,$p,$max_records);
//print_r($staff);
		//exit;
	$staff_array=$staff['staff'];
	$total_records=$staff['TotalRecords'];
	$total_pages=$staff['TotalPages'];

	$smarty->assign_notnull("STAFF",$staff_array,true);
	$smarty->assign_notnull("P",$p,true);
	$smarty->assign_notnull("MAX_RECORDS",$max_records,true);
	$smarty->assign_notnull("TOTAL_RECORDS",$total_records,true);
	$smarty->assign_notnull("TOTAL_PAGES",$total_pages,true);


	/*seo information*/  
	$content=add_metatags('Faculty & Staff','Faculty & Staff','Faculty & Staff');

	/******* End SEO information Section ***********************************/

	$smarty->renderLayout();
?>
