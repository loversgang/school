<?php
/*top navigation*/
include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/noticeBoardClass.php');
include_once(DIR_FS_SITE.'include/functionClass/galleryClass.php');

#School Information
$smarty->assign_notnull('SCHOOL',$school,true);

#About Us
$aboutObj=new content();
$about=$aboutObj->get_page_by_call_id('about-'.$school_id);
$smarty->assign_notnull("ABOUT",$about,true);

#infrastructure
$infrastructureObj=new content();
$infrastructure=$infrastructureObj->get_page_by_call_id('infrastructure-'.$school_id);
$smarty->assign_notnull("INFRASTRUCTURE",$infrastructure,true);

#awards
$awardsObj=new content();
$awards=$awardsObj->get_page_by_call_id('awards-'.$school_id);
$smarty->assign_notnull("AWARDS",$awards,true);

#info
$infoObj=new content();
$info=$infoObj->get_page_by_call_id('info-'.$school_id);
$smarty->assign_notnull("INFO",$info,true);
 
#admission
$admissionObj=new content();
$admission=$admissionObj->get_page_by_call_id('admission-'.$school_id); 
$smarty->assign_notnull("ADMISSION",$admission,true);

#Get Randomaly Staff
$staffObjQuery=new staff();
$randomStaff=$staffObjQuery->getSchoolStaffFrontRandomly($school_id,2); 
$smarty->assign_notnull("RANDOM_STAFF",$randomStaff,true);

#Notice Board Contents
$noteQuery=new noticeBoard();
$notice=$noteQuery->getSchoolNoticeBoardList($school->id,'1','array');
$smarty->assign_notnull('NOTICE',$notice,true);

#All Gallery Images Contents
$GalObjQuery=new gallery();
$Gallery_cont=$GalObjQuery->listSchoolGalleryImages($school->id,'1','array');
$smarty->assign_notnull('ALL_GALLERY',$Gallery_cont,true);

?>
