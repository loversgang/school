<?php
/*****************get navigation*********************************/

include_once(DIR_FS_SITE.'php/navigation.php');

if(isset($_GET['msg'])):
	$smarty->assign_notnull("MSG",$_GET['msg'],true);
endif;			
			
	
if(isset($_POST['submit'])): 

	#validate captcha
          if($_POST['captcha']==$_SESSION['captcha_sum']):  
			$data=MakeDataArray($_POST, array('submit', 'submit_x', 'submit_y','captcha'));
			$header='Contact Request';
			$center_content='';
			$subject=SITE_NAME.': Contact Request';
			$footer='Best Regards, '."<br/>".SITE_NAME;				
		
			$name=$_POST['name'];
			$email=$_POST['email'];
			$message=$_POST['message'];
			$footer='<br /><br /><b>Contact form submitted on:</b><br />'.SITE_NAME.'<br /><br />This email is triggered by the '.SITE_NAME.', when a visitor submitted the contact form located on this page. The form was submitted at <b>'.date("h:m:i A").'</b> on <b>'.date("d F, Y").'</b> from <b>'.$_SERVER['REMOTE_ADDR'].'</b> IP address.<br /><br /><br />&ldquo;Please do not reply to this email.&ldquo;<br /><br />Thank you -';


			include_once(DIR_FS_SITE.'include/email/general_front.php');
			$content=ob_get_contents();
			
			send_email($subject, $school->email_address, ADMIN_EMAIL, SITE_NAME, $content, BCC_EMAIL, 'html');
			Redirect(make_url('contact&msg=Your request has been submitted successfully'));
		else:			
			Redirect(make_url('contact&msg=The entered code was not correct. Please try again.'));		 
		endif; 			
		 
endif;



$string="";
for ($i = 0; $i < 5; $i++) 
	{
	$string .= chr(rand(97, 122));
	}
$_SESSION['captcha_sum'] =$string;

$smarty->assign_notnull("CAPTCHA",DIR_WS_SITE_PATH."captcha/captcha_code_file.php",true);

$randNo=rand();
$smarty->assign_notnull("RAND",$randNo,true);

/*******************seo information************************************/  

$content=add_metatags('Contact Us','Contact Us','Contact Us');
/******* End SEO information Section ***********************************/

$smarty->renderLayout();
?>
