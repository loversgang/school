<?php
include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/noticeBoardClass.php');


/*****************get navigation*********************************/

include_once(DIR_FS_SITE.'php/navigation.php');

/*************get right panel data starts here**************************/


/*******************seo information************************************/  


$id=$_GET['id'];

/*get Notice Detail*/
$noteQuery=new noticeBoard();
$notice=$noteQuery->getNoticeBoard($id);
$smarty->assign_notnull("NOTICE_DETAIL",$notice,true);

 /*seo information*/  
$content=add_metatags("Notice Board | ".$notice->name,$notice->meta_keyword,$notice->meta_description);
/******* End SEO information Section ***********************************/

$smarty->renderLayout();
?>
