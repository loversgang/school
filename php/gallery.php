<?php
include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/galleryClass.php');


/*****************get navigation*********************************/

include_once(DIR_FS_SITE.'php/navigation.php');

$p=isset($_GET['p'])?$_GET['p']:1;
$max_records='6';


	/*****************Gallery Contents*********************************/
	$QueryGal = new gallery();
	$gallery=$QueryGal->getGalleries($school->id,$p,$max_records);
	
	$gallery_array=$gallery['gallery'];
	$total_records=$gallery['TotalRecords'];
	$total_pages=$gallery['TotalPages'];

	$smarty->assign_notnull("GALLERY",$gallery_array,true);
	$smarty->assign_notnull("P",$p,true);
	$smarty->assign_notnull("MAX_RECORDS",$max_records,true);
	$smarty->assign_notnull("TOTAL_RECORDS",$total_records,true);
	$smarty->assign_notnull("TOTAL_PAGES",$total_pages,true);


	/*seo information*/  
	$content=add_metatags('Gallery','Gallery','Gallery');

	/******* End SEO information Section ***********************************/

	$smarty->renderLayout();
?>
