<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<ul class="page-sidebar-menu">
				<li style="margin-bottom:10px;">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				
				<li class="start <?php echo ($Page=='home')?'active':''?> ">
					<a href="<?php echo make_admin_url('home', 'list', 'list');?>">
					<i class="icon-home"></i> 
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
				
				
				<li class="<?php echo ($Page=='school')?'active':''?>">
                        <?php include_once(DIR_FS_SITE.ADMIN_FOLDER.'/left/client.php');?>
				</li>
				
   
			
				<li class="<?php echo ($Page=='logout')?'active':''?>">
					<a href="<?php echo make_admin_url('logout', 'list', 'list');?>">
					<i class="icon-lock"></i> 
					<span class="title">Logout</span>
					<span class="selected"></span>
					</a>
				</li>				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
                
		<!-- BEGIN PAGE -->
		<div class="page-content">
   
   