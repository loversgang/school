
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                     Invoice
                            </h3>
							<div class="hidden-print" >
								<!-- END PAGE HEADER-->
								<div class="clearfix"></div>
									<div class="tiles pull-right">
											<div class="tile bg-purple <?php echo ($section=='invoice')?'selected':''?>">
												<a href="<?php echo make_admin_url('school', 'payment', 'payment&id='.$id);?>">
													<div class="corner"></div>
													<div class="tile-body"><i class="icon-arrow-left"></i></div>
													<div class="tile-object"><div class="name">Back To Payment</div></div>
												</a> 
											</div>
											<div class="tile bg-yellow <?php echo ($section=='print')?'selected':''?>" id='print_document'>							
												<a class="hidden-print" onclick="javascript:window.print();">
													<div class="corner"></div>
													<div class="tile-body"><i class="icon-print"></i></div>
													<div class="tile-object"><div class="name">Print Invoice</div></div>
												</a>
											</div>							
									
										<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut_payment.php');?>
									</div>
								<div class="clearfix"></div>
							</div>	
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
            
           

           <div class="clearfix"></div>
			 
			<div class="row-fluid invoice">
					<div class="row-fluid invoice-logo">
						<div class="span6 invoice-logo-space"><img src="assets/img/invoice/webgarh.png" alt=""> </div>
					</div>
					<hr>
					<div class="row-fluid">
						<div class="span7">
							<h4>Client:</h4>
							<ul class="unstyled">
								<li><?=$school->school_name?></li>
								<li><?=$school->address1?>,</li>
								<li><?=$school->address2?>,</li>
								<li><?=$school->city?>, <?=$school->district?>,</li>
								<li>Phone: <?=$school->phone1?></li>								
							</ul>
						</div>
						
						
						<div class="span3 invoice-payment">
							<h4>Payment Details:</h4>
							<ul class="unstyled">
								<li><strong>Invoice #:</strong> <?=$invoice->id?></li>
								<li><strong>Date:</strong> <?=$invoice->payment_date?></li>
								<li><strong>Amount:</strong> <?=CURRENCY_SYMBOL." ".number_format($invoice->amount,2)?> </li>
								
							</ul>
						</div>
					</div><br/>
					<div class="row-fluid">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Sr. No.</th>
									<th>Payment Date</th>
									<th class="hidden-480">Payment Type</th>
									<th>Amount (₹)</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1.</td>
									<td><?=$invoice->payment_date?></td>
									<td class="hidden-480"><?=ucfirst($invoice->payment_type)?></td>
									<td class="hidden-480"><?=CURRENCY_SYMBOL." ".number_format($invoice->subscription_price,2)?></td>	
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>Set-up Fee</td>
									<td class="hidden-480"><?=CURRENCY_SYMBOL." ".number_format($invoice->set_up_fee,2)?></td>																		
								</tr>	
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>Subtotal</td>
									<td class="hidden-480"><?=CURRENCY_SYMBOL." ".number_format($invoice->amount,2)?></td>																		
								</tr>	
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td class='bold_total'>Total</td>
									<td class="hidden-480 bold_total"><?=CURRENCY_SYMBOL." ".number_format($invoice->amount,2)?></td>																		
								</tr>	
							</tbody>
						</table>
					</div><br/><br/>
					<div class="row-fluid">
						<div class="span4">
							<div class="well">
								<address>
									<strong><?=SITE_NAME?></strong><br>
									<?=ADDRESS1?><br>
									<?=ADDRESS2?><br>
									<?=CITY?><br>
									<abbr title="Phone">P:</abbr> <?=PHONE_NUMBER?><br/>
									<?=SUPPORT_EMAIL?>
								</address>
							</div>
						</div>
						<div class="span8" style='margin-top: 65px; text-align: right; padding-right: 64px;'><a class="btn blue big hidden-print" onclick="javascript:window.print();">Print <i class="icon-print icon-big"></i></a></div>
					</div>
					
				</div>
    <!-- END PAGE CONTAINER-->    
 