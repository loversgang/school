
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Clients > <?=$school->school_name.', '.$school->address1?>
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li><a href="<?php echo make_admin_url('school', 'list', 'list');?>">List Clients<i class="icon-angle-right"></i></a></li>
									<li>Client Setting</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut_payment.php');?>
				</div>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<form class="form-horizontal" action="<?php echo make_admin_url('school', 'setting', 'setting&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                        <div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-cogs"></i><?=$school->school_name.', '.$school->address1?> > Settings</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body form">  
							<? if(is_object($setting)):?>
							<!-- Update Case -->	
									<!-- School Limits  -->
										<h4 class="form-section alert alert-info"><strong>Client Limits</strong></h4>
										<div class="row-fluid">
										<div class="span12" id='large_space'>
						                    <div class="control-group">
                                                    <label class="control-label" for="payment_type">Client<span class="required">*</span></label>
                                                    <div class="controls">
                                                          <input type="text" disabled='' id="payment_type" value='<?=$school->school_name.", ".$school->address1?>' readonly class="span6 m-wrap validate[required]"/>
													</div>
                                            </div>  				
                                            <div class="control-group">
                                                    <label class="control-label" for="max_courses_allowed">Maximum Courses<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled='' name="max_courses_allowed" value="<?=$setting->max_courses_allowed?>" id="max_courses_allowed" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="max_sections_per_session">Maximum Sections Per Course<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled='' name="max_sections_per_session" value="<?=$setting->max_sections_per_session?>" id="max_sections_per_session" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                    </div>
                                            </div>											
                                            <div class="control-group">
                                                    <label class="control-label" for="max_teacher_allowed">Maximum Number of Teachers<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled='' name="max_teacher_allowed" value="<?=$setting->max_teacher_allowed?>" id="max_teacher_allowed" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="max_student_per_session">Maximum Students Per Session<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled='' name="max_student_per_session" value="<?=$setting->max_student_per_session?>" id="max_student_per_session" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                    </div>
                                            </div>	
										</div>
										</div>
										
									<!-- SMS Setting  -->
										<h4 class="form-section alert alert-info"><strong>SMS Settings</strong></h4>
										<div class="row-fluid">
										<div class="span12" id='large_space'>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_sms_allowed">SMS Allowed OR Not<span class="required">*</span></label>
                                                    <div class="controls">
                                                       	<label class="checkbox line">
														<input type="checkbox" disabled='' name="is_sms_allowed"  value="1" <?=($setting->is_sms_allowed=='1')?'checked':'';?>/>
														</label>
                                                    </div>
                                            </div>										
                                            <div class="control-group">
                                                    <label class="control-label" for="max_sms_limit">Maximum Number of SMS's Allowed</label>
                                                    <div class="controls">
                                                      <input type="text" disabled='' name="max_sms_limit" value="<?=$setting->max_sms_limit?>" id="max_sms_limit" class="span10 m-wrap"/>
                                                    </div>
                                            </div>
											 
                                            <div class="control-group">
                                                    <label class="control-label" for="user_name">SMS's</label>
													<div class="controls">
														<label class="checkbox line">
														<input type="checkbox" disabled='' name="sms[admission]"  value="1" <? if(in_array('admission',$smsArray)) { echo 'checked';}?>/>New Admission
														</label>
														<label class="checkbox line">
														<input type="checkbox" disabled='' name="sms[monthly_report]"  value="1" <? if(in_array('monthly_report',$smsArray)) { echo 'checked';}?>/>Monthly Reports
														</label>
														<label class="checkbox line">
														<input type="checkbox" disabled='' name="sms[present]"  value="1" <? if(in_array('present',$smsArray)) { echo 'checked';}?>/>Present Attendance - Daily
														</label>
														<label class="checkbox line">
														<input type="checkbox" disabled='' name="sms[absent]"  value="1" <? if(in_array('absent',$smsArray)) { echo 'checked';}?>/>Absent From School
														</label>
														<label class="checkbox line">
														<input type="checkbox" disabled=''  name="sms[fee_deposit]"  value="1" <? if(in_array('fee_deposit',$smsArray)) { echo 'checked';}?>/>Fee Deposit
														</label>
														<label class="checkbox line">
														<input type="checkbox" disabled='' name="sms[fee_pending]"  value="1" <? if(in_array('fee_pending',$smsArray)) { echo 'checked';}?>/>Fee Pending
														</label>
														<label class="checkbox line">
														<input type="checkbox" disabled='' name="sms[birthday]"  value="1" <? if(in_array('birthday',$smsArray)) { echo 'checked';}?>/>Birthday Wishes
														</label>
														<label class="checkbox line">
														<input type="checkbox" disabled='' name="sms[holiday]"  value="1"<? if(in_array('holiday',$smsArray)) { echo 'checked';}?> />Holiday
														</label>
													</div>
                                            </div>
										</div>
										</div>										
										
									
		                                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
                                            <div class="form-actions">
														 <a href="<?php echo make_admin_url('school', 'list', 'list');?>" class="btn" name="cancel" > Go Back</a>
                                             </div>
                                     <?php endif;?>
							<? else:?>
							<!-- Insert Case -->
									<!-- School Limits  -->
										<h4 class="form-section alert alert-info"><strong>Client Limits</strong></h4>
										<div class="row-fluid">
										<div class="span12" id='large_space'>
						                    <div class="control-group">
                                                    <label class="control-label" for="payment_type">Client<span class="required">*</span></label>
                                                    <div class="controls">
                                                          <input type="text" id="payment_type" value='<?=$school->school_name.", ".$school->address1?>' readonly class="span6 m-wrap validate[required]"/>
													</div>
                                            </div>  				
                                            <div class="control-group">
                                                    <label class="control-label" for="max_courses_allowed">Maximum Courses Per Session<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="max_courses_allowed" value="" id="max_courses_allowed" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="max_sections_per_session">Maximum Sections Per Session<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="max_sections_per_session" value="" id="max_sections_per_session" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                    </div>
                                            </div>											
                                            <div class="control-group">
                                                    <label class="control-label" for="max_teacher_allowed">Maximum Number of Teachers<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="max_teacher_allowed" value="" id="max_teacher_allowed" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="max_student_per_session">Maximum Students Per Session<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="max_student_per_session" value="" id="max_student_per_session" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                    </div>
                                            </div>	
										</div>
										</div>
										
									<!-- SMS Setting  -->
										<h4 class="form-section alert alert-info"><strong>SMS Settings</strong></h4>
										<div class="row-fluid">
										<div class="span12" id='large_space'>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_sms_allowed">SMS Allowed OR Not<span class="required">*</span></label>
                                                    <div class="controls">
                                                       	<label class="checkbox line">
														<input type="checkbox" name="is_sms_allowed"  value="1" />
														</label>
                                                    </div>
                                            </div>										
                                            <div class="control-group">
                                                    <label class="control-label" for="max_sms_limit">Maximum Number of SMS's Allowed</label>
                                                    <div class="controls">
                                                      <input type="text" name="max_sms_limit" id="max_sms_limit" class="span10 m-wrap"/>
                                                    </div>
                                            </div>
											 
                                            <div class="control-group">
                                                    <label class="control-label" for="user_name">SMS's</label>
													<div class="controls">
														<label class="checkbox line">
														<input type="checkbox" name="sms[admission]"  value="1" />New Admission
														</label>
														<label class="checkbox line">
														<input type="checkbox" name="sms[monthly_report]"  value="1" />Monthly Reports
														</label>
														<label class="checkbox line">
														<input type="checkbox" name="sms[absent]"  value="1" />Absent From School
														</label>
														<label class="checkbox line">
														<input type="checkbox" name="sms[fee_deposit]"  value="1" />Fee Deposit
														</label>
														<label class="checkbox line">
														<input type="checkbox" name="sms[fee_pending]"  value="1" />Fee Pending
														</label>
														<label class="checkbox line">
														<input type="checkbox" name="sms[birthday]"  value="1" />Birthday Wishes
														</label>
														<label class="checkbox line">
														<input type="checkbox" name="sms[holiday]"  value="1" />Holiday
														</label>
													</div>
                                            </div>
										</div>
										</div>										
										
									<!-- Other Setting  -->
									<!--
										<h4 class="form-section alert alert-info"><strong>Payment Reminder Settings</strong></h4>
										<div class="row-fluid">
										<div class="span12" id='large_space'>
	                                            <div class="control-group">
                                                    <label class="control-label" for="state">Payment Reminder Status<span class="required">*</span></label>
                                                    <div class="controls">
														<label class="checkbox line">
														<input type="checkbox" name="is_active"  value="1" />
														</label>
                                                    </div>
												</div>  											
											</div>
										</div>
									-->	
		                                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
                                            <div class="form-actions">
														<input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" /> 
														<input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('school', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                             </div>
                                     <?php endif;?>							
							
							<? endif;?>							
							</div>
							
						</div>
						
                        </form>	                      
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

