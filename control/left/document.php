<a href="javascript:;">
<i class="icon-file-text"></i> 
<span class="title">Manage Documents</span>
<span class="arrow <?=($Page=='document')?'open':''?>"></span>
</a>
<ul class="sub-menu">
        <li class="<?php echo ($Page=='document' && ($section=='list' || $section=='view'))?'active':''?>">
                <a href="<?php echo make_admin_url('document', 'list', 'list');?>">
                All Documents</a>
        </li>
		<!--
        <li class="<?php echo ($Page=='document' && $section=='insert')?'active':''?>">
                <a href="<?php echo make_admin_url('document', 'list', 'insert');?>">
                New Document</a>
        </li>
		-->
</ul>