<a href="javascript:;">
<i class="icon-building"></i> 
<span class="title">Manage Institute Type</span>
<span class="arrow <?=($Page=='type')?'open':''?>"></span>
</a>
<ul class="sub-menu">
        <li class="<?php echo ($Page=='type' && $section=='list' || $section=='thrash')?'active':''?>">
                <a href="<?php echo make_admin_url('type', 'list', 'list');?>">
                All Types</a>
        </li>
        <li class="<?php echo ($Page=='type' && $section=='insert')?'active':''?>">
                <a href="<?php echo make_admin_url('type', 'list', 'insert');?>">
                New Type</a>
        </li>
</ul>