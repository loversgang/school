<a href="javascript:;">
<i class="icon-book"></i> 
<span class="title">Manage Subjects</span>
<span class="arrow <?=($Page=='subject')?'open':''?>"></span>
</a>
<ul class="sub-menu">
        <li class="<?php echo ($Page=='subject' && $section=='list')?'active':''?>">
                <a href="<?php echo make_admin_url('subject', 'list', 'list');?>">
                All Subjects</a>
        </li>
        <li class="<?php echo ($Page=='subject' && $section=='insert')?'active':''?>">
                <a href="<?php echo make_admin_url('subject', 'list', 'insert');?>">
                New Subjects</a>
        </li>
</ul>