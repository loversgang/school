<?php
include_once(DIR_FS_SITE.'include/functionClass/servicesClass.php');

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';







	#update all school data to webgarh school

		$QueryObj = new query('school');
		$QueryObj->Field=('id,create_date');
		$QueryObj->Where="where school.is_deleted='0'";
		$record=$QueryObj->ListOfAllRecords('object');
		
		if(!empty($record)):
			foreach($record as $key=>$val):
				$QueryIns = new query('webgarh_school');
				$QueryIns->Data['school_id']=$val->id;
				$QueryIns->Data['monthly_fee']=WEBGARH_AMOUNT; 
				$QueryIns->Data['signup_date']=$val->create_date;
				$QueryIns->Data['on_date']=date('Y-m-d');
				$QueryIns->Insert();
			endforeach;
		endif;
		echo 'job done';
		exit;









#handle actions here.
switch ($action):
	case'list':
		$QueryObj = new services();
		$QueryObj->listServices();
		break;
	case'insert':
		if(isset($_POST['submit'])):
                    $QueryObj = new services();
                    $QueryObj->saveServices($_POST);
                    $admin_user->set_pass_msg('Services has been inserted successfully.');
                    Redirect(make_admin_url('services', 'list', 'list'));
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('services', 'list', 'list'));    
		endif;
		break;
	case'update':
		$QueryObj = new services();
		$service=$QueryObj->getServices($id);                
		if(isset($_POST['submit'])):
                    $QueryObj = new services();
                    $QueryObj->saveServices($_POST);
                    $admin_user->set_pass_msg('Services has been updated successfully.');
                    Redirect(make_admin_url('services', 'list', 'list'));
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('services', 'list', 'list'));       
		endif;
		break;
	case 'update2':
                if(is_var_set_in_post('submit_position')):
                    
                    foreach ($_POST['position'] as $k=>$v):
                            $q= new query('services');
                            $q->Data['id']=$k;
                            $q->Data['position']=$v;
                            $q->Update();
                    endforeach;
                endif;
                if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):
                    if($_POST['multiopt_action']=='delete'):
                        foreach($_POST['multiopt'] as $k=>$v):
                                $query= new query('services');
                                $query->id="$k";
                                $query->Delete();
                        endforeach;
                    endif;
                endif;     
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('services', 'list', 'list', 'id='.$id.'&page='.$page));
                break;
	 case'delete':
		$QueryObj = new services();
		$QueryObj->deleteServices($id);
		$admin_user->set_pass_msg('Services has been deleted successfully');
		Redirect(make_admin_url('services', 'list', 'list'));
		break;
	case 'delete_image':
                if($id){
                    $object= get_object('services', $id);
                    $QueryObj= new query('services');
                    $QueryObj->Data['image']='';
                    $QueryObj->Data['id']=$id;
                    $QueryObj->Update();
                    
                    #delete images from folder
                    @unlink(DIR_FS_SITE_UPLOAD.'photo/services/large/'.$object->image);
                    @unlink(DIR_FS_SITE_UPLOAD.'photo/services/medium/'.$object->image);
                    @unlink(DIR_FS_SITE_UPLOAD.'photo/services/thumb/'.$object->image);
                    
                    $admin_user->set_pass_msg('Services Image has been successfully deleted.');
                    Redirect(make_admin_url('services', 'update', 'update','id='.$id));
                }        
	default:break;
endswitch;
?>
