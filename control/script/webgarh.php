<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/adminUserClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subscriptionPlanClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSubscriptionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/paymentHistoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSettingClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/themeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');

$permitted_school=explode(',',$login_user->list_clients);


$modName='webgarh';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['p_id'])?$p_id=$_GET['p_id']:$p_id='0';

$SubObj = new subscriptionPlan();
$SubObj->listAll(1);

#get School Types
$QueryS_type=new schoolType;
$QueryS_type->listAll();

#get Active Documents
$QueryDoc = new document();
$activeDoc=$QueryDoc->listUnderCatName('1');


switch ($action):
	case'list':
		if($login_user->type=='Affiliate' || $login_user->type=='Payment'):
			 $QueryObj = new school();
			 $record=$QueryObj->getSchoolWithPlanForWebgarh();	
		else:
			 $QueryObj = new school();
			 $record=$QueryObj->getSchoolWithPlanForWebgarh();	
		endif;
		
	break;
	
	case'insert': 
			if(isset($_POST['submit'])):			
				
				#get max id for the school unique id
				$QueryUn = new school();
				$Unique_id=$QueryUn->get_last_unique_id();
				
				/* Add New School */
				$_POST['client_id']=$Unique_id;
				$QueryObj = new school();
				$school_id=$QueryObj->saveData($_POST);
				
				$_POST['subscription']['school_id']=$school_id;
				
				/* Add Subscription */
				$QbSub= new schoolSubscription();
				$QbSub->saveData($_POST['subscription']);
				
				/* Save the predefined pages for school if not added */	
				$QueryPages = new content();
				$QueryPages->saveNeccessaryPage($pre_defined_pages,$school_id);	
					
				/* Save School default SMS */
				$_POST['sms']=array('admission'=>1,'monthly_report'=>1,'present'=>1,'absent'=>1,'fee_deposit'=>1,'fee_pending'=>1,'birthday'=>1,'holiday'=>1,'exam_result'=>1);		

				if(!empty($_POST['sms'])):
					#Save SMS TYPE
					$QuerySms = new schoolSms();
					$QuerySms->saveData($_POST['sms'],$school_id);
					
					#Save Emails
					$QueryEmail = new schoolEmail();
					$QueryEmail->saveData($_POST['sms'],$school_id);					
				endif;
				
				/* Save Document_rel */	
				$_POST['doc']=array(2,16,22,26,28,27,29,30,33,34);		
				
				if(!empty($_POST['doc'])):
					$QuerySms = new SchoolDocumentTemplate();
					$QuerySms->saveData($_POST['doc'],$school_id);
				endif;	
				
				/* Save School default Settings */	
				
				$_POST['max_courses_allowed']=100;
				$_POST['max_sections_per_session']=5;
				$_POST['max_teacher_allowed']=100;
				$_POST['max_student_per_session']=100;
				$_POST['is_sms_allowed']=1;
				$_POST['is_email_allowed']=1;
				$_POST['max_sms_limit']=10000;
				$_POST['max_email_limit']=100000;
				$_POST['school_id']=$school_id;
				$_POST['reg_id_prefix']=get_school_reg_prefix($_POST['school_name'],$school_id);				
							
				$QueryObj = new schoolSetting();
				$QueryObj->saveData($_POST);	

				#if new client created by afilate user
				if($login_user->type!='Administrator'):
					$all_school=explode(',',$login_user->list_clients);
						array_push($all_school,$school_id);
						unset($_POST);
						$_POST['id']=$login_user->id;
						$_POST['email']=$login_user->email;
						$_POST['is_active']='1';
						$_POST['list_clients']=$all_school;
	                    $QueryObjUser = new admin();
						$QueryObjUser->saveAdminUser($_POST);				
				endif;
				
				$admin_user->set_pass_msg(MSG_CLIENT_ADDED);
				Redirect(make_admin_url('school', 'list', 'list'));				
			endif;
		break;
       
	case'update':
			#check for affiliate can see only our own school.
			if($login_user->type!='Administrator'):
				if(!empty($id) && !in_array($id,$permitted_school)):
						Redirect(make_admin_url('error', 'list', 'list'));	
				endif;
			endif;
		
					
				/* Get School with plan*/				
				$Query = new school();
				$webgarh=$Query->getSchoolWithPlan($id);	
			
			if(isset($_POST['submit'])):
				/* Save the predefined pages for school if not added */	
				$QueryPages = new content();
				$QueryPages->saveNeccessaryPage($pre_defined_pages,$id);	
				
				/* Edit School */				
				$QueryObj = new school();
				$school_id=$QueryObj->saveData($_POST);
				
				$_POST['subscription']['school_id']=$school_id;
				
				/* Edit Subscription */
				$QbSub= new schoolSubscription();
				$QbSub->saveData($_POST['subscription']);
				
				$admin_user->set_pass_msg(MSG_CLIENT_UPDATE);
				Redirect(make_admin_url('school', 'list', 'list'));				
			endif;	
		break;
	case'delete_image':
			#delete school logo
			$QueryObj = new school();
			$QueryObj->deleteImage($id);
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('school', 'update', 'update&id='.$id));	
		break;		
		
	case'delete':
			#soft delete
			$QueryObj = new school();
			$QueryObj->deleteRecord($id);
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('school', 'list', 'list'));
		break;
            
        case'permanent_delete': 
			#delete school 
			$QueryObj = new school();
			$QueryObj->deleteCompleteSchool($id);
		
			
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('school', 'thrash', 'thrash'));
        	break;
          
        case'restore':
			#restore school
            $QueryObj = new school();
            $QueryObj->restoreObject($id);
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('school', 'list', 'list'));
            break;
			
        case'payment': 
			#check for affiliate can see only our own school.
			if($login_user->type!='Administrator'):
				if(!empty($id) && !in_array($id,$permitted_school)):
						Redirect(make_admin_url('error', 'list', 'list'));	
				endif;
			endif;		
		
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);
			
			
			/* Get School No of payment till now */				
			$QueryPay = new school();
			$no_payment=$QueryPay->getSchoolNoPayment($school->create_date,date('Y-m-d'),$school->payment_type);				
			
			#get previous payments of school
			$pay=new paymentHistory();
			$prevous_payment=$pay->get_total_school_payments($id);

			#total_payment	
			$total_payment=(($no_payment*$school->subscription_fee)+$school->set_up_fee);	
				
			#Total pending fee	
			$total_pending=($total_payment-$prevous_payment);			
			
			#list Payment History of a school
            $QueryObj = new paymentHistory();
            $QueryObj->listAll($id);

            break;
			
        case'setting':		
			#check for affiliate can see only our own school.
			if($login_user->type!='Administrator'):
				if(!empty($id) && !in_array($id,$permitted_school)):
						Redirect(make_admin_url('error', 'list', 'list'));	
				endif;
			endif;
			
			$smsArray=array();
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);	
			
			/* Get School setting*/				
			$QuerySchool = new schoolSetting();
			$setting=$QuerySchool->getSchoolSetting($school->id);	

			/* Get School sms array*/				
			$QuerySchool = new schoolSms();
			$smsArray=$QuerySchool->getSchoolSmsArray($school->id);
			
			/* Get School Emails array*/				
			$QuerySchoolEmail = new schoolEmail();
			$emailArray=$QuerySchoolEmail->getSchoolEmailArray($school->id);			
			
			/* Get School document tamplate array */				
			$QuerySchDoc = new SchoolDocumentTemplate();
			$DocTemp=$QuerySchDoc->getSchoolDocumentTemplate($school->id);			
			
			
				if(isset($_POST['submit'])): 
					
					/* Check School Prefix */
					$_POST['reg_id_prefix']=strtoupper($_POST['reg_id_prefix']);					
					$QueryObj = new schoolSetting();
					$rec=$QueryObj->checkSchoolPrefix($id,$_POST['reg_id_prefix']);
					if(is_object($rec)):
						$admin_user->set_error();  
						$admin_user->set_pass_msg(MSG_REG_ID_EXIST);
						Redirect(make_admin_url('school', 'setting', 'setting&id='.$id));   					
					endif;
					
					$_POST['reg_id_prefix']=strtoupper(str_replace(' ','',$_POST['reg_id_prefix']));
				
					/* Delete all School Template Relation first */	
					$QuerySmsDel = new SchoolDocumentTemplate();
					$QuerySmsDel->deleteSchoolRecord($_POST['school_id']);	
					
					/* Save Document_rel */	
					if(!empty($_POST['doc'])):
						$QuerySms = new SchoolDocumentTemplate();
						$QuerySms->saveData($_POST['doc'],$_POST['school_id']);
					endif;				
				
				
					/* Delete all SMS of this school first */	
					$QuerySmsDel = new schoolSms();
					$QuerySmsDel->deleteSchoolRecord($_POST['school_id']);	
					
					/* Delete all Emails of this school first */	
					$QueryEmailDel = new schoolEmail();
					$QueryEmailDel->deleteSchoolRecord($_POST['school_id']);						
					
					/* Save SMS */	
					if(!empty($_POST['sms'])):
						$QuerySms = new schoolSms();
						$QuerySms->saveData($_POST['sms'],$_POST['school_id']);
					endif;

					/* Save Email */	
					if(!empty($_POST['email'])):
						$QuerySms = new schoolEmail();
						$QuerySms->saveData($_POST['email'],$_POST['school_id']);
					endif;					
				
					/* Save Settings */				
					$QueryObj = new schoolSetting();
					$QueryObj->saveData($_POST);
					$admin_user->set_pass_msg(MSG_CLIENT_SETTING_UPDATE);
					Redirect(make_admin_url('school', 'setting', 'setting&id='.$id));					
				endif;
            break;
			
        case'website':
			#check for affiliate can see only our own school.
			if($login_user->type!='Administrator'):
				if(!empty($id) && !in_array($id,$permitted_school)):
						Redirect(make_admin_url('error', 'list', 'list'));	
				endif;
			endif;		
		
			/* Get All Themes*/				
			$QueryTheme = new theme();
			$All_theme=$QueryTheme->listThemes('1');			
			

			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);	
			
				if(isset($_POST['submit'])):  
					if(!isset($_POST['theme'])):
						$_POST['theme']='';						
						if (is_dir(DIR_FS_SITE_PUBLIC.'site/'.$id)): 
							unlink(DIR_FS_SITE_PUBLIC.'/site/'.$id.'/index.php');
							unlink(DIR_FS_SITE_PUBLIC.'site/'.$id.'/.htaccess');							
							unlink(DIR_FS_SITE_PUBLIC.'site/'.$id.'/error_log');		
							@rmdir(DIR_FS_SITE_PUBLIC.'site/'.$id);							
					   endif;
					   
					   if($_POST['is_website']=='1'):	
							$_POST['is_website']='0';					   
							$QueryObj = new school();
							$QueryObj->saveData($_POST);					   
					   
							$admin_user->set_error();
							$admin_user->set_pass_msg(MSG_SELECT_WEBSITE);
							Redirect(make_admin_url('school', 'website', 'website&id='.$id));
						else:
							$_POST['is_website']='0';					   
							$QueryObj = new school();
							$QueryObj->saveData($_POST);							
					   endif;
					else:
						if (!is_dir(DIR_FS_SITE_PUBLIC.'site/'.$id)):
							@mkdir(DIR_FS_SITE_PUBLIC.'site/'.$id,0755);
							chmod(DIR_FS_SITE_PUBLIC.'site/'.$id, 0755);  // octal; correct value of mode
							copy(DIR_FS_SITE_PUBLIC.'new/index.php', DIR_FS_SITE_PUBLIC.'site/'.$id.'/index.php');
							copy(DIR_FS_SITE_PUBLIC.'new/.htaccess', DIR_FS_SITE_PUBLIC.'/site/'.$id.'/.htaccess');
					   endif;					
					endif;
					/* Save website data */				
					$QueryObj = new school();
					$QueryObj->saveData($_POST);	
					$admin_user->set_pass_msg(MSG_CLIENT_WEBSITE_UPDATE);
					Redirect(make_admin_url('school', 'website', 'website&id='.$id));					
				endif;
            break;
			
        
			
        case'invoice': 
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);	
			
			/* Get Invoice detail*/				
			$QueryInvoice = new paymentHistory();
			$invoice=$QueryInvoice->getPaymentRecord($p_id);				

            break;			
        case'download':
			#download invoice 	
					Redirect(make_admin_url_window('ajax_calling','download','download','temp=school&id='.$id.'&p_id='.$p_id));
		
			break;			
        case'delete_payment':
			#delete payment 			
					$QueryObj = new paymentHistory();
					$QueryObj->deleteRecord($_GET['p_id']);		
					$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
					Redirect(make_admin_url('school', 'payment', 'payment&id='.$id));			
			break;
			
        case'thrash':
			#list thrash

				if($login_user->type=='Affiliate' || $login_user->type=='Payment'):
					$QueryObj = new school();
					$QueryObj->getThrashWithAfilate($login_user->list_clients);
				else:
					$QueryObj = new school();
					$QueryObj->getThrash();	
				endif;			
			
            break;
	default:break;
endswitch;
?>
