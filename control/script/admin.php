<?php
include_once(DIR_FS_SITE.'include/functionClass/adminUserClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolClass.php');

$modName='admin';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';
isset($_POST['business_email']) ? $email = $_POST['business_email'] : $email = '';  
isset($_POST['username']) ? $username = $_POST['username'] : $username = '';  
/*#handle actions here.*/

#get School List
if($login_user->type=='Affiliate'):
	 $QueryObjSchool = new school();
	 $QueryObjSchool->listAllWithAfilate($login_user->list_clients,'1');
else:
	 $QueryObjSchool = new school();
	 $QueryObjSchool->listAll('1');	
endif;
		
switch ($action):
	case'list':
		if($login_user->type=='Affiliate'):
			$QueryObj = new admin();
			$QueryObj->listAdminUsers($login_user->id);
		else:
			$QueryObj = new admin();
			$QueryObj->listAdminUsers();		
		endif;
		break;
	case'insert':
		if(isset($_POST['submit'])):
                    $QueryObj11 = new admin();
		    $QueryObj11->checkUsers($username, $email);
                    if($QueryObj11->GetNumRows()):
                         $admin_user->set_error();   
                         $admin_user->set_pass_msg('Sorry,User with this Email Id Or Username already exists.');
                         Redirect(make_admin_url('admin', 'list', 'insert'));    
                    else:
                         $QueryObj = new admin();
                         $QueryObj->saveAdminUser($_POST);
                         $admin_user->set_pass_msg('New admin user has been created successfully.');
                         Redirect(make_admin_url('admin', 'list', 'list'));  
                    endif;
                    
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();   
                    $admin_user->set_pass_msg('The operation has been cancelled.');
                    Redirect(make_admin_url('admin', 'list', 'list'));    
		endif;
		break;
	case'update':
		$QueryObj = new admin();
		$page_cotent=$QueryObj->getAdminUser($id);                
		
		if(isset($_POST['submit'])):
                    $QueryObj11 = new admin();
					$QueryObj11->checkUsersWithID($username, $email,$_POST['id']);
                    if($QueryObj11->GetNumRows()):
                         $admin_user->set_error();   
                         $admin_user->set_pass_msg('Sorry,User with this Email Id Or Username already exists.');
                         Redirect(make_admin_url('admin', 'update', 'update','id='.$_POST['id']));   
                    else:
                        $QueryObj = new admin();
                        $QueryObj->saveAdminUser($_POST);
                        $admin_user->set_pass_msg('Admin user has been updated successfully.');
                        Redirect(make_admin_url('admin', 'list', 'list'));
                    endif;
		elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();   
                    $admin_user->set_pass_msg('The operation has been cancelled.');
                    Redirect(make_admin_url('admin', 'list', 'list'));
		endif;
		break;
	
	 case'delete':
		$QueryObj = new admin();
		$QueryObj->deleteAdminUser($id);
		$admin_user->set_pass_msg('Admin User has been deleted successfully.');
		Redirect(make_admin_url('admin', 'list', 'list'));
		break;
            
	 case'permanent_delete':
		$QueryObj = new admin();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg('Admin User has been deleted successfully.');
		Redirect(make_admin_url('admin', 'thrash', 'thrash'));
        	break;
          
    case'restore':
		$QueryObj = new admin();
		$QueryObj->restoreObject($id);
		$admin_user->set_pass_msg('Admin User has been restored successfully.');
		Redirect(make_admin_url('admin', 'thrash', 'thrash'));
        	break;
            
    case'thrash':
		if($login_user->type=='Affiliate'):
			$QueryObj = new admin();
			$QueryObj->getThrash($login_user->id);
		else:
			$QueryObj = new admin();
			$QueryObj->getThrash();	
		endif;	
		
		break;
	default:break;
endswitch;
?>
