<?php
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');

$modName='document';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';

#document document master category
$QueryMaster = new DocumentMasterCategory();
$QueryMaster->listAll();

#handle actions here.
switch ($action):
	case'list':
		$QueryObj = new document();
		$record=$QueryObj->listAllWithName();
		break;
	case'insert':
		if(isset($_POST['submit'])):
                    $QueryObj = new document();
                    $QueryObj->saveData($_POST);
                    $admin_user->set_pass_msg(NEW_DOCUMENT_CREATED);
                    Redirect(make_admin_url('document', 'list', 'list'));
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg(OPERATION_CANCEL);
                    Redirect(make_admin_url('document', 'list', 'list'));   
		endif;
		break;
	case'update':
		$QueryObj = new document();
		$object=$QueryObj->getRecord($id);                
		if(isset($_POST['submit'])):
                    $QueryObj = new document();
                    $QueryObj->saveData($_POST);
                    $admin_user->set_pass_msg(DOCUMENT_UPDATED);
                    Redirect(make_admin_url('document', 'list', 'list'));
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg(OPERATION_CANCEL);
                    Redirect(make_admin_url('document', 'list', 'list'));      
		endif;
		break;
	case'view':
		$QueryObj = new document();
		$object=$QueryObj->getRecord($id);                
		
		break;
	 case'delete':
		$QueryObj = new document();
		$QueryObj->deleteRecord($id);
		$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
		Redirect(make_admin_url('document', 'list', 'list'));  
		break;
	
	default:break;
endswitch;
?>
