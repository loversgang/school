                      <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							Dashboard <small>statistics and more</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>Dashboard</li>
							
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
                                
                                <?php 
                                /* display message */
                                display_message(1);
                                $error_obj->errorShow();
                                ?>
                                
                                
				<div id="dashboard">
					<!-- BEGIN DASHBOARD STATS -->
					<? if($login_user->type!='Payment' && $login_user->type!='Affiliate'):?>
					<div class="row-fluid">
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
							<div class="dashboard-stat blue">
								<div class="visual">
									<i class="icon-user"></i>
								</div>
								<div class="details">
									<div class="number">
										<?=$school?>
									</div>
									<div class="desc">                           
										Active Clients
									</div>
								</div>
								<a class="more" href="<?php echo make_admin_url('school', 'list', 'list');?>">
								View more <i class="m-icon-swapright m-icon-white"></i>
								</a>                 
							</div>
						</div>
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
							<div class="dashboard-stat green">
								<div class="visual">
									<i class="icon-building"></i>
								</div>
								<div class="details">
									<div class="number"><?=$institute?></div>
									<div class="desc">Institute Type</div>
								</div>
								<a class="more" href="<?php echo make_admin_url('type', 'list', 'list');?>">
								View more <i class="m-icon-swapright m-icon-white"></i>
								</a>                 
							</div>
						</div>
						<div class="span3 responsive" data-tablet="span6  fix-offset" data-desktop="span3">
							<div class="dashboard-stat purple">
								<div class="visual">
									<i class="icon-edit"></i>
								</div>
								<div class="details">
									<div class="number"><?=$document?></div>
									<div class="desc">Documents</div>
								</div>
								<a class="more" href="<?php echo make_admin_url('document', 'list', 'list');?>">
								View more <i class="m-icon-swapright m-icon-white"></i>
								</a>                 
							</div>
						</div>
						<div class="span3 responsive" data-tablet="span6" data-desktop="span3">
							<div class="dashboard-stat yellow">
								<div class="visual">
									<i class="icon-cogs"></i>
								</div>
								<div class="details">
                                    <div class="number">
									</div>
									<div class="desc">Settings</div>
								</div>
								<a class="more" href="<?php echo make_admin_url('setting', 'list', 'list');?>">
								View more <i class="m-icon-swapright m-icon-white"></i>
								</a>                 
							</div>
						</div>
					</div>
					<? endif;?>
					
					<div class="clearfix"></div>
					
					
			<div class="row-fluid">
			<? if(!empty($pending_payment_rec)):?>
				<div class='span6'>
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption"><i class="icon-user"></i>Pending Client Fee</div>
						<div class="tools">
							<a href="#portlet-config" data-toggle="modal" class="config"></a>
							<a href="javascript:;" class="reload"></a>
						</div>
					</div>
					<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover" >
									<thead>
										 <tr>
												<th>Institute ID</th>
												<th>Institute Name</th>												
										</tr>
									</thead>
										<tbody>
                                            <?php foreach($pending_payment_rec as $p_k=>$object):?>
                                                <tr class="odd gradeX">
													<td><?=$object->client_id?></td>
													<td><?php echo $object->school_name.', '.$object->address1?></td>
												</tr>
                                            <?php 
												endforeach;?>
										</tbody>									   
								</table>
					</div>
				</div>
				</div>
			 <?php endif;?> 	

			<? if(!empty($terminate_rec)):?>  	
				<div class='span6'>
				<div class="portlet box blue">
					<div class="portlet-title">
						<div class="caption"><i class="icon-user"></i>Terminated Client</div>
						<div class="tools">
							<a href="#portlet-config" data-toggle="modal" class="config"></a>
							<a href="javascript:;" class="reload"></a>
						</div>
					</div>
					<div class="portlet-body">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										 <tr>
												<th>Institute ID</th>
												<th>Institute Name</th>												
										</tr>
									</thead>
										<tbody>
                                            <?php foreach($terminate_rec as $ter_k=>$object_ter):?>
                                                <tr class="odd gradeX">
													<td><?=$object_ter->client_id?></td>
													<td><?php echo $object_ter->school_name.', '.$object_ter->address1?></td>
												</tr>
                                            <?php 
												endforeach;?>
										</tbody>									   
								</table>
						
					</div>
				</div>
				</div>
				<?php endif;?> 	
			</div>						
			<div class="clearfix"></div>		
					

					<!-- Session Student Chart -->
					<? if(!empty($Fee_record)):?><br/>
							<script type="text/javascript" src="https://www.google.com/jsapi"></script>
										<div class="row-fluid">
											<div class="portlet box red">
												<div class="portlet-title">
													<div class="caption"><i class="icon-user"></i>Current Year Client Fee</div>
													<div class="tools">
														<a href="#portlet-config" data-toggle="modal" class="config"></a>
														<a href="javascript:;" class="reload"></a>
													</div>
												</div>
												<div class="portlet-body">
													<div id="pie_chart_7" class="chart"></div>
													
												</div>
											</div>
										</div>		
										<script type="text/javascript">
										  google.load("visualization", "1", {packages:["corechart"]});
										  google.setOnLoadCallback(drawChart);
										  function drawChart() {
											var data = google.visualization.arrayToDataTable([
											  ['Clients', 'Current Year Fee'],
											  
											  <?php 
												  foreach($Fee_record as $k=>$v):
												  if(empty($v->total)){ $v->total='0';}
												  
													echo "['".$v->school_name."', ".$v->total."],";
												  endforeach;
											  ?>         
											]);
											var options = {
											  colors:['#E02222'],		  
											  title: 'Current Year',
											  hAxis: {title: 'Clients', titleTextStyle: {color: '#E02222'}}
											};

											var chart = new google.visualization.ColumnChart(document.getElementById('pie_chart_7'));
											chart.draw(data, options);
										  }
										</script>
					 <? endif;?>						
				
					<?php /*
                                                if((GOOGLE_AC!='') && (ANALYTIC_USER!='') && (ANALYTIC_PASS!='') && (ANALYTIC_TABLE!='')):
                                                        //include_once('google_analytics.php');

                                                endif;		
                                        */?>
				
				
				
				</div>
				
				
				
				
				
				
				
			</div>
			<!-- END PAGE CONTAINER-->    