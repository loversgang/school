
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Admin Users 
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('admin', 'list', 'list');?>">List Admin Users</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        New User
                                    </li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('admin', 'insert', 'list')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Add New Admin User</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>


                                            </div>
                                    </div>
                                    <div class="portlet-body form">

                                    
                                            <div class="control-group">
                                                    <label class="control-label" for="username">Username<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="username" value="" id="username" class="span6 m-wrap validate[required]"/>
                                                    </div>
                                            </div>        
                                            <div class="control-group">
                                                    <label class="control-label" for="password">Password<span class="required">*</span></label>
                                                     <div class="controls">
                                                       <input type="password" name="password"  value="" id="password" class="span6 m-wrap validate[required]" />
                                           
                                                     </div>
                                            </div>         
                                            <div class="control-group">
                                                    <label class="control-label" for="repassword">Confirm Password<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input type="password" id="repassword"  class="span6 m-wrap validate[required,equals[password]]"  />
                                                    </div>
                                            </div>  
                                            <div class="control-group">
                                                    <label class="control-label" for="business_email">Email Address<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input type="text" name="business_email" id="business_email" value="" class="span6 m-wrap validate[required,custom[business_email]]" />
                                                    </div>
                                            </div>  
                                            <div class="control-group">
                                                    <label class="control-label" for="is_active">Make User Active</label>
                                                    <div class="controls">
                                                        
                                                       <input type="checkbox" name="is_active" id="is_active" value="1" />

                                                    </div>
                                            </div>  

                                 
                                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
                                            <div class="form-actions">
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('admin', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                                    
                                            </div>
                                         
                                     <?php endif;?>        
                                  
                              </div> 
                            </div>
                        </div>
 

                     </form>
                     <div class="clearfix"></div>
                                          
                                   
                
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

