<!-- / Breadcrumbs -->
 <script type="text/javascript">
        jQuery(document).ready(function() {
           /* on click */
            $(".status_on").live("click",function(){
               
                var id=$(this).attr('on');	
                var dataString = 'table=admin_user&id='+id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo make_admin_url('status_on')?>",
                    data: dataString,
                    success: function(data, textStatus) {
						$(this).removeClass("active");
                    }
                });
            });
            
            $(".status_off").live("click",function(){
                var id=$(this).attr('off');
                var dataString = 'table=admin_user&id='+id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo make_admin_url('status_off')?>",
                    data: dataString,
                    success: function(data, textStatus) { 
                        $(this).removeClass("active");
                        
                    }
                });
            });
        });
    </script>



  

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Users 
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List Users</li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>Manage Users</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">								
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
											<th class="hidden-480">Sr. No.</th>
											<th>Username</th>
											<th class="hidden-480 sorting_disabled">Email</th>
											<th class="hidden-480 sorting_disabled">User Type</th>
											<th class="hidden-480 sorting_disabled">Status</th>
											<th >Action</th>
											</tr>
									</thead>
									<tbody>                                                                            
										<?php $sr=1;
										while($QueryObj1=$QueryObj->GetObjectFromRecord()):?>
											<tr class="odd gradeX">
													<td  class="hidden-480"><?php echo $sr++;?>.</td>
													<td><?php echo $QueryObj1->username;?></td>
													<td class="hidden-480 sorting_disabled"><a href="mailto:<?php echo $QueryObj1->email;?>"><?php echo $QueryObj1->email;?></a></td>
													<td class="hidden-480 sorting_disabled"><?=$QueryObj1->type?></td>
													<td class="hidden-480 sorting_disabled">
													<? if($QueryObj1->id!='1'):?>
  														<div class="btn-group tooltips" id='on_off_button' data-toggle="buttons-radio" title="Click here to change status">
															<div class="btn status_on <?php echo ($QueryObj1->is_active=='1')?'active':'';?>" on="<?php echo $QueryObj1->id;?>" rel="<?php echo ($QueryObj1->is_active=='1')?'on':'off';?>"  >SHOW</div>
															<div class="btn status_off <?php echo ($QueryObj1->is_active=='0')?'active':'';?>" off="<?php echo $QueryObj1->id;?>" rel="<?php echo ($QueryObj1->is_active=='1')?'off':'on';?>">HIDE</div>
														</div>	
													<? endif;?>	
													</td>
													<td>
													<a class="btn blue icn-only tooltips" href="<?php echo make_admin_url('admin', 'update', 'update', 'id='.$QueryObj1->id)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
													<?php if($QueryObj1->id!='1'):?>
													<a class="btn red icn-only tooltips" href="<?php echo make_admin_url('admin', 'delete', 'list', 'id='.$QueryObj1->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this page.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
													<?php endif;?>
													</td>
											</tr>
										<?php endwhile;?>
									</tbody>
								</table>
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



