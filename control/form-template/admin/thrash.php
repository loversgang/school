
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Admin Users 
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                     <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('admin', 'list', 'list');?>">List Admin Users</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                     <li class="last">
                                        Trash
                                    </li>
                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>Trash</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								
									
								</div>
							</div>
							<div class="portlet-body">
							
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
                                                                                        <th class="hidden-480">Sr. No.</th>
                                                                                        <th>Username</th>
                                                                                        <th class="hidden-480">Email</th>
                                                                                        <th >Action</th>
                                                                                        

                                                                                </tr>
									</thead>
									<tbody>
                                                                            
                                                                              <?php $sr=1;
                                                                               while($QueryObj1=$QueryObj->GetObjectFromRecord()):?>
                                                                                    <tr class="odd gradeX">
                                                                                        <td class="hidden-480"><?php echo $sr++;?></td>
																							<td><?php echo $QueryObj1->username;?></td>
																							<td class="hidden-480"><a href="mailto:<?php echo $QueryObj1->email;?>"><?php echo $QueryObj1->email;?></a></td>
																							
																							<td>
                                                                                                                                                                                        
                                                                                             <a class="btn green icn-only tooltips" href="<?php echo make_admin_url('admin', 'restore', 'restore', 'id='.$QueryObj1->id)?>" class="tipTop smallbtn" title="click here to restore this record"><i class="icon-undo icon-white"></i></a>&nbsp;&nbsp;
                                                                                             <a class="btn red icn-only tooltips" href="<?php echo make_admin_url('admin', 'permanent_delete', 'permanent_delete', 'id='.$QueryObj1->id.'&delete=1')?>" onclick="return confirm('Are you deleting this item permanently?.');" class="tipTop smallbtn" title="click here to delete this record permanently" ><i class="icon-remove icon-white"></i></a>  
                                                                                        </td>
											

                                                                                    </tr>
                                                                             <?php endwhile;?>

										
										
									
									</tbody>
								</table>
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

