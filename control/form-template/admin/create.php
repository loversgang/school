
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Users 
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('admin', 'list', 'list');?>">List Users</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        New User
                                    </li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('admin', 'insert', 'list')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Add New User</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>


                                            </div>
                                    </div>
                                    <div class="portlet-body form">

                                    
                                            <div class="control-group">
                                                    <label class="control-label" for="username">Username<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="username" value="" id="username" class="span6 m-wrap validate[required]"/>
                                                    </div>
                                            </div>        
                                            <div class="control-group">
                                                    <label class="control-label" for="password">Password<span class="required">*</span></label>
                                                     <div class="controls">
                                                       <input type="password" name="password"  value="" id="password" class="span6 m-wrap validate[required]" />
                                           
                                                     </div>
                                            </div>         
                                            <div class="control-group">
                                                    <label class="control-label" for="repassword">Confirm Password<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input type="password" id="repassword"  class="span6 m-wrap validate[required,equals[password]]"  />
                                                    </div>
                                            </div>  
                                            <div class="control-group">
                                                    <label class="control-label" for="email">Email Address<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input type="text" name="email" id="email"  class="span6 m-wrap validate[required,custom[email]]" />
                                                    </div>
                                            </div>  
		
										<? if($login_user->type=='Affiliate'): ?>
												<div class="control-group">
													<label class="control-label">User Type</label>
													<div class="controls">														
														<label class="radio">
														<div class="radio"><span class="checked"><input type="radio" checked="" value="Payment" name="type" class='select_admin'></span></div>
														 Payment User
														</label>  
														 
													</div>										
												</div>
													<div id='dyanamic_admin'>
															<div class="control-group" style='margin-bottom:0px;'>
																<div class="controls">				
																<div class="span4" style='width:265px;'><strong>All Clients</strong></div>
																<div class="span6"><strong>Selected List</strong> </div>
																</div>
															</div>													
															<div class="control-group" style='margin-bottom:0px;'>
																<div class="controls">
																	<select multiple="multiple" class='multiple_selection' for="list_clients" name='list_clients[]'>
																		<?php $sr=1;while($school=$QueryObjSchool->GetObjectFromRecord()):?>
																					<option value='<?=$school->id?>'><?=$school->school_name?></option>
																		<? $sr++; endwhile;?>	
																	</select>
																</div>
															</div>	
															<div class="control-group">
																<div class="controls">				
																<div class="span4" style='width:265px;'>Click on any school to add to list.</div>
																<div class="span6">Click on any school to remove from selected list. </div>
																</div>
															</div>
													</div>	
													
											<? else:?>
											
												<div class="control-group">
													<label class="control-label">User Type</label>
													<div class="controls">
														<label class="radio">
														<div class="radio"><input type="radio" checked="" class='select_admin' value="Administrator" name="type"></div>
														Administrator
														</label>
														<label class="radio">
														<div class="radio"><input type="radio" value="Payment" name="type" class='select_admin'></div>
														 Payment User
														</label>  
														<label class="radio">
														<div class="radio"><input type="radio" value="Affiliate" name="type" class='select_admin'></div>
														 Affiliate User
														</label>  
													</div>										
												</div>
													<div id='dyanamic_admin' style='display:none;'>
															<div class="control-group" style='margin-bottom:0px;'>
																<div class="controls">				
																<div class="span4" style='width:265px;'><strong>All Clients</strong></div>
																<div class="span6"><strong>Selected List</strong> </div>
																</div>
															</div>													
															<div class="control-group" style='margin-bottom:0px;'>
																<div class="controls">
																	<select multiple="multiple" class='multiple_selection' for="list_clients" name='list_clients[]'>
																		<?php $sr=1;while($school=$QueryObjSchool->GetObjectFromRecord()):?>
																					<option value='<?=$school->id?>'><?=$school->school_name?></option>
																		<? $sr++; endwhile;?>	
																	</select>
																</div>
															</div>	
															<div class="control-group">
																<div class="controls">				
																<div class="span4" style='width:265px;'>Click on any school to add to list.</div>
																<div class="span6">Click on any school to remove from selected list. </div>
																</div>
															</div>
													</div>												
											<? endif;?>	


											
                                            <div class="control-group">
                                                    <label class="control-label" for="is_active">Make User Active</label>
                                                    <div class="controls">                                                        
                                                       <input type="checkbox" name="is_active" id="is_active" value="1" />
                                                    </div>
                                            </div>  

                                 
                                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
                                            <div class="form-actions">
													<? if($login_user->type=='Affiliate'):?>
															<input type="hidden" name="created_by" id="created_by" value="<?=$login_user->id?>" />
                                                    <?php endif;?>
													 <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('admin', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                                    
                                            </div>
                                         
                                     <?php endif;?>        
                                  
                              </div> 
                            </div>
                        </div>
 

                     </form>
                     <div class="clearfix"></div>
                                          
                                   
                
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

<script type="text/javascript">
        jQuery(document).ready(function() {
			$(".select_admin").live('click',function(){	
				var type=$(this).val();

				if(type=='Payment' || type=='Affiliate'){
						$("#dyanamic_admin").show();
					}
					else{
						$("#dyanamic_admin").hide();
					}
				});	
			
		});
 </script>	