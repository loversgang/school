
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Clients > <?=$school->school_name.', '.$school->address1?>
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li><a href="<?php echo make_admin_url('school', 'list', 'list');?>">List Clients</a></li>
									<i class="icon-angle-right"></i>
									<li>Client Payment</li>
                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
			<div class="large-tile double bg-grey span6" style='margin: 0px 0px 25px; padding: 1px 1px 16px 18px; color: rgb(255, 255, 255);'>
									<div class="tile-body">
										<h3><?=$school->school_name?></h3>
										<div class="span2" style='margin-left:0px;'>
										<p>
											Start Date : <?=$school->create_date?><br/>
											Set Up-Fee : <?=number_format($school->set_up_fee,2)?><br/>
											Subscription Fee : <?=number_format($school->subscription_fee,2)?><br/>
											Payment Type : <? if($school->payment_type=='1'): echo 'Monthly'; elseif($school->payment_type=='3'): echo 'Quarterly'; elseif($school->payment_type=='6'): echo 'Half Yearly'; elseif($school->payment_type=='12'): echo 'Yearly'; endif;?><br/>
											No. Of Payments : <?=$no_payment?><br/>
											No. of Paid Payments: <?=$QueryObj->GetNumRows()?><br/>											
										</p>
										</div>
										<div class="span4" style='margin-left:25px;'>
											<div class="tile bg-green">
												<div class="tile-body">
													<i class="icon-alt" style='font-size:20px;'><?=CURRENCY_SYMBOL.number_format($total_payment)?></i>
												</div>
												<div class="tile-object">
													<div class="name">
														Total Payment
													</div>
												</div>
											</div>				
											<div class="tile bg-red">
												<div class="tile-body">
													<i class="icon-alt" style='font-size:20px;'><?=CURRENCY_SYMBOL.number_format($total_pending)?></i>
												</div>
												<div class="tile-object">
													<div class="name">
													<? if($total_pending>=0):?>
														Pending Payment
													<? else:?>
														Adjustment
													<? endif;?>	
													</div>
												</div>
											</div>								
										</div>
										<div class="clearfix"></div>
									</div>			
			<div class="clearfix"></div>
			</div>						
				
				
				<div class="tiles pull-right">
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut_payment.php');?>
				</div>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box yellow">
							<div class="portlet-title">
								<div class="caption"><i class="icon-inr"></i>Payment History > <?=$school->school_name.', '.$school->address1?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">      
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
												<th>Sr. No.</th>
												<th>Payment Date</th>
												<th style='text-align:center;'>Payment Type</th>												
												<th>Amount (<?=CURRENCY_SYMBOL?>)</th>
												<th>Action</th>												
										</tr>
									</thead>
									 <tbody>                                                                            
											<? if($QueryObj->GetNumRows()!=0):?>
											<?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
											<tr class="odd gradeX">
													<td><?=$sr?></td>
													<td><?php echo $object->payment_date?></td>
													<td style='text-align:center;'><?php echo $object->payment_type	?></td>																								
													<td><?php echo CURRENCY_SYMBOL." ".number_format($object->amount,2)?></td>
													<td>
													<a class="btn green icn-only tooltips" href="<?php echo make_admin_url('school', 'invoice', 'invoice', 'id='.$id.'&p_id='.$object->id)?>" class="tipTop smallbtn" title="click here to see invoice">Invoice</a>&nbsp;&nbsp;
													<a class="btn blue icn-only tooltips" href="<?php echo make_admin_url('school', 'edit_payment', 'edit_payment&s_id='.$id.'&p_id='.$object->id)?>" class="tipTop smallbtn" title="click here to edit the payment" ><i class="icon-edit icon-white"></i></a>  &nbsp;&nbsp;
													<? if($login_user->type!='Payment'):?>
														<a class="btn red icn-only tooltips" href="<?php echo make_admin_url('school', 'delete_payment', 'delete_payment&id='.$id.'&p_id='.$object->id)?>" onclick="return confirm('Are you sure? You are deleting this record.');" class="tipTop smallbtn" title="click here to delete this record" ><i class="icon-remove icon-white"></i></a> 
													<? endif;?>
													</td>
											</tr>
											<?php $sr++; endwhile;?>
									</tbody>
                                    <?php endif;?>  
								</table>
                                                            
                                                            
                              
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

