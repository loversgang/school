 <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Clients 
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('school', 'list', 'list');?>">List Clients</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        Edit Client
                                    </li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
				</div>
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('school', 'update', 'update&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Edit Client</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">
									
									<!-- School Info  -->
										<h4 class="form-section alert alert-info"><strong>Institute Information</strong></h4>
										<div class="row-fluid">
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="school_name">Institute Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="school_name" value="<?=$school->school_name?>" id="school_name" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>
											<? if($login_user->type!='Payment'):?>
                                            <div class="control-group">
                                                    <label class="control-label" for="username">Username<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="username" value="<?=$school->username?>" id="username" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>
											<? endif;?>	
                                            <div class="control-group">
                                                    <label class="control-label" for="address1">Address1<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="address1" value="<?=$school->address1?>" id="address1" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="city">City<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="city" value="<?=$school->city?>" id="city" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>												
                                            <div class="control-group">
                                                    <label class="control-label" for="state">State<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="state" value="<?=$school->state?>" id="state" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="image">Institute Logo</label>
                                                    <div class="controls">
													<? if($school->logo):
															$im_obj=new imageManipulation()?>
														<div class="item span10" style='text-align:center;'>	
														<a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<?=$im_obj->get_image('school','large',$school->logo);?>">
															<div class="zoom">
																<img src="<?=$im_obj->get_image('school','medium',$school->logo);?>" />
																<div class="zoom-icon"></div>
															</div>
														</a>
															<? if($login_user->type!='Payment'):?>
															<div class="details">
																<a href="<?=make_admin_url('school','delete_image','delete_image&id='.$id);?>" class="icon" onclick="return confirm('Are you sure? You are deleting this image.');" title="click here to delete this image"><i class="large icon-remove"></i></a>    
															</div>
															<? endif;?> 
														</div>															
													<? else:?>
                                                      <input type="file" name="image"  id="image" class="span10 m-wrap"/><br/>
													  <font style="font-size:10px;">Note: Institute Logo should be 150px(width)*100px(height).</font>
													<? endif;?> 
                                                    </div>
                                            </div> 											
										</div>
										<div class="span6 ">	
                                            <div class="control-group">
                                                    <label class="control-label" for="school_type">Institute Type<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select name="school_type" id="school_type"  class="span10 m-wrap ">
														<?php $sr=1;while($s_type=$QueryS_type->GetObjectFromRecord()):?>
																	<option value='<?=$s_type->id?>' <? if($school->school_type==$s_type->id){ echo 'selected';} ?>><?=ucfirst($s_type->name)?></option>
														<? $sr++; endwhile;?>															
                                                        </select>   														
													</div>
                                            </div>
											<? if($login_user->type!='Payment'):?>		
                                            <div class="control-group">
                                                    <label class="control-label" for="password">Password<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="password" value="<?=trim(encrypt_decrypt('decrypt',$school->password))?>" id="password" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>	
											<? endif;?>	
                                            <div class="control-group">
                                                    <label class="control-label" for="address2">Address2</label>
                                                    <div class="controls">
                                                      <input type="text" name="address2" value="<?=$school->address2?>" id="address2" class="span10"/>
                                                    </div>
                                            </div>											
  
                                            <div class="control-group">
                                                    <label class="control-label" for="District">District<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="district" value="<?=$school->district?>" id="district" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>  										
                                            <div class="control-group">
                                                    <label class="control-label" for="country">Country<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <select name="country" id="country" class="span10 m-wrap validate[required]">
													   <?php foreach($countries as $key=>$value):?>
																<option value="<?=$value?>" <? if(trim($school->country)==trim($value)): echo 'selected'; elseif($value=="India"): echo 'selected'; endif;?>><?=$value?></option>
													   <?php endforeach;?>
													  </select>
													</div>
                                            </div> 
										</div>	
										</div>	

									<!-- Contact Info  -->
										<h4 class="form-section alert alert-info"><strong>Contact Information</strong></h4>
										<div class="row-fluid">
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="school_head_name">Institute Head Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="school_head_name" value="<?=$school->school_head_name?>" id="school_head_name" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 	
                                            <div class="control-group">
                                                    <label class="control-label" for="school_head_email">Institute Head Email<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="school_head_email" value="<?=$school->school_head_email?>" id="school_head_email" class="span10 m-wrap validate[required,custom[email]]"/>
                                                    </div>
                                            </div>  											
                                            <div class="control-group">
                                                    <label class="control-label" for="phone1_contact_name">Contact Person Name1<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="phone1_contact_name" value="<?=$school->phone1_contact_name?>" id="phone1_contact_name" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 										
									
                                            <div class="control-group">
                                                    <label class="control-label" for="phone2_contact_name">Contact Person Name2</label>
                                                    <div class="controls">
                                                      <input type="text" name="phone2_contact_name" value="<?=$school->phone2_contact_name?>" id="phone2_contact_name" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="email_address">Institute Email<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="email_address" value="<?=$school->email_address?>" id="email_address" class="span10 m-wrap validate[required,custom[email]]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="website_url">Website Url</label>
                                                    <div class="controls">
                                                      <input type="text" name="website_url" value="<?=$school->website_url?>" id="website_url" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 	 											
										</div>
										<div class="span6 ">	
                                            <div class="control-group">
                                                    <label class="control-label" for="school_head_phone">Mobile No.<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="school_head_phone" value="<?=$school->school_head_phone?>" id="school_head_phone" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="fax">Fax<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="fax" value="<?=$school->fax?>" id="fax" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 											
                                            <div class="control-group">
                                                    <label class="control-label" for="phone1">Contact Person Phone1<span class="required" style='padding-left:0px;'>*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="phone1" value="<?=$school->phone1?>" id="phone1" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>  										
                                            <div class="control-group">
                                                    <label class="control-label" for="phone2">Contact Person Phone2</label>
                                                    <div class="controls">
                                                      <input type="text" name="phone2" value="<?=$school->phone2?>" id="phone2" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 	
                                            <div class="control-group">
                                                    <label class="control-label" for="school_phone">Institute Landline No.</label>
                                                    <div class="controls">
                                                      <input type="text" name="school_phone" value="<?=$school->school_phone?>" id="school_phone" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 											
										</div>
										</div>	

									<!-- Other Info  -->
										<h4 class="form-section alert alert-info"><strong>Other Information</strong></h4>
										<div class="row-fluid">
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="affiliation_board">Affiliation Board</label>
                                                    <div class="controls">
                                                      <input type="text" name="affiliation_board" value="<?=$school->affiliation_board?>" id="affiliation_board" class="span10 m-wrap"/>
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="established_year">Established Year</label>
                                                    <div class="controls">
                                                      <input type="text" name="established_year" value="<?=$school->established_year?>" id="established_year" class="span10 m-wrap"/>
                                                    </div>
                                            </div>												
										</div>
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="affiliation_code">Affiliation Code</label>
                                                    <div class="controls">
                                                      <input type="text" name="affiliation_code" value="<?=$school->affiliation_code?>" id="affiliation_code" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 
										</div>
										</div>
										
										
									<!-- Other Info  -->
										<h4 class="form-section alert alert-info"><strong>Subscription Plan Information</strong></h4>
											<div class="row-fluid" id='dynamic_model'>
											<div class="span6 ">	
												<div class="control-group">
														<label class="control-label" for="set_up_fee">Set-up Fee (<?=CURRENCY_SYMBOL?>)<span class="required">*</span></label>
														<div class="controls">
														  <input type="text"  name="subscription[set_up_fee]" value="<?=$school->set_up_fee?>" id="set_up_fee" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
														</div>
												</div>	
												<div class="control-group">
														<label class="control-label" for="payment_type">Subscription Period<span class="required">*</span></label>
														<div class="controls">
														  <select name="subscription[payment_type]" id="payment_type" class="span10 m-wrap ">
															<option value='1' <?php echo $school->payment_type=='1'?"selected":""?>>Monthly</option>
															<option value='3' <?php echo $school->payment_type=='3'?"selected":""?>>Quarterly</option>
															<option value='6' <?php echo $school->payment_type=='6'?"selected":""?>>Half Yearly</option>
															<option value='12' <?php echo $school->payment_type=='12'?"selected":""?>>Yearly</option>
														  </select>
														</div>
												</div>												
											</div>
											<div class="span6 ">
												<div class="control-group">
														<label class="control-label" for="subscription_fee">Subscription Fee (<?=CURRENCY_SYMBOL?>)<span class="required">*</span></label>
														<div class="controls">
														  <input type="text"  name="subscription[subscription_fee]" value="<?=$school->subscription_fee?>" id="subscription_fee" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
														</div>
												</div> 
											</div>
											</div>
																				
										<h3 class="form-section"></h3>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_active">Make Active</label>
                                                    <div class="controls">
                                                       <input type="checkbox" name="is_active" id="is_active" value="1" <?=($school->is_active=='1')?'checked':'';?>/>
                                                    </div>
                                            </div> 
                                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
									<? if($login_user->type!='Payment'):?>
                                            <div class="form-actions">
													<input type="hidden" name="subscription[id]" value="<?=$school->subscription_record_id?>" tabindex="7" /> 
													 <input type="hidden" name="id" value="<?=$school->id?>" tabindex="7" /> 
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('school', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                             </div>
									 <?php endif;?>		 
                                     <?php endif;?>
                              </div> 
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
	     </div>
             <div class="clearfix"></div>
    </div>
		
