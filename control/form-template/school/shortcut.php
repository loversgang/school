
        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('school', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                List Clients
                        </div>
                </div>
            </a>   
        </div>
	<? if($login_user->type!='Payment'):?>
        <div class="tile bg-blue <?php echo ($section=='insert')?'selected':''?>">
            <a href="<?php echo make_admin_url('school', 'insert', 'insert');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Add New Client
                        </div>
                </div>
            </a> 
        </div>

        <div class="tile bg-red <?php echo ($section=='thrash')?'selected':''?>">
            <a href="<?php echo make_admin_url('school', 'thrash', 'thrash');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-trash"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Trash
                        </div>
                </div>
            </a> 
        </div>
	<? endif;?>