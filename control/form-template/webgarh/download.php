<? 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subscriptionPlanClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSubscriptionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/paymentHistoryClass.php');

if($_GET):
isset($_GET['id'])?$id=$_GET['id']:$id='';
isset($_GET['p_id'])?$p_id=$_GET['p_id']:$p_id='';
				
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);	
			
			/* Get Invoice detail*/	
			$QueryInvoice = new paymentHistory();
			$invoice=$QueryInvoice->getPaymentRecord($p_id);
		
		
		
		?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<style type="text/css">

.main_table{
border:#cccccc 1px solid;
width:930px;
margin:auto;
font-family:Arial, Helvetica, sans-serif;
font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
font-size: 12px;
line-height: 20px;
color: #333333;
}
h1{
font-size:24px;
color:#555555;
text-transform:uppercase;
padding-left:10px;
}
.h1_line{
border-bottom:#e5e5e5 1px solid; width:900px; margin:auto;
}
p{
font-family:Arial, Helvetica, sans-serif;
color:#555555;
font-size:12px;
line-height:18px;

}
h2{
font-size:20px;
color:#555555;
}

.table_2{
width:850px;
margin:auto;
}

.table_3{
width:200px;
border:1px solid #CCCCCC;
/*background:#e5e5e5;*/

}

.table_3 .td_left{
background:#eeeeee;
width:110px;
padding-left:8px;
color:#555555;
font-size:12px;
line-height:20px;
border:1px solid #CCCCCC;
}
.table_3 .td_right{
width:192px;
padding-left:8px;
text-align:right;
background:#FFFFFF;
color:#555555;
font-size:12px;
padding-right:8px;
line-height:20px;
border:1px solid #CCCCCC;
}

.table_4{
width:800px;
margin:auto;

}
.table_4 th{
/*border:#cccccc 1px solid;*/
background:#eeeeee;
}
.table_4 .tr{
background:#ebebeb;
height:28px;
border:#cccccc 1px solid;
font-size:12px
}

#items {
clear: both;
width: 96%;
margin: 30px auto 0;
border: 1px solid #ccc;
}
#items th {
font-size:12px;
background: #f9f9f9;
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJod…EiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top,#f9f9f9 0%,#e5e5e5 100%);
background: -webkit-gradient(linear,left top,left bottom,color-stop(0%,#f9f9f9),color-stop(100%,#e5e5e5));
background: -webkit-linear-gradient(top,#f9f9f9 0%,#e5e5e5 100%);
background: -o-linear-gradient(top,#f9f9f9 0%,#e5e5e5 100%);
background: -ms-linear-gradient(top,#f9f9f9 0%,#e5e5e5 100%);
background: linear-gradient(to bottom,#f9f9f9 0%,#e5e5e5 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e5e5e5',GradientType=0 );
}

#items tr.item-row td {
border: 0;
vertical-align: top;
}
#items td.item-name {
width: 175px;}

#items td.description {
width: 250px;
}
#items td.blank {
border: 1px solid white;
}
#items td.total-line {
border-right: 0;
text-align: right;
}
#items td{
border: 1px solid #ccc;
padding: 5px;
}
#items table {
border-collapse: collapse;
}
#items tr {
display: table-row;
vertical-align: inherit;
border-color: inherit;
}
</style>
<body onload="javascript:window.print();">	
<table class="main_table">
<tr>
<td>
<h1>Invoice</h1>
</td>
</tr>
<tr>
<td>
<div class="h1_line"></div>
</td>
</tr>
<tr>
<td>
<table class="table_2">
<tr>
<td align="left">

<p><?=$school->school_name?><br>
   <?=$school->address1?><br>
	<? if(!empty($school->address2)){ echo $school->address2."<br/>";}?>
	<?=$school->city?>,&nbsp;<?=$school->district?>,&nbsp;<?=$school->state?><br/>  
 </p>
</td>
<td align="right" style="text-align:right;width: 320px;float:right;">

</td>
</tr>
<tr>
<td align="left">
<h2><?php echo $school->school_name;?></h2>
</td>
<td align="right">
<table class="table_3">
<tr>
<td class="td_left">Invoice #</td>
<td class="td_right"><?php echo $invoice->id;?></td>
</tr>
<tr>
<td class="td_left">Date</td>
<td class="td_right"><?php echo $invoice->payment_date;?></td>
</tr>
<tr>
<td class="td_left">Amount </td>
<td class="td_right"><?=CURRENCY_SYMBOL?> <?php echo number_format($invoice->amount,2);?></td>
</tr>
</table>
</td>
</tr>
</table>
<!------------------------>
<br />
<table class="table_4" id="items" align="center">
<tr>
<th>Sr. No.</th>
<th>Payment Date</th>
<th>Payment Type</th>
<th>Amount (<?=CURRENCY_SYMBOL?>)</th>
</tr>
<tr class="item-row" style="border-bottom: 1px solid #CCCCCC">
<td style="text-align:center;border-bottom: 1px solid #CCCCCC">1. </td>
<td class="item-name" style="text-align:center;border-bottom: 1px solid #CCCCCC"><div class="delete-wpr"><?php echo $invoice->payment_date;?></div></td>
<td class="qty" style="text-align: center;"><?=ucfirst($invoice->payment_type)?></td>
<td style="text-align: center;"><span class="price"><?='₹'." ".number_format($invoice->subscription_price,2)?></span></td>
</tr>
<tr>
<td style="border-bottom:none;" colspan="2" class="blank"> </td>
<td style="border-bottom:none;" class="total-line">Set-up Fee</td>
<td style="border-bottom:none;" class="total-value" style="text-align: center;"><div id="total" style="text-align:right;"><?='₹'." ".number_format($invoice->set_up_fee,2)?></div></td>
</tr>
<tr >
<td style="border-bottom:none;" colspan="2" class="blank"> </td>
<td style="border-bottom:none;" class="total-line">Subtotal</td>
<td style="border-bottom:none;" class="total-value" style="text-align: center;"><div id="subtotal" style="text-align:right;"><?='₹'." ".number_format($invoice->amount,2)?></div></td>
</tr>
<tr>
<td style="border-bottom:none;" colspan="2" class="blank"> </td>
<td style="border-bottom:none; background:#eeeeee;"  class="total-line balance">Total</td>
<td style="border-bottom:none; background:#eeeeee;" class="total-value balance" style="text-align: center;"><div class="due" style="text-align:right;"><?='₹'." ".number_format($invoice->amount,2)?></div></td>
</tr>

</table><br/><br/><br/><br/>
</td>
</tr>
</table>
</body>
</html>		




										
<?		
endif;
?>
