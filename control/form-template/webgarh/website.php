
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Clients > <?=$school->school_name.', '.$school->address1?>
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a>                                            
                                    </li>
									 <i class="icon-angle-right"></i>
                                    <li><a href="<?php echo make_admin_url('school', 'list', 'list');?>">List Clients</a></li>
									<i class="icon-angle-right"></i>
									<li>Client Website Setting</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut_payment.php');?>
				</div>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<form class="form-horizontal" action="<?php echo make_admin_url('school', 'website', 'website&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                        <div class="portlet box grey">
							<div class="portlet-title">
								<div class="caption"><i class="icon-cogs"></i><?=$school->school_name.', '.$school->address1?> > Website Setting</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body form">  
							
							<!-- Update Case -->	
									<!-- School website  -->
										<h4 class="form-section alert alert-info"><strong>Client website Setting</strong></h4>

										
									
										<div class="row-fluid">
										<div class="span12" id='large_space'>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_website">Allow Website OR Not<span class="required">*</span></label>
                                                    <div class="controls">
                                                       	<label class="checkbox line">
														<input type="checkbox" name="is_website"  value="1" <?=($school->is_website=='1')?'checked':'';?> id='master_checker'/>
														</label>
                                                    </div>													
                                            </div>
											<div class="control-group">
													<label class="control-label" for="is_website">Website URL<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <h5><strong><? echo DIR_WS_SITE.'site/'.$id;?></strong></h5>
                                                    </div>
											</div>		
										</div>
										</div>										

									<!-- Other Setting  -->										
										<h4 class="form-section alert alert-info"><strong>Assign Templates</strong></h4>
										<div class="row-fluid">
										<div class="span12" id='large_space'>
													<? if(!empty($All_theme)): 
														foreach($All_theme as $key=>$value): ?>
														<div class="control-group">
														<label class="control-label"><?=$value->name?></label>
														<div class="controls">
																<label class="radio line span1">
																	<span>
																	<input type="radio" value="<?=$value->id?>" name="theme" class='theme_class' <?=($school->is_website=='1')?'':'disabled';?> <?=($school->theme==$value->id)?'checked':'';?>/>
																	</span>	
																</label>	
																	<? $im_obj=new imageManipulation()?>
																	<div class="item span6" style='text-align:center;'>	
																	 <div class="zoom" style='text-align:left;'>
																		<a class="fancybox-button" data-rel="fancybox-button" title="<?=$value->name?>" href="<?=$im_obj->get_image('theme','large',$value->image);?>">
																			<img src="<?=$im_obj->get_image('theme','thumb',$value->image);?>" />
																		</a>
																	 </div>
																	</div>
																	<div class="clearfix"></div>																													
														</div>
														</div>
													<?	
													 endforeach;	
													 else:?>
														Sorry, No Record Found..! 
													<? endif;?>								
											</div>
										</div>
									
		                                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
											<? if($login_user->type!='Payment'):?>
                                            <div class="form-actions">
														<input type="hidden" name="is_active" value="<?=$school->is_active?>" tabindex="7" />
														<input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" />
														<input type="hidden" name="id" value="<?=$school->id?>" tabindex="7" /> 														
														<input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('school', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                             </div>
											<?php endif;?> 
                                     <?php endif;?>
											
							</div>							
						</div>						
                        </form>	                      
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    
<script type="text/javascript">
	$("#master_checker").click(function(){	
		var ct_val=$(this).val();
		if (!$(this).is(':checked')) {
			$('.theme_class').removeAttr("checked");
			$('.theme_class').parent().removeClass('checked');
			//$('.theme_class').attr("disabled",true);	
		}
		else{
			$(".theme_class").each(function() {
				$('.theme_class').removeAttr("disabled",false);
				$('.theme_class').removeClass('checked');	
			});	
		}
	});	
	$(".theme_class").click(function(){	
		$('#master_checker').attr("checked");
		$('#master_checker').parent().addClass('checked');
	});
</script>	