  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Staff Categories
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List Staff Categories</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
						<div class="tile bg-blue">
							<a href="<?php echo make_admin_url('setting', 'category_new', 'category_new');?>">
								<div class="corner"></div>
								<div class="tile-body">
									<i class="icon-plus"></i>
								</div>
								<div class="tile-object">
										<div class="name">
												Add New Category
										</div>
								</div>
							</a>   
						</div>   
				</div>
						
            <div class="clearfix"></div>
			<div style="clear:both;"></div>	
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-file-text"></i>Manage Staff Categories</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
							    <form action="<?php echo make_admin_url('setting', 'category', 'category');?>" method="post" id="form_data" name="form_data" >	
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										<tr>                                                                                      
												<th class="hidden-480">Sr. No</th>																								
												<th style='width:60%;' class="hidden-480">Title</th>												
												<th>Action</th>
												<th style='border-left:none;border-right:none;width:2%;'></th>
												<th style='border-left:none;border-right:none;width:2%;'></th>
												<th style='border-left:none;border-right:none;width:2%;'></th>
                                        </tr>
									</thead>
									 <tbody>                                                                            
										<? if($QueryObj->GetNumRows()!=0):?>
										<?php $sr=1;while($obj=$QueryObj->GetObjectFromRecord()):?>
											<tr class="odd gradeX">
                                                <td class="hidden-480"><?php echo $sr++;?></td>												
                                                <td class="hidden-480"><?=$obj->name?></td>                                                										
												<td>
                                                    <a class="btn blue icn-only tooltips" href="<?php echo make_admin_url('setting', 'category_edit', 'category_edit', 'id='.$obj->id)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                                        <a class="btn red icn-only tooltips" href="<?php echo make_admin_url('setting', 'delete', 'delete', 'type=category&id='.$obj->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                                </td>
												<td style='border-left:none;border-right:none;'></td>
												<td style='border-left:none;border-right:none;'></td>
												<td style='border-left:none;border-right:none;'></td>
                                            </tr>
                                        <?php endwhile;?>
                                        <?php endif;?>
									</tbody>                          
								</table>
								</form>    
							</div>
						</div>                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



