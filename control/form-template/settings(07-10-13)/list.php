
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Settings
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>Settings</li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<div class="span3">
							<ul class="ver-inline-menu tabbable margin-bottom-10">
                                                            
                                                               <?php if($setting_type=='website'):?>
                                                            
                                                                    <li class="<?php echo $sname=='action'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=action'.'&setting_type=website');?>"><i class="icon-move"></i> Action Positioning</a></li>
                                                                    <!--<li class="<?php echo $sname=='image'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=image'.'&setting_type=website');?>"><i class="icon-picture"></i> Image Sizes</a></li>-->
                                                                    <li class="<?php echo $sname=='email'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=email'.'&setting_type=website');?>"><i class="icon-table"></i> Notification Emails </a></li>
                                                                    <li class="<?php echo $sname=='site-verify'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=site-verify'.'&setting_type=website');?>"><i class="icon-tint"></i> Site Verifications</a></li>
                                                                    <li class="<?php echo $sname=='analytics'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=analytics'.'&setting_type=website');?>"><i class="icon-google-plus-sign"></i> Google Analytics</a></li>
                                                                   <!-- <li class="<?php echo $sname=='smtp'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=smtp'.'&setting_type=website');?>"><i class="icon-cloud"></i> SMTP Servers</a></li>-->
                                                               <?php elseif($setting_type=='social'):?>
                                                                    <li class="<?php echo $sname=='twitter'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=twitter'.'&setting_type=social');?>"><i class="icon-twitter"></i>  Twitter Integration</a></li>
                                                                    <li class="<?php echo $sname=='facebook'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=facebook'.'&setting_type=social');?>"><i class="icon-facebook"></i> FaceBook Integration</a></li>
                                                                    <li class="<?php echo $sname=='youtube'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=youtube'.'&setting_type=social');?>"><i class="icon-youtube-sign"></i> YouTube Integration</a></li>
                                                                   <!-- <li class="<?php echo $sname=='google-plus'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=google-plus'.'&setting_type=social');?>"><i class="icon-google-plus"></i> Google Plus Integration</a></li>-->
                                                               <?php else:?>  
                                                                    <li class="active"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=general'.'&setting_type=general');?>"><i class="icon-cogs"></i> General Settings</a></li>
                                                               <?php endif;?>
                                                            
                                                            
								
							</ul>
						</div>
						
                                                  <!-- / Box -->
                                                  <div class="span9">
                                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                     <div class="portlet box yellow">
                                                            <div class="portlet-title">
                                                                    <div class="caption"></i><?php echo ucfirst($sname);?> Settings </div>
                                                                    <div class="tools">
                                                                            <a href="javascript:;" class="collapse"></a>


                                                                    </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <form class="form-horizontal" action="<?php echo make_admin_url('setting', 'update', 'list');?>" method="POST" enctype="multipart/form-data" id="validation">
                                                                 <?php
                                                            
                                                                    foreach($ob as $kk=>$vv):
                                                                 ?>
                                                                         <div class="control-group">
                                                                                <label class="control-label"><?php echo ucfirst($vv['title']);?>:</label>
                                                                                <div class="controls">
                                                                                    <?php echo get_setting_control($vv['id'], $vv['type'], $vv['value']);?>
                                                                                </div>    
                                                                        </div>
                                                                     <?php if($vv['key']=='FDP' && $vv['value']):?>
                                                                         <div class="control-group">
                                                                                <label class="control-label">Setup Facebook Account(only if not set):</label>
                                                                                <div class="controls">
                                                                                   <a style="color:#0066CC" target="_blank" href="<?php echo DIR_WS_SITE?>admin/facebook/get-code.php">Click here to setup FB Account</a>
                                                                       
                                                                                </div>
                                                                         </div>       
                                                                    <?php endif;?>
                                                                    <?php if($vv['key']=='TDP' && $vv['value']):?>
                                                                        <div class="control-group">
                                                                                <label class="control-label">Setup Twitter Account(only if not set):</label>
                                                                                <div class="controls">
                                                                                 <a  style="color:#0066CC"target="_blank" href="<?php echo DIR_WS_SITE?>admin/twitter/connect.php">Click here to setup Twitter Account</a>
                                                                                 </div>
                                                                        </div>        
                                                                    <?php endif;?>

                                                              <?php endforeach;?>   
                                                                
                                                                
                                                           
                                                                <div class="form-actions">
                                                                    
                                                                         <input  type="hidden" name="sname1" value="<?php echo $sname?>"/>
                                                                         <input  type="hidden" name="setting_type" value="<?php echo $setting_type?>"/>
                                                                         
                                                                         <input class="btn yellow" type="submit" name="Submit" value="Submit" tabindex="7" /> 
                                                                         <a href="<?php echo make_admin_url('setting', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>

                                                                </div>

                                                                </form>

                                                            </div> 
                                                    </div>
                                                </div>

						<!--end span9-->                                   
					</div>
				</div>

            <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    





