<style>
    td {
        border: solid 2px #CCC;
        -moz-box-shadow: 0px 2px 0px #69C;
        -webkit-box-shadow: 0px 2px 0px #69C;
        box-shadow: 0px 2px 0px #69C; }
    </style>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">

            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Document Templates
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <i class="icon-file-text"></i>
                    <a href="<?php echo make_admin_url('document', 'list', 'list'); ?>">List Templates</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li class="last">
                    View Template
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <!-- / Box -->
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <?php echo html_entity_decode($object->format); ?>


            <!--
                          <div class="form-actions">
                                                                      <input type='hidden' name='id' value="<?= $object->id ?>"/>
                                   <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                   <a href="<?php echo make_admin_url('document', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                          </div>
            -->


        </div>

        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>             
</div>
<!-- END PAGE CONTAINER-->    



