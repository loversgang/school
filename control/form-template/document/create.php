<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">

            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Templates
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>

                <li>
                    <i class="icon-file-text"></i>
                    <a href="<?php echo make_admin_url('document', 'list', 'list'); ?>">List Templates</a>
                    <i class="icon-angle-right"></i>

                </li>
                <li class="last">
                    New Template
                </li>

            </ul>


            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>

    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>

    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('document', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-file-text"></i>New Template</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="control-group">
                            <label class="control-label" for="name">Name<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="name" id="name" value="" class="span6 m-wrap validate[required]"  />
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label" for="type">Type<span class="required">*</span></label>
                            <div class="controls">
                                <select  name="type" id="type"  class="span6 m-wrap ">
                                    <?php
                                    if ($QueryMaster->GetNumRows() != '0'):
                                        while ($master = $QueryMaster->GetObjectFromRecord()):
                                            ?>														 
                                            <option value="<?= $master->id ?>"><?= $master->name ?></option>
                                        <?php
                                        endwhile;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>  
                        <div class="control-group">
                            <label class="control-label" for="format">Format<span class="required">*</span></label>
                            <div class="controls">
                                <textarea id="format" class="span12 ckeditor m-wrap" name="format" rows="6"></textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="is_active">Make Active</label> 
                            <div class="controls">
                                <input type="checkbox" name="is_active" id="is_active" value="1" />
                            </div>
                        </div> 								 
<?php if (defined('BOTTOM_ACTION') && BOTTOM_ACTION == 1): ?>
                            <div class="form-actions">
                                <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                <a href="<?php echo make_admin_url('document', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                            </div>
<?php endif; ?>	
                    </div> 
                </div>
            </div>

        </form>
        <div class="clearfix"></div>




    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



