
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
             <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Institute Types
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>
                                            <i class="icon-list"></i>
                                            <a href="<?php echo make_admin_url('type', 'list', 'list');?>">List Institute Types</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>									
                                    <li>Trash</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
				</div>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>Trash</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">      
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
												<th class="hidden-480">Sr. No.</th>																							
												<th style='width:70%;'>Title</th>												
												<th>Action</th>		
																						
										</tr>
									</thead>
									 <tbody>                                                                            
											<? if($QueryObj->GetNumRows()!=0):?>
											<?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>																	
											<tr class="odd gradeX">
                                                <td class="hidden-480"><?php echo $sr++;?>.</td>												
                                                <td class="hidden-480"><?=$object->name?></td>																							
												<td>
													<a class="btn green icn-only tooltips" href="<?php echo make_admin_url('type', 'restore', 'restore', 'id='.$object->id)?>" onclick="return confirm('Are you restoring this record?.');" class="tipTop smallbtn" title="click here to restore this record"><i class="icon-undo icon-white"></i></a>&nbsp;&nbsp;
													 <a class="btn red icn-only tooltips" href="<?php echo make_admin_url('type', 'permanent_delete', 'permanent_delete', 'id='.$object->id.'&delete=1')?>" onclick="return confirm('Are you deleting this item permanently?.');" class="tipTop smallbtn" title="click here to delete this record permanently" ><i class="icon-remove icon-white"></i></a> 
												</td>
												 													
											</tr>
											<?php endwhile;?>            
										<?php endif;?>  
									</tbody>
								</table>                  
                              
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

