<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        
			<ul class="page-sidebar-menu">
				<li style="margin-bottom:10px;">
				<div class='span2' style='color:#fff;'><?=$login_user->type?> User</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone">
					
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				
				<li class="start <?php echo ($Page=='home')?'active':''?> ">
					<a href="<?php echo make_admin_url('home', 'list', 'list');?>">
					<i class="icon-home"></i> 
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>				
				<li class="<?php echo ($Page=='school')?'active':''?>">
                        <?php include_once(DIR_FS_SITE.ADMIN_FOLDER.'/left/client.php');?>
				</li>
				<? if($login_user->type=='Administrator'):?>
					<li class="<?php echo ($Page=='type')?'active':''?>">
						<?php include_once(DIR_FS_SITE.ADMIN_FOLDER.'/left/type.php');?>
					</li>				
								 
					<li class="<?php echo ($Page=='document')?'active':''?>">
						<?php include_once(DIR_FS_SITE.ADMIN_FOLDER.'/left/document.php');?>
					</li>
				<? endif;?>
				
				<? if($login_user->type=='Administrator' || $login_user->type=='Affiliate'):?>
					<li class="<?php echo ($Page=='admin')?'active':''?>">
							<?php include_once(DIR_FS_SITE.ADMIN_FOLDER.'/left/admin.php');?>					
					</li>
				<? endif;?>
				
				<? if($login_user->type=='Administrator'):?>
					<li class="<?php echo ($Page=='email')?'active':''?>">
						<?php include_once(DIR_FS_SITE.ADMIN_FOLDER.'/left/email.php');?>					
					</li>				
					<li class="last <?php echo ($Page=='setting' || $Page=='backup')?'active':''?>">
						<?php include_once(DIR_FS_SITE.ADMIN_FOLDER.'/left/setting.php');?>
					</li>
				<? endif;?>
				<li class="<?php echo ($Page=='logout')?'active':''?>">
					<a href="<?php echo make_admin_url('logout', 'list', 'list');?>">
					<i class="icon-lock"></i> 
					<span class="title">Log Out</span>
					<span class="selected"></span>
					</a>
				</li>				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
                
		<!-- BEGIN PAGE -->
		<div class="page-content">
   
   