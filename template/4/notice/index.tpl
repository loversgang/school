
<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
        <!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, validationEngine.jquery']->
        <!-[add_js name='jquery.min, jquery.validationEngine, jquery.validationEngine-en,  newsletter, bootstrap']->        
    </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->

                             
       <div class="row-fluid">
            <div class="span8">
                <h1>Notice Board</h1>
                       <div class="row-fluid">
                                 <!-[foreach from=$NOTICE key=nk item=nv]->   
                                    <div class="row-fluid">
                                     <a href="<!-[make_url page=notice_detail query='id='|cat:$nv->id]->"><!-[$nv->name]-></a>
                                        <div class='notice_date'>On Date: <!-[date('d M, Y',strtotime($nv->on_date))]-></div>
                                     <p ><!-[$nv->short_description]-></p>
                                               <div class='lern_more'>
                                               <a href="<!-[make_url page=notice_detail query='id='|cat:$nv->id ]->" class="link"> View More..</a>
                                             </div>                                                        
                                     <hr/>
                                    </div>
                                 <!-[/foreach]-> 
                     </div>                    
              </div>
             <div class="span4" id="mid">
                         <!-- Admission Part -->
                        <!-[include file="../include/admission.tpl" title=Admission]->


                         <!-- Right Box Notice Board Part -->
                        <!-[include file="../include/right_notice.tpl" title=Notice]->

            </div>                     
       </div>
                     
    

<!-[include file="../include/footer.tpl" title=footer]->
