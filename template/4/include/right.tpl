<div class="span3" id="right-panel">
        <div class="well-blue">
        <div class="well-white">

            <dl class="dl-horizontal" id="about-right">
            <h3>About Constituency</h3><div class="clearfix"></div>
                <!--[if $CONST->name]--><dt>Name</dt><dd><!--[$CONST->name]--></dd><!--[/if]--> 
                <!--[if $CONST->district]--><dt>District</dt><dd><!--[$CONST->district]--></dd><!--[/if]-->
                <!--[if $CONST->state]--><dt>State</dt><dd><!--[$CONST->state]--></dd><!--[/if]-->                 
                <!--[if $CONST->country]--><dt>Country</dt><dd><!--[$CONST->country]--></dd><!--[/if]-->  
                <!--[if $CONST->sex_ratio]--><dt>Sex Ratio</dt><dd><!--[$CONST->sex_ratio]--></dd><!--[/if]-->                 
                <!--[if $CONST->population]--><dt>Population</dt><dd><!--[$CONST->population]--></dd><!--[/if]--> 
                <!--[if $CONST->total_voters]--><dt>Total Voters</dt><dd><!--[$CONST->total_voters]--></dd><!--[/if]-->
              </dl>
              </div>
        <div class="clearfix"></div>

        <div class="well-white" id="about-me">
        <h3>About Me</h3><div class="clearfix"></div>
         <!--[if $ABOUT_ME->current_title]--><center><small><!--[$ABOUT_ME->current_title]--></small></center><!--[/if]-->
        <div class="clearfix"></div>

        <div class="row-fluid">
        <center>
            <!--[if $ABOUT_ME->image]-->
                <div class="circle">
                   <img src="<!--[$smarty.const.DIR_WS_SITE]-->upload/photo/news/about/<!--[$ABOUT_ME->image]-->" alt=""/>
                </div>
            <!--[/if]-->
          <div class="clearfix"></div>
        <!--[if $ABOUT_ME->full_name]--><h6><!--[$ABOUT_ME->full_name]--></h6><div class="clearfix"></div> <!--[/if]-->
        </center>
        </div>
        <div class="clearfix"></div>

        <div class="row-fluid">
        <center>
             <!--[if $ABOUT_ME->party_logo]-->
                    <div class="circle">
                          <img src="<!--[$smarty.const.DIR_WS_SITE]-->upload/photo/news/about/<!--[$ABOUT_ME->party_logo]-->" alt=""/>
                    </div>
             <!--[/if]--> 
             <div class="clearfix"></div>
        <!--[if $ABOUT_ME->party_name]--><h6><!--[$ABOUT_ME->party_name]--></h6> <!--[/if]--> <div class="clearfix"></div>
        </center>
        </div>
        <div class="clearfix"></div>
        <!--[if $ABOUT_ME->manifesto_document]-->
           <!--[if file_exists($smarty.const.DIR_FS_SITE|cat:"public/upload/file/news/"|cat:$ABOUT_ME->manifesto_document)]-->
                <center>
                <a href="<!--[$smarty.const.DIR_WS_SITE]-->upload/file/news/<!--[$ABOUT_ME->manifesto_document]-->" target="_blank">Download Manifesto</a>
                </center>
           <!--[/if]-->
         <!--[/if]--> 
                <center>
                <a href="<!--[make_url page='about-me']-->">View more</a>
                </center>         
        </div>
        <div class="clearfix"></div>

        <div class="well-white" id="career">
        <h3>Political Career</h3><div class="clearfix"></div>
         <div class="row-fluid">
        <center>
              <!--[if count($CAREER)]-->
                     <!--[foreach from=$CAREER key=ks item=vs]-->          
                            <a href="<!--[make_url page='career-details' query='id='|cat:$vs['id']]-->"><!--[ucwords($vs['title'])]--><br/>
                                <!--[date("Y",strtotime($vs['from_date']))]--> - <!--[date("Y",strtotime($vs['to_date']))]-->
                            </a>
                            <div class="clearfix"></div>
                            <hr/>
              <!--[/foreach]-->
        </center>  
         <center>
            <a href="<!--[make_url page='career']-->" class="link">View More</a>
        </center>            
       <!--[else]-->
               <h5 id="not_found">No Career available !</h5>                                         
          <!--[/if]-->               
    
        </div>
        <div class="clearfix"></div>


        <div class="clearfix"></div>
        <center>

        </center>
        </div>
        <div class="clearfix"></div>

        <div class="well-white">
        <div id="links">
        <h3>Follow ME on</h3><div class="clearfix"></div>
        <center>
             <ul>
             <li>
                 <a href="<!--[$smarty.const.TWITTER_PAGE]-->" target="_blank" class="twitter-link" title="Twitter Page"></a>
                 <a href="<!--[$smarty.const.YOUTUBE_PAGE]-->" target="_blank" class="you-tube-link" title="Youtube"></a>
                 <a href="<!--[$smarty.const.FACEBOOK_PAGE]-->" target="_blank" class="fb-link" title="Facebook Page"></a>
             </li>
            </ul>
        </center>

      <div class="clearfix"></div>
    </div>
      <div class="clearfix"></div>
        </div>  <div class="clearfix"></div>

</div>
</div>