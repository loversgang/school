<body>
	<div class="container">
             <div class="row-fluid">
            	<div class="row-fluid" id='logo_back_image'>
                    <div class="span12">
                        <!-[nocache]->  
                        <a href="<!-[make_url page='home']->" title='Home'>
                            <!-[if $SCHOOL->logo]->
                                <img src="<!-[$smarty.const.DIR_WS_SITE_PATH]->upload/photo/school/medium/<!-[$SCHOOL->logo]->" alt="" style='width:150px;float:left;margin-right: 10px'/>
                                    <div id='school_name' style='margin-top: 45px;'><!-[$SCHOOL->school_name]-></div>
                                    <div id='school_type' style='width: 30%;margin-left: 17%'><!-[$SCHOOL->school_type]-></div>                            
                            <!-[else]-> 
                                    <div id='school_name'><!-[$SCHOOL->school_name]-></div>
                                     <div id='school_type'><!-[$SCHOOL->school_type]-></div>
                            <!-[/if]->                         
                        </a>    
                        <!-[/nocache]->                     
                    </div>    
                               
                </div>	                     

                 
           	<div class="row-fluid">
                        <div class="navbar">
                          <div class="navbar-inner">
                            <div class="container">
                              <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </a>

                              <div class="nav-collapse collapse navbar-responsive-collapse">
                                 <!-[nocache]->  
                                  <ul class="nav">
                                  <li class="<!-[if $PAGE eq 'home']->active<!-[/if]->"><a href="<!-[make_url page=home]->" title='Home'>Home</a></li>
                                  <li class="dropdown <!-[if $PAGE eq 'content' && ($ID eq $ABOUT->id || $ID eq $INFO->id || $ID eq $INFRASTRUCTURE->id || $ID eq $AWARDS->id)]->active<!-[/if]->">
                                    <a data-toggle="dropdown" class="dropdown-toggle <!-[if $PAGE eq 'content' && $ID eq $ABOUT->id]->active<!-[/if]->" href='' title='About'>About</a>
                                        <ul class="dropdown-menu">
                                           <!-[if $ABOUT]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $ABOUT->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$ABOUT->id]->" title='<!-[$ABOUT->name]->'><!-[$ABOUT->name]-></a></li>
                                          <!-[/if]->                                               
                                           <!-[if $INFO]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $INFO->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$INFO->id]->" title='<!-[$INFO->name]->'><!-[$INFO->name]-></a></li>
                                          <!-[/if]->                                            
                                          <!-[if $INFRASTRUCTURE]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $INFRASTRUCTURE->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$INFRASTRUCTURE->id]->" title='<!-[$INFRASTRUCTURE->name]->'><!-[$INFRASTRUCTURE->name]-></a></li>
                                          <!-[/if]->
                                           <!-[if $AWARDS]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $AWARDS->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$AWARDS->id]->" title='<!-[$AWARDS->name]->'><!-[$AWARDS->name]-></a></li>
                                          <!-[/if]->  
                                        </ul>
                                  </li>                                    
                                  <li class="<!-[if $PAGE eq 'gallery']->active<!-[/if]->"><a href="<!-[make_url page=gallery]->" title='Gallery'>Gallery</a></li>
                                  <li class="<!-[if $PAGE eq 'staff']->active<!-[/if]->"><a href="<!-[make_url page=staff]->" title='Faculty & Staff '>Faculty & Staff </a></li>
                                  <li class="<!-[if $PAGE eq 'contact']->active<!-[/if]->"><a href="<!-[make_url page=contact]->" title='Contact Us'>Contact Us</a></li>
                                  
                                </ul>
                                <!-[/nocache]->   
                                <div class="span3 pull-right" id='nav_phone'>
                                   <span class="pull-left"><img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->img/phone.png" alt=""/></span>
                                        <a href="tel:<!-[$SCHOOL->school_phone]->" title='Make a call'><!-[$SCHOOL->school_phone]-></a>
                                </div>
            
                              </div><!-- /.nav-collapse -->
                            </div>
                          </div><!-- /navbar-inner -->
                        </div><!-- /navbar -->
                </div>               
            </div>