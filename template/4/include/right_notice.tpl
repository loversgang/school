           <div class="well">
               <h1>Notice Board <!-[if count($ALL_GALLERY)]-><a href="<!-[make_url page=notice]->" title='View All'>View All</a><!-[/if]-></h1>
               <div class="row-fluid" id="notice1">
                    <!-[if count($NOTICE)]-> 
                    <!-[assign var="NV" value=1]->
                    <ul class="unstyled">                        
                        <!-[foreach from=$NOTICE key=nk item=nv]->
                        <!-[if $NV<=5]->
                         <li>  
                                <a href="<!-[make_url page=notice_detail query='id='|cat:$nv->id]->" title='Click here to View'><!-[$nv->name]-> <span id='news_date'><!-[date('d M, Y',strtotime($nv->on_date))]-></span></a>
                                <p><!-[$nv->short_description]-></p>
                                <div class="clear"></div> 
                         </li>
                        <!-[assign var="NV" value=$NV+1]->
                        <!-[/if]->
                        <!-[/foreach]-> 
                    </ul>
                    <!-[else]-> 
                       <p> Sorry, No Record Found..!</p>
                    <!-[/if]-> 
                </div>
               <div class="clearfix"></div>
            </div>