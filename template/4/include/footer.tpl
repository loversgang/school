<div class="footer">
            <div class="row-fluid">
            <small>Copyright © <!-[date('Y')]-></small>
             <!-[nocache]->  
            <ul><li>
            
            <!-[if $SCHOOL->google]->            
                <a href="<!-[$SCHOOL->google]->" target='_blank' title='Follow us on Google'><span class="youtube"></span></a>
            <!-[/if]-> 
            
            <!-[if $SCHOOL->twitter]->
                <a href="<!-[$SCHOOL->twitter]->" target='_blank' title='Follow us on Twitter'><span class="twitter"></span></a>
            <!-[/if]-> 
            
            <!-[if $SCHOOL->facebook]-> 
                <a href="<!-[$SCHOOL->facebook]->" target='_blank' title='Follow us on Facebook'><span class="fb"></span></a>         
            <!-[/if]-> 
            </li></ul>
           <!-[/nocache]->  
            </div>
</div>          

</div>
</body>
</html>