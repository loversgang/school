
<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
        <!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, demo,responsive-slider']->
        <!-[add_js name='jquery.min,newsletter, bootstrap,responsive-slider,jquery-latest.pack,jcarousellite_1.0.1c4,jquery.event.move']->    

        

    </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->

        <!-- Home Page Banner -->
        <!-[include file="../include/banner.tpl" ]->            

                     
            <div class="row-fluid">
            <div class="span8">
                       <div class="row-fluid" id="home-id">
                           <div class="span6">
                             <img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->img/info.png" alt=""/><h1><!-[$ABOUT->name]-></h1>
                             <div id='home_about'>
                                <!-[if !empty($ABOUT->description)]->                                 
                                                    <!-[html_entity_decode($ABOUT->description)]->
                                 <!-[else]->  
                                            <p><br/>Coming Soon..!</p>
                                <!-[/if]-> 
                             </div>                            
                             <div class='lern_more'>
                                <!-[if (strlen($ABOUT->description) > 1000)]->
                                        <a href="<!-[make_url page=content query='id='|cat:$ABOUT->id]->" title='Read More'> Read more <img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->img/read-more.png" alt=""/></a>
                                <!-[/if]->        
                             </div> 
                           </div>  
                                           
                        
                           <div class="span6">
                             <img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->img/staff.png" alt=""/><h1><!-[$AWARDS->name]-></h1>
                             <div id='home_about'>
                                <!-[if !empty($AWARDS->description)]->                                 
                                                    <!-[html_entity_decode($AWARDS->description)]->
                                 <!-[else]->  
                                            <p><br/>Coming Soon..!</p>
                                <!-[/if]-> 
                             </div>
                             <div class='lern_more'>
                                <!-[if (strlen($AWARDS->description) > 1000)]->
                                        <a href="<!-[make_url page=content query='id='|cat:$AWARDS->id]->" title='Read More'> View More..</a>
                                <!-[/if]->        
                             </div> 
                           </div> 
                            <div class="clearfix"></div>
                            <hr/>                           
                        </div>                       
                    

                    <div id="gallery">
                        <!-- Gallery Part -->
                        <!-[include file="../include/gallery.tpl" title=Gallery]-> 
                     </div>  
              </div>
             <div class="span4">
                        <!-- Right Notice Board -->
                        <!-[include file="../include/right_notice.tpl" title=Notice]->                 
                        
                        <!-- Admission Part -->
                        <!-[include file="../include/admission.tpl" title=Admission]->

                        <!-- School Timing Part -->
                        <!-[include file="../include/timing.tpl" title=Timing]->

            </div>                     
       </div>
                     
    

<!-[include file="../include/footer.tpl" title=footer]->
   