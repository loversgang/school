
    
    <div class="span4">
            <h5>Photo Gallery</h5>
            <div class="well-grey" id="gallery">
            <div class="newsticker-jcarousellite">
                
                    
                    <!-[if count($ALL_GALLERY)]->    
                    <ul>
                        <!-[foreach from=$ALL_GALLERY key=Agk item=Agv]->
                         <li>
                            <div class="thumbnail">
                            <!-[if $Agv->image]->
                               <a href="<!-[make_url page=gallery]->" data-fancybox-group="gallery">
                                        <img src="<!-[$smarty.const.DIR_WS_SITE_PATH]->upload/photo/gallery/medium/<!-[$Agv->image]->" alt=""/>
                               </a>    
                             <!-[else]-> 
                                   <img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->noimage.jpg" alt="" width="205" height="123"/>                                
                             <!-[/if]->    
                            </div>
                            <div class="clear"></div> 
                         </li>
                        <!-[/foreach]-> 
                    </ul>
                    <!-[else]-> 
                       <p> Sorry, No Record Found..!</p>
                    <!-[/if]-> 
    
                    
            </div>
            </div>
            </div>