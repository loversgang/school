             <!-[if count($RANDOM_STAFF)]->
                <div id="teacher" class="row-fliud">
                <div class="well-grey">
                    <h4>Teachers</h4>
					<div class="row-fluid">
					<!-[foreach from=$RANDOM_STAFF key=rsk item=rsv]->
                                        <div class='span6'>
							<div class='side_staff_imge'>
								<!-[if $rsv->photo]->
									<img src="<!-[$smarty.const.DIR_WS_SITE_PATH]->upload/photo/staff/medium/<!-[$rsv->photo]->" alt=""/>
								<!-[else]-> 
									<img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->no_staff.jpg" alt=""/>
								<!-[/if]->
							</div>
							<div id='staff_side_name'>
								<!-[$rsv->title|cat:' '|cat:$rsv->first_name|cat:' '|cat:$rsv->last_name]-><br/>
							 <span><!-[$rsv->designation]-></span>
						    </div>
					</div>
                   <!-[/foreach]->
				   </div><br/>
                   <a href="<!-[make_url page=staff]->" class="btn-orange" title='View More Staff'>Read More</a>
                <div class="clearfix"></div>
                </div>
            </div>
	<!-[/if]->