<body>
            	<div class="container">
                    
                    
                       <div class="row-fluid">
                        <div class="span9" id="logo">
                         <!-[nocache]->  
                            <a href="<!-[make_url page='home']->" title='Home'>
							
                            <!-[if $SCHOOL->logo]->
											<div class='span3'>
												<img src="<!-[$smarty.const.DIR_WS_SITE_PATH]->upload/photo/school/medium/<!-[$SCHOOL->logo]->" alt="" />
                                            </div>
											<div class='span6'>	
												<div class='head_name'><!-[$SCHOOL->school_name]-></div>
												<div class='head_bottm'><!-[$SCHOOL->school_type]-></div>  
											</div>	
                                
                                <!-[else]-> <div class='span9'>	
                                            <div class='head_name'><!-[$SCHOOL->school_name]-></div>
                                            <div class='head_bottm'><!-[$SCHOOL->school_type]-></div>
											</div>	
                            <!-[/if]->                         
                        </a>                           
                         <!-[/nocache]->      
  
                        </div>
                        <div class="span3" id="contact-link">
                                <div class="well-black">
                            <center>
                                <a href="tel:<!-[$SCHOOL->school_phone]->"><span>Call Us:&nbsp;</span><!-[$SCHOOL->school_phone]-></a>
                                </center>
                            </div>
                        </div>
                      </div>
                    
                    
          <div class="row-fluid">
            <div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </a>
                 
                 
                  <div class="nav-collapse collapse navbar-responsive-collapse">
                   <!-[nocache]->  
                      <ul class="nav">                                                                                            
                      <li class="<!-[if $PAGE eq 'home']->active<!-[/if]->"><a href="<!-[make_url page=home]->" title='Home'>Home</a></li>
                      <li class="<!-[if $PAGE eq 'gallery']->active<!-[/if]->"><a href="<!-[make_url page=gallery]->" title='Gallery'>Gallery</a></li>
                                          <!-[if $INFRASTRUCTURE]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $INFRASTRUCTURE->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$INFRASTRUCTURE->id]->" title='<!-[$INFRASTRUCTURE->name]->'><!-[$INFRASTRUCTURE->name]-></a></li>
                                          <!-[/if]->
                                          
                                           <!-[if $AWARDS]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $AWARDS->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$AWARDS->id]->" title='<!-[$AWARDS->name]->'><!-[$AWARDS->name]-></a></li>
                                          <!-[/if]->  
                                          
                                           <!-[if $INFO]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $INFO->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$INFO->id]->" title='<!-[$INFO->name]->'><!-[$INFO->name]-></a></li>
                                          <!-[/if]->
                                          
                       <li class="<!-[if $PAGE eq 'contact']->active<!-[/if]->"><a href="<!-[make_url page=contact]->" title='Contact Us'>Contact Us</a></li>
                    </ul>
               <!-[/nocache]->  
                  </div><!-- /.nav-collapse -->
                </div>
              </div><!-- /navbar-inner -->
            </div><!-- /navbar -->       
          </div>                   
                    
                    
                	
                    
                    
   