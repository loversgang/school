             <div class="span8" id="notice">
                <h5>Notice Board</h5>
                 <div class="well-grey" >
                    <div class="newsticker-jcarousellite">
                    <!-[if count($NOTICE)]->    
                    <ul>
                        <!-[foreach from=$NOTICE key=nk item=nv]->
                         <li>
                                <a href="<!-[make_url page=notice_detail query='id='|cat:$nv->id]->"><!-[$nv->name]-></a>
                                <h6 class='small_notice_date'>On Date: <!-[date('d M, Y',strtotime($nv->on_date))]-></h6>
                                <p><!-[$nv->short_description]-></p>
                                <div class="border-btm"></div>
                                <div class="clear"></div>           
                         </li>
                        <!-[/foreach]-> 
                    </ul>
                    <!-[else]-> 
                       <p> Sorry, No Record Found..!</p>
                    <!-[/if]-> 
            </div>
                   
                </div>
            </div>