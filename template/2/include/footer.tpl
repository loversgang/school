          <div class="footer">
          	<div class="row-fluid">
            	<small>Copyright  <!-[date('Y')]-></small>
                
                <div class="pull-right">
                	<span> Follow Us</span>
                        <!-[if $SCHOOL->google]->
                            <a href="<!-[$SCHOOL->google]->" target='_blank' title='Follow us on Google' class="google">
                                <img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->img/feed.png" alt=""/>
                            </a>
                        <!-[/if]->
                        <!-[if $SCHOOL->facebook]-> 
                            <a href="<!-[$SCHOOL->facebook]->" target='_blank' title='Follow us on Facebook' class="fb">
                               <img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->img/fb.png" alt=""/>
                            </a>
                        <!-[/if]->
                        <!-[if $SCHOOL->twitter]->
                            <a href="<!-[$SCHOOL->twitter]->" target='_blank' title='Follow us on Twitter' class="twitter">
                                <img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->img/twitter.png" alt=""/>
                            </a>
                        <!-[/if]->                        
                      
                </div>
            </div>
          </div>
</div>