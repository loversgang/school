
<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
        <!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, demo,responsive-slider']->
        <!-[add_js name='jquery.min,newsletter, bootstrap,responsive-slider,jquery-latest.pack,jcarousellite_1.0.1c4,jquery.event.move']->    
        <link href='http://fonts.googleapis.com/css?family=Economica' rel='stylesheet' type='text/css'>
        
         <!-- Notice Board Jquery Part -->
         <!-[include file="../include/nitice_jquery.tpl" title=Jquery]->
    </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->

        <!-- Home Page Banner -->
        <!-[include file="../include/banner.tpl" ]->            

                     
       <div class="row-fluid" style="margin-top:20px;">
            <div class="span8">
                       <div class="row-fluid">
                           
                             <h5><!-[$ABOUT->name]-></h5>
                             <div id='home_about'>
                                <!-[if !empty($ABOUT->description)]->                                 
                                                    <!-[html_entity_decode($ABOUT->description)]->
                                 <!-[else]->  
                                            <p><br/>Coming Soon..!</p>
                                <!-[/if]-> 
                             </div>
                             <div class='lern_more'>
                                <!-[if $ABOUT->description]->
                                        <a href="<!-[make_url page=content query='id='|cat:$ABOUT->id]->"> View More..</a>
                                <!-[/if]->        
                             </div>  
                        </div>                        
                        
            
                     <div id="mid-panel" class="row-fluid" style="margin-top:20px;">
                        <!-- Gallery Part -->
                        <!-[include file="../include/gallery.tpl" title=Gallery]->

                        <!-- Notice Board Part -->
                        <!-[include file="../include/notice.tpl" title=Notice_Board]->                        
                        
                     </div>  
              </div>
             <div class="span4" id="mid">
                         <!-- Admission Part -->
                        <!-[include file="../include/admission.tpl" title=Admission]->

                        <!-- School Timing Part -->
                        <!-[include file="../include/timing.tpl" title=Timing]->
                        
                        <!-- Teacher Part -->
                        <!-[include file="../include/teacher.tpl" title=Teacher]->                         

            </div>                     
       </div>
                     
    

<!-[include file="../include/footer.tpl" title=footer]->
   