<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
        <!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, demo,responsive-slider']->
        <!-[add_js name='jquery.min,newsletter, bootstrap,responsive-slider,jquery-latest.pack,jcarousellite_1.0.1c4,jquery.event.move,custom_validation']->    
        <link href='http://fonts.googleapis.com/css?family=Economica' rel='stylesheet' type='text/css'>
         
         <!-- Notice Jquery Part -->
         <!-[include file="../include/nitice_jquery.tpl" title=Jquery]->
         
        </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->   
      
        <div class="row-fluid" style="margin-top:20px;">
            <div class="span8">
                       <div class="row-fluid" >
                            <h5>Contact Us</h5>
                            	<!-[nocache]->
                                    <!-[if !empty($MSG)]-> 
                                        <div class='error_msg'> <!-[$MSG]-></div>
                                    <!-[/if]->
                                <!-[nocache]-> 
                                  <div class="row-fluid">
                                    <!--  
                                    <iframe id="map_id" width="618" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/?ie=UTF8&amp;ll=20.983588,82.752628&amp;spn=44.539231,86.572266&amp;t=m&amp;z=4&amp;output=embed"></iframe>
                                    -->
                                    <iframe id="map_id" width="618" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<!-[$SCHOOL->school_name]->,<!-[$SCHOOL->address1]->,<!-[$SCHOOL->city]->,<!-[$SCHOOL->state]->,<!-[$SCHOOL->country]->&amp;aq=2&amp;&amp;sspn=0.011221,0.021136&amp;ie=UTF8&amp;t=m&amp;z=9&amp;output=embed"></iframe>
            
                                  </div><br/>
                                 <div id="contact" class="well-grey">
                                        <form class="form-horizontal" id='contact_form' method="POST">
                                            <div class="control-group">
                                            <label class="control-label">Name:</label>
                                            <div class="controls">
                                            <input type="text" name='name' class='span10' id='name'/>
                                            </div>
                                            </div>

                                            <div class="control-group">
                                            <label class="control-label">Email:</label>
                                            <div class="controls">
                                            <input type="text" class='span10' name='email' id='email'/>
                                            </div>
                                            </div>



                                            <div class="control-group">
                                            <label class="control-label">Message:</label>
                                            <div class="controls">
                                            <textarea cols="7" class='span10' rows="5" name='message' id='message'></textarea>
                                            </div>
                                            </div>
											
											<!-[nocache]->
                                            <div class="control-group">
                                            <label class="control-label" style="padding-top:0px;"><img src="<!-[$smarty.const.DIR_WS_SITE_PATH]->captcha/captcha_code_file.php"/ >:</label>
                                            <div class="controls">
                                            <input type="text" class='span10' name='captcha' id='captcha'/>
                                            </div>
                                            </div>
											<!-[nocache]->
											
                                            <div class="control-group">
                                            <div class="controls">
                                            <button class="btn-orange" id='check_form' name='submit' type="submit">Submit</button>
                                            </div>
                                            </div>
                                        </form>

                                 </div>                                
                                                                    
                        </div>                    
              </div>
             <div class="span4" id="mid">
                         <!-- Admission Part -->
                        <!-[include file="../include/admission.tpl" title=Admission]->
                        
                       

                         <!-- Right Box Notice Board Part -->
                        <!-[include file="../include/right_notice.tpl" title=Notice]->

            </div>                     
       </div>
                     
    

<!-[include file="../include/footer.tpl" title=footer]->
   
     