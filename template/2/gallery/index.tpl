<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
		<!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, demo,responsive-slider,jquery.fancybox']->
        <!-[add_js name='jquery.min,newsletter, bootstrap,jcarousellite_1.0.1c4,jquery.event.move,jquery.fancybox']->    
        <link href='http://fonts.googleapis.com/css?family=Economica' rel='stylesheet' type='text/css'>
  
        <!-- Gallery Paging Jquery Part -->
         <!-[include file="../gallery/gallery_paging_jquery.tpl" title=Jquery]->
         
        <!-- Notice Jquery Part -->
         <!-[include file="../include/nitice_jquery.tpl" title=Jquery]->
         
        	<script type="text/javascript">
			$(document).ready(function() {
				$('.fancybox').fancybox({					
				openEffect  : 'fade',
				closeEffect	: 'fade',

				prevEffect : 'fade',
				nextEffect : 'fade'
					});	
			});
                </script>    
    </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->   
      
        <div class="row-fluid" style="margin-top:20px;">
            <div class="span8">
                       <div class="row-fluid" >
                            <h5 style="margin-bottom:20px;">Gallery</h5>
                                                 <!-[nocache]-> 
                                                     <div id="gallery" class="row-fluid"> 
                                                      <!-[include file=$smarty.const.DIR_FS_SITE_TEMPLATE_GALLERY|cat:'gallery.tpl' title=gallery]-> 
                                                     </div> 
                                                  <!-[/nocache]->                            
                        </div>                    
              </div>
             <div class="span4" id="mid">
                         <!-- Admission Part -->
                        <!-[include file="../include/admission.tpl" title=Admission]->
                        
                        <!-- Teacher Part -->
                        <!-[include file="../include/teacher.tpl" title=Teacher]->  

                         <!-- Right Box Notice Board Part -->
                        <!-[include file="../include/right_notice.tpl" title=Notice]->

            </div>                     
       </div>
                     
    

<!-[include file="../include/footer.tpl" title=footer]->
   