<body>
	<div class="top-line"></div>
	<div class="container-1" style='margin-bottom:0px;'>
	<div class="container">
             <div class="row-fluid">                           
               	<div class="header">
            	<div class="span6" id="logo">
                     <!-[nocache]->  
                    <a href="<!-[make_url page='home']->" title='Home'>
                        <!-[if $SCHOOL->logo]->
                            <div class='span4'>
								<img src="<!-[$smarty.const.DIR_WS_SITE_PATH]->upload/photo/school/medium/<!-[$SCHOOL->logo]->" alt="" />
							</div>
							<div class='span8'>	                                
								<div id='school_name'><!-[$SCHOOL->school_name]-></div>
                                <div id='school_type'><!-[$SCHOOL->school_type]-></div> 
							</div>			
                        <!-[else]-> 
							<div class='span9'>	
                                <div id='school_name'><!-[$SCHOOL->school_name]-></div>
                                <div id='school_type'><!-[$SCHOOL->school_type]-></div>
							</div>		
                        <!-[/if]->                         
                    </a>
                     <!-[/nocache]->  
                </div>
                <div class="span6">
                    <div class="row-fluid">
                        <div class="navbar">
                          <div class="navbar-inner">
                            <div class="container">
                              <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </a>
                              <div class="nav-collapse collapse navbar-responsive-collapse">
                                <ul class="nav">
                                  <li class="dropdown <!-[if $PAGE eq 'content' && ($ID eq $ABOUT->id || $ID eq $INFO->id || $ID eq $INFRASTRUCTURE->id || $ID eq $AWARDS->id)]->active<!-[/if]->">
                                    <a data-toggle="dropdown" class="dropdown-toggle <!-[if $PAGE eq 'content' && $ID eq $ABOUT->id]->active<!-[/if]->" href='#' title='About'>About</a>
                                     <!-[nocache]->      
                                    <ul class="dropdown-menu">
                                           <!-[if $ABOUT]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $ABOUT->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$ABOUT->id]->" title='<!-[$ABOUT->name]->'><!-[$ABOUT->name]-></a></li>
                                          <!-[/if]->                                               
                                           <!-[if $INFO]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $INFO->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$INFO->id]->" title='<!-[$INFO->name]->'><!-[$INFO->name]-></a></li>
                                          <!-[/if]->                                            
                                          <!-[if $INFRASTRUCTURE]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $INFRASTRUCTURE->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$INFRASTRUCTURE->id]->" title='<!-[$INFRASTRUCTURE->name]->'><!-[$INFRASTRUCTURE->name]-></a></li>
                                          <!-[/if]->
                                           <!-[if $AWARDS]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $AWARDS->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$AWARDS->id]->" title='<!-[$AWARDS->name]->'><!-[$AWARDS->name]-></a></li>
                                          <!-[/if]->  
                                        </ul>
                                     <!-[/nocache]->   
                                  </li>                                    
                                  <li class="<!-[if $PAGE eq 'gallery']->active<!-[/if]->"><a href="<!-[make_url page=gallery]->" title='Gallery'>Gallery</a></li>
                                  <li class="<!-[if $PAGE eq 'staff']->active<!-[/if]->"><a href="<!-[make_url page=staff]->" title='Faculty & Staff '>Faculty & Staff </a></li>
                                  <li class="<!-[if $PAGE eq 'contact']->active<!-[/if]->"><a href="<!-[make_url page=contact]->" title='Contact Us'>Contact Us</a></li>
                                  
                                </ul>
            
                              </div><!-- /.nav-collapse -->
                            </div>
                          </div><!-- /navbar-inner -->
                        </div><!-- /navbar -->
                    </div>
                </div>
            	</div>                        
            </div>
            </div>
            </div>
            <div class="top-line" style='margin-top:0px;'></div>
	<div class="container-1">
	<div class="container">            