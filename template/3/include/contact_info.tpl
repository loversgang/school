            <div class="row-fluid">
             <h1>Contact Info</h1><div class="clearfix"></div>
                	    <address>
                        <strong><!-[$SCHOOL->school_name]-></strong><br>
                        <!-[$SCHOOL->address1]-><br>
                        <!-[$SCHOOL->address2]->, <!-[$SCHOOL->city]->, <!-[$SCHOOL->state]->, <!-[$SCHOOL->country]-><br>
                        <abbr title="Phone">Phone:</abbr> <a href='tel:<!-[$SCHOOL->school_phone]->'><!-[$SCHOOL->school_phone]-></a>
                        </address>
                         
                        <address>
                        <strong>Email</strong><br>
                        <a href="mailto:<!-[$SCHOOL->email_address]->"><!-[$SCHOOL->email_address]-></a>
                        </address>

            
            </div>