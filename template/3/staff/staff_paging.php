<?php

include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');

isset($_POST['p']) ? $p = $_POST['p'] : $p = '1';
isset($_POST['total_pages']) ? $total_pages = $_POST['total_pages'] : $total_pages = '1';
isset($_POST['max_records']) ? $max_records = $_POST['max_records'] : $max_records = '10';

$staff = array();
$QueryStaff = new staff();
$staff = $QueryStaff->getSchoolStaffFront($school->id, $p, $max_records);


$staff_array = $staff['staff'];
$total_records = $staff['TotalRecords'];
$total_pages = $staff['TotalPages'];

$smarty->assign_notnull("STAFF", $staff_array, true);
$smarty->assign_notnull("P", $p, true);
$smarty->assign_notnull("MAX_RECORDS", $max_records, true);
$smarty->assign_notnull("TOTAL_RECORDS", $total_records, true);
$smarty->assign_notnull("TOTAL_PAGES", $total_pages, true);

/* * *************gallery page query ends here************************** */

$smarty->setTemplateDirectory('staff');
$smarty->renderLayout('staff');
?>
