<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(document).delegate(".paging_change", "click", function () {
            var lin = '<div style="width:100%;height:100%;text-align:center;margin-top:10%;margin-bottom:15%;"><img src="<!--[$smarty.const.DIR_WS_SITE_GRAPHIC]-->loading-ajax.gif"></div>';
            jQuery(".container #fill_gallery").html(lin);
            var page = $(this).attr('id');
            var total_pages = $(this).attr('rel');
            var max_records = $(this).attr('max_records');
            var dataString = 'p=' + page + '&total_pages=' + total_pages + '&max_records=' + max_records;

            jQuery.ajax({
                type: "POST",
                url: "<!-[make_url page='ajax' query='action=staff_paging']->",
                data: dataString,
                success: function (data, textStatus) {
                    $(".container #gallery").hide();
                    $(".container #gallery").html(data);
                    $(".container #gallery").fadeIn(1500);
                }
            })
        })
    });
</script>