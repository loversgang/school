
<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
        <!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, validationEngine.jquery']->
        <!-[add_js name='jquery.min, jquery.validationEngine, jquery.validationEngine-en,  newsletter, bootstrap']->        
    </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->

                             
       <div class="row-fluid">
                     <div class="span12" id="right-panel">
						<div class="well-white not_found">
                                                <div class='breadcrumb'> 
                                                    <!-[nocache]->  
                                                    <a href="<!-[make_url page=home]->" class="link"> Home</a> >> 
                                                    <!-[$CONTENT->name]->
                                                    <!-[/nocache]->  
                                                </div>                                                     
						<h4><!-[$CONTENT->name]-></h4>
							<!-[html_entity_decode($CONTENT->description)]->
						</div>
                     </div>                    
       </div>
                     
      
</div>

<!-[include file="../include/footer.tpl" title=footer]->
   