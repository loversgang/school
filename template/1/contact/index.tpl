
<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
        <!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, validationEngine.jquery']->
        <!-[add_js name='jquery.min, jquery.validate.min, script,  newsletter, bootstrap,custom_validation']->        
    </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->

<!-- jQuery via Google + local fallback, see h5bp.com -->
	  <script src="assets/js/jquery-1.7.1.min.js"></script>



                             
       <div class="row-fluid">
                     <div class="span12" id="right-panel">
						<div class="well-white not_found">
                                                <div class='breadcrumb'> 
                                                    <!-[nocache]->  
                                                    <a href="<!-[make_url page=home]->" class="link"> Home</a> >> 
                                                    Contact Us
                                                    <!-[/nocache]->  
                                                </div>                                                     
						<!-[nocache]->
                                                    <!-[if !empty($MSG)]-> 
                                                        <div class='error_msg'> <!-[$MSG]-></div>
                                                 <!-[/if]->
                                                <!-[/nocache]-> 
                                                <div class="well-white" id="contact">
                                                        <h4>Contact Us</h4>
                                                        <div class="row-fluid">
                                                        <!--
                                                            <iframe id="map_id" width="852" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/?ie=UTF8&amp;ll=20.983588,82.752628&amp;spn=44.539231,86.572266&amp;t=m&amp;z=4&amp;output=embed"></iframe>
                                                        -->
                                                        <iframe width="852" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<!-[$SCHOOL->school_name]->,<!-[$SCHOOL->address1]->,<!-[$SCHOOL->city]->,<!-[$SCHOOL->state]->,<!-[$SCHOOL->country]->&amp;aq=2&amp;&amp;sspn=0.011221,0.021136&amp;ie=UTF8&amp;t=m&amp;z=9&amp;output=embed"></iframe>
            
                                                        </div>

                                                         <div class="row-fluid">
                                                               <form class="form-horizontal" method="POST">
                                                                <input type="text" name='name' id='name' class="span6 validate[required]" placeholder="Name">
                                                                <input type="text" name='email' id='email' class="span6 validate[required]" placeholder="Email">
                                                                <textarea  name='message' id='message' rows="4"  class="span12 validate[required]" placeholder="Message" style='width:99%'></textarea>
                                  										
											<!-[nocache]->
                                            <div class="control-group">
                                            <label class="control-label" style="padding-top:0px;"><img src="<!-[$smarty.const.DIR_WS_SITE_PATH]->captcha/captcha_code_file.php"/ >:</label>
                                            <div class="controls">
                                            <input type="text" class='span2' name='captcha' placeholder="Enter Code" id='captcha' style='margin-top:0px;'/>
                                            </div>
                                            </div>
											<!-[nocache]->
										
                                                                <button type="submit" name='submit' id='check_form' class="btn btn-danger">Submit</button>
                                                                </form>
                                                             
                                                         </div>
                                                 </div>
                    
						</div>
                     </div>                    
       </div>
                     
      
</div>

<!-[include file="../include/footer.tpl" title=footer]->
   