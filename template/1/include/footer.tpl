	<div class="footer">
		<div class="container">
			<div class="row-fluid">
				<center>                    
				 <!-[nocache]->  	
                                    <ul>
					<li>
                                          <a class="<!-[if $PAGE eq 'home']->active<!-[/if]->" href="<!-[make_url page=home]->">Home</a>                                          
                                          <!-[if $ABOUT]->
                                           <a class="<!-[if $PAGE eq 'content' && $ID eq $ABOUT->id]->active<!-[/if]->" href="<!-[make_url page=content query='id='|cat:$ABOUT->id]->"><!-[$ABOUT->name]-></a>
                                          <!-[/if]->
                                          
                                          <!-[if $INFRASTRUCTURE]->
                                            <a class="<!-[if $PAGE eq 'content' && $ID eq $INFRASTRUCTURE->id]->active<!-[/if]->" href="<!-[make_url page=content query='id='|cat:$INFRASTRUCTURE->id]->"><!-[$INFRASTRUCTURE->name]-></a>
                                          <!-[/if]->
                                          
                                           <!-[if $AWARDS]->
                                            <a class="<!-[if $PAGE eq 'content' && $ID eq $AWARDS->id]->active<!-[/if]->" href="<!-[make_url page=content query='id='|cat:$AWARDS->id]->"><!-[$AWARDS->name]-></a>
                                          <!-[/if]->                                         
 
                                          <!-[if $INFO]->
                                            <a class="<!-[if $PAGE eq 'content' && $ID eq $INFO->id]->active<!-[/if]->" href="<!-[make_url page=content query='id='|cat:$INFO->id]->"><!-[$INFO->name]-></a>
                                          <!-[/if]->  
                                        </li>
					</ul>
                                  <!-[/nocache]->  
					<span>Copyright © <!-[date('Y')]->  <!-[$smarty.const.SITE_NAME]-></span>
				</center>
			</div>
		</div>
	</div>
<div class="clearfix"></div>   