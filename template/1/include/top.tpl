<body>
<img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->img/bg.png" class="bg">
            	<div class="container">
                	<div class="row-fluid">
                    		<div class="header">
                                <div class="span10" id="logo">
                                     <!-[nocache]->  
                                    <a href="<!-[make_url page='home']->" title='Home'>
									 <div class='span3'>
                                        <!-[if $SCHOOL->logo]->
                                            <img src="<!-[$smarty.const.DIR_WS_SITE_PATH]->upload/photo/school/medium/<!-[$SCHOOL->logo]->" alt="" />
                                        <!-[else]-> 
                                            <img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->img/logo-sign.png" alt=""/>
                                        <!-[/if]->    
                                      </div>
									<div class='span9'>									  
                                    <h4><!-[$SCHOOL->school_name]-></h4>
                                    <h6><!-[$SCHOOL->school_type]-></h6>
									</div>
                                    </a>
                                      <!-[/nocache]->  
                                </div>
                                                       
                                <div class="span2">
                                    <div class="link-box">
                                        <!-[if $SCHOOL->google]->
                                            <a href="<!-[$SCHOOL->google]->" target='_blank' title='Follow us on Google' class="google"></a>
                                        <!-[/if]->
                                        <!-[if $SCHOOL->facebook]-> 
                                            <a href="<!-[$SCHOOL->facebook]->" target='_blank' title='Follow us on Facebook' class="fb"></a>
                                        <!-[/if]->
                                        <!-[if $SCHOOL->twitter]->
                                            <a href="<!-[$SCHOOL->twitter]->" target='_blank' title='Follow us on Twitter' class="twitter"></a>
                                        <!-[/if]->
                                    </div>
                                    <!--
                                        <div class="row-fluid">
                                        <div class="input-append pull-right">
                                        <input class="span8" id="appendedInputButton" type="text">
                                        <button class="btn search-blk" type="button">Search</button>
                                        </div>
                                        </div>
                                    -->
                                </div>
                                
                            <div class="clearfix"></div>
                   			 </div>
                             
                             <div class="row-fluid">
                                <div class="navbar">
                                  <div class="navbar-inner">
                                    <div class="container">
                                      <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                      </a>
                                                                                                                                                       
                                      <div class="nav-collapse collapse navbar-responsive-collapse">
                                      <!-[nocache]->    
                                        <ul class="nav">
                                          <li class="<!-[if $PAGE eq 'home']->active<!-[/if]->"><a href="<!-[make_url page=home]->" title='Home'>Home</a></li>
                                          
                                          <!-[if $ABOUT]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $ABOUT->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$ABOUT->id]->" title='<!-[$ABOUT->name]->'><!-[$ABOUT->name]-></a></li>
                                          <!-[/if]->
                                          
                                          <!-[if $INFRASTRUCTURE]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $INFRASTRUCTURE->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$INFRASTRUCTURE->id]->" title='<!-[$INFRASTRUCTURE->name]->'><!-[$INFRASTRUCTURE->name]-></a></li>
                                          <!-[/if]->
                                          
                                           <!-[if $AWARDS]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $AWARDS->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$AWARDS->id]->" title='<!-[$AWARDS->name]->'><!-[$AWARDS->name]-></a></li>
                                          <!-[/if]->                                         
 
                                          
                                           <!-[if $INFO]->
                                            <li class="<!-[if $PAGE eq 'content' && $ID eq $INFO->id]->active<!-[/if]->"><a href="<!-[make_url page=content query='id='|cat:$INFO->id]->" title='<!-[$INFO->name]->'><!-[$INFO->name]-></a></li>
                                          <!-[/if]->  
                                          <li class="<!-[if $PAGE eq 'staff']->active<!-[/if]->"><a href="<!-[make_url page=staff]->" title='Faculty & Staff'>Faculty & Staff</a></li>
                                          <li class="<!-[if $PAGE eq 'gallery']->active<!-[/if]->"><a href="<!-[make_url page=gallery]->" title='Gallery'>Gallery</a></li>
                                          <li class="<!-[if $PAGE eq 'contact']->active<!-[/if]->"><a href="<!-[make_url page=contact ]->" title='Contact Us'>Contact Us</a></li>
                                       </ul>
                                     <!-[/nocache]->
                                        
                                      </div><!-- /.nav-collapse -->
                                    </div>
                                  </div><!-- /navbar-inner -->
                                </div><!-- /navbar -->
                              </div>
           		     </div>
                    
                    
   