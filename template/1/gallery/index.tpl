
<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
        <!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, validationEngine.jquery,jquery.fancybox']->
        <!-[add_js name='jquery.min, jquery.validationEngine, jquery.validationEngine-en,  newsletter, bootstrap,jquery.fancybox']->        
    	<script type="text/javascript">
			$(document).ready(function() {
	 
				/*
				 *  Simple image gallery. Uses default settings
				 */
	
				$('.fancybox').fancybox({
					// Disable opening and closing animations, change title type
		
				openEffect  : 'fade',
				closeEffect	: 'fade',

				prevEffect : 'fade',
				nextEffect : 'fade'
					});
	
			});
	</script>    
   <script type="text/javascript">
    jQuery(document).ready(function() {
    jQuery(document).delegate(".paging_change","click",function(){ 
    var lin='<div style="width:100%;height:100%;text-align:center;margin-top:10%;margin-bottom:15%;"><img src="<!--[$smarty.const.DIR_WS_SITE_GRAPHIC]-->loading-ajax.gif"></div>';
    jQuery(".container #fill_gallery").html(lin);
    var page=$(this).attr('id');
    var total_pages=$(this).attr('rel');
    var max_records=$(this).attr('max_records');
    var dataString = 'p='+page+'&total_pages='+total_pages+'&max_records='+max_records;

    jQuery.ajax({
    type: "POST",
    url: "<!-[make_url page='ajax' query='action=gallery_paging']->",
    data: dataString,
    success: function(data, textStatus) {
    $(".container #gallery").hide();
    $(".container #gallery").html(data);
    $(".container #gallery").fadeIn(1500);
    }
    })
})
});
</script>       
        </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->

                             
       <div class="row-fluid">
                     <div class="span12" id="right-panel">
						<div class="well-white not_found">
                                                <div class='breadcrumb'> 
                                                    <!-[nocache]->  
                                                    <a href="<!-[make_url page=home]->" class="link"> Home</a> >> 
                                                    Gallery
                                                    <!-[/nocache]->  
                                                </div>    
                                                <h4>Gallery</h4> 
                                                  <!-[nocache]-> 
                                                     <div id="gallery" class="row-fluid"> 
                                                      <!-[include file=$smarty.const.DIR_FS_SITE_TEMPLATE_GALLERY|cat:'gallery.tpl' title=gallery]-> 
                                                     </div> 
                                                  <!-[/nocache]->   
                                                </div>                    
                    </div>
    </div>
    </div>       

<!-[include file="../include/footer.tpl" title=footer]->
   