<?php

include_once(DIR_FS_SITE.'include/functionClass/galleryClass.php');

isset($_POST['p'])?$p=$_POST['p']:$p='1';
isset($_POST['total_pages'])?$total_pages=$_POST['total_pages']:$total_pages='1';
isset($_POST['max_records'])?$max_records=$_POST['max_records']:$max_records='10';

$gallery=array();
$query_gallery= new gallery();
$gallery=$query_gallery->getGalleries($school_id,$p,$max_records);

$gallery_array=$gallery['gallery'];
$total_records=$gallery['TotalRecords'];
$total_pages=$gallery['TotalPages'];


$smarty->assign_notnull("GALLERY",$gallery_array,true);
$smarty->assign_notnull("P",$p,true);
$smarty->assign_notnull("MAX_RECORDS",$max_records,true);
$smarty->assign_notnull("TOTAL_RECORDS",$total_records,true);
$smarty->assign_notnull("TOTAL_PAGES",$total_pages,true);

/***************gallery page query ends here***************************/

$smarty->setTemplateDirectory('gallery');
$smarty->renderLayout('gallery');
?>
