                                                          <ul>
                                                             <!-[if count($STAFF)]->
                                                             <!-[assign var="j" value=1]->
                                                             <!-[assign var="STAFF_CAT" value='']->
                                                              <li>
                                                                  <div class="row-fluid">                                     
                                                                    <!-[foreach from=$STAFF key=gk item=gv]->                                                                            
                                                                            <!-[if $STAFF_CAT !=$gv->staff_category]->
                                                                                </div>
                                                                                <div class="row-fluid" id='staff_cat_name'> - <!-[$gv->staff_category]-> -</div><div style='clear:both'></div>
                                                                                <div class="row-fluid">     
                                                                             <!-[/if]-> 
                                                                                <div class="span6">                                                       
                                                                                  <div class="row-fluid" id='gray_back'> 
                                                                                  <div class="span5"> 
                                                                                    <center>
                                                                                            <!-[if $gv->photo]->
                                                                                                <a class="fancybox" href="<!-[$smarty.const.DIR_WS_SITE_PATH]->upload/photo/staff/large/<!-[$gv->photo]->" data-fancybox-group="STAFF">  
                                                                                                    <div class="well no_bottom_margin">
                                                                                                        <img src="<!-[$smarty.const.DIR_WS_SITE_PATH]->upload/photo/staff/medium/<!-[$gv->photo]->" alt=""/>
                                                                                                    </div>
                                                                                                </a>    
                                                                                             <!-[else]-> 
                                                                                                <div class="well no_bottom_margin">
                                                                                                   <img src="<!-[$smarty.const.DIR_WS_SITE_GRAPHIC]->no_staff.jpg" alt=""/>
                                                                                                </div>   
                                                                                             <!-[/if]->                                                                                
                                                                                    </center>
                                                                                    </div>
                                                                                    <div class="span7"> 
                                                                                        <span class="staff_name"><!-[$gv->title|cat:' '|cat:$gv->first_name|cat:' '|cat:$gv->last_name]-></span>
                                                                                        <span class="staff_job_title"><!-[$gv->designation]-></span>
                                                                                        <span class="staff_email"><a href='mailto:<!-[$gv->email]->'><!-[$gv->email]-></a></span>
                                                                                        <span class="staff_phone"><a href='tel:<!-[$gv->mobile]->'><!-[$gv->mobile]-></a></span>
                                                                                    </div> 
                                                                                 </div>   
                                                                                </div>

                                                                  <!-[if $j is div by 2]->
                                                                        </div>
                                                                        </li>
                                                                        <li><div class="row-fluid">   
                                                                  <!-[/if]->                                        
                                                                  <!-[assign var="j" value=$j+1]->
                                                                  <!-[assign var="STAFF_CAT" value=$gv->staff_category]->
                                                                  <!-[/foreach]-> 
                                                       <!-[else]->                
                                                            <h5 id="not_found">Sorry No Record Found..!</h5> 
                                                              <div class="clearfix"></div>                                 
                                                       <!-[/if]->                                        
                                                           <div class="clearfix"></div>        
                                                          </ul>
                                                        <!-[if $TOTAL_PAGES gt 1]->
                                                                <!-[paging_ajax page=$P url=STAFF total_pages=$TOTAL_PAGES total_records=$TOTAL_RECORDS max_records=$MAX_RECORDS]->
                                                         <!-[/if]-> 