
<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
        <!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, validationEngine.jquery']->
        <!-[add_js name='jquery.min, jquery.validationEngine, jquery.validationEngine-en,  newsletter, bootstrap']->        
    </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->

                             
       <div class="row-fluid">
                     <div class="span12" id="right-panel">
						<div class="well-white not_found">
                                                <div class='breadcrumb'> 
                                                    <!-[nocache]->  
                                                    <a href="<!-[make_url page=home]->" class="link"> Home</a> >> 
                                                    <a href="<!-[make_url page=notice]->" class="link"> Notice Board</a> >>                                                     
                                                    <!-[$NOTICE_DETAIL->name]->
                                                    <!-[/nocache]->  
                                                </div>  
						<h4><!-[$NOTICE_DETAIL->name]-></h4>
                                                <h6 class='for_notice_date'>On Date: <!-[date('d M, Y',strtotime($NOTICE_DETAIL->on_date))]-></h6>
							<!-[html_entity_decode($NOTICE_DETAIL->description)]->
						</div>
                     </div>                    
       </div>
                     
      
</div>

<!-[include file="../include/footer.tpl" title=footer]->
   