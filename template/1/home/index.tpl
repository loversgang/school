
<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
        <!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, demo,responsive-slider']->
        <!-[add_js name='jquery.min, jquery.validationEngine, jquery.validationEngine-en,  newsletter, bootstrap']->        
    </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->

        <!-- Home Page Banner -->
        <!-[include file="../include/banner.tpl" ]->            

                     
       <div class="row-fluid" id="boxes">
            <div class="span4">
                        <div class="well-green">
                            <div class="home_news">
                            <h4><!-[$AWARDS->name]-></h4>
                             <!-[html_entity_decode($AWARDS->description)]->
                             </div>
                             <div class='lern_more'>
                                <!-[if $AWARDS->description]->
                                        <a href="<!-[make_url page=content query='id='|cat:$AWARDS->id]->"> View More..</a>
                                <!-[/if]->        
                             </div>  
                        </div>                        
                        <div class="shadow"></div>
             </div>
                     
             <div class="span4">
                        <div class="well-red">
                            <div class="home_news">
                            <h4><!-[$ADMISSION->name]-></h4>
                             <!-[html_entity_decode($ADMISSION->description)]->
                             </div>
                             <div class='lern_more'>
                                <!-[if $ADMISSION->description]->
                                    <a href="<!-[make_url page=content query='id='|cat:$ADMISSION->id]->"> View More..</a>
                                <!-[/if]->    
                             </div> 
                        </div>
                        
                        <div class="shadow"></div>
             </div>
                     
             <div class="span4">
                        <div class="well-yellow">
                            <div class="home_news">
                            <h4>School Timing</h4>
                              <!-[html_entity_decode($SCHOOL->school_time)]->
                             </div>
                                                       
                        </div>
                        
                        <div class="shadow"></div>
            </div>                     
       </div>
                     
      <div class="row-fluid">
            <div class="span8">
                     <div class="well-white">
                       <div class="for_home">  
                        <h4><!-[$ABOUT->name]-></h4>
                        <!-[html_entity_decode($ABOUT->description)]->
  
                       </div>
                          <div class='lern_more'>
                            <!-[if $ABOUT->description]->
                                 <a href="<!-[make_url page=content query='id='|cat:$ABOUT->id]->"> View More..</a>
                             <!-[/if]-> 
                        </div>
                          <div class="clearfix"></div>
                     </div>
            </div>
                     
            <div class="span4">
                 <center> 
                        <div class="well-white">
                           <div class="for_home">
                                <h4>Notice Board</h4>
                                <!-[if count($NOTICE)]->
                                 <!-[foreach from=$NOTICE key=nk item=nv]->   
                                    <div class="row-fluid">
                                     <a href="<!-[make_url page=notice_detail query='id='|cat:$nv->id]->" class="link"><!-[$nv->name]-></a>
                                     <p class="link2"><!-[$nv->short_description]-></p>
                                     <hr/>
                                    </div>
                                 <!-[/foreach]-> 
                                  <!-[else]-> 
                                   <p> Sorry, No Record Found..!</p>  
                                   <!-[/if]->   
                            </div> 
                          <div class='lern_more'>
                            <!-[if count($NOTICE)]->
                            <a href="<!-[make_url page=notice]->"> View More..</a>
                            <!-[/if]->                  
                          </div>        
                         </div>
                         
                         </div>
                </center>
            </div>   
      </div>
</div>

<!-[include file="../include/footer.tpl" title=footer]->
   