
<!--[config_load file="smarty.conf" section="setup"]--> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <meta http-equiv="cache-control" content="no-cache" /> 
        <!-[nocache]-><!-[head]-><!-[/nocache]->
        <!-[add_css name='custom, bootstrap, bootstrap-responsive, validationEngine.jquery']->
        <!-[add_js name='jquery.min, jquery.validationEngine, jquery.validationEngine-en,  newsletter, bootstrap']->        
    </head>
        <!-- Header And Navigation -->
        <!-[include file="../include/top.tpl" title=header]->

                             
     <div class="row-fluid">
          <div class="span12">
               <div class="well-grey">
                  
                  <div class="clearfix"></div>
                  <div class="well not_found">
                      <h1>HTTP Error - 404</h1><div class="clearfix"></div>
                      <p><!-[$smarty.const.NOT_FOUND]-></p>
                       <div class="clearfix"></div>
                       <div style="clear:both;height:100px;"></div>
                 </div>      
             </div>
        </div>
    </div>
                     
      
</div>

<!-[include file="../include/footer.tpl" title=footer]->
   