<?php

include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/applicationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/leave_typesClass.php');


$modName = 'application';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
//isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
$page = isset($_GET['Page']) ? $_GET['Page'] : $page = '';

#handle actions here.
switch ($action):
    case'list':
        $query = new application;
        $list_applications = $query->listApplicationStudent($_SESSION['admin_session_secure']['user_id']);

        if (isset($_POST['submit'])) {
            if (isset($_POST['check_single'])) {
                foreach ($_POST['check_single'] as $id) {
                    $query = new application();
                    $delete = $query->deleteApplication($id);
                }
            }
        }
        break;
    case'insert':
        $object = new student;
        $s_info = $object->getStudent($id);
        if (isset($_POST['submit'])) {
            $to = SCHOOL_EMAIL;
            $subject = $_POST['subject'];
            $message = '<div id=":bn" class="a3s" style="overflow: hidden;">

<p style="line-height:20.7999992370605px">Hi <strong>Admin</strong>,</p>

<p style="line-height:20.7999992370605px">You received school leave Application from ' . $s_info->first_name . ' ' . $s_info->last_name . '</p>

<p style="line-height:20.7999992370605px">' . $_POST['message'] . '</p>


<p style="line-height:20.7999992370605px">Best Regards,</p>

<p style="line-height:20.7999992370605px"><strong>' . SITE_NAME . '</strong></p><div class="yj6qo"></div><div class="adL">


</div></div>';

            $send_email = send_email_by_cron($to, $message, $subject, $school->school_name);

            $query = new application;
            $_POST['send_to'] = SCHOOL_EMAIL;
            $_POST['student_id'] = $_SESSION['admin_session_secure']['user_id'];
            $_POST['on_date'] = date('Y-m-d');
            $query->saveData($_POST);
            $admin_user->set_pass_msg('Application Send Successfully');
            redirect(make_admin_url('application'));
        }

        $query = new leave_types();
        $list_leave_types = $query->lists();
        break;

    default:break;
endswitch;
?>
