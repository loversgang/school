<?php

include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
$modName = 'time_table';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
$page = isset($_GET['Page']) ? $_GET['Page'] : $page = '';
if ($id != $_SESSION['admin_session_secure']['user_id']) {
    Redirect(make_admin_url('home'));
}
#handle actions here.
switch ($action):
    case 'list':
        $CurrObj = new studentSession();
        $session_id = $CurrObj->getStudentCurrentSession($school->id, $id, TRUE);

        // Get Student Section
        $object = new studentSession();
        $ct_sec = $object->checkStudentSection($session_id, $id);

        // Get Time Table
        $obj = new timeTable();
        $times = $obj->getSessionTiming($session_id, $ct_sec);
        $temp = array();
        foreach ($times as $time_table) {
            $ts = strtotime($time_table->time_from);
            $temp[$ts] = $time_table;
        }
        ksort($temp);
        $time_tables = $temp;
        break;
    default:break;
endswitch;
