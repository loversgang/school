<?php

include_once(DIR_FS_SITE . 'include/functionClass/complaintsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
$modName = 'complaints';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : '';

$page = isset($_GET['Page']) ? $_GET['Page'] : '';
$login_student_id = $_SESSION['admin_session_secure']['user_id'];
switch ($action):
    case'list':
        $query = new complaints();
        $lists_complaints = $query->lists_complaints_posted_student($login_student_id);
        break;
    case'insert':
        $query = new staff;
        $all_staff = $query->getStaffList($school->id);
        if (isset($_POST['submit'])) {
            $query = new complaints();
            $query->saveComplaints($_POST);
            $admin_user->set_pass_msg('Complaint Edit Successfully');
            Redirect(make_admin_url('complaints', 'list', 'list'));
        }
        break;
    case 'delete':
        $query = new complaints();
        $query->delete_complaint($id);
        $admin_user->set_pass_msg('Complaint Deleted Successfully');
        Redirect(make_admin_url('complaints', 'list', 'list'));
        break;

    case'feedback':
        $query = new complaints;
        $check_correct_data = $query->check_correct_data($id, $login_student_id);
        if ($check_correct_data == false) {
            $admin_user->set_pass_msg('You cannot view this Complaint');
            Redirect(make_admin_url('complaints'));
        }

        if (isset($_POST['submit'])) {
            $query = new complaints_feedback;
            $query->saveComplaints_feedback($_POST);
            $admin_user->set_pass_msg('Feedback Send Successfully');
        }

        $query = new complaints_feedback;
        $all_complaints_feeback = $query->all_complaints_feeback($id, $login_student_id);
        break;
    default:break;
endswitch;
?>