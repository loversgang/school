<?php

include_once(DIR_FS_SITE . 'include/functionClass/schoolEventClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
$modName = 'event';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
$page = isset($_GET['Page']) ? $_GET['Page'] : '';
if ($id != $_SESSION['admin_session_secure']['user_id']) {
    Redirect(make_admin_url('home'));
}
#handle actions here.
switch ($action):
    case 'list':
        $CurrObj = new studentSession();
        $session_id = $CurrObj->getStudentCurrentSession($school->id, $id, TRUE);

        // Get Syllabus
        $obj = new event;
        $events = $obj->getEventsBySessionId($session_id);
        break;
    default:break;
endswitch;
