<?php

include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subscriptionPlanClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSubscriptionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSettingClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsClass.php');


$modName = 'teacher_profile';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
//isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['type']) ? $type = $_GET['type'] : $type = 'basic_information';

$page = isset($_GET['Page']) ? $_GET['Page'] : '';

#handle actions here.
switch ($action):
    case'list':
        $query = new staff;
        $s_info = $query->staffBasicInformation($id);

        $query = new staffQualification;
        $s_qualification = $query->getStaffQualification2($id);

        break;
    default:break;
endswitch;
?>
