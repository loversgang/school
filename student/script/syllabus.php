<?php

include_once(DIR_FS_SITE . 'include/functionClass/syllabusClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
$modName = 'syllabus';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
$page = isset($_GET['Page']) ? $_GET['Page'] : $page = '';
if ($id != $_SESSION['admin_session_secure']['user_id']) {
    Redirect(make_admin_url('home'));
}
#handle actions here.
switch ($action):
    case 'list':
        $CurrObj = new studentSession();
        $session_id = $CurrObj->getStudentCurrentSession($school->id, $id, TRUE);

        // Get Syllabus
        $obj = new syllabus;
        $syllabus_details = $obj->getSyllabusBySessionId($session_id);
        break;

    default:break;
endswitch;
