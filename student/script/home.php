<?php

include_once(DIR_FS_SITE . 'include/functionClass/circularsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/homeworkClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEventClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/applicationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/leave_typesClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/messageClass.php');

$modName = 'home';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
$page = isset($_GET['Page']) ? $_GET['Page'] : $page = '';
if ($id != $_SESSION['admin_session_secure']['user_id']) {
    Redirect(make_admin_url('home'));
}
#handle actions here.
switch ($action):
    case 'list':
        $CurrObj = new studentSession();
        $session_id = $CurrObj->getStudentCurrentSession($school->id, $id, TRUE);

        $obj = new studentSession;
        $section = $obj->checkStudentSection($session_id, $id);

        // Get Section Students
        $obj = new studentSession;
        $students = $obj->getSectionStudents($session_id, $section);

        // Get Section Students Upcoming Birthday
        $obj = new studentSession;
        $birthdays = $obj->getSectionStudentsBirthday($session_id, $section);

        $query = new circulars();
        $circulars = $query->listLimitedCirculars();

        // Get Events
        $obj = new event;
        $events = $obj->getEventsBySessionId($session_id);

        // Get Student Section
        $object = new studentSession();
        $ct_sec = $object->checkStudentSection($session_id, $id);

        // Get Time Table
        $obj = new timeTable();
        $times = $obj->getSessionTiming($session_id, $ct_sec);
        $temp = array();
        foreach ($times as $time_table) {
            $ts = strtotime($time_table->time_from);
            $temp[$ts] = $time_table;
        }
        ksort($temp);
        $time_tables = $temp;

        $query = new homework();
        $homeworks = $query->listsHomeWorkStudent($session_id);

        $query = new application;
        $list_applications = $query->listApplicationStudent($_SESSION['admin_session_secure']['user_id']);

        $obj = new school_header;
        $inbox_messages = $obj->getInBoxMessagesStudent($school->id, $id);

        // My Teachers
        $obj = new studentSession;
        $staff_list = $obj->getSectionTeachersBirthday($session_id, $section);
        break;
    case 'update':
        break;
    case 'view':
        break;
    case'print':
        break;
    default:break;
endswitch;
