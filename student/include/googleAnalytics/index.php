<?php
/*
 * ************** Google Analytics Php Class ***********************
 * ************** @author :- Aman Virk ***************************
 * *************  @uri :- www.thetutlage.com ********************
 * ************* @browse all api and php class :- 
 */


//error_reporting(0);
include_once( 'class.analytics.php' );
define('ga_email', 'rocky.developer001@gmail.com');
define('ga_password', 'deve#001');
define('ga_profile_id', '71897598');

// Start date and end date is optional
// if not given it will get data for the current month
$start_date = '2013-04-01';
$end_date = '2013-05-03';

$init = new fetchAnalytics(ga_email, ga_password, ga_profile_id, $start_date, $end_date);

$trafficCount = $init->trafficCount();
$referralTraffic = $init->referralCount();
$trafficCountNum = $init->sourceCountNum();
$trafficCountPer = $init->sourceCountPer();
$perDayCount = $init->perDayCount();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

    </head>
    <body>
        <div id="header">
            dfdfdfdf
        </div>
        <div class="container">
            <div class="blocks">
                <h1> Traffic Count </h1>
                <table>
                    <tr>
                        <th> Type </th>
                        <th> Values </th>
                    </tr>
                    <?php
                    foreach ($trafficCount[0] as $key => $value) {
                        echo '<tr>
					<td>' . $key . '</td>
					<td>' . $value . '</td>
				</tr>';
                    }
                    ?>
                </table>
            </div><!-- end blocks -->

            <div class="blocks">
                <h1> Traffic Referrals </h1>
                <table>
                    <tr>
                        <th> Type </th>
                        <th> Values </th>
                    </tr>
                    <?php
                    foreach ($referralTraffic as $key => $value) {
                        echo '<tr>
					<td>' . $key . '</td>
					<td>' . $value . '</td>
				</tr>';
                    }
                    ?>
                </table>
            </div><!-- end blocks -->

            <div class="blocks">
                <h1> Traffic Source In Numbers </h1>
                <table>
                    <tr>
                        <th> Type </th>
                        <th> Values </th>
                    </tr>
                    <?php
                    foreach ($trafficCountNum as $key => $value) {
                        echo '<tr>
					<td>' . $key . '</td>
					<td>' . $value . '</td>
				</tr>';
                    }
                    ?>
                </table>
            </div><!-- end blocks -->

            <div class="blocks">
                <h1> Traffic Source In Percentage </h1>
                <table>
                    <tr>
                        <th> Type </th>
                        <th> Values </th>
                    </tr>
                    <?php
                    foreach ($trafficCountPer as $key => $value) {
                        echo '<tr>
					<td>' . $key . '</td>
					<td>' . $value . '%</td>
				</tr>';
                    }
                    ?>
                </table>
            </div><!-- end blocks -->

            <div class="blocks">
                <h1> Daily Traffic Page Views </h1>
                <table>
                    <tr>
                        <th> Day </th>
                        <th> Page Views </th>
                    </tr>

                    <?php
                    foreach ($perDayCount as $key => $value) {
                        echo '<tr>
						<td>' . $key . '</td>
						<td>' . $value . '</td>
					</tr>';
                    }
                    ?>
                </table>
            </div><!-- end blocks -->
        </div>


    </body>
</html>
