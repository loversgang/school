<?php
/*
 * ************** Google Analytics Php Class ***********************

 */

//error_reporting(0);
include_once( 'class.analytics.php' );
define('ga_email', 'rocky.developer001@gmail.com');
define('ga_password', 'deve#001');
define('ga_profile_id', '71897598');

// Start date and end date is optional
// if not given it will get data for the current month


$ct = date('Y');
$start_date = $ct . '-01-01';
$end_date = $ct . '-12-31';

$init = new fetchAnalytics(ga_email, ga_password, ga_profile_id, $start_date, $end_date);

$trafficCount = $init->trafficCount();
$referralTraffic = $init->referralCount();
$trafficCountNum = $init->sourceCountNum();
$trafficCountPer = $init->sourceCountPer();
$perDayCount = $init->perDayCount();

$new_month_data = array();
foreach ($perDayCount as $key1 => $value1) {
    $m_name = trim(substr($key1, 0, 4));
    if (array_key_exists($m_name, $new_month_data)):
        $new_month_data[$m_name] = $new_month_data[$m_name] + $value1;
    else:
        $new_month_data[$m_name] = $value1;
    endif;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            body{
                background: #f7f7f7;
                font-family: Arial, "MS Trebuchet", sans-serif;
                font-size: 12px;
            }
            *{
                margin: 0;
                padding: 0;
            }
            .container{
                width: 1000px;
                margin: auto;
                padding: 10px;
                background: #fff;
            }
            .blocks{
                background: #fff;
                padding-bottom: 30px;
            }
            .blocks h1{
                padding: 10px;
                background: #f7f7f7;
            }
            .blocks table{
                border-collapse: collapse;
                width: 100%;
                border: 1px solid #ccc;
            }
            .blocks table th,.blocks table td{
                padding: 10px;
                text-align: left;
                border-top: 1px solid #ccc;
                border-right: 1px solid #ccc;
            }
        </style>
    </head>
    <body>

        <div class="container">
            <div class="blocks">
                <h1>Monthly Traffic</h1>
                <br />
                <script type="text/javascript" src="https://www.google.com/jsapi"></script>
                <script type="text/javascript">
                    google.load("visualization", "1", {packages: ["corechart"]});
                    google.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Month', 'Page View'],
<?php
foreach ($new_month_data as $k => $v):
    echo "['" . $k . "', " . $v . "],";
endforeach;
?>
                        ]);
                        var options = {
                            colors: ['#EC561B'],
                            title: 'Monthly Traffic Page Views',
                            hAxis: {title: 'Month', titleTextStyle: {color: 'red'}}
                        };

                        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
                        chart.draw(data, options);
                    }
                </script>
                <div id="chart_div" style="width:100%; height:100%;"></div>		
            </div>

            <!-- start of first Block -->
            <div class="first_block">	
                <div class="blocks" style="float:left;width:50%;">
                    <h1>Traffic Count </h1>
                    <br />
                    <?php echo $init->graphSourceCount(); ?>
                </div>
                <div class="blocks" style="float:right;width:50%;">
                    <h1>&nbsp;</h1>
                    <br />
                    <table style="top: 5px; right: 5px; font-size: smaller; color: rgb(84, 84, 84); width: 90%; float: right;">
                        <tr>
                            <th> Type </th>
                            <th> Values </th>
                        </tr>
                        <?php foreach ($trafficCountNum as $key => $value) { ?>
                            <tr>
                                <td class="legendLabel" ><?php echo ucfirst($key); ?></td>
                                <td><?= $value ?></td>
                            </tr>
                        <?php } ?>
                    </table>			
                </div>
                <div style="clear:both;"></div>
            </div>
            <!-- End of first Block -->


            <!-- Start of Second Block -->
            <div class="Second_block">	
                <div class="blocks" style="float:left;width:50%;">
                    <h1>Traffic Type </h1>
                    <br />
                    <?php echo $init->graphVisitorType(); ?>
                </div>
                <div class="blocks" style="float:left;width:50%;">
                    <h1>&nbsp;</h1>
                    <br />
                    <table style="top: 5px; right: 5px; font-size: smaller; color: rgb(84, 84, 84); width: 90%; float: right;">
                        <tr>
                            <th> Type </th>
                            <th> Values </th>
                        </tr>
                        <?php foreach ($trafficCount[0] as $key => $value) { ?>
                            <tr>
                                <td class="legendLabel" ><?php echo ucfirst($key); ?></td>
                                <td align='center'><?= $value ?></td>
                            </tr>
                        <?php } ?>
                    </table>

                </div>
                <div style="clear:both;"></div>	
            </div>

            <!-- end second blocks -->		
            <div style="clear:both"></div>
        </div>
    </body>
</html>
