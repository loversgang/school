<!DOCTYPE html>
<!--
--- About Us  ---
Content Management System
Developed by:- cWebConsultants India
http://www.cwebconsultants.com
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>

        <meta charset="utf-8" />
        <title>
            <?php
            if (defined('SITE_NAME')):
                echo 'Management Panel | ' . SITE_NAME;
            else:
                echo "Management Panel";
            endif;
            ?>
        </title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="assets/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="assets/global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="assets/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="assets/template/plugin/validation/validationEngine.jquery.css"/>
        <link rel="stylesheet" type="text/css" href="assets/template/plugin/validation/validationEngine.jquery.css"/>
        <link rel="stylesheet" type="text/css" href="assets/datatables.css"/>
        <?php if ($Page == 'profile') { ?>
            <link rel="stylesheet" type="text/css" href="assets/template/css/profile.css"/>
        <?php } ?>

        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <?php
    include_once(DIR_FS_SITE . 'include/functionClass/messageClass.php');
    include_once(DIR_FS_SITE . 'include/functionClass/circularsClass.php');
    $total_unread_msgs = school_header::getInboxCountUnread($school->id, $_SESSION['admin_session_secure']['user_id'], 'student');
    $logged_user = get_object('student', $_SESSION['admin_session_secure']['user_id']);
    
    $query = new circulars();
    $listNCirculars = $query->listNotificationCirculars();

    echo $circular_count = circulars::countCirculars();
    ?>
    <body class="page-header-fixed page-quick-sidebar-over-content">
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo make_admin_url('home'); ?>">
                        <h4>Welcome,</h4>
                        <h2><?php echo SITE_NAME; ?></h2>
                    </a>
                    <div class="menu-toggler sidebar-toggler hide"></div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown" id="header_inbox_bar">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php if ($circular_count > 0) { ?>
                                    <span class="badge badge-default">
                                        <span>
                                            <?php echo $circular_count ?>
                                        </span>
                                    <?php } ?>
                                </span>
                                <i class="fa fa-bell"></i>
                            </a>
                            <ul class="dropdown-menu extended notification">
                                <?php if (count($listNCirculars) > 0) { ?>
                                    <?php foreach ($listNCirculars as $list_circular) { ?>
                                        <li>
                                            <a href="<?php echo make_admin_url('circulars', 'list', 'list') ?>"><span class="label label-important" style='width:13px;'><i class="fa fa-male" style="color:red"></i></span><?php echo $list_circular->circular_title; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                <?php } else { ?>
                                    <li>
                                        <a href="<?php echo make_admin_url('circulars', 'list', 'list') ?>"><span class="label label-important" style='width:13px;'><i class="fa fa-male" style="color:red"></i></span>
                                            No New Circulars found
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="dropdown" id="header_inbox_bar">
                            <a href="<?php echo make_admin_url('messages'); ?>" class="dropdown-toggle">
                                <?php if ($total_unread_msgs > '0') { ?>
                                    <span class="badge badge-default">
                                        <?php echo $total_unread_msgs; ?>
                                    </span>
                                <?php } ?>
                                <i class="fa fa-comment"></i>
                            </a>
                            <ul class="dropdown-menu"></ul>
                        </li>
                        <li class="dropdown dropdown-user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <?
                                if (is_object($logged_user) && $logged_user->photo):
                                    $im_obj = new imageManipulation()
                                    ?>
                                    <img src="<?= $im_obj->get_image('student', 'medium', $logged_user->photo); ?>" class="img img-circle hide1"/>
                                <? else: ?>
                                    <img class="img img-circle hide1" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                <? endif; ?> 
                                <span class="username username-hide-on-mobile">
                                    <?php echo $logged_user->first_name ?>
                                </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo make_admin_url('profile'); ?>">
                                        <i class="fa fa-user"></i> My Profile </a>
                                </li>
                                <li class="divider">
                                </li>
                                <li>
                                    <a href="<?php echo make_admin_url('logout'); ?>">
                                        <i class="fa fa-key"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <div class="clearfix"></div>
        <div class="page-container">
            