<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper">
                <div class="sidebar-toggler"></div>
            </li>
            <li class="active" style="margin-top: 10px">
                <ul class="sub-menu">
                    <li>
                        <?
                        include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
                        include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
                        $obj = new studentAddress;
                        $address = $obj->getStudentAddressByType($logged_user->id, 'permanent');
                        #get students current session
                        $CurrObj = new studentSession();
                        $current_session = $CurrObj->getStudentCurrentSession($school->id, $logged_user->id);
                        
                        
                        ?>
                        <div class="card hovercard">
                            <div class="card-background">
                                <img class="card-bkimg" src="<?php echo DIR_WS_SITE_GRAPHIC . 'profile_cover.jpg' ?>" alt="">
                            </div>
                            <div class="useravatar">
                                <?php
                                if (is_object($logged_user) && $logged_user->photo):
                                    $im_obj = new imageManipulation()
                                    ?>
                                    <img src="<?= $im_obj->get_image('student', 'medium', $logged_user->photo); ?>" class="img img-circle"/>
                                <? else: ?>
                                    <img class="img img-circle" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                <? endif; ?> 
                            </div>
                            <br />
                            <div class="card-info">
                                <span class="card-title">
                                    <?php echo $logged_user->first_name; ?> <?php echo $logged_user->last_name; ?><br />
                                    <span style="font-size:14px"><?php echo $current_session ?></span>
                                </span>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="<?= $Page == 'home' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('home') ?>">
                    <i class="fa fa-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="<?= $Page == 'profile' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('profile') ?>">
                    <i class="fa fa-user"></i>
                    <span class="title">Profile</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'messages' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('messages') ?>">
                    <i class="fa fa-envelope "></i>
                    <span class="title">Messages</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'exam_results' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('exam_results') ?>">
                    <i class="fa fa-book "></i>
                    <span class="title">Exam Results</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'homework' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('homework') ?>">
                    <i class="fa fa-book "></i>
                    <span class="title">Assignments</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'remarks' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('remarks') ?>">
                    <i class="fa fa-book "></i>
                    <span class="title">Remarks</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'complaints' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('complaints') ?>">
                    <i class="fa fa-th "></i>
                    <span class="title">Complaints</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'classmates' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('classmates') ?>">
                    <i class="fa fa-users"></i>
                    <span class="title">Classmates</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'syllabus' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('syllabus') ?>">
                    <i class="fa fa-file"></i>
                    <span class="title">Syllabus</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'teachers' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('teachers') ?>">
                    <i class="fa fa-user-secret"></i>
                    <span class="title">My Teachers</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'school_sales' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('school_sales') ?>">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="title">School Products</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'circulars' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('circulars') ?>">
                    <i class="fa fa-inbox"></i>
                    <span class="title">News & Circulars</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'time_table' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('time_table') ?>">
                    <i class="fa fa-clock-o"></i>
                    <span class="title">Time Table</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'event' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('event') ?>">
                    <i class="fa fa-calendar"></i>
                    <span class="title">Activity Calendar</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= ($Page == 'application') ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('application') ?>">
                    <i class="fa fa-pencil-square-o"></i>
                    <span class="title">Leave Application</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= ($Page == 'setting' || $Page == 'setting') ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('setting') ?>">
                    <i class="fa fa-cogs"></i>
                    <span class="title">Setting</span>
                    <span class="selected"></span>
                </a> 
            </li>
            <li class="<?= $Page == 'logout' ? 'active' : '' ?>">
                <a href="<?php echo make_admin_url('logout') ?>">
                    <i class="fa fa-lock"></i>
                    <span class="title">Logout</span>
                    <span class="selected"></span>
                </a> 
            </li>
        </ul>
    </div>
</div>