<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px"><section id="portfoli">
            <div class="container-fluid">
                <h3 class="page-title">
                    Circulars
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Circulars</a>
                        </li>
                    </ul>
                </div>
                <?php if (count($lists) > 0) { ?>
                    <?php foreach ($lists as $key => $list) { ?>
                        <h3 class="name"><?php echo ucfirst($list->circular_title) ?></h3>
                        <h4 style="color: green"><?php echo ucfirst($list->on_date) ?></h4>
                        <p>
                            <?php
                            if (strlen($list->description) > 300) {
                                echo substr($list->description, 0, 300) . '..';
                                ?>
                                <a style="word-wrap: break-word;" data-toggle="modal" data-target="#myModal" data-circular_title="<?php echo ucfirst($list->circular_title) ?>" data-description="<?php echo ucfirst($list->description) ?>" id="view" data-file='<?php echo ($list->file !== '') ? '<a href="' . DIR_WS_SITE_UPLOAD . 'file/circulars' . '/' . $list->file . '" target="_blank" >' . $list->file . '</a>' : '' ?>'>
                                    <span class="hidden-xs" style="cursor:pointer">More</span>
                                </a>
                                <?php require 'model.php'; ?>
                                <?php
                            } else {
                                echo $list->description;
                            }
                            ?>
                        </p>
                        <hr/>
                        <?php
                    }
                } else {
                    ?>
                    <div class="alert alert-info text-center"><strong>No Circulars Found..!</strong></div>

                <?php }
                ?>
            </div>
        </section>
    </div>
</div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).on('click', '#view', function () {
        var data_description = $(this).attr('data-description');
        var data_title = $(this).attr('data-circular_title');
        var data_file = $(this).attr('data-file');
        $('#circular_description').html(data_description);
        $('#circular_title').html(data_title);
        $('#data_file').html(data_file);
    });
</script>
