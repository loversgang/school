<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="circular_title"></h4>
            </div>
            <div class="modal-body">
                <p id="circular_description"></p><br />
                <div id="data_file" style="word-wrap: break-word;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>