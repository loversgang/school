<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    List Application
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">List Applications</a>
                        </li>
                    </ul>
                </div>

                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>   
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="clearfix"></div>
            </div>
            <br />
            <div class="row-fluid">
                <div class="span12">
                    <div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-file-text"></i>Manage Applications</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div style="overflow-y: hidden">
                                <table class="table table-striped table-bordered table-hover" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>To</th>
                                            <th>Application Type</th>
                                            <th>Subject</th>
                                            <th>Message</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($list_applications as $list_application) { ?>
                                            <tr>
                                                <td><?php echo ($list_application['send_to'] == SCHOOL_EMAIL) ? ucfirst('admin') : '' ?></td>
                                                <td>
                                                    <?php
                                                    $leave_type = get_object('leave_types', $list_application['leave_type_id']);
                                                    echo $leave_type->type;
                                                    ?>
                                                </td>
                                                <td><?php echo $list_application['subject'] ?></td>
                                                <td>
                                                    <div class="col-md-6">
                                                        <?php
                                                        if (strlen($list_application['message']) > 10) {
                                                            echo substr($list_application['message'], 0, 10) . '....';
                                                        } else {
                                                            echo $list_application['message'];
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <a href="javascript:;" data-type="<?php echo ucfirst($leave_type->type) ?>" data-message="<?php echo ucfirst($list_application['message']) ?>"  id="view" class="btn btn-xs blue tooltips" title="Read Message">Read Message</a>
                                                    </div>
                                                </td>
                                                <td><?php echo $list_application['on_date'] ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require 'model.php' ?>
        </section>
    </div>
</div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).on('click', '#view', function () {
        var data_type = $(this).attr('data-type');
        var data_message = $(this).attr('data-message');
        $('#data_type').html(data_type);
        $('#data_message').html(data_message);
        $('#myModal').modal();
    });
</script>