<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Classmates
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Classmates</a>
                        </li>
                    </ul>
                </div>
                <?php if (count($lists) == 0) { ?>
                    <ul class="list-group">
                        <li class="list-group-item">
                        <center>
                            <span class="name">Sorry, No Record Found..!</span>
                        </center>
                        </li>
                    </ul>
                <?php } else { ?>
                    <div class="row tab-pane active" id="tab1">
                        <ul class="thumbnails product-list-inline-small">
                            <?php foreach ($lists as $key => $list) { ?>
                                <div class="col-md-6">
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                            <div class="col-md-4">
                                                <?
                                                if (is_object($list) && $list->photo):
                                                    $im_obj = new imageManipulation()
                                                    ?>
                                                    <img src="<?= $im_obj->get_image('student', 'medium', $list->photo); ?>" class="img-responsive" style="width: 120px;height: 120px"/>
                                                <? else: ?>
                                                    <img src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" class="img-responsive" style="width:120px;height:120px"/>
                                                <? endif; ?>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="name"><?php echo $list->first_name ?></span><br /><br />
                                                <?php
                                                $query = new studentAddress;
                                                $address = $query->getStudentAddressByType($list->student_id, 'permanent');
                                                if (is_object($address)) {
                                                    echo $address->address1 . '<br />';
                                                    echo $address->city . '<br />';
                                                    echo $address->tehsil . '<br />';
                                                }
                                                ?>
                                                <?php
                                                $date_of_birth = student::getStudentBirthInformation($list->student_id);
                                                echo $date_of_birth->date_of_birth;
                                                ?>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>

                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </section>
    </div>
</div>