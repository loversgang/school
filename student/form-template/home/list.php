<style>
    .lSNext {
        display:none !important;
    }
    .lSPrev {
        display:none !important;
    }

    .lSPager {
        display : none;
    }
</style>
<link rel='stylesheet prefetch' href='http://sachinchoolur.github.io/lightslider/dist/css/lightslider.css'>
<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <h3 class="page-title">
            Dashboard
        </h3>
        <div class="container-fluid">
            <div class="row">
                <div class="portlet box b tasks-widget" style="background-color:#e43a45;border:1px solid #e43a45">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar" style="color:#fff"></i>Dashboard
                        </div>
                    </div>
                    <div class="portlet-body" style="height:100%;overflow: auto;text-align: center">
                        <div class="col-md-12" style="margin-top: 20px">
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('profile', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-user fa-3x"></i><br/>
                                    <span class="span_port">Profile</span>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-envelope fa-3x"></i><br/>
                                    <span class="span_port">Messages</span>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('classmates', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-users fa-3x"></i><br/>
                                    <span class="span_port">Classmates</span>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('school_sales', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-shopping-cart fa-3x"></i><br/>
                                    <span class="span_port">School Products</span>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('syllabus', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-files-o fa-3x"></i><br/>
                                    <span class="span_port">Syllabus</span>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('circulars', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-inbox fa-3x"></i><br/>
                                    <span class="span_port">Circulars</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 30px">
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('teachers', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-user-secret fa-3x"></i><br/>
                                    <span class="span_port">My Teachers</span>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('time_table', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-clock-o fa-3x"></i><br/>
                                    <span class="span_port">Time Table</span>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('event', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-calendar fa-3x"></i><br/>
                                    <span class="span_port">Activity Calendar</span>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('application', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-pencil-square-o fa-3x"></i><br/>
                                    <span class="span_port">Leave Application</span>
                                </a>
                            </div>
                            <div class="col-md-2">
                                <a href="<?php echo make_admin_url('logout', 'list', 'list'); ?>" class="portfolio-link">
                                    <i class="fa fa-lock fa-3x"></i><br/>
                                    <span class="span_port">Logout</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="portlet box tasks-widget" style="background-color:#087120;border:1px solid #087120">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-birthday-cake" style="color:#fff"></i>Upcoming Birthdays
                        </div>
                    </div>
                    <div class="portlet-body" style="">
                        <div class="task-content">
                            <div class="demo">
                                <?php if ($birthdays) { ?>
                                    <?php if (count($birthdays) == 0) { ?>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                            <center>
                                                <span class="name">Sorry, No Record Found..!</span>
                                            </center>
                                            </li>
                                        </ul>
                                    <?php } else { ?>
                                        <ul id="lightSlider" class="task-list random" style="min-height: 150px">
                                            <?php
//                                    $student = $birthdays[array_rand($birthdays)];
                                            foreach ($birthdays as $student) {
                                                ?>
                                                <li>
                                                <center>
                                                    <?php
                                                    if (is_object($student) && $student->photo):
                                                        $im_obj = new imageManipulation();
                                                        ?>
                                                        <img src="<?= $im_obj->get_image('student', 'medium', $student->photo); ?>" style="height:130px;width: 130px" class="img img-circle img-thumbnail"/>
                                                    <? else: ?>
                                                        <img style="height:130px;width: 130px" class="img img-circle img-thumbnail" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                    <? endif; ?>
                                                    <div style="margin-bottom: 5px;font-weight: bold">
                                                        <?php echo $student->first_name . ' ' . $student->last_name; ?>
                                                    </div>
                                                    <div style="margin-bottom: 10px;color:darkred">
                                                        <b><i class="fa fa-birthday-cake"></i></b> <?php echo date('d M', strtotime($student->date_of_birth)); ?>
                                                    </div>
                                                </center>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                        <ul class="task-list" id="all_upcoming_birthdays" style="display:none">
                                            <?php
                                            foreach ($birthdays as $student) {
                                                ?>
                                                <div id="random_birthdays" class="col-md-12" style="border:1px solid #eee;padding:5px">
                                                    <div class="col-md-3">
                                                        <?php
                                                        if (is_object($student) && $student->photo):
                                                            $im_obj = new imageManipulation();
                                                            ?>
                                                            <img src="<?= $im_obj->get_image('student', 'medium', $student->photo); ?>" style="height:45px;width: 45px" class="img img-circle img-thumbnail"/>
                                                        <? else: ?>
                                                            <img style="height:45px;width: 45px" class="img img-circle" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                        <? endif; ?>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div style="margin-bottom: 5px;font-weight: bold">
                                                            <?php echo $student->first_name . ' ' . $student->last_name; ?>
                                                        </div>
                                                        <div style="margin-bottom: 10px;color:darkred">
                                                            <b><i class="fa fa-birthday-cake"></i></b> <?php echo date('d M', strtotime($student->date_of_birth)); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <div class="task-footer">
                                <div class="btn-arrow-link pull-right">
                                    <a href="javascript:;" id="view_all_birthdays">View All Birthdays</a>
                                    <a href="javascript:;" id="hide_all_birthdays" style="display:none">Hide All Birthdays</a>
                                    <i class="fa fa-arrow-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="portlet box blue-hoki tasks-widget">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar"></i>Events
                        </div>
                    </div>
                    <div class="portlet-body" style="">
                        <div class="task-content">
                            <ul class="task-list">
                                <?php foreach ($events as $event) { ?>
                                    <li>
                                        <div class="task-title">
                                            <i class="fa fa-calendar"></i>
                                            <span class="task-title-sp" style="margin-bottom: 5px">
                                                <b><?php echo $event->event ?> <font color="green">(<?php echo $event->on_date ?>)</font></b><br/>
                                                <?php
                                                if (strlen($event->description) > 200) {
                                                    echo substr($event->description, 0, 200) . '..';
                                                } else {
                                                    echo $event->description;
                                                }
                                                ?>
                                            </span>
                                            <br/>

                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="task-footer">
                            <div class="btn-arrow-link pull-right">
                                <a href="<?php echo make_admin_url('event', 'list', 'list'); ?>">See All Records</a>
                                <i class="fa fa-arrow-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="portlet box tasks-widget" style="background-color:#087120;border:1px solid #087120">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-birthday-cake" style="color:#fff"></i>My Staff Birthdays
                        </div>
                    </div>
                    <div class="portlet-body" style="">
                        <div class="task-content">
                            <div class="demo">
                                <?php if (count($staff_list) == 0) { ?>
                                    <ul class="list-group">
                                        <li class="list-group-item">
                                        <center>
                                            <span class="name">Sorry, No Record Found..!</span>
                                        </center>
                                        </li>
                                    </ul>
                                <?php } else { ?>
                                    <ul id="lightSlider2" class="task-list random_staff" style="min-height: 150px">
                                        <?php
                                        //                                    $student = $birthdays[array_rand($birthdays)];
                                        foreach ($staff_list as $staff) {
                                            if (is_object($staff)) {
                                                ?>
                                                <li>
                                                <center>
                                                    <?php
                                                    if (is_object($staff) && $staff->photo):
                                                        $im_obj = new imageManipulation();
                                                        ?>
                                                        <img src="<?= $im_obj->get_image('staff', 'medium', $staff->photo); ?>" style="height:130px;width: 130px" class="img img-circle img-thumbnail"/>
                                                    <? else: ?>
                                                        <img style="height:130px;width: 130px" class="img img-circle img-thumbnail" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                    <? endif; ?>
                                                    <div style="margin-bottom: 5px;font-weight: bold">
                                                        <?php echo $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name; ?>
                                                    </div>
                                                    <div style="margin-bottom: 10px;color:darkred">
                                                        <b><i class="fa fa-birthday-cake"></i></b> <?php echo date('d M', strtotime($staff->date_of_birth)); ?>
                                                    </div>
                                                </center>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                    <ul class="task-list" id="all_upcoming_birthdays_staff" style="display:none">
                                        <?php
                                        foreach ($staff_list as $staff) {
                                            ?>
                                            <div id="random_birthdays_staff" class="col-md-12" style="border:1px solid #eee;padding:5px">
                                                <div class="col-md-3">
                                                    <?php
                                                    if (is_object($staff) && $staff->photo):
                                                        $im_obj = new imageManipulation();
                                                        ?>
                                                        <img src="<?= $im_obj->get_image('staff', 'medium', $student->photo); ?>" style="height:45px;width: 45px" class="img img-circle img-thumbnail"/>
                                                    <? else: ?>
                                                        <img style="height:45px;width: 45px" class="img img-circle img-thumbnail" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                    <? endif; ?>
                                                </div>
                                                <div class="col-md-9">
                                                    <div style="margin-bottom: 5px;font-weight: bold">
                                                        <?php echo $staff->first_name . ' ' . $staff->last_name; ?>
                                                    </div>
                                                    <div style="margin-bottom: 10px;color:darkred">
                                                        <b><i class="fa fa-birthday-cake"></i></b> <?php echo date('d M', strtotime($staff->date_of_birth)); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </div>
                            <div class="task-footer">
                                <div class="btn-arrow-link pull-right">
                                    <a href="javascript:;" id="view_all_birthdays_staff">View All Birthdays</a>
                                    <a href="javascript:;" id="hide_all_birthdays_staff" style="display:none">Hide All Birthdays</a>
                                    <i class="fa fa-arrow-right"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet box" style="background-color:#e43a45;border:1px solid #e43a45">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-clock-o" style="color:#fff"></i>Time Table - <?php echo date('l'); ?> (<?php echo date('d-m-Y'); ?>)
                            </div>
                        </div>
                        <div class="portlet-body" style="">
                            <?php
                            if (date('D') == 'Sun') {
                                echo '<div class="alert alert-info"><h3>Oops.. Today Is Sunday!</h3></div>';
                            } else {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr style="background: #eee">
                                            <?php foreach ($time_tables as $time_table) { ?>
                                                <th style="text-align: center"><?php echo $time_table->time_from ?> - <?php echo $time_table->time_to ?></th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php foreach ($time_tables as $time_table) { ?>
                                                <td style="background: #fff">
                                        <center>
                                            <?php
                                            $day_name = date('D');
                                            $day = timeTable::getClassTime($session_id, $ct_sec, $day_name, $time_table->time_from, $time_table->time_to);
                                            if (is_object($day)) {
                                                if ($day->subject == '0') {
                                                    echo "LUNCH BREAK" . '<br/>';
                                                    echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                                } else {
                                                    $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                                    ?>
                                                    <div>
                                                        <?php
                                                        if (is_object($staff) && $staff->photo):
                                                            $im_obj = new imageManipulation()
                                                            ?>
                                                            <img style="width: 50px; height: 50px" class="img img-circle" src="<?= $im_obj->get_image('staff', 'medium', $staff->photo); ?>" />
                                                        <? else: ?>
                                                            <img style="width: 50px; height: 50px" class="img img-circle" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                        <? endif; ?> 
                                                    </div>
                                                    <?php
                                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                                    echo is_object($subject) ? $subject->name : '-';
                                                    echo "<br/>";
                                                    echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                                }
                                            } else {
                                                echo "-";
                                            }
                                            ?>
                                        </center>
                                        </td>
                                    <?php } ?>
                                    </tr>
                                    </tbody>
                                </table>
                            <?php } ?>
                            <div class="btn-arrow-link pull-right">
                                <a href="<?php echo make_admin_url('time_table', 'list', 'list'); ?>">See Full Time Table</a>
                                <i class="fa fa-arrow-right"></i>
                            </div>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="portlet box" style="background-color:#087120;border:1px solid #087120;height: 100%">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-users" style="color:#fff"></i> My Classmates
                            </div>
                        </div>
                        <div class="portlet-body" style="height:100%;overflow: auto;text-align: center">
                            <ul id="lightSlider3" class="task-list random_staff" style="min-height: 150px">
                                <?php foreach ($students as $student) { ?>
                                    <li class="col-md-3">
                                        <div style="border:1px solid #eee;padding:5px">
                                            <div style="margin-bottom: 10px">
                                                <?php
                                                if (is_object($student) && $student->photo):
                                                    $im_obj = new imageManipulation();
                                                    ?>
                                                    <img src="<?= $im_obj->get_image('student', 'medium', $student->photo); ?>" style="height:100px;width: 100px" class="img img-circle img-thumbnail"/>
                                                <? else: ?>
                                                    <img style="height:100px;width: 100px" class="img img-circle" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                <? endif; ?>
                                            </div>
                                            <div style="margin-bottom: 5px;font-weight: bold">
                                                <?php echo $student->first_name . ' ' . $student->last_name; ?>
                                            </div>
                                            <div style="margin-bottom: 5px;color:green">
                                                <b><i class="fa fa-<?php echo $student->sex == 'Male' ? 'male' : 'female'; ?>"></i></b> <?php echo $student->sex; ?>
                                            </div>
                                            <div style="margin-bottom: 10px;color:darkred">
                                                <b><i class="fa fa-birthday-cake"></i></b> <?php echo date('d-m-Y', strtotime($student->date_of_birth)); ?>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script>
        $('#view_all_birthdays').click(function () {
            $(this).hide();
            $('#all_upcoming_birthdays').show();
            $('#hide_all_birthdays').show();
            $('.random').hide();
        });
        $('#hide_all_birthdays').click(function () {
            $(this).hide();
            $('#all_upcoming_birthdays').hide();
            $('#view_all_birthdays').show();
            $('.random').show();
        });
        $('#view_all_birthdays_staff').click(function () {
            $(this).hide();
            $('#all_upcoming_birthdays_staff').show();
            $('#hide_all_birthdays_staff').show();
            $('.random_staff').hide();
        });
        $('#hide_all_birthdays_staff').click(function () {
            $(this).hide();
            $('#all_upcoming_birthdays_staff').hide();
            $('#view_all_birthdays_staff').show();
            $('.random_staff').show();
        });
    </script>
    <script src='http://sachinchoolur.github.io/lightslider/dist/js/lightslider.js'></script>
    <script>
        $('#lightSlider').lightSlider({
            gallery: true,
            item: 1,
            loop: true,
            slideMargin: 0,
            thumbItem: 9,
            auto: true,
            speed: 1000,
            mode: 'fade',
            pause: 3000,
        });
    </script>
    <script>
        $('#lightSlider2').lightSlider({
            gallery: true,
            item: 1,
            loop: true,
            slideMargin: 0,
            thumbItem: 9,
            auto: true,
            speed: 1000,
            mode: 'fade',
            pause: 3000,
        });
    </script>
    <script>
        $('#lightSlider3').lightSlider({
            gallery: true,
            item: 4,
            loop: true,
            slideMargin: 0,
            thumbItem: 9,
            auto: true,
            speed: 1000,
            //mode: 'fade',
            pause: 3000,
        });
    </script>