<div class="page-content-wrapper">
    <div class="page-content">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Profile
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Profile</a>
                        </li>
                    </ul>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption"><i class="fa fa-user"></i>Student Profile</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row tab-pane active" id="tab1">
                                    <ul class="thumbnails product-list-inline-small">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <?php require_once 'tabs/left_tab.php'; ?>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="portlet-body form">
                                                    <div class="form-wizard">
                                                        <div class="tab-content">
                                                            <div id="tab1" class="tab-pane <?= ($type == 'student_detail') ? 'active' : ''; ?>">
                                                                <div class="row-fluid">
                                                                    <?php require_once 'tabs/basic_info.php'; ?>
                                                                    <?php require_once 'tabs/family_info.php'; ?>
                                                                    <?php require_once 'tabs/previous_school_info.php'; ?>
                                                                    <?php require_once 'tabs/document_info.php'; ?>
                                                                    <?php require_once 'tabs/fee_info.php'; ?>
                                                                    <?php require_once 'tabs/attendance_info.php'; ?>
                                                                    <?php require_once 'tabs/exam_info.php'; ?>
                                                                    <?php require_once 'tabs/print_document.php'; ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
<?php require_once 'jquery.php'; ?>