<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">
    $(".session_filter").on("change", function () {
        var session_id = $(this).val();
        $('#session_filter').submit();
    });
    $(".session_filter_exam").on("change", function () {
        var session_id = $(this).val();
        $('#session_filter_exam').submit();
    });
    $(".session_filter_fee").on("change", function () {
        var session_id = $(this).val();
        $('#session_filter_fee').submit();
    });
    $(".session_filter_attendance").on("change", function () {
        var session_id = $(this).val();
        if (session_id.length > 0) {
            $('#session_filter_attendance').submit();
        }
    });
</script>
<?php
$obj = new studentSession;
$session = $obj->getStudentCurrentSessionInfo($school->id, $id);
$obj = new examination;
$subjects = $obj->getTotalSubjects($session->id, $session->section);
?>
<script>
    //Ist Term Exam Graph
    var data = [<?php
foreach ($subjects as $subject) {
    $result1 = examination::getMarksObtained($subject->id, 'FA1', $id, $session->id, $session->section);
    $result2 = examination::getMarksObtained($subject->id, 'FA2', $id, $session->id, $session->section);
    $result3 = examination::getMarksObtained($subject->id, 'SA1', $id, $session->id, $session->section);
    $maximum_marks_total1 = ($result1->max_marks + $result2->max_marks + $result3->max_marks);
    $marks_obtained1 = ($result1->marks + $result2->marks + $result3->marks);
    $marks_percent1 = ($marks_obtained1 * 100 / $maximum_marks_total1);
    if ($marks_percent1 != '0') {
        echo round($marks_percent1) . ",";
    }
}
?>];
    var total = 0;
    for (var val in data) {
        total += data[val];
    }
    var dummyData = total;
    data.push(dummyData);
    if (data.length === 2) {
    var seriesColorsArray = ['#DF7401', '#fff'];
    } else if (data.length === 3) {
    var seriesColorsArray = ['#DF7401', '#2E9AFE', '#fff'];
    } else if (data.length === 4) {
    var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#fff'];
    } else if (data.length === 5) {
    var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#fff'];
    } else if (data.length === 6) {
    var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#fff'];
    } else if (data.length === 7) {
    var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#999', '#fff'];
    }
    var dataLabelsArray = [<?php
foreach ($subjects as $subject) {
    echo "'" . $subject->name . "',";
}
?>'OPTIONAL'];
            var plot1 = jQuery.jqplot('term_ist', [data],
                    {
                        seriesDefaults: {
                            // Make this a pie chart.
                            renderer: jQuery.jqplot.PieRenderer,
                            rendererOptions: {
                                highlightMouseOver: false,
                                highlightMouseDown: false,
                                highlightColor: null,
                                startAngle: 180,
                                shadowDepth: 0,
                                dataLabelThreshold: 0,
                                showDataLabels: true,
                                dataLabels: data,
                                dataLabelFormatString: "<span style='font-size: 18px;color: #000 !important'>%s %</span>"
                            }
                        },
                        //Setting the series colors - defined above.. 
                        seriesColors: seriesColorsArray,
                        highlighter: {
                            show: false
                        },
                        // Make the backround transparent, remove canvas borders..
                        grid: {
                            backgroundColor: 'transparent',
                            drawBorder: false,
                            shadow: false
                        },
                        legend: {marginTop: '-200px', labels: dataLabelsArray, show: true, location: 's', rendererOptions: {
                                numberRows: 1
                            }}
                    }
            );

    // Second Term Exam Graph
    $(document).on('click', '#2nd_term_div', function () {
        var data = [<?php
foreach ($subjects as $subject) {
    $result4 = examination::getMarksObtained($subject->id, 'FA3', $id, $session->id, $session->section);
    $result5 = examination::getMarksObtained($subject->id, 'FA4', $id, $session->id, $session->section);
    $result6 = examination::getMarksObtained($subject->id, 'SA2', $id, $session->id, $session->section);
    $maximum_marks_total2 = ($result4->max_marks + $result5->max_marks + $result6->max_marks);
    $marks_obtained2 = ($result4->marks + $result5->marks + $result6->marks);
    $marks_percent2 = ($marks_obtained2 * 100 / $maximum_marks_total2);
    if ($marks_percent2 != '0') {
        echo round($marks_percent2) . ",";
    }
}
?>];
        var total = 0;
        for (var val in data) {
            total += data[val];
        }
        var dummyData = total;
        data.push(dummyData);
        if (data.length === 2) {
        var seriesColorsArray = ['#DF7401', '#fff'];
        } else if (data.length === 3) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#fff'];
        } else if (data.length === 4) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#fff'];
        } else if (data.length === 5) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#fff'];
        } else if (data.length === 6) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#fff'];
        } else if (data.length === 7) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#999', '#fff'];
        }
        var dataLabelsArray = [<?php
foreach ($subjects as $subject) {
    echo "'" . $subject->name . "',";
}
?>'OPTIONAL'];
                var plot1 = jQuery.jqplot('term_2nd', [data],
                        {
                            seriesDefaults: {
                                // Make this a pie chart.
                                renderer: jQuery.jqplot.PieRenderer,
                                rendererOptions: {
                                    highlightMouseOver: false,
                                    highlightMouseDown: false,
                                    highlightColor: null,
                                    startAngle: 180,
                                    shadowDepth: 0,
                                    dataLabelThreshold: 0,
                                    showDataLabels: true,
                                    dataLabels: data,
                                    dataLabelFormatString: "<span style='font-size: 18px;color: #000 !important'>%s %</span>"
                                }
                            },
                            //Setting the series colors - defined above.. 
                            seriesColors: seriesColorsArray,
                            highlighter: {
                                show: false
                            },
                            // Make the backround transparent, remove canvas borders..
                            grid: {
                                backgroundColor: 'transparent',
                                drawBorder: false,
                                shadow: false
                            },
                            legend: {marginTop: '-200px', labels: dataLabelsArray, show: true, location: 's', rendererOptions: {
                                    numberRows: 1
                                }}
                        }
                );
    });

    // Final Exam Graph
    $(document).on('click', '#final_exam_div', function () {
        var data = [<?php
foreach ($subjects as $subject) {
    $result1 = examination::getMarksObtained($subject->id, 'FA1', $id, $session->id, $session->section);
    $result2 = examination::getMarksObtained($subject->id, 'FA2', $id, $session->id, $session->section);
    $result3 = examination::getMarksObtained($subject->id, 'SA1', $id, $session->id, $session->section);
    $result4 = examination::getMarksObtained($subject->id, 'FA3', $id, $session->id, $session->section);
    $result5 = examination::getMarksObtained($subject->id, 'FA4', $id, $session->id, $session->section);
    $result6 = examination::getMarksObtained($subject->id, 'SA2', $id, $session->id, $session->section);
    $final_maximum_marks = ($result1->max_marks + $result2->max_marks + $result3->max_marks + $result4->max_marks + $result5->max_marks + $result6->max_marks);
    $final_marks_obtained = ($result1->marks + $result2->marks + $result3->marks + $result4->marks + $result5->marks + $result6->marks);
    $total_marks_percent = ($final_marks_obtained * 100 / $final_maximum_marks);
    if ($total_marks_percent != '0') {
        echo round($total_marks_percent) . ",";
    }
}
?>];
        var total = 0;
        for (var val in data) {
            total += data[val];
        }
        var dummyData = total;
        data.push(dummyData);
        if (data.length === 2) {
        var seriesColorsArray = ['#DF7401', '#fff'];
        } else if (data.length === 3) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#fff'];
        } else if (data.length === 4) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#fff'];
        } else if (data.length === 5) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#fff'];
        } else if (data.length === 6) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#fff'];
        } else if (data.length === 7) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#999', '#fff'];
        }
        var dataLabelsArray = [<?php
foreach ($subjects as $subject) {
    echo "'" . $subject->name . "',";
}
?>'OPTIONAL'];
                var plot1 = jQuery.jqplot('exam_final', [data],
                        {
                            seriesDefaults: {
                                // Make this a pie chart.
                                renderer: jQuery.jqplot.PieRenderer,
                                rendererOptions: {
                                    highlightMouseOver: false,
                                    highlightMouseDown: false,
                                    highlightColor: null,
                                    startAngle: 180,
                                    shadowDepth: 0,
                                    dataLabelThreshold: 0,
                                    showDataLabels: true,
                                    dataLabels: data,
                                    dataLabelFormatString: "<span style='font-size: 18px;color: #000 !important'>%s %</span>"
                                }
                            },
                            //Setting the series colors - defined above.. 
                            seriesColors: seriesColorsArray,
                            highlighter: {
                                show: false
                            },
                            // Make the backround transparent, remove canvas borders..
                            grid: {
                                backgroundColor: 'transparent',
                                drawBorder: false,
                                shadow: false
                            },
                            legend: {marginTop: '-200px', labels: dataLabelsArray, show: true, location: 's', rendererOptions: {
                                    numberRows: 1
                                }}
                        }
                );
    });

    // Include bootstrap Tabs
    jQuery(document).ready(function ($) {
        $('#tabs').tab();
    });
</script>    