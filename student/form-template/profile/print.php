<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <div class="hidden-print" >
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Manage Sessions
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>"> List Student</a> 
                        <i class="icon-angle-right"></i>
                    </li>
                    <li class="last">
                        Print Document
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-right">
            <?php if (isset($_GET['doc'])): ?>
                <div class = "tile bg-purple <?php echo ($section == 'update') ? 'selected' : '' ?>" id = 'print_document'>
                    <a class = "hidden-print" href = "<?php echo make_admin_url('certificate', 'list', 'list'); ?>">
                        <div class = "corner"></div>
                        <div class = "tile-body"><i class = "icon-arrow-left"></i></div>
                        <div class = "tile-object"><div class = "name">List Certificates</div></div>
                    </a>
                </div>
            <?php else: ?>
                <div class="tile bg-purple <?php echo ($section == 'update') ? 'selected' : '' ?>" id='print_document'>							
                    <a class="hidden-print" href="<?php echo make_admin_url('view', 'list', 'list&type=print&id=' . $history->student_id); ?>">
                        <div class="corner"></div>
                        <div class="tile-body"><i class="icon-arrow-left"></i></div>
                        <div class="tile-object"><div class="name">Back To List</div></div>
                    </a>
                </div>
            <?php endif; ?>
            <div id="print_document" class="tile bg-yellow selected">
                <a onclick="javascript:window.print();" class="hidden-print">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-print"></i></div>
                    <div class="tile-object"><div class="name">Print</div></div>
                </a>
            </div>					
        </div>            
        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="icon-print"></i> Print View</div>
            </div>
        </div>
    </div>   
    <br/>	
    <div class="clearfix"></div>

    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('view', 'view', 'view&type=print&id=' . $history->student_id); ?>" method="POST" enctype="multipart/form-data" id="validation">
            <div class="control-group">
                <?= html_entity_decode($content) ?>
            </div> 				
        </form>
    </div>
</div>
<!-- END PAGE CONTAINER-->    
