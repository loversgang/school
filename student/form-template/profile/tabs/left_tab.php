<ul class="list-unstyled" id='left_user_info'>
    <li>
        <?php
        if (is_object($s_info) && $s_info->photo):
            $im_obj = new imageManipulation()
            ?>
            <div class="item">	
                <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                    <div class="zoom">
                        <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                        <div class="zoom-icon"></div>
                    </div>
                </a>
            </div>															
        <? else: ?>
            <img src="<?php echo DIR_WS_SITE_GRAPHIC . 'not_uploaded.jpg' ?>" alt="No image" class="img-responsive">
        <? endif; ?> 
    </li>
    <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=student_detail') ?>">Basic Information</a></li>
    <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=family') ?>">Family Information</a></li>
    <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=document') ?>">Document Information </a></li>
    <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=previous_school') ?>">Previous Schools Information</a></li>
    <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=fee') ?>">Fee Information</a></li>
    <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=attendance') ?>">Attendance Information</a></li>
    <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=exam') ?>">Exam Information</a></li>
<!--    <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=print') ?>">Print Documents</a></li>-->
</ul>