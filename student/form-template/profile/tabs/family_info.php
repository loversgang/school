<h4 class="form-section hedding_inner1">Family Detail</h4>				
<ul class="list-unstyled col-md-6" id='user_info'>
    <li><span>Father Name :</span> <?
        if (is_object($s_f)) {
            echo $s_f->father_name;
        }
        ?></li>										
    <li><span>Father Occupation :</span> <?
        if (is_object($s_f)) {
            echo $s_f->father_occupation;
        }
        ?></li>										
    <li><span>Father Education :</span> <?
        if (is_object($s_f)) {
            echo $s_f->father_education;
        }
        ?></li>
    <li><span>Email Address :</span> <?
        if (is_object($s_f)) {
            echo "<a href='mailto:" . $s_f->email . "'>" . $s_f->email . "</a>";
        }
        ?></li>
    <li><span>Annual Income (<?= CURRENCY_SYMBOL ?>) :</span> <?
        if (is_object($s_f)) {
            echo $s_f->household_annual_income;
        }
        ?></li>
</ul>
<ul class="list-unstyled col-md-6" id='user_info'>										
    <li><span>Mother Name :</span> <?
        if (is_object($s_f)) {
            echo $s_f->mother_name;
        }
        ?></li>										
    <li><span>Mother Occupation :</span> <?
        if (is_object($s_f)) {
            echo $s_f->mother_occupation;
        }
        ?></li>										
    <li><span>Mother Education :</span> <?
        if (is_object($s_f)) {
            echo $s_f->mother_education;
        }
        ?></li>
    <li><span>Mobile Number. :</span> <?
        if (is_object($s_f)) {
            echo $s_f->mobile;
        }
        ?></li>
    <li><span>Family Photo :</span> 
        <?
        if (is_object($s_f) && $s_f->family_photo):
            $im_obj = new imageManipulation()
            ?>
            <div class="item span10">	
                <a class="fancybox-button" data-rel="fancybox-button" title="Family Photo" href="<?= $im_obj->get_image('student_family', 'large', $s_f->family_photo); ?>">
                    <div class="zoom">
                        <img src="<?= $im_obj->get_image('student_family', 'medium', $s_f->family_photo); ?>" style='margin-top: 6px;'/>
                        <div class="zoom-icon"></div>
                    </div>
                </a>

            </div>															
        <? else: ?>
            <img src="<?php echo DIR_WS_SITE_GRAPHIC . 'not_uploaded.jpg' ?>" alt="No image" class="img-responsive">
        <? endif; ?>

    </li>
</ul>
<div class="clearfix"></div>

<!-- Sibling Info -->
<h4 class="form-section hedding_inner1">Sibling Study in School</h4>
<? if ($QueryS->GetNumRows()): $sib_r = 1; ?>
    <? while ($sib = $QueryS->GetObjectFromRecord()): ?>
        <div class='span2'><strong>Sibling <?= $sib_r ?></strong></div>
        <div class='span9'>
            <ul class="unstyled span10" id='user_info'>					
                <li><span>Registration ID :</span><?= $sib->reg_id ?></li>
                <li><span>Name :</span><?= $sib->name ?></li>
                <li><span>Class :</span><?= $sib->class ?></li>
                <li><span>Relation :</span><?= $sib->relation ?></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <hr/>
        <?
        $sib_r++;
    endwhile;
else:
    ?>
    <ul class="list-unstyled col-md-11" id='user_info'>
        <li></li>															
        <li><span>Sorry, No Record Found..!</span></li>
        <li></li>	
    </ul>		
<? endif; ?>
</div>
<div class="clearfix"></div>
