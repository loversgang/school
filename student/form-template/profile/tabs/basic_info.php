<div class="col-md-12">
    <!-- Basic Info -->
    <h4 class="form-section hedding_inner1">Basic Information</h4>				
    <ul class="list-unstyled col-md-6" id='user_info'>
        <li><span>Registration Id :</span> <?= $s_info->reg_id ?></li>
        <li><span>Registration Date :</span> <?= $s_info->date_of_admission ?></li>																
        <li><span>First Name :</span> <?= $s_info->first_name ?></li>										
        <li><span>Sex :</span> <?= $s_info->sex ?></li>										
        <li><span>Cast :</span> <?= $s_info->cast ?></li>
        <li><span>Category :</span> <?= $s_info->category ?></li>
        <li><span>Mobile Number:</span> <?= $s_info->phone ?></li>
    </ul>
    <ul class="list-unstyled col-md-6" id='user_info'>	
        <li><span></span></li>
        <li><span></span></li>								
        <li><span>Last Name :</span> <?= $s_info->last_name ?></li>	
        <li><span>Date Of Birth :</span> <?= $s_info->date_of_birth ?></li>	
        <li><span>Religion :</span> <?= $s_info->religion ?></li>
        <li><span>Email :</span> <a href="mailto:<?= $s_info->email ?>"> <?= $s_info->email ?></a></li>	
    </ul>
    <div class="clearfix"></div>
    <!-- Address Info -->
    <h4 class="form-section hedding_inner1">Address Information</h4>				
    <ul class="list-unstyled col-md-12" id='user_info'>
        <li><span>Permanent Address :</span> 
            <?
            if (isset($s_p_ad) && !empty($s_p_ad->address1)) {
                echo $s_p_ad->address1 . ", ";
            }
            if (isset($s_p_ad) && !empty($s_p_ad->city)) {
                echo $s_p_ad->city . ", ";
            }
            if (isset($s_p_ad) && !empty($s_p_ad->tehsil)) {
                echo $s_p_ad->tehsil . ", ";
            }
            if (isset($s_p_ad) && !empty($s_p_ad->district)) {
                echo $s_p_ad->district . ", ";
            }
            if (isset($s_p_ad) && !empty($s_p_ad->state)) {
                echo $s_p_ad->state . ", ";
            }
            if (isset($s_p_ad) && !empty($s_p_ad->zip)) {
                echo $s_p_ad->zip;
            }
            ?>
        </li>
        <li><span>Correspondence Address :</span> 
            <?
            if (isset($s_c_ad) && !empty($s_c_ad->address1)) {
                echo $s_c_ad->address1 . ", ";
            }
            if (isset($s_c_ad) && !empty($s_c_ad->city)) {
                echo $s_c_ad->city . ", ";
            }
            if (isset($s_c_ad) && !empty($s_c_ad->tehsil)) {
                echo $s_c_ad->tehsil . ", ";
            }
            if (isset($s_c_ad) && !empty($s_c_ad->district)) {
                echo $s_c_ad->district . ", ";
            }
            if (isset($s_c_ad) && !empty($s_c_ad->state)) {
                echo $s_c_ad->state . ", ";
            }
            if (isset($s_c_ad) && !empty($s_c_ad->zip)) {
                echo $s_c_ad->zip;
            }
            ?>
        </li>				
    </ul>
    <div class="clearfix"></div>
    <!-- Health Info -->
    <h4 class="form-section hedding_inner1">Health Information</h4>				
    <ul class="list-unstyled col-md-6" id='user_info'>
        <li><span>Blood Group :</span> <?= $s_info->blood_group ?></li>										
        <li><span>Height :</span> <?= $s_info->height ?></li>										
        <li><span>Eye Right :</span> <?= $s_info->eye_right ?></li>
        <li><span>Family Doctor Name :</span> <?= $s_info->family_doctor ?></li>
    </ul>
    <ul class="list-unstyled col-md-6" id='user_info'>
        <li><span>Allergies :</span> <?= $s_info->allergies ?></li>	
        <li><span>Weight :</span> <?= $s_info->weight ?></li>
        <li><span>Eye Left :</span> <?= $s_info->eye_left ?></li>
        <li><span>Family Doctor No. :</span> <?= $s_info->family_doctor_no ?></li>
    </ul>
    <div class="clearfix"></div>

    <!-- Health Info -->
    <h4 class="form-section hedding_inner1">In Case Of Emergency</h4>				
    <ul class="list-unstyled col-md-6" id='user_info'>
        <li><span>Contact Person :</span> <?= $s_info->contact_person ?></li>		
        <li><span>Contact No :</span> <?= $s_info->contact_person_no ?></li>	
    </ul>
    <ul class="list-unstyled col-md-6" id='user_info'>		
        <li><span>Relation :</span> <?= $s_info->contact_relation ?></li>
    </ul>
    <div class="clearfix"></div>
    <!-- Reference Detail -->
    <h4 class="form-section hedding_inner1">Reference Detail</h4>				
    <ul class="list-unstyled col-md-6" id='user_info'>
        <li><span> By Reference :</span> <?= $s_info->ref_name ?></li>
    </ul>
    <ul class="list-unstyled col-md-6" id='user_info'>	
        <li><span>Remark :</span><?= $s_info->ref_remark ?></li>
    </ul>
    <div class="clearfix"></div>
</div>		
</div>
</div>


<!-- Family Detail -->
<div id="tab3" class="tab-pane <?= ($type == 'family') ? 'active' : ''; ?>">
    <!-- Photo -->
    <!--    <ul class="unstyled profile-nav span3" id='left_user_info'>
            <li>
    <?
    if (is_object($s_info) && $s_info->photo):
        $im_obj = new imageManipulation()
        ?>
                                                                                                                                                                                                                                                                                                                                                                <div class="item" style='text-align:center;'>	
                                                                                                                                                                                                                                                                                                                                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                                                                                                                                                                                                                                                                                                                                        <div class="zoom">
                                                                                                                                                                                                                                                                                                                                                                            <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                                                                                                                                                                                                                                                                                                                                            <div class="zoom-icon"></div>
                                                                                                                                                                                                                                                                                                                                                                        </div>
                                                                                                                                                                                                                                                                                                                                                                    </a>
                                                                                                                                                                                                                                                                                                                                                                </div>															
    <? else: ?>
                                                                                                                                                                                                                                                                                                                                                                <img src="assets/img/profile/img.jpg" alt="">
    <? endif; ?> 
            </li>
            <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=student_detail') ?>">Basic Information</a></li>
            <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=family') ?>">Family Information</a></li>
            <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=document') ?>">Document Information </a></li>
            <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=previous_school') ?>">Previous Schools Information</a></li>
            <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=fee') ?>">Fee Information</a></li>
            <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=attendance') ?>">Attendance Information</a></li>
            <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=exam') ?>">Exam Information</a></li>
            <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=print') ?>    ">Print Documents</a></li>
        </ul>-->