<!-- Step 5 Student Documents -->
<div id="tab5" class="tab-pane <?= ($type == 'document') ? 'active' : ''; ?>">
    <div class="row-fluid">
        <!-- Photo -->
<!--        <ul class="unstyled profile-nav span3" id='left_user_info'>
            <li>
                <?
                if (is_object($s_info) && $s_info->photo):
                    $im_obj = new imageManipulation()
                    ?>
                    <div class="item" style='text-align:center;'>	
                        <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                            <div class="zoom">
                                <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                <div class="zoom-icon"></div>
                            </div>
                        </a>
                    </div>																
                <? else: ?>
                    <img src="assets/img/profile/img.jpg" alt="">
                <? endif; ?> 
            </li>
            <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=student_detail') ?>">Basic Information</a></li>
            <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=family') ?>">Family Information</a></li>
            <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=document') ?>">Document Information </a></li>
            <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=previous_school') ?>">Previous Schools Information</a></li>
            <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=fee') ?>">Fee Information</a></li>
            <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=attendance') ?>">Attendance Information</a></li>
            <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=exam') ?>">Exam Information</a></li>
            <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=print') ?>">Print Documents</a></li>
        </ul>	-->
        <div class="span9">
            <!-- Address Info -->
            <h4 class="form-section hedding_inner1">Document Detail</h4>				
            <ul class="list-unstyled col-md-12" id='user_info'>
                <? if ($S_doc->GetNumRows()): $Sd_r = '0'; ?>																
                    <? while ($s_d = $S_doc->GetObjectFromRecord()): ?>														
                        <li><span><?= $s_d->title ?>:</span> 
                            <a target='_blank' href="<?= DIR_WS_SITE_UPLOAD . 'file/student_document/' . $s_d->file ?>"><?= $s_d->file ?></a>
                        </li>
                        <?
                        $Sd_r++;
                    endwhile;
                else:
                    ?>	
                    <li></li>															
                    <li><span>Sorry, No Record Found..!</span></li>
                    <li></li>														
                <? endif; ?>
            </ul>
            <div class="clearfix"></div>
        </div>		
    </div>	
</div>	
