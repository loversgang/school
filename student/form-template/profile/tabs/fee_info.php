<!-- Get Student Fee Info -->
<div id="tab6" class="tab-pane <?= ($type == 'fee') ? 'active' : ''; ?>">
    <div class="row-fluid">
        <!-- Photo -->
        <!--        <ul class="unstyled profile-nav span3" id='left_user_info'>
                    <li>
        <?
        if (is_object($s_info) && $s_info->photo):
            $im_obj = new imageManipulation()
            ?>
                                                                <div class="item" style='text-align:center;'>	
                                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                                        <div class="zoom">
                                                                            <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                                            <div class="zoom-icon"></div>
                                                                        </div>
                                                                    </a>
                                                                </div>														
        <? else: ?>
                                                                <img src="assets/img/profile/img.jpg" alt="">
        <? endif; ?> 
                    </li>
                    <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=student_detail') ?>">Basic Information</a></li>
                    <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=family') ?>">Family Information</a></li>
                    <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=document') ?>">Document Information </a></li>
                    <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=previous_school') ?>">Previous Schools Information</a></li>
                    <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=fee') ?>">Fee Information</a></li>
                    <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=attendance') ?>">Attendance Information</a></li>
                    <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=exam') ?>">Exam Information</a></li>
                    <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=print') ?>">Print Documents</a></li>
                </ul>	-->
        <div class="col-md-12">
            <form action="<?php echo make_admin_url('profile', 'list', 'list') ?>" method="GET" id="session_filter_fee">
                <input type='hidden' name='Page' value='profile'/>
                <input type='hidden' name='action' value='list'/>
                <input type='hidden' name='section' value='list'/>
                <input type='hidden' name='type' value='fee'/>
                <input type='hidden' name='id' value='<?= $id ?>'/>
                <div class="col-md-12">
                    <div class="col-md-5"></div>
                    <div class="col-md-7">
                        <label class="control-label" style="margin-left:30px">Select Session</label>
                        <div class="controls">
                            <select class="select2_category form-control session_filter_fee" data-placeholder="Select Session Students" name="session_id"  style="margin-left:30px">
                                <option value="">Select Session</option>
                                <?php
                                $session_sr = 1;
                                foreach ($Session_list as $s_k => $session):
                                    ?>
                                    <option value='<?= $session->session_id ?>' <?
                                    if ($session->session_id == $session_id) {
                                        echo 'selected';
                                    }
                                    ?> ><?= ucfirst($session->session_name) ?></option>
                                            <?
                                            $session_sr++;
                                        endforeach;
                                        ?>
                            </select>
                        </div>	
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </form>	
            <br />
            <h4 class="form-section hedding_inner1">Fee Detail</h4>				
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="hidden-480" >Sr. No.</th>
                            <th class="hidden-480" style='text-align:center;'>Payment Date</th>
                            <th class="hidden-480" style='text-align:center;'>Receipt No.</th>
                            <th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:center'>Total Paid(<?= CURRENCY_SYMBOL ?>)</th>
                            <th class="hidden-480" style='text-align:center;'>Balance</th>
                        </tr>
                    </thead>
                    <? if (!empty($Fee_list)): ?>
                        <tbody>
                            <?php
                            $sr = 1;
                            foreach ($Fee_list as $sk => $rec): $checked = '';
                                ?>
                                <tr class="odd gradeX">
                                    <td><?= $sr ?>.</td>                                                                                                                                                                        
                                    <td style='text-align:center;'><?= date('d M, Y', strtotime($rec->payment_date)) ?></td>
                                    <td style='text-align:center;'><?= $rec->id ?></td>
                                    <td style='text-align:center;'><?= CURRENCY_SYMBOL . ' ' . number_format($rec->total_amount, 2) ?></td>
                                    <td style='text-align:center;'>
                                        <?
                                        $QueryFList = new studentSession();
                                        $recc = $QueryFList->getBalanceDetail($rec->session_id, $rec->student_id, $rec->payment_date);

                                        echo CURRENCY_SYMBOL . number_format($recc, 2);
                                        ?>  


                                    </td>

                                </tr>
                                <?
                                $sr++;
                            endforeach;
                            ?>									
                        </tbody>
                    <? endif; ?>
                </table>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>		
    </div>	
</div>
