<!-- Get Print Documents -->
<div id="tab9" class="tab-pane <?= ($type == 'print') ? 'active' : ''; ?>">
    <div class="row-fluid">
        <!-- Photo -->
        <!--        <ul class="unstyled profile-nav span3" id='left_user_info'>
                    <li>
        <?
        if (is_object($s_info) && $s_info->photo):
            $im_obj = new imageManipulation()
            ?>
                                                                                                                                        <div class="item" style='text-align:center;'>	
                                                                                                                                            <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                                                                                                                <div class="zoom">
                                                                                                                                                    <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                                                                                                                    <div class="zoom-icon"></div>
                                                                                                                                                </div>
                                                                                                                                            </a>
                                                                                                                                        </div>														
        <? else: ?>
                                                                                                                                        <img src="assets/img/profile/img.jpg" alt="">
        <? endif; ?> 
                    </li>
                    <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=student_detail') ?>">Basic Information</a></li>
                    <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=family') ?>">Family Information</a></li>
                    <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=document') ?>">Document Information </a></li>
                    <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=previous_school') ?>">Previous Schools Information</a></li>
                    <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=fee') ?>">Fee Information</a></li>
                    <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=attendance') ?>">Attendance Information</a></li>
                    <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=exam') ?>">Exam Information</a></li>
                    <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=print') ?>">Print Documents</a></li>
                </ul>	-->
        <div class="col-md-12">
            <form action="<?php echo make_admin_url('profile', 'list', 'list') ?>" method="GET" id='session_filter'>
                <input type='hidden' name='Page' value='profile'/>
                <input type='hidden' name='action' value='list'/>
                <input type='hidden' name='section' value='list'/>
                <input type='hidden' name='type' value='print'/>
                <input type='hidden' name='id' value='<?= $id ?>'/>
                <div class="col-md-12">
                    <div class="col-md-7"></div>
                    <div class="col-md-5">
                        <label class="control-label">Select Session</label>
                        <div class="controls">
                            <select class="select2_category session_filter form-control col-md-12" data-placeholder="Select Session Students" name="session_id">
                                <option value="">Select Session</option>
                                <?php
                                $session_sr = 1;
                                foreach ($Session_list as $s_k => $session):
                                    ?>
                                    <option value='<?= $session->session_id ?>' <?
                                    if ($session->session_id == $session_id) {
                                        echo 'selected';
                                    }
                                    ?> ><?= ucfirst($session->session_name) ?></option>
                                            <?
                                            $session_sr++;
                                        endforeach;
                                        ?>
                            </select>
                        </div>	
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </form>	
            <br />
            <h4 class="form-section hedding_inner1">Document Printing History</h4>				
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_3">
                    <thead>
                        <tr>
                            <th class="hidden-480" >Sr. No.</th>
                            <th class="hidden-480" style='text-align:center;'>On Date</th>
                            <th class="hidden-480">Document Type</th>
                            <th class="hidden-480">Reprint</th>
                            <th class="hidden-480">Session</th>	
                            <th class="hidden-480">Action</th>
                        </tr>
                    </thead>
                    <? if (!empty($Print_list)): ?>
                        <tbody>
                            <?php
                            $sr = 1;
                            foreach ($Print_list as $sk => $p_rec):
                                ?>
                                <tr class="odd gradeX">
                                    <td><?= $sr ?>.</td>
                                    <td style='text-align:center;'><?= $p_rec->on_date ?></td>
                                    <td><?= $p_rec->name ?></td>
                                    <td><a href="<?= make_admin_url('profile', 'print', 'print&id=' . $p_rec->his_id) ?>">Print Again</a></td>
                                    <td><?= $p_rec->session_name ?></td>
                                    <td><a href='<?= make_admin_url('profile', 'del_rec', 'del_rec' . '&session_id=' . $session_id . '&del_id=' . $p_rec->his_id) ?>' onclick="return confirm('Are you sure? You are deleting this record.');" class="btn red btn-sm"><i class="fa fa-remove"></i></a></td>                                
                                </tr>
                                <?
                                $sr++;
                            endforeach;
                            ?>									
                        </tbody>
                    <? endif; ?>
                </table>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div><br/><br/>
            <h4 class="form-section hedding_inner1">Print Documents</h4>
            <div class="row-fluid">
                    <? if (!empty($session_id)): ?>

                        <? if (!empty($result)): ?>
                            <div class="col-md-3">	
                                <a class="btn black big" href="<?= make_admin_url('exam', 'group', 'group&session_id=' . $session_id) ?>" style='height:auto;line-height:18px;'>Student DMC <br/><span style='font-size:13px;'>for <?= $session_info->title ?></span></a>
                            </div>
                        <? endif; ?>			
                        <? if (is_object($student_icard)): ?>
                            <div class="col-md-3">
                                <a class="btn black big" href="<?= make_admin_url('session', 'print', 'print' . '&ct_session=' . $session_id . '&ct_section=' . $section_name . '&doc_id=' . $student_icard->id) ?>" title='Print ID Card' style='height:auto;line-height:18px;'>Student ID Card<br/><span style='font-size:13px;'>for <?= $session_info->title ?></span></a>
                            </div>
                        <? endif; ?>	
                        <? if (is_object($certificate)): ?>
                            <div class="col-md-3"  style="margin-left:16px">
                                <a class="btn black big" href="<?= make_admin_url('profile', 'update', 'update' . '&session_id=' . $session_id . '&ct_section=' . $section_name . '&doc_id=' . $certificate->id) ?>" style='height:auto;line-height:18px;'>Character Certificate <br/><span style='font-size:13px;'>for <?= $session_info->title ?></span>
                                </a>
                            </div>
                        <? endif; ?>	

                    <? else: ?>
                        <p style='font-size: 15px; margin-left: 40px;'> To Print the various documents please select the session..!</p>
                    <? endif; ?>	
            </div>


        </div>		
    </div>	
</div>