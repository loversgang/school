<!-- Step 4 Previous School -->
<div id="tab4" class="tab-pane <?= ($type == 'previous_school') ? 'active' : ''; ?>">

    <div class="row-fluid">
        <!-- Photo -->
<!--        <ul class="unstyled profile-nav span3" id='left_user_info'>
            <li>
                <?
                if (is_object($s_info) && $s_info->photo):
                    $im_obj = new imageManipulation()
                    ?>
                    <div class="item" style='text-align:center;'>	
                        <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                            <div class="zoom">
                                <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                <div class="zoom-icon"></div>
                            </div>
                        </a>
                    </div>															
                <? else: ?>
                    <img src="assets/img/profile/img.jpg" alt="">
                <? endif; ?> 
            </li>
            <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=student_detail') ?>">Basic Information</a></li>
            <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=family') ?>">Family Information</a></li>
            <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=document') ?>">Document Information </a></li>
            <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=previous_school') ?>">Previous Schools Information</a></li>
            <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=fee') ?>">Fee Information</a></li>
            <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=attendance') ?>">Attendance Information</a></li>
            <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=exam') ?>">Exam Information</a></li>
            <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=print') ?>">Print Documents</a></li>
        </ul>	-->
        <div class="span9">
            <!-- Address Info -->
            <h4 class="form-section hedding_inner1">Previous Schools Information</h4>				
            <? if ($P_school->GetNumRows()): $PS_r = 1; ?>
                <? while ($sch = $P_school->GetObjectFromRecord()): ?>
                    <div class='span2'><strong>School <?= $PS_r ?></strong></div>
                    <div class='span9'>
                        <ul class="list-unstyled col-md-12" id='user_info'>					
                            <li><span>School Name :</span><?= $sch->school_name ?></li>
                            <li><span>Class :</span><?= $sch->class ?></li>
                            <li><span>Address1 :</span><?= $sch->address1 ?></li>
                            <li><span>Address2 :</span><?= $sch->address2 ?></li>
                            <li><span>City :</span><?= $sch->city ?></li>
                            <li><span>State :</span><?= $sch->state ?></li>
                            <li><span>Country :</span><?= $sch->country ?></li>
                            <li><span>Percentage :</span><?= $sch->percentage ?></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <hr/>
                    <?
                    $PS_r++;
                endwhile;
            else:
                ?>
                <ul class="list-unstyled col-md-12" id='user_info'>
                    <li></li>															
                    <li><span>Sorry, No Record Found..!</span></li>
                    <li></li>	
                </ul>																	
            <? endif; ?>	
        </div>		
    </div>	
</div>
