<!-- Get Student Exam Info -->
<div id="tab8" class="tab-pane <?= ($type == 'exam') ? 'active' : ''; ?>">
    <div class="row-fluid">
        <!-- Photo -->
        <!--        <ul class="unstyled profile-nav span3" id='left_user_info'>
                    <li>
        <?
        if (is_object($s_info) && $s_info->photo):
            $im_obj = new imageManipulation()
            ?>
                                                                                <div class="item" style='text-align:center;'>	
                                                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                                                        <div class="zoom">
                                                                                            <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                                                            <div class="zoom-icon"></div>
                                                                                        </div>
                                                                                    </a>
                                                                                </div>														
        <? else: ?>
                                                                                <img src="assets/img/profile/img.jpg" alt="">
        <? endif; ?> 
                    </li>
                    <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=student_detail') ?>">Basic Information</a></li>
                    <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=family') ?>">Family Information</a></li>
                    <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=document') ?>">Document Information </a></li>
                    <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=previous_school') ?>">Previous Schools Information</a></li>
                    <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=fee') ?>">Fee Information</a></li>
                    <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=attendance') ?>">Attendance Information</a></li>
                    <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=exam') ?>">Exam Information</a></li>
                    <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=print') ?>">Print Documents</a></li>
                </ul>	-->
        <div class="col-md-12">
            <form action="<?php echo make_admin_url('profile', 'list', 'list') ?>" method="GET" id='session_filter_exam'>
                <input type='hidden' name='Page' value='profile'/>
                <input type='hidden' name='action' value='list'/>
                <input type='hidden' name='section' value='list'/>
                <input type='hidden' name='type' value='exam'/>
                <input type='hidden' name='id' value='<?= $id ?>'/>
                <div class="col-md-12">
                    <div class="col-md-5"></div>
                    <div class="col-md-7">
                        <label class="control-label" style="margin-left:30px">Select Session</label>
                        <div class="controls">
                            <select class="select2_category session_filter_exam form-control col-md-12" data-placeholder="Select Session Students" name="session_id" style="margin-left:30px">
                                <?php
                                $session_sr = 1;
                                foreach ($Session_list as $s_k => $session):
                                    ?>
                                    <option value='<?= $session->session_id ?>' <?
                                    if ($session->session_id == $session_id) {
                                        echo 'selected';
                                    }
                                    ?> ><?= ucfirst($session->session_name) ?></option>
                                            <?
                                            $session_sr++;
                                        endforeach;
                                        ?>
                            </select>
                        </div>	
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </form>
            <br />
            <h4 class="form-section hedding_inner1">Exam Detail</h4>
            <div id="content">
                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                    <li class="active"><a href="#ist_term" data-toggle="tab"><span style="margin: 10px"><font size="4"><b>Ist Terms</b></font></span></a></li>
                    <li><a href="#2nd_term" data-toggle="tab" id="2nd_term_div"><span style="margin: 10px"><font size="4"><b>2nd Terms</b></font></span></a></li>
                    <li><a href="#final_exam" data-toggle="tab" id="final_exam_div"><span style="margin: 10px"><font size="4"><b>Final Exam</b></font></span></a></li>
                </ul>
                <div id="my-tab-content" class="tab-content">
                    <div class="tab-pane active" id="ist_term">
                        <div class="chart">
                            <div id="term_ist"></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="2nd_term">
                        <div class="chart">
                            <div id="term_2nd"></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="final_exam">
                        <div class="chart">
                            <div id="exam_final"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>		
    </div>	
</div>