<!-- Get Student Attendance Info -->
<div id="tab7" class="tab-pane <?= ($type == 'attendance') ? 'active' : ''; ?>">
    <div class="row-fluid">
        <!-- Photo -->
<!--        <ul class="unstyled profile-nav span3" id='left_user_info'>
            <li>
                <?
                if (is_object($s_info) && $s_info->photo):
                    $im_obj = new imageManipulation()
                    ?>
                    <div class="item" style='text-align:center;'>	
                        <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                            <div class="zoom">
                                <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                <div class="zoom-icon"></div>
                            </div>
                        </a>
                    </div>															
                <? else: ?>
                    <img src="assets/img/profile/img.jpg" alt="">
                <? endif; ?> 
            </li>
            <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=student_detail') ?>">Basic Information</a></li>
            <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=family') ?>">Family Information</a></li>
            <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=document') ?>">Document Information </a></li>
            <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=previous_school') ?>">Previous Schools Information</a></li>
            <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=fee') ?>">Fee Information</a></li>
            <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=attendance') ?>">Attendance Information</a></li>
            <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=exam') ?>">Exam Information</a></li>
            <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('profile', 'list', 'list&type=print') ?>">Print Documents</a></li>
        </ul>	-->
        <div class="col-md-12">
            <form action="<?php echo make_admin_url('profile', 'list', 'list') ?>" method="GET" id='session_filter_attendance'>
                <input type='hidden' name='Page' value='profile'/>
                <input type='hidden' name='action' value='list'/>
                <input type='hidden' name='section' value='list'/>
                <input type='hidden' name='type' value='attendance'/>
                <input type='hidden' name='id' value='<?= $id ?>'/>
                <div class="col-md-12">
                    <div class="col-md-5"></div>
                    <div class="col-md-7">
                        <label class="control-label" style="margin-left:30px">Select Session</label>
                        <div class="controls">
                            <select class="select2_category session_filter_attendance form-control" data-placeholder="Select Session Students" name="session_id"  style="margin-left:30px">
                                <option value="">Select Session</option>
                                <?php
                                $session_sr = 1;
                                foreach ($Session_list as $s_k => $session):
                                    ?>
                                    <option value='<?= $session->session_id ?>' <?
                                    if ($session->session_id == $session_id) {
                                        echo 'selected';
                                    }
                                    ?> ><?= ucfirst($session->session_name) ?></option>
                                            <?
                                            $session_sr++;
                                        endforeach;
                                        ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>	
            </form>	
            <br />
            <h4 class="form-section hedding_inner1">Attendance Detail</h4>				
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="hidden-480" style='text-align:center;'>Month</th>
                            <? foreach ($Attendance_array as $key => $value): ?>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='<?= $key ?>'>&nbsp;<?= $value ?></font></th>
                            <? endforeach; ?>
                            <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='NM'>&nbsp;Not Marked</font></th>
                            <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='W'>&nbsp;Working Days</font></th>
                        </tr>
                    </thead>
                    <?
                    if (!empty($session_id) && ($session_id !== 'None')):
                        $All_months = getMonthsArray($session_info->start_date, $session_info->end_date);
                        ?>
                        <? if (!empty($All_months)): ?>
                            <tbody>
                                <?php
                                $sr = 1;
                                foreach ($All_months as $m_k => $m_v): $checked = '';
                                    if (($m_k) && strlen($m_k) == '1'):
                                        $m_number = '0' . $m_k;
                                    else:
                                        $m_number = $m_k;
                                    endif;

                                    /* Get working days */
                                    $first_date_of_month = $m_v . "-01";

                                    $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($m_v . '-01'))));

                                    $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
                                    $total_month_days = (date('d', strtotime($last_date_of_month)));
                                    $working_days = $total_month_days - $total_sunday;

                                    /* Get Attendance */
                                    $Q_obj = new attendance();
                                    $r_atten = $Q_obj->checkSessionSectionMonthYearAttendace($session_id, $section_name, $id, date('m', strtotime($m_v . '-01')), date('Y', strtotime($m_v . '-01')));
                                    $t_attend = array_sum($r_atten);

                                    $M_obj = new schoolHolidays();
                                    $M_holiday = $M_obj->checkMonthlyHoliday($school->id, date('m', strtotime($m_v . '-01')), date('Y', strtotime($m_v . '-01')));
                                    ?>
                                    <tr class="odd gradeX">
                                        <td style='text-align:center;'><?= date('M Y', strtotime($m_v . '-01')) ?></td>
                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><?
                                            if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo "<font class='P'>&nbsp;" . $r_atten['P'] . "</font>";
                                            else: echo '0';
                                            endif;
                                            ?></td>
                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><?
                                            if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo "<font class='A'>&nbsp;" . $r_atten['A'] . "</font>";
                                            else: echo '0';
                                            endif;
                                            ?></td>
                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><?
                                            if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo "<font class='L'>&nbsp;" . $r_atten['L'] . "</font>";
                                            else: echo '0';
                                            endif;
                                            ?></td>
                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= ($working_days - $t_attend - $M_holiday) ?></td>
                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $working_days - $M_holiday ?></td>	
                                    </tr>
                                    <?
                                    $sr++;
                                endforeach;
                                ?>									
                            </tbody>
                        <? endif; ?>	
                    <? else: ?>	
                        <tbody>
                            <tr><td colspan='8'> Sorry,No Record Found..! Please select the session</td></tr>
                        </tbody>
                    <? endif; ?>
                </table>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>		
    </div>	
</div>