<div class="page-content-wrapper">
    <div class="page-content">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Time Table
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Time Table</a>
                        </li>
                    </ul>
                </div>
                <?php if (count($time_tables) > 0) { ?>
                    <div class="row-fluid">
                        <div style="overflow-y: hidden">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr style="background: #eee">
                                        <th style="text-align: center">Time</th>
                                        <th style="text-align: center">Monday</th>
                                        <th style="text-align: center">Tuesday</th>
                                        <th style="text-align: center">Wednesday</th>
                                        <th style="text-align: center">Thursday</th>
                                        <th style="text-align: center">Friday</th>
                                        <th style="text-align: center">Saturday</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($time_tables as $time_table) { ?>
                                        <tr>
                                            <td style="background: #eee">
                                    <center>
                                        <?php echo $time_table->time_from ?> - <?php echo $time_table->time_to ?>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <?php
                                        $day = timeTable::getClassTime($session_id, $ct_sec, 'Mon', $time_table->time_from, $time_table->time_to);
                                        if (is_object($day)) {
                                            if ($day->subject == '0') {
                                                echo "LUNCH BREAK" . '<br/>';
                                                echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                            } else {
                                                $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                                ?>
                                                <div>
                                                    <?php
                                                    if (is_object($staff) && $staff->photo):
                                                        $im_obj = new imageManipulation()
                                                        ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?= $im_obj->get_image('staff', 'medium', $staff->photo); ?>" />
                                                    <? else: ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                    <? endif; ?> 
                                                </div>
                                                <?php
                                                $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                                echo is_object($subject) ? $subject->name : '-';
                                                echo "<br/>";
                                                echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                            }
                                        } else {
                                            echo "-";
                                        }
                                        ?>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <?php
                                        $day = timeTable::getClassTime($session_id, $ct_sec, 'Tue', $time_table->time_from, $time_table->time_to);
                                        if (is_object($day)) {
                                            if ($day->subject == '0') {
                                                echo "LUNCH BREAK" . '<br/>';
                                                echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                            } else {
                                                $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                                ?>
                                                <div>
                                                    <?php
                                                    if (is_object($staff) && $staff->photo):
                                                        $im_obj = new imageManipulation()
                                                        ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?= $im_obj->get_image('staff', 'medium', $staff->photo); ?>" />
                                                    <? else: ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                    <? endif; ?> 
                                                </div>
                                                <?php
                                                $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                                echo is_object($subject) ? $subject->name : '-';
                                                echo "<br/>";
                                                echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                            }
                                        } else {
                                            echo "-";
                                        }
                                        ?>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <?php
                                        $day = timeTable::getClassTime($session_id, $ct_sec, 'Wed', $time_table->time_from, $time_table->time_to);
                                        if (is_object($day)) {
                                            if ($day->subject == '0') {
                                                echo "LUNCH BREAK" . '<br/>';
                                                echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                            } else {
                                                $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                                ?>
                                                <div>
                                                    <?php
                                                    if (is_object($staff) && $staff->photo):
                                                        $im_obj = new imageManipulation()
                                                        ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?= $im_obj->get_image('staff', 'medium', $staff->photo); ?>" />
                                                    <? else: ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                    <? endif; ?> 
                                                </div>
                                                <?php
                                                $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                                echo is_object($subject) ? $subject->name : '-';
                                                echo "<br/>";
                                                echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                            }
                                        } else {
                                            echo "-";
                                        }
                                        ?>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <?php
                                        $day = timeTable::getClassTime($session_id, $ct_sec, 'Thu', $time_table->time_from, $time_table->time_to);
                                        if (is_object($day)) {
                                            if ($day->subject == '0') {
                                                echo "LUNCH BREAK" . '<br/>';
                                                echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                            } else {
                                                $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                                ?>
                                                <div>
                                                    <?php
                                                    if (is_object($staff) && $staff->photo):
                                                        $im_obj = new imageManipulation()
                                                        ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?= $im_obj->get_image('staff', 'medium', $staff->photo); ?>" />
                                                    <? else: ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                    <? endif; ?> 
                                                </div>
                                                <?php
                                                $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                                echo is_object($subject) ? $subject->name : '-';
                                                echo "<br/>";
                                                echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                            }
                                        } else {
                                            echo "-";
                                        }
                                        ?>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <?php
                                        $day = timeTable::getClassTime($session_id, $ct_sec, 'Fri', $time_table->time_from, $time_table->time_to);
                                        if (is_object($day)) {
                                            if ($day->subject == '0') {
                                                echo "LUNCH BREAK" . '<br/>';
                                                echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                            } else {
                                                $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                                ?>
                                                <div>
                                                    <?php
                                                    if (is_object($staff) && $staff->photo):
                                                        $im_obj = new imageManipulation()
                                                        ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?= $im_obj->get_image('staff', 'medium', $staff->photo); ?>" />
                                                    <? else: ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                    <? endif; ?> 
                                                </div>
                                                <?php
                                                $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                                echo is_object($subject) ? $subject->name : '-';
                                                echo "<br/>";
                                                echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                            }
                                        } else {
                                            echo "-";
                                        }
                                        ?>
                                    </center>
                                    </td>
                                    <td>
                                    <center>
                                        <?php
                                        $day = timeTable::getClassTime($session_id, $ct_sec, 'sat', $time_table->time_from, $time_table->time_to);
                                        if (is_object($day)) {
                                            if ($day->subject == '0') {
                                                echo "LUNCH BREAK" . '<br/>';
                                                echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                            } else {
                                                $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                                ?>
                                                <div>
                                                    <?php
                                                    if (is_object($staff) && $staff->photo):
                                                        $im_obj = new imageManipulation()
                                                        ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?= $im_obj->get_image('staff', 'medium', $staff->photo); ?>" />
                                                    <? else: ?>
                                                        <img style="width: 50px; height: 50px" class="img img-circle" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                    <? endif; ?> 
                                                </div>
                                                <?php
                                                $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                                echo is_object($subject) ? $subject->name : '-';
                                                echo "<br/>";
                                                echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                            }
                                        } else {
                                            echo "-";
                                        }
                                        ?>
                                    </center>
                                    </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php } else {
                    ?>
                    <div class="alert alert-info text-center"><strong>TimeTable Not Found..!</strong></div>
                <?php }
                ?>
            </div>
        </section>
    </div>
</div>