<div class="page-content-wrapper">
    <div class="page-content">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Setting
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Setting</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <?php
                /* display message */
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Setting</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row tab-pane active" id="tab1">
                                    <ul class="thumbnails product-list-inline-small">
                                        <div class="col-md-12" >
                                            <form method="POST">
                                                <label>Enable Email Notification</label>
                                                <input type="checkbox" name="msg_notif_email" <?php echo ($listStudent->msg_notif_email == '1') ? 'checked' : '' ?> /><br /><br />
                                                <input type="submit" name="submit" class="btn btn-success btn-sm" Value="Submit" />
                                            </form>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </section>
    </div>
</div>