<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Activity Calendar
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Activity Calendar</a>
                        </li>
                    </ul>
                </div>
                <?php if (count($events) > 0) { ?>
                    <div class="row-fliud">
                        <?php foreach ($events as $event) { ?>
                            <h3><?php echo $event->event ?></h3>
                            <h4 style="color: green"><?php echo $event->on_date ?></h4>
                            <p>
                                <?php
                                if (strlen($event->description) > 300) {
                                    echo substr($event->description, 0, 300) . '..';
                                    ?>
                                    <a style="word-wrap: break-word;" data-toggle="modal" data-target="#myModal" data-circular_title="<?php echo ucfirst($event->event) ?>" data-date="<?php echo $event->on_date ?>" data-description="<?php echo ucfirst($event->description) ?>" id="view">
                                        <span class="hidden-xs">More</span>
                                    </a>
                                    <?php
                                } else {
                                    echo $event->description;
                                }
                                ?>  
                            </p>
                            <hr/>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-info text-center"><strong>No Upcoming Events..!</strong></div>
                <?php } ?>
            </div>
        </section>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-title">
                    <h3 id="circular_title"></h3>
                    <h4 style="color: green" id="event_date"></h4>
                </div>
            </div>
            <div class="modal-body">
                <p id="circular_description"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).on('click', '#view', function () {
        var data_description = $(this).attr('data-description');
        var data_title = $(this).attr('data-circular_title');
        var data_date = $(this).attr('data-date');
        $('#circular_description').html(data_description);
        $('#circular_title').html(data_title);
        $('#event_date').html(data_date);
    });
</script>
