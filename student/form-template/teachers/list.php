<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    My Teachers
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">My Teachers</a>
                        </li>
                    </ul>
                </div>
                <?php if (count($staff_list) == 0) { ?>
                    <ul class="list-group">
                        <li class="list-group-item">
                        <center>
                            <span class="name">Sorry, No Record Found..!</span>
                        </center>
                        </li>
                    </ul>
                <?php } else { ?>
                    <div class="row tab-pane active" id="tab1">
                        <ul class="thumbnails product-list-inline-small">
                            <?php
                            foreach ($staff_list as $staff) {
                                $staff_detail = get_object('staff', $staff->staff_id);
                                if (is_object($staff_detail)) {
                                    ?>
                                    <div class="col-md-6">
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <div class="col-md-4">
                                                    <?
                                                    if (is_object($staff_detail) && $staff_detail->photo):
                                                        $im_obj = new imageManipulation()
                                                        ?>
                                                        <img src="<?= $im_obj->get_image('staff', 'medium', $staff_detail->photo); ?>"  class="img-responsive" style="height:120px;width:120px" />
                                                    <? else: ?>
                                                        <img src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="Scott Stevens" class="img-responsive" style="height:120px;width:120px" />
                                                    <? endif; ?> <br />
                                                </div>
                                                <div class="col-md-8">
                                                    <span class="name"><strong><?php echo $staff_detail->title ?> <?php echo $staff_detail->first_name ?></strong></span><br /><br />
                                                    <span><?php
                                                        $designation = staff::staffDesignationByDesignation($staff_detail->designation);
                                                        if (is_object($designation)) {
                                                            echo $designation->name;
                                                        }
                                                        ?>

                                                    </span><br />
                                                    <span>
                                                        <?php echo $staff_detail->correspondence_address1 ?>
                                                    </span><br/>
                                                    <span>
                                                        <a class="text text-success" href="<?php echo make_admin_url('messages', 'compose', 'compose', 'user_id=' . $staff_detail->id . '&user_type=staff'); ?>"><i class="fa fa-envelope"></i> Send Message</a>
                                                    </span>
                                                </div>
                                                <div class="clearfix"></div>
                                            </li>
                                        </ul>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </section>
    </div>
</div>