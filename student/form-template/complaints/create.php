<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Complaints
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Complaints</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="clearfix"></div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="fa fa-plus"></i>Add Complaints</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="validation">
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="title">Title:</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="title" class="form-control validate[required]" id="title" placeholder="Enter Title">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="detail">Description</label>
                                        <div class="col-sm-10">          
                                            <textarea class="form-control validate[required]" name="description" id="detail" placeholder="Enter Description"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="title">Against:</label>
                                         <div class="col-sm-10">
                                        <input type="text" name="against" class="form-control validate[required]" id="against" placeholder="Against">
                                         </div>

                                    </div>
                                    <div class="form-group">        
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <input type="hidden" name="school_id" value="<?php echo $school->id ?>" />
                                            <input type="hidden" name="date" value="<?php echo date('d-m-Y') ?>" />
                                            <input type="hidden" name="student_id" value="<?php echo $login_student_id ?>" />
                                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                            <a href="<?php echo make_admin_url('complaints') ?>" class="btn btn-default">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
</div>