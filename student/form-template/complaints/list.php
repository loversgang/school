<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Complaints
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Complaints</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Complaints</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (count($lists_complaints) !== 0) { ?>
                                    <div style="overflow-y: hidden">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Complaint Title</th>
                                                    <th>Complaint Description</th>
                                                    <th>Complaint Against</th>
                                                    <th>Complaint Date</th>
                                                    <th>Status</th>
                                                    <th>Feedback</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <?php foreach ($lists_complaints as $key => $list_complaint) { ?>
                                                <tr>
                                                    <td><?php echo $list_complaint['title'] ?></td>
                                                    <td>
                                                        <div class="col-md-6">

                                                            <?php
                                                            if (strlen($list_complaint['description']) > 5) {
                                                                echo substr($list_complaint['description'], 0, 5) . '.....';
                                                            } else {
                                                                echo $list_complaint['description'];
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <a href="javascript:;" class="btn btn-xs blue" id="view_description" data-title="<?php echo $list_complaint['title'] ?>" data-description="<?php echo $list_complaint['description'] ?>">View</a>

                                                        </div>

                                                    </td>
                                                    <td>
                                                        <?php echo $list_complaint['against'] ?>
                                                    </td>
                                                    <td><?php echo date('d M Y', strtotime($list_complaint['date'])) ?></td>
                                                    <td>
                                                        <?php
                                                        if ($list_complaint['status'] == 0) {
                                                            echo 'Pending';
                                                        } else {
                                                            echo 'Solved';
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>

                                                        <a href="<?php echo make_admin_url('complaints', 'feedback', 'feedback&id=' . $list_complaint['id']) ?>" class="btn btn-xs blue">View Feedback</a>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-xs red icn-only tooltips" href="<?php echo make_admin_url('complaints', 'delete', 'delete&id=' . $list_complaint['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="" data-original-title="click here to delete this record"><i class="fa fa-remove icon-white"></i></a>
                                                    </td>

                                                </tr>
                                            <?php } ?>
                                            </tbody>                          
                                        </table>
                                    </div>
                                <?php } else { ?>
                                    <div class="alert alert-info">No Complaints found..!</div>
                                <?php } ?>
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php require 'model.php' ?>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
                                                            $(document).on('click', '#view', function () {
                                                                var feedback = $(this).attr('data-feedback');
                                                                var date = $(this).attr('data-date');
                                                                $('#myModal').modal();
                                                                $('#feedback').html(feedback);
                                                                $('#feedback_date').html(date);
                                                            });

                                                            $(document).on('click', '#view_description', function () {
                                                                var title = $(this).attr('data-title');
                                                                var description = $(this).attr('data-description');
                                                                $('#myModal').modal();
                                                                $('#feedback_date').html(title);
                                                                $('#feedback').html(description);

                                                            })
</script>