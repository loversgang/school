<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Complaints Feedback
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Manage Complaints Feedback</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Complaints Feedback</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="container" id="searchable-container">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form class="form-inline" method="POST" id="validation">
                                                <div class="form-group">
                                                    <label  for="feedback">Replay</label>
                                                    <input type="text" class="form-control validate[required]" name="feedback" id="feedback" placeholder="Enter Your Feedback">

                                                </div>
                                                <div class="form-group">        
                                                    <input type="hidden" name="school_id" value="<?php echo $school->id ?>" />
                                                    <input type="hidden" name="complaint_id" value="<?php echo $_GET['id'] ?>" />
                                                    <input type="hidden" name="date" value="<?php echo time(); ?>" />
                                                    <input type="hidden" name="student_id" value="<?php echo $login_student_id ?>" />
                                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                            <br />
                                            <div class="panel panel-default">
                                                <div class="panel-heading c-list">
                                                    <span class="title">
                                                        <?php if (count($all_complaints_feeback) > 0) { ?>
                                                            Feedback Messages
                                                        <?php } else { ?>
                                                            No Messages.....!
                                                        <?php } ?>
                                                    </span>
                                                </div>
                                                <div class="row" style="display: none;">
                                                    <div class="col-xs-12">
                                                        <div class="input-group c-search">
                                                            <input type="text" class="form-control" id="contact-list-search">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button"><span class="fa fa-search text-muted"></span></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="list-group" id="contact-list">
                                                    <?php foreach ($all_complaints_feeback as $complaints_feeback) { ?>
                                                        <li class="list-group-item">
                                                            <div class="col-xs-12 col-sm-3">

                                                                <?php
                                                                if ($complaints_feeback['admin'] == 0) {
                                                                    if (is_object($logged_user) && $logged_user->photo):
                                                                        $im_obj = new imageManipulation()
                                                                        ?>
                                                                        <img src="<?= $im_obj->get_image('student', 'medium', $logged_user->photo); ?>" class="img img-responsive"/>
                                                                    <? else: ?>
                                                                        <img class="img img-responsive" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                                    <? endif; ?>
                                                                <?php } else { ?>
                                                                    <img class="img img-responsive" src="<?php echo DIR_WS_SITE_GRAPHIC . 'noimage.jpg' ?>" alt="">
                                                                <?php } ?>
                                                            </div>
                                                            <div class="col-xs-12 col-sm-9">
                                                                <span class="name">
                                                                    <?php
                                                                    if ($complaints_feeback['admin'] == 0) {
                                                                        $student = get_object('student', $complaints_feeback['student_id']);
                                                                        echo $student->first_name . ' ' . $student->last_name;
                                                                    } else {
                                                                        echo 'Admin</a>';
                                                                    }
                                                                    ?>
                                                                </span><br/>
                                                                <?php echo date('d-m-Y : h m i', $complaints_feeback['date']) ?><br /><br />
                                                                <?php echo $complaints_feeback['feedback'] ?>

                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                           </div>
                                    </div>
                                    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
                                </div>
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>