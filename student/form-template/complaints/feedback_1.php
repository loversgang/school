<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Complaints Feedback
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Manage Complaints Feedback</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Complaints Feedback</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="validation">
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="detail">Replay</label>
                                                <div class="col-sm-10">          
                                                    <textarea class="form-control validate[required]" name="feedback" id="feedback" placeholder="Enter Description"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">        
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <input type="hidden" name="school_id" value="<?php echo $school->id ?>" />
                                                    <input type="hidden" name="complaint_id" value="<?php echo $_GET['id'] ?>" />
                                                    <input type="hidden" name="date" value="<?php echo time(); ?>" />
                                                    <input type="hidden" name="student_id" value="<?php echo $login_student_id ?>" />
                                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                                    <a href="<?php echo make_admin_url('complaints') ?>" class="btn btn-default">Cancel</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <?php foreach ($all_complaints_feeback as $complaints_feeback) { ?>
                                            <div class="col-md-6">

                                                <?php
                                                if ($complaints_feeback['admin'] == 0) {
                                                    $student = get_object('student', $complaints_feeback['student_id']);
                                                    echo '<a href="javascript:;" style="margin-left:-16px">' . $student->first_name . ' ' . $student->last_name . '</a><br /><br />';
                                                } else {
                                                    echo '<a href="javascript:;" style="margin-left:-16px">Admin</a><br /><br />';
                                                }
                                                ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo date('d-m-Y : h m i', $complaints_feeback['date']) ?>

                                            </div>
                                            <div  class="clearfix"></div>
                                            <?php echo $complaints_feeback['feedback'] ?>
                                            <div  class="clearfix"></div>
                                            <br />
                                            <hr />
                                            <br />
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>