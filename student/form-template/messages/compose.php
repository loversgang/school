<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <h3 class="page-title">
            Compose Message
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="fa fa-inbox"></i>
                    <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>">Inbox</a>
                    <i class="fa fa-angle-right"></i>                                       
                </li>
                <li>
                    <a href="#">Compose Message</a>
                </li>
            </ul>
        </div>
        <div class="row tab-pane active" id="tab1">
            <ul class="thumbnails product-list-inline-small">
                <div class="row-fliud">
                    <form method="post" action="<?php echo make_admin_url('messages', 'getlist', 'getlist'); ?>">
                        <div class="form-group col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">Select User Type:</span>
                                <select class="form-control" autofocus="" name="user_type" id="user_type" <?php echo (isset($_GET['user_type'])) ? 'disabled' : '' ?>>
                                    <option value="admin">Pricipal</option>
                                    <option value="staff" <?php
                                    if (isset($_GET['user_type']) && $_GET['user_type'] == 'staff') {
                                        echo "selected";
                                    }
                                    ?>>Staff</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    <form method="post" id="validation">
                        <?php if (isset($_GET['user_id'])) { ?>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon">To:</span>
                                    <input type="hidden" name="to_id" value="<?php echo $to_id; ?>">
                                    <input disabled type="text" value="<?php echo $to_name; ?>" class="form-control" autofocus="">
                                </div>
                            </div>
                        <?php } else { ?>
                            <div id="get_users_details"></div>
                        <?php } ?>
                        <div class="form-group col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon">Subject:</span>
                                <input name="subject" type="text" class="form-control validate[required]">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-group">
                                <label class="input-group-addon" style="text-align: left; padding: 10px">Message:</label>
                                <textarea name="content" rows="10" class="form-control validate[required]"></textarea>
                            </div>
                        </div>
                        <center>
                            <input type="hidden" name="school_id" value="<?php echo $school->id; ?>">
                            <input type="hidden" name="from_id" value="<?php echo $id; ?>">
                            <input type="hidden" name="from_type" value="student">
                            <input type="hidden" name="to_type" id="to_type" value="<?php echo $_GET['user_type']; ?>">

                            <button name="submit" value="Submit" class="btn btn-success"><i class="fa fa-envelope"></i> Send</button>
                            <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>" class="btn btn-green btn-danger"><i class="fa fa-close"></i> Cancel</a>
                        </center>
                    </form>
                </div>
            </ul>
        </div>
    </div>
</div>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(function () {
        $('select#user_type').change(function () {
            var type = $(this).val();
            $('#to_type').val(type);
            if (type === 'staff') {
                $("#get_users_details").html('<div style="width:100%;text-align:center;padding-top:5px;"><i class="fa fa-spinner fa-pulse fa-5x"></i></div>');
                $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_staff'}, function (data) {
                    $('#get_users_details').html(data);
                });
            }
            if (type === 'admin') {
                $('#get_users_details').html('<div style="width:100%;text-align:center;padding-top:5px;"><i class="fa fa-spinner fa-pulse fa-5x"></i></div>');
                $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_admin'}, function (data) {
                    $('#get_users_details').html(data);
                });
            }
        }).change();
    }).change();
</script>