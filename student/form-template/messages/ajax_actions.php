<?php
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');

if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'get_staff') {
        $QueryObj = new staff();
        $staff_list = $QueryObj->getSchoolStaffDetailActiveDeactive($school->id, '1');
        ?>
        <div class="form-group col-md-12">
            <div class="input-group">
                <span class="input-group-addon">To:</span>
                <select class="form-control" data-placeholder="Select Student" name='to_id' id='to_id'>
                    <?php
                    foreach ($staff_list as $staff) {
                        $to_name = $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name;
                        ?>
                        <option value="<?php echo $staff->id; ?>"><?php echo $to_name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php
    }

    if ($act == 'get_admin') {
        ?>
        <div class="form-group col-md-12">
            <div class="input-group">
                <span class="input-group-addon">To:</span>
                <input type="hidden" name="to_id" value="1">
                <input disabled type="text" value="Principal" class="form-control" autofocus="">
            </div>
        </div>
        <?php
    }
}