<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <h3 class="page-title">
            Message Details
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <i class="fa fa-inbox"></i>
                    <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>">Inbox</a>
                    <i class="fa fa-angle-right"></i>                                       
                </li>
                <li>
                    <a href="#">Message Details</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-sm-12 widget">
                <div class="well" style="margin-top: -20px;">
                    <div id="email-view" class="email-view">
                        <?php if ($_GET['msg_box'] == 'inbox') { ?>
                            <?php if ($header->from_type == 'admin') { ?>
                                <span class="pull-right"><a href="<?php echo make_admin_url('messages', 'compose', 'compose', 'user_id=' . $school->id . '&user_type=admin') ?>"><i class="fa fa-reply"></i> Reply</a></span>
                            <?php } else { ?>
                                <span class="pull-right"><a href="<?php echo make_admin_url('messages', 'compose', 'compose', 'user_id=' . $header->from_id . '&user_type=staff') ?>"><i class="fa fa-reply"></i> Reply</a></span>
                            <?php } ?>
                        <?php } ?>
                        <h4><?php echo $header->subject; ?></h4>
                        <div class="email-date">
                            <span style="color: green"><?php echo date('h:i A', strtotime($header->time)); ?></span> . <a href="<?php echo make_admin_url('messages', 'read', 'read', 'act=delete&h_id=' . $header->id . '&msg_box=' . $_GET['msg_box']); ?>"><i style="color: darkred" class="fa fa-trash"></i></a>
                        </div>
                        <br/>
                        <p><?php echo $message->content; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>