<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    School Sales
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">School Sales</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-12 col-sm-12">
                    <div class="card hovercard">
                        <div class="card-background">
                            <img class="card-bkimg" alt="" src="http://lorempixel.com/100/100/people/9/">
                        </div>
                        <div class="useravatar">
                            <?
                            if (is_object($s_info) && $s_info->photo):
                                $im_obj = new imageManipulation()
                                ?>
                                <img src="<?= $im_obj->get_image('staff', 'medium', $s_info->photo); ?>" class="img img-circle" />
                            <? else: ?>
                                <img src="<?php echo DIR_WS_SITE_GRAPHIC ?>noimage.jpg" alt="" class="img img-circle">
                            <? endif; ?> 
                        </div>
                        <div class="card-info"> <span class="card-title"><?php echo $s_info->title; ?> <?php echo $s_info->first_name; ?> <?php echo $s_info->last_name; ?></span>
                        </div>
                    </div>
                    <div class="row" style="padding-top:10px">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1">

                                <div class="col-md-4">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">Basic Information</div>
                                        <div class="panel-body">
                                            <div class="info_profile" style="word-wrap:break-word">
                                                <strong>Designation:</strong>
                                                <br />
                                                <?php
                                                $s_d = get_object('staff_designation_master', $s_info->designation);
                                                echo $s_d->name;
                                                ?>
                                                <hr/>
                                                <strong>Joining Date :</strong>
                                                <br />
                                                <?= $s_info->join_date ?>
                                                <hr/>
                                                <strong>Sex:</strong>
                                                <br />
                                                <?= $s_info->sex ?>
                                                <hr />
                                                <strong>Marital Status :</strong>
                                                <br />
                                                <?= $s_info->marital ?>
                                                <hr/>

                                                <strong>Date Of Birth : </strong>
                                                <br />
                                                <?= $s_info->date_of_birth ?>
                                                <hr />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">Address Information</div>
                                        <div class="panel-body">
                                            <div class="info_profile" style="word-wrap:break-word"><strong>Email : </strong><br>
                                                <a href="mailto:<?= $s_info->email ?>"> <?= $s_info->email ?></a><hr />

                                                <strong>Mobile Number : </strong>
                                                <br />
                                                <?= $s_info->phone ?>
                                                <hr/>
                                                <strong>Permanent Address:</strong>
                                                <br />
                                                <?php echo $s_info->permanent_address1 ?>
                                                <hr />
                                                <strong>Correspondence Address :</strong>
                                                <br />
                                                <?php echo $s_info->correspondence_address1 ?>
                                                <hr />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">Qualification Information</div>
                                        <div class="panel-body">
                                            <div class="info_profile" style="word-wrap:break-word">
                                                <?php if (is_object($s_qualification)) { ?>
                                                    <div class="info_profile"><strong>Qualification : </strong><br>
                                                        <?= $s_qualification->qualification ?><hr />

                                                        <strong>Institute : </strong>
                                                        <br />
                                                        <?= $s_qualification->institute ?>
                                                        <hr/>
                                                        <strong>Year Of Passing :</strong>
                                                        <br />
                                                        <?= $s_qualification->year_of_passing ?>
                                                        <hr />
                                                        <strong>Percentage (%) :</strong>
                                                        <br />
                                                        <?= $s_qualification->percentage ?>
                                                        <hr />
                                                    <?php } else { ?>
                                                        Sorry, No Record Found..!
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>