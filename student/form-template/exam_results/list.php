<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Exam Results
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Exam Results</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Subjects</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <form  method="POST" id="form_data" name="form_data" >
                                    <div style="overflow-y: hidden">
                                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                                            <thead>
                                                <tr>
                                                    <th>Examination Title</th>
                                                    <th>Subject</th>
                                                    <th>Teacher Name</th>
                                                    <th>Examination Date</th>
                                                    <th>Max Mark</th>
                                                    <th>Min Mark</th>
                                                    <th>Mark Obtain</th>
                                                    <th>Percentage</th>
                                                    <!--<th>Grade</th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($examination_rel as $student_exam) { ?>
                                                    <tr>
                                                        <td><?php echo $student_exam['title'] ?></td>
                                                        <td>
                                                            <?php
                                                            $subject_master = get_object('subject_master', $student_exam['subject_id']);
                                                            echo $subject_master->name;
                                                            ?>

                                                        </td>
                                                        <td>
                                                            <?php
                                                            $staff = get_object('staff', $student_exam['teacher_id']);
                                                            echo $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name;
                                                            ?>

                                                        </td>

                                                        <td><?php echo $student_exam['date_of_examination'] ?></td>
                                                        <td><?php echo $student_exam['maximum_marks'] ?></td>
                                                        <td><?php echo $student_exam['minimum_marks'] ?></td>
                                                        <td><?php echo $student_exam['marks_obtained'] ?></td>
                                                        <td>
                                                            <?php
                                                            echo number_format($student_exam['marks_obtained'] / $student_exam['maximum_marks'] * 100, 2) . '%';
                                                            ?>
                                                        </td>
                                                    </tr>

                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </form>    
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">
    $(".session_filter").on("change", function () {
        var session_id = $(this).val();
        $('#session_filter').submit();
    });
</script>