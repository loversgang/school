<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).on('click', '#view', function () {
        $('#data-file').hide();
        var data_detail = $(this).attr('data-detail');
        var data_title = $(this).attr('data-title');
        var data_file = $(this).attr('data-file');
        var data_valid = $(this).attr('data-valid');
        $('#data-detail').html(data_detail);
        $('#data-title').html(data_title);
        if (data_valid) {
            $('#data-file').show();
            $('#data-file').html('<a class="btn btn-xs blue icn-only tooltips" href="' + data_file + '" target="_blank">View File</a>');
        }
        $('#myModal').modal();
    });


    $(document).on('click', '#mark', function () {
        var id = $(this).attr('data-id');
        $('#mark[data-id="' + id + '"]').attr('disabled', true);
        $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=homework'); ?>', {action: 'mark', id: id}, function (data) {
            $('#mark[data-id="' + id + '"]').attr('disabled', false);
            $('#mark[data-id="' + id + '"]').hide()
            $('#marked[data-id="' + id + '"]').show()
        });
    });

    $(document).on('click', '#marked', function () {
        var id = $(this).attr('data-id');
        $('#marked[data-id="' + id + '"]').attr('disabled', true);
        $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=homework'); ?>', {action: 'marked', id: id}, function (data) {
            $('#marked[data-id="' + id + '"]').attr('disabled', false);
            $('#mark[data-id="' + id + '"]').show()
            $('#marked[data-id="' + id + '"]').hide()
        });
    });
</script>