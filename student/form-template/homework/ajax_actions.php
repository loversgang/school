<?php

include_once(DIR_FS_SITE . 'include/functionClass/homeworkClass.php');
if (isset($_POST)) {
    extract($_POST);
    if ($action == 'mark') {
        $query = new homework();
        $query->saveMark($id, 1);
    }

    if ($action == 'marked') {
        $query = new homework();
        $query->saveMark($id, 0);
    }
}
?>