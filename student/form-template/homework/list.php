<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Assignments
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Assignments</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Assignments</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (count($lists) !== 0) { ?>
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <th>Teacher</th>
                                        <th>Date of Assignment</th>
                                        <th>Date of Submission</th>
                                        <th>Subject</th>
                                        <th>Assignment Title</th>
                                        <th>Assignment Detail</th>
                                        <th>Remarks</th>
                                        </tr>
                                        </thead>
                                        <?php foreach ($lists as $key => $list) { ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    $staff = get_object('staff', $list['staff_id']);
                                                    echo $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name;
                                                    ?>
                                                </td>
                                                <td><?php echo date('d M Y', strtotime($list['date'])) ?></td>
                                                <td><?php echo date('d M Y', strtotime($list['submission'])) ?></td>
                                                <td>
                                                    <?php
                                                    $subject_master = get_object('subject_master', $list['subject']);
                                                    echo ucfirst($subject_master->name)
                                                    ?>
                                                </td>
                                                <td><?php echo ucfirst($list['title']) ?></td>
                                                <td>
                                                    <span class="col-md-6">
                                                        <?php if (strlen($list['detail']) > 10) { ?>
                                                            <?php echo substr(ucfirst($list['detail']), 0, 10) ?>....
                                                        <?php } else { ?>
                                                            <?php echo $list['detail'] ?>
                                                        <?php } ?>
                                                    </span>
                                                    <span class="col-md-6">
                                                        <a href="javascript:;" data-detail="<?php echo ucfirst($list['detail']) ?>" data-title="<?php echo ucfirst($list['title']) ?>" data-file="<?php echo DIR_WS_SITE_UPLOAD . 'file/homework/' . $list['file'] ?>" data-valid="<?php echo $list['file'] ?>" id="view" class="btn btn-xs blue">View</a>
                                                    </span>
                                                </td>
                                                <td>
                                                    <a class="btn btn-xs yellow tooltips" href="<?php echo make_admin_url('remarks', 'list', 'list&id=' . $list['id']) ?>" title="click here to view this record">View</a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>                          
                                    </table>
                                <?php } else { ?>
                                    <div class="alert alert-info">No Assignments found..!</div>
                                <?php } ?>
                            </div>
                        </div>                                                
                    </div>
                </div>
                <?php require 'model.php'; ?>
            </div>
        </section>
    </div>
</div>
<?php require 'script.php'; ?>