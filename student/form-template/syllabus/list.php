<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Syllabus
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Syllabus</a>
                        </li>
                    </ul>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Syallabus</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (count($syllabus_details) > 0) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th>Subject</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($syllabus_details as $syllabus) { ?>
                                                <tr>
                                                    <td>
                                                        <?php $subject = get_object('subject_master', $syllabus->subject_id); ?>
                                                        <?= $subject->name ?>
                                                    </td>
                                                    <td>
                                                        <a href="<?php echo DIR_WS_SITE_UPLOAD . 'file/syllabus/' . $syllabus->file ?>" class="btn btn-success btn-sm hidden-xs" target="_blank">
                                                            <i class="fa fa-download"></i>
                                                            Download
                                                        </a>
                                                        <a href="<?php echo DIR_WS_SITE_UPLOAD . 'file/syllabus/' . $syllabus->file ?>" class="btn btn-success btn-sm btn-block visible-xs" target="_blank">
                                                            <i class="fa fa-download"></i>
                                                            Download
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php } else { ?>
                                <div class="alert alert-info text-center"><strong>No Syllabus Found..!</strong></div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>