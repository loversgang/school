<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    Remarks
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Remarks</a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>
                <?php
                display_message(1);
                $error_obj->errorShow();
                ?>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-file-text"></i>Manage Remarks</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if (count($remarks) > 0) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="sample_">
                                        <thead>
                                            <tr>
                                                <th>Teacher</th>
                                                <th>Remark</th>
                                            </tr>
                                        </thead>
                                        <?php foreach ($remarks as $remark) { ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    $staff = get_object('staff', $remark->staff_id);

                                                    if (is_object($staff)) {
                                                        echo $staff->title.' '.$staff->first_name . ' ' . $staff->last_name;
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $remark->remarks ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>                          
                                    </table>
                                <?php } else { ?>
                                    <div class="alert alert-info">You added zero Remarks..!</div>
                                <?php } ?>
                            </div>
                        </div>                                                
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>