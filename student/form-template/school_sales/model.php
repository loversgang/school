<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="sales_title"></h4>
            </div>
            <div class="modal-body">
                <p id="sales_description"></p><br />
                <div id="sales_file"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-sm hidden-xs" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

