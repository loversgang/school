<div class="page-content-wrapper">
    <div class="page-content" style="min-height:826px">
        <section id="portfolio">
            <div class="container-fluid">
                <h3 class="page-title">
                    School Products
                </h3>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo make_admin_url('home'); ?>">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">School Products</a>
                        </li>
                    </ul>
                </div>
                <div class="row tab-pane active" id="tab1">
                    <ul class="thumbnails product-list-inline-small">
                        <div class="row-fluid">
                            <div class="col-md-3"></div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3"></div>
                            <div class="col-md-3">
                                <select class="form-control" name="select_sales_category" id="select_sales_category">
                                    <?php foreach ($lists_sales_category as $key => $list) { ?>
                                        <option value="<?php echo $list['id'] ?>"><?php echo ucfirst($list['title']) ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div> <br /><br />

                        <div id="display_sales_category"></div>
                    </ul>
                </div>
            </div>
        </section>
    </div>
</div>
<?php require 'model.php' ?>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(function () {
        $('select#select_sales_category').change(function () {
            var id = $(this).val();
            $('#display_sales_category').html('<div style="width:100%;text-align:center;padding-top:5px;"><i class="fa fa-spinner fa-pulse fa-5x"></i></div>');
            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=school_sales'); ?>', {action: 'select_sales_category', id: id}, function (data) {
                $('#display_sales_category').html(data);
            });
        }).change();
        $(document).on('click', '#view_detail', function () {
            var data_description = $(this).attr('data-description');
            var data_title = $(this).attr('data-title');
            var data_file = $(this).attr('data-file');
            $.post('', {action: 'select_category_click', data_description: data_description}, function (data) {
                $('#sales_description').html(data_description);
                $('#sales_title').html(data_title);
                $('#sales_file').html(data_file);
            });
        }).change();
    });
</script>

