<?php
include_once(DIR_FS_SITE . 'include/functionClass/school_salesClass.php');
if (isset($_POST['action'])) {
    extract($_POST);
    if ($action == 'select_sales_category') {
        $query = new school_sales;
        $query->Where = "WHERE `sales_cat_id` = '" . $id . "' ORDER BY `id` DESC";
        $lists = $query->ListOfAllRecords();
        if (count($lists) > 0) {
            ?>
            <?php foreach ($lists as $key => $list) { ?>
                <div class="col-md-3">
                    <ul class="list-group">
                        <li class="list-group-item">
                        <center>
                            <?php echo ($list['file'] == '') ? '<img src="' . DIR_WS_SITE_GRAPHIC . 'noimage.jpg" style="height:120px;" /><br />' : '<img src="' . DIR_WS_SITE_UPLOAD . 'file/school_sales/' . $list['file'] . '" class="img img-responsive" style="height:120px;" />'; ?>
                            <br />
                            <span class="name"><?php echo ucfirst($list['name']) ?>
                                <p><i class="fa fa-inr"></i><?php echo number_format($list['price'], 2) ?></p>
                            </span>
                            <span class="name"><a class="btn btn-success btn-sm" style="cursor:pointer" data-toggle="modal" data-target="#myModal" data-id="<?php echo $list['id'] ?>" data-description="<?php echo ucfirst($list['description']) ?>" data-title='<?php echo ucfirst($list['name']) ?>' data-file='<?php echo ($list['file'] == '') ? '<img src="' . DIR_WS_SITE_GRAPHIC . 'noimage.jpg" />' : '<img src="' . DIR_WS_SITE_UPLOAD . 'file/school_sales/' . $list['file'] . '" class="img img-responsive" />'; ?>' id="view_detail">
                                    <i class="fa fa-file-text"></i>
                                    Detail
                                </a></span><br />
                        </center>
                        <div class="clearfix"></div>
                        </li>
                    </ul>
                </div>
                <?php
            }
        } else {
            ?>
            <div class="alert alert-info text-center"><strong>No Product Found..!</strong></div>
            <?php
        }
    }
}
?>
       