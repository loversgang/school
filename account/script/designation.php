<?php

include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');


isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';

isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['type']) ? $type = $_GET['type'] : $type = '';
$modName = 'designation';

// Pay Rules
// Get Earning Rules
$obj = new staffPayHeads;
$earningHeads = $obj->getPayRuleByType('earning');

// Get Deduction Rules
$object = new staffPayHeads;
$deductionHeads = $object->getPayRuleByType('deduction');
#handle actions here.
switch ($action):
    case'list':
        # All data from staff_designation_master table
        $QueryObj = new staffDesignation;
        $QueryObj->listAll($school->id);
        break;

    case'insert':
        # Add new designation
        if (isset($_POST['submit'])):
            if (isset($_POST['earning'])) {
                $_POST['earning_heads'] = serialize($_POST['earning']);
            } else {
                $_POST['earning_heads'] = '';
            }
            if (isset($_POST['deduction'])) {
                $_POST['deduction_heads'] = serialize($_POST['deduction']);
            } else {
                $_POST['deduction_heads'] = '';
            }
            $QueryObj = new staffDesignation;
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_STAFF_DESIGNATION_ADDED);
            Redirect(make_admin_url('designation', 'list', 'list'));
        endif;
        break;

    case'update':
        # edit Staff Designation
        $QuObj = new staffDesignation;
        $object = $QuObj->getDesignation($id);

        $earning_heads = array();
        if ($object->earning_heads) {
            $earning_heads = unserialize(html_entity_decode($object->earning_heads));
        }

        $deduction_heads = array();
        if ($object->deduction_heads) {
            $deduction_heads = unserialize(html_entity_decode($object->deduction_heads));
        }
        if (isset($_POST['submit'])):
            if (isset($_POST['earning'])) {
                $_POST['earning_heads'] = serialize($_POST['earning']);
            } else {
                $_POST['earning_heads'] = '';
            }
            if (isset($_POST['deduction'])) {
                $_POST['deduction_heads'] = serialize($_POST['deduction']);
            } else {
                $_POST['deduction_heads'] = '';
            }
            $QueryObj = new staffDesignation;
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_STAFF_DESIGNATION_UPDATE);
            Redirect(make_admin_url('designation', 'list', 'list'));
        endif;
        break;

    case'delete':
        # delete record
        $QueryObj = new staffDesignation();
        $QueryObj->deleteRecord($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('designation', 'list', 'list'));
        break;

    default:break;

endswitch;
?>

