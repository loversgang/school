<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');


$modName='content';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';


switch ($action):
	case'list':
		/* Get School Pages*/
		 $QueryObj=new content();
         $QueryObj->getSchoolPages($school->id);
         break;
	case'insert':
		/* Insert School Pages*/
			if(isset($_POST['submit'])):
						$QueryObj = new content();
						$QueryObj->saveData($_POST);
						$admin_user->set_pass_msg(MSG_CONTENT_ADDED);
						Redirect(make_admin_url('content', 'list', 'list'));
					elseif(isset($_POST['cancel'])):
						$admin_user->set_error();  
						$admin_user->set_pass_msg(OPERATION_CANCEL);
						Redirect(make_admin_url('content', 'list', 'list'));   
			endif;
		break;	
	case'update':
			/* Get School Pages by Id*/
						$QueryPage = new content();
						$object=$QueryPage->getPage($id);			
			
			/* Update School Pages*/
			if(isset($_POST['submit'])):
						$QueryObj = new content();
						$QueryObj->saveData($_POST);
						$admin_user->set_pass_msg(MSG_CONTENT_UPDATE);
						Redirect(make_admin_url('content', 'list', 'list'));
                  elseif(isset($_POST['cancel'])):
						$admin_user->set_error();  
						$admin_user->set_pass_msg(OPERATION_CANCEL);
						Redirect(make_admin_url('content', 'list', 'list'));       
	        endif;   
		break;
        case'delete':
			#delete page 			
					$QueryObj = new content();
					$QueryObj->deleteRecord($id);		
					$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
					Redirect(make_admin_url('content', 'list', 'list')); 			
			break;	
	default:break;
endswitch;
?>
