<?php
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');


isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';

isset($_GET['id'])?$id=$_GET['id']:$id='0';

$modName='fee_head';
#handle actions here.
switch ($action):
	case'list':			
			# fee records
				$QueryObj=new feeType;
				$QueryObj->listAll($school->id);
		break;			

	case'insert':
		# ADD Fee Heads Record
			if(isset($_POST['submit'])):			
	            $QueryObj=new feeType;
				$QueryObj->saveData($_POST); 
	            $admin_user->set_pass_msg(MSG_FEE_HEADS_ADDED);
                Redirect(make_admin_url('fee_head', 'list', 'list'));				
			endif;		
		break;	

	case'update':
		# edit fee heads
		$QuObj=new feeType;
		$object=$QuObj->getRecord($id);
		if(isset($_POST['submit'])):			
	            $QueryObj=new feeType;
				$QueryObj->saveData($_POST); 
	            $admin_user->set_pass_msg(MSG_FEE_HEAD_UPDATE);
                Redirect(make_admin_url('fee_head', 'list', 'list'));
		endif;		
		break;		

	case'delete':
	    # delete record		
				$QueryObj = new feeType();
				$QueryObj->deleteRecord($id);	
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('fee_head', 'list', 'list'));					
		break;

	default:break;

endswitch;


?>

