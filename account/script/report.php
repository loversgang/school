<?php

/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = 'A';
isset($_REQUEST['interval_id']) ? $interval_id = $_REQUEST['interval_id'] : $interval_id = '';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
isset($_REQUEST['report_type']) ? $report_type = $_REQUEST['report_type'] : $report_type = 'admission_detail';
isset($_REQUEST['date']) ? $date = $_REQUEST['date'] : $date = '';
isset($_REQUEST['date1']) ? $date1 = $_REQUEST['date1'] : $date1 = '0';
isset($_REQUEST['month']) ? $month = $_REQUEST['month'] : $month = '';
isset($_REQUEST['exam']) ? $exam = $_REQUEST['exam'] : $exam = '0';
isset($_REQUEST['v_report_type']) ? $v_report_type = $_REQUEST['v_report_type'] : $v_report_type = 'vehicle_info';
isset($_REQUEST['s_report_type']) ? $s_report_type = $_REQUEST['s_report_type'] : $s_report_type = 'recieve';
isset($_REQUEST['f_report_type']) ? $f_report_type = $_REQUEST['f_report_type'] : $f_report_type = 'recieve';
isset($_REQUEST['e_report_type']) ? $e_report_type = $_REQUEST['e_report_type'] : $e_report_type = 'live';
isset($_REQUEST['ex_type']) ? $ex_type = $_REQUEST['ex_type'] : $ex_type = '';
isset($_REQUEST['a_type']) ? $a_type = $_REQUEST['a_type'] : $a_type = '';
isset($_REQUEST['f_type']) ? $f_type = $_REQUEST['f_type'] : $f_type = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['subject']) ? $subject = $_REQUEST['subject'] : $subject = '';
isset($_REQUEST['tmp']) ? $tmp = $_REQUEST['tmp'] : $tmp = 'student';
isset($_REQUEST['from']) ? $from = $_REQUEST['from'] : $from = '';
isset($_REQUEST['to']) ? $to = $_REQUEST['to'] : $to = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = '';
isset($_GET['sess_int']) ? $sess_int = $_GET['sess_int'] : $sess_int = '';
#handle actions here.
/* Convert the date format in case of class attendance */
if (!empty($date)):
    if ($a_type == 'class_wise'):
        $date = date('m-Y', strtotime('01 ' . $date));
    endif;
    $pos = strrpos($date, "-");
    if ($pos == true):
        $month = $date;
        $m_y = explode('-', $date);
        $date = $m_y['0'];
        $year = $m_y['1'];
    endif;
endif;
if (empty($month)):
    $month = date('m-Y');
endif;
$modName = 'report';
if (empty($from) && empty($to)):
    $to = date('Y-m-d');
    $from = date('Y-m-d', strtotime($to . ' -1 months'));
elseif (!empty($from) && empty($to)):
    $to = date('Y-m-d', strtotime($from . ' +1 months'));
elseif (empty($from) && !empty($to)):
    $from = date('Y-m-d', strtotime($to . ' -1 months'));
endif;
#report type array
$report_array = array(
    "withdrawal_detail" => "Admission & Withdrawal Register",
    "student_type" => "Granted Concession Students Detail",
    "contact_detail" => "Photo,Address And Contact Detail",
    "admission_detail" => "Registration & Date Of Birth",
    "category_detail" => "SC,ST & OBC Student Detail",
    "address_label" => "Students Address Label",
    "subject_detail" => "Subjects Detail"
);
#Vehicle report type array
$vehicle_report_array = array("vehicle_info" => "Vehicle Information",
    "vendor" => "Vendor OR Owned",
    "vendor_info" => "Vendor Information",
    "vendor_reg" => "Vendor Registration"
);
#Enquiry report type array
$enquiry_report_array = array("live" => "Live Enquiries",
    "confirm" => "Confirm Enquiries",
    "dead" => "Dead Enquiries"
);
#Vehicle report type array
$salary_report_array = array("pending" => "Pending Salary",
    "recieve" => "Paid Salary"
);
$QueryObjQ = new session();
$AllSessYear = $QueryObjQ->listOfYearSess($school->id);
if (empty($sess_int)):
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
endif;
switch ($action):
    case'list':
        if (!empty($from) && !empty($to)):
            $time2 = strtotime($from);
            if (strtotime($to) > strtotime(date('Y-m-d', $time2) . ' +3 months')):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_NOT_ALLLOWED_MORE_THEN_THREE_MONTH);
                Redirect(make_admin_url('account', 'list', 'list&from=&to=&cat=' . $cat));
            endif;
        endif;
        #Get Exp Category
        $QueryExpCat = new schoolExpenseCategory();
        $QueryExpCat->listAll($school->id);
        #Get Records
        $QueryObj = new schoolExpense();
        $record = $QueryObj->ExpenseReport($school->id, $from, $to, $cat);
        break;
    case'student':
        $QueryObjQ = new session();
        $AllSessYear = $QueryObjQ->listOfYearSess($school->id);
        #get_all session 
        $QuerySession = new session();
        $session_array = $QuerySession->listAll($school->id, '1', 'array');
        // Get Session Detail By Id
        if (!empty($session_id)):
            $obj = new session;
            $session_obj = $obj->getRecord($session_id);
        else:
            $obj = new session;
            $session_obj = $obj->getRecord($session_array['0']->id);
        endif;
        if (empty($session_id)):
            if (!empty($session_array)):
                $session_id = $session_array['0']->id;
            endif;
        endif;
        #Get Struck Off Students
        $obj = new stuck_off;
        $All_Session = $obj->listStuckOffStudents($session_id, $ct_sec, $sess_int);
        #get Student Report 
        $StudentObj = new studentSession();
        $record = $StudentObj->get_various_student_report($school->id, $session_id, $ct_sec, $report_type);
        if ($report_type == 'category_detail' && !empty($record)):
            foreach ($record as $key => $value):
                if (($value->category == '') || ($value->category == 'None') || ($value->category == 'GEN')):
                    unset($record[$key]);
                endif;
            endforeach;
        endif;
        break;
    case'fee':
        $QueryObjQ = new session();
        $AllSessYear = $QueryObjQ->listOfYearSess($school->id);
        if (empty($sess_int)):
            $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
        endif;
        #get_all session 
        $QuerySession = new session();
        $session_array = $QuerySession->listAll($school->id, '1', 'array');
        if ($f_type == 'month_wise'):
            if (empty($session_id)):
                if (!empty($session_array)):
                //$session_id=$session_array['0']->id;
                endif;
            endif;
            /* Get session Pages */
            $QuerySec = new studentSession();
            $QuerySec->listAllSessionSection($session_id);
            //echo $ct_sec; exit;
            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);
            $session = get_object('session', $session_id);
            #get Session Interval
            $QueryInt = new session_interval();
            $interval = $QueryInt->listOfInterval($session_id);
            $current_date = date('Y-m-d');
            $current = '0';
        elseif ($f_type == 'head_wise'):
            if (empty($session_id)):
                if (!empty($session_array)):
                //$session_id=$session_array['0']->id;
                endif;
            endif;
            /* Get session Pages */
            $QuerySec = new studentSession();
            $QuerySec->listAllSessionSection($session_id);
            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);
            $session = get_object('session', $session_id);
            #get Session Interval
            $QueryInt = new session_interval();
            $interval = $QueryInt->listOfInterval($session_id);
            //pr($interval);
            #Get Session Fee Heads
            $QuerySess = new session_fee_type();
            $Sesspay_heads = $QuerySess->sessionPayHeadsWithKey($session_id);
            $current_date = date('Y-m-d');
            $current = '0';
            if ($session_id) {
                if (empty($interval_id)) {
                    $interval_id = $interval[0]->id;
                }
            }
            $interval_detail = get_object('session_interval', $interval_id);
            elseif ($f_type == 'class_due') :
                
            if (empty($session_id)):
                if (!empty($session_array)):
                //$session_id=$session_array['0']->id;
                endif;
            endif;
            $QueryObjQ = new session();
            $AllSessYear = $QueryObjQ->listOfYearSess($school->id);
            if (empty($sess_int)):
                $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
            endif;
            /* Get session Pages */
            $QuerySec = new studentSession();
            $QuerySec->listAllSessionSection($session_id);
            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->getIntervalDuePendingFee($session_id, $ct_sec);
        elseif ($f_type == 'head_conc'):
            #concession list array
            $conc_heads = array('Full Poverty' => '0', 'Half Poverty' => '0', 'Scholarship' => '0', 'Special Student' => '0', 'Sports Student' => '0', 'Sibling Student' => '0');
            if (empty($session_id)):
                if (!empty($session_array)):
                    $session_id = $session_array['0']->id;
                endif;
            endif;
            /* Get session Pages */
            $QuerySec = new studentSession();
            $QuerySec->listAllSessionSection($session_id);
            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->getAllConcessionWiseAmount($session_id, $ct_sec);
            $session = get_object('session', $session_id);
            $current_date = date('Y-m-d');
            $current = '0';
        endif;
        break;
    case'daily':
        #Get Student Fee Record
        $QueryOb = new studentFeeRecord();
        $record = $QueryOb->SingleDayReport($school->id, $to);
        break;
    case'attendance':
        $QueryObjQ = new session();
        $AllSessYear = $QueryObjQ->listOfYearSess($school->id);

        if (empty($sess_int)):
            $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
        endif;

        #get_all session 
        $QuerySession = new session();
        $session_array = $QuerySession->listAll($school->id, '1', 'array');

        if (empty($session_id)):
            if (!empty($session_array)):
                $session_id = $session_array['0']->id;
            endif;
        endif;

        if ($a_type == 'half'):
            #Get Section Students
            $QueryOb = new attendance();
            $record = $QueryOb->getHalfAttendance($session_id, $from, $to);



        elseif ($a_type == 'class_wise'):
            $sess_int = isset($_GET['sess_int']) ? $_GET['sess_int'] : '2015-04-01,2016-03-31';
            if (empty($date)): $date = date('m');
            endif;
            if (empty($year)): $year = date('Y');
            endif;

            $M_obj = new schoolHolidays();
            $M_holiday = $M_obj->checkMonthlyHoliday($school->id, $date, $year);

            #count working days
            $working_days = '';
            if (!empty($date) && !empty($year)):
                $first_date_of_month = $year . "-" . $date . "-01";
                $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($year . "-" . $date . "-1"))));

                $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
                $total_month_days = (date('d', strtotime($last_date_of_month)));
                $working_days = $total_month_days - $total_sunday;
            endif;
            #Get Section Students
            $QueryOb = new attendance();
            $record = $QueryOb->getClassWiseAttendance($school->id, $first_date_of_month, $last_date_of_month, $sess_int);

        elseif ($a_type == 'single'):

            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);

            if (!empty($student_id)):
                #get Student detail	
                $QueryStu = new studentSession();
                $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

                #get Student Permanent Address	
                $QueryP = new studentAddress();
                $s_p_ad = $QueryP->getStudentAddressByType($student_id, 'permanent');

                #get Session Info	
                $session_info = get_object('session', $session_id);

            endif;
        else:
            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);
        endif;

        if (!($date)): $date = date('m');
        endif;
        if (!isset($year)): $year = date('Y');
        endif;

        #count working days
        $working_days = '';
        if (!empty($date) && !empty($year)):
            $first_date_of_month = $year . "-" . $date . "-01";
            $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($year . "-" . $date . "-1"))));

            $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
            $total_month_days = (date('d', strtotime($last_date_of_month)));
            $working_days = $total_month_days - $total_sunday;
        endif;

        $session_info = get_object('session', $session_id);

        break;

    case'exam':
        #get_all session 
        $QuerySession = new session();
        $session_array = $QuerySession->listAll($school->id, '1', 'array');

        if (empty($session_id)):
            if (!empty($session_array)):
                $session_id = $session_array['0']->id;
            endif;
        endif;

        $session_info = get_object('session', $session_id);

        #Get exam Grades
        $QueryGrOb = new examGrade();
        $all_grade = $QueryGrOb->listAll($school->id);

        if ($ex_type == 'term'):
            #groups
            $QueryObjExam = new examinationGroup();
            $groups = $QueryObjExam->getTermExamSessionSection($session_id, $ct_sec);
            if (!empty($groups) && empty($exam)):
                $exam = $groups['0']->id;
            endif;

            #get exam Info
            $QueryG = new examinationGroup();
            $group = $QueryG->getRecord($exam);

            if (!is_object($group)): $group->exam_id = '0';
            endif;
            #get Subject names
            $QueryExam = new examination();
            $exams = $QueryExam->groupExaminations($group->exam_id);

            #Get exam Students result
            $QueryOb = new examinationMarks();
            $record = $QueryOb->getExamStudents($group->exam_id);

        elseif ($ex_type == 'subject'):
            #here subject will be the exam id and exam will be the group;
            if (!empty($subject)):
                #Get exam Students result
                $QueryOb = new examinationMarks();
                $record = $QueryOb->getExamStudents($subject);

                #get exam group Info
                $QueryG = new examinationGroup();
                $group = $QueryG->getRecord($exam);

                #get Exam detail with its subject
                $QueryExam = new examination();
                $exams = $QueryExam->groupExaminations($group->exam_id);
            endif;

            #All groups
            $QueryObjExam = new examinationGroup();
            $groups = $QueryObjExam->getTermExamSessionSection($session_id, $ct_sec);

            if (!empty($groups) && empty($exam)):
                $exam = $groups['0']->id;
            endif;

            if (!empty($exam)):
                $group_detail = get_object('student_examination_group', $exam);

                #get Session Info
                $QueryExsub = new examinationGroup();
                $AllSubject = $QueryExsub->getSubjectByExams($group_detail->exam_id);

            endif;

        elseif ($ex_type == 'term_subject'):
            #Get Subject List
            $QuerySubject = new subject();
            $sessionSubjects = $QuerySubject->listSessionSubject($session_info->compulsory_subjects, $session_info->elective_subjects, '1', 'array');

            if (empty($subject)):
                $subject = $sessionSubjects['0']->id;
            endif;

            $subject_detail = get_object('subject_master', $subject);

            #get exam groups whose subject id is selected
            $QueryG = new examinationGroup();
            $record = $QueryG->getSelectedSubjectGroup($session_id, $ct_sec, $subject);

        elseif ($ex_type == 'all'):
            $all_exam_array = array();

            #get exam groups whose subject id is selected
            $QueryG = new examinationGroup();
            $groups = $QueryG->getTermExamSessionSection($session_id, $ct_sec);

            if (!empty($groups)):
                foreach ($groups as $keyy => $vall):
                    $all_exam_array[] = $vall->exam_id;
                endforeach;
            endif;
            $all_exams = implode(',', $all_exam_array);

            if (empty($all_exams)):
                $all_exams = '0';
            endif;

            #Get exam Students result
            $QueryOb = new examinationMarks();
            $record = $QueryOb->getExamStudents($all_exams);

        else:

        endif;
        break;

    case'vehicle':
        /* Get vehicle Pages */
        $QueryObj = new vehicle();
        $record = $QueryObj->listAll($school->id, '1', 'array');

        break;

    case'enquiry':
        /* Get enquiry Pages */
        if ($e_report_type == 'live'):
            $QueryObj = new schoolEnquiry();
            $record = $QueryObj->listAllLive($school->id, 'array');
        elseif ($e_report_type == 'confirm'):
            $QueryObj = new schoolEnquiry();
            $record = $QueryObj->listAllConfirm($school->id, 'array');
        elseif ($e_report_type == 'dead'):
            $QueryObj = new schoolEnquiry();
            $record = $QueryObj->listAllDead($school->id, 'array');
        endif;
        break;

    case'salary':
        #Get Staff Category
        $QuerySalCat = new staffCategory();
        $QuerySalCat->listAll($school->id);


        if (empty($session_id)):
            if (!empty($session_array)):
                $session_id = $session_array['0']->id;
            endif;
        endif;

        #Get Records for salary
        $QueryObj = new staffSalary();
        $record = $QueryObj->SalaryReportMonthWise($school->id, $month, $s_report_type, $cat);

        $first_date_of_month = date('Y-m-d', strtotime('01-' . $month));
        $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($first_date_of_month))));

        $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
        $total_month_days = (date('d', strtotime($last_date_of_month)));
        $working_days = $total_month_days - $total_sunday;

        $M_obj = new schoolHolidays();
        $M_holiday = $M_obj->checkMonthlyHoliday($school->id, date('m', strtotime($first_date_of_month)), date('Y', strtotime($first_date_of_month)));
        break;

    case'download':
        if ($tmp == 'student'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $file_pdf_name = $report_type;
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&report_type=' . $report_type . '&temp=report&file=student_pdf';

            //echo $post_url; exit;
            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'portrait');
            $dompdf->render();
            $dompdf->stream($file_pdf_name . ".pdf");
            exit;

        elseif ($tmp == 'attendance'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $file_pdf_name = $a_type . 'AttendanceReport';
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&month=' . $month . '&a_type=' . $a_type . '&from=' . $from . '&student_id=' . $student_id . '&to=' . $to . '&temp=report&file=attendance_pdf';
            //echo $post_url; exit;
            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'portrait');
            $dompdf->render();
            $dompdf->stream($file_pdf_name . ".pdf");
            exit;
        elseif ($tmp == 'fee'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $file_pdf_name = 'FeeReport';
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&session_id=' . $session_id . '&interval_id=' . $interval_id . '&ct_sec=' . $ct_sec . '&f_type=' . $f_type . '&temp=report&file=fee_pdf';

            //echo $post_url; exit;
            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'portrait');
            $dompdf->render();
            $dompdf->stream($file_pdf_name . ".pdf");
            exit;
        elseif ($tmp == 'daily'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $file_pdf_name = 'DailyCollection';
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&session_id=' . $session_id . '&interval_id=' . $interval_id . '&ct_sec=' . $ct_sec . '&f_type=' . $f_type . '&to=' . $to . '&temp=report&file=daily_pdf';

            //echo $post_url; exit;
            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'portrait');
            $dompdf->render();
            $dompdf->stream($file_pdf_name . ".pdf");
            exit;
        elseif ($tmp == 'exam'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $file_pdf_name = 'ExamReport';
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&exam=' . $exam . '&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&ex_type=' . $ex_type . '&subject=' . $subject . '&temp=report&file=exam_pdf';

            //echo $post_url; exit;
            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'portrait');
            $dompdf->render();
            $dompdf->stream($file_pdf_name . ".pdf");
            exit;
        elseif ($tmp == 'salary'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $file_pdf_name = "salary_report";
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&month=' . $month . '&s_report_type=' . $s_report_type . '&cat=' . $cat . '&temp=report&file=salary_pdf';

            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'portrait');
            $dompdf->render();
            $dompdf->stream($file_pdf_name . ".pdf");
            exit;
        elseif ($tmp == 'vehicle'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $file_pdf_name = $v_report_type;
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&v_report_type=' . $v_report_type . '&temp=report&file=vehicle_pdf';

            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'portrait');
            $dompdf->render();
            $dompdf->stream($file_pdf_name . ".pdf");
            exit;
        elseif ($tmp == 'enquiry'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $file_pdf_name = $e_report_type . '_enquiries';
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&e_report_type=' . $e_report_type . '&temp=report&file=enquiry_pdf';

            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'portrait');
            $dompdf->render();
            $dompdf->stream($file_pdf_name . ".pdf");
            exit;
        endif;
        break;

    case'excel':
        if ($tmp == 'student'):
            #get Report 
            $StudentObj = new studentSession();
            $record = $StudentObj->download_report_various_student_report($school->id, $session_id, $ct_sec, $report_type, $report_array);
        elseif ($tmp == 'attendance'):
            #Get Report
            $QueryOb = new studentSession();
            $record = $QueryOb->sectionSessionStudentsDownload($school->id, $session_id, $ct_sec, $month);
        elseif ($tmp == 'fee'):
            #Get Report
            $QueryOb = new studentSession();
            $record = $QueryOb->sectionSessionStudentsFeeDownload($school->id, $session_id, $ct_sec, $interval_id);
            exit;
        elseif ($tmp == 'exam'):
            #Get Report
            $QueryOb = new examination();
            $record = $QueryOb->examinationResultReport($exam, $session_id, $ct_sec);
        elseif ($tmp == 'salary'):
            #Get Report
            $QueryObj = new staffSalary();
            $record = $QueryObj->SalaryReportMonthWiseDownload($school->id, $month, $s_report_type, $cat, $salary_report_array);
        elseif ($tmp == 'vehicle'):
            #Get Report
            $QueryObj = new vehicle();
            $record = $QueryObj->listAllReport($school->id, '1', $v_report_type, $vehicle_report_array);
        endif;
        break;

    default:break;
endswitch;
?>