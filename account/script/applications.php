<?php

/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/applicationClass.php');

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['type']) ? $type = $_GET['type'] : $type = 'confirm';

#handle actions here.
$modName = 'applications';

switch ($action):
    case'staff':
        $object = new application;
        $listStaff = $object->listStaffApplications();

        $staff_application_count = application:: countStaffApplication('staff');
        $student_application_count = application:: countStaffApplication('student');
        break;

    case 'student':
        $object = new application;
        $listStudents = $object->listStudentApplications();
        break;

    case'stu_delete':
        $query = new application();
        $query->deleteStudentApplicationById($id);
        $admin_user->set_pass_msg('Deleted Successfully');
        Redirect(make_admin_url('applications', 'student', 'student'));
        break;
    case'staff_delete':
        $query = new application();
        $query->deleteStaffApplicationById($id);
        $admin_user->set_pass_msg('Deleted Successfully');
        Redirect(make_admin_url('applications', 'staff', 'staff'));
        break;
    case'stu_delete':
        $query = new application();
        $query->deleteStudentApplicationById($id);
        $admin_user->set_pass_msg('Deleted Successfully');
        Redirect(make_admin_url('applications', 'student', 'student'));
        break;
    default:break;
endswitch;
?>
