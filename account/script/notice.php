<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/noticeBoardClass.php');


$modName='notice';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';


switch ($action):
	case'list':
		/* Get School Pages*/
		 $QueryObj=new noticeBoard();
         $QueryObj->getSchoolNoticeBoardList($school->id);
         break;
	case'insert':
		/* Insert School Pages*/
			if(isset($_POST['submit'])):
						$QueryObj = new noticeBoard();
						$QueryObj->saveData($_POST);
						$admin_user->set_pass_msg(MSG_NOTICE_ADDED);
						Redirect(make_admin_url('notice', 'list', 'list'));
					elseif(isset($_POST['cancel'])):
						$admin_user->set_error();  
						$admin_user->set_pass_msg(OPERATION_CANCEL);
						Redirect(make_admin_url('notice', 'list', 'list'));   
			endif;
		break;	
	case'update':
			/* Get School Pages by Id*/
						$QueryPage = new noticeBoard();
						$object=$QueryPage->getNoticeBoard($id);			
			
			/* Update School Pages*/
			if(isset($_POST['submit'])):
						$QueryObj = new noticeBoard();
						$QueryObj->saveData($_POST);
						$admin_user->set_pass_msg(MSG_NOTICE_UPDATE);
						Redirect(make_admin_url('notice', 'list', 'list'));
                  elseif(isset($_POST['cancel'])):
						$admin_user->set_error();  
						$admin_user->set_pass_msg(OPERATION_CANCEL);
						Redirect(make_admin_url('notice', 'list', 'list'));       
	        endif;   
		break;
        case'delete':
			#delete page 			
					$QueryObj = new noticeBoard();
					$QueryObj->deleteRecord($id);		
					$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
					Redirect(make_admin_url('notice', 'list', 'list')); 			
			break;	
	case'delete_image':
			#delete school logo
			$QueryObj = new noticeBoard();
			$QueryObj->deleteImage($id);
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('notice', 'update', 'update&id='.$id));	
		break;			
	default:break;
endswitch;
?>
