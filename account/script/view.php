<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');

$modName = 'view';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['type']) ? $type = $_GET['type'] : $type = 'student_detail';
isset($_GET['session_id']) ? $session_id = $_GET['session_id'] : $session_id = '';
isset($_GET['ct_section']) ? $ct_section = $_GET['ct_section'] : $ct_section = '';
isset($_GET['month']) ? $month = $_GET['month'] : $month = date('m');
isset($_GET['doc_id']) ? $doc_id = $_GET['doc_id'] : $doc_id = '';

#get Student Info 
$QueryInfo = new student();
$s_info = $QueryInfo->getStudent($id);

#get students current session
$CurrObj = new studentSession();
$current_session = $CurrObj->getStudentCurrentSessionId($school->id, $id);
if (empty($session_id)):
    $session_id = $current_session;
endif;

#get student permanent address
$QueryP = new studentAddress();
$s_p_ad = $QueryP->getStudentAddressByType($id, 'permanent');

#get student correspondence address
$QueryC = new studentAddress();
$s_c_ad = $QueryC->getStudentAddressByType($id, 'correspondence');

#get student correspondence address
$QueryF = new studentFamily();
$s_f = $QueryF->getStudentFamilyDeatils($id);

#get student Sibling Info
$QueryS = new studentSibling();
$QueryS->getStudentSibling($id);

#get student Previous School Info
$P_school = new studentPreviousSchool();
$P_school->getStudentPreviousSchool($id);

#get student Document Info
$S_doc = new studentDocument();
$S_doc->getStudentDocument($id);

#Get Student previous session
$QuerySList = new studentSession();
$Session_list = $QuerySList->getStudentSessionList($id);

#Get Session info
$sessionIQuery = new session();
$session_info = $sessionIQuery->getRecord($session_id);

#Get Student Section Name
$sectionQuery = new studentSession();
$section_name = $sectionQuery->checkStudentSection($session_id, $id);

#Get Student previous Fee List
$QueryFList = new studentFee();
$Fee_list = $QueryFList->listFeeSingleSutdent($session_id, $id);

#Get Student print doc List
$QueryDocList = new SchoolDocumentPrintHistory();
$Print_list = $QueryDocList->listPrintingdocs($school->id, $id, $session_id);

/* Get school document template for student id card */
$QueryDocs = new SchoolDocumentTemplate();
$student_icard = $QueryDocs->get_school_single_document($school->id, 1);

/* Get school document template for student id character certificate */
$QueryDocsChr = new SchoolDocumentTemplate();
$certificate = $QueryDocsChr->get_school_single_document($school->id, 6);

/* Get the student exam list */
$QueryExam = new examination();
$ExamList = $QueryExam->studentAllExam($id, $session_id);

/* Get exams Groups */
$QueryObj = new examinationGroup();
$result = $QueryObj->listAll($school->id, $session_id);

$month_array = array('1' => 'Jan', '2' => 'Feb', '3' => 'Mar', '4' => 'Apr', '5' => 'May', '6' => 'Jun', '7' => 'Jul', '8' => 'Aug', '9' => 'Sep', '10' => 'Oct', '11' => 'Nov', '12' => 'Dec');
#handle actions here.
switch ($action):
    case 'list':
        break;

    case 'update':
        #get Student detail	
        $QueryStu = new studentSession();
        $student = $QueryStu->SingleStudentsWithSession($id, $session_id, $ct_section);

        $session = get_object('session', $session_id);


        /* Get Last Update Data */
        $QueryDoc = new SchoolDocumentPrintHistory();
        $last_doc = $QueryDoc->getLastUpdateDoc($school->id, $_GET['session_id'], $_GET['ct_section'], $_GET['doc_id'], $id);

        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $_GET['session_id'], $_GET['ct_section'], $_GET['doc_id'], $id);

        /* Load the last updated doc if is it */
        if (is_object($last_doc)):
            $content = html_entity_decode($last_doc->document_text);
        else:
            #Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('staff', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($doc)):
                $content = $doc->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                endif;
            endif;
        endif;


        break;

    case 'view':
        #get Student detail	
        $QueryStu = new studentSession();
        $student = $QueryStu->SingleStudentsWithSession($id, $session_id, $ct_section);

        $session = get_object('session', $session_id);

        /* Get Last Update Data */
        $QueryDoc = new SchoolDocumentPrintHistory();
        $last_doc = $QueryDoc->getLastUpdateDoc($school->id, $_GET['session_id'], $_GET['ct_section'], $_GET['doc_id'], $id);

        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $_GET['session_id'], $_GET['ct_section'], $_GET['doc_id'], $id);

        /* Load the last updated doc if is it */
        if (is_object($last_doc)):
            $content = html_entity_decode($last_doc->document_text);
        else:
            #Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('staff', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($doc)):
                $content = $doc->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;

                endif;
            endif;
        endif;

        /* Update document text for single student */
        if (isset($_POST['submit'])):
            #get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($id, $_GET['session_id'], $_GET['ct_section']);

            #Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($_GET['doc_id']);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('staff', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($doc)):
                $content = $doc->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;

                endif;
            endif;

            if (is_object($last_doc)):
                $_POST['id'] = $last_doc->id;
            elseif (is_object($last_his)):
                $_POST['id'] = $last_his->id;
            else:
                $_POST['doc_type'] = $content;
                $_POST['document_id'] = $_GET['doc_id'];
                $_POST['school_id'] = $school->id;
                $_POST['session_id'] = $_GET['session_id'];
                $_POST['section'] = $_GET['ct_section'];
            endif;
            $_POST['student_id'] = $id;

            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);

            $admin_user->set_pass_msg(MSG_STUDENT_DOCUMENT_UPDATE);
            Redirect(make_admin_url('view', 'update', 'update&id=' . $id . '&session_id=' . $session_id . '&ct_section=' . $ct_section . '&doc_id=' . $doc_id));
        endif;


        break;
    case'print':
        #Get document
        $history = get_object('school_document_print_history', $id);

        $QueryDoc = new document();
        //$doc=$QueryDoc->getRecord('',$history->document_id); 

        if (!empty($history->document_text)):
            $content = $history->document_text;
        else:
            $content = '';
        endif;

        break;

    case'del_rec':

        $QuerySess = new SchoolDocumentPrintHistory();
        $QuerySess->deleteRecord($_REQUEST['del_id']);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);

        Redirect(make_admin_url('view', 'list', 'list&type=print&id=' . $id . '&session_id=' . $session_id));
        break;

    case'confirm_print':
        #Get document
        $QueryDoc = new document();
        $doc = $QueryDoc->getRecord($doc_id);
        #Add doc entry to history
        $_POST['student_id'] = $id;
        $_POST['doc_type'] = $doc->type;
        $_POST['school_id'] = $school->id;
        $_POST['session_id'] = $_GET['session_id'];
        $_POST['section'] = $_GET['ct_section'];
        $_POST['document_id'] = $_GET['doc_id'];
        $_POST['document_text'] = $doc->format;
        $QuerySess = new SchoolDocumentPrintHistory();
        $QuerySess->saveData($_POST);

        $admin_user->set_pass_msg(MSG_DOCUMENT_GENERATETD);
        Redirect(make_admin_url('view', 'update', 'update&id=' . $id . '&session_id=' . $session_id . '&ct_section=' . $ct_section . '&doc_id=' . $doc_id));
        break;
    default:break;
endswitch;
?>
