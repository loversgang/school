<?php

error_reporting(0);
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsEmailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');

$modName = 'certificate';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_REQUEST['id']) ? $id = $_REQUEST['id'] : $id = '';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';

isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = 'select';
isset($_REQUEST['doc']) ? $doc = $_REQUEST['doc'] : $doc = '';
isset($_REQUEST['staff_id']) ? $staff_id = $_REQUEST['staff_id'] : $staff_id = '';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
isset($_REQUEST['exam_id']) ? $exam_id = $_REQUEST['exam_id'] : $exam_id = '';
isset($_GET['group_id']) ? $group_id = $_GET['group_id'] : $group_id = '';
isset($_GET['indicator_id']) ? $indicator_id = $_GET['indicator_id'] : $indicator_id = '';

$modName = 'certificate';
switch ($action):
    case'list':
        // List All Grades
        $obj = new examGrade;
        $grades = $obj->listAll($school->id);

        // List Indicators
        $obj1 = new SchoolDocumentIndicators;
        $indicators_sem1_optional = $obj1->listIndicators($school->id, 'SEM1', 'optional');
        $obj2 = new SchoolDocumentIndicators;
        $indicators_sem2_optional = $obj2->listIndicators($school->id, 'SEM2', 'optional');
        $obj3 = new SchoolDocumentIndicators;
        $indicators_sem1_cultural = $obj3->listIndicators($school->id, 'SEM1', 'cultural');
        $obj4 = new SchoolDocumentIndicators;
        $indicators_sem2_cultural = $obj4->listIndicators($school->id, 'SEM2', 'cultural');
        $obj5 = new SchoolDocumentIndicators;
        $indicators_sem1_personality = $obj5->listIndicators($school->id, 'SEM1', 'personality');
        $obj6 = new SchoolDocumentIndicators;
        $indicators_sem2_personality = $obj6->listIndicators($school->id, 'SEM2', 'personality');

        $content = '';
        $printed = '0';
        /* Get school document template for staff id card */
        $QueryDocs = new SchoolDocumentTemplate();
        $student_icard = $QueryDocs->get_school_single_document($school->id, 1);

        /* Get school document template for student id character certificate */
        $QueryDocsChr = new SchoolDocumentTemplate();
        $student_charcter = $QueryDocsChr->get_school_single_document($school->id, 6);

        /* Get school document template for student id card */
        $QueryDocs = new SchoolDocumentTemplate();
        $student_dmc = $QueryDocs->get_school_single_document($school->id, 3);

        /* Get school document template for student appreciation card */
        $QueryDocs1 = new SchoolDocumentTemplate();
        $student_appri = $QueryDocs1->get_school_single_document($school->id, 9);

        /* Get school document template for student sports card */
        $QueryDocs10 = new SchoolDocumentTemplate();
        $student_sports = $QueryDocs10->get_school_single_document($school->id, 10);

        /* Get school document template for student participation card */
        $QueryDocs11 = new SchoolDocumentTemplate();
        $student_parti = $QueryDocs11->get_school_single_document($school->id, 11);

        /* Get school document template for student School Leaving card */
        $QueryDocs12 = new SchoolDocumentTemplate();
        $student_leaving = $QueryDocs12->get_school_single_document($school->id, 12);

        /* Get school document template for student School Transfer certificate card */
        $QueryDocs13 = new SchoolDocumentTemplate();
        $student_transfer = $QueryDocs13->get_school_single_document($school->id, 13);

        /* Get school document template for student Test Report card */
        $QueryDocs14 = new SchoolDocumentTemplate();
        $student_test_report_card = $QueryDocs14->get_school_single_document($school->id, 14);

        /* Get school document template for student Yearly DMC */
        $QueryDocs15 = new SchoolDocumentTemplate();
        $student_yearly_dmc = $QueryDocs15->get_school_single_document($school->id, 16);

        /* Make all certificates array */
        $certificates = array();
        if (!empty($student_icard)):
            $icard = get_object('document_master', $student_icard->id);
            $certificates['icard'] = $icard->name;
        endif;
        if (!empty($student_charcter)):
            $character = get_object('document_master', $student_charcter->id);
            $certificates['character'] = $character->name;
        endif;
        if (!empty($student_dmc)):
            $dmc = get_object('document_master', $student_dmc->id);
            $certificates['dmc'] = $dmc->name;
        endif;
        if (!empty($student_appri)):
            $appri = get_object('document_master', $student_appri->id);
            $certificates['appri'] = $appri->name;
        endif;
        if (!empty($student_sports)):
            $sports = get_object('document_master', $student_sports->id);
            $certificates['sports'] = $sports->name;
        endif;
        if (!empty($student_parti)):
            $participation = get_object('document_master', $student_parti->id);
            $certificates['participation'] = $participation->name;
        endif;
        if (!empty($student_leaving)):
            $leaving = get_object('document_master', $student_leaving->id);
            $certificates['leaving'] = $leaving->name;
        endif;
        if (!empty($student_transfer)):
            $transfer = get_object('document_master', $student_transfer->id);
            $certificates['transfer'] = $transfer->name;
        endif;
        if (!empty($student_test_report_card)):
            $test_report_card = get_object('document_master', $student_test_report_card->id);
            $certificates['test_report_card'] = $test_report_card->name;
        endif;
        if (!empty($student_yearly_dmc)):
            $yearly_dmc = get_object('document_master', $student_yearly_dmc->id);
            $certificates['yearly_dmc'] = $yearly_dmc->name;
        endif;

        #get_all current session 
        $QuerySession = new session();
        $All_Session = $QuerySession->listAllCurrent($school->id, '1', 'array');

        /* Set First time session id */
        if (empty($session_id) && !empty($All_Session)):
            $session_id = $All_Session['0']->id;
        endif;

        /* Get section Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);

        #Get Section Students
        $QueryOb = new studentSession();
        $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);

        /* Now Make the text for id card for first staff id */
        if ($doc == 'icard' && is_object($icard) && !empty($student_id)):
            /* Get Last Entry */
            $QueryDocHis = new SchoolDocumentPrintHistory();
            $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $icard->id, $student_id);

            /* Load the last updated doc if is it */
            if (is_object($last_his)):
                $link = make_admin_url('view', 'print', 'print&id=' . $last_his->id);
                $content = '
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							<strong>Note:-</strong>
							You have already print this document on ' . date('d M, Y') . '. <a href="' . $link . '" target="_blank">Click here</a> to reprint this document.
						</div>';
                $printed = '1';
            else:
#get Student detail	
                $QueryStu = new studentSession();
                $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);
#get session detail	
                $session = get_object('session', $session_id);

                $im_obj = new imageManipulation();
                if ($student->photo):
                    $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                else:
                    $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
                endif;
                if ($school->logo):
                    $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                else:
                    $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
                endif;

                $replace = array('SCHOOL' => $school->school_name,
                    'NAME' => $student->first_name . " " . $student->last_name,
                    'FATHER_NAME' => $student->father_name,
                    'CLASS' => $student->session_name,
                    'SECTION' => $student->section,
                    'ROLL_NO' => $student->roll_no,
                    'GENDER' => $student->sex,
                    'DATE_OF_BIRTH' => $student->date_of_birth,
                    'SESSION' => $session->start_date . ' to ' . $session->end_date,
                    'PHOTO' => $student_image,
                    'LOGO' => $school_logo
                );

                $content = '';
                if (is_object($icard)):
                    $content = $icard->format;
                    if (count($replace)):
                        foreach ($replace as $k => $v):
                            $literal = '{' . trim(strtoupper($k)) . '}';
                            $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                        endforeach;

                    endif;
                endif;
            endif;

        elseif ($doc == 'character' && is_object($character) && !empty($student_id)):
            /* Get Last Entry */
            $QueryDocHis = new SchoolDocumentPrintHistory();
            $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $character->id, $student_id);

            /* Load the last updated doc if is it */
            if (is_object($last_his)):
                $link = make_admin_url('view', 'print', 'print&id=' . $last_his->id);
                $content = '
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							<strong>Note:-</strong>
							You have already print this document on ' . date('d M, Y') . '. <a href="' . $link . '" target="_blank">Click here</a> to reprint this document.
						</div>';
                $printed = '1';
            else:
#get Student detail	
                $QueryStu = new studentSession();
                $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);


#get session detail	
                $session = get_object('session', $session_id);

                $im_obj = new imageManipulation();
                if ($student->photo):
                    $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                else:
                    $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
                endif;
                if ($school->logo):
                    $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                else:
                    $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
                endif;

                $replace = array('SCHOOL_NAME' => $school->school_name,
                    'SCHOOL_ADDRESS' => $school->address1 . ' ' . $school->city . ' ' . $school->district,
                    'NAME' => $student->first_name . " " . $student->last_name,
                    'FATHER_NAME' => $student->father_name,
                    'CLASS' => $student->session_name,
                    'SECTION' => $student->section,
                    'ROLL_NO' => $student->roll_no,
                    'GENDER' => $student->sex,
                    'DATE_OF_BIRTH' => $student->date_of_birth,
                    'SESSION' => $session->start_date . ' to ' . $session->end_date,
                    'PHOTO' => $student_image,
                    'LOGO' => $school_logo
                );

                $content = '';
                if (is_object($character)):
                    $content = $character->format;
                    if (isset($_POST['character_certificate'])):

                        $_POST['LOGO'] = $school_logo;

                        foreach ($_POST as $k => $v):
                            $literal = '{' . trim(strtoupper($k)) . '}';
                            $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                        endforeach;
                    endif;
                endif;
            endif;
        elseif ($doc == 'report' && is_object($dmc) && !empty($session_id)):
            /* Get exams sessions */
            $QueryExmObj = new examinationGroup();
            $exams = $QueryExmObj->getGroupExamSessionSection($session_id, $ct_sec);

            /* Get the first exam group id to get the students */
            if (!empty($exams) && !empty($exam_id)):
                $exam_id = $exams['0']->id;

#get exam Info
                $QueryG = new examinationGroup();
                $group = $QueryG->getRecord($exam_id);

#Get exam Students result
                $QueryOb = new examinationMarks();
                $record = $QueryOb->getExamStudents($group->exam_id);

                /* Get Last Entry */
                $QueryDocHis = new SchoolDocumentPrintHistory();
                $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $dmc->id, $student_id);

                /* Load the last updated doc if is it */
                if (is_object($last_his)):
                    $link = make_admin_url('view', 'print', 'print&id=' . $last_his->id);
                    $content = '
									<div class="alert alert-error">
										<button class="close" data-dismiss="alert"></button>
										<strong>Note:-</strong>
										You have already print this document on ' . date('d M, Y') . '. <a href="' . $link . '" target="_blank">Click here</a> to reprint this document.
									</div>';
                    $printed = '1';
                else:
#get Student detail	
                    $QueryStu = new studentSession();
                    $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
                    $session = get_object('session', $session_id);

#get Student result
                    $QueryRes = new examination();
                    $Result = $QueryRes->single_student_all_examination_result($group->exam_id, $student_id);

                    $result_data = '';
                    $minimum = '0';
                    $obtain = '0';
                    $maximum = '0';
                    $sr = '1';
                    if (!empty($Result)):
                        $result_data.="<table style='width: 100%; text-align: center; line-height: 40px;'>
												<tr><th style='text-align:left;width:10%;border-bottom:1px solid;' >Sr. No.</th><th style='text-align: left; width: 30%;border-bottom:1px solid;'>Subject Name</th><th style='width:20%;border-bottom:1px solid;'>Passing Marks</th><th style='width:20%;border-bottom:1px solid;'>Minimum Obtained</th><th style='width:20%;border-bottom:1px solid;'>Maximum Marks</th></tr>
												";
                        foreach ($Result as $r_k => $r_v):
                            $result_data.="<tr><td style='text-align:left;width:10%;' >" . $sr . ".</td><td style='text-align: left; width: 30%;'>" . $r_v->subject . "</td><td style='width:20%;'>" . $r_v->minimum_marks . "</td><td style='width:20%;'>" . $r_v->marks_obtained . "</td><td style='width:20%;'>" . $r_v->maximum_marks . "</td></tr>";

                            $minimum = $minimum + $r_v->minimum_marks;
                            $obtain = $obtain + $r_v->marks_obtained;
                            $maximum = $maximum + $r_v->maximum_marks;
                            $sr++;
                        endforeach;
                        $result_data.="</table>";
                    endif;

#Get Percentage
                    $percentage = number_format(($obtain / $maximum) * 100, 2) . "%";

#Get exam Grades
                    $QueryGrOb = new examGrade();
                    $all_grade = $QueryGrOb->listAll($school->id);

#Get Grade
                    if (!empty($all_grade)): $grade = 'None';
                        foreach ($all_grade as $kg => $kv):
                            if (($percentage >= $kv->minimum) && ($percentage < $kv->maximum)): $grade = $kv->title;
                            endif;
                        endforeach;
                    else:
                        $grade = 'None';
                    endif;

                    $replace = array('SCHOOL' => $school->school_name,
                        'TITLE' => $group->title,
                        'NAME' => $student->first_name . " " . $student->last_name,
                        'CLASS' => $student->session_name,
                        'SECTION' => $student->section,
                        'ROLL_NO' => $student->roll_no,
                        'GENDER' => $student->sex,
                        'MINIMUM_TOTAL' => $minimum,
                        'OBTAIN_TOTAL' => $obtain,
                        'MAXIMUM_TOTAL' => $maximum,
                        'PERCENTAGE' => $percentage,
                        'GRADE' => $grade,
                        'MARKS_RESULT' => $result_data,
                        'SESSION' => date('d, M Y', strtotime($session->start_date)) . ' - ' . date('d M, Y', strtotime($session->end_date))
                    );

                    $content = '';
                    if (is_object($dmc)):
                        $content = $dmc->format;
                        if (count($replace)):
                            foreach ($replace as $k => $v):
                                $literal = '{' . trim(strtoupper($k)) . '}';
                                $content = html_entity_decode(str_replace($literal, $v, $content));
                                $content = str_replace('&nbsp;', '', $content);
                            endforeach;
                        endif;
                    endif;
                endif;

            else:
                $record = array();
                $content = '
								<div class="alert alert-error">
									<button class="close" data-dismiss="alert"></button>
									<strong>Note:-</strong>
									Sorry, There is no selected student.
								</div>';
            endif;

        elseif ($doc == 'appreciation' && is_object($appri) && !empty($student_id)):
            /* Get Last Entry */
            $QueryDocHis = new SchoolDocumentPrintHistory();
            $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $appri->id, $student_id);

            /* Load the last updated doc if is it */
            if (is_object($last_his)):
                $link = make_admin_url('view', 'print', 'print&id=' . $last_his->id);
                $content = '
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							<strong>Note:-</strong>
							You have already print this document on ' . date('d M, Y') . '. <a href="' . $link . '" target="_blank">Click here</a> to reprint this document.
						</div>';
                $printed = '1';
            else:
#get Student detail	
                $QueryStu = new studentSession();
                $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
                $session = get_object('session', $session_id);

                $im_obj = new imageManipulation();
                if ($student->photo):
                    $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                else:
                    $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
                endif;
                if ($school->logo):
                    $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                else:
                    $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
                endif;

                $replace = array('SCHOOL' => $school->school_name,
                    'NAME' => $student->first_name . " " . $student->last_name,
                    'FATHER_NAME' => $student->father_name,
                    'CLASS' => $student->session_name,
                    'SECTION' => $student->section,
                    'ROLL_NO' => $student->roll_no,
                    'GENDER' => $student->sex,
                    'DATE_OF_BIRTH' => $student->date_of_birth,
                    'SESSION' => $session->start_date . ' to ' . $session->end_date,
                    'PHOTO' => $student_image,
                    'DATE' => date('d M,Y'),
                    'LOGO' => $school_logo
                );

                $content = '';
                if (is_object($appri)):
                    $content = $appri->format;
                    if (count($replace)):
                        foreach ($replace as $k => $v):
                            $literal = '{' . trim(strtoupper($k)) . '}';
                            $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                        endforeach;
                    endif;
                endif;
            endif;


        elseif ($doc == 'participation' && is_object($participation) && !empty($student_id)):
            /* Get Last Entry */
            $QueryDocHis = new SchoolDocumentPrintHistory();
            $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $participation->id, $student_id);

            /* Load the last updated doc if is it */
            if (is_object($last_his)):
                $link = make_admin_url('view', 'print', 'print&id=' . $last_his->id);
                $content = '
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							<strong>Note:-</strong>
							You have already print this document on ' . date('d M, Y') . '. <a href="' . $link . '" target="_blank">Click here</a> to reprint this document.
						</div>';
                $printed = '1';
            else:
#get Student detail	
                $QueryStu = new studentSession();
                $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
                $session = get_object('session', $session_id);

                $im_obj = new imageManipulation();
                if ($student->photo):
                    $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                else:
                    $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
                endif;
                if ($school->logo):
                    $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                else:
                    $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
                endif;

                $replace = array('SCHOOL' => $school->school_name,
                    'NAME' => $student->first_name . " " . $student->last_name,
                    'FATHER_NAME' => $student->father_name,
                    'CLASS' => $student->session_name,
                    'SECTION' => $student->section,
                    'ROLL_NO' => $student->roll_no,
                    'GENDER' => $student->sex,
                    'DATE_OF_BIRTH' => $student->date_of_birth,
                    'SESSION' => $session->start_date . ' to ' . $session->end_date,
                    'PHOTO' => $student_image,
                    'DATE' => date('d M,Y'),
                    'LOGO' => $school_logo
                );

                $content = '';
                if (is_object($participation)):
                    $content = $participation->format;
                    if (count($replace)):
                        foreach ($replace as $k => $v):
                            $literal = '{' . trim(strtoupper($k)) . '}';
                            $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                        endforeach;
                    endif;
                endif;
            endif;


        elseif ($doc == 'leaving' && is_object($leaving) && !empty($student_id)):
            /* Get Last Entry */
            $QueryDocHis = new SchoolDocumentPrintHistory();
            $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $leaving->id, $student_id);

            /* Load the last updated doc if is it */
            if (is_object($last_his)):
                $link = make_admin_url('view', 'print', 'print&id=' . $last_his->id);
                $content = '
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							<strong>Note:-</strong>
							You have already print this document on ' . date('d M, Y') . '. <a href="' . $link . '" target="_blank">Click here</a> to reprint this document.
						</div>';
                $printed = '1';
            else:
#get Student detail	
                $QueryStu = new studentSession();
                $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
                $session = get_object('session', $session_id);

                $im_obj = new imageManipulation();
                if ($student->photo):
                    $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                else:
                    $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
                endif;
                if ($school->logo):
                    $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                else:
                    $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
                endif;

                $replace = array('SCHOOL' => $school->school_name,
                    'NAME' => $student->first_name . " " . $student->last_name,
                    'FATHER_NAME' => $student->father_name,
                    'CLASS' => $student->session_name,
                    'SECTION' => $student->section,
                    'COURSE' => $student->session_name,
                    'ROLL_NO' => $student->roll_no,
                    'GENDER' => $student->sex,
                    'DATE_OF_BIRTH' => $student->date_of_birth,
                    'SESSION' => $session->start_date . ' to ' . $session->end_date,
                    'PHOTO' => $student_image,
                    'DATE' => date('d M,Y'),
                    'LOGO' => $school_logo
                );

                $content = '';
                if (is_object($leaving)):
                    $content = $leaving->format;
                    if (count($replace)):
                        foreach ($replace as $k => $v):
                            $literal = '{' . trim(strtoupper($k)) . '}';
                            $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                        endforeach;
                    endif;
                endif;
            endif;




        elseif ($doc == 'transfer' && is_object($transfer) && !empty($student_id)):
            /* Get Last Entry */
            $QueryDocHis = new SchoolDocumentPrintHistory();
            $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $transfer->id, $student_id);
//'<pre>'; print_r($last_his); exit;
            /* Load the last updated doc if is it */
            if (is_object($last_his)):
                $link = make_admin_url('view', 'print', 'print&id=' . $last_his->id);
                $content = '
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							<strong>Note:-</strong>
							You have already print this document on ' . date('d M, Y') . '. <a href="' . $link . '" target="_blank">Click here</a> to reprint this document.
						</div>';
                $printed = '1';
            else:
#get Student detail	
                $QueryStu = new studentSession();
                $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
                $session = get_object('session', $session_id);

                $im_obj = new imageManipulation();
                if ($student->photo):
                    $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                else:
                    $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
                endif;
                if ($school->logo):
                    $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                else:
                    $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
                endif;

                $sessioniNFO = get_object('session', $session_id);

                $QueryStComSub = new subject();
                $cmpSub = $QueryStComSub->GetSessionSubjectsInArray($sessioniNFO->compulsory_subjects);

                $QuerySt = new studentSessionInterval();
                $bal = $QuerySt->getOnlyPreviousSessionBalance($session_id, $student_id);
                if (isset($bal['sum']) && $bal['sum'] > 0):
                    $Balance = 'Not Fully Paid';
                else:
                    $Balance = 'Fully Paid';
                endif;

                $QuerySt = new attendance();
                $att = $QuerySt->checkSessionStudentAttendace($session_id, $ct_sec, $student_id);
                if (isset($att['P'])):
                    $present = $att['P'];
                else:
                    $present = '';
                endif;
                $total_days = array_sum($att);
//echo '<pre>'; print_r($att);  echo '<pre>'; print_r($student); exit;





                $content = '';
                if (is_object($transfer)):
                    $content = $transfer->format;
                    if (isset($_POST['transfer_certificate'])):

                        $_POST['PHOTO'] = $student_image;
                        $_POST['LOGO'] = $school_logo;


                        foreach ($_POST as $k => $v):
                            if (empty($v)):
                                $v = '&nbsp;';
                            endif;
                            $literal = '{' . trim(strtoupper($k)) . '}';
                            $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                        endforeach;



                    endif;
                endif;

            endif;


        elseif ($doc == 'sports' && is_object($sports) && !empty($student_id)):
            /* Get Last Entry */
            $QueryDocHis = new SchoolDocumentPrintHistory();
            $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $sports->id, $student_id);

            /* Load the last updated doc if is it */
            if (is_object($last_his)):
                $link = make_admin_url('view', 'print', 'print&id=' . $last_his->id);
                $content = '
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							<strong>Note:-</strong>
							You have already print this document on ' . date('d M, Y') . '. <a href="' . $link . '" target="_blank">Click here</a> to reprint this document.
						</div>';
                $printed = '1';
            else:
                #get Student detail	
                $QueryStu = new studentSession();
                $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

                #get session detail	
                $session = get_object('session', $session_id);

                $im_obj = new imageManipulation();
                if ($student->photo):
                    $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                else:
                    $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
                endif;
                if ($school->logo):
                    $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                else:
                    $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
                endif;

                $replace = array('SCHOOL' => $school->school_name,
                    'NAME' => $student->first_name . " " . $student->last_name,
                    'FATHER_NAME' => $student->father_name,
                    'CLASS' => $student->session_name,
                    'SECTION' => $student->section,
                    'ROLL_NO' => $student->roll_no,
                    'GENDER' => $student->sex,
                    'DATE_OF_BIRTH' => $student->date_of_birth,
                    'SESSION' => $session->start_date . ' to ' . $session->end_date,
                    'PHOTO' => $student_image,
                    'DATE' => date('d M,Y'),
                    'LOGO' => $school_logo
                );

                $content = '';
                if (is_object($sports)):
                    $content = $sports->format;
                    if (count($replace)):
                        foreach ($replace as $k => $v):
                            $literal = '{' . trim(strtoupper($k)) . '}';
                            $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                        endforeach;
                    endif;
                endif;
            endif;
        elseif ($doc == 'test_report_card' && is_object($test_report_card) && !empty($student_id)):
            /* Get Last Entry */
            $QueryDocHis = new SchoolDocumentPrintHistory();
            $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $test_report_card->id, $student_id);
            #get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

            #get session detail	
            $session = get_object('session', $session_id);

            // Get Student Address Details
            $obj = new studentAddress;
            $student_address = $obj->getStudentAddressByType($student_id, 'permanent');

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            // Get Student Attendance
            $student_attendance = attendance::getTotalAttendanceOfStudent($student_id, $session_id, $ct_sec);

            // Get Total Working Days
            $total_working_days = attendance::getTotalWorkingDays($ct_sec, $session_id, $student_id);

            //Get Total Subjects
            $obj = new examination;
            $subjects = $obj->getTotalSubjects($session_id, $ct_sec);
            if (!$subjects):
                $admin_user->set_error();
                $admin_user->set_pass_msg('Sorry, No Exam found for ' . $student->session_name . ' (' . $ct_sec . ')');
                Redirect(make_admin_url('exam', 'list', 'list'));
            endif;
            $total_subjects = count($subjects);
            $info_table = '<table style="width: 100%;clear:both">'
                    . '<tr>'
                    . '<td colspan="2"><span style="margin-left: 20px"><font size="3">Student Profile</font></span></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><span style="margin-left: 20px"><b><font size="3">Name Of Student</font></b></span></td><td><font size="3">' . $student->first_name . " " . $student->last_name . '</font></td>'
                    . '</tr>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><span style="margin-left: 20px"><b><font size="3">House</font></b></span></td><td><font size="3">' . ucfirst($student_address->city) . '</font></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><span style="margin-left: 20px"><b><font size="3">Admission Number</font></b></span></td><td><font size="3">' . $student->reg_id . '</font></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><span style="margin-left: 20px"><b><font size="3">Date Of Birth</font></b></span></td><td><font size="3">' . $student->date_of_birth . '</font></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><span style="margin-left: 20px"><b><font size="3">Residential Address</font></b></span></td><td><font size="3">' . $student_address->address1 . ', ' . $student_address->address2 . ', (' . $student_address->state . ')' . '</font></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><span style="margin-left: 20px"><b><font size="3">Telephone No.</font></b></span></td><td><font size="3">' . $student->phone . '</font></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><span style="margin-left: 20px"><b><font size="3">Mothers Name</font></b></span></td><td><font size="3">' . $student->mother_name . '</font></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><span style="margin-left: 20px"><b><font size="3">Fathers Name</font></b></span></td><td><font size="3">' . $student->father_name . '</font></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td colspan="2"><span style="margin-left: 20px"><font size="3"><b>Attendance</></font></span></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><span style="margin-left: 20px"><b>Total attendance of the student: ' . $student_attendance . '</b></span></td><td><b>Total Working Days: ' . $total_working_days . '</b></td>'
                    . '</tr>'
                    . '</table>';
            $exam_table = '<table style="width: 100%; border: 1px #000 solid;clear:both">'
                    . '<tr style="border: 1px #000 solid">'
                    . '<th style="width: 40%;text-align: left;border: 1px #000 solid"><span style="margin-left: 5px">SUBJECT</span></th>'
                    . '<th style="text-align: center;margin: 10px;border: 1px #000 solid">FA1<br>(10)</th>'
                    . '<th style="text-align: center;margin: 10px;border: 1px #000 solid">FA2<br/>(10)</th>'
                    . '<th style="text-align: center;margin: 10px;border: 1px #000 solid">SA1<br/>(30)</th>'
                    . '<th style="text-align: center;border: 1px #000 solid">Total(FA+SA1)<br/>(50)</th>'
                    . '</tr>';
            foreach ($subjects as $subject):
                $exam_table .= '<tr>'
                        . '<td style="border: 1px #000 solid"><span style="margin-left: 5px;">' . $subject->name . '</span></td>';

                // FA1 Marks Detail
                $result1 = examination::getMarksObtained($subject->id, 'FA1', $student_id, $session_id, $ct_sec);
                $marks_percent1 = ($result1->marks * 100 / $result1->max_marks);
                $total_result1 = (10 * $marks_percent1 / 100);
                $grade1 = examination::claculateGrade($total_result1, 10, $school->id);
                $exam_table.= '<td style="border: 1px #000 solid;text-align: center">' . $grade1 . '</td>';

                // FA2 Marks Detail
                $result2 = examination::getMarksObtained($subject->id, 'FA2', $student_id, $session_id, $ct_sec);
                $marks_percent2 = ($result2->marks * 100 / $result2->max_marks);
                $total_result2 = (10 * $marks_percent2 / 100);
                $grade2 = examination::claculateGrade($total_result2, 10, $school->id);
                $exam_table.= '<td style="border: 1px #000 solid;text-align: center">' . $grade2 . '</td>';

                // SA1 Marks Detail
                $result3 = examination::getMarksObtained($subject->id, 'SA1', $student_id, $session_id, $ct_sec);
                $marks_percent3 = ($result3->marks * 100 / $result3->max_marks);
                $total_result3 = (30 * $marks_percent3 / 100);
                $grade3 = examination::claculateGrade($total_result3, 30, $school->id);
                $exam_table.= '<td style="border: 1px #000 solid;text-align: center">' . $grade3 . '</td>';

                // SEM1 Marks Detail
                $sem1_marks = round($total_result1 + $total_result2 + $total_result3);
                $f_grade = examination::claculateGrade($sem1_marks, 50, $school->id);
                $exam_table.= '<td style="border: 1px #000 solid;text-align: center">' . $f_grade . '</td>';

                $exam_table.= '</tr>';
            endforeach;
            $exam_table .= '</table>';
            $replace = array(
                'SCHOOL_NAME' => $school->school_name,
                'SCHOOL_ADDRESS' => $school->address1 . ' ' . $school->city . ' ' . $school->district,
                'SCHOOL_PHONE' => $school->school_phone,
                'SCHOOL_EMAIL' => $school->email_address,
                'SCHOOL_WEB' => $school->website_url,
                'SCHOOL_AFFL' => $school->affiliation_code,
                'LOGO' => $school_logo,
                'STUDENT_CLASS' => $student->session_name,
                'STUDENT_INFO' => $info_table,
                'EXAM_TABLE' => $exam_table,
                'DMC_DATA' => $dmc_data,
                'RESULT_DATA' => $result_data
            );
            $content = '';
            if (is_object($test_report_card)):
                $content = $test_report_card->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                endif;
            endif;
        elseif ($doc == 'yearly_dmc' && is_object($yearly_dmc) && !empty($student_id)):
            // Get Session Subjects
            $obj = new session;
            $subjects_data = $obj->getSessionSubjects($session_id);
            /* Get Last Entry */
            $QueryDocHis = new SchoolDocumentPrintHistory();
            $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $yearly_dmc->id, $student_id);
            #get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

            #get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            // Get Student Attendance
            $student_attendance = attendance::getTotalAttendanceOfStudent($student_id, $session_id, $ct_sec);

            // Get Total Working Days
            $total_working_days = attendance::getTotalWorkingDays($ct_sec, $session_id, $student_id);

            //Get Total Subjects
            $obj = new examination;
            $subjects = $obj->getTotalSubjects($session_id, $ct_sec);
            if (!$subjects):
                $admin_user->set_error();
                $admin_user->set_pass_msg('Sorry, No Exam found for ' . $student->session_name . ' (' . $ct_sec . ')');
                Redirect(make_admin_url('exam', 'list', 'list'));
            endif;
            $total_subjects = count($subjects);
            $info_table = '<table style="border: 1px solid #000; width: 100%;clear:both">'
                    . '<tr>'
                    . '<td colspan="3"><font size="3"><span style="margin-left: 5px"><b>Student Name:</b> ' . ucfirst($student->first_name . ' ' . $student->last_name) . '</span></font></td>'
                    . '</tr>'
                    . '<tr style="border-top: 1px solid #000">'
                    . '<td><font size="3"><span style="margin-left: 5px"><b>Standard:</b> ' . $student->session_name . '</span></font></td>'
                    . '<td><font size="3"><b>GR No.:</b> ' . $student->reg_id . '<font></td>'
                    . '<td><font size="3"><b>Roll No.:</b> ' . $student->roll_no . '</font></td>'
                    . '</tr>'
                    . '</table>';
            $exam_table = '<table style="width: 100%; border: 1px #000 solid;clear:both">'
                    . '<thead style="border-bottom: 1px #000 solid">'
                    . '<tr>'
                    . '<th style="text-align: left"><span style="margin-left: 5px">Subject</span></th>'
                    . '<th style="text-align: center;margin: 10px">FA1<br>(10)</th>'
                    . '<th style="text-align: center;margin: 10px">FA2<br/>(10)</th>'
                    . '<th style="text-align: center;margin: 10px">SA1<br/>(30)</th>'
                    . '<th style="text-align: center">SEM1<br/>(50)</th>'
                    . '<th style="text-align: center">FA3<br/>(10)</th>'
                    . '<th style="text-align: center">FA4<br/>(10)</th>'
                    . '<th style="text-align: center">SA2<br/>(30)</th>'
                    . '<th style="text-align: center">SEM2<br/>(50)</th>'
                    . '<th style="text-align: center">Total<br/>(SEM1+SEM2)</th>'
                    . '<th style="text-align: center">Grade</th>'
                    . '</tr>'
                    . '</thead>'
                    . '<tbody style="border-bottom: 1px #000 solid">';
            foreach ($subjects as $subject):
                $exam_table .= '<tr>'
                        . '<td><span style="margin-left: 5px">' . $subject->name . '</span></td>';

                // FA1 Marks Detail
                $result1 = examination::getMarksObtained($subject->id, 'FA1', $student_id, $session_id, $ct_sec);
                $marks_percent1 = ($result1->marks * 100 / $result1->max_marks);
                $total_result1 = (10 * $marks_percent1 / 100);
                $exam_table.= '<td style="text-align: center">' . (float) number_format($total_result1, 1) . '</td>';

                // FA2 Marks Detail
                $result2 = examination::getMarksObtained($subject->id, 'FA2', $student_id, $session_id, $ct_sec);
                $marks_percent2 = ($result2->marks * 100 / $result2->max_marks);
                $total_result2 = (10 * $marks_percent2 / 100);
                $exam_table.= '<td style="text-align: center">' . (float) number_format($total_result2, 1) . '</td>';

                // SA1 Marks Detail
                $result3 = examination::getMarksObtained($subject->id, 'SA1', $student_id, $session_id, $ct_sec);
                $marks_percent3 = ($result3->marks * 100 / $result3->max_marks);
                $total_result3 = (30 * $marks_percent3 / 100);
                $exam_table.= '<td style="text-align: center">' . (float) number_format($total_result3, 1) . '</td>';

                // SEM1 Marks Detail
                $sem1_marks = round($total_result1 + $total_result2 + $total_result3);
                $exam_table.= '<td style="text-align: center">' . $sem1_marks . '</td>';

                // FA3 Marks Detail
                $result4 = examination::getMarksObtained($subject->id, 'FA3', $student_id, $session_id, $ct_sec);
                $marks_percent4 = ($result4->marks * 100 / $result4->max_marks);
                $total_result4 = (10 * $marks_percent4 / 100);
                $exam_table.= '<td style="text-align: center">' . (float) number_format($total_result4, 1) . '</td>';

                // FA4 Marks Detail
                $result5 = examination::getMarksObtained($subject->id, 'FA4', $student_id, $session_id, $ct_sec);
                $marks_percent5 = ($result5->marks * 100 / $result5->max_marks);
                $total_result5 = (10 * $marks_percent5 / 100);
                $exam_table.= '<td style="text-align: center">' . (float) number_format($total_result5, 1) . '</td>';

                // SA2 Marks Detail
                $result6 = examination::getMarksObtained($subject->id, 'SA2', $student_id, $session_id, $ct_sec);
                $marks_percent6 = ($result6->marks * 100 / $result6->max_marks);
                $total_result6 = (30 * $marks_percent6 / 100);
                $exam_table.= '<td style="text-align: center">' . (float) number_format($total_result6, 1) . '</td>';

                // SEM2 Marks Detail
                $sem2_marks = round($total_result4 + $total_result5 + $total_result6);
                $exam_table.= '<td style="text-align: center">' . $sem2_marks . '</td>';

                //  SEM1+SEM2 Marks Detail
                $total_marks = $sem1_marks + $sem2_marks;
                $exam_table.= '<td style="text-align: center">' . $total_marks . '</td>';

                // Exam Grades
                $exam_table.= '<td style="text-align: center">' . examination::claculateGrade($total_marks, 100, $school->id) . '</td>';

                $exam_table.= '</tr>';
            endforeach;
            $exam_table .= '</tbody>'
                    . '<tfoot>'
                    . '<td style="text-align: left"><span style="margin-left: 5px"><b>Total</b></span></td>';
            // FA1 Total Marks
            $total_fa1 = examination::calculateTotal('FA1', $student_id, $session_id, $ct_sec, 10);
            $exam_table.= '<td style="text-align: center"><b>' . round($total_fa1) . '</b></td>';

            // FA2 Total Marks
            $total_fa2 = examination::calculateTotal('FA2', $student_id, $session_id, $ct_sec, 10);
            $exam_table.= '<td style="text-align: center"><b>' . round($total_fa2) . '</b></td>';

            // SA1 Total Marks
            $total_sa1 = examination::calculateTotal('SA1', $student_id, $session_id, $ct_sec, 30);
            $exam_table.= '<td style="text-align: center"><b>' . round($total_sa1) . '</b></td>';

            // SEM1 Total Marks
            $total_sem1 = round($total_fa1 + $total_fa2 + $total_sa1);
            $exam_table.= '<td style="text-align: center"><b>' . $total_sem1 . '</b></td>';

            // FA3 Total Marks
            $total_fa3 = examination::calculateTotal('FA3', $student_id, $session_id, $ct_sec, 10);
            $exam_table.= '<td style="text-align: center"><b>' . round($total_fa3) . '</b></td>';

            // FA4 Total Marks
            $total_fa4 = examination::calculateTotal('FA4', $student_id, $session_id, $ct_sec, 10);
            $exam_table.= '<td style="text-align: center"><b>' . round($total_fa4) . '</b></td>';

            // SA2 Total Marks
            $total_sa2 = examination::calculateTotal('SA2', $student_id, $session_id, $ct_sec, 30);
            $exam_table.= '<td style="text-align: center"><b>' . round($total_sa2) . '</b></td>';

            // SEM2 Total Marks
            $total_sem2 = round($total_fa3 + $total_fa4 + $total_sa2);
            $exam_table.= '<td style="text-align: center"><b>' . $total_sem2 . '</b></td>';

            // SEM1 + SEM2 Total Marks
            $total_sem1_sem2_marks = ($total_sem1 + $total_sem2);
            $exam_table.= '<td style="text-align: center"><b>' . $total_sem1_sem2_marks . '</b></td>';

            // Final Grade
            $final_grade = examination::claculateGrade($total_sem1_sem2_marks, ($total_subjects * 100), $school->id);
            $exam_table.= '<td style="text-align: center"><b>' . $final_grade . '</b></td>';
            $exam_table.= '</tfoot>'
                    . '</table>';

            // Extra Details of student
            $typeOptional = studentDMC::checkTypeExists($student_id, 'optional');
            $typePersonality = studentDMC::checkTypeExists($student_id, 'personality');
            $typeCultural = studentDMC::checkTypeExists($student_id, 'cultural');
            $dmc_data = '<table style="margin-top: -10px;width: 100%;clear:both">'
                    . '<tbody>';
            if ($typeOptional):
                $obj = new studentDMC;
                $titles = $obj->listTitles($student_id, 'optional');
                $dmc_data .= '<tr><td colspan="5"><center><h4><strong>Section 1(B) Assessment of Optional Subjects</strong></h4></center></td></tr>'
                        . '<tr style="border: 1px solid #000">'
                        . '<td style="border: 1px solid #000; text-align: center;" width="20%"><b>Subject</b></td>'
                        . '<td style="border: 1px solid #000; text-align: center;"><b>Semester I - Indicators</b></td>'
                        . '<td style="border: 1px solid #000; text-align: center;"><b>Grade</b></td>'
                        . '<td style="border: 1px solid #000; text-align: center;"><b>Semester II - Indicators</b></td>'
                        . '<td style="border: 1px solid #000; text-align: center;"><b>Grade</b></td>'
                        . '</tr>';
                foreach ($titles as $title):
                    $dmc_data.='<tr>'
                            . '<td style="border: 1px solid #000" width="20%"><span style="margin-left: 5px">' . $title->title . '</span></td>'
                            . '<td style="border: 1px solid #000"><ol><li>' . studentDMC::DMCData($student_id, 'optional', 1, $title->title, 'description') . '</li></ol></td>'
                            . '<td style="border: 1px solid #000; text-align: center">' . studentDMC::DMCData($student_id, 'optional', 1, $title->title, 'grade') . '</td>'
                            . '<td style="border: 1px solid #000"><ol><li>' . studentDMC::DMCData($student_id, 'optional', 2, $title->title, 'description') . '</li></ol></td>'
                            . '<td style="border: 1px solid #000; text-align: center">' . studentDMC::DMCData($student_id, 'optional', 2, $title->title, 'grade') . '</td>'
                            . '</tr>';
                endforeach;
            endif;
            if ($typeCultural):
                $obj = new studentDMC;
                $titles = $obj->listTitles($student_id, 'cultural');
                $dmc_data .= '<tr><td colspan="5"><center><h4><strong>Section 2(A) Co-Scholastic Activities (Literary/Cultural/Social)</strong></h4></center></td></tr>'
                        . '<tr style="border: 1px solid #000">'
                        . '<td style="border: 1px solid #000; text-align: center;" width="20%"><b>Activity</b></td>'
                        . '<td colspan="2" style="border: 1px solid #000; text-align: center;"><b>Semester I - Indicators</b></td>'
                        . '<td colspan="2" style="border: 1px solid #000; text-align: center;"><b>Semester II - Indicators</b></td>'
                        . '</tr>';
                foreach ($titles as $title):
                    $dmc_data.='<tr>'
                            . '<td style="border: 1px solid #000" width="20%"><span style="margin-left: 5px">' . $title->title . '</span></td>'
                            . '<td colspan="2" style="border: 1px solid #000"><ol><li>' . studentDMC::DMCData($student_id, 'cultural', 1, $title->title, 'description') . '</li></ol></td>'
                            . '<td colspan="2" style="border: 1px solid #000"><ol><li>' . studentDMC::DMCData($student_id, 'cultural', 2, $title->title, 'description') . '</li></ol></td>'
                            . '</tr>';
                endforeach;
            endif;
            if ($typePersonality):
                $obj = new studentDMC;
                $titles = $obj->listTitles($student_id, 'personality');
                $dmc_data .= '<tr><td colspan="5"><center><h4><strong>Section 2(B) Co-Scholastic Evaluation (Personality Development)</strong></h4></center></td></tr>'
                        . '<tr style="border: 1px solid #000">'
                        . '<td style="border: 1px solid #000; text-align: center" width="20%"><b>Activity</b></td>'
                        . '<td colspan="2" style="border: 1px solid #000; text-align: center"><b>Semester I - Indicators</b></td>'
                        . '<td colspan="2" style="border: 1px solid #000; text-align: center"><b>Semester II - Indicators</b></td>'
                        . '</tr>';
                foreach ($titles as $title):
                    $dmc_data.='<tr>'
                            . '<td style="border: 1px solid #000"><span style="margin-left: 5px" width="20%">' . $title->title . '</span></td>'
                            . '<td colspan="2" style="border: 1px solid #000"><ol><li>' . studentDMC::DMCData($student_id, 'personality', 1, $title->title, 'description') . '</li></ol></td>'
                            . '<td colspan="2" style="border: 1px solid #000"><ol><li>' . studentDMC::DMCData($student_id, 'personality', 2, $title->title, 'description') . '</li></ol></td>'
                            . '</tr>';
                endforeach;
            endif;
            $dmc_data .='</tbody>'
                    . '</table>';
            $result_data = '<table style="border: 1px solid #000; width: 100%;clear:both">'
                    . '<tr>'
                    . '<td><font size="3"><span style="margin-left: 5px"><b>Total Marks:</b> ' . $total_sem1_sem2_marks . '/' . ($total_subjects * 100) . '</span></font></td>'
                    . '<td><font size="3"><b>Attendance:</b> ' . $student_attendance . '/' . $total_working_days . '<font></td>'
                    . '<td><font size="3"><b>Final Grade:</b> ' . $final_grade . '</font></td>'
                    . '</tr>'
                    . '<tr style="border-top: 1px solid #000">'
                    . '<td colspan="3"><font size="3"><span style="margin-left: 5px"><b>Percentage:</b> ' . number_format(($total_sem1_sem2_marks * 100) / ($total_subjects * 100), 2) . '%</span></font></td>'
                    . '</tr>'
                    . '</table>';
            $replace = array(
                'SCHOOL_NAME' => $school->school_name,
                'SCHOOL_ADDRESS' => $school->address1 . ' ' . $school->city . ' ' . $school->district,
                'LOGO' => $school_logo,
                'CLASS' => $student->session_name,
                'STUDENT_INFO' => $info_table,
                'EXAM_RESULT' => $exam_table,
                'DMC_DATA' => $dmc_data,
                'RESULT_DATA' => $result_data
            );
            $content = '';
            if (is_object($yearly_dmc)):
                $content = $yearly_dmc->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                endif;
            endif;
        endif;
        break;

    case'insert':
        $content = '';
        /* Get school document template for staff id card */
        $QueryDocs = new SchoolDocumentTemplate();
        $staff_icard = $QueryDocs->get_school_single_document($school->id, 2);

        /* Get school document template for staff appreciation card */
        $QueryDocs1 = new SchoolDocumentTemplate();
        $staff_appri = $QueryDocs1->get_school_single_document($school->id, 8);

        /* Get school document template for Teacher Experience Certificate */
        $QueryDocs15 = new SchoolDocumentTemplate();
        $teacher_experience_certificate = $QueryDocs15->get_school_single_document($school->id, 15);

        /* Make all certificates array */
        $staff_certificates = array();
        if (!empty($staff_icard)):
            $icard = get_object('document_master', $staff_icard->id);
            $staff_certificates['icard'] = $icard->name;
        endif;
        if (!empty($staff_appri)):
            $appri = get_object('document_master', $staff_appri->id);
            $staff_certificates['appri'] = $appri->name;
        endif;

        if (!empty($teacher_experience_certificate)):
            $teacher_experience = get_object('document_master', $teacher_experience_certificate->id);
            $staff_certificates['teacher_experience'] = $teacher_experience->name;
        endif;

        /* Get All School Staff */
        $QueryObj = new staff();
        $record = $QueryObj->getSchoolStaff_detail($school->id);


        /* Now Make the text for id card for first staff id */
        if ($doc == 'icard' && is_object($icard) && !empty($staff_id)):
#Get Staff
            $QueryStaff = new staff();
            $staff = $QueryStaff->getStaff_detail($staff_id);

            $im_obj = new imageManipulation();
            if ($staff->photo):
                $staff_image = "<img src='" . $im_obj->get_image_echo('staff', 'medium', $staff->photo) . "'/>";
            else:
                $staff_image = '<img src = "assets/img/profile/img-1.jpg"/>';
            endif;
            if ($staff->school_logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $staff->school_logo) . "' style='max-width:86px;
            max-height:89px;
            '/>";
            else:
                $school_logo = '<img src = "assets/img/invoice/webgarh.png"/>';
            endif;

            $replace = array('SCHOOL' => $staff->school_name,
                'STAFF_NAME' => $staff->title . " " . $staff->first_name . " " . $staff->last_name,
                'DESIGNATION' => $staff->designation,
                'DEPARTMENT' => $staff->staff_category,
                'STAFF_PHOTO' => $staff_image,
                'LOGO' => $school_logo
            );
            $content = $icard->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = ' {
                ' . trim(strtoupper($k)) . '
            }';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                endforeach;
            endif;
        /* Now Make the text for id card for first staff id */ elseif ($doc == 'appreciation' && is_object($appri) && !empty($staff_id)):
#Get Staff
            $QueryStaff = new staff();
            $staff = $QueryStaff->getStaff_detail($staff_id);
            $im_obj = new imageManipulation();
            if ($staff->photo):
                $staff_image = "<img src='" . $im_obj->get_image_echo('staff', 'medium', $staff->photo) . "'/>";
            else:
                $staff_image = '<img src = "assets/img/profile/img-1.jpg"/>';
            endif;
            if ($staff->school_logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $staff->school_logo) . "' style='max-width:86px;
            max-height:89px;
            '/>";
            else:
                $school_logo = '<img src = "assets/img/invoice/webgarh.png"/>';
            endif;

            $replace = array('SCHOOL' => $staff->school_name,
                'NAME' => $staff->title . " " . $staff->first_name . " " . $staff->last_name,
                'DESIGNATION' => $staff->designation,
                'DEPARTMENT' => $staff->staff_category,
                'STAFF_PHOTO' => $staff_image,
                'DATE' => date('d M, Y'),
                'LOGO' => $school_logo
            );
            $content = $appri->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = ' {
                ' . trim(strtoupper($k)) . '
            }';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                endforeach;
            endif;
        /* Now Make the text for id card for first staff id */ elseif ($doc == 'teacher_experience' && is_object($teacher_experience) && !empty($staff_id)):
#Get Staff
            $QueryStaff = new staff();
            $staff = $QueryStaff->getStaff_detail($staff_id);

// Get School Details
            $obj = new school;
            $school = $obj->getSchool($staff->school_id);

            $im_obj = new imageManipulation();
            if ($staff->photo):
                $staff_image = "<img src='" . $im_obj->get_image_echo('staff', 'medium', $staff->photo) . "'/>";
            else:
                $staff_image = '<img src = "assets/img/profile/img-1.jpg"/>';
            endif;
            if ($staff->school_logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $staff->school_logo) . "' style='max-width:86px;
            max-height:89px;
            '/>";
            else:
                $school_logo = '<img src = "assets/img/invoice/webgarh.png"/>';
            endif;

            $replace = array(
                'SCHOOL_NAME' => $staff->school_name,
                'SCHOOL_ADDRESS' => $school->address1 . ' ' . $school->city . ' ' . $school->district,
                'SCHOOL_TEMP_ADDRESS' => $school->address1 . ' ' . $school->district,
                'NAME' => $staff->title . " " . $staff->first_name . " " . $staff->last_name,
                'DESIGNATION' => $staff->designation,
                'PHOTO' => $staff_image,
                'DEPARTMENT' => $staff->staff_category,
                'FROM' => $staff->join_date,
                'TO' => $staff->quit_date,
                'SCHOOL_AFFL' => $school->affiliation_code,
                'SCHOOL_NUMBER' => $school->client_id,
                'LOGO' => $school_logo
            );
            $content = $teacher_experience->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = ' {
                ' . trim(strtoupper($k)) . '
            }';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                endforeach;
            endif;
        endif;
        break;

    case 'update':
        /* Get school document template for staff id card */
        $QueryDocs = new SchoolDocumentTemplate();
        $student_icard = $QueryDocs->get_school_single_document($school->id, 1);

        /* Get school document template for student id character certificate */
        $QueryDocsChr = new SchoolDocumentTemplate();
        $student_charcter = $QueryDocsChr->get_school_single_document($school->id, 6);

        /* Get school document template for student id card */
        $QueryDocs = new SchoolDocumentTemplate();
        $student_dmc = $QueryDocs->get_school_single_document($school->id, 3);

        /* Get school document template for student appreciation card */
        $QueryDocs1 = new SchoolDocumentTemplate();
        $student_appri = $QueryDocs1->get_school_single_document($school->id, 9);

        /* Get school document template for student participation card */
        $QueryDocs10 = new SchoolDocumentTemplate();
        $student_sports = $QueryDocs10->get_school_single_document($school->id, 10);

        /* Get school document template for student participation card */
        $QueryDocs11 = new SchoolDocumentTemplate();
        $student_parti = $QueryDocs11->get_school_single_document($school->id, 11);

        /* Get school document template for student School Transfer certificate card */
        $QueryDocs13 = new SchoolDocumentTemplate();
        $student_transfer = $QueryDocs13->get_school_single_document($school->id, 13);

        /* Get school document template for student Test Report card */
        $QueryDocs14 = new SchoolDocumentTemplate();
        $student_test_report_card = $QueryDocs14->get_school_single_document($school->id, 14);

        if ($doc == 'participation'):
            $content = '';
            $printed = '0';

            /* Make all certificates array */
            $certificates = array();
            if (!empty($student_icard)):
                $icard = get_object('document_master', $student_icard->id);
                $certificates['icard'] = $icard->name;
            endif;
            if (!empty($student_charcter)):
                $character = get_object('document_master', $student_charcter->id);
                $certificates['character'] = $character->name;
            endif;
            if (!empty($student_dmc)):
                $dmc = get_object('document_master', $student_dmc->id);
                $certificates['dmc'] = $dmc->name;
            endif;
            if (!empty($student_appri)):
                $appri = get_object('document_master', $student_appri->id);
                $certificates['appri'] = $appri->name;
            endif;
            if (!empty($student_sports)):
                $sports = get_object('document_master', $student_sports->id);
                $certificates['sports'] = $sports->name;
            endif;
            if (!empty($student_parti)):
                $participation = get_object('document_master', $student_parti->id);
                $certificates['participation'] = $participation->name;
            endif;

            if (!empty($student_test_report_card)):
                $test_report_card = get_object('document_master', $student_test_report_card->id);
                $certificates['test_report_card'] = $test_report_card->name;
            endif;

            if ($doc == 'participation' && is_object($participation) && !empty($student_id)):
                /* Get Last Entry */
                $QueryDocHis = new SchoolDocumentPrintHistory();
                $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $participation->id, $student_id);

                /* Load the last updated doc if is it */
                if (is_object($last_his)):
                    $content = html_entity_decode($last_his->document_text);
                    $printed = '1';
                else:
#get Student detail	
                    $QueryStu = new studentSession();
                    $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
                    $session = get_object('session', $session_id);

                    $im_obj = new imageManipulation();
                    if ($student->photo):
                        $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                    else:
                        $student_image = '<img src = "assets/img/profile/img-1.jpg" style = "max-width:165px"/>';
                    endif;
                    if ($school->logo):
                        $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                    else:
                        $school_logo = '<img src = "assets/img/invoice/webgarh.png" />';
                    endif;

                    $replace = array('SCHOOL' => $school->school_name,
                        'NAME' => $student->first_name . " " . $student->last_name,
                        'FATHER_NAME' => $student->father_name,
                        'CLASS' => $student->session_name,
                        'SECTION' => $student->section,
                        'ROLL_NO' => $student->roll_no,
                        'GENDER' => $student->sex,
                        'DATE_OF_BIRTH' => $student->date_of_birth,
                        'SESSION' => $session->start_date . ' to ' . $session->end_date,
                        'PHOTO' => $student_image,
                        'DATE' => date('d M, Y'),
                        'LOGO' => $school_logo
                    );

                    $content = '';
                    if (is_object($participation)):
                        $content = $participation->format;
                        if (count($replace)):
                            foreach ($replace as $k => $v):
                                $literal = ' {
                ' . trim(strtoupper($k)) . '
            }';
                                $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                            endforeach;
                        endif;
                    endif;
                endif;
            endif;
            $doc_id = $participation->id;
        elseif ($doc == 'sports'):
            $content = '';
            $printed = '0';

            /* Make all certificates array */
            $certificates = array();
            if (!empty($student_icard)):
                $icard = get_object('document_master', $student_icard->id);
                $certificates['icard'] = $icard->name;
            endif;
            if (!empty($student_charcter)):
                $character = get_object('document_master', $student_charcter->id);
                $certificates['character'] = $character->name;
            endif;
            if (!empty($student_dmc)):
                $dmc = get_object('document_master', $student_dmc->id);
                $certificates['dmc'] = $dmc->name;
            endif;
            if (!empty($student_appri)):
                $appri = get_object('document_master', $student_appri->id);
                $certificates['appri'] = $appri->name;
            endif;
            if (!empty($student_sports)):
                $sports = get_object('document_master', $student_sports->id);
                $certificates['sports'] = $sports->name;
            endif;
            if (!empty($student_parti)):
                $participation = get_object('document_master', $student_parti->id);
                $certificates['participation'] = $participation->name;
            endif;

            if ($doc == 'sports' && is_object($sports) && !empty($student_id)):
                /* Get Last Entry */
                $QueryDocHis = new SchoolDocumentPrintHistory();
                $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $sports->id, $student_id);

                /* Load the last updated doc if is it */
                if (is_object($last_his)):
                    $content = html_entity_decode($last_his->document_text);
                    $printed = '1';
                else:
#get Student detail	
                    $QueryStu = new studentSession();
                    $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
                    $session = get_object('session', $session_id);

                    $im_obj = new imageManipulation();
                    if ($student->photo):
                        $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                    else:
                        $student_image = '<img src = "assets/img/profile/img-1.jpg" style = "max-width:165px"/>';
                    endif;
                    if ($school->logo):
                        $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                    else:
                        $school_logo = '<img src = "assets/img/invoice/webgarh.png" />';
                    endif;

                    $replace = array('SCHOOL' => $school->school_name,
                        'NAME' => $student->first_name . " " . $student->last_name,
                        'FATHER_NAME' => $student->father_name,
                        'CLASS' => $student->session_name,
                        'SECTION' => $student->section,
                        'ROLL_NO' => $student->roll_no,
                        'GENDER' => $student->sex,
                        'DATE_OF_BIRTH' => $student->date_of_birth,
                        'SESSION' => $session->start_date . ' to ' . $session->end_date,
                        'PHOTO' => $student_image,
                        'DATE' => date('d M, Y'),
                        'LOGO' => $school_logo
                    );

                    $content = '';
                    if (is_object($sports)):
                        $content = $sports->format;
                        if (count($replace)):
                            foreach ($replace as $k => $v):
                                $literal = ' {
                ' . trim(strtoupper($k)) . '
            }';
                                $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                            endforeach;
                        endif;
                    endif;
                endif;
            endif;
            $doc_id = $sports->id;
        elseif ($doc == 'transfer'):
            $content = '';
            $printed = '0';

            /* Make all certificates array */
            $certificates = array();
            if (!empty($student_icard)):
                $icard = get_object('document_master', $student_icard->id);
                $certificates['icard'] = $icard->name;
            endif;
            if (!empty($student_charcter)):
                $character = get_object('document_master', $student_charcter->id);
                $certificates['character'] = $character->name;
            endif;
            if (!empty($student_dmc)):
                $dmc = get_object('document_master', $student_dmc->id);
                $certificates['dmc'] = $dmc->name;
            endif;
            if (!empty($student_appri)):
                $appri = get_object('document_master', $student_appri->id);
                $certificates['appri'] = $appri->name;
            endif;
            if (!empty($student_sports)):
                $sports = get_object('document_master', $student_sports->id);
                $certificates['sports'] = $sports->name;
            endif;
            if (!empty($student_parti)):
                $participation = get_object('document_master', $student_parti->id);
                $certificates['participation'] = $participation->name;
            endif;



            if (!empty($student_transfer)):
                $transfer = get_object('document_master', $student_transfer->id);
                $certificates['transfer'] = $transfer->name;
//echo'<pre>'; print_r($transfer);exit;
            endif;

            if ($doc == 'transfer' && is_object($transfer) && !empty($student_id)):
                /* Get Last Entry */
                $QueryDocHis = new SchoolDocumentPrintHistory();
                $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $transfer->id, $student_id);

                /* Load the last updated doc if is it */
                if (is_object($last_his)):
                    $content = html_entity_decode($last_his->document_text);
                    $printed = '1';
                else:
#get Student detail	
                    $QueryStu = new studentSession();
                    $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
                    $session = get_object('session', $session_id);

                    $im_obj = new imageManipulation();
                    if ($student->photo):
                        $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                    else:
                        $student_image = '<img src = "assets/img/profile/img-1.jpg" style = "max-width:165px"/>';
                    endif;
                    if ($school->logo):
                        $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                    else:
                        $school_logo = '<img src = "assets/img/invoice/webgarh.png" />';
                    endif;

                    $sessioniNFO = get_object('session', $session_id);

                    $QueryStComSub = new subject();
                    $cmpSub = $QueryStComSub->GetSessionSubjectsInArray($sessioniNFO->compulsory_subjects);

                    $QuerySt = new studentSessionInterval();
                    $bal = $QuerySt->getOnlyPreviousSessionBalance($session_id, $student_id);
                    if (isset($bal['sum']) && $bal['sum'] > 0):
                        $Balance = 'Not Fully Paid';
                    else:
                        $Balance = 'Fully Paid';
                    endif;

                    $QuerySt = new attendance();
                    $att = $QuerySt->checkSessionStudentAttendace($session_id, $ct_sec, $student_id);
                    if (isset($att['P'])):
                        $present = $att['P'];
                    else:
                        $present = '';
                    endif;
                    $total_days = array_sum($att);
//echo '<pre>'; print_r($att);  echo '<pre>'; print_r($student); exit;


                    $replace = array('SCHOOL_NAME' => $school->school_name,
                        'SCHOOL_ADDRESS' => $school->address1 . ' ' . $school->city . ' ' . $school->district,
                        'AFF_NUM' => $school->affiliation_code,
                        'NAME' => $student->first_name . " " . $student->last_name,
                        'CAST' => $student->category . ' - ' . $student->cast,
                        'NUMBER' => $student->reg_id,
                        'ADDMISSION_DATE' => $student->date_of_admission . ' (' . $student->course_of_admission . ') ',
                        'DOB' => $student->date_of_birth,
                        'DOB_IN_WORD' => date('d M Y', strtotime($student->date_of_birth)),
                        'LAST_CLASS' => $student->session_name,
                        'CONCESSION' => $student->concession_type,
                        'SUBJECT' => implode(', ', $cmpSub),
                        'DUES_PAID' => $Balance,
                        'PRESENT_WORKING_DAYS' => $present,
                        'WORKING_DAYS' => $total_days,
                        'FATHER_NAME' => $student->father_name,
                        'MOTHER_NAME' => $student->mother_name,
                        'CLASS' => $student->session_name,
                        'SECTION' => $student->section,
                        'COURSE' => $student->session_name,
                        'ROLL_NO' => $student->roll_no,
                        'GENDER' => $student->sex,
                        'DATE_OF_BIRTH' => $student->date_of_birth,
                        'SESSION' => $session->start_date . ' to ' . $session->end_date,
                        'PHOTO' => $student_image,
                        'DATE_OF_ISSUE_APPLICATION' => date('d M, Y'),
                        'LOGO' => $school_logo
                    );

                    $content = '';
                    if (is_object($transfer)):
                        $content = $transfer->format;
                        if (count($replace)):
                            foreach ($replace as $k => $v):
                                $literal = ' {
                ' . trim(strtoupper($k)) . '
            }';
                                $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                            endforeach;
                        endif;
                    endif;
                endif;
            endif;
            $doc_id = $transfer->id;
        elseif ($doc == 'test_report_card'):
            $content = '';
            $printed = '0';

            /* Make all certificates array */
            $certificates = array();
            if (!empty($student_icard)):
                $icard = get_object('document_master', $student_icard->id);
                $certificates['icard'] = $icard->name;
            endif;
            if (!empty($student_charcter)):
                $character = get_object('document_master', $student_charcter->id);
                $certificates['character'] = $character->name;
            endif;
            if (!empty($student_dmc)):
                $dmc = get_object('document_master', $student_dmc->id);
                $certificates['dmc'] = $dmc->name;
            endif;
            if (!empty($student_appri)):
                $appri = get_object('document_master', $student_appri->id);
                $certificates['appri'] = $appri->name;
            endif;
            if (!empty($student_sports)):
                $sports = get_object('document_master', $student_sports->id);
                $certificates['sports'] = $sports->name;
            endif;
            if (!empty($student_parti)):
                $participation = get_object('document_master', $student_parti->id);
                $certificates['participation'] = $participation->name;
            endif;
            if (!empty($test_report_card)):
                $participation = get_object('document_master', $student_parti->id);
                $certificates['participation'] = $participation->name;
            endif;

            if (!empty($student_test_report_card)):
                $test_report_card = get_object('document_master', $student_test_report_card->id);
                $certificates['test_report_card'] = $test_report_card->name;
            endif;

            if ($doc == 'test_report_card' && is_object($test_report_card) && !empty($student_id)):
                /* Get Last Entry */
                $QueryDocHis = new SchoolDocumentPrintHistory();
                $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $test_report_card->id, $student_id);

                /* Load the last updated doc if is it */
                if (is_object($last_his)):
                    $link = make_admin_url('view', 'print', 'print&id = ' . $last_his->id);
                    $content = '
            <div class = "alert alert-error">
            <button class = "close" data-dismiss = "alert"></button>
            <strong>Note:-</strong>
            You have already print this document on ' . date('d M, Y') . '. <a href = "' . $link . '" target = "_blank">Click here</a> to reprint this document.
            </div>';
                    $printed = '1';
                else:
                    if (isset($_POST['submit'])) {
                        $group_id = $_POST['group_id'];
                    }
                    // Get Examination Group Details
                    $obj = new examinationGroup;
                    $group = $obj->getRecord($group_id);

                    $obj = new examination();
                    $subjects = $obj->getSubjectMarks($group->exam_id, $student_id);

                    // Get Exam Details
                    $obj = new examination;
                    $exams = $obj->getExamDetails($group->exam_id);

                    #get Student detail	
                    $QueryStu = new studentSession();
                    $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

                    #get session detail	
                    $session = get_object('session', $session_id);

                    // Get Student Address Details
                    $obj = new studentAddress;
                    $student_address = $obj->getStudentAddressByType($student_id, 'permanent');

                    // Get Student Attendance
                    $student_attendance = attendance::getTotalAttendanceOfStudent($student_id, $session_id, $ct_sec);

                    // Get Total Working Days
                    $total_working_days = attendance::getTotalWorkingDays($ct_sec, $session_id, $student_id);

                    $im_obj = new imageManipulation();
                    if ($student->photo):
                        $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
                    else:
                        $student_image = '<img src = "assets/img/profile/img-1.jpg" style = "max-width:165px"/>';
                    endif;
                    if ($school->logo):
                        $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
                    else:
                        $school_logo = '<img src = "assets/img/invoice/webgarh.png" />';
                    endif;
                    $exam_table = '<table class = "table table-bordered" style = "width: 98%; border: 1px #ddd ridge">'
                            . '<thead><tr>'
                            . '<th style = "text-align: center">SUBJECT</th><th style = "text-align: center">' . ucfirst($group->title) . '</th>'
                            . '</tr></thead>'
                            . '<tbody><tr><td>'
                            . '<table class = "table table-bordered" style = "width: 98%; border: 1px #ddd ridge">'
                            . '<tr><td style = "text-align: center"><b>MAX. MARKS</b></td></tr>'
                            . '<tr"><td style="text-align: center"><b>EXAMS</b></td></tr>';
                    foreach ($subjects as $subject):
                        $exam_table .= '<tr><td style="text-align: center">' . $subject->title . '</td></tr>';
                    endforeach;
                    $exam_table.='</table></td><td>'
                            . '<table class="table table-bordered" style="width: 98%;
            border: 1px #ddd ridge">'
                            . '<thead><tr>';
                    foreach ($exams as $exam):
                        $exam_table.='<td style="text-align: center">' . $exam->maximum_marks . '</td>';
                    endforeach;
                    $exam_table.='</tr></thead>'
                            . '<tbody><tr>';
                    foreach ($exams as $exam):
                        $exam_table.='<td style="text-align: center"><b>' . ucfirst($exam->title) . '</b></td>';
                    endforeach;
                    $exam_table .= '</tr></tbody>';
                    foreach ($subjects as $subject):
                        $exam_table.='<tbody><tr>';
                        foreach ($exams as $exam):
                            $exam_table.='<td style="text-align: center">' . examination::getExamniationGrade($student_id, $exam->id, $exam->minimum_marks, $exam->maximum_marks, $subject->subject_id, $school->id) . '</td>';
                        endforeach;
                        $exam_table.='</tr></tbody>';
                    endforeach;
                    $exam_table.= '</table>'
                            . '</tbody></table>';
                    $replace = array(
                        'LOGO' => $school_logo,
                        'SCHOOL_NAME' => $school->school_name,
                        'SCHOOL_ADDRESS' => $school->address1 . ' ' . $school->city . ' ' . $school->district,
                        'SCHOOL_PHONE' => $school->school_phone,
                        'SCHOOL_EMAIL' => $school->email_address,
                        'SCHOOL_TOTAL_WORKING_DAYS' => $total_working_days,
                        'SCHOOL_WEB' => $school->website_url,
                        'SCHOOL_AFFL' => $school->affiliation_code,
                        'STUDENT_NAME' => $student->first_name . " " . $student->last_name,
                        'STUDENT_FATHER_NAME' => $student->father_name,
                        'STUDENT_MOTHER_NAME' => $student->mother_name,
                        'STUDENT_CLASS' => $student->session_name,
                        'STUDENT_ADMISSION' => $student->reg_id,
                        'STUDENT_DOB' => $student->date_of_birth,
                        'STUDENT_SECTION' => $student->section,
                        'STUDENT_TOTAL_ATTENDANCE' => $student_attendance,
                        'STUDENT_HOUSE' => $student_address->city,
                        'STUDENT_RATN' => $student_address->address1 . ', ' . $student_address->address2 . ', ' . $student_address->tehsil . ', ' . $student_address->district . ', ' . $student_address->city . ' (' . $student_address->state . ') - ' . $student->phone,
                        'EXAM_TABLE' => $exam_table
                    );

                    $content = '';
                    if (is_object($test_report_card)):
                        $content = $test_report_card->format;
                        if (count($replace)):
                            foreach ($replace as $k => $v):
                                $literal = '{' . trim(strtoupper($k)) . '}';
                                $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                            endforeach;
                        endif;
                    endif;
                endif;
            endif;
            $doc_id = $test_report_card->id;
        endif;
        /* Update document text for single student */
        if (isset($_POST['submit'])):

            $content = $_POST['document_text'];

            if (is_object($last_his)):
                $_POST['id'] = $last_his->id;
            endif;
            $_POST['doc_type'] = $content;
            $_POST['document_id'] = $doc_id;
            $_POST['school_id'] = $school->id;
            $_POST['session_id'] = $session_id;

            $_POST['section'] = $ct_sec;

            $_POST['student_id'] = $student_id;

            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);

            $admin_user->set_pass_msg(MSG_STUDENT_DOCUMENT_UPDATE);
            Redirect(make_admin_url('certificate', 'list', 'list&type=' . $type . '&doc=' . $doc . '&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&student_id=' . $student_id . '&group_id=' . $group_id));
        endif;

        break;
    case'list_indicators':
        $obj = new SchoolDocumentIndicators;
        $indicators = $obj->listIndicators($school->id);
        break;
    case'create_indicators':
        if (isset($_POST['submit'])) {
            $obj = new SchoolDocumentIndicators;
            $obj->saveIndicators($_POST);
            $admin_user->set_pass_msg('Indicator has been added successfully.');
            Redirect(make_admin_url('certificate', 'list_indicators', 'list_indicators'));
        }
        break;
    case'update_indicators':
        $obj = new SchoolDocumentIndicators;
        $indicator = $obj->getRecord($indicator_id);

        if (isset($_POST['submit'])) {
            $obj = new SchoolDocumentIndicators;
            $obj->saveIndicators($_POST);
            $admin_user->set_pass_msg('Indicator has been added successfully.');
            Redirect(make_admin_url('certificate', 'list_indicators', 'list_indicators'));
        }
        break;
    case'delete_indicators':
        $obj = new SchoolDocumentIndicators;
        $obj->id = $indicator_id;
        $obj->Delete();
        $admin_user->set_pass_msg('Indicator has been deleted successfully.');
        Redirect(make_admin_url('certificate', 'list_indicators', 'list_indicators'));
        break;

    default:break;
endswitch;
?>
