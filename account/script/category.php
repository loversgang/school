<?php
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');



isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';

isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['type'])?$type=$_GET['type']:$type='';
$modName='category';
/*handle actions here. */
switch ($action):
	case'list':			
			/* All data from staff_category_master table */
                $QueryObj=new staffCategory;
				$QueryObj->listAll($school->id);
		break;
	case'insert':
			/* Add staff_category */
			if(isset($_POST['submit'])):			
					$QueryObj=new staffCategory;
					$QueryObj->saveData($_POST); 
					$admin_user->set_pass_msg(MSG_STAFF_CAT_ADDED);
					Redirect(make_admin_url('category', 'list', 'list'));				
			endif;		
		break;	
	case'update':
			/* edit Staff Category */
			$QuObj=new staffCategory;
			$object=$QuObj->getStaffCategory($id);
			if(isset($_POST['submit'])):			
					$QueryObj=new staffCategory;
					$QueryObj->saveData($_POST); 
					$admin_user->set_pass_msg(MSG_STAFF_CAT_UPDATE);
					Redirect(make_admin_url('category', 'list', 'list'));
			endif;		
		break;	
		
	case'delete':
	    /* delete record		 */
				$QueryObj = new staffCategory();
				$QueryObj->deleteRecord($id);  
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('category', 'list', 'list'));
	break;

	default:break;

endswitch;


?>

