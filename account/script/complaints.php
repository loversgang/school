<?php

include_once(DIR_FS_SITE . 'include/functionClass/complaintsClass.php');
$modName = 'complaints';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : '';

$page = isset($_GET['Page']) ? $_GET['Page'] : '';
switch ($action):
    case'list':
        $query = new complaints();
        $lists_complaints = $query->lists_complaints();

        break;
    case'feedback':

        $query = new complaints;
        $check_correct_data = $query->check_correct_data($id, $_GET['student_id']);
        if ($check_correct_data == false) {
            $admin_user->set_pass_msg('You cannot view this Complaint');
            Redirect(make_admin_url('complaints'));
        }
        if (isset($_POST['submit'])) {
            $query = new complaints_feedback();
            $query->saveComplaints_feedback($_POST);
            $admin_user->set_pass_msg('Feedback Send Successfully');
        }

        $query = new complaints_feedback;
        $all_complaints_feeback = $query->all_complaints_feeback($id, $_GET['student_id']);

        break;
    case 'delete':
        $query = new complaints();
        $query->delete_complaint($id);
        $admin_user->set_pass_msg('Complaint Deleted Successfully');
        Redirect(make_admin_url('complaints', 'list', 'list'));
        break;

    case 'referer':
        $query = new referers;
        $all_staff = $query->get_staff();

        if (isset($_POST['submit'])) {
            foreach ($_POST['staff_id'] as $staff_id) {
                $data = array();
                $data['staff_id'] = $staff_id;
                $data['description'] = $_POST['description'];
                $data['school_id'] = $_POST['school_id'];
                $data['date'] = $_POST['date'];
                $data['complaint_id'] = $_POST['complaint_id'];
                $query = new referers();
                $query->save_referers($data);
            }
            $admin_user->set_pass_msg('Referer Send Successfully');
            Redirect(make_admin_url('complaints'));
        }
        break;
    case 'referer_list':
        $query = new referers;
        $all_referers = $query->all_referers($id);

        break;
    case 'message':
        if (isset($_POST['submit'])) {
            $query = new referers_messages;
            $query->save_referers_messages($_POST);
            $admin_user->set_pass_msg('Message Send Successfully');
        }

        $query = new referers_messages;
        $referers_messages = $query->all_referers_message($id, $_GET['staff_id']);
        break;

    case'resolve':
        $query = new complaints();
        $query->complaint_solved($_GET['complaint_id'], $_GET['action']);
        $admin_user->set_pass_msg('Complaint Resolved Successfully');
        Redirect(make_admin_url('complaints', 'list', 'list'));
        break;

    case'resolved':
        $query = new complaints();
        $complaints = $query->complaint_solved($_GET['complaint_id'], $_GET['action']);
        $admin_user->set_pass_msg('Complaint Resolved Successfully');
        Redirect(make_admin_url('complaints', 'list', 'list'));
        break;
    default:break;
endswitch;
?>