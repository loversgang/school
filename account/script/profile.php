<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subscriptionPlanClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSubscriptionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSettingClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsClass.php');


$modName = 'profile';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';

#get School Types
$QueryS_type = new schoolType;
$QueryS_type->listAll();

#handle actions here.
switch ($action):
    case'list':
        /* Get School with plan */
        $Query = new school();
        $object = $Query->getSchoolWithPlan($school->id);

        /* Update School Info */
        if (isset($_POST['submit'])):
            $QueryObj = new school();
            $school_id = $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_SCHOOL_INFO_UPDATE);
            Redirect(make_admin_url('profile', 'list', 'list'));

        endif;
        break;
    case'insert':
        if (isset($_POST['submit'])):
            $QueryObj = new schoolExpense();
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_EXPENSE_ADDED);
            Redirect(make_admin_url('profile', 'list', 'list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('profile', 'list', 'list'));
        endif;
        break;
    case'update':
        $smsArray = array();
        /* Get School with plan */
        $Query = new school();
        $object = $Query->getSchoolWithPlan($school->id);

        /* Get School setting */
        $QuerySchool = new schoolSetting();
        $setting = $QuerySchool->getSchoolSetting($school->id);

        /* Get School setting */
        $QuerySchool = new schoolSms();
        $smsArray = $QuerySchool->getSchoolSmsArray($school->id);

        /* Update School Info */
        if (isset($_POST['submit'])):
            $QueryObj = new school();
            $school_id = $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_ACCOUNT_PASSWORD_CHANGE_SUCCESS);
            Redirect(make_admin_url('profile', 'update', 'update'));

        endif;

        break;

    case'delete_image':
        #delete school logo
        $QueryObj = new school();
        $QueryObj->deleteImage($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('profile', 'list', 'list'));
        break;


    default:break;
endswitch;
?>
