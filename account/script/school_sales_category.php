<?php

/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/school_sales_categoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');


$modName = 'school_sales_category';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['ct_veh']) ? $ct_veh = $_REQUEST['ct_veh'] : $ct_veh = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['stoppage_id']) ? $stoppage_id = $_REQUEST['stoppage_id'] : $stoppage_id = '';
isset($_REQUEST['vehicle_id']) ? $vehicle_id = $_REQUEST['vehicle_id'] : $vehicle_id = '';


switch ($action):
    case'list':
        $query = new school_sales_category;
        $query->Where = "ORDER BY `id` DESC ";
        $lists = $query->listschool_sales();
        break;
    case'insert':
        if (isset($_POST['submit'])) {
            $query = new school_sales_category;
            if (isset($_POST['is_active'])) {
                $_POST['is_active'] = 1;
            } else {
                $_POST['is_active'] = 0;
            }
            $query->saveData($_POST);
//            $admin_user->set_error();
            $admin_user->set_pass_msg('Sales Category inserted successfully');
            Redirect(make_admin_url('school_sales_category', 'list', 'list'));
        }
        break;
    case'update':
        if ($id == false) {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Id not found');
            Redirect(make_admin_url('school_sales_category', 'list', 'list'));
        }

        $checkId = school_sales_category :: checkId($id);

        if ($checkId) {
            $query = new school_sales_category();
            $query->Where = "WHERE `id` = $id";
            $list = $query->DisplayOne();
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Id is incorrect');
            Redirect(make_admin_url('school_sales_category', 'list', 'list'));
        }

        if (isset($_POST['submit'])) {
            $query = new school_sales_category;

            if (isset($_POST['is_active'])) {
                $_POST['is_active'] = 1;
            } else {
                $_POST['is_active'] = 0;
            }
            $query->saveData($_POST);
            $admin_user->set_pass_msg('Sales updated successfully');
            Redirect(make_admin_url('school_sales_category', 'list', 'list'));
        }
        break;

    case'delete':
        $query = new school_sales_category;
        $query->id = $id;
        $query->Delete();
        $admin_user->set_pass_msg('Sales Deleted successfully');
        Redirect(make_admin_url('school_sales_category', 'list', 'list'));
        break;

    default:break;
endswitch;
?>