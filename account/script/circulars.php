<?php

/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/circularsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');


$modName = 'circulars';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['ct_veh']) ? $ct_veh = $_REQUEST['ct_veh'] : $ct_veh = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['stoppage_id']) ? $stoppage_id = $_REQUEST['stoppage_id'] : $stoppage_id = '';
isset($_REQUEST['vehicle_id']) ? $vehicle_id = $_REQUEST['vehicle_id'] : $vehicle_id = '';


switch ($action):
    case'list':
        $query = new circulars;
        $lists = $query->listCirculars();
        break;

    case'insert':
        if (isset($_POST['submit'])) {
            $query = new circulars;
            $query->saveData($_POST);
            $admin_user->set_pass_msg('Circular inserted successfully');
            Redirect(make_admin_url('circulars', 'list', 'list'));
        }
        break;

    case'update':
        if ($id == false) {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Id not found');
            Redirect(make_admin_url('circulars', 'list', 'list'));
        }
        $checkId = circulars :: checkId($id);

        if ($checkId) {
            $query = new circulars();
            $query->Where = "WHERE `id` = $id";
            $list = $query->DisplayOne();
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Id is incorrect');
            Redirect(make_admin_url('circulars', 'list', 'list'));
        }

        if (isset($_POST['submit'])) {
            $query = new circulars;
            $query->Where = "WHERE `id` = '$id'";
            $list = $query->DisplayOne();

            $query = new circulars;
            $query->saveData($_POST, $list->file);
            $admin_user->set_pass_msg('Circular updated successfully');
            Redirect(make_admin_url('circulars', 'list', 'list'));
        }
        break;

    case'delete':
        $query = new circulars;
        $query->Where = "WHERE `id` = '$id'";
        $list = $query->DisplayOne();

        $query = new circulars();
        $query->delete_school_sales($id, $list->file);
        $admin_user->set_pass_msg('Circular Deleted successfully');
        Redirect(make_admin_url('circulars', 'list', 'list'));
        break;

    default:break;
endswitch;
?>