<?php

include_once(DIR_FS_SITE . 'include/functionClass/syllabusClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
$modName = 'syllabus';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';

$QueryObjQ = new session();
$AllSessYear = $QueryObjQ->listOfYearSess($school->id);
if (empty($sess_int)):
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
endif;
#handle actions here.
switch ($action):
    case'list':
        $QueryObj = new syllabus();
        $syllabus_list = $QueryObj->listSyllabus($sess_int);
        break;
    case'insert':
        $QueryObj = new session();
        $QueryObj->listOfYearSession($school->id, $sess_int);
        if (isset($_POST['submit'])) {
            if ($_FILES['file']['error'] == '0') {
                $target_dir = DIR_FS_SITE_UPLOAD . 'file/syllabus/';
                $target_file = $target_dir . basename(time() . $_FILES['file']['name']);
                if (move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) {
                    $arr['school_id'] = $_POST['school_id'];
                    $arr['session_year'] = $_POST['session_year'];
                    $arr['session_id'] = $_POST['session_id'];
                    $arr['subject_id'] = $_POST['subject_id'];
                    $arr['description'] = $_POST['description'];
                    $arr['is_active'] = isset($_POST['is_active']) ? '1' : '0';
                    $arr['file'] = time() . $_FILES['file']['name'];
                    $obj = new syllabus;
                    $obj->saveSyllabus($arr);
                    $admin_user->set_pass_msg('Syllabus Added Successfully!');
                    Redirect(make_admin_url('syllabus', 'list', 'list'));
                } else {
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something Went Wrong!');
                    Redirect(make_admin_url('syllabus', 'list', 'list'));
                }
            }
        }
        break;
    case'update':
        $QueryObj = new session();
        $QueryObj->listOfYearSession($school->id, $sess_int);

        $obj = new syllabus;
        $syllabus = $obj->getRecord($id);

        if (isset($_POST['submit'])) {
            if ($_FILES['file']['error'] == '0') {
                $target_dir = DIR_FS_SITE_UPLOAD . 'file/syllabus/';
                $target_file = $target_dir . basename(time() . $_FILES['file']['name']);
                if (move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) {
                    $arr['id'] = $_POST['id'];
                    $arr['session_id'] = $_POST['session_id'];
                    $arr['subject_id'] = $_POST['subject_id'];
                    $arr['description'] = $_POST['description'];
                    $arr['is_active'] = isset($_POST['is_active']) ? '1' : '0';
                    $arr['file'] = time() . $_FILES['file']['name'];
                    $obj = new syllabus;
                    $obj->saveSyllabus($arr);
                    $admin_user->set_pass_msg('Syllabus Updated Successfully!');
                    Redirect(make_admin_url('syllabus', 'list', 'list'));
                } else {
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Something Went Wrong!');
                    Redirect(make_admin_url('syllabus', 'list', 'list'));
                }
            } else {
                $array['id'] = $_POST['id'];
                $array['session_id'] = $_POST['session_id'];
                $array['subject_id'] = $_POST['subject_id'];
                $array['description'] = $_POST['description'];
                $array['is_active'] = isset($_POST['is_active']) ? '1' : '0';
                $obj = new syllabus;
                $obj->saveSyllabus($array);
                $admin_user->set_pass_msg('Syllabus Updated Successfully!');
                Redirect(make_admin_url('syllabus', 'list', 'list'));
            }
        }
        break;
    case'delete':
        $obj = new syllabus;
        $obj->id = $id;
        $obj->Delete();
        $admin_user->set_pass_msg("Syllabus Deleted Successfully!");
        Redirect(make_admin_url('syllabus', 'list', 'list'));
        break;
    default:
        break;
endswitch;
