<?php

error_reporting(0);
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsEmailClass.php');

$modName = 'fee';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_REQUEST['id']) ? $id = $_REQUEST['id'] : $id = '';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';

isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['show_check']) ? $show = $_GET['show_check'] : $show = '';
isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = 'select';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
isset($_REQUEST['interval_id']) ? $interval_id = $_REQUEST['interval_id'] : $interval_id = '';
isset($_REQUEST['sess_int']) ? $sess_int = $_REQUEST['sess_int'] : $sess_int = '';





$QueryObjQ = new session();
$AllSessYear = $QueryObjQ->listOfYearSess($school->id);


if (empty($sess_int)):
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
endif;

#get_all current session 
$QuerySession = new session();
$QuerySession->listAllOnlyCurrent($school->id, '1', $sess_int);


if (empty($sess_int)):
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
endif;




// echo '<pre>';print_r($QuerySession); exit;
/* Get school fee slip template for student fee */
$QueryDocsTmp = new SchoolDocumentTemplate();
$slip = $QueryDocsTmp->get_school_single_document($school->id, 5);
//pr($slip); exit;
switch ($action):
    case'list':
        $amount = '';
        if ($type == 'select'):
            $per = '0%';
        elseif ($type == 'list'):
            $per = '35%';
        else:
            $per = '25%';
        endif;

        if ($type == 'select'):

        elseif ($type == 'list'):
            if (empty($session_id)):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_SESSION_SECTION_NEED_SELECT);
                Redirect(make_admin_url('fee', 'list', 'list'));
            endif;


            #get Session Info
            $QueryS = new session();
            $session = $QueryS->getRecord($session_id);

            #Get interval detail
            $interval_detail = get_object('session_interval', $interval_id);

            #Get Section Students
            $QueryOb = new studentSession();
            $records = $QueryOb->sectionSessionStudents($session_id, $ct_sect);

            if ($type == 'list' && empty($records)):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_SESSION_EMPTY);
                Redirect(make_admin_url('fee', 'list', 'list'));
            endif;

        endif;

        #get Student Info
        $QueryStu = new studentSession();
        $student_info = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sect);

        break;

    case'insert':

        if ($type == 'select'):
            $per = '0%';
        elseif ($type == 'list'):
            $per = '35%';
        elseif ($type == 'fee'):
            $per = '70%';
        else:
            $per = '25%';
        endif;

        #get Session Info
        $QueryS = new session();
        $session = $QueryS->getRecord($session_id);

        if ($type == 'list'):
            if (empty($session_id)):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_SESSION_SECTION_NEED_SELECT);
                Redirect(make_admin_url('fee', 'insert', 'insert'));
            endif;

            #Get Section Students
            $QueryOb = new studentSession();
            $records = $QueryOb->sectionSessionStudents($session_id, $ct_sect);

            #check student in session section
            if (empty($records)):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_SESSION_EMPTY);
                Redirect(make_admin_url('fee', 'insert', 'insert'));
            endif;


        elseif ($type == 'fee'):

            //pr($_POST); exit;
            #if empty student id
            if (empty($student_id)):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_STUDENT_NEED_SELECT);
                Redirect(make_admin_url('fee', 'insert', 'insert&type=list'));
            endif;

            #Get student pay heads students
            $SessStuFeeObj = new session_student_fee();
            $pay_heads = $SessStuFeeObj->GetStudentPayHeads($session_id, $student_id, 'array');

            #fill the previous interval fee if not update then insert
            $SessOb = new studentSessionInterval();
            $SessOb->CheckPreviousFeeRecord($session_id, $ct_sect, $student_id, $interval_id, $pay_heads);

            #Get Last Or Current Interval	
            $SessIntOb = new session_interval();
            $last_int_id = $SessIntOb->getCurrentLastIntervalId($session_id);

            if ($interval_id == $last_int_id):
                #update Current interval Fee
                $SessObFee = new studentSessionInterval();
                $SessObFee->updateIntervalFee($session_id, $ct_sect, $student_id, $interval_id, $pay_heads);
            endif;

            ############################################
            #get Again All details 
            #Get student pay heads students
            $SessStuFeeObj = new session_student_fee();
            $pay_heads = $SessStuFeeObj->GetStudentPayHeads($session_id, $student_id, 'array');

            #Get interval detail
            $SessIntOb = new studentSessionInterval();
            $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sect, $student_id, $interval_id);

            #Get Session Interval In array
            $SessIntOb = new session_interval();
            $interval = $SessIntOb->listOfIntervalInArray($session_id);

            #get Student Info
            $QueryStu = new studentSession();
            $student_info = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sect);

            #get first date and last list of this interval		
            $from = $student_interval_detail->from_date;
            $to = $student_interval_detail->to_date;

            #Get Student Previous Fee List in this interval
            $QueryFirst = new studentFee();
            $Fee_list = $QueryFirst->listOfAllPreviousRecords($session_id, $ct_sect, $student_id, $from, $to);

            if (empty($Fee_list)):
                # if empty update total paid
                $SessObFee = new studentSessionInterval();
                $SessObFee->updateTotalPaidFeeToZero($student_interval_detail->id);

                # Again Get interval detail
                $SessIntOb = new studentSessionInterval();
                $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sect, $student_id, $interval_id);
            endif;
//print_r($student_interval_detail); exit;
            #Get Total paid in this interval
            $total_paid = $student_interval_detail->paid;

            #check the absent fee if not the first fee
            if ($student_interval_detail->interval_position == '1'):
                $Absent = '';
            else:
                foreach ($interval as $ik => $iv):
                    if ($iv->id == $interval_id):
                        break;
                    endif;
                    $atten_from = $iv->from_date;
                    $atten_to = $iv->to_date;
                endforeach;

                #get Student Absent Now for previous date
                $QurAtten = new attendance();
                $Absent = $QurAtten->getStudentAbsent($session_id, $ct_sect, $student_id, $atten_from, $atten_to);
            endif;

            #get Pending Fee for previous month
            $SessObPre = new studentSessionInterval();
            $pending_fee = $SessObPre->getPreviousBalanceFee($session_id, $ct_sect, $student_id, $interval_id);

            #get Pending Fee for previous month
            $SessObLast = new studentSessionInterval();
            $end_interval = $SessObLast->getEndInterval($session_id);

            if ($end_interval == $interval_id):
                #get Student Absent Now for current interval
                $QurAttenLast = new attendance();
                $Absent_last = $QurAttenLast->getStudentAbsent($session_id, $ct_sect, $student_id, $student_interval_detail->from_date, $student_interval_detail->to_date);
            endif;

            # Again Get interval detail
            $SessIntOb = new studentSessionInterval();
            $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sect, $student_id, $interval_id);


            $prevYearFee = array();
            if (!empty($session_id)):
                #get students current session
                $CurrObj = new studentSession();
                $prevYearFee = $CurrObj->getStudentPreviousSessionFeePending($student_id, $session_id);

            endif;

        endif;
        //echo '<pre>'; print_r($student_interval_detail); exit;

        /* Add Student Fee */
        if (isset($_POST['submit_fee'])):
            #Get interval fee detail
            $SessIntOb = new studentSessionInterval();
            $student_interval_detail = $SessIntOb->getDetail($_POST['session_id'], $_POST['section'], $_POST['student_id'], $_POST['interval_id']);

            #check payment date within the interval
            if (isset($_POST['use_current_date']) && $_POST['use_current_date'] == '1'):
                echo "";
            else:
                if (($_POST['payment_date'] < $student_interval_detail->from_date) || ($_POST['payment_date'] > $student_interval_detail->to_date)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(PATMMENT_DATE_WITH_IN . $student_interval_detail->from_date . ' to ' . $student_interval_detail->to_date);
                    Redirect(make_admin_url('fee', 'insert', 'insert&type=fee&session_id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section'] . '&interval_id=' . $_POST['interval_id'] . '&student_id=' . $_POST['student_id'] . '&show_check=true'));
                endif;
            endif;
            // echo '<pre>'; print_r($_POST); exit;
            #Add student Fee  in student fee table				
            $QueryFee = new studentFee();
            $max_id = $QueryFee->saveData($_POST);
            #Add student record
            $_POST['student_fee_id'] = $max_id;
            #Save Fee Record
            $QueryFeeRec = new studentFeeRecord();
            $QueryFeeRec->saveFeeRecords($_POST['fee_record'], $max_id);
            #update interval fee detail
            $SessPayOb = new studentSessionInterval();
            $SessPayOb->updatePaymentData($_POST, $student_interval_detail->id);

            #Seb Mail to His parents
            $s_info = get_object('student', $_POST['student_id']);


            $admin_user->set_pass_msg(MSG_STUDENT_FEE_ADDED);
            if (is_object($slip)):
                Redirect(make_admin_url('fee', 'thrash', 'thrash&type=fee&fee_id=' . $max_id . '&interval_id=' . $_POST['interval_id']));
            else:
                Redirect(make_admin_url('fee', 'insert', 'insert&type=fee&session_id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section'] . '&interval_id=' . $_POST['interval_id'] . '&student_id=' . $_POST['student_id']));
            endif;
        endif;

        break;

    case'view':
        $amount = '';
        $paid_heads = array();

        $balance_amount = '';
        $paid_heads = array();
        #if blank
        if (empty($student)):
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_STUDENT_NEED_SELECT);
            Redirect(make_admin_url('fee', 'insert', 'insert&type=list'));
        endif;

        #get Session Info
        $QueryS = new session();
        $object = $QueryS->getRecord($session_id);

        #get Student Info
        $QueryStu = new studentSession();
        $student_info = $QueryStu->SingleStudentsWithSession($student, $session_id, $ct_sect);

        #Get Session Fee Heads In Array
        $Queryhd = new session_fee_type();
        $all_heads = $Queryhd->sessionPayHeads($session_id, 'array');

        #Get Main Entry of this month Fee List

        $QueryFirst = new studentFee();
        $Month_entry = $QueryFirst->listRecordMonth($session_id, $ct_sect, $student, $month, $year);

        if (is_object($Month_entry)):
            #Get Student previous Fee List
            $QueryFList = new studentFeeRecord();
            $Fee_list = $QueryFList->listAll($Month_entry->id);

            #Get Student previous Fee List
            $Query_heads = new studentFee();
            $paid_heads = $Query_heads->check_previous_pay_heads($Month_entry->id);

            #Get Student previous Fee List
            $QuerySum = new studentFeeRecord();
            $Sum = $QuerySum->sum_amount($Month_entry->id);

            $balance_amount = $Month_entry->total_amount - $Sum;
        endif;

        #get Student Month Attendance
        $student_atten = array();
        $Q_obj = new attendance();
        $student_atten = $Q_obj->checkSessionSectionMonthYearAttendace($session_id, $ct_sect, $student, $month - 1, $year);

        if (!empty($student_atten)):
            if (array_key_exists('A', $student_atten)):
                $Absent = $student_atten['A'];
            else:
                $Absent = '0';
            endif;
        else:
            $Absent = '0';
        endif;


        break;

    case'update':

        #Get interval detail
        $SessIntOb = new studentSessionInterval();
        $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sect, $student_id, $interval_id);

        $amount = '';
        #get Session Info
        $QueryS = new session();
        $session = $QueryS->getRecord($session_id);

        #get Student Info
        $QueryStu = new studentSession();
        $student_info = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sect);

        #Get student Fee record
        $QueryRec = new studentFee();
        $record = $QueryRec->getRecord($id);



        /* Add Student Fee */
        if (isset($_POST['submit'])):

            $fee_detail = get_object('student_fee', $id);

            #Get interval detail
            $SessIntOb = new studentSessionInterval();
            $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sect, $student_id, $interval_id);

            #less amount in interval entry 			
            $SessIntOb = new studentSessionInterval();
            $SessIntOb->updateInDelete($_POST['prev_amount'], $student_interval_detail->id);

            #less amount in interval entry 			
            $SessIntOb = new studentSessionInterval();
            $SessIntOb->addPaidAmount($_POST['total_amount'], $student_interval_detail->id);

            #check payment date within the interval	
            if (($_POST['payment_date'] < $student_interval_detail->from_date) || ($_POST['payment_date'] > $student_interval_detail->to_date)):
                $admin_user->set_error();
                $admin_user->set_pass_msg(PATMMENT_DATE_WITH_IN . $student_interval_detail->from_date . ' to ' . $student_interval_detail->to_date);
                Redirect(make_admin_url('fee', 'update', 'update&id=' . $_POST['id'] . '&session_id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section'] . '&interval_id=' . $_POST['interval_id'] . '&student_id=' . $_POST['student_id']));

            endif;


            #Add student Fee 
            $QueryFee = new studentFee();
            $max_id = $QueryFee->saveData($_POST);

            $admin_user->set_pass_msg(MSG_STUDENT_FEE_ADDED);
            Redirect(make_admin_url('fee', 'insert', 'insert&type=fee&session_id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section'] . '&student_id=' . $_POST['student_id'] . '&interval_id=' . $_POST['interval_id']));

        endif;
        break;


    case'delete':
        #Get interval detail
        $SessIntOb = new studentSessionInterval();
        $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sect, $student_id, $interval_id);

        $fee_detail = get_object('student_fee', $id);

        #less amount in interval entry 			
        $SessIntOb = new studentSessionInterval();
        $SessIntOb->updateInDelete($fee_detail->total_amount, $student_interval_detail->id);

        #delete reocrd Fee entry 
        $QueryFeeHeads = new studentFee();
        $QueryFeeHeads->deleteRecord($id);

        #Get Fee Heads paid
        $QueryFeeHeadsDel = new studentFeeRecord();
        $QueryFeeHeadsDel->deleteAllRecord($id);



        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('fee', 'insert', 'insert&type=fee&session_id=' . $session_id . '&ct_sect=' . $ct_sect . '&student_id=' . $student_id . '&interval_id=' . $interval_id));
        break;

    case'thrash':
        #Print the receipt
        #Get Document
        $QueryDoc = new document();
        $Doc = $QueryDoc->getRecord($slip->id);

        #Get Fee Entry
        $QueryFee = new studentFee();
        $Fee = $QueryFee->getRecord($_REQUEST['fee_id']);

        #Get Course Name
        $QueryCou = new studentSession();
        $course = $QueryCou->getClassNameBySession($Fee->session_id);

        #Get Student
        $QueryStu = new studentSession();
        $student = $QueryStu->SingleStudentsWithSession($Fee->student_id, $Fee->session_id, $Fee->section);

        #get student permanent address
        $QueryP = new studentAddress();
        $s_p_ad = $QueryP->getStudentAddressByType($student->id, 'permanent');

        #Get Fee Heads paid
        $QueryFeeHeads = new studentFeeRecord();
        $FeeRecord = $QueryFeeHeads->listAll($_REQUEST['fee_id']);
        $rec_head = '';
        $sub_total = '';

        $object = get_object('session', $Fee->session_id);

        #Get Receipt No.
        $QueryFeeRec = new studentFee();
        $ReceiptNo = $QueryFeeRec->getLastFeeReceipt($school_setting->school_id, $school_setting->receipt_prefix, $school_setting->receipt_start_no);

        #Get interval detail
        $SessIntOb = new studentSessionInterval();
        $student_interval_detail = $SessIntOb->getDetail($Fee->session_id, $Fee->section, $Fee->student_id, $interval_id);

        if (!empty($FeeRecord)):
            foreach ($FeeRecord as $key => $val):
                $rec_head = $rec_head . "<tr>
					<th style='text-align:left;width:60%;font-weight: normal;'>" . $val->fee_type . "</th>
					<th style='text-align:right;width:20%;font-weight: normal;'>" . number_format($val->amount, 2) . "</th>
				</tr>";
                $sub_total = $sub_total + $val->amount;
            endforeach;
        else:
            $rec_head = $rec_head . "<tr>
					<th style='text-align:left;width:60%;font-weight: normal;'>Fee</th>
					<th style='text-align:right;width:40%;font-weight: normal;'>" . number_format($Fee->total_amount, 2) . "</th>
				</tr>";
            $sub_total = $Fee->total_amount;
        endif;
        #Add Late Fee
        if (!empty($student_interval_detail->late_fee)):
            $rec_head = $rec_head . "<tr>
					<th style='text-align:left;width:60%;font-weight: normal;'>Late Fee</th>
					<th style='text-align:right;width:40%;font-weight: normal;'><strong>+ </strong>" . number_format($student_interval_detail->late_fee, 2) . "</th>
				</tr>";
            $sub_total = $sub_total + $student_interval_detail->late_fee;
        endif;
        #Add Vehicle Fee
        if (!empty($student_interval_detail->vehicle_fee)):
            $rec_head = $rec_head . "<tr>
					<th style='text-align:left;width:60%;font-weight: normal;'>Vehicle Fee</th>
					<th style='text-align:right;width:40%;font-weight: normal;'><strong>+ </strong>" . number_format($student_interval_detail->vehicle_fee, 2) . "</th>
				</tr>";
            $sub_total = $sub_total + $student_interval_detail->vehicle_fee;
        endif;
        #Add Absent Fine
        if (!empty($student_interval_detail->absent_fine)):
            $rec_head = $rec_head . "<tr>
					<th style='text-align:left;width:60%;font-weight: normal;'>Attendance Fine</th>
					<th style='text-align:right;width:40%;font-weight: normal;'><strong>+ </strong>" . number_format($student_interval_detail->absent_fine, 2) . "</th>
				</tr>";
            $sub_total = $sub_total + $student_interval_detail->absent_fine;
        endif;
        #Add Subtract other fee Fine
        if (!empty($student_interval_detail->other_fee)):
            $rec_head = $rec_head . "<tr>
					<th style='text-align:left;width:60%;font-weight: normal;'>" . $student_interval_detail->other_fee_name . "</th>
					<th style='text-align:right;width:40%;font-weight: normal;'><strong>" . $student_interval_detail->other_fee_type . " </strong>" . number_format($student_interval_detail->other_fee, 2) . "</th>
				</tr>";
            if ($student_interval_detail->other_fee_type == "-"):
                $sub_total = ($sub_total - $student_interval_detail->other_fee);
            elseif ($student_interval_detail->other_fee_type == "+"):
                $sub_total = ($sub_total + $student_interval_detail->other_fee);
            endif;

        endif;
        #Less Concession
        if (!empty($student_interval_detail->concession)):
            $rec_head = $rec_head . "<tr>					
					<th style='text-align:left;width:60%;font-weight: normal;'>Concession (" . $student->concession . ")</th>
					<th style='text-align:right;width:40%;font-weight: normal;'><strong>- </strong>" . number_format($Fee->concession, 2) . "</th>
				</tr>";
            $sub_total = $sub_total - $student_interval_detail->concession;
        endif;

        if (getTotalDays($student_interval_detail->from_date, $student_interval_detail->to_date) <= '31'):
            $month_name = date('F-Y', strtotime($student_interval_detail->from_date));
        else:
            $month_name = date('F', strtotime($student_interval_detail->from_date)) . "  to  " . date('F,Y', strtotime($student_interval_detail->to_date));
        endif;

        if (!empty($Doc)):
            if (is_object($s_p_ad)):
                $address = $s_p_ad->address1 . ', ' . $s_p_ad->tehsil . ', ' . $s_p_ad->district;
            else:
                $address = '';
            endif;

            if (!empty($Fee->remarks)):
                $remark = $Fee->remarks;
            else:
                $remark = '';
            endif;
            $replace = array('SCHOOL_NAME' => $school->school_name,
                'AFFILATION' => $school->affiliation_board,
                'AFFILATION_NO' => $school->affiliation_code,
                'SCHOOL_ADDRESS' => $school->address1 . ', ' . $school->city . ', Distt. ' . $school->district,
                'SCHOOL_PHONE' => $school->phone1,
                'DATE' => date('d M, Y', strtotime($Fee->payment_date)),
                'NAME' => $student->first_name . " " . $student->last_name,
                'RECEIPT' => $ReceiptNo,
                'FATHER_NAME' => $student->father_name,
                'ADDRESS1' => $address,
                'MONTH' => $month_name,
                'REMARK' => $remark,
                'AMOUNT_IN_WORD' => number_to_word($student_interval_detail->paid),
                'CLASS' => $course,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'REG_ID' => $student->reg_id,
                'FEE_ADDED' => "<table style='width: 100%; text-align: left; line-height: 14px;'>
										<tbody>" . $rec_head . "</tbody>
									</table>",
                'SUB_TOTAL' => number_format($sub_total, 2),
                'PAID' => number_format($student_interval_detail->paid, 2),
                'BALANCE' => number_format(($sub_total - $student_interval_detail->paid), 2)
            );

            $content = $Doc->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = '{' . trim(strtoupper($k)) . '}';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                    $content = str_replace('&nbsp;', '', $content);
                endforeach;
            endif;


            /* Get Parents Deatil */
            $QueryPr = new query('student_family_details');
            $QueryPr->Where = "where student_id='" . $student->id . "'";
            $parents = $QueryPr->DisplayOne();

            if ($parents):
                /* Now Fee Deposit Email */
                $earr1 = array();
                $earr1['PARENTS'] = ucfirst($parents->father_name) . " & " . ucfirst($parents->mother_name);
                $earr1['NAME'] = ucfirst($student->first_name) . ' ' . $student->last_name;
                $earr1['PAID'] = number_format($student_interval_detail->paid, 2);
                $earr1['BALANCE'] = number_format(($sub_total - $student_interval_detail->paid), 2);
                $earr1['TOTAL_FEE'] = number_format($sub_total, 2);
                $earr1['SITE_NAME'] = $school->school_name;
                $earr1['DATE'] = $Fee->payment_date;
                $earr1['PRINCIPAL_NAME'] = $school->school_head_name;
                $earr1['SCHOOL_NAME'] = $school->school_name;
                $earr1['CONTACT_PHONE'] = $school->school_phone;
                $earr1['CONTACT_EMAIL'] = $school->email_address;

                #Send and Add Entry to email counter if set in setting
                if ($school_setting->is_email_allowed == '1'):
                    $QueryCountEmail = new schoolEmail();
                    $email_count = $QueryCountEmail->getMonthlyCount($school->id);
                    $QueryEmail = new schoolEmail();
                    $Check_email = $QueryEmail->getTypeEmail($school->id, 'fee_deposit');
                    if (is_object($Check_email)):
                        $msg1 = get_database_msg('7', $earr1);
                        $subject = get_database_msg_subject('7', $earr1);
                        unset($POST);

                        $POST['school_id'] = $school->id;
                        $POST['email_type'] = 'fee_deposit';
                        $POST['type'] = 'email';
                        $POST['on_date'] = date('Y-m-d');
                        $POST['email_subject'] = $subject;
                        $POST['to_email'] = $parents->email;
                        $POST['email_text'] = $msg1;
                        $POST['from_email'] = $school->email_address;
                        $POST['email_school_name'] = $school->school_name;

                        $QuerySave = new schoolSmsEmailHistory();
                        $QuerySave->saveData($POST);
                    endif;
                endif;

                #Send and Add Entry to sms counter if set in setting
                if ($school_setting->is_sms_allowed == '1'):
                    $QuerySms = new schoolSms();
                    $Check_sms = $QuerySms->getTypeSms($school->id, 'fee_deposit');
                    if (is_object($Check_sms)):
                        $msg = get_database_msg_only('11', $earr1);
                        unset($POST['id']);
                        unset($POST['email_type']);
                        unset($POST['to_email']);
                        unset($POST['email_text']);
                        unset($POST['from_email']);
                        unset($POST['email_school_name']);
                        unset($POST['email_subject']);
                        $POST['school_id'] = $school->id;
                        $POST['sms_type'] = 'fee_deposit';
                        $POST['type'] = 'sms';
                        $POST['on_date'] = date('Y-m-d');
                        $POST['to_number'] = $parents->mobile;
                        $POST['sms_text'] = $msg;

                        $QuerySave1 = new schoolSmsEmailHistory();
                        $QuerySave1->saveData($POST);
                    endif;
                endif;

            endif;
        endif;
        break;

    default:break;
endswitch;
?>
