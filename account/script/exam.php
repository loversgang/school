<?php

/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsEmailClass.php');

$modName = 'exam';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_REQUEST['id']) ? $id = $_REQUEST['id'] : $id = '';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = 'A';
isset($_REQUEST['date']) ? $date = $_REQUEST['date'] : $date = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = 'select';
isset($_REQUEST['exam_id']) ? $exam_id = $_REQUEST['exam_id'] : $exam_id = '';
isset($_REQUEST['st_id']) ? $st_id = $_REQUEST['st_id'] : $st_id = '';
isset($_GET['doc_id']) ? $doc_id = $_GET['doc_id'] : $doc_id = '';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';

#Get exam Grades
$QueryGrOb = new examGrade();
$all_grade = $QueryGrOb->listAll($school->id);

#get_all current session 
$QuerySession = new session();
$QuerySession->listAllCurrent($school->id, '1');

#Get School staff
//$QueryStaff=new staff();
//$QueryStaff->getSchoolStaff($school->id,'1');
#Get School staff Category
$QueryStaff = new staffCategory();
$QueryStaff->listAll($school->id, '1');

/* Get school document template for student id card */
$QueryDocs = new SchoolDocumentTemplate();
$dmc = $QueryDocs->get_school_single_document($school->id, 3);

switch ($action):
    case'list':
        /* Get exams */
        $QueryObj = new examination();
        $result = $QueryObj->listAll($school->id, $session_id);
        /* Get exams sessions */
        $QuerySessObj = new examination();
        $Session_list = $QuerySessObj->getExamSession($school->id);
        

        break;

    case'insert':
        if ($type == 'select'):
            $per = '0%';
        elseif ($type == 'exam'):
            $per = '35%';
        elseif ($type == 'result'):
            $per = '70%';
        else:
            $per = '25%';
        endif;

        #get Session Info
        $QueryS = new session();
        $object = $QueryS->getRecord($id);

        if (is_object($object)):
            #Get Subject List
            $QuerySubject = new subject();
            $QuerySubject->listSessionSubject($object->compulsory_subjects, $object->elective_subjects);
        endif;

        #Get Section Students
        $QueryOb = new studentSession();
        $records = $QueryOb->sectionSessionStudents($id, $ct_sect);

        if (($type == 'exam' || $type == 'result') && empty($records)):
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_SESSION_EMPTY);
            Redirect(make_admin_url('exam', 'insert', 'insert'));
        endif;


        if ($exam_id):
            $QueryEx = new examination();
            $exam = $QueryEx->getRecord($exam_id);

            #Get subject info
            $QuerySu = new subject();
            $subject = $QuerySu->getRecord($exam->subject_id);

            #check the elective subject and its students
            if ($subject->type == 'Elective'):
                $QuerySub = new studentSubject();
                $records = $QuerySub->GetSubjectStudents($id, $exam->subject_id, $exam->section);
            endif;
        endif;

        if (isset($_POST['submit1'])):
            #Get subject info
            $QuerySu = new subject();
            $subject = $QuerySu->getRecord($_POST['subject_id']);

            #check the elective subject and its students
            if ($subject->type == 'Elective'):
                $QuerySub = new studentSubject();
                $sub_student = $QuerySub->GetSubjectStudents($_POST['session_id'], $_POST['subject_id'], $_POST['section']);

                if (empty($sub_student)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_SUBJECT_STUDENT_EMPTY);
                    Redirect(make_admin_url('exam', 'insert', 'insert&type=exam&id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section']));
                endif;
            endif;

            #Add Exam
            $QueryE = new examination();
            $exam_id = $QueryE->saveData($_POST);
            $admin_user->set_pass_msg(MSG_EXAM_ADDED);
            Redirect(make_admin_url('exam', 'insert', 'insert&id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section'] . '&exam_id=' . $exam_id . '&type=result'));
        elseif (isset($_POST['submit_marks'])):

            #Max Marks and not blank
            if (!empty($_POST['marks'])):

                foreach ($_POST['marks'] as $p_key => $p_value):
                    if (empty($p_value['marks_obtained'])):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg(MSG_EXAM_MARKS_NEED);
                        Redirect(make_admin_url('exam', 'insert', 'insert&id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section'] . '&exam_id=' . $exam_id . '&type=result'));
                    elseif ($p_value['marks_obtained'] > $p_value['max_mark']):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg(MSG_EXAM_MARKS_NOT_MAX);
                        Redirect(make_admin_url('exam', 'insert', 'insert&id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section'] . '&exam_id=' . $exam_id . '&type=result'));
                    endif;
                endforeach;
            endif;

            #Add Exam Marks
            $QueryEMarks = new examinationMarks();
            $QueryEMarks->saveData($_POST['marks']);

            ## examination Info
            $exam_info = get_object('student_examination', $exam_id);

            $earr1 = array();
            $earr1['SITE_NAME'] = $school->school_name;
            $earr1['PRINCIPAL_NAME'] = $school->school_head_name;
            $earr1['SCHOOL_NAME'] = $school->school_name;
            $earr1['CONTACT_PHONE'] = $school->school_phone;
            $earr1['CONTACT_EMAIL'] = $school->email_address;
            $earr1['EXAM_NAME'] = $exam_info->title;
            $earr1['EXAM'] = $exam_info->title;
            $earr1['TOTAL_MARKS'] = $exam_info->maximum_marks;

            foreach ($_POST['marks'] as $st_id => $e_mark):
                ## Parent Info
                $QueryPr = new query('student_family_details');
                $QueryPr->Where = "where student_id='" . $st_id . "'";
                $parents = $QueryPr->DisplayOne();

                ## Student Info
                $student = get_object('student', $st_id);

                $earr1['PARENTS'] = ucfirst($parents->father_name) . " & " . ucfirst($parents->mother_name);
                $earr1['NAME'] = ucfirst($student->first_name) . ' ' . $student->last_name;
                $earr1['OBTAIN'] = $e_mark['marks_obtained'];
                $earr1['MARKS'] = $e_mark['marks_obtained'];
                $earr1['POSITION'] = $e_mark['marks_obtained'];

                #Get Percentage
                $percentage = number_format(($e_mark['marks_obtained'] / $exam_info->maximum_marks) * 100, 2) . "%";

                #Get Grade
                if (!empty($all_grade)): $grade = 'None';
                    foreach ($all_grade as $kg => $kv):
                        if (($percentage >= $kv->minimum) && ($percentage < $kv->maximum)): $grade = $kv->title;
                        endif;
                    endforeach;
                else:
                    $grade = '';
                endif;
                $earr1['DIVISION'] = $grade;
                $earr1['GRADE'] = $grade;
                $earr1['PERCENTAGE'] = $percentage;

                #Send and Add Entry to email counter if set in setting
                if ($school_setting->is_email_allowed == '1'):
                    $QueryCountEmail = new schoolEmail();
                    $email_count = $QueryCountEmail->getMonthlyCount($school->id);
                    $QueryEmail = new schoolEmail();
                    $Check_email = $QueryEmail->getTypeEmail($school->id, 'exam_result');
                    if (is_object($Check_email)):
                        $msg1 = get_database_msg('16', $earr1);
                        $subject = get_database_msg_subject('16', $earr1);
                        unset($POST);
                        $POST['student_id'] = $st_id;
                        $POST['school_id'] = $school->id;
                        $POST['email_type'] = 'exam_result';
                        $POST['type'] = 'email';
                        $POST['on_date'] = date('Y-m-d');
                        $POST['email_subject'] = $subject;
                        $POST['to_email'] = $parents->email;
                        $POST['email_text'] = $msg1;
                        $POST['from_email'] = $school->email_address;
                        $POST['email_school_name'] = $school->school_name;

                        $QuerySave = new schoolSmsEmailHistory();
                        $QuerySave->saveData($POST);

                    endif;
                endif;

                #Send and Add Entry to sms counter if set in setting
                if ($school_setting->is_sms_allowed == '1'):
                    $QuerySms = new schoolSms();
                    $Check_sms = $QuerySms->getTypeSms($school->id, 'exam_result');
                    if (is_object($Check_sms)):
                        $msg = get_database_msg_only('17', $earr1);
                        unset($POST['id']);
                        unset($POST['email_type']);
                        unset($POST['to_email']);
                        unset($POST['email_text']);
                        unset($POST['from_email']);
                        unset($POST['email_school_name']);
                        unset($POST['email_subject']);
                        $POST['student_id'] = $st_id;
                        $POST['school_id'] = $school->id;
                        $POST['sms_type'] = 'exam_result';
                        $POST['type'] = 'sms';
                        $POST['on_date'] = date('Y-m-d');
                        $POST['to_number'] = $parents->mobile;
                        $POST['sms_text'] = $msg;

                        $QuerySave1 = new schoolSmsEmailHistory();
                        $QuerySave1->saveData($POST);
                    endif;
                endif;

            endforeach;

            $admin_user->set_pass_msg(MSG_EXAM_MARKS_ADDED);
            Redirect(make_admin_url('exam', 'list', 'list'));
        endif;
        break;

    case'update':
        $records = array();

        if ($type == 'select'):
            $per = '35%';
        elseif ($type == 'result'):
            $per = '100%';
        else:
            $per = '35%';
        endif;

        #get exam Info
        $QueryS = new examination();
        $exam = $QueryS->getRecord($id);

        if ($exam_id):
            $QueryEx = new examination();
            $exam = $QueryEx->getRecord($exam_id);

            #Get subject info
            $QuerySu = new subject();
            $subject = $QuerySu->getRecord($exam->subject_id);

            #Get Section Students
            $QueryOb = new examination();
            $records = $QueryOb->sectionSessionStudentsExam($id, $ct_sect, $exam_id);

            #get Session Info
            $QuerySession = new session();
            $object = $QuerySession->getRecord($exam->session_id);
        else:
            #get Session Info
            $QuerySession = new session();
            $object = $QuerySession->getRecord($exam->session_id);
        endif;

        if (is_object($object)):
            #Get Subject List
            $QuerySubject = new subject();
            $QuerySubject->listSessionSubject($object->compulsory_subjects, $object->elective_subjects);
        endif;

        if (isset($_POST['submit1'])):

            #Add Exam
            $QueryE = new examination();
            $exam_id = $QueryE->saveData($_POST);
            $admin_user->set_pass_msg(MSG_EXAM_UPDATE);
            Redirect(make_admin_url('exam', 'update', 'update&id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section'] . '&exam_id=' . $exam_id . '&type=result'));
        elseif (isset($_POST['submit_marks'])):
            #Max Marks and not blank
            if (!empty($_POST['marks'])):
                foreach ($_POST['marks'] as $p_key => $p_value):
                    if (empty($p_value['marks_obtained'])):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg(MSG_EXAM_MARKS_NEED);
                        Redirect(make_admin_url('exam', 'update', 'update&id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section'] . '&exam_id=' . $exam_id . '&type=result'));
                    elseif ($p_value['marks_obtained'] > $p_value['max_mark']):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg(MSG_EXAM_MARKS_NOT_MAX);
                        Redirect(make_admin_url('exam', 'update', 'update&id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section'] . '&exam_id=' . $exam_id . '&type=result'));
                    endif;

                endforeach;
            endif;

            #Add Exam Marks
            $QueryEMarks = new examinationMarks();
            $QueryEMarks->updateData($_POST['marks']);

            $admin_user->set_pass_msg(MSG_EXAM_MARKS_UPDATE);
            Redirect(make_admin_url('exam', 'list', 'list'));
        endif;
        break;

    case'view':
        #get exam Info
        $QueryS = new examination();
        $exam = $QueryS->getRecord($id);
        
        

        #get Session Info
        $QuerySession = new session();
        $object = $QuerySession->getRecord($exam->session_id);

        #Get exam Students result
        $QueryOb = new examination();
        $result = $QueryOb->examination_result($id);
        
        
        break;

    case'student':
        #get exam Info
        $QueryG = new examinationGroup();
        $group = $QueryG->getRecord($id);

        #get Session Info
        $QuerySession = new session();
        $object = $QuerySession->getRecord($group->session_id);

        #get Subject names
        $QueryExam = new examination();
        $exams = $QueryExam->examination_titles($group->exam_id);

        $all_exam = implode(', ', $exams);

        #Get exam Students result
        $QueryOb = new examinationMarks();
        $result = $QueryOb->getExamStudents($group->exam_id);
        break;



    case'result':
        #get exam Info
        $QueryG = new examinationGroup();
        $group = $QueryG->getRecord($id);

        #get Session Info
        $QuerySession = new session();
        $object = $QuerySession->getRecord($group->session_id);

        #Get document
        $QueryDoc = new document();
        $doc = $QueryDoc->getRecord($doc_id);

        #get Student detail	
        $QueryStu = new studentSession();
        $student = $QueryStu->SingleStudentsWithSession($_GET['st_id'], $group->session_id, $group->section);

        #get Student result
        $QueryRes = new examination();
        $Result = $QueryRes->single_student_all_examination_result($group->exam_id, $_GET['st_id']);
        #Get exam Grades
        $QueryGrOb = new examGrade();
        $all_grade = $QueryGrOb->listAll($school->id);

        $result_data = '';
        $minimum = '0';
        $obtain = '0';
        $maximum = '0';
        $sr = '1';
        if (!empty($Result)):
            $result_data.="<table style='width: 100%; text-align: center; line-height: 40px;'>
							<tr><th style='text-align:left;width:10%;border-bottom:1px solid;' >Sr. No.</th><th style='text-align: left; width: 30%;border-bottom:1px solid;'>Subject Name</th><th style='width:20%;border-bottom:1px solid;'>Passing Marks</th><th style='width:20%;border-bottom:1px solid;'>Minimum Obtained</th><th style='width:20%;border-bottom:1px solid;'>Maximum Marks</th></tr>
							";
            foreach ($Result as $r_k => $r_v):
                $result_data.="<tr><td style='text-align:left;width:10%;' >" . $sr . ".</td><td style='text-align: left; width: 30%;'>" . $r_v->subject . "</td><td style='width:20%;'>" . $r_v->minimum_marks . "</td><td style='width:20%;'>" . $r_v->marks_obtained . "</td><td style='width:20%;'>" . $r_v->maximum_marks . "</td></tr>";

                $minimum = $minimum + $r_v->minimum_marks;
                $obtain = $obtain + $r_v->marks_obtained;
                $maximum = $maximum + $r_v->maximum_marks;
                $sr++;
            endforeach;
            $result_data.="</table>";
        endif;

        #Get Percentage
        $percentage = number_format(($obtain / $maximum) * 100, 2) . "%";

        #Get Grade
        if (!empty($all_grade)): $grade = 'None';
            foreach ($all_grade as $kg => $kv):
                if (($percentage >= $kv->minimum) && ($percentage < $kv->maximum)): $grade = $kv->title;
                endif;
            endforeach;
        else:
            $grade = 'None';
        endif;

        $replace = array('SCHOOL' => $school->school_name,
            'TITLE' => $group->title,
            'NAME' => $student->first_name . " " . $student->last_name,
            'CLASS' => $student->session_name,
            'SECTION' => $student->section,
            'ROLL_NO' => $student->roll_no,
            'GENDER' => $student->sex,
            'MINIMUM_TOTAL' => $minimum,
            'OBTAIN_TOTAL' => $obtain,
            'MAXIMUM_TOTAL' => $maximum,
            'MARKS_RESULT' => $result_data,
            'PERCENTAGE' => $percentage,
            'GRADE' => $grade,
            'SESSION' => date('d, M Y', strtotime($object->start_date)) . ' - ' . date('d, M Y', strtotime($object->end_date))
        );

        $content = '';
        if (is_object($doc)):
            $content = $doc->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = '{' . trim(strtoupper($k)) . '}';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                    $content = str_replace('&nbsp;', '', $content);
                endforeach;
            endif;
        endif;

        break;

    case'confirm_print':
        #get exam Info
        $QueryS = new examination();
        $exam = $QueryS->getRecord($id);

        #get Session Info
        $QuerySession = new session();
        $object = $QuerySession->getRecord($exam->session_id);

        #get Student result
        $QueryRes = new examination();
        $Result = $QueryRes->single_student_examination_result($id, $_GET['st_id']);

        #Get document
        $QueryDoc = new document();
        $doc = $QueryDoc->getRecord($doc_id);

        #Get exam Grades
        $QueryGrOb = new examGrade();
        $all_grade = $QueryGrOb->listAll($school->id);

        #Get Percentage
        $percentage = number_format(($Result->marks_obtained / $Result->maximum_marks) * 100, 2) . "%";

        #Get Grade
        if (!empty($all_grade)): $grade = 'None';
            foreach ($all_grade as $kg => $kv):
                if (($percentage >= $kv->minimum) && ($percentage < $kv->maximum)): $grade = $kv->title;
                endif;
            endforeach;
        else:
            $grade = 'None';
        endif;

        #get Student detail	
        $QueryStu = new studentSession();
        $student = $QueryStu->SingleStudentsWithSession($_GET['st_id'], $exam->session_id, $exam->section);
        $subject = get_object('subject_master', $exam->subject_id);
        $replace = array('SCHOOL' => $school->school_name,
            'TITLE' => $exam->title,
            'NAME' => $student->first_name . " " . $student->last_name,
            'CLASS' => $student->session_name,
            'SECTION' => $student->section,
            'ROLL_NO' => $student->roll_no,
            'GENDER' => $student->sex,
            'MARKS_RESULT' => "<table style='width: 100%; text-align: center; line-height: 40px;'>
										<tr><th style='text-align:left;width:10%;border-bottom:1px solid;' >Sr. No.</th><th style='text-align: left; width: 30%;border-bottom:1px solid;'>Subject Name</th><th style='width:20%;border-bottom:1px solid;'>Passing Marks</th><th style='width:20%;border-bottom:1px solid;'>Minimum Obtained</th><th style='width:20%;border-bottom:1px solid;'>Maximum Marks</th></tr>
										<tr><td style='text-align:left;width:10%;' >1.</td><td style='text-align: left; width: 30%;'>" . $subject->name . "</td><td style='width:20%;'>" . $Result->minimum_marks . "</td><td style='width:20%;'>" . $Result->marks_obtained . "</td><td style='width:20%;'>" . $Result->maximum_marks . "</td></tr>		
										</table>",
            'MINIMUM_TOTAL' => $Result->minimum_marks,
            'OBTAIN_TOTAL' => $Result->marks_obtained,
            'MAXIMUM_TOTAL' => $Result->maximum_marks,
            'PERCENTAGE' => $percentage,
            'GRADE' => $grade,
            'SESSION' => date('d, M Y', strtotime($object->start_date)) . ' - ' . date('d, M Y', strtotime($object->end_date))
        );

        $content = '';
        if (is_object($doc)):
            $content = $doc->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = '{' . trim(strtoupper($k)) . '}';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                    $content = str_replace('&nbsp;', '', $content);
                endforeach;
            endif;
        endif;

        #Add record
        $_POST['student_id'] = $student->id;
        $_POST['session_id'] = $student->session_id;
        $_POST['section'] = $student->section;
        $_POST['school_id'] = $school->id;
        $_POST['document_id'] = $_GET['doc_id'];
        $_POST['doc_type'] = $doc->type;
        $_POST['doc_text'] = $content;
        $QuerySess = new SchoolDocumentPrintHistory();
        $QuerySess->saveData($_POST);

        $admin_user->set_pass_msg(MSG_DOCUMENT_GENERATETD);
        Redirect(make_admin_url('exam', 'print', 'print' . '&id=' . $id . '&st_id=' . $st_id . '&doc_id=' . $doc_id));

        break;


    case'confirm_print_group':
        #get exam Info
        $QueryG = new examinationGroup();
        $group = $QueryG->getRecord($id);

        #get Session Info
        $QuerySession = new session();
        $object = $QuerySession->getRecord($group->session_id);

        #Get document
        $QueryDoc = new document();
        $doc = $QueryDoc->getRecord($doc_id);

        #get Student detail	
        $QueryStu = new studentSession();
        $student = $QueryStu->SingleStudentsWithSession($_GET['st_id'], $group->session_id, $group->section);

        #get Student result
        $QueryRes = new examination();
        $Result = $QueryRes->single_student_all_examination_result($group->exam_id, $_GET['st_id']);

        #Get exam Grades
        $QueryGrOb = new examGrade();
        $all_grade = $QueryGrOb->listAll($school->id);

        $result_data = '';
        $minimum = '0';
        $obtain = '0';
        $maximum = '0';
        $sr = '1';
        if (!empty($Result)):
            $result_data.="<table style='width: 100%; text-align: center; line-height: 40px;'>
							<tr><th style='text-align:left;width:10%;border-bottom:1px solid;' >Sr. No.</th><th style='text-align: left; width: 30%;border-bottom:1px solid;'>Subject Name</th><th style='width:20%;border-bottom:1px solid;'>Passing Marks</th><th style='width:20%;border-bottom:1px solid;'>Minimum Obtained</th><th style='width:20%;border-bottom:1px solid;'>Maximum Marks</th></tr>
							";
            foreach ($Result as $r_k => $r_v):
                $result_data.="<tr><td style='text-align:left;width:10%;' >" . $sr . ".</td><td style='text-align: left; width: 30%;'>" . $r_v->subject . "</td><td style='width:20%;'>" . $r_v->minimum_marks . "</td><td style='width:20%;'>" . $r_v->marks_obtained . "</td><td style='width:20%;'>" . $r_v->maximum_marks . "</td></tr>";

                $minimum = $minimum + $r_v->minimum_marks;
                $obtain = $obtain + $r_v->marks_obtained;
                $maximum = $maximum + $r_v->maximum_marks;
                $sr++;
            endforeach;
            $result_data.="</table>";
        endif;

        #Get Percentage
        $percentage = number_format(($obtain / $maximum) * 100, 2) . "%";

        #Get Grade
        if (!empty($all_grade)): $grade = 'None';
            foreach ($all_grade as $kg => $kv):
                if (($percentage >= $kv->minimum) && ($percentage < $kv->maximum)): $grade = $kv->title;
                endif;
            endforeach;
        else:
            $grade = 'None';
        endif;

        $replace = array('SCHOOL' => $school->school_name,
            'TITLE' => $group->title,
            'NAME' => $student->first_name . " " . $student->last_name,
            'CLASS' => $student->session_name,
            'SECTION' => $student->section,
            'ROLL_NO' => $student->roll_no,
            'GENDER' => $student->sex,
            'MINIMUM_TOTAL' => $minimum,
            'OBTAIN_TOTAL' => $obtain,
            'MAXIMUM_TOTAL' => $maximum,
            'MARKS_RESULT' => $result_data,
            'PERCENTAGE' => $percentage,
            'GRADE' => $grade,
            'SESSION' => date('d, M Y', strtotime($object->start_date)) . ' - ' . date('d, M Y', strtotime($object->end_date))
        );

        $content = '';
        if (is_object($doc)):
            $content = $doc->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = '{' . trim(strtoupper($k)) . '}';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                    $content = str_replace('&nbsp;', '', $content);
                endforeach;
            endif;
        endif;

        #Add record
        $_POST['student_id'] = $student->id;
        $_POST['session_id'] = $student->session_id;
        $_POST['section'] = $student->section;
        $_POST['school_id'] = $school->id;
        $_POST['document_id'] = $_GET['doc_id'];
        $_POST['doc_type'] = $doc->type;
        $_POST['doc_text'] = $content;
        $QuerySess = new SchoolDocumentPrintHistory();
        $QuerySess->saveData($_POST);

        $admin_user->set_pass_msg(MSG_DOCUMENT_GENERATETD);
        Redirect(make_admin_url('exam', 'result', 'result' . '&id=' . $id . '&st_id=' . $st_id . '&doc_id=' . $doc_id));

        break;

    case'print':
        #get exam Info
        $QueryS = new examination();
        $exam = $QueryS->getRecord($id);

        #get Session Info
        $QuerySession = new session();
        $object = $QuerySession->getRecord($exam->session_id);

        #get Student result
        $QueryRes = new examination();
        $Result = $QueryRes->single_student_examination_result($id, $_GET['st_id']);

        #Get document
        $QueryDoc = new document();
        $doc = $QueryDoc->getRecord($doc_id);

        #Get exam Grades
        $QueryGrOb = new examGrade();
        $all_grade = $QueryGrOb->listAll($school->id);

        #Get Percentage
        $percentage = number_format(($Result->marks_obtained / $Result->maximum_marks) * 100, 2) . "%";

        #Get Grade
        if (!empty($all_grade)): $grade = 'None';
            foreach ($all_grade as $kg => $kv):
                if (($percentage >= $kv->minimum) && ($percentage < $kv->maximum)): $grade = $kv->title;
                endif;
            endforeach;
        else:
            $grade = 'None';
        endif;

        #get Student detail	
        $QueryStu = new studentSession();
        $student = $QueryStu->SingleStudentsWithSession($_GET['st_id'], $exam->session_id, $exam->section);
        $subject = get_object('subject_master', $exam->subject_id);
        $replace = array('SCHOOL' => $school->school_name,
            'TITLE' => $exam->title,
            'NAME' => $student->first_name . " " . $student->last_name,
            'CLASS' => $student->session_name,
            'SECTION' => $student->section,
            'ROLL_NO' => $student->roll_no,
            'GENDER' => $student->sex,
            'MARKS_RESULT' => "<table style='width: 100%; text-align: center; line-height: 40px;'>
										<tr><th style='text-align:left;width:10%;border-bottom:1px solid;' >Sr. No.</th><th style='text-align: left; width: 30%;border-bottom:1px solid;'>Subject Name</th><th style='width:20%;border-bottom:1px solid;'>Passing Marks</th><th style='width:20%;border-bottom:1px solid;'>Minimum Obtained</th><th style='width:20%;border-bottom:1px solid;'>Maximum Marks</th></tr>
										<tr><td style='text-align:left;width:10%;' >1.</td><td style='text-align: left; width: 30%;'>" . $subject->name . "</td><td style='width:20%;'>" . $Result->minimum_marks . "</td><td style='width:20%;'>" . $Result->marks_obtained . "</td><td style='width:20%;'>" . $Result->maximum_marks . "</td></tr>		
										</table>",
            'MINIMUM_TOTAL' => $Result->minimum_marks,
            'OBTAIN_TOTAL' => $Result->marks_obtained,
            'MAXIMUM_TOTAL' => $Result->maximum_marks,
            'PERCENTAGE' => $percentage,
            'GRADE' => $grade,
            'SESSION' => date('d, M Y', strtotime($object->start_date)) . ' - ' . date('d, M Y', strtotime($object->end_date))
        );

        $content = '';
        if (is_object($doc)):
            $content = $doc->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = '{' . trim(strtoupper($k)) . '}';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                    $content = str_replace('&nbsp;', '', $content);
                endforeach;
            endif;
        endif;


        break;


    case'group':
        /* Get exams Groups */
        $QueryObj = new examinationGroup();
        $result = $QueryObj->listAll($school->id, $session_id);

        /* Get exams sessions */
        $QuerySessObj = new examinationGroup();
        $Session_list = $QuerySessObj->getGroupExamSession($school->id);

        break;

    case'edit_group':
        #get examination group detail
        $QueryGroup = new examinationGroup();
        $ExamGroup = $QueryGroup->getRecord($id);


        #get Session Info
        $QuerySession = new session();
        $object = $QuerySession->getRecord($ExamGroup->session_id);


        /* Get exams */
        $QueryObj = new examination();
        $result = $QueryObj->listAllSessionSection($school->id, $ExamGroup->session_id, $ExamGroup->section);
        if (isset($_POST['submit'])):
            #Update Exam
            $QueryE = new examinationGroup();
            $QueryE->saveData($_POST);
            if ($_POST['send_sms'] == '1'):
                #Get exam Students result
                $QueryOb = new studentSession();
                $result = $QueryOb->sessionStudentsWithSessionOrSimple($ExamGroup->id, $ExamGroup->session_id, $ExamGroup->section);

                if (!empty($result)):
                    foreach ($result as $key => $value):
                        //Get Family Detail
                        $QueryPr = new query('student_family_details');
                        $QueryPr->Where = "where student_id='" . $value->id . "'";
                        $parents = $QueryPr->DisplayOne();

                        $earr1['PARENTS'] = ucfirst($parents->father_name) . " & " . ucfirst($parents->mother_name);
                        $earr1['NAME'] = ucfirst($value->first_name) . ' ' . $value->last_name;
                        $earr1['EXAM'] = $ExamGroup->title;
                        $earr1['SITE_NAME'] = $school->school_name;

                        #get Student result
                        $QueryRes = new examination();
                        $Result = $QueryRes->single_student_all_examination_result($ExamGroup->exam_id, $value->id);

                        #Send and Add Entry to email counter if set in setting
                        if ($school_setting->is_email_allowed == '1'):
                            $QueryEmail = new schoolEmail();
                            $Check_email = $QueryEmail->getTypeEmail($school->id, 'exam_result');
                            if (is_object($Check_email)):
                                #get exam result text for email
                                $QueryExRes = new examinationMarks();
                                $result_data = $QueryExRes->getExamResultText($Result);

                                $earr1['STATUS'] = $result_data;

                                $msg1 = get_database_msg('16', $earr1);
                                $subject = get_database_msg_subject('16', $earr1);

                                unset($POST);
                                $POST['school_id'] = $school->id;
                                $POST['email_type'] = 'exam_result';
                                $POST['type'] = 'email';
                                $POST['on_date'] = date('Y-m-d');
                                $POST['email_school_name'] = $school->school_name;
                                $POST['email_subject'] = $subject;
                                $POST['to_email'] = $parents->email;
                                $POST['email_text'] = $msg1;
                                $POST['student_id'] = $value->id;
                                $POST['from_email'] = $school->email_address;

                                $QuerySave = new schoolSmsEmailHistory();
                            //$QuerySave->saveData($POST);														
                            endif;
                        endif;

                        #Send and Add Entry to Sms counter if set in setting
                        if ($school_setting->is_sms_allowed == '1'):
                            $QuerySms = new schoolSms();
                            $Check_sms = $QuerySms->getTypeSms($school->id, 'exam_result');
                            if (is_object($Check_sms)):
                                #get exam result text for email
                                $QueryExRes = new examinationMarks();
                                $result_data1 = $QueryExRes->getExamResultTextMSG($Result);

                                $earr1['STATUS'] = $result_data1;
                                $msg = get_database_msg_only('17', $earr1);

                                unset($POST);
                                $POST['school_id'] = $school->id;
                                $POST['sms_type'] = 'exam_result';
                                $POST['type'] = 'sms';
                                $POST['on_date'] = date('Y-m-d');
                                $POST['to_number'] = $parents->mobile;
                                $POST['sms_text'] = $msg;
                                $POST['student_id'] = $value->id;

                                $QuerySave1 = new schoolSmsEmailHistory();
                                $QuerySave1->saveData($POST);

                            endif;
                        endif;
                    endforeach;
                endif;
            endif;

            $admin_user->set_pass_msg(MSG_EXAM_GROUP_UPDATE);
            Redirect(make_admin_url('exam', 'group', 'group'));
        endif;
        break;

    case'add_group':
        if ($type == 'select'):
            $per = '0%';
        elseif ($type == 'group'):
            $per = '35%';
        else:
            $per = '35%';
        endif;

        if ($id):
            #get Session Info
            $QuerySession = new session();
            $object = $QuerySession->getRecord($id);
        endif;


        if ($type == 'group' && empty($id)):
            #send back
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_SESSION_SECTION_NEED_SELECT);
            Redirect(make_admin_url('exam', 'add_group', 'add_group'));
        endif;


        /* Get exams */
        $QueryObj = new examination();
        $result = $QueryObj->listAllSessionSection($school->id, $id, $ct_sect);
        if (isset($_POST['submit'])):
            if (empty($_POST['exam'])):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_EXAM_NEED);
                Redirect(make_admin_url('exam', 'add_group', 'add_group&type=group&id=' . $_POST['session_id'] . '&ct_sect=' . $_POST['section']));
            endif;
            #Add Exam
            $QueryE = new examinationGroup();
            $new_exam = $QueryE->saveData($_POST);

            #get examination group detail
            $QueryGroup = new examinationGroup();
            $ExamGroup = $QueryGroup->getRecord($new_exam);

            if ($_POST['intimation_sms'] == '1'):
                #Get exam Students result
                $QueryOb = new studentSession();
                $result = $QueryOb->sessionStudentsWithSessionOrSimple($ExamGroup->id, $ExamGroup->session_id, $ExamGroup->section);

                if (!empty($result)):
                    foreach ($result as $key => $value):
                        //Get Family Detail
                        $QueryPr = new query('student_family_details');
                        $QueryPr->Where = "where student_id='" . $value->id . "'";
                        $parents = $QueryPr->DisplayOne();

                        $earr1['PARENTS'] = ucfirst($parents->father_name) . " & " . ucfirst($parents->mother_name);
                        $earr1['NAME'] = ucfirst($value->first_name) . ' ' . $value->last_name;
                        $earr1['EXAM'] = $ExamGroup->title;
                        $earr1['START_DATE'] = $ExamGroup->from_date;
                        $earr1['END_DATE'] = $ExamGroup->to_date;
                        $earr1['SITE_NAME'] = $school->school_name;

                        #get Student result
                        $QueryRes = new examination();
                        $Result = $QueryRes->single_student_all_examination_result($ExamGroup->exam_id, $value->id);


                        #Send and Add Entry to Sms counter if set in setting
                        if ($school_setting->is_sms_allowed == '1'):
                            $QuerySms = new schoolSms();
                            $Check_sms = $QuerySms->getTypeSms($school->id, 'exam_result');
                            if (is_object($Check_sms)):
                                #get exam result text for email

                                $msg = get_database_msg_only('18', $earr1);

                                unset($_POST);
                                $_POST['school_id'] = $school->id;
                                $_POST['sms_type'] = 'exam_result';
                                $_POST['type'] = 'sms';
                                $_POST['on_date'] = date('Y-m-d');
                                $_POST['to_number'] = $parents->mobile;
                                $_POST['sms_text'] = $msg;
                                $_POST['student_id'] = $value->id;

                                $QuerySave1 = new schoolSmsEmailHistory();
                                $QuerySave1->saveData($_POST);

                            endif;
                        endif;
                    endforeach;
                endif;

            endif;


            $admin_user->set_pass_msg(MSG_EXAM_GROUP_ADDED);
            Redirect(make_admin_url('exam', 'group', 'group'));
        endif;
        break;

    case'delete_group':
        #delete exam examinationGroup 			
        $QueryObj = new examinationGroup();
        $QueryObj->deleteRecord($id);

        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('exam', 'group', 'group'));
        break;
    case'delete':
        #delete exam 			
        $QueryObj = new examination();
        $QueryObj->deleteRecord($id);

        #delete all result of exam
        $QuerySHeads = new examinationMarks();
        $QuerySHeads->deleteExamResults($id);

        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('exam', 'list', 'list'));
        break;
        break;

    default:break;
endswitch;
?>
