<?php

include_once(DIR_FS_SITE . 'include/functionClass/schoolEventClass.php');
$modName = 'event_cat';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';

#handle actions here.
switch ($action):
    case'list':
        $obj = new eventCat;
        $cats = $obj->listAllCats();
        break;
    case'insert':
        if (isset($_POST['submit'])) {
            $obj = new eventCat;
            $obj->saveEventCat($_POST);
            $admin_user->set_pass_msg('Event Category Added Successfully!');
            Redirect(make_admin_url('event_cat', 'list', 'list'));
        }
        break;
    case'update':
        $obj = new eventCat;
        $cat = $obj->getRecord($id);

        if (isset($_POST['submit'])) {
            $obj = new eventCat;
            $obj->saveEventCat($_POST);
            $admin_user->set_pass_msg('Event Category Updated Successfully!');
            Redirect(make_admin_url('event_cat', 'list', 'list'));
        }
        break;
    case'delete':
        $obj = new eventCat;
        $obj->id = $id;
        $obj->Delete();
        $admin_user->set_pass_msg("Event Category Deleted Successfully!");
        Redirect(make_admin_url('event_cat', 'list', 'list'));
        break;
    default:
        break;
endswitch;
