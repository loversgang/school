<?php

set_time_limit(1000000000000);
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsEmailClass.php');

$modName = 'attendance';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_REQUEST['id']) ? $id = $_REQUEST['id'] : $id = '';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';

isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = '';
isset($_REQUEST['date']) ? $date = $_REQUEST['date'] : $date = '';
isset($_REQUEST['atten_date']) ? $atten_date = $_REQUEST['atten_date'] : $atten_date = date('Y-m-d');
isset($_REQUEST['year']) ? $year = $_REQUEST['year'] : $year = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = 'select';
isset($_REQUEST['select']) ? $select = $_REQUEST['select'] : $select = 'single';


if (!empty($date)):
    $pos = strrpos($date, "-");
    if ($pos == true):
        $m_y = explode('-', $date);
        $date = $m_y['0'];
        $year = $m_y['1'];
    endif;
endif;

#get_all current session 
$QuerySession = new session();
$QuerySession->listAllCurrent($school->id, '1');

#date range for attendance
$current_date = date('d M Y');
$start = date("d M Y", strtotime("-3 days", strtotime($current_date)));
$to_date = date("d M Y", strtotime("+3 days", strtotime($current_date)));


switch ($action):
    case 'list':

        #get_all session 
        $QuerySession = new session();
        $QuerySession->listAll($school->id, '1');

        if ($type == 'select'):
            $per = '0%';
        elseif ($type == 'session'):
            $per = '35%';
        elseif ($type == 'attendance'):
            $per = '70%';
        else:
            $per = '25%';
        endif;

        #get Session Info
        $QueryS = new session();
        $object = $QueryS->getRecord($session_id);

        #count working days
        $working_days = '';
        if (!empty($date) && !empty($year)):
            $first_date_of_month = $year . "-" . $date . "-01";
            $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($year . "-" . $date . "-1"))));

            $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
            $total_month_days = (date('d', strtotime($last_date_of_month)));
            $working_days = $total_month_days - $total_sunday;
        endif;



        if ($select == 'multiple'):
            if ($type == 'attendance' && empty($_GET['session_id'])):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_SESSION_SECTION_NEED_SELECT);
                Redirect(make_admin_url('attendance', 'list', 'list&select=multiple&type=session'));
            else:
                #Get Section Students
                $QueryOb = new studentSession();
                $records = $QueryOb->sectionSessionStudents($session_id, $ct_sect);
            endif;
        else:

            if ($select == 'single'):
                if ($type == 'attendance' && !isset($_GET['st_id'])):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_STUDENT_NEED_SELECT);
                    Redirect(make_admin_url('attendance', 'list', 'list&select=single&type=session'));
                elseif (isset($_GET['st_id'])):
                    #Get Section Students
                    $QueryOb = new studentSession();
                    $records = $QueryOb->sectionSessionSingleStudentsWithAttendance($_GET['st_id']);
                endif;
            else:
                #Get Section Students
                $QueryOb = new studentSession();
                $records = $QueryOb->sectionSessionStudents($session_id, $ct_sect);
            endif;

        endif;


        break;

    case'insert':
        if ($type == 'select'):
            $per = '0%';
        elseif ($type == 'section'):
            $per = '25%';
        elseif ($type == 'date'):
            $per = '50%';
        elseif ($type == 'attendance'):
            $per = '75%';
        else:
            $per = '25%';
        endif;

        #get Session Info
        $QueryS = new session();
        $object = $QueryS->getRecord($session_id);

        /* Add attendance */
        if (isset($_POST['multi_select'])):
            if (isset($_POST['attendance']) && count($_POST['attendance']) > 0):

                #Add Multiple attendance into Session
                $QuerySess = new attendance();
                $QuerySess->saveData($_POST['attendance']);

                #Check the Email/Sms Subscribe and send them
                foreach ($_POST['attendance'] as $key => $p_value):

                    $s_info = get_object('student', $p_value['student_id']);
                    /* Get Parents Deatil */
                    $QueryPr = new query('student_family_details');
                    $QueryPr->Where = "where student_id='" . $s_info->id . "'";
                    $parents = $QueryPr->DisplayOne();

                    $earr1['PARENTS'] = ucfirst($parents->father_name) . " & " . ucfirst($parents->mother_name);
                    $earr1['NAME'] = ucfirst($s_info->first_name) . ' ' . $s_info->last_name;
                    $earr1['DATE'] = $p_value['date'];
                    $earr1['PRINCIPAL_NAME'] = $school->school_head_name;
                    $earr1['SCHOOL_NAME'] = $school->school_name;
                    $earr1['CONTACT_PHONE'] = $school->school_phone;
                    $earr1['CONTACT_EMAIL'] = $school->email_address;

                    $earr1['SITE_NAME'] = $school->school_name;
                    #Send and Add Entry to email counter if set in setting
                    unset($_POST['sms_text']);
                    if ($school_setting->is_email_allowed == '1'):
                        if ($p_value['status'] == "P"):
                            $earr1['STATUS'] = "Present";
                            $QueryEmail = new schoolEmail();
                            $Check_email = $QueryEmail->getTypeEmail($school->id, 'present');

                            if (is_object($Check_email)):
                                $msg1 = get_database_msg('5', $earr1);
                                $subject = get_database_msg_subject('5', $earr1);
                                unset($_POST['id']);
                                unset($_POST['student_id']);
                                $_POST['school_id'] = $school->id;
                                $_POST['email_type'] = 'present';
                                $_POST['type'] = 'email';
                                $_POST['on_date'] = date('Y-m-d');
                                $_POST['email_school_name'] = $school->school_name;
                                $_POST['email_subject'] = $subject;
                                $_POST['to_email'] = $parents->email;
                                $_POST['email_text'] = $msg1;
                                $_POST['student_id'] = $p_value['student_id'];
                                $_POST['from_email'] = $school->email_address;

                                $QuerySave = new schoolSmsEmailHistory();
                                $QuerySave->saveData($_POST);
                            endif;
                        elseif ($p_value['status'] == "A"):
                            $earr1['STATUS'] = "Absent";
                            $QueryEmail = new schoolEmail();
                            $Check_email = $QueryEmail->getTypeEmail($school->id, 'absent');
                            if (is_object($Check_email)):
                                $msg1 = get_database_msg('5', $earr1);
                                $subject = get_database_msg_subject('5', $earr1);
                                unset($_POST['id']);
                                unset($_POST['student_id']);
                                $_POST['school_id'] = $school->id;
                                $_POST['email_type'] = 'absent';
                                $_POST['type'] = 'email';
                                $_POST['on_date'] = date('Y-m-d');
                                $_POST['email_school_name'] = $school->school_name;
                                $_POST['email_subject'] = $subject;
                                $_POST['to_email'] = $parents->email;
                                $_POST['email_text'] = $msg1;
                                $_POST['student_id'] = $p_value['student_id'];
                                $_POST['from_email'] = $school->email_address;

                                $QuerySave = new schoolSmsEmailHistory();
                                $QuerySave->saveData($_POST);
                            endif;
                        endif;
                    endif;

                    #Send and Add Entry to Sms counter if set in setting
                    if ($school_setting->is_sms_allowed == '1'):
                        if ($p_value['status'] == "P"):
                            $earr1['STATUS'] = "Present";
                            $QuerySms = new schoolSms();
                            $Check_sms = $QuerySms->getTypeSms($school->id, 'present');
                            if (is_object($Check_sms)):
                                $msg = get_database_msg_only('12', $earr1);

                                unset($_POST['id']);
                                unset($_POST['email_type']);
                                unset($_POST['to_email']);
                                unset($_POST['email_text']);
                                unset($_POST['student_id']);
                                unset($_POST['from_email']);
                                unset($_POST['email_school_name']);
                                unset($_POST['email_subject']);
                                $_POST['school_id'] = $school->id;
                                $_POST['sms_type'] = 'present';
                                $_POST['type'] = 'sms';
                                $_POST['on_date'] = date('Y-m-d');
                                $_POST['to_number'] = $parents->mobile;
                                $_POST['sms_text'] = $msg;
                                $_POST['student_id'] = $p_value['student_id'];

                                $QuerySave1 = new schoolSmsEmailHistory();
                                $QuerySave1->saveData($_POST);

                            endif;
                        elseif ($p_value['status'] == "A"):
                            $earr1['STATUS'] = "Absent";
                            $QuerySms = new schoolSms();
                            $Check_sms = $QuerySms->getTypeSms($school->id, 'absent');
                            if (is_object($Check_sms)):
                                $msg = get_database_msg_only('12', $earr1);
                                unset($_POST['id']);
                                unset($_POST['email_type']);
                                unset($_POST['to_email']);
                                unset($_POST['email_text']);
                                unset($_POST['student_id']);
                                $_POST['school_id'] = $school->id;
                                $_POST['sms_type'] = 'absent';
                                $_POST['type'] = 'sms';
                                $_POST['on_date'] = date('Y-m-d');
                                $_POST['to_number'] = $parents->mobile;
                                $_POST['sms_text'] = $msg;
                                $_POST['student_id'] = $p_value['student_id'];

                                $QuerySave1 = new schoolSmsEmailHistory();
                                $QuerySave1->saveData($_POST);

                            endif;
                        endif;
                    endif;
                endforeach;

                $admin_user->set_pass_msg($object->title . " Section " . $ct_sect . " " . MSG_ATTENDANCE_ADDED);
                Redirect(make_admin_url('attendance', 'insert', 'insert'));
            else:
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_SELECT_MULTI_STUDENT);
                Redirect(make_admin_url('attendance', 'insert', 'insert&id=' . $session_id . '&ct_sect=' . $ct_sect . '&atten_date=' . $atten_date));
            endif;
        endif;

        #if blank session id
        if ($type == 'attendance' && (strtotime($atten_date) < strtotime($object->start_date) || strtotime($atten_date) > strtotime($object->end_date))):
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_SESSION_SECTION_MORE_DATE);
            Redirect(make_admin_url('attendance', 'insert', 'insert'));
        endif;

        #if date select less then session or more then session
        if ($type == 'attendance' && empty($session_id)):
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_SESSION_SECTION_NEED_SELECT);
            Redirect(make_admin_url('attendance', 'insert', 'insert'));
        endif;

        #check if Sunday or holiday
        if ($type == 'attendance'):
            #check Sunday
            $sunday = CheckSunday($atten_date);

            #check Holiday
            $M_obj = new schoolHolidays();
            $holiday = $M_obj->checkHoliday($school->id, $atten_date);

            if ($sunday == 'Sunday'):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_HOLIDAY_DATE_SUNDAY);
                Redirect(make_admin_url('attendance', 'insert', 'insert'));
            elseif ($holiday == 'Holiday'):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_HOLIDAY_DATE);
                Redirect(make_admin_url('attendance', 'insert', 'insert'));
            endif;
        endif;

        /* Get list of section in this session Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);
        $total_section = $QuerySec->GetNumRows();


        #check this date section
        $QueryCheckAtt = new attendance();
        $CheckAtt = $QueryCheckAtt->checkCurrentDateAttendace($session_id, $ct_sect, $atten_date);
        if ($CheckAtt):
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_ATTENDANCE_ALREDY_ADDED);

            #Get Section Students
            $QueryOb = new studentSession();
            $records = $QueryOb->sectionSessionStudentsWithAttendance($session_id, $ct_sect, $atten_date);
        else:
            #Get Section Students
            $QueryOb = new studentSession();
            $records = $QueryOb->sectionSessionStudents($session_id, $ct_sect);
        endif;

        if (($type == 'attendance') && empty($records)):
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_SESSION_EMPTY);
            Redirect(make_admin_url('attendance', 'insert', 'insert'));
        endif;

        break;

    case'view':
        #get Session Info
        $QueryS = new session();
        $object = $QueryS->getRecord($session_id);

        /* Get session Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);
        $total_section = $QuerySec->GetNumRows();
        if ($total_section > 1):
            #Get Section Students
            $QueryOb = new studentSession();
            $records = $QueryOb->sectionSessionStudents($session_id, $ct_sect);
        else:
            #Get All Session Students without Section 
            $QueryOb = new studentSession();
            $records = $QueryOb->sessionStudents($session_id);
        endif;
        break;

    default:break;
endswitch;
?>