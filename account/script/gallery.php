<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/galleryClass.php');

$modName='gallery';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
$p=isset($_GET['p'])?$_GET['p']:1;
$max_records='8';/*set maximum records on a page*/
/*get logged in user*/
$user_id=LOGIN_USER_ID;
                
switch ($action):
	case'list':
				$QueryObj = new gallery();
                 $QueryObj->enablePaging($p, $max_records);
				 $QueryObj->listSchoolGalleryImages($school->id);
                 $total_records= $QueryObj->TotalRecords;
                 $total_pages= $QueryObj->TotalPages;
                 
		 break;
	case'insert':
		 if(isset($_POST['submit'])):
                    $QueryObj = new gallery();
                    $getID=$QueryObj->saveImage($_POST);
                    $admin_user->set_pass_msg(MSG_GALLERY_ADDED);
					Redirect(make_admin_url('gallery', 'list', 'list'));
         elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();    
                    $admin_user->set_pass_msg(OPERATION_CANCEL);
                    Redirect(make_admin_url('gallery', 'list', 'list'));     
		 endif;
                break;
     
	case'update':
				$QueryObj = new gallery();
				$gallery=$QueryObj->getImage($id); 
                               
		if(isset($_POST['submit'])):
                    $QueryObj = new gallery();
                    $getID=$QueryObj->saveImage($_POST);
                    $admin_user->set_pass_msg(MSG_GALLERY_UPDATE);
					Redirect(make_admin_url('gallery', 'list', 'list'));
		elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg(OPERATION_CANCEL);
                    Redirect(make_admin_url('gallery', 'list', 'list'));     
	        endif;   
		break;

	case'delete':
			$QueryObj = new gallery();
			$QueryObj->deleteImage($id);
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('gallery', 'list', 'list'));
		break;
		
        case 'delete_image':
                if($id){
                    $object= get_object('gallery', $id);
					$QueryGal = new gallery();
                    $QueryGal->deleteOnlyImage($id);

                    #delete images from all folders
                    $image_obj=new imageManipulation();
                    $image_obj->DeleteImagesFromAllFolders('gallery',$object->image);
               
                    /*End Clear Cache code*/
                    $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
                    Redirect(make_admin_url('gallery', 'update', 'update','id='.$id));
                }
                


        case'thrash':
            $QueryObj = new gallery();
            $QueryObj->setUserId($user_id);
            $QueryObj->getThrash();
            break;
	default:break;
endswitch;
?>
