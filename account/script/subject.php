<?php

include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';

isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['type']) ? $type = $_GET['type'] : $type = '';
$modName = 'subject';
#handle actions here.
switch ($action):
    case'list':
        # subjects records
        $QueryObj = new subject;
        $QueryObj->listAll($school->id);

        break;

    case'insert':
        # Add subject
        if (isset($_POST['submit'])):
            $QueryObj = new subject;
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_SUBJECT_ADDED);
            Redirect(make_admin_url('subject', 'list', 'list'));
        endif;
        break;

    case'update':
        # edit subject
        $QuObj = new subject;
        $object = $QuObj->getRecord($id);
        if (isset($_POST['submit'])):
            $QueryObj = new subject;
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_SUBJECT_UPDATE);
            Redirect(make_admin_url('subject', 'list', 'list'));
        endif;
        break;


    case'delete':
        $QueryObj = new subject();
        $QueryObj->deleteRecord($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('subject', 'list', 'list'));

        break;

    default:break;

endswitch;
?>

