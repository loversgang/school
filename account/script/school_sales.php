<?php

include_once(DIR_FS_SITE . 'include/functionClass/school_salesClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');

$modName = 'school_sales';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['ct_veh']) ? $ct_veh = $_REQUEST['ct_veh'] : $ct_veh = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['stoppage_id']) ? $stoppage_id = $_REQUEST['stoppage_id'] : $stoppage_id = '';
isset($_REQUEST['vehicle_id']) ? $vehicle_id = $_REQUEST['vehicle_id'] : $vehicle_id = '';

switch ($action):
    case'list':
        $query = new school_sales;
        $query->Where = "ORDER BY `id` DESC ";
        $lists = $query->listschool_sales();

        $query = new school_sales;
        $lists2 = $query->school_sales_category_data_using_without_id();

        if (isset($_POST['submit'])) {
            foreach ($_POST['stock'] as $key => $stock) {
                $query = new school_sales;
                $query->Data['id'] = $key;
                $query->Data['stock'] = $stock;
                $query->Update();
            }
            foreach ($_POST['stock_left'] as $key => $stock_left) {
                $query = new school_sales;
                $query->Data['id'] = $key;
                $query->Data['stock_left'] = $stock_left;
                $query->Update();
            }
            foreach ($_POST['stock_available'] as $key => $stock_available) {
                $query = new school_sales;
                $query->Data['id'] = $key;
                $query->Data['stock_available'] = $stock_available;
                $query->Update();
            }
            $admin_user->set_pass_msg('Stock inserted successfully');
            Redirect(make_admin_url('school_sales', 'list', 'list'));
        }
        break;
    case'insert':
        $query = new school_sales;
        $category_lists = $query->school_sales_category_data();

        if (isset($_POST['submit'])) {
            $query = new school_sales;
            $_POST['stock_left'] = $_POST['stock_available'];
            $query->saveData($_POST);
            $admin_user->set_pass_msg('Sales inserted successfully');
            Redirect(make_admin_url('school_sales', 'list', 'list'));
        }
        break;
    case'update':

        if ($id == false) {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Id not found');
            Redirect(make_admin_url('school_sales', 'list', 'list'));
        }

        $checkId = school_sales :: checkId($id);

        if ($checkId) {
            $query = new school_sales();
            $query->Where = "WHERE `id` = $id";
            $list = $query->DisplayOne();
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg('Id is incorrect');
            Redirect(make_admin_url('school_sales', 'list', 'list'));
        }

        $query = new school_sales();
        $sales = $query->school_sales_category_data_using_id($id);
        $sales_cat_id = $sales->sales_cat_id;

        $query = new school_sales;
        $query->TableName = "school_sales_category";
        $query->Where = "WHERE `is_active` = 1   ORDER BY `id` DESC";
        $category_lists = $query->ListOfAllRecords();

        if (isset($_POST['submit'])) {
            $query = new school_sales;
            $query->Where = "WHERE `id` = '$id'";
            $list = $query->DisplayOne();

            $query = new school_sales;
            $query->saveData($_POST, $list->file);
            $admin_user->set_pass_msg('Sales updated successfully');
            Redirect(make_admin_url('school_sales', 'list', 'list'));
        }
        break;

    case'delete':
        $query = new school_sales;
        $query->Where = "WHERE `id` = '$id'";
        $list = $query->DisplayOne();

        $query = new school_sales;
        $query->delete_school_sales($id, $list->file);
        $admin_user->set_pass_msg('Sales Deleted successfully');
        Redirect(make_admin_url('school_sales', 'list', 'list'));
        break;

    default:break;
endswitch;
?>