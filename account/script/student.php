<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsEmailClass.php');

$modName = 'student';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['type']) ? $type = $_GET['type'] : $type = 'student_detail';
isset($_REQUEST['s_id']) ? $s_id = $_REQUEST['s_id'] : $s_id = '';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';

if ($s_id != '0' && $id == '0'):
    $id = $s_id;
endif;

#check for the same school student
if (!empty($id)):
    $student = get_object('student', $id);
    if ($student->school_id != $school->id):
        Redirect(make_admin_url('error', 'list', 'list'));
    endif;
endif;


#get_all course 
$QueryCourse = new course();
$QueryCourse->listAll($school->id, '1');

#get_all session 
$QuerySession = new session();
$QuerySession->listAll($school->id, '1');

#get All Active Vehicle List
$QueryVeh = new vehicle();
$QueryVeh->listAll($school->id, '1');

$QueryObjQ = new session();
$AllSessYear = $QueryObjQ->listOfYearSess($school->id);

if (empty($sess_int)):
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
endif;
#handle actions here.
switch ($action):
    case'list':
        #get_all current session 
        $QuerySession = new session();
        $All_Session = $QuerySession->listAllCurrent($school->id, '1', 'array');

        /* Set First time session id */
        if (empty($session_id) && !empty($All_Session)):
            $session_id = $All_Session['0']->id;
        endif;

        /* Get section Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);

        #Get Section Students
        $QueryOb = new studentSession();
        $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);

        $QueryObj = new student();
        $school_students = $QueryObj->getSchoolStudentsWithParents($school->id);
        break;
    case'insert':
        #get Prefix for new student
        $QueryNewPre = new student();
        $prefix_number = $QueryNewPre->getStudentPrefix($school->id);
        $prefix = $school_setting->reg_id_prefix . '-' . $prefix_number;

        #get student info
        $QueryInfo = new student();
        $s_info = $QueryInfo->getStudent($s_id);

        #get student permanent address
        $QueryP = new studentAddress();
        $s_p_ad = $QueryP->getStudentAddressByType($s_id, 'permanent');

        #get student correspondence address
        $QueryC = new studentAddress();
        $s_c_ad = $QueryC->getStudentAddressByType($s_id, 'correspondence');

        #get student correspondence address
        $QueryF = new studentFamily();
        $s_f = $QueryF->getStudentFamilyDeatils($s_id);

        #get student Sibling Info
        $QueryS = new studentSibling();
        $QueryS->getStudentSibling($s_id);

        #get student Previous School Info
        $P_school = new studentPreviousSchool();
        $P_school->getStudentPreviousSchool($s_id);

        #get student Document Info
        $S_doc = new studentDocument();
        $S_doc->getStudentDocument($s_id);

        #get current session
        $Ad_stu = new studentSession();
        $ct_addmission = $Ad_stu->checkStudentInSession($s_id);

        if ($type == 'vehicle'):
            #get session info
            $session_info = get_object('session', $session_id);

            #get Vehicle Info
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($s_id, $session_id, $ct_sec);

            #get Vehicle student info
            $QueryChkStu = new vehicleStudent();
            $VehstudentAdd = $QueryChkStu->checkVehicleStudentAssign($session_id, $ct_sec, $s_id);
            if (is_object($VehstudentAdd)):
            //$admin_user->set_error();
            //$admin_user->set_pass_msg(MSG_VEHICLE_SINGLE_STUDENT_ALREDY_ADDED);
            endif;
        endif;

        if (isset($_POST['submit'])):
            if ($type == 'student_detail'):

                #Check Student reg_id
                $QueryCheckReg = new student();
                $reg_check = $QueryCheckReg->cehck_reg_id($school->id, $_POST['reg_id']);

                if (!isset($_POST['id'])):
                    if (is_object($reg_check)):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg(MSG_REG_ID_EXIST);
                        return false;
                    //Redirect(make_admin_url('student', 'insert', 'insert'));
                    endif;
                endif;
                #Add Basic Detail of student
                $QueryObj1 = new student();
                $student_id = $QueryObj1->saveData($_POST);

                #Generate Student Login ID & Password
                // Generate Unique Password
                $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMONOPQRSTUVWXYZ0123456789';
                $password = '';
                for ($i = 0; $i < 10; $i++) {
                    $password .= $characters[rand(0, strlen($characters) - 1)];
                }
                $arr['id'] = $student_id;
                $arr['student_login_id'] = $school_setting->reg_id_prefix . 'STD' . $student_id;
                $arr['password'] = $password;
                $obj = new student;
                $obj->saveStudentDetails($arr);

                #Add blank entry to student family detail
                $QueryObj2 = new studentFamily();
                $QueryObj2->add_blank_entry($student_id);


                $admin_user->set_pass_msg(MSG_STUDENT_DETAIL_ADDED);
                Redirect(make_admin_url('student', 'insert', 'insert&type=address&s_id=' . $student_id));
            elseif (empty($s_id)):
                $admin_user->set_pass_msg(MSG_STUDENT_ID_REQUIRE);
                Redirect(make_admin_url('student', 'insert', 'insert&type=student_detail'));

            elseif ($type == 'address'):#Add permanent address of student
                $QueryObj1 = new studentAddress();
                $QueryObj1->saveData($_POST['permanent']);

                #Add correspondence address of student
                $QueryCor = new studentAddress();
                $QueryCor->saveData($_POST['correspondence']);

                $admin_user->set_pass_msg(MSG_STUDENT_ADDRESS_ADDED);
                Redirect(make_admin_url('student', 'insert', 'insert&type=family&s_id=' . $s_id));

            elseif ($type == 'family'):
                #get student correspondence address
                $QueryFm = new studentFamily();
                $s_fm = $QueryFm->getStudentFamilyDeatils($_POST['student_id']);
                if (is_object($s_fm)):
                    $_POST['family']['id'] = $s_fm->id;
                endif;
                #Add family detail of student				
                $QueryObj1 = new studentFamily();
                $QueryObj1->saveData($_POST['family']);

                #Add sibling inf of student
                $QuerySib = new studentSibling();
                $QuerySib->saveData($_POST['sibling'], $s_id);


                #get student info
                $QueryInfo = new student();
                $s_info = $QueryInfo->getStudent($_POST['student_id']);

                /* Now Confirmation Email */
                $earr1 = array();
                $earr1['NAME'] = ucfirst($s_info->first_name) . ' ' . $s_info->last_name;
                $earr1['REG_ID'] = $s_info->reg_id;
                $earr1['SITE_NAME'] = $school->school_name;
                $earr1['PRINCIPAL_NAME'] = $school->school_head_name;
                $earr1['SCHOOL_NAME'] = $school->school_name;
                $earr1['CONTACT_PHONE'] = $school->school_phone;
                $earr1['CONTACT_EMAIL'] = $school->email_address;

                #Send and Add Entry to email counter if set in setting
                if ($school_setting->is_email_allowed == '1'):
                    $QueryCountEmail = new schoolEmail();
                    $email_count = $QueryCountEmail->getMonthlyCount($school->id);
                    if ($email_count < $school_setting->max_email_limit):
                        $QueryEmail = new schoolEmail();
                        $Check_email = $QueryEmail->getTypeEmail($school->id, 'admission');
                        if (is_object($Check_email)):
                            $msg1 = get_database_msg('6', $earr1);
                            $subject = get_database_msg_subject('6', $earr1);

                            $_POST['school_id'] = $school->id;
                            $_POST['email_type'] = 'admission';
                            $_POST['type'] = 'email';
                            $_POST['on_date'] = date('Y-m-d');
                            $_POST['email_school_name'] = $school->school_name;
                            $_POST['email_subject'] = $subject;
                            $_POST['to_email'] = $_POST['family']['email'];
                            $_POST['email_text'] = $msg1;
                            $_POST['student_id'] = $s_info->id;
                            $_POST['from_email'] = $school->email_address;

                            $QuerySave1 = new schoolSmsEmailHistory();
                            $QuerySave1->saveData($_POST);

                        endif;
                    endif;
                endif;

                #Send and Add Entry to sms counter if set in setting
                if ($school_setting->is_sms_allowed == '1'):
                    $QuerySms = new schoolSms();
                    $Check_sms = $QuerySms->getTypeSms($school->id, 'admission');
                    if (is_object($Check_sms)):
                        $msg = get_database_msg_only('13', $earr1);
                        $msg = $msg;
                        unset($_POST['id']);
                        unset($_POST['email_type']);
                        unset($_POST['to_email']);
                        unset($_POST['email_text']);
                        unset($_POST['from_email']);
                        unset($_POST['email_school_name']);
                        unset($_POST['email_subject']);
                        $_POST['school_id'] = $school->id;
                        $_POST['sms_type'] = 'admission';
                        $_POST['type'] = 'sms';
                        $_POST['on_date'] = date('Y-m-d');
                        $_POST['to_number'] = $_POST['family']['mobile'];
                        $_POST['sms_text'] = $msg;

                        $QuerySave1 = new schoolSmsEmailHistory();
                        $QuerySave1->saveData($_POST);

                    endif;
                endif;


                $admin_user->set_pass_msg(MSG_STUDENT_FAMILY_ADDED);
                Redirect(make_admin_url('student', 'insert', 'insert&type=previous_school&s_id=' . $s_id));
            elseif ($type == 'previous_school'):
                #Add Previous School detail of student				
                $QueryObj1 = new studentPreviousSchool();
                $QueryObj1->saveData($_POST['school'], $s_id);

                Redirect(make_admin_url('student', 'insert', 'insert&type=document&s_id=' . $s_id));
            elseif ($type == 'document'):
                #Add Previous School detail of student				
                $QueryObj1 = new studentDocument();
                $QueryObj1->saveDataInsertCase($_POST['document'], $s_id);

                $admin_user->set_pass_msg(MSG_STUDENT_DOCUMENT_ADDED);
                Redirect(make_admin_url('student', 'insert', 'insert&type=admission&s_id=' . $s_id));
            elseif ($type == 'admission'):
                $CheckSessStudent = new studentSession();
                $check = $CheckSessStudent->GetStudentWithSessionID($_POST['session_id'], $_POST['student_id']);
                if ($check):
                    $_POST['id'] = $check->id;
                    $QuerySess = new studentSession();
                    $QuerySess->saveData($_POST);

                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_SESSION_STUDENT_ALREADY_ADDED);
                    Redirect(make_admin_url('student', 'insert', 'insert&type=vehicle&session_id=' . $check->session_id . '&ct_sec=' . $check->section . '&s_id=' . $_POST['student_id']));
                else:

                    #count the last section student
                    $QueryCountSt = new studentSession();
                    $section_wise = $QueryCountSt->getSessionStudentBySection($_POST['session_id']);

                    $session = get_object('session', $_POST['session_id']);

                    # set the section and roll no
                    if (count($section_wise) >= 1):
                        $last_section = $section_wise[(count($section_wise) - 1)]['section'];
                        $last_section_students = $section_wise[(count($section_wise) - 1)]['count'];
                    else:
                        $last_section = 'A';
                        $last_section_students = '0';
                    endif;

                    if (empty($last_section)) {
                        $last_section = 'A';
                    }
                    if (empty($last_section_students)) {
                        $last_section_students = '0';
                    }

                    #Get Max Roll No
                    $QuerylastRoll = new studentSession();
                    $last_section_roll_no = $QuerylastRoll->getSessionSectionMaxRollNo($_POST['session_id'], $last_section);

                    if ($last_section_students >= $session->max_sections_students):
                        $_POST['section'] = $Master_Section[$last_section];
                        $_POST['roll_no'] = '1';
                    else:
                        $_POST['section'] = $last_section;
                        $_POST['roll_no'] = ($last_section_roll_no + 1);
                    endif;

                    $new_sect = get_charcter_value($_POST['section']);

                    # if maximum limit is over
                    if ($new_sect > $session->total_sections_allowed):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg(MSG_SESSION_STUDENT_LIMIT);
                        Redirect(make_admin_url('student', 'insert', 'insert&type=admission&s_id=' . $_POST['student_id']));
                    endif;

                    # check duplicate roll no
                    $QueryDup = new studentSession();
                    $duplicate = $QueryDup->checkDuplicateRecord($_POST['session_id'], $_POST['section'], $_POST['roll_no']);

                    if (is_object($duplicate)):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg(MSG_NOT_ADDED_TO_SESSION);
                        Redirect(make_admin_url('student', 'insert', 'insert&type=admission&s_id=' . $_POST['student_id']));
                    endif;

                    #Add student to session	
                    $QuerySess = new studentSession();
                    $QuerySess->saveData($_POST);

                    $_POST['Compulsory'] = explode(',', $session->compulsory_subjects);

                    #delete all subject for the student
                    $QuerySub = new studentSubject();
                    $QuerySub->DeleteSubjects($_POST['session_id'], $_POST['student_id']);

                    #update subjects
                    $QuerySub = new studentSubject();
                    $QuerySub->saveSubjects($_POST);

                    #Get all session students in array
                    $SessFeeObj = new session_fee_type();
                    $SessFeeHeads = $SessFeeObj->sessionPayHeads($_POST['session_id'], 'array');

                    #Add All Session Fee
                    $SessFeeAdd = new studentSessionInterval();
                    $SessFeeAdd->AddCompleteSessionFee($SessFeeHeads, $_POST);

                    #Add Multiple Session Fee into Student Fee List
                    $QuerySess = new session_student_fee();
                    $QuerySess->saveData(array($_POST['student_id'] => $_POST['student_id']), $_POST['session_id'], $SessFeeHeads);

                    $admin_user->set_pass_msg(MSG_SESSION_STUDENT_ADDED);
                    Redirect(make_admin_url('student', 'insert', 'insert&type=vehicle&session_id=' . $_POST['session_id'] . '&ct_sec=' . $_POST['section'] . '&s_id=' . $_POST['student_id']));
                endif;

            elseif ($type == 'vehicle'):
                #get Vehicle student info
                $QueryChkStu = new vehicleStudent();
                $VehstudentAdd = $QueryChkStu->checkOtherVehicleAssign($_POST['session_id'], $_POST['section'], $_POST['student_id'], $_POST['vehicle_id']);


                if (is_object($VehstudentAdd) && ($VehstudentAdd->vehicle_id != $_POST['vehicle_id'])):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_VEHICLE_SINGLE_STUDENT_ALREDY_ADDED_OLD);
                    Redirect(make_admin_url('student', 'insert', 'insert&type=vehicle&session_id=' . $_POST['session_id'] . '&ct_sec=' . $_POST['section'] . '&s_id=' . $_POST['student_id']));
                endif;

                $session_info = get_object('session', $_POST['session_id']);

                if (strtotime($_POST['from_date']) > strtotime($session_info->end_date)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_THIS_SESSSION_PASSED);
                    Redirect(make_admin_url('student', 'update', 'update&type=vehicle&session_id=' . $_POST['session_id'] . '&ct_sec=' . $_POST['section'] . '&s_id=' . $_POST['student_id']));
                endif;


                #now Assign vehicle
                $stuVeh = new vehicleStudent();
                $stuVeh->start_vehicle($_POST['session_id'], $_POST['section'], $_POST['student_id'], $_POST['vehicle_id'], $_POST['amount']);

                $admin_user->set_pass_msg(MSG_VEHICLE_SINGLE_STUDENT_ADDED);
                Redirect(make_admin_url('student', 'list', 'list'));
            endif;
        endif;

        if (isset($_POST['stop_vehicle'])):
            #get Vehicle student info
            $QueryChkStu = new vehicleStudent();
            $VehstudentAdd = $QueryChkStu->checkVehicleStudentAssign($_POST['session_id'], $_POST['section'], $_POST['student_id']);

            if (is_object($VehstudentAdd)):
                $QueryStStu = new vehicleStudent();
                $QueryStStu->stop_vehicle($VehstudentAdd->assign_id);
            endif;
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('student', 'insert', 'insert&type=vehicle&session_id=' . $_POST['session_id'] . '&ct_sec=' . $_POST['section'] . '&s_id=' . $_POST['student_id']));
        endif;


        break;

    case'delete_record':
        #delete record
        if ($type == 'family'):
            $QueryObj = new studentSibling();
            $QueryObj->deleteRecord($_GET['del_id']);

            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('student', 'update', 'update&type=' . $type . '&s_id=' . $s_id));
        elseif ($type == 'previous_school'):
            $QueryObj = new studentPreviousSchool();
            $QueryObj->deleteRecord($_GET['del_id']);

            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('student', 'update', 'update&type=' . $type . '&s_id=' . $s_id));
        elseif ($type == 'document'):
            $QueryObj = new studentDocument();
            $QueryObj->deleteRecord($_GET['del_id']);

            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('student', 'update', 'update&type=' . $type . '&s_id=' . $s_id));
        endif;
        break;

    case'delete_image':

        #delete record
        if ($type == 'student_detail'):
            $QueryObj = new student();
            $QueryObj->deleteImage($s_id);
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('student', 'update', 'update&type=' . $type . '&s_id=' . $s_id));
        elseif ($type == 'family'):
            $QueryObj = new studentFamily();
            $QueryObj->deleteImage($_GET['del_id']);

            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('student', 'update', 'update&type=' . $type . '&s_id=' . $s_id));
        endif;

        break;

    case'update':
        if (empty($s_id)):
            $s_id = $id;
        endif;
        #get session info
        $session_info = get_object('session', $session_id);

        #get student info
        $QueryInfo = new student();
        $s_info = $QueryInfo->getStudent($s_id);

        #get student permanent address
        $QueryP = new studentAddress();
        $s_p_ad = $QueryP->getStudentAddressByType($s_id, 'permanent');

        #get student correspondence address
        $QueryC = new studentAddress();
        $s_c_ad = $QueryC->getStudentAddressByType($s_id, 'correspondence');

        #get student correspondence address
        $QueryF = new studentFamily();
        $s_f = $QueryF->getStudentFamilyDeatils($s_id);

        #get student Sibling Info
        $QueryS = new studentSibling();
        $QueryS->getStudentSibling($s_id);

        #get student Previous School Info
        $P_school = new studentPreviousSchool();
        $P_school->getStudentPreviousSchool($s_id);

        #get student Document Info
        $S_doc = new studentDocument();
        $S_doc->getStudentDocument($s_id);

        #get current session
        $Ad_stu = new studentSession();
        $ct_addmission = $Ad_stu->checkStudentInSession($s_id);

        $CurrObj = new studentSession();
        $Student_current_session = $CurrObj->getStudentCurrentSessionInfo($school->id, $s_id);
//pr($session_info); exit;
        if ($type == 'vehicle'):
            #get Vehicle Info
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($s_id, $session_id, $ct_sec);

            #get Vehicle student info
            $QueryChkStu = new vehicleStudent();
            $VehstudentAdd = $QueryChkStu->checkVehicleStudentAssign($session_id, $ct_sec, $s_id);

            if (is_object($VehstudentAdd)):
            //$admin_user->set_error();
            //$admin_user->set_pass_msg(MSG_VEHICLE_SINGLE_STUDENT_ALREDY_ADDED);
            endif;

        endif;

        if (isset($_POST['submit'])):

            #Generate Student Login ID & Password if not generated
            $obj = new student;
            $student = $obj->getStudent($_POST['id']);
            if (empty($student->student_login_id) || empty($student->password)) {
                // Generate Unique Password
                $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMONOPQRSTUVWXYZ0123456789';
                $password = '';
                for ($i = 0; $i < 10; $i++) {
                    $password .= $characters[rand(0, strlen($characters) - 1)];
                }
                $arr['id'] = $_POST['id'];
                $arr['student_login_id'] = $school_setting->reg_id_prefix . 'STD' . $_POST['id'];
                $arr['password'] = $password;
                $obj = new student;
                $obj->saveStudentDetails($arr);
            }
            if ($type == 'student_detail'):
                #Check Student reg_id
                $QueryCheckReg = new student();
                $reg_check = $QueryCheckReg->cehck_reg_id($school->id, $_POST['reg_id'], $_POST['id']);
                if (is_object($reg_check)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_REG_ID_EXIST);
                    Redirect(make_admin_url('student', 'update', 'update&id=' . $_POST['id']));
                endif;

                #Add Basic Detail of student
                $QueryObj1 = new student();
                $student_id = $QueryObj1->saveData($_POST);



                $admin_user->set_pass_msg(MSG_STUDENT_DETAIL_ADDED);
                Redirect(make_admin_url('student', 'update', 'update&type=address&s_id=' . $student_id));
            elseif ($type == 'address'):
                #Add permanent address of student
                $QueryObj1 = new studentAddress();
                $QueryObj1->saveData($_POST['permanent']);

                #Add correspondence address of student
                $QueryCor = new studentAddress();
                $QueryCor->saveData($_POST['correspondence']);

                $admin_user->set_pass_msg(MSG_STUDENT_ADDRESS_ADDED);
                Redirect(make_admin_url('student', 'update', 'update&type=family&s_id=' . $s_id));
            elseif ($type == 'family'):
                #get student correspondence address
                $QueryFm = new studentFamily();
                $s_fm = $QueryFm->getStudentFamilyDeatils($_POST['student_id']);
                if (is_object($s_fm)):
                    $_POST['family']['id'] = $s_fm->id;
                endif;

                #Add family detail of student				
                $QueryObj1 = new studentFamily();
                $QueryObj1->saveData($_POST['family']);

                #Add sibling inf of student
                $QuerySib = new studentSibling();
                $QuerySib->saveData($_POST['sibling'], $s_id);

                $admin_user->set_pass_msg(MSG_STUDENT_FAMILY_ADDED);
                Redirect(make_admin_url('student', 'update', 'update&type=previous_school&s_id=' . $s_id));
            elseif ($type == 'previous_school'):
                #Add Previous School detail of student				
                $QueryObj1 = new studentPreviousSchool();
                $QueryObj1->saveData($_POST['school'], $s_id);

                $admin_user->set_pass_msg(MSG_STUDENT_PREVIOUS_SCHOOL_ADDED);
                Redirect(make_admin_url('student', 'update', 'update&type=document&s_id=' . $s_id));
            elseif ($type == 'document'):

                #Add Previous School detail of student				
                $QueryObj1 = new studentDocument();
                $QueryObj1->saveData($_POST['document'], $s_id);

                Redirect(make_admin_url('student', 'update', 'update&type=admission&s_id=' . $s_id));
            elseif ($type == 'admission'):
                $CheckSessStudent = new studentSession();
                $check = $CheckSessStudent->GetStudentWithSessionID($_POST['session_id'], $_POST['student_id']);

                if ($check):
                    $_POST['id'] = $check->id;
                    $QuerySess = new studentSession();
                    $QuerySess->saveData($_POST);
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_SESSION_STUDENT_ALREADY_ADDED);
                    Redirect(make_admin_url('student', 'update', 'update&type=vehicle&session_id=' . $check->session_id . '&ct_sec=' . $check->section . '&s_id=' . $_POST['student_id']));
                else:

                    #count the last section student
                    $QueryCountSt = new studentSession();
                    $section_wise = $QueryCountSt->getSessionStudentBySection($_POST['session_id']);

                    #session info
                    $session = get_object('session', $_POST['session_id']);

                    # set the section and roll no
                    if (count($section_wise) >= 1):
                        $last_section = $section_wise[(count($section_wise) - 1)]['section'];
                        $last_section_students = $section_wise[(count($section_wise) - 1)]['count'];
                    else:
                        $last_section = 'A';
                        $last_section_students = '0';
                    endif;

                    if (empty($last_section)) {
                        $last_section = 'A';
                    }
                    if (empty($last_section_students)) {
                        $last_section_students = '0';
                    }

                    #Get Max Roll No
                    $QuerylastRoll = new studentSession();
                    $last_section_roll_no = $QuerylastRoll->getSessionSectionMaxRollNo($_POST['session_id'], $last_section);

                    if ($last_section_students >= $session->max_sections_students):
                        $_POST['section'] = $Master_Section[$last_section];
                        $_POST['roll_no'] = '1';
                    else:
                        $_POST['section'] = $last_section;
                        $_POST['roll_no'] = ($last_section_roll_no + 1);
                    endif;

                    $new_sect = get_charcter_value($_POST['section']);

                    # if meximum limit is over
                    if ($new_sect > $session->total_sections_allowed):
                        $admin_user->set_error();
                        $admin_user->set_pass_msg(MSG_SESSION_STUDENT_LIMIT);
                        Redirect(make_admin_url('student', 'update', 'update&type=admission&s_id=' . $_POST['student_id']));
                    endif;

                    #Add student to session	
                    $QuerySess = new studentSession();
                    $QuerySess->saveData($_POST);

                    #Get all session students in array
                    $SessFeeObj = new session_fee_type();
                    $SessFeeHeads = $SessFeeObj->sessionPayHeads($_POST['session_id'], 'array');

                    #Add All Session Fee
                    $SessFeeAdd = new studentSessionInterval();
                    $SessFeeAdd->AddCompleteSessionFee($SessFeeHeads, $_POST);

                    #Add Multiple Session Fee into Student Fee List
                    $QuerySess = new session_student_fee();
                    $QuerySess->saveData(array($_POST['student_id'] => $_POST['student_id']), $_POST['session_id'], $SessFeeHeads);


                    $admin_user->set_pass_msg(MSG_SESSION_STUDENT_ADDED);
                    Redirect(make_admin_url('student', 'update', 'update&type=vehicle&session_id=' . $_POST['session_id'] . '&ct_sec=' . $_POST['section'] . '&s_id=' . $_POST['student_id']));
                endif;
            elseif ($type == 'vehicle'):
                #get Vehicle student info
                $QueryChkStu = new vehicleStudent();
                $VehstudentAdd = $QueryChkStu->checkOtherVehicleAssign($_POST['session_id'], $_POST['section'], $_POST['student_id'], $_POST['vehicle_id']);


                if (is_object($VehstudentAdd) && ($VehstudentAdd->vehicle_id != $_POST['vehicle_id'])):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_VEHICLE_SINGLE_STUDENT_ALREDY_ADDED_OLD);
                    Redirect(make_admin_url('student', 'update', 'update&type=vehicle&session_id=' . $_POST['session_id'] . '&ct_sec=' . $_POST['section'] . '&s_id=' . $_POST['student_id']));
                endif;

                $session_info = get_object('session', $_POST['session_id']);

                if (strtotime($_POST['from_date']) > strtotime($session_info->end_date)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_THIS_SESSSION_PASSED);
                    Redirect(make_admin_url('student', 'update', 'update&type=vehicle&session_id=' . $_POST['session_id'] . '&ct_sec=' . $_POST['section'] . '&s_id=' . $_POST['student_id']));
                endif;


                #now Assign vehicle
                $stuVeh = new vehicleStudent();
                $stuVeh->start_vehicle($_POST['session_id'], $_POST['section'], $_POST['student_id'], $_POST['vehicle_id'], $_POST['amount']);

                $admin_user->set_pass_msg(MSG_VEHICLE_SINGLE_STUDENT_ADDED);
                Redirect(make_admin_url('student', 'list', 'list'));
            endif;
        endif;

        if (isset($_POST['stop_vehicle'])):
            #get Vehicle student info
            $QueryChkStu = new vehicleStudent();
            $VehstudentAdd = $QueryChkStu->checkVehicleStudentAssign($_POST['session_id'], $_POST['section'], $_POST['student_id']);

            if (is_object($VehstudentAdd)):
                $QueryStStu = new vehicleStudent();
                $QueryStStu->stop_vehicle($VehstudentAdd->assign_id);
            endif;
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('student', 'update', 'update&type=vehicle&session_id=' . $_POST['session_id'] . '&ct_sec=' . $_POST['section'] . '&s_id=' . $_POST['student_id']));
        endif;

        break;

    case'stuck_off':
        #get_all current session 
        $QuerySession = new session();
        $All_Session = $QuerySession->listAllCurrentSessionStruckOff($school->id, '1', 'array');

        /* Set First time session id */
        if (empty($session_id) && !empty($All_Session)):
            $session_id = $All_Session['0']->id;
        endif;

        /* Get section Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);

        #Get Struck Off Students
        $obj = new stuck_off;
        $record = $obj->listStuckOffStudents($session_id, $ct_sec);
        break;

    case'delete':
        $QueryObj = new student();
        $QueryObj->deleteRecord($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('student', 'list', 'list'));
        break;

    default:break;
endswitch;
?>
