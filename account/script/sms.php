<?php

/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/quickSmsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSmsEmailClass.php');


$modName='sms';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_REQUEST['type'])?$type=$_REQUEST['type']:$type='select';
isset($_GET['type'])?$sms_id=$_GET['sms_id']:$sms_id='';


switch ($action):
	case'list':
		 $QueryObj = new quickSms();
	     $record=$QueryObj->listAll('1');	
		
			if($type=='type'):
				$QueryObj = new quickSms();
				$sms=$QueryObj->getRecord($sms_id);				
			endif;
			
			if(isset($_POST['submit'])):
				ignore_user_abort(true);
				$QueryObj = new studentSession();
				$allStudent=$QueryObj->findActiveSessionStudents($school->id);			
								
				if($school_setting->is_sms_allowed!='1'):
					$admin_user->set_pass_msg("Sorry, You are not allowed to send sms..!");
					Redirect(make_admin_url('sms', 'list', 'list')); 					
				endif;
				
				if(!empty($allStudent)):
					foreach($allStudent as $key=>$s_info):
						$earr1=array();
						$earr1['STUDENT']=ucfirst($s_info->first_name).' '.$s_info->last_name;				
						if($sms_id=='1'):
							$earr1['DATE']=date('M Y',strtotime($_POST['DATE']));
						else:
							$earr1['START_DATE']=date('d M Y',strtotime($_POST['START_DATE']));
						endif;	
						$earr1['FROM_TIME']=$_POST['FROM_TIME'];
						$earr1['TO_TIME']=$_POST['TO_TIME'];
						
						$msg=get_database_quick_sms($sms_id,$earr1);
						
						$send_sms=send_sms($s_info->mobile,$msg);
						if($send_sms=='1'):
							$_POST['sms_send_status']='1';
							$_POST['sms_send_date']=date('Y-m-d');
						endif;
						$_POST['school_id']=$school->id; $_POST['sms_type']='quick'; $_POST['type']='sms'; $_POST['on_date']=date('Y-m-d');
						$_POST['to_number']=$s_info->mobile; $_POST['sms_text']=$msg; $_POST['student_id']=$s_info->student_id; 
						
						$QuerySave1 = new schoolSmsEmailHistory();												
						$QuerySave1->saveData($_POST);
						unset($_POST['sms_send_status']);
						unset($_POST['sms_send_date']);
					endforeach;
				endif;
				$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
				Redirect(make_admin_url('sms', 'list', 'list')); 				
			endif;
			
			
		break;
	case'insert':
		
	 break;

	case'update':
		
		
	 break;
	 
	case'delete':
				
    break;      
	case'restore':

	break;

	case'thrash':

	break;
	default:break;
endswitch;
?>
