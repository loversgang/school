<?php

include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
$modName = 'time_table';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';

$QueryObjQ = new session();
$AllSessYear = $QueryObjQ->listOfYearSess($school->id);
if (empty($sess_int)):
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
endif;
#handle actions here.
switch ($action):
    case'list':
        #get_all current session 
        $QuerySession = new session();
        $All_Session = $QuerySession->listAllCurrent($school->id, '1', 'array');

        /* Set First time session id */
        if (empty($session_id) && !empty($All_Session)):
            $session_id = $All_Session['0']->id;
        endif;

        /* Get section Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);

        $QueryObj = new timeTable;
        $time_table_list = $QueryObj->listTimeTable($sess_int);
        break;
    case'insert':
        $obj = new staff;
        $staff_list = $obj->getSchoolStaffDetailActiveDeactive($school->id, '1');
        $QueryObj = new session();
        $QueryObj->listOfYearSession($school->id, $sess_int);
        if (isset($_POST['submit'])) {
            $data_array = array();
            for ($i = 0; $i < count($_POST['subject']); $i++) {
                $data_array[] = array(
                    'school_id' => $school->id,
                    'session_year' => $_POST['session_year'],
                    'session_id' => $_POST['session_id'],
                    'section' => $_POST['section'],
                    'day' => $_POST['day'],
                    'subject' => $_POST['subject'][$i],
                    'staff_id' => $_POST['subject'][$i] ? $_POST['staff_id'][$i] : '0',
                    'comment' => !$_POST['subject'][$i] ? $_POST['comment'][$i] : '',
                    'time_from' => $_POST['time_from'][$i],
                    'time_to' => $_POST['time_to'][$i]
                );
            }
            foreach ($data_array as $data) {
                $obj = new timeTable;
                $obj->saveTimeTable($data);
            }
            $admin_user->set_pass_msg('Time Table Added Successfully!');
            Redirect(make_admin_url('time_table', 'list', 'list'));
        }
        break;
    case'update':
        $obj = new staff;
        $staff_list = $obj->getSchoolStaffDetailActiveDeactive($school->id, '1');

        $obj = new timeTable;
        $time_table = $obj->getRecord($id);

        $QueryObj = new session();
        $QueryObj->listOfYearSession($school->id, $sess_int);
        if (isset($_POST['submit'])) {
            if ($_POST['subject'] == '0') {
                $_POST['staff_id'] = '0';
            } else {
                $_POST['comment'] = '';
            }
            $obj = new timeTable;
            $obj->saveTimeTable($_POST);
            $admin_user->set_pass_msg('Time Table Updated Successfully!');
            Redirect(make_admin_url('time_table', 'list', 'list'));
        }
        break;
    case'delete':
        $obj = new timeTable;
        $obj->id = $id;
        $obj->Delete();
        $admin_user->set_pass_msg("Time Table Deleted Successfully!");
        Redirect(make_admin_url('time_table', 'list', 'list'));
        break;
    default:
        break;
endswitch;
