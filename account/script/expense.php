<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');

$modName = 'expense';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';

$QueryCat = new schoolExpenseCategory();
$list = $QueryCat->listAll($school->id);

// List All Expense Categories
$obj = new schoolExpenseCategory;
$cats = $obj->listAllExpenseCat($school->id);


$obj = new schoolExpense;
$expense_years = $obj->listExpenseYears($school->id);

#handle actions here.
switch ($action):
    case'list':
        $QueryObj = new schoolExpense();
        $record = $QueryObj->listAll($school->id);
        break;
    case'insert':
        if (isset($_POST['submit'])):
            $files_array = array();
            for ($i = 0; $i < count($_FILES['doc']['name']); $i++) {
                $files_array[] = array(
                    'title' => $_POST['doc_title'][$i],
                    'name' => $_FILES['doc']['name'][$i],
                    'tmp_name' => $_FILES['doc']['tmp_name'][$i],
                    'error' => $_FILES['doc']['error'][$i]
                );
            }
            $QueryObj = new schoolExpense();
            $expense_id = $QueryObj->saveData($_POST);
            foreach ($files_array as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/expense_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['expense_id'] = $expense_id;
                        $arr['title'] = $file['title'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new expense_docs;
                        $obj->saveExpenseDoc($arr);
                    endif;
                endif;
            endforeach;
            $admin_user->set_pass_msg(MSG_EXPENSE_ADDED);
            Redirect(make_admin_url('expense', 'list', 'list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('expense', 'list', 'list'));
        endif;
        break;
    case'update':
        $QueryObj = new schoolExpense();
        $object = $QueryObj->getRecord($id);

        // List Expense Docs
        $obj = new expense_docs;
        $expense_docs = $obj->listExpenseDocs($id);

        if (isset($_POST['submit'])):
            // Update Old Docs
            $titles_array = array_combine($_POST['doc_id'], $_POST['title_old']);
            foreach ($titles_array as $id => $title) {
                $array['id'] = $id;
                $array['title'] = $title;
                $obj = new expense_docs;
                $obj->saveExpenseDoc($array);
            }
            $files_array_old = array();
            for ($i = 0; $i < count($_FILES['doc_old']['name']); $i++) {
                $files_array_old[] = array(
                    'id' => $_POST['doc_id'][$i],
                    'name' => $_FILES['doc_old']['name'][$i],
                    'tmp_name' => $_FILES['doc_old']['tmp_name'][$i],
                    'error' => $_FILES['doc_old']['error'][$i]
                );
            }
            foreach ($files_array_old as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/expense_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['id'] = $file['id'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new expense_docs;
                        $obj->saveExpenseDoc($arr);
                    endif;
                endif;
            endforeach;
            // Save Docs
            $files_array = array();
            for ($i = 0; $i < count($_FILES['doc']['name']); $i++) {
                $files_array[] = array(
                    'title' => $_POST['doc_title'][$i],
                    'name' => $_FILES['doc']['name'][$i],
                    'tmp_name' => $_FILES['doc']['tmp_name'][$i],
                    'error' => $_FILES['doc']['error'][$i]
                );
            }
            foreach ($files_array as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/expense_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['expense_id'] = $_POST['id'];
                        $arr['title'] = $file['title'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new expense_docs;
                        $obj->saveExpenseDoc($arr);
                    endif;
                endif;
            endforeach;
            $QueryObj = new schoolExpense();
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_EXPENSE_UPDATE);
            Redirect(make_admin_url('expense', 'list', 'list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('expense', 'list', 'list'));
        endif;
        break;

    case'delete':
        $QueryObj = new schoolExpense();
        $QueryObj->deleteRecord($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('expense', 'list', 'list'));
        break;

    case'cat_list':
        $QueryObj = new schoolExpenseCategory();
        $QueryObj->listAll($school->id);
        break;

    case'cat_insert':
        if (isset($_POST['submit'])):
            $QueryObj = new schoolExpenseCategory();
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_EXPENSE_CAT_ADDED);
            Redirect(make_admin_url('expense', 'cat_list', 'cat_list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('expense', 'cat_list', 'cat_list'));
        endif;
        break;
    case'cat_update':
        $QueryObj = new schoolExpenseCategory();
        $object = $QueryObj->getRecord($id);
        if (isset($_POST['submit'])):
            $QueryObj = new schoolExpenseCategory();
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_EXPENSE_CAT_UPDATE);
            Redirect(make_admin_url('expense', 'cat_list', 'cat_list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('expense', 'cat_list', 'cat_list'));
        endif;
        break;

    case'cat_delete':
        $QueryObj = new schoolExpenseCategory();
        $QueryObj->deleteRecord($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('expense', 'cat_list', 'cat_list'));
        break;

    default:break;
endswitch;
?>
