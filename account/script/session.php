<?php

/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');


$modName = 'session';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['ct_session']) ? $ct_session = $_GET['ct_session'] : $ct_session = '';
isset($_GET['ct_section']) ? $ct_section = $_GET['ct_section'] : $ct_section = 'A';
isset($_GET['doc_id']) ? $doc_id = $_GET['doc_id'] : $doc_id = '';
isset($_REQUEST['s_s']) ? $s_s = $_GET['s_s'] : $s_s = '';
isset($_GET['sess_int']) ? $sess_int = $_GET['sess_int'] : $sess_int = '';


#Get School Couses
$QueryCourse = new course();
$QueryCourse->listAll($school->id, '1');

#get_all session 
$QuerySession = new session();
$QuerySession->listAll($school->id, '1');

#Get School staff
$QueryStaff = new staff();
$QueryStaff->getSchoolStaff($school->id, '1');

#Get School staff Category
$QueryStaffCat = new staffCategory();
$all_Staff_cat = $QueryStaffCat->listAll($school->id, '1', 'array');

#Get Subject List
$QuerySubject = new subject();
$QuerySubject->ComplserySubjectList($school->id);

#Get Subject List
$QuerySubject1 = new subject();
$QuerySubject1->ElectiveSubjectList($school->id);

#Get Fee Heads
$QueryFee = new feeType();
$QueryFee->listAll($school->id, '1');


$QueryObjQ = new session();
$AllSessYear = $QueryObjQ->listOfYearSess($school->id);

if (empty($sess_int)):
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
endif;



switch ($action):
    case'list':

        #Get All current session Section
        $QueryObj = new session();
        $QueryObj->listOfYearSession($school->id, $sess_int);

        //echo '<pre>'; print_r($QuerySec); exit;				 
        /* Get session Pages */
        // $QueryObj=new session();
        // $QueryObj->listAll($school->id);
//echo '<pre>'; print_r($rec); exit;
        $QuerySess = new session();
        $AllSession = $QuerySess->listOfYearSess($school->id);
        if (!empty($AllSession) && empty($school->id)):
            $school->id = $AllSession['0']->id;
        endif;

        #Get All current session Section
        $QuerySec = new session();
        $AllSection = $QuerySec->listOfYearSess($school->id);

        break;
    case'insert':
        /* Insert session */
        if (isset($_POST['submit'])):
            $course = get_object('school_course', $_POST['course_id']);
            $_POST['title'] = $course->course_name . ' ' . date('Y', strtotime($_POST['start_date'])) . '-' . date('y', strtotime($_POST['end_date']));

            $diff = get_difference_in_months($_POST['start_date'], $_POST['end_date']);

            if ($_POST['frequency'] > $diff):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_RESELECT_PAYMENT_INTERVAL);
                return false;
            //Redirect(make_admin_url('session', 'insert', 'insert'));						
            endif;

            #Add Session
            $QueryObj = new session();
            $session_id = $QueryObj->saveData($_POST);

            #Session fee interval
            $QueryObjSess = new session();
            $interval = $QueryObjSess->GetFeeInterval($_POST['start_date'], $_POST['end_date'], $_POST['frequency']);

            #Update Session Interval
            $QueryObjIn = new session_interval();
            $QueryObjIn->saveIntervals($interval, $session_id);

            #Add Session Fee Heads
            $QuerySess = new session_fee_type();
            $QuerySess->saveData($_POST['head'], $session_id);

            $admin_user->set_pass_msg(MSG_SESSION_ADDED);
            Redirect(make_admin_url('session', 'session_sec', 'session_sec&id=' . $session_id));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('session', 'list', 'list'));
        endif;
        break;

    case'session_sec':
        /* Get session Pages by Id */
        $QueryPage = new session();
        $object = $QueryPage->getRecord($id);

        #Get School staff
        $QueryStaff = new staff();
        $Staff = $QueryStaff->getSchoolStaff($school->id, '1', 'array');

        #Get School staff session section relation for update case
        $SessionStaff = new session_section_staff();
        $record = $SessionStaff->listAllSessionSection($object->id, $object->total_sections_allowed);



        /* Update session Pages */
        if (isset($_POST['submit'])):
            #delete Previous Record Into Session Section
            $DelObj = new session_section_staff();
            $DelObj->DeleteSessionSessionRecord($id);


            #Insert Record Into Session Section
            $QueryObj = new session_section_staff();
            $QueryObj->saveData($_POST, $id);

            $admin_user->set_pass_msg(SESSION_STAFF_UPDATE);
            Redirect(make_admin_url('session', 'list', 'list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('session', 'list', 'list'));
        endif;
        break;
    case'update':
        /* Get session Pages by Id */
        $QueryPage = new session();
        $object = $QueryPage->getRecord($id);

        #Get Session Fee Heads
        $QuerySess = new session_fee_type();
        $pay_heads = $QuerySess->sessionPayHeads($object->id, 'array');

        #Get Session Fee Heads In Array
        $Queryhd = new session_fee_type();
        $all_heads = $Queryhd->sessionPayHeadsIdInArray($object->id);


        /* Update session Pages */
        if (isset($_POST['submit'])):
            $course = get_object('school_course', $_POST['course_id']);
            $_POST['title'] = $course->course_name . ' ' . date('Y', strtotime($_POST['start_date'])) . '-' . date('y', strtotime($_POST['end_date']));

            $diff = get_difference_in_months($_POST['start_date'], $_POST['end_date']);

            #Session fee interval
            $QueryObjSess = new session();
            $interval = $QueryObjSess->GetFeeInterval($_POST['start_date'], $_POST['end_date'], $_POST['frequency']);

            #Update Session
            $QueryObj = new session();
            $session_id = $QueryObj->saveData($_POST);

            #Update Session Interval
            $QueryObjIn = new session_interval();
            //$QueryObjIn->saveIntervals($interval,$session_id);							
            #firstly delete all Session Fee Heads
            $QuerySHeads = new session_fee_type();
            $QuerySHeads->DeleteSessionFeeHeads($session_id);

            #Add Session Fee Heads
            $QuerySess = new session_fee_type();
            $QuerySess->saveData($_POST['head'], $session_id);

            $admin_user->set_pass_msg(MSG_SESSION_UPDATE);
            Redirect(make_admin_url('session', 'session_sec', 'session_sec&id=' . $session_id));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('session', 'list', 'list'));
        endif;
        break;

    case'view':
        /* Get session  by Id */
        $QueryPage = new session();
        $object = $QueryPage->getRecord($id);

        /* Get school document template for student id card */
        $QueryDocs = new SchoolDocumentTemplate();
        $student_icard = $QueryDocs->get_school_single_document($school->id, 1);

        #Get session students
        $QueryStu = new studentSession();
        $AllStu = $QueryStu->sessionStudents($id);

        #Get all session students
        $SessStu = new studentSession();
        $session_st = $SessStu->sessionStudentIdArray($id);

        #Get section
        $SessSecObj = new studentSession();
        $SessSec = $SessSecObj->getSessionStudentBySection($id);

        if (!empty($SessSec)):
            foreach ($SessSec as $s_key => $s_val):
                $countFemaleObj = new studentSession();
                $countMale = $countFemaleObj->countSexInSection($id, $s_val['section'], 'Male');
                $countFemale = $countFemaleObj->countSexInSection($id, $s_val['section'], 'Female');

                #check error for the max boy and girl limit in every section	
                if (isset($object->max_boy) && $countMale > $object->max_boy):
                    $admin_user->set_error();
                //$admin_user->set_pass_msg(MSG_MEX_BOY_LIMIT_IN_SECTION_1.$s_val['section'].MSG_MEX_BOY_LIMIT_IN_SECTION_2);									
                endif;

                if (isset($object->max_girl) && $countFemale > $object->max_girl):
                    $admin_user->set_error();
                //$admin_user->set_pass_msg(MSG_MEX_GIRL_LIMIT_IN_SECTION_1.$s_val['section'].MSG_MEX_GIRL_LIMIT_IN_SECTION_2);									
                endif;
            endforeach;
        endif;

        break;

    case'print':

        #get Student detail	
        $QueryStu = new studentSession();
        $student = $QueryStu->SingleStudentsWithSession($id, $ct_session, $ct_section);

        #Get document
        $QueryDoc = new document();
        $doc = $QueryDoc->getRecord($doc_id);

        $im_obj = new imageManipulation();
        if ($student->photo):
            $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
        else:
            $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
        endif;
        if ($school->logo):
            $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
        else:
            $school_logo = '<img src="assets/img/invoice/webgarh.png"  style="max-width:165px"/>';
        endif;
        //echo $staff_image; exit;

        $replace = array('SCHOOL' => $school->school_name,
            'NAME' => $student->first_name . " " . $student->last_name,
            'CLASS' => $student->session_name,
            'SECTION' => $student->section,
            'ROLL_NO' => $student->roll_no,
            'GENDER' => $student->sex,
            'DATE_OF_BIRTH' => $student->date_of_birth,
            'SESSION' => $student->session,
            'PHOTO' => $student_image,
            'LOGO' => $school_logo
        );

        $content = '';
        if (is_object($doc)):
            $content = $doc->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = '{' . trim(strtoupper($k)) . '}';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                endforeach;
            endif;
        endif;

        break;



    case'confirm_print':

        #get Student detail	
        $QueryStu = new studentSession();
        $student = $QueryStu->SingleStudentsWithSession($id, $_GET['ct_session'], $_GET['ct_section']);

        #Get document
        $QueryDoc = new document();
        $doc = $QueryDoc->getRecord($_GET['doc_id']);

        $im_obj = new imageManipulation();
        if ($student->photo):
            $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
        else:
            $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
        endif;
        if ($school->logo):
            $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
        else:
            $school_logo = '<img src="assets/img/invoice/webgarh.png"  style="max-width:165px"/>';
        endif;

        $replace = array('SCHOOL' => $school->school_name,
            'NAME' => $student->first_name . " " . $student->last_name,
            'CLASS' => $student->session_name,
            'SECTION' => $student->section,
            'ROLL_NO' => $student->roll_no,
            'GENDER' => $student->sex,
            'DATE_OF_BIRTH' => $student->date_of_birth,
            'SESSION' => $student->session,
            'PHOTO' => $student_image,
            'LOGO' => $school_logo
        );

        $content = '';
        if (is_object($doc)):
            $content = $doc->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = '{' . trim(strtoupper($k)) . '}';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                endforeach;
            endif;
        endif;


        #Get document
        $QueryDoc = new document();
        $doc = $QueryDoc->getRecord($doc_id);
        #Add Student into session
        $_POST['student_id'] = $id;
        $_POST['doc_type'] = $doc->type;
        $_POST['school_id'] = $school->id;
        $_POST['session_id'] = $_GET['ct_session'];
        $_POST['section'] = $_GET['ct_section'];
        $_POST['document_id'] = $_GET['doc_id'];
        $_POST['document_text'] = $content;
        $QuerySess = new SchoolDocumentPrintHistory();

        $QuerySess->saveData($_POST);

        $admin_user->set_pass_msg(MSG_DOCUMENT_GENERATETD);
        Redirect(make_admin_url('session', 'print', 'print&id=' . $id . '&ct_session=' . $ct_session . '&ct_section=' . $ct_section . '&doc_id=' . $doc_id));
        break;

    case'section':

        $obj = new vehicle;
        $first_vehicle = $obj->getIstVehicle($school->id);
        // List Vehicle Routes
        $obj = new vehicle_route;
        $routes = $obj->listVehicleRoute($school->id);
        //$routes = vehicle_route::getVehicleRoutes($school->id, $first_vehicle->round_routes);

        /* Get session  by Id */
        $QueryPage = new session();
        $session = $QueryPage->getRecord($ct_session);

        #Get session students
        $QueryStu = new studentSession();
        $session_student = $QueryStu->getRecord($id);

        $student = get_object('student', $session_student->student_id);

        #Get all session students
        $SessStu = new studentSession();
        $session_st = $SessStu->sessionStudentIdArray($id);

        /* Get session Pages */
        $QuerySec = new studentSession();
        //$all_section=$QuerySec->listAllSessionSection($ct_session,'1','array');
        #get Possible Section
        $all_section = get_possible_section($session->total_sections_allowed);

        #Get session Comp Sub
        $ComSessSubObj = new subject();
        $ComSessSub = $ComSessSubObj->GetSessionSubjects($school->id, $session->compulsory_subjects);

        #Get session Elc Sub
        $ElcSessSubObj = new subject();
        $ElcSessSub = $ElcSessSubObj->GetSessionSubjects($school->id, $session->elective_subjects);

        #Get Session Fee Heads
        $QuerySess = new session_fee_type();
        $Sesspay_heads = $QuerySess->sessionPayHeadsWithKey($ct_session);

        #Get student payheads students
        $SessStuFeeObj = new session_student_fee();
        $pay_heads = $SessStuFeeObj->GetStudentPayHeads($ct_session, $session_student->student_id, 'array');

        #get the student subject list if enter previous
        $QueryStComSub = new studentSubject();
        $cmpSub = $QueryStComSub->getStudentSubjectArray($session->id, $student->id, 'Compulsory');

        $QueryStElcSub = new studentSubject();
        $elcSub = $QueryStElcSub->getStudentSubjectArray($session->id, $student->id, 'Elective');

        #get All Active Vehicle List
        $QueryVeh = new vehicle();
        $QueryVeh->listAll($school->id, '1');

        #get Vehicle student info
        $QueryChkStu = new vehicleStudent();
        $VehstudentAdd = $QueryChkStu->checkVehicleStudentAssign($session->id, $session_student->section, $student->id);

        #Update student section and roll no
        if (isset($_POST['submit'])):

            #check roll no for another student
            $QueryRollCheck = new studentSession();
            $roll_check = $QueryRollCheck->checkRollnoForSection($ct_session, $_POST['section'], $_POST['roll_no'], $id);

            if (is_object($roll_check)):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_ROLL_NO_DUPLICATE);
                Redirect(make_admin_url('session', 'section', 'section&ct_session=' . $ct_session . '&id=' . $id));
            else:
                #update Roll No and section
                $QuerySess = new studentSession();
                $QuerySess->saveData($_POST);

                #delete all subject for the student
                $QuerySub = new studentSubject();
                $QuerySub->DeleteSubjects($_POST['session_id'], $_POST['student_id']);

                #update subjects
                $QuerySub = new studentSubject();
                $QuerySub->saveSubjects($_POST);

                #Update student session fee heads
                $SessStuFeeObj = new session_student_fee();
                $SessStuFeeObj->saveSingleStudentData($_POST['student_id'], $_POST['session_id'], $_POST['head']);

                #Get all session students in array
                $QueryInt = new session_interval();
                $interval = $QueryInt->listOfCurrentAndFutureInterval($_POST['session_id']);

                #Get student payheads students
                $SessStuFeeObj = new session_student_fee();
                $pay_heads = $SessStuFeeObj->GetStudentPayHeads($_POST['session_id'], $_POST['student_id'], 'array');

                #Update all session students in array
                $QueryInt = new studentSessionInterval();
                $interval = $QueryInt->UpdateCurrentAndFutureInterval($interval, $pay_heads, $_POST);

                $admin_user->set_pass_msg(MSG_STUDENT_INFO_UPDATE);
                Redirect(make_admin_url('session', 'section', 'section&ct_session=' . $ct_session . '&id=' . $id));
            endif;
        endif;

        break;

    #obly fo student fee for this session
    case'session_fee':
        $Sesspay_heads = array();
        /* Get session  by Id */
        $QueryPage = new session();
        $session = $QueryPage->getRecord($ct_session);

        #Get session students
        $QueryStu = new studentSession();
        $session_student = $QueryStu->SingleStudentsWithSession($id, $ct_session, $ct_section);

        $student = get_object('student', $id);

        #Get Session Fee Heads
        $QuerySess = new session_fee_type();
        $Sesspay_heads = $QuerySess->sessionPayHeadsWithKey($ct_session);

        #Get student payheads students
        $SessStuFeeObj = new session_student_fee();
        $pay_heads = $SessStuFeeObj->GetStudentPayHeads($ct_session, $id, 'array');


        if (isset($_POST['submit'])):

            #Update student session fee heads
            $SessStuFeeObj = new session_student_fee();
            $SessStuFeeObj->saveSingleStudentData($_POST['student_id'], $ct_session, $_POST['head']);

            $admin_user->set_pass_msg(MSG_STUDENT_FEE_UPDATE);
            Redirect(make_admin_url('session', 'session_fee', 'session_fee&ct_session=' . $ct_session . '&id=' . $id . '&ct_section=' . $_POST['ct_section']));

        endif;

        break;

    case'previous':
        /* Get session  by Id */
        $AllStu = array();
        $QueryPage = new session();
        $object = $QueryPage->getRecord($id);

        #Get session students
        $QueryStu = new studentSession();
        $AllStu = $QueryStu->sessionStudentsWithSessionOrSimple($school->id, $id);

        #Get all session students in array
        $SessStu = new studentSession();
        $session_st = $SessStu->sessionStudentIdArray($ct_session);


        /* Update session */
        if (isset($_POST['multi_select'])):
            if (isset($_POST['multiopt']) && count($_POST['multiopt']) > 0):
                $session = get_object('session', $ct_session);
                #Check Total Limit
                if (count($_POST['multiopt']) > ($session->total_sections_allowed * $session->max_sections_students)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_ONLY_LIMIT_STUDENT_1 . $session->total_sections_allowed * $session->max_sections_students . MSG_ONLY_LIMIT_STUDENT_2);
                    Redirect(make_admin_url('session', 'previous', 'previous&ct_session=' . $ct_session . '&id=' . $id));
                endif;

                #Add Multiple Student into Session
                $QuerySess = new studentSession();
                $QuerySess->AddMultipleStudentIntoSession($_POST['multiopt'], $ct_session, $id);

                #Get all session students in array
                $SessFeeObj = new session_fee_type();
                $SessFeeHeads = $SessFeeObj->sessionPayHeads($ct_session, 'array');

                #Add Multiple Session Fee into Student Fee List
                $QuerySess = new session_student_fee();
                $QuerySess->saveData($_POST['multiopt'], $ct_session, $SessFeeHeads);

                #update subjects
                $QuerySubAdd = new studentSubject();
                $QuerySubAdd->saveSubjectsFromPrevious($_POST['multiopt'], $ct_session);

                $admin_user->set_pass_msg(MSG_SESSION_MULTI_STUDENT_ADDED);
                Redirect(make_admin_url('session', 'previous', 'previous&ct_session=' . $ct_session . '&id=' . $id));
            else:
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_SELECT_MULTI_STUDENT);
                Redirect(make_admin_url('session', 'previous', 'previous&ct_session=' . $ct_session . '&id=' . $id));
            endif;
        endif;
        break;


    case'other':
        /* Get session  by Id */
        $AllStu = array();
        $QueryPage = new session();
        $object = $QueryPage->getRecord($id);

        #Get session students
        $QueryStu = new studentSession();
        $AllStu = $QueryStu->AllSchoolStudent($school->id);

        #Get all session students in array
        $SessStu = new studentSession();
        $session_st = $SessStu->sessionStudentIdArray($ct_session);

        /* Update session */
        if (isset($_POST['multi_select'])):
            if (isset($_POST['multiopt']) && count($_POST['multiopt']) > 0):

                #Add Multiple Student into Session
                $QuerySess = new studentSession();
                $QuerySess->AddMultipleStudentFromList($_POST['multiopt'], $ct_session);

                #update subjects
                $QuerySubAdd = new studentSubject();
                $QuerySubAdd->saveSubjectsFromPrevious($_POST['multiopt'], $ct_session);

                #Get all session students in array
                $SessFeeObj = new session_fee_type();
                $SessFeeHeads = $SessFeeObj->sessionPayHeads($ct_session, 'array');

                #Add Multiple Session Fee into Student Fee List
                $QuerySess = new session_student_fee();
                $QuerySess->saveData($_POST['multiopt'], $ct_session, $SessFeeHeads);

                $admin_user->set_pass_msg(MSG_SESSION_MULTI_STUDENT_ADDED);
                Redirect(make_admin_url('session', 'other', 'other&ct_session=' . $ct_session));
            else:
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_SELECT_MULTI_STUDENT);
                Redirect(make_admin_url('session', 'other', 'other&ct_session=' . $ct_session));
            endif;
        endif;
        break;

    case'stuck_off':
        #get_all current session 
        $QuerySession = new session();
        $All_Session = $QuerySession->listAllCurrentSessionStruckOff($school->id, '1', $sess_int, 'array');

        /* Set First time session id */
        if (empty($ct_session) && !empty($All_Session)):
            $ct_session = $All_Session['0']->id;
        endif;

        /* Get section Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($ct_session);

        $obj = new stuck_off;
        $record = $obj->listStuckOffStudents($ct_session, $ct_section);
        break;
    case'add_to_session':
        #Add Student into session
        $_POST['session_id'] = $id;
        $_POST['student_id'] = $_GET['st_id'];

        $QuerySess = new studentSession();
        $QuerySess->saveData($_POST);

        $admin_user->set_pass_msg(MSG_SESSION_STUDENT_ADDED);
        Redirect(make_admin_url('session', 'view', 'view&id=' . $id));
        break;
    case'remove_to_session':
        #delete Student from session
        $QuerySess = new studentSession();
        $QuerySess->deleteData($id, $_GET['st_id']);

        #Remove Student Fee into session Fee List
        $QuerySessObj = new session_student_fee();
        $QuerySessObj->removeSessionStudentFee($id, $_GET['st_id']);

        $admin_user->set_pass_msg(MSG_SESSION_STUDENT_DELETE);
        Redirect(make_admin_url('session', 'view', 'view&id=' . $id));
        break;

    case'delete':
        #check the student in session
        $QueryChSt = new studentSession();
        $SessStu = $QueryChSt->getSessionStudentBySection($id);

        if (!empty($SessStu)):
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_SESSION_NOT_DELETE);
            Redirect(make_admin_url('session', 'list', 'list'));
        endif;

        #delete session 			
        $QueryObj = new session();
        $QueryObj->deleteRecord($id);

        #delete all Session Fee Heads
        $QuerySHeads = new session_fee_type();
        $QuerySHeads->DeleteSessionFeeHeads($id);

        #delete all students from Session 
        $QuerySt = new studentSession();
        $QuerySt->DeleteStudentFromSession($id);

        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('session', 'list', 'list'));
        break;

    default:break;
endswitch;
?>
