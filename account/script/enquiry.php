<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';
isset($_GET['type'])?$type=$_GET['type']:$type='confirm';

#handle actions here.
$modName='enquiry';

switch ($action):
	case'list':
		/* Get All enquiry */
		 $QueryObj=new schoolEnquiry();
         $QueryObj->listAllType($school->id,$type);	
		break;

	case'insert':
			/* Insert schoolEnquiry*/
			if(isset($_POST['submit'])):
						#Add schoolEnquiry
						$QueryObj = new schoolEnquiry();
						$QueryObj->saveData($_POST);
						
						$admin_user->set_pass_msg(MSG_ENQUIRY_ADDED);
						Redirect(make_admin_url('enquiry', 'list', 'list'));
					elseif(isset($_POST['cancel'])):
						$admin_user->set_error();  
						$admin_user->set_pass_msg(OPERATION_CANCEL);
						Redirect(make_admin_url('enquiry', 'list', 'list'));   
			endif;
		break;	
	case'update':
			    /* Get enquiry Pages by Id*/
				$QueryPage = new schoolEnquiry();
				$object=$QueryPage->getRecord($id);		
			
				
				  /* Update schoolEnquiry Pages*/
				  if(isset($_POST['submit'])):			
			 
						#Update schoolEnquiry
						$QueryObj = new schoolEnquiry();
						$QueryObj->saveData($_POST);				  
					
						$admin_user->set_pass_msg(MSG_ENQUIRY_UPDATE);
						Redirect(make_admin_url('enquiry', 'list', 'list'));
                  elseif(isset($_POST['cancel'])):
						$admin_user->set_error();  
						$admin_user->set_pass_msg(OPERATION_CANCEL);
						Redirect(make_admin_url('enquiry', 'list', 'list'));       
	        endif; 
			break;	
        case'delete':
					#delete schoolEnquiry 			
					$QueryObj = new schoolEnquiry();
					$QueryObj->deleteRecord($id);
					
					$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
					Redirect(make_admin_url('enquiry', 'list', 'list')); 			
			break;			
		break;  
	case'thrash':
		/* Get Dead enquiry */
		 $QueryObj=new schoolEnquiry();
         $QueryObj->listAllDead($school->id);	
		break;		
	default:break;
endswitch;
?>
