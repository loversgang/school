<?php

include_once(DIR_FS_SITE . 'include/functionClass/loanClass.php');

$modName = 'loan';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['staff_id']) ? $staff_id = $_GET['staff_id'] : $staff_id = '';

switch ($action) :
    case'list':
        $query = new loan;
        $list_loans = $query->list_loans($staff_id);
        break;
    case'insert':
        if (isset($_POST['submit'])) {
            $query = new loan;
            $query->saveLoan($_POST);
            $admin_user->set_pass_msg('Loan Added Successfully');
            Redirect(make_admin_url('loan', 'list', 'list&staff_id=' . $staff_id));
        }
        break;
    case'update':
        $query = new loan();
        $list_loan = $query->list_loan($id);
      

        if (isset($_POST['submit'])) {
            $query = new loan;
            $query->saveLoan($_POST);
            $admin_user->set_pass_msg('Loan Edit Successfully');
            Redirect(make_admin_url('loan', 'list', 'list&staff_id=' . $staff_id));
        }
        break;

    case 'delete':
        $query = new loan();
        $query->Delete_using_id($id);
        $admin_user->set_pass_msg('Loan Deleted Successfully');
        Redirect(make_admin_url('loan', 'list', 'list&staff_id=' . $staff_id));
        break;
endswitch;
?>