<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
$modName = 'staff_pay_rule';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['type']) ? $type = $_GET['type'] : $type = 'select';
isset($_GET['date']) ? $date = $_GET['date'] : $date = date('M Y');
isset($_REQUEST['staff_id']) ? $staff_id = $_REQUEST['staff_id'] : $staff_id = '0';
isset($_REQUEST['doc_id']) ? $doc_id = $_REQUEST['doc_id'] : $doc_id = '0';

switch ($action):
    case'list':
        // List All Rules
        $obj = new staffPayHeads;
        $payHeads = $obj->listPayHeads();
        break;
    case'insert':
        // Add Pay Rule
        if (isset($_POST['submit'])) {
            $obj = new staffPayHeads;
            $obj->savePayHead($_POST);
            $admin_user->set_pass_msg('Pay Rule Added Successfully!');
            Redirect(make_admin_url('staff_pay_rule'));
        }

        break;
    case'update':
        // Get Pay Rule Detail
        $obj = new staffPayHeads;
        $payHead = $obj->getPayHead($id);
        $query = new staffPayHeads;
        $earnings = $query->all_earnings();
        // Update Pay Rule
        if (isset($_POST['submit'])) {
            $obj = new staffPayHeads;
            if ($_POST['type'] == 'earning') {
                $_POST['earning_deduction'] = '';
            }
            $obj->savePayHead($_POST);
            $admin_user->set_pass_msg('Pay Rule Updated Successfully!');
            Redirect(make_admin_url('staff_pay_rule'));
        }

        break;
    case'delete':
        $obj = new staffPayHeads;
        $obj->id = $id;
        $obj->Delete();
        $admin_user->set_pass_msg('Pay Rule Deleted Successfully!');
        Redirect(make_admin_url('staff_pay_rule'));
        break;
    default:
        break;
endswitch;
