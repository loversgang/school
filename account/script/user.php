<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/userClass.php');
$modName='user';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';


switch ($action):
	case'list':
		 $QueryObj = new user();
	     $QueryObj->listUsers($school->id);
		 break;
	case'insert':
		if(isset($_POST['submit'])):
                    $find='0';
					#check username from school table
					$QueryObSch = new school();
					$SchObj=$QueryObSch->check_user_name_exists($_POST['username']);				
					
					#check username from user table
					$QueryObj11 = new user();
					$UserObj=$QueryObj11->check_user_name_exists($_POST['username']);					
					
					if(is_object($SchObj)):
						$find='1';
					elseif(is_object($UserObj)):
						$find='1';
					endif;
					

                    if($find=='1'):
                         $admin_user->set_error();   
                         $admin_user->set_pass_msg('Sorry, this user with this Username already exists.');
                         Redirect(make_admin_url('user', 'list', 'insert'));    
                    else:
                         $QueryObj = new user();
                         $QueryObj->saveUser($_POST);
                         $admin_user->set_pass_msg('New User has been created successfully.');
                         Redirect(make_admin_url('user', 'list', 'list'));  
                    endif;
		endif;	
	 break;

	case'update':
		
		$QueryObj = new user();
		$page_cotent=$QueryObj->getUser($id);   	
	
		if(isset($_POST['submit'])):
                    $find='0';
					#check username from school table
					$QueryObSch = new school();
					$SchObj=$QueryObSch->check_user_name_exists($_POST['username']);				
					
					#check username from user table
					$QueryObj11 = new user();
					$UserObj=$QueryObj11->check_user_name_exists_But_Not_this($_POST['username'],$_POST['id']);					
					
					if(is_object($SchObj)):
						$find='1';
					elseif(is_object($UserObj)):
						$find='1';
					endif;
					

                    if($find=='1'):
                         $admin_user->set_error();   
                         $admin_user->set_pass_msg('Sorry,User with this Username already exists.');
                         Redirect(make_admin_url('user', 'update', 'update&id='.$_POST['id']));    
                    else:
                         $QueryObj = new user();
                         $QueryObj->saveUser($_POST);
                         $admin_user->set_pass_msg('New User has been created successfully.');
                         Redirect(make_admin_url('user', 'list', 'list'));  
                    endif;
		endif;	
	 break;
	 
	case'delete':
		$QueryObj = new user();
		$QueryObj->deleteRecord($id);
		$admin_user->set_pass_msg('User has been deleted successfully.');
		Redirect(make_admin_url('user', 'list', 'list'));		
    break;      
	case'restore':

	break;

	case'thrash':

	break;
	default:break;
endswitch;
?>
