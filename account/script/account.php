<?php
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = '';
isset($_REQUEST['from']) ? $from = $_REQUEST['from'] : $from = '';
isset($_REQUEST['to']) ? $to = $_REQUEST['to'] : $to = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = '';
#handle actions here.

$modName = 'account';
if (empty($from) && empty($to)):
    $to = date('Y-m-d');
    $from = date('Y-m-d', strtotime($to . ' -3 months'));
elseif (!empty($from) && empty($to)):
    $to = date('Y-m-d', strtotime($from . ' +3 months'));
elseif (empty($from) && !empty($to)):
    $from = date('Y-m-d', strtotime($to . ' -3 months'));
endif;



switch ($action):
    case'list':
        if (!empty($from) && !empty($to)):
            $time2 = strtotime($from);
            if (strtotime($to) > strtotime(date('Y-m-d', $time2) . ' +3 months')):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_NOT_ALLLOWED_MORE_THEN_THREE_MONTH);
                Redirect(make_admin_url('account', 'list', 'list&from=&to=&cat=' . $cat));
            endif;
        endif;

        #Get Exp Category
        $QueryExpCat = new schoolExpenseCategory();
        $QueryExpCat->listAll($school->id);

        #Get Records
        $QueryObj = new schoolExpense();
        $record = $QueryObj->ExpenseReport($school->id, $from, $to, $cat);

        break;

    case'list_rep':

        #Get Exp Category
        $QueryExpCat = new schoolExpenseCategory();
        $QueryExpCat->listAll($school->id);

        #Get Records
        $QueryObj = new schoolExpense();
        $record = $QueryObj->ExpenseReport($school->id, $from, $to, $cat);

        break;

    case'update':
        if (!empty($from) && !empty($to)):
            $time2 = strtotime($from);
            if (strtotime($to) > strtotime(date('Y-m-d', $time2) . ' +3 months')):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_NOT_ALLLOWED_MORE_THEN_THREE_MONTH);
                Redirect(make_admin_url('account', 'update', 'update&from=&to=&cat=' . $cat));
            endif;
        endif;

        #Get Exp Category
        $QueryIncCat = new schoolIncomeCategory();
        $QueryIncCat->listAll($school->id);

        #Get Records
        $QueryObj = new schoolIncome();
        $record = $QueryObj->IncomeReport($school->id, $from, $to, $cat);
        break;

    case'update_rep':
        #Get Exp Category
        $QueryIncCat = new schoolIncomeCategory();
        $QueryIncCat->listAll($school->id);

        #Get Records
        $QueryObj = new schoolIncome();
        $record = $QueryObj->IncomeReport($school->id, $from, $to, $cat);
        break;

    case'insert':
        if (!empty($from) && !empty($to)):
            $time2 = strtotime($from);
            if (strtotime($to) > strtotime(date('Y-m-d', $time2) . ' +3 months')):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_NOT_ALLLOWED_MORE_THEN_THREE_MONTH);
                Redirect(make_admin_url('account', 'insert', 'insert&from=&to=&cat=' . $cat));
            endif;
        endif;

        #Get Exp Category
        $QuerySalCat = new staffCategory();
        $QuerySalCat->listAll($school->id);

        #Get Records
        $QueryObj = new staffSalary();
        $record = $QueryObj->SalaryReport($school->id, $from, $to, $cat);

        break;

    case'insert_rep':
        #Get Exp Category
        $QuerySalCat = new staffCategory();
        $QuerySalCat->listAll($school->id);

        #Get Records
        $QueryObj = new staffSalary();
        $record = $QueryObj->SalaryReport($school->id, $from, $to, $cat);

        break;

    case'view':
        #get_all session 
        $QuerySession = new session();
        $QuerySession->listAll($school->id, '1');

        /* Get section Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);

        if (!empty($from) && !empty($to)):
            $time2 = strtotime($from);
            if (strtotime($to) > strtotime(date('Y-m-d', $time2) . ' +3 months')):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_NOT_ALLLOWED_MORE_THEN_THREE_MONTH);
                Redirect(make_admin_url('account', 'view', 'view&from=&to=&cat=' . $cat));
            endif;
        endif;


        #Get Student previous Fee List
        $QueryRec = new studentFeeRecord();
        $record = $QueryRec->listReport($school->id, $from, $to, $session_id, $ct_sec);

        break;

    case'view_rep':
        #Get Student previous Fee List
        $QueryRec = new studentFeeRecord();
        $record = $QueryRec->listReport($school->id, $from, $to, $session_id, $ct_sec);

        break;

    case'report':
        if (!empty($from) && !empty($to)):
            $time2 = strtotime($from);
            if (strtotime($to) > strtotime(date('Y-m-d', $time2) . ' +3 months')):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_NOT_ALLLOWED_MORE_THEN_THREE_MONTH);
                Redirect(make_admin_url('account', 'report', 'report&from=&to=&cat='));
            endif;
        endif;

        #Get Exp Records
        $QueryObjExp = new schoolExpense();

        $CatExpRecord = $QueryObjExp->CatExpenseReport($school->id, $from, $to);


        #Get Salary Exp Records
        $QuerySalObj = new staffSalary();
        $SalRecord = $QuerySalObj->SumSalaryReport($school->id, $from, $to);


        #Get Income Records
        $QueryObjInc = new schoolIncome();
        $CatIncRecord = $QueryObjInc->CatIncomeReport($school->id, $from, $to);


        #Get Fee Income Records
        $QueryFeeObj = new studentFeeRecord();
        $FeeRecord = $QueryFeeObj->OverAlllistReport($school->id, $from, $to);
        $exp_amount = '';
        $inc_amount = '';


        break;


    case'report_rep':

        #Get Exp Records
        $QueryObjExp = new schoolExpense();
        $CatExpRecord = $QueryObjExp->CatExpenseReport($school->id, $from, $to);


        #Get Salary Exp Records
        $QuerySalObj = new staffSalary();
        $SalRecord = $QuerySalObj->SumSalaryReport($school->id, $from, $to);

        #Get Income Records
        $QueryObjInc = new schoolIncome();
        $CatIncRecord = $QueryObjInc->CatIncomeReport($school->id, $from, $to);

        #Get Fee Income Records
        $QueryFeeObj = new studentFeeRecord();
        $FeeRecord = $QueryFeeObj->OverAlllistReport($school->id, $from, $to);
        $exp_amount = '';
        $inc_amount = '';

        break;

    case'download':
        if ($type == 'expense'):
            $QueryObj = new schoolExpense();
            $record = $QueryObj->ExpenseReportDownload($school->id, $from, $to, $cat);
        elseif ($type == 'income'):
            $QueryObj = new schoolIncome();
            $record = $QueryObj->IncomeReportDownload($school->id, $from, $to, $cat);
        elseif ($type == 'salary'):
            $QueryObj = new staffSalary();
            $record = $QueryObj->SalaryReportDownload($school->id, $from, $to, $cat);
        elseif ($type == 'fee'):
            $QueryRec = new studentFeeRecord();
            $record = $QueryRec->listReportDownload($school->id, $from, $to, $session_id, $ct_sec);
        elseif ($type == 'full'):
            #Get Exp Records
            $QueryObjExp = new schoolExpense();
            $CatExpRecord = $QueryObjExp->CatExpenseReport($school->id, $from, $to);


            #Get Salary Exp Records
            $QuerySalObj = new staffSalary();
            $SalRecord = $QuerySalObj->SumSalaryReport($school->id, $from, $to);

            #Get Income Records
            $QueryObjInc = new schoolIncome();
            $CatIncRecord = $QueryObjInc->CatIncomeReport($school->id, $from, $to);

            #Get Fee Income Records
            $QueryFeeObj = new studentFeeRecord();
            $FeeRecord = $QueryFeeObj->OverAlllistReport($school->id, $from, $to);

            #Generate Report
            $QueryRec = new school();
            $record = $QueryRec->FullReportDownload($CatExpRecord, $SalRecord, $CatIncRecord, $FeeRecord, $from, $to);

        endif;
        break;

    case'pdf':
        if ($type == 'expense'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $car_pdf_name = 'ExpenseReport';
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&temp=account&file=list_pdf&from=' . $from . '&to=' . $to . '&cat=' . $cat;
            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'landscape');
            $dompdf->render();
            $dompdf->stream($car_pdf_name . ".pdf");
            exit;
        elseif ($type == 'income'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $car_pdf_name = 'IncomeReport';
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&temp=account&file=edit_pdf&from=' . $from . '&to=' . $to . '&cat=' . $cat;
            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'landscape');
            $dompdf->render();
            $dompdf->stream($car_pdf_name . ".pdf");
            exit;
        elseif ($type == 'salary'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $car_pdf_name = 'SalaryReport';
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&temp=account&file=create_pdf&from=' . $from . '&to=' . $to . '&cat=' . $cat;
            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'landscape');
            $dompdf->render();
            $dompdf->stream($car_pdf_name . ".pdf");
            exit;
        elseif ($type == 'fee'):
            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $car_pdf_name = 'FeeReport';
            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&temp=account&file=view_pdf&from=' . $from . '&to=' . $to . '&session_id=' . $session_id . '&ct_sec=' . $ct_sec;
            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'landscape');
            $dompdf->render();
            $dompdf->stream($car_pdf_name . ".pdf");
            exit;
        elseif ($type == 'full'):

            set_time_limit(500000);
            require_once(DIR_FS_SITE_PUBLIC . ADMIN_FOLDER . '/dompdf/dompdf_config.inc.php');
            $car_pdf_name = 'FullReport';

            $post_url = DIR_WS_SITE . '' . ADMIN_FOLDER . '/download_window.php?school_id=' . $school->id . '&temp=account&file=report_pdf&from=' . $from . '&to=' . $to;


            $html = file_get_contents($post_url);
            $dompdf = new DOMPDF();
            $dompdf->load_html(html_entity_decode($html));
            $dompdf->set_paper('27x21', 'landscape');
            $dompdf->render();
            $dompdf->stream($car_pdf_name . ".pdf");
            exit;
        endif;
        break;


    default:break;
endswitch;
?>