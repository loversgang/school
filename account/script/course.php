<?php
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');

$modName='course';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';

	$QuerySvhoolCourse= new course();
	$QuerySvhoolCourse->listAll($school->id);
	$total_course=$QuerySvhoolCourse->GetNumRows(); 
	
#handle actions here.
switch ($action):
	case'list':
		$QueryObj = new course();
		$QueryObj->listAll($school->id);
		break;
	case'insert':
		if(isset($_POST['submit'])):
			
			if(!empty($school_setting->max_courses_allowed) && $total_course>=$school_setting->max_courses_allowed):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg(MSG_MAX_COURSE_PART_1.$school_setting->max_courses_allowed.MSG_MAX_COURSE_PART_2);
                    Redirect(make_admin_url('course', 'list', 'list'));   			
			endif;
		            
		
                    $QueryObj = new course();
                    $QueryObj->saveData($_POST);
                    $admin_user->set_pass_msg(MSG_COURSE_ADDED);
                    Redirect(make_admin_url('course', 'list', 'list'));
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg(OPERATION_CANCEL);
                    Redirect(make_admin_url('course', 'list', 'list'));   
		endif;
		break;
	case'update':
		$QueryObj = new course();
		$object=$QueryObj->getRecord($id);                
		if(isset($_POST['submit'])):
                    $QueryObj = new course();
                    $QueryObj->saveData($_POST);
                    $admin_user->set_pass_msg(MSG_COURSE_UPDATE);
                    Redirect(make_admin_url('course', 'list', 'list'));
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg(OPERATION_CANCEL);
                    Redirect(make_admin_url('course', 'list', 'list'));      
		endif;
		break;

	 case'delete':
		$QueryObj = new course();
		$QueryObj->deleteRecord($id);
		$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
		Redirect(make_admin_url('course', 'list', 'list'));  
		break;
	
	default:break;
endswitch;
?>
