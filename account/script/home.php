<?php

/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/applicationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/messageClass.php');

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['oby']) ? $oby = $_GET['oby'] : $oby = 'order_date';
isset($_GET['so']) ? $so = $_GET['so'] : $so = 'ASC';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['update_id']) ? $update_id = $_GET['update_id'] : $update_id = '0';
isset($_GET['g_sort']) ? $g_sort = $_GET['g_sort'] : $g_sort = '';

#handle actions here.
switch ($action):
    case'list':
        #Get Current Year Session With Students
        $SessStu = new studentSession();
        $session_st = $SessStu->get_current_session_with_students_with_boys_girls($school->id);

        // Get Total Students
        $totalFemaleStudents = studentSession::getTotalStudentCount($school->id, 'Female');
        $totalMaleStudents = studentSession::getTotalStudentCount($school->id, 'Male');
        $totalStudents = $totalMaleStudents + $totalFemaleStudents;

        #Get Current Year session Fee record 
        $SessFee = new studentSession();
        $session_fee = $SessFee->get_current_session_fee_record_with_balance_and_paid($school->id);

        #Get Daily attendance report
        $SessAtt = new attendance();
        $dailyAttendance = $SessAtt->get_current_session_attendance($school->id);

        $object = new application;
        $listStaff = $object->listStaffApplications();

        $object = new application;
        $listStudents = $object->listStudentApplications();

        $inbox_count = school_header::getInboxCountUnread($school->id, $id, 'admin');
        $outbox_count = school_header::getOutboxCountTotal($school->id, $id, 'admin');

        $admin_inbox_count = school_header::getInboxCount($school->id, $id, 'admin');
        $admin_outbox_count = school_header::getOutboxCount($school->id, $id, 'admin');

        $inbox_count = school_header::getInboxCountUnread($school->id, $id, 'admin');
        $outbox_count = school_header::getOutboxCountTotal($school->id, $id, 'admin');

        $obj = new school_header;
        $inbox_messages = $obj->getInBoxMessagesAdmin($school->id, $id);


        break;

    case'insert':
        break;
    case'update':
        break;
    case'delete':
        break;
    default:break;
endswitch;
?>
