<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
$modName = 'income';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';


$QueryCat = new schoolIncomeCategory();
$QueryCat->listAll($school->id);

// List All Expense Categories
$obj = new schoolIncomeCategory;
$cats = $obj->listAllIncomeCat($school->id);

$obj = new schoolIncome;
$income_years = $obj->listIncomeYears($school->id);

#handle actions here.
switch ($action):
    case'list':
        $QueryObj = new schoolIncome();
        $record = $QueryObj->listAll($school->id);
        break;
    case'insert':
        if (isset($_POST['submit'])):
            $files_array = array();
            for ($i = 0; $i < count($_FILES['doc']['name']); $i++) {
                $files_array[] = array(
                    'title' => $_POST['doc_title'][$i],
                    'name' => $_FILES['doc']['name'][$i],
                    'tmp_name' => $_FILES['doc']['tmp_name'][$i],
                    'error' => $_FILES['doc']['error'][$i]
                );
            }
            $QueryObj = new schoolIncome();
            $income_id = $QueryObj->saveData($_POST);
            foreach ($files_array as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/income_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['income_id'] = $income_id;
                        $arr['title'] = $file['title'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new income_docs;
                        $obj->saveIncomeDoc($arr);
                    endif;
                endif;
            endforeach;
            $admin_user->set_pass_msg(MSG_INCOME_ADDED);
            Redirect(make_admin_url('income', 'list', 'list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('income', 'list', 'list'));
        endif;
        break;
    case'update':
        $QueryObj = new schoolIncome();
        $object = $QueryObj->getRecord($id);

        // List Expense Docs
        $obj = new income_docs;
        $income_docs = $obj->listIncomeDocs($id);

        if (isset($_POST['submit'])):
            // Update Old Docs
            $titles_array = array_combine($_POST['doc_id'], $_POST['title_old']);
            foreach ($titles_array as $id => $title) {
                $array['id'] = $id;
                $array['title'] = $title;
                $obj = new income_docs;
                $obj->saveIncomeDoc($array);
            }
            $files_array_old = array();
            for ($i = 0; $i < count($_FILES['doc_old']['name']); $i++) {
                $files_array_old[] = array(
                    'id' => $_POST['doc_id'][$i],
                    'name' => $_FILES['doc_old']['name'][$i],
                    'tmp_name' => $_FILES['doc_old']['tmp_name'][$i],
                    'error' => $_FILES['doc_old']['error'][$i]
                );
            }
            foreach ($files_array_old as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/income_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['id'] = $file['id'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new income_docs;
                        $obj->saveIncomeDoc($arr);
                    endif;
                endif;
            endforeach;
            // Save Docs
            $files_array = array();
            for ($i = 0; $i < count($_FILES['doc']['name']); $i++) {
                $files_array[] = array(
                    'title' => $_POST['doc_title'][$i],
                    'name' => $_FILES['doc']['name'][$i],
                    'tmp_name' => $_FILES['doc']['tmp_name'][$i],
                    'error' => $_FILES['doc']['error'][$i]
                );
            }
            foreach ($files_array as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/income_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['income_id'] = $_POST['id'];
                        $arr['title'] = $file['title'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new income_docs;
                        $obj->saveIncomeDoc($arr);
                    endif;
                endif;
            endforeach;
            $QueryObj = new schoolIncome();
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_INCOME_UPDATE);
            Redirect(make_admin_url('income', 'list', 'list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('income', 'list', 'list'));
        endif;
        break;

    case'delete':
        $QueryObj = new schoolIncome();
        $QueryObj->deleteRecord($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('income', 'list', 'list'));
        break;

    case'cat_list':
        $QueryObj = new schoolIncomeCategory();
        $QueryObj->listAll($school->id);
        break;

    case'cat_insert':
        if (isset($_POST['submit'])):
            $QueryObj = new schoolIncomeCategory();
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_INCOME_CAT_ADDED);
            Redirect(make_admin_url('income', 'cat_list', 'cat_list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('income', 'cat_list', 'cat_list'));
        endif;
        break;
    case'cat_update':
        $QueryObj = new schoolIncomeCategory();
        $object = $QueryObj->getRecord($id);

        if (isset($_POST['submit'])):
            $QueryObj = new schoolIncomeCategory();
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_INCOME_CAT_UPDATE);
            Redirect(make_admin_url('income', 'cat_list', 'cat_list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('income', 'cat_list', 'cat_list'));
        endif;
        break;

    case'cat_delete':
        $QueryObj = new schoolIncomeCategory();
        $QueryObj->deleteRecord($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('income', 'cat_list', 'cat_list'));
        break;

    default:break;
endswitch;
