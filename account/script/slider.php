<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/sliderClass.php');

$modName='slider';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
$p=isset($_GET['p'])?$_GET['p']:1;
$max_records='8';/*set maximum records on a page*/
/*get logged in user*/
$user_id=LOGIN_USER_ID;
                
switch ($action):
	case'list':
		 $QueryObj = new slider();
                 $QueryObj->setUserId($user_id);
                 $QueryObj->enablePaging($p, $max_records);
	         $QueryObj->listImages();
                 $total_records= $QueryObj->TotalRecords;
                 $total_pages= $QueryObj->TotalPages;
		 break;
	case'insert':
		 if(isset($_POST['submit'])):
                    $QueryObj = new slider();
                    $getID=$QueryObj->saveImage($_POST,$user_id);
                                  
                    $admin_user->set_pass_msg('Slider Image has been inserted successfully.');
		    Redirect(make_admin_url('slider', 'list', 'list'));
                 elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();    
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('slider', 'list', 'list'));     
		 endif;
                break;
         case 'update2': 
                if(is_var_set_in_post('submit_position')):
                       
                    foreach ($_POST['position'] as $k=>$v):
                            $q= new query('user_slider');
                            $q->Data['id']=$k;
                            $q->Data['position']=$v;
                            $q->Update();
                    endforeach;
                endif;
                 
                 if(isset($_POST['multiopt_go']) && $_POST['multiopt_go']=='Go'):

                    if($_POST['multiopt_action']=='delete'):
                        if(count($_POST['multiopt'])):
                            foreach($_POST['multiopt'] as $k=>$v):
                                    $query= new query('user_slider');
                                    $query->Data['id']="$k";
                                    $query->Data['is_deleted']='1';
                                    $query->Update();

                            endforeach;
                         else:
                            $admin_user->set_error();   
                            $admin_user->set_pass_msg('Sorry, Please select atleast one item for operation');
                            Redirect(make_admin_url('slider', 'list', 'list')); 
                            
                         endif;   
                      
                    endif;
                endif;
            
                $admin_user->set_pass_msg('Operation has been performed successfully');
                Redirect(make_admin_url('slider', 'list', 'list'));
                break;  
                
	case'update':
		$QueryObj = new slider();
                $QueryObj->setUserId($user_id);
		$slider=$QueryObj->getImageByID($id,0);  
	 
                               
		if(isset($_POST['submit'])):
                    $QueryObj = new slider();
                    $getID=$QueryObj->saveImage($_POST,$user_id);
                                    
                    $admin_user->set_pass_msg('Slider Image has been updated successfully.');
		    Redirect(make_admin_url('slider', 'list', 'list'));
                  elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg('The operation has been cancelled');
                    Redirect(make_admin_url('slider', 'list', 'list'));     
	        endif;   
		break;
                
         case'change_status':
                $QueryObj = new slider();
                $QueryObj->setUserId($user_id);
		$QueryObj->changeStatus($id);
		$admin_user->set_pass_msg('Status has been changes successfully.');
		Redirect(make_admin_url('slider', 'list', 'list'));
                break;
            
	case'delete':
		$QueryObj = new slider();
		$QueryObj->deleteObject($id);
		$admin_user->set_pass_msg('Slider Image has been deleted successfully.');
		Redirect(make_admin_url('slider', 'list', 'list'));
		break;
        case 'delete_image':
                if($id){
                    $object= get_object('user_slider', $id);
                    $QueryObj= new query('user_slider');
                    $QueryObj->Data['image']='';
                    $QueryObj->Data['id']=$id;
                    $QueryObj->Update();

                    #delete images from all folders
                    $image_obj=new imageManipulation();
                    $image_obj->DeleteImagesFromAllFolders('slider',$object->image);

               
                    /*End Clear Cache code*/
                    $admin_user->set_pass_msg('Slider Image Image has been successfully deleted.');
                    Redirect(make_admin_url('slider', 'update', 'update','id='.$id));
                }
                
        case'permanent_delete':
                $object= get_object('user_slider', $id);
                #delete images from all folders
                $image_obj=new imageManipulation();
                $image_obj->DeleteImagesFromAllFolders('slider',$object->image);
                    
		$QueryObj = new slider();
		$QueryObj->purgeObject($id);
		$admin_user->set_pass_msg('Slider Image has been deleted successfully');
		Redirect(make_admin_url('slider', 'thrash', 'thrash'));
        	break;
          
        case'restore':
            $QueryObj = new slider();
            $QueryObj->restoreObject($id);
            $admin_user->set_pass_msg('Slider Image has been restored successfully');
            Redirect(make_admin_url('slider', 'thrash', 'thrash'));
            break;

        case'thrash':
            $QueryObj = new slider();
            $QueryObj->setUserId($user_id);
            $QueryObj->getThrash();
            break;
	default:break;
endswitch;
?>
