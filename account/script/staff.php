<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');

$modName = 'staff';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['active']) ? $active = $_GET['active'] : $active = '1';
isset($_GET['doc_id']) ? $doc_id = $_GET['doc_id'] : $doc_id = '';

#get_all_staff designation 
$QueryDes = new staffDesignation();
$QueryDes->listAll($school->id, '1');
#get_all_staff Category 
$QueryCat = new staffCategory();
$QueryCat->listAll($school->id, '1');

// Pay Rules
// Get Earning Rules
$obj = new staffPayHeads;
$earningHeads = $obj->getPayRuleByType('earning');

// Get Deduction Rules
$object = new staffPayHeads;
$deductionHeads = $object->getPayRuleByType('deduction');
#handle actions here.
switch ($action):
    case'list':

        /* Get school document template for staff id card */
        $QueryDocs = new SchoolDocumentTemplate();
        $staff_icard = $QueryDocs->get_school_single_document($school->id, 2);

        $QueryObj = new staff();
        $record = $QueryObj->getSchoolStaffDetailActiveDeactive($school->id, $active);
        break;

    case'list1':

        /* Get school document template for staff id card */
        $QueryDocs = new SchoolDocumentTemplate();
        $staff_icard = $QueryDocs->get_school_single_document($school->id, 2);

        $QueryObj = new staff();
        $record = $QueryObj->getSchoolOldStaffDetail($school->id, $active);
        break;

    case'insert':
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMONOPQRSTUVWXYZ0123456789';
        $password = '';
        for ($i = 0; $i < 10; $i++) {
            $password .= $characters[rand(0, strlen($characters) - 1)];
        }

        if (isset($_POST['submit'])):
            if (isset($_POST['earning'])) {
                $_POST['earning_heads'] = maybe_serialize($_POST['earning']);
            } else {
                $_POST['earning_heads'] = '';
            }
            if (isset($_POST['deduction'])) {
                $_POST['deduction_heads'] = maybe_serialize($_POST['deduction']);
            } else {
                $_POST['deduction_heads'] = '';
            }
            # Add New Staff 
            $QueryObj = new staff();
            $staff_id = $QueryObj->saveData($_POST);

            $query = new staff;
            $query->Data['staff_login_id'] = $school_setting->reg_id_prefix . 'STF' . $staff_id;
            $query->Data['password'] = $password;
            $query->Where = " WHERE `id` = $staff_id";
            $query->UpdateCustom();

            # Add Qualification 
            $QbSub = new staffQualification();
            $QbSub->saveData($_POST['qualification'], $staff_id);

            # Add Experience 
            $QbExp = new staffExperience();
            $QbExp->saveData($_POST['experience'], $staff_id);

            # Add Documents 
            $QbDoc = new staffDocument();
            $QbDoc->saveData($_POST['document'], $staff_id);

            $admin_user->set_pass_msg(MSG_STAFF_ADDED);
            Redirect(make_admin_url('staff', 'list', 'list'));
        endif;
        break;
    case'update':

        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMONOPQRSTUVWXYZ0123456789';
        $password = '';
        for ($i = 0; $i < 10; $i++) {
            $password .= $characters[rand(0, strlen($characters) - 1)];
        }
        #Get Staff
        $QueryStaff = new staff();
        $staff = $QueryStaff->getStaff($id);

        // Get Staff Pay Rule
        $earning_heads = unserialize(html_entity_decode($staff->earning_heads));
        if (!is_array($earning_heads)) {
            $earning_heads = array();
        }

        $deduction_heads = unserialize(html_entity_decode($staff->deduction_heads));
        if (!is_array($deduction_heads)) {
            $deduction_heads = array();
        }


        #Get Staff Qualification
        $QueryQual = new staffQualification();
        $QueryQual->getStaffQualification($id);

        #Get Staff Experience
        $QueryExp = new staffExperience();
        $QueryExp->getStaffExp($id);

        #Get Staff Document
        $QueryDoc = new staffDocument();
        $QueryDoc->getStaffDocument($id);

        if (isset($_POST['submit'])):
            if (isset($_POST['earning'])) {
                $_POST['earning_heads'] = maybe_serialize($_POST['earning']);
            } else {
                $_POST['earning_heads'] = '';
            }
            if (isset($_POST['deduction'])) {
                $_POST['deduction_heads'] = maybe_serialize($_POST['deduction']);
            } else {
                $_POST['deduction_heads'] = '';
            }
            # Update Staff 
            $QueryObj = new staff();
            $staff_id = $QueryObj->saveData($_POST);

            if ($staff->staff_login_id == '') {
                $query = new staff;
                $query->Data['staff_login_id'] = $school_setting->reg_id_prefix . 'STF' . $staff_id;
                $query->Data['password'] = $password;
                $query->Where = " WHERE `id` = $id";
                $query->UpdateCustom();
            }
            # Add Qualification 
            $QbSub = new staffQualification();
            $QbSub->saveData($_POST['qualification'], $staff_id);

            # Add Experience 
            $QbExp = new staffExperience();
            $QbExp->saveData($_POST['experience'], $staff_id);

            # Add Documents 
            $QbDoc = new staffDocument();
            $QbDoc->saveData($_POST['document'], $staff_id);

            $admin_user->set_pass_msg(MSG_STAFF_UPDATE);
            Redirect(make_admin_url('staff', 'list', 'list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('staff', 'list', 'list'));
        endif;
        break;

    case'view':
        #Get Staff
        $QueryStaff = new staff();
        $staff = $QueryStaff->getStaff_detail($id);

        #Get document
        $QueryDoc = new document();
        $doc = $QueryDoc->getRecord($doc_id);

        $im_obj = new imageManipulation();
        if ($staff->photo):
            $staff_image = "<img src='" . $im_obj->get_image_echo('staff', 'medium', $staff->photo) . "'/>";
        else:
            $staff_image = '<img src="assets/img/profile/img-1.jpg"/>';
        endif;
        if ($staff->school_logo):
            $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $staff->school_logo) . "' style='max-width:86px;max-height:89px;'/>";
        else:
            $school_logo = '<img src="assets/img/invoice/webgarh.png"/>';
        endif;
        //echo $staff_image; exit;

        $replace = array('SCHOOL' => $staff->school_name,
            'STAFF_NAME' => $staff->title . " " . $staff->first_name . " " . $staff->last_name,
            'DESIGNATION' => $staff->designation,
            'DEPARTMENT' => $staff->staff_category,
            'STAFF_PHOTO' => $staff_image,
            'LOGO' => $school_logo
        );

        $content = '';
        if (is_object($doc)):
            $content = $doc->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = '{' . trim(strtoupper($k)) . '}';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                endforeach;
            endif;

            /* Now show the last date of document printing if they have */

            #Get last printing date
            $QueryStPrint = new staff();
            $printHist = $QueryStPrint->getLastDatePrinting($school->id, $id, $doc_id);
            if (!empty($printHist)):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_LAST_DOC_PRINT . date('d M, Y', strtotime($printHist)) . '.');
            endif;
        endif;

        break;

    case'confirm_print':
        #Get document
        $QueryDoc = new document();
        $doc = $QueryDoc->getRecord($doc_id);

        #Add record
        $_POST['staff_id'] = $id;
        $_POST['school_id'] = $school->id;
        $_POST['document_id'] = $_GET['doc_id'];
        $_POST['doc_type'] = $doc->type;
        $QuerySess = new SchoolDocumentPrintHistory();
        $QuerySess->saveData($_POST);

        $admin_user->set_pass_msg(MSG_DOCUMENT_GENERATETD);
        Redirect(make_admin_url('staff', 'view', 'view&id=' . $id . '&doc_id=' . $doc_id));
        break;
    case'delete_image':
        #delete staff image
        $QueryObj = new staff();
        $QueryObj->deleteImage($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('staff', 'update', 'update&id=' . $id));
        break;

    case'delete_qualification':
        #delete staff Qualification
        $QueryObj = new staffQualification();
        $QueryObj->deleteRecord($_GET['q_id']);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('staff', 'update', 'update&id=' . $id));
        break;

    case'delete_exp':
        #delete staff Experience
        $QueryObj = new staffExperience();
        $QueryObj->deleteRecord($_GET['e_id']);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('staff', 'update', 'update&id=' . $id));
        break;

    case'delete_doc':
        #delete staff Document
        $QueryObj = new staffDocument();
        $QueryObj->deleteRecord($_GET['d_id']);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('staff', 'update', 'update&id=' . $id));
        break;

    case'delete':
        $QueryObj = new staff();
        $QueryObj->deleteRecord($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('staff', 'list', 'list'));
        break;


    default:break;
endswitch;
?>