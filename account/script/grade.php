<?php

include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');


isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';

isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';

$modName = 'grade';
#handle actions here.
switch ($action):
    case'list':
        # grade records
        $QueryObj = new examGrade;
        $grades = $QueryObj->listAll($school->id);
        break;

    case'insert':
        # ADD Fee Heads Record
        if (isset($_POST['submit'])):
            $QueryObj = new examGrade;
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_GRADE_HEADS_ADDED);
            Redirect(make_admin_url('grade', 'list', 'list'));
        endif;
        break;

    case'update':
        # edit fee heads
        $QuObj = new examGrade;
        $object = $QuObj->getRecord($id);
        if (isset($_POST['submit'])):
            $QueryObj = new examGrade;
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_GRADE_HEAD_UPDATE);
            Redirect(make_admin_url('grade', 'list', 'list'));
        endif;
        break;

    case'delete':
        # delete record		
        $QueryObj = new examGrade();
        $QueryObj->deleteRecord($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('grade', 'list', 'list'));
        break;

    default:break;

endswitch;
?>

