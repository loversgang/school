<?php

include_once(DIR_FS_SITE . 'include/functionClass/messageClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
$modName = 'messages';
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['m_type']) ? $m_type = $_GET['m_type'] : $m_type = 'inbox';
isset($_GET['h_id']) ? $h_id = $_GET['h_id'] : $h_id = '';
isset($_GET['id']) ? $id = $_GET['id'] : $id = $_SESSION['admin_session_secure']['user_id'];
isset($_GET['type']) ? $type = $_GET['id'] : $type = $_SESSION['admin_session_secure']['login_type'];
$page = isset($_GET['Page']) ? $_GET['Page'] : $page = '';
if ($id != $_SESSION['admin_session_secure']['user_id']) {
    Redirect(make_admin_url('home'));
}
#handle actions here.
switch ($action):
    case 'list':
        $admin_inbox_count = school_header::getInboxCount($school->id, $id, 'admin');
        $admin_outbox_count = school_header::getOutboxCount($school->id, $id, 'admin');

        $inbox_count = school_header::getInboxCountUnread($school->id, $id, 'admin');
        $outbox_count = school_header::getOutboxCountTotal($school->id, $id, 'admin');

        $obj = new school_header;
        $inbox_messages = $obj->getInBoxMessagesAdmin($school->id, $id);

        $obj = new school_header;
        $outbox_messages = $obj->getOutBoxMessagesAdmin($school->id, $id);

        break;
    case 'compose':
        if (isset($_GET['user_id'])) {
            $CurrObj = new studentSession();
            $current_session = $CurrObj->getStudentCurrentSessionInfo($school->id, $_GET['user_id']);
        }

        if (isset($_POST['submit'])) {
            // Save Header
            $header_array['school_id'] = $_POST['school_id'];
            $header_array['from_id'] = $_POST['from_id'];
            $header_array['from_type'] = $_POST['from_type'];
            $header_array['to_id'] = $_POST['to_id'];
            $header_array['to_type'] = $_POST['to_type'];
            $header_array['subject'] = $_POST['subject'];
            $obj = new school_header;
            $header_id = $obj->saveHeader($header_array);

            // Save Message
            $message['school_id'] = $_POST['school_id'];
            $message['header_id'] = $header_id;
            $message['content'] = $_POST['content'];
            $obj = new messages;
            $obj->saveMessage($message);
            $admin_user->set_pass_msg("Message Sent Successfully!");
            Redirect(make_admin_url('messages', 'list', 'list'));
        }
        if (isset($_GET['user_id']) && $_GET['user_id'] != '') {
            if (isset($_GET['user_type']) && $_GET['user_type'] == 'student') {
                $to_id = $_GET['user_id'];
                $student = get_object('student', $_GET['user_id']);
                $to_name = $student->first_name . ' ' . $student->last_name;
            }
            if (isset($_GET['user_type']) && $_GET['user_type'] == 'staff') {
                $to_id = $_GET['user_id'];
                $staff_detail = get_object('staff', $_GET['user_id']);
                $to_name = $staff_detail->title . ' ' . $staff_detail->first_name . ' ' . $staff_detail->last_name;
            }
        }
        break;
    case 'getlist':
        if (isset($_POST['user_submit'])) {
            extract($_POST);
            if ($user_type == 'student') {
                Redirect(make_admin_url('student', 'list', 'list'));
            }
            if ($user_type == 'staff') {
                Redirect(make_admin_url('staff', 'list', 'list'));
            }
        }
        break;
    case'read':
        $auth = school_header::checkAuth($school->id, $id, 'admin', $h_id);
        if (!$auth) {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Something Went Wrong!");
            Redirect(make_admin_url('messages', 'list', 'list'));
        }
        $header = get_object('school_header', $h_id);
        // Get Message Details
        $obj = new messages;
        $message = $obj->getMessageDetails($h_id);

        if (isset($_GET['msg_box']) && $_GET['msg_box'] != '') {
            extract($_GET);
            if ($msg_box == 'inbox') {
                $arr['id'] = $message->id;
                $arr['is_read'] = 1;
                $obj = new messages;
                $obj->saveMessage($arr);
            }
            if (isset($act)) {
                if ($msg_box == 'inbox' && $act == 'delete') {
                    $obj = new school_header;
                    $obj->updateStatusAdminInbox($h_id, $id);
                }
                if ($msg_box == 'outbox' && $act == 'delete') {
                    $obj = new school_header;
                    $obj->updateStatusAdminOutbox($h_id, $id);
                }
                $admin_user->set_pass_msg("Message Deleted Successfully!");
                Redirect(make_admin_url('messages', 'list', 'list', 'm_type=' . $msg_box));
            }
        } else {
            $admin_user->set_error();
            $admin_user->set_pass_msg("Something Went Wrong!");
            Redirect(make_admin_url('messages', 'list', 'list'));
        }
        break;
    default:break;
endswitch;
