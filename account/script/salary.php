<?php
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/loanClass.php');

$modName = 'salary';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_GET['type']) ? $type = $_GET['type'] : $type = 'select';
isset($_GET['date']) ? $date = $_GET['date'] : $date = date('M Y');
isset($_REQUEST['staff_id']) ? $staff_id = $_REQUEST['staff_id'] : $staff_id = '0';
isset($_REQUEST['doc_id']) ? $doc_id = $_REQUEST['doc_id'] : $doc_id = '0';

$QueryStaff = new staffCategory();
$QueryStaff->listAll($school->id, '1');

$staff = get_object('staff', $staff_id);


$earning_heads = array();
if ($staff->earning_heads) {
    $earning_heads = unserialize(html_entity_decode($staff->earning_heads));
}

$deduction_heads = array();
if ($staff->deduction_heads) {
    $deduction_heads = unserialize(html_entity_decode($staff->deduction_heads));
}

$QueryDocsTmp = new SchoolDocumentTemplate();
$slip = $QueryDocsTmp->get_school_single_document($school->id, 7);

switch ($action):
    case'list':
        $QueryObj = new staffSalary();
        $record = $QueryObj->getSingleSraffSalary($school->id, $staff_id);

        break;
    case'insert':
        $query = new loan;
        $list_loans = $query->list_loans($staff_id);

        $start_year = date('Y');

        $query = new loan;
        $list_loans_not_paid = $query->list_loans_not_paid($staff_id, $start_year);

        if ($type == 'list'):
            $first_date_of_month = date('Y-m-d', strtotime('01 ' . $date));
            $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($first_date_of_month))));

            $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
            $total_month_days = (date('d', strtotime($last_date_of_month)));
            $working_days = $total_month_days - $total_sunday;

            $Q_obj = new staffAttendance();
            $r_atten = $Q_obj->GetStaffAttendace($staff_id, $first_date_of_month, $last_date_of_month);
            $t_attend = array_sum($r_atten);

            $M_obj = new schoolHolidays();
            $M_holiday = $M_obj->checkMonthlyHoliday($school->id, date('m', strtotime($first_date_of_month)), date('Y', strtotime($first_date_of_month)));
        endif;

        if (isset($_POST['submit'])):

            $query = new loan;
            $list_loans = $query->list_loans($staff_id);

            foreach ($list_loans as $loan) {
                if ($loan->installments_paid < $loan->total_installments) {
                    $query = new loan;
                    $query->installments_paid($loan->installments_paid, $loan->id);
                }
            }

            $QueryCheck = new staffSalary();
            $check = $QueryCheck->check_for_the_month_salary($_POST['payment_date'], $_POST['staff_id']);

            if (is_object($check)):
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_SALARY_ALREAY_ADDED_MONTH);
                Redirect(make_admin_url('salary', 'insert', 'insert&staff_id=' . $staff_id));
            endif;
            #print receipt no.			
            $QueryRec = new staffSalary();
            $_POST['receipt_no'] = $QueryRec->getMaxReciept($school->id);

            if (!$_POST['other_amount']) {
                $_POST['other_type'] = '';
            }
            $QueryObj = new staffSalary();
            $max_id = $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_SALARY_ADDED);

            if (is_object($slip)):
                Redirect(make_admin_url('salary', 'view', 'view&id=' . $max_id . '&staff_id=' . $staff_id . '&doc_id=' . $slip->id));
            else:
                Redirect(make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id));
            endif;

        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id));
        endif;
        break;
    case'update':
        $query = new loan;
        $list_loans = $query->list_loans($staff_id);
        $list_loans_not_paid = array();
        foreach ($list_loans as $loan) {
            $array[] = $loan;
            $query = new loan;
            $list_loans_not_paid = $query->list_loans_not_paid($staff_id, $loan->start_year);
        }

        $QueryObj = new staffSalary();
        $object = $QueryObj->getRecord($id);


        $first_date_of_month = date('Y-m-d', strtotime($object->month_year));
        $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($first_date_of_month))));

        $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
        $total_month_days = (date('d', strtotime($last_date_of_month)));
        $working_days = $total_month_days - $total_sunday;

        $Q_obj = new staffAttendance();
        $r_atten = $Q_obj->GetStaffAttendace($staff_id, $first_date_of_month, $last_date_of_month);
        $t_attend = array_sum($r_atten);

        $M_obj = new schoolHolidays();
        $M_holiday = $M_obj->checkMonthlyHoliday($school->id, date('m', strtotime($first_date_of_month)), date('Y', strtotime($first_date_of_month)));

        if (isset($_POST['submit'])):
            $QueryCheck = new staffSalary();
            $check = $QueryCheck->check_for_the_month_salary($_POST['payment_date'], $_POST['staff_id'], $_POST['id']);
//            if (is_object($check)):
//                $admin_user->set_error();
//                $admin_user->set_pass_msg(MSG_SALARY_ALREAY_ADDED_MONTH);
//                Redirect(make_admin_url('salary', 'update', 'update&id=' . $_GET['id'] . '&staff_id=' . $staff_id));
//            endif;
            $QueryObj = new staffSalary();
            $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_SALARY_UPDATE);
            Redirect(make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id));
        endif;

        $query = new staffSalary;
        $staff_salary_detail = $query->getStaffSalaryRecordsById($id, $staff_id);
        break;

    case'view':
        $QueryObj = new staffSalary();
        $object = $QueryObj->getRecord($id);
        ?>
        <?php ob_start(); ?>
        <table class="table table-striped table-bordered table-advance table-hover">
            <tbody>
                <?php $earningHeadTitle = array(); ?>
                <?php if ($earning_heads) { ?>
                    <?php foreach ($earning_heads as $k => $earning) { ?>
                        <?php
                        $earning_amount = $earning['amount'];
                        $earning_title = staffPayHeads::getFieldDetail($k, 'title');
                        $earningHeadTitle[$earning_title] = $earning_amount;
                        ?>
                        <tr>
                            <td style="text-transform: uppercase">
                                <?php echo $earning_title; ?>
                            </td>
                            <td>
                                <span class="pull-right sal_left">
                                    <i class="icon-inr"></i> <?= number_format($earning_amount, 2); ?>
                                </span>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>
        </table>
        <?php $earning_html = ob_get_clean(); ?>

        <?php ob_start(); ?>
        <table class="table table-striped table-bordered table-advance table-hover">
            <tbody>
                <?php $ed = array(); ?>
                <?php if ($deduction_heads) { ?>
                    <?php foreach ($deduction_heads as $k => $deduction) { ?>
                        <?php
                        $deduction_amount = 0;
                        $$deduction['deduction_for'] = !isset($$deduction['deduction_for']) ? 0 : $$deduction['deduction_for'];
                        if ($deduction['type'] == 'percentage') {
                            $deduction_amount = (($deduction['amount'] * $earningHeadTitle[$deduction['deduction_for']]) / 100);
                        } else {
                            $deduction_amount = $deduction['amount'];
                        }
                        $$deduction['deduction_for'] += $deduction_amount;
                        $ed[$deduction['deduction_for']] = $$deduction['deduction_for'];
                        $deduction_title = staffPayHeads::getFieldDetail($k, 'title');
                        $erdd = staffPayHeads::getFieldDetail($k, 'earning_deduction');
                        ?>
                        <tr>
                            <td style="text-transform: uppercase">
                                <?php echo $deduction_title; ?> (<span style="text-transform: capitalize"><?php echo $erdd; ?></span>)
                            </td>
                            <td>
                                <span class="pull-right sal_left"> - <i class="icon-inr"></i> <?= number_format($deduction_amount, 2); ?></span>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>
        </table>
        <?php $deduction_html = ob_get_clean(); ?>

        <?php ob_start(); ?>
        <table class="table table-striped table-bordered table-advance table-hover">
            <tbody>
                <?php
                $start_year = date('Y');
                $query = new loan;
                $list_loans_not_paid = $query->list_loans_not_paid($staff_id, $start_year);
                $total_amount = 0;
                ?>
                <?php if ($earning_heads) { ?>
                    <?php foreach ($earning_heads as $k => $earning) { ?>
                        <?php
                        $earning_title = staffPayHeads::getFieldDetail($k, 'title');
                        $earning_amount = array_key_exists($earning_title, $ed) ? ($earning['amount'] - $ed[$earning_title]) : $earning['amount'];
                        $total_amount += $earning_amount;
                        ?>
                        <tr>
                            <td style="text-transform: uppercase">
                                <?php echo $earning_title; ?>
                            </td>
                            <td>
                                <span class="pull-right sal_left">
                                    <i class="icon-inr"></i> <?php echo number_format($earning_amount, 2); ?>
                                </span>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                <!-- Loan Details -->
                <?php $installment_amount = 0; ?>
                <?php if (count($list_loans_not_paid) > 0) { ?>
                    <?php
                    foreach ($list_loans_not_paid as $loan) {
                        if ($loan->installments_paid < $loan->total_installments) {
                            $amount = $loan->amount / $loan->total_installments;
                            $installment_amount += $amount;
                            ?>
                            <tr>
                                <td style="text-transform: uppercase">
                                    <?php echo $loan->type ?>
                                </td>
                                <td>
                                    <span class="pull-right sal_left"> - <i class="icon-inr"></i> <?php echo number_format($amount, 2); ?></span>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                <?php
                if ($object->other_amount) {
                    if ($object->other_type == 'other_deduction') {
                        $total_amount = ($total_amount - $object->other_amount);
                    } else {
                        $total_amount = ($total_amount + $object->other_amount);
                    }
                    ?>
                    <tr>
                        <td style="text-transform: uppercase">
                            <?php echo $object->other_name ?>
                        </td>
                        <td>
                            <span class="pull-right sal_left"> <?php echo $object->other_type == 'other_deduction' ? '-' : ''; ?> <i class="icon-inr"></i> <?php echo number_format($object->other_amount, 2); ?></span>
                        </td>
                    </tr>
                <?php } ?>
                <?php $AmountTotal = ($total_amount - $installment_amount); ?>
                <tr>
                    <td class="total">Sub Total</td>
                    <td class="total" colspan="2" style="text-align:right;">
                        <i class="icon-inr"></i> <span class="make_total_new"><?php echo $AmountTotal; ?></span>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php $total_amount_html = ob_get_clean(); ?>

        <?php ob_start(); ?>
        <div class="row-fliud">
            <table class="table table-striped table-bordered table-advance table-hover">
                <thead>
                    <tr>
                        <th>Allowance</th>
                        <th>Deduction</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php echo $earning_html; ?>
                        </td>
                        <td>
                            <?php echo $deduction_html; ?>
                        </td>
                        <td>
                            <?php echo $total_amount_html; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <?php $total_salary_details = ob_get_clean(); ?>
        <?php
        $QueryDoc = new document();
        $Doc = $QueryDoc->getRecord($doc_id);

        $QueryDet = new staff();
        $staff = $QueryDet->getStaffDetail($school->id, $staff_id);

        $content = '';
        if (!empty($Doc)):
            if (empty($object->receipt_no)):
                $object->receipt_no = '0';
            endif;
            $ed = array();
            $sal_added = $total_salary_details;

            $replace = array('SCHOOL_NAME' => $school->school_name,
                'DATE' => date('d M, Y', strtotime($object->payment_date)),
                'NAME' => $staff->title . " " . $staff->first_name . " " . $staff->last_name,
                'CATEGORY' => $staff->staff_category,
                'DESIGNATION' => $staff->designation,
                'REC_NO' => $object->receipt_no,
                'SAL_ADDED' => $sal_added,
                'SUB_TOTAL' => CURRENCY_SYMBOL . ' ' . number_format($object->amount, 2),
                'TOTAL' => CURRENCY_SYMBOL . ' ' . number_format($object->amount, 2),
            );

            $content = $Doc->format;
            if (count($replace)):
                foreach ($replace as $k => $v):
                    $literal = '{' . trim(strtoupper($k)) . '}';
                    $content = html_entity_decode(str_replace($literal, $v, $content));
                    $content = str_replace('&nbsp;', '', $content);
                endforeach;
            endif;
        endif;
        break;

    case'delete':
        $QueryObj = new staffSalary();
        $QueryObj->deleteRecord($id);
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id));
        break;
    default:break;
endswitch;
?>