<?php
set_time_limit(1000000000000);

/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');

$modName='staff_attendance';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_REQUEST['id'])?$id=$_REQUEST['id']:$id='';
isset($_REQUEST['session_id'])?$session_id=$_REQUEST['session_id']:$session_id='';

isset($_GET['page'])?$page=$_GET['page']:$page='1';
isset($_REQUEST['cat'])?$cat=$_REQUEST['cat']:$cat='';

isset($_REQUEST['atten_date'])?$atten_date=$_REQUEST['atten_date']:$atten_date=date('Y-m-d');
isset($_REQUEST['type'])?$type=$_REQUEST['type']:$type='';
isset($_REQUEST['select'])?$select=$_REQUEST['select']:$select='single';


#Get Staff Category
$QuerySalCat = new staffCategory();
$QuerySalCat->listAll($school->id);	
 

switch ($action):
	case'list':
		#get Session Info
		$category=get_object('staff_category_master',$cat);	
		
		#Get staff detail
		$QueryOb=new staff();
		$record=$QueryOb->getSchoolStaffWithCategory($school->id,$cat);		

		if($type=='attendance'):
		
			$first_date_of_month= date('Y-m-d', strtotime('01 '.$atten_date));
			$last_date_of_month=date("Y-m-d",strtotime("+1 month -1 second",strtotime(date($first_date_of_month))));
			
			$total_sunday=getTotalSunday($first_date_of_month,$last_date_of_month); 
			$total_month_days=(date('d',strtotime($last_date_of_month))); 
			$working_days=$total_month_days-$total_sunday;
			
		endif;
		
    break;

	case'insert':
		#get Session Info
		$category=get_object('staff_category_master',$cat);	
		
		#Get staff detail
		$QueryOb=new staff();
		$record=$QueryOb->getSchoolStaffWithCategory($school->id,$cat);		

	  #check if Sunday or holiday
	  if($type=='attendance'):  
			#check Sunday
			$sunday=CheckSunday($atten_date);
			
			#check Holiday
			$M_obj= new schoolHolidays();
			$holiday=$M_obj->checkHoliday($school->id,$atten_date);  	
			
			if($sunday=='Sunday'):	
				$admin_user->set_error(); 
				$admin_user->set_pass_msg(MSG_HOLIDAY_DATE_SUNDAY);
				Redirect(make_admin_url('staff_attendance', 'insert', 'insert'));	 
			elseif($holiday=='Holiday'):	
				$admin_user->set_error();  				
				$admin_user->set_pass_msg(MSG_HOLIDAY_DATE);
				Redirect(make_admin_url('staff_attendance', 'insert', 'insert'));	 			
			endif;
	  endif;		
	
		/* Add attendance */
		if(isset($_POST['multi_select'])): 
			if(isset($_POST['attendance']) && count($_POST['attendance'])>0): 
				
				#Add Multiple attendance into Session
				$QuerySess = new staffAttendance();
				$QuerySess->saveAttendance($_POST['attendance']);			
				
				$admin_user->set_pass_msg(ucfirst(MSG_ATTENDANCE_ADDED));
				Redirect(make_admin_url('staff_attendance', 'insert', 'insert'));
			else:
				Redirect(make_admin_url('staff_attendance', 'insert', 'insert'));
			endif; 						
		endif;  	
		
    break;
	default:break;
endswitch;
?>