<?php
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');


$modName = 'vehicles';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['ct_veh']) ? $ct_veh = $_REQUEST['ct_veh'] : $ct_veh = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['stoppage_id']) ? $stoppage_id = $_REQUEST['stoppage_id'] : $stoppage_id = '';
isset($_REQUEST['vehicle_id']) ? $vehicle_id = $_REQUEST['vehicle_id'] : $vehicle_id = '';

#Get School Couses
$QueryCourse = new course();
$QueryCourse->listAll($school->id, '1');

#get_all session 
$QuerySession = new session();
$QuerySession->listAllCurrent($school->id, '1');

#Get School staff
$QueryStaff = new staff();
$staff1 = $QueryStaff->getSchoolStaff_detail($school->id, '1');

$QueryStaff1 = new staff();
$staff2 = $QueryStaff1->getSchoolStaff_detail($school->id, '1');

#Get School staff Category
$QueryStaff = new staffCategory();
$QueryStaff->listAll($school->id, '1');

#Get School staff Category
$QueryStaffCond = new staffCategory();
$QueryStaffCond->listAll($school->id, '1');

switch ($action):
    case'list':
        /* Get vehicle Pages */
        $QueryObj = new vehicle();
        $QueryObj->listAll($school->id);
        break;
    case'insert':
        // List All Vehicle routes
        $obj = new vehicle_route;
        $routes = $obj->listVehicleRoute($school->id);

        /* Insert vehicle */
        if (isset($_POST['submit'])):
            // Save Docs
            $files_array = array();
            for ($i = 0; $i < count($_FILES['doc']['name']); $i++) {
                $files_array[] = array(
                    'title' => $_POST['title'][$i],
                    'name' => $_FILES['doc']['name'][$i],
                    'tmp_name' => $_FILES['doc']['tmp_name'][$i],
                    'error' => $_FILES['doc']['error'][$i]
                );
            }

            $_POST['round_routes'] = implode(',', $_POST['round_route']);

            #Add Vehicle
            $QueryObj = new vehicle();
            $vehicle_id = $QueryObj->saveData($_POST);
            foreach ($files_array as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/vehicle_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['vehicle_id'] = $vehicle_id;
                        $arr['title'] = $file['title'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new vehicle_docs;
                        $obj->saveVehicleDoc($arr);
                    endif;
                endif;
            endforeach;
            $admin_user->set_pass_msg(MSG_VEHICLE_ADDED);
            Redirect(make_admin_url('vehicles', 'list', 'list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('vehicles', 'list', 'list'));
        endif;
        break;
    case'update':
        // List All Vehicle routes
        $obj = new vehicle_route;
        $routes = $obj->listVehicleRoute($school->id);

        /* Get vehicle Pages by Id */
        $QueryPage = new vehicle();
        $object = $QueryPage->getRecord($id);

        // Get Vehicle Docs
        $obj = new vehicle_docs;
        $vehicle_docs = $obj->listVehicleDocs($id);
        /* Update vehicle Pages */
        if (isset($_POST['submit'])):
            // Update Old Docs
            $titles_array = array_combine($_POST['doc_id'], $_POST['title_old']);
            foreach ($titles_array as $id => $title) {
                $array['id'] = $id;
                $array['title'] = $title;
                $obj = new vehicle_docs;
                $obj->saveVehicleDoc($array);
            }
            $files_array_old = array();
            for ($i = 0; $i < count($_FILES['doc_old']['name']); $i++) {
                $files_array_old[] = array(
                    'id' => $_POST['doc_id'][$i],
                    'name' => $_FILES['doc_old']['name'][$i],
                    'tmp_name' => $_FILES['doc_old']['tmp_name'][$i],
                    'error' => $_FILES['doc_old']['error'][$i]
                );
            }
            foreach ($files_array_old as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/vehicle_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['id'] = $file['id'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new vehicle_docs;
                        $obj->saveVehicleDoc($arr);
                    endif;
                endif;
            endforeach;
            // Save Docs
            $files_array = array();
            for ($i = 0; $i < count($_FILES['doc']['name']); $i++) {
                $files_array[] = array(
                    'title' => $_POST['title'][$i],
                    'name' => $_FILES['doc']['name'][$i],
                    'tmp_name' => $_FILES['doc']['tmp_name'][$i],
                    'error' => $_FILES['doc']['error'][$i]
                );
            }
            foreach ($files_array as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/vehicle_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['vehicle_id'] = $_POST['id'];
                        $arr['title'] = $file['title'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new vehicle_docs;
                        $obj->saveVehicleDoc($arr);
                    endif;
                endif;
            endforeach;
            $_POST['round_routes'] = implode(',', $_POST['round_route']);

            #Add Vehicle
            $QueryObj = new vehicle();
            $vehicle_id = $QueryObj->saveData($_POST);
            $admin_user->set_pass_msg(MSG_VEHICLE_UPDATE);
            Redirect(make_admin_url('vehicles', 'list', 'list'));
        elseif (isset($_POST['cancel'])):
            $admin_user->set_error();
            $admin_user->set_pass_msg(OPERATION_CANCEL);
            Redirect(make_admin_url('vehicles', 'list', 'list'));
        endif;
        break;

    case'delete_image':
        #delete session 			
        $QueryObj = new vehicle();
        $QueryObj->deleteImage($id, $_GET['type']);

        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('vehicles', 'update', 'update&id=' . $id));
        break;

    case'view':
        /* Get All Session For This Vehicle who have this */
        $QuerySess = new vehicleStudent();
        $AllSession = $QuerySess->getSessionList($id);

        #if empty session id
        if (!empty($AllSession) && empty($session_id)):
            $session_id = 0;
        endif;

        #Get All current session Section
        $QuerySec = new vehicleStudent();
        $AllSection = $QuerySec->getSessionsection($session_id);

        /* Get vehicle Pages by Id */
        $QueryPage = new vehicle();
        $object = $QueryPage->getRecord($id);

        #Get vehicle students
        if ($session_id == '0') {
            $obj = new vehicleStudent;
            $AllStu = $obj->ListAllVehicleStudents($id);
        } else {
            $QueryStu = new vehicleStudent();
            $AllStu = $QueryStu->vehicleStudentsWithDetail($id, $session_id, $ct_sec);
        }

        #Get all vehicle students
        $VehStu = new vehicleStudent();
        $veh_st = $VehStu->vehicleStudentIdArray($id);

        break;
    case'student':
        /* Get session  by Id */
        $AllStu = array();
        $QueryPage = new session();
        $object = $QueryPage->getRecord($id);

        $QueryS = new session();
        $select_session = $QueryS->getRecord($id);

        $QueryVh = new vehicle();
        $object_vh = $QueryVh->getRecord($ct_veh);

        #Get All current session Section
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($id);

        #Get session students
        $QueryStu = new studentSession();
        $AllStu = $QueryStu->sessionStudentsWithSessionOrSimple($school->id, $id, $ct_sec);

        #Get all vehicle students in array
        $SessStu = new vehicleStudent();
        $veh_st = $SessStu->vehicleStudentIdArray($ct_veh);


        /* Update session */
        if (isset($_POST['multi_select'])):
            if (isset($_POST['multiopt']) && count($_POST['multiopt']) > 0):
                $session_info = get_object('session', $_POST['session_id']);
                if (strtotime(date('Y-m-d')) > strtotime($session_info->end_date)):
                    $admin_user->set_error();
                    $admin_user->set_pass_msg(MSG_THIS_SESSSION_PASSED);
                    Redirect(make_admin_url('vehicles', 'student', 'student&ct_veh=' . $ct_veh . '&id=' . $id . '&ct_sec=' . $ct_sec));
                endif;


                #Add Multiple Student into Session
                $QuerySess = new vehicleStudent();
                $QuerySess->AddMultipleStudentIntoVehicle($_POST['multiopt'], $ct_veh, $id, $ct_sec);

                $admin_user->set_pass_msg(MSG_VEHICLE_MULTI_STUDENT_ADDED);
                Redirect(make_admin_url('vehicles', 'view', 'view&id=' . $ct_veh));
            else:
                $admin_user->set_error();
                $admin_user->set_pass_msg(MSG_SELECT_MULTI_STUDENT);
                Redirect(make_admin_url('vehicles', 'student', 'student&ct_veh=' . $ct_veh . '&id=' . $id . '&ct_sec=' . $ct_sec));
            endif;
        endif;
        break;

    case'remove_to_vehicle':
        #delete Student from session
        $QuerySess = new vehicleStudent();
        $QuerySess->deleteData($id, $_GET['st_id']);

        $admin_user->set_pass_msg(MSG_SESSION_STUDENT_DELETE);
        Redirect(make_admin_url('vehicles', 'view', 'view&id=' . $id));
        break;
    case'route':
        // List All Vehicle routes
        $obj = new vehicle_route;
        $routes = $obj->listVehicleRoute($school->id);
        if (isset($_POST['submit'])) {
            $final_array = array();
            for ($i = 0; $i < count($_POST['stoppage']); $i++) {
                $final_array[] = array(
                    'stoppage' => $_POST['stoppage'][$i],
                    'amount' => $_POST['amount'][$i],
                    'pick_time' => $_POST['pick_time'][$i],
                    'drop_time' => $_POST['drop_time'][$i]
                );
            }
            // Save Route Map
            $route_arr['school_id'] = $school->id;
            $route_arr['route_from'] = $_POST['route_from'];
            $route_arr['route_to'] = $_POST['route_to'];
            $route_obj = new vehicle_route;
            $route_id = $route_obj->saveVehicleRoute($route_arr);

            foreach ($final_array as $vehicle_stoppage) {
                $stoppage_arr['school_id'] = $school->id;
                $stoppage_arr['route_id'] = $route_id;
                $stoppage_arr['stoppage'] = $vehicle_stoppage['stoppage'];
                $stoppage_arr['amount'] = $vehicle_stoppage['amount'];
                $stoppage_arr['pick_time'] = $vehicle_stoppage['pick_time'];
                $stoppage_arr['drop_time'] = $vehicle_stoppage['drop_time'];
                $stoppage_obj = new vehicle_stoppages;
                $stoppage_obj->saveStoppages($stoppage_arr);
            }
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('vehicles', 'route', 'route'));
        }
        break;

    case'update_route':
        // Edit Route Details
        $obj = new vehicle_route;
        $route = $obj->getRecord($id);

        // List Stoppages
        $obj = new vehicle_stoppages;
        $stoppages = $obj->listStoppages($id);

        if (isset($_POST['submit'])) {
            $final_array_update = array();
            for ($i = 0; $i < count($_POST['stoppage_old']); $i++) {
                $final_array_update[] = array(
                    'id' => $_POST['stoppage_id'][$i],
                    'stoppage' => $_POST['stoppage_old'][$i],
                    'amount' => $_POST['amount_old'][$i],
                    'pick_time' => $_POST['pick_time_old'][$i],
                    'drop_time' => $_POST['drop_time_old'][$i]
                );
            }
            //pr($final_array); exit;
            $route_obj = new vehicle_route;
            $route_obj->saveVehicleRoute($_POST);
            foreach ($final_array_update as $vehicle_stoppage_update) {
                $stoppage_arr_update['id'] = $vehicle_stoppage_update['id'];
                $stoppage_arr_update['stoppage'] = $vehicle_stoppage_update['stoppage'];
                $stoppage_arr_update['amount'] = $vehicle_stoppage_update['amount'];
                $stoppage_arr_update['pick_time'] = $vehicle_stoppage_update['pick_time'];
                $stoppage_arr_update['drop_time'] = $vehicle_stoppage_update['drop_time'];
                $stoppage_obj = new vehicle_stoppages;
                $stoppage_obj->saveStoppages($stoppage_arr_update);
            }

            $final_array = array();
            for ($i = 0; $i < count($_POST['stoppage']); $i++) {
                $final_array[] = array(
                    'stoppage' => $_POST['stoppage'][$i],
                    'amount' => $_POST['amount'][$i],
                    'pick_time' => $_POST['pick_time'][$i],
                    'drop_time' => $_POST['drop_time'][$i]
                );
            }
            foreach ($final_array as $vehicle_stoppage) {
                $stoppage_arr['school_id'] = $school->id;
                $stoppage_arr['route_id'] = $id;
                $stoppage_arr['stoppage'] = $vehicle_stoppage['stoppage'];
                $stoppage_arr['amount'] = $vehicle_stoppage['amount'];
                $stoppage_arr['pick_time'] = $vehicle_stoppage['pick_time'];
                $stoppage_arr['drop_time'] = $vehicle_stoppage['drop_time'];
                $stoppage_obj = new vehicle_stoppages;
                $stoppage_obj->saveStoppages($stoppage_arr);
            }
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('vehicles', 'route', 'route'));
        }
        break;

    case'delete_route':
        // Delete Route
        $obj = new vehicle_route;
        $obj->id = $id;
        $obj->Delete();
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('vehicles', 'route', 'route'));
        break;

    case'stoppages':
        // List Stoppages
        $obj = new vehicle_stoppages;
        $stoppages = $obj->listStoppages($id);
        break;
    case'update_stoppage':
        // Update Stoppage
        $obj = new vehicle_stoppages;
        $stoppage = $obj->getRecord($stoppage_id);
        if (isset($_POST['submit'])) {
            $obj = new vehicle_stoppages;
            $obj->saveStoppages($_POST);
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('vehicles', 'stoppages', 'stoppages', 'id=' . $id));
        }
        break;
    case'delete_stoppage':
        // Delete Route
        $obj = new vehicle_stoppages;
        $obj->id = $stoppage_id;
        $obj->Delete();
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('vehicles', 'stoppages', 'stoppages', 'id=' . $id));
        break;

    case'expenses':
        $obj = new vehicle;
        $vehicle = $obj->getRecord($vehicle_id);
        // List Stoppages
        $obj = new vehicle_expenses;
        $expenses = $obj->listVehicleExpenses($vehicle_id);
        break;
    case'create_expense':
        /* Get vehicle Pages */
        $obj = new vehicle;
        $vehicles = $obj->listAllVehicles($school->id);
        if (isset($_POST['submit'])) {
            $files_array = array();
            for ($i = 0; $i < count($_FILES['doc']['name']); $i++) {
                $files_array[] = array(
                    'title' => $_POST['title'][$i],
                    'name' => $_FILES['doc']['name'][$i],
                    'tmp_name' => $_FILES['doc']['tmp_name'][$i],
                    'error' => $_FILES['doc']['error'][$i]
                );
            }
            $obj = new vehicle_expenses;
            $expense_id = $obj->saveVehicleExpense($_POST);
            foreach ($files_array as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/vehicle_expense_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['vehicle_id'] = $_POST['vehicle_id'];
                        $arr['expense_id'] = $expense_id;
                        $arr['title'] = $file['title'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new vehicle_expense_docs;
                        $obj->saveVehicleExpenseDoc($arr);
                    endif;
                endif;
            endforeach;
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('vehicles', 'expenses', 'expenses', 'vehicle_id=' . $_POST['vehicle_id']));
        }
        break;
    case'update_expense':
        // Get Expense Details
        $obj = new vehicle_expenses;
        $expense = $obj->getRecord($id);

        /* Get vehicle Pages */
        $obj = new vehicle;
        $vehicles = $obj->listAllVehicles($school->id);

        // List Expense Docs
        $obj = new vehicle_expense_docs;
        $expense_docs = $obj->listVehicleExpenseDocs($id);

        if (isset($_POST['submit'])) {
            // Update Old Docs
            $titles_array = array_combine($_POST['doc_id'], $_POST['title_old']);
            foreach ($titles_array as $id => $title) {
                $array['id'] = $id;
                $array['title'] = $title;
                $obj = new vehicle_expense_docs;
                $obj->saveVehicleExpenseDoc($array);
            }
            $files_array_old = array();
            for ($i = 0; $i < count($_FILES['doc_old']['name']); $i++) {
                $files_array_old[] = array(
                    'id' => $_POST['doc_id'][$i],
                    'name' => $_FILES['doc_old']['name'][$i],
                    'tmp_name' => $_FILES['doc_old']['tmp_name'][$i],
                    'error' => $_FILES['doc_old']['error'][$i]
                );
            }
            foreach ($files_array_old as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/vehicle_expense_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['id'] = $file['id'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new vehicle_docs;
                        $obj->saveVehicleExpenseDoc($arr);
                    endif;
                endif;
            endforeach;
            // Save Docs
            $files_array = array();
            for ($i = 0; $i < count($_FILES['doc']['name']); $i++) {
                $files_array[] = array(
                    'title' => $_POST['title'][$i],
                    'name' => $_FILES['doc']['name'][$i],
                    'tmp_name' => $_FILES['doc']['tmp_name'][$i],
                    'error' => $_FILES['doc']['error'][$i]
                );
            }
            foreach ($files_array as $file):
                if ($file['error'] == '0'):
                    $target_dir = DIR_FS_SITE_UPLOAD . 'file/vehicle_expense_docs/';
                    $target_file = $target_dir . basename(time() . $file['name']);
                    if (move_uploaded_file($file['tmp_name'], $target_file)):
                        $arr['expense_id'] = $_POST['id'];
                        $arr['vehicle_id'] = $_POST['vehicle_id'];
                        $arr['title'] = $file['title'];
                        $arr['doc'] = time() . $file['name'];
                        $obj = new vehicle_expense_docs;
                        $obj->saveVehicleExpenseDoc($arr);
                    endif;
                endif;
            endforeach;
            $obj = new vehicle_expenses;
            $obj->saveVehicleExpense($_POST);
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('vehicles', 'expenses', 'expenses', 'vehicle_id=' . $_POST['vehicle_id']));
        }
        break;
    case'delete_expense':
        // Delete Route
        $obj = new vehicle_expenses;
        $obj->id = $id;
        $obj->Delete();
        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('vehicles', 'expenses', 'expenses', 'vehicle_id=' . $vehicle_id));
        break;
    case'delete':
        #delete vehicle 			
        $QueryObj = new vehicle();
        $QueryObj->deleteRecord($id);

        #delete all vehicle student records 
        $QuerySHeads = new vehicleStudent();
        $QuerySHeads->DeleteStudentFromVehicle($id);

        $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        Redirect(make_admin_url('vehicles', 'list', 'list'));
        break;
    default:break;
endswitch;
?>
