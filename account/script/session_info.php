<?php
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/vehicleClass.php');


$modName='session_info';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['interval_id'])?$interval_id=$_GET['interval_id']:$interval_id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';
isset($_GET['type'])?$type=$_GET['type']:$type='info';
isset($_REQUEST['session_id'])?$session_id=$_REQUEST['session_id']:$session_id='';
isset($_REQUEST['ct_sect'])?$ct_sect=$_REQUEST['ct_sect']:$ct_sect='A';
isset($_GET['month'])?$month=$_GET['month']:$month=date('m');



	#get session Info 
	$QueryInfo = new session();
	$session=$QueryInfo->getRecord($id);	

	#get session Info 
	$QuerySection = new studentSession();
	$all_section=$QuerySection->listAllSessionSection($id,'1','array');
	
	$month_array=array('1'=>'Jan', '2'=>'Feb', '3'=>'Mar', '4'=>'Apr', '5'=>'May', '6'=>'Jun', '7'=>'Jul', '8'=>'Aug', '9'=>'Sep', '10'=>'Oct', '11'=>'Nov', '12'=>'Dec');
	
#handle actions here.
switch ($action):
	case'list':
		if($type=='info'):
			#get staff Info 
			$QueryStaff = new staff();
			$incharge=$QueryStaff->getStaff($session->incharge);	
			
			#get compulsory_subjects Info 
			$QueryCsubjects = new subject();
			$compulsory_subjects=$QueryCsubjects->listSessionSelectedSubjects($session->compulsory_subjects);

			#get elective_subjects Info 
			$QueryEsubjects = new subject();
			$elective_subjects=$QueryEsubjects->listSessionSelectedSubjects($session->elective_subjects);
			
			#get Session Pay Heads Info 
			$QueryPayHeads = new session_fee_type();
			$pay_heads=$QueryPayHeads->sessionPayHeads($session->id,'by_objects');
			
			#get Session Section 
			$QuerySessSect = new session_section_staff();
			$AllSect=$QuerySessSect->ListOfSection($session->id);			
		
		elseif($type=='student'):	
			#Get Section Students
			$QueryOb=new studentSession();
			$AllStu=$QueryOb->sectionSessionStudents($id,$ct_sect);	
			
		elseif($type=='attendance'):	
				#Get Student Attendance
				$month=date('m',strtotime($session->start_date));
				$year=date('Y',strtotime($session->start_date));
				
				$first_date_of_month=$year."-".$month."-01";
				$last_date_of_month=date("Y-m-d",strtotime("+1 month -1 second",strtotime(date($year."-".$month."-1"))));
				
				$total_sunday=getTotalSunday($first_date_of_month,$last_date_of_month); 
				$total_month_days=(date('d',strtotime($last_date_of_month))); 
				$working_days=$total_month_days-$total_sunday;				

				#Get Section Students for attancance
				$QueryOb=new studentSession();
				$records=$QueryOb->sectionSessionStudents($id,$ct_sect);
				
		elseif($type=='fee'):					
				$current_date=date('Y-m-d');
				$current='0';
				
				#Get Section Students for fee
				$QueryOb=new studentSession();
				$records=$QueryOb->sectionSessionStudents($id,$ct_sect);	
				
				#get Session Interval
				$QueryInt=new session_interval();
				$interval=$QueryInt->listOfInterval($id);				
				
				if(!empty($interval) && empty($interval_id)):
					$interval_id=$interval['0']->id;
				endif;
				
				#Get interval detail
				$interval_detail=get_object('session_interval',$interval_id);					
				
		elseif($type=='exam'):					
			#Get Section Students exam list
			$QueryOb=new examination();
			$records=$QueryOb->listAllSessionSectionExam($id,$ct_sect);	
			
		elseif($type=='vehicle'):					
			#Get Section Students vehicle list
			$QueryOb=new vehicleStudent();
			$records=$QueryOb->sessionSectionVehicles($id,$ct_sect);	
			
		endif;
	break;
	default:break;
endswitch;
?>
