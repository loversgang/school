<?php

include_once(DIR_FS_SITE . 'include/functionClass/leave_typesClass.php');



$modName = 'leave_types';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '';



switch ($action):
    case'list':
        $query = new leave_types;
        $lists = $query->lists();
        break;

    case'insert':
        if (isset($_POST['submit'])) {
            $query = new leave_types;
            $query->saveData($_POST);
            $admin_user->set_pass_msg('Type inserted successfully');
            Redirect(make_admin_url('leave_types', 'list', 'list'));
        }
        break;

    case'update':

        $query = new leave_types();
        $list = $query->lists($id);

        if (isset($_POST['submit'])) {
            $query = new leave_types;
            $query->saveData($_POST);
            $admin_user->set_pass_msg('Type inserted successfully');
            Redirect(make_admin_url('leave_types', 'list', 'list'));
        }

        break;

    case'delete':
        $query = new leave_types;
        $query->deleteType($id);
        $admin_user->set_pass_msg('Type Deleted successfully');
        Redirect(make_admin_url('leave_types', 'list', 'list'));
        break;

    default:break;
endswitch;
?>