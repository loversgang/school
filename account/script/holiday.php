<?php
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolClass.php');

$modName='holiday';

isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';

#handle actions here.
switch ($action):
	case'list':
		$QueryObj = new schoolHolidays();
		$QueryObj->listAll($school->id);
		break;
	case'insert':
		if(isset($_POST['submit'])):
		
                    $QueryObj = new schoolHolidays();
                    $QueryObj->saveData($_POST);
                    $admin_user->set_pass_msg(MSG_HOLIDAY_ADDED);
                    Redirect(make_admin_url('holiday', 'list', 'list'));
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg(OPERATION_CANCEL);
                    Redirect(make_admin_url('holiday', 'list', 'list'));   
		endif;
		break;
	case'update':
		$QueryObj = new schoolHolidays();
		$object=$QueryObj->getRecord($id);                
		if(isset($_POST['submit'])): 
                    $QueryObj = new schoolHolidays();
                    $QueryObj->saveData($_POST);
                    $admin_user->set_pass_msg(MSG_HOLIDAY_UPDATE);
                    Redirect(make_admin_url('holiday', 'list', 'list'));
                elseif(isset($_POST['cancel'])):
                    $admin_user->set_error();  
                    $admin_user->set_pass_msg(OPERATION_CANCEL);
                    Redirect(make_admin_url('holiday', 'list', 'list'));      
		endif;
		break;

	 case'delete':
		$QueryObj = new schoolHolidays();
		$QueryObj->deleteRecord($id);
		$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
		Redirect(make_admin_url('holiday', 'list', 'list'));  
		break;
	
	default:break;
endswitch;
?>
