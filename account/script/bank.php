<?php

include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');

$modName = 'bank';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';

switch ($action):
    case'list':
        $obj = new bank;
        $banks = $obj->banks();
        break;
    case'insert':
        if (isset($_POST['submit'])) {
            $obj = new bank;
            $obj->saveBank($_POST);
            $admin_user->set_pass_msg('Bank Added Successfully!');
            Redirect(make_admin_url('bank'));
        }

        break;
    case'update':
        $obj = new bank;
        $bank = $obj->banks($id);

        if (isset($_POST['submit'])) {
            $obj = new bank;
            $obj->saveBank($_POST);
            $admin_user->set_pass_msg('Bank Updated Successfully!');
            Redirect(make_admin_url('bank'));
        }

        break;
    case'delete':
        $obj = new bank;
        $obj->delete_bank($id);
        $admin_user->set_pass_msg('Bank Deleted Successfully!');
        Redirect(make_admin_url('bank'));
        break;
    default:
        break;
endswitch;
