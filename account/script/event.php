<?php

include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEventClass.php');
$modName = 'event';

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';

$QueryObjQ = new session();
$AllSessYear = $QueryObjQ->listOfYearSess($school->id);
if (empty($sess_int)):
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
endif;
#handle actions here.
switch ($action):
    case'list':
        $QueryObj = new event;
        $events = $QueryObj->listAllEvents();
        break;
    case'insert':
        $obj = new eventCat;
        $cats = $obj->listAllCats();

        $QueryObj = new session();
        $QueryObj->listOfYearSession($school->id, $sess_int);

        if (isset($_POST['submit'])) {
            $obj = new event;
            $obj->saveEvent($_POST);
            $admin_user->set_pass_msg('Event Added Successfully!');
            Redirect(make_admin_url('event', 'list', 'list'));
        }
        break;
    case'update':
        $obj = new event;
        $event = $obj->getRecord($id);

        $obj = new eventCat;
        $cats = $obj->listAllCats();

        $QueryObj = new session();
        $QueryObj->listOfYearSession($school->id, $sess_int);

        if (isset($_POST['submit'])) {
            $obj = new event;
            $obj->saveEvent($_POST);
            $admin_user->set_pass_msg('Event Updated Successfully!');
            Redirect(make_admin_url('event', 'list', 'list'));
        }
        break;
    case'delete':
        $obj = new event;
        $obj->id = $id;
        $obj->Delete();
        $admin_user->set_pass_msg("Event Deleted Successfully!");
        Redirect(make_admin_url('event', 'list', 'list'));
        break;
    default:
        break;
endswitch;
