<!-- section coding -->
<?php
/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'view':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/view.php');
        break;
    case 'insert':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create.php');
        break;
    case 'update':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/edit.php');
        break;
    case 'stuck_off':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/stuck_off.php');
        break;
    case 'thrash':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/thrash.php');
        break;
    default:break;
endswitch;
?>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<script src="assets/scripts/form-wizard.js"></script>     
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
        $(".check_valid").live("click", function () {
            // Validation engine
            $(".validation").validationEngine({
                promptPosition: "topLeft"

            });
        });
    });
</script>