<!-- section coding -->
<?php
/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'feedback':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/feedback.php');
        break;
    case 'insert':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create.php');
        break;
    case 'update':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/edit.php');
        break;
    case 'referer':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/referer.php');
        break;
    case 'referer_list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/referer_list.php');
        break;
    case 'message':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/message.php');
        break;
    case 'resolve':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/resolve.php');
        break;
    case 'resolved':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/resolved.php');
        break;
    default:break;
endswitch;
?>
	