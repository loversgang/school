<?php
#handle sections here.
switch ($section):
    case 'list':
        ?>
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">

                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Dashboard <small>statistics and more</small>
                    </h3>
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>Dashboard</li>

                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->

            <?php
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>


            <div id="dashboard">
                <!-- BEGIN DASHBOARD STATS -->


                <div class="row-fluid">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Reports</div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row-fluid" id='custom_dashboard'>
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('report', 'enquiry', 'enquiry') ?>"/>
                                        <img src="assets/img/icon/EXM.png"/>
                                        </a>	
                                        <a href="<?= make_admin_url('report', 'enquiry', 'enquiry') ?>" class="title">Enquiry Reports</a>
                                    </div>
                                </div>							
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('report', 'student', 'student') ?>"/>
                                        <img src="assets/img/icon/EXM2.png"/>
                                        </a>	
                                        <a href="<?= make_admin_url('report', 'student', 'student') ?>" class="title">Student Reports</a>
                                    </div>
                                </div>
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('report', 'attendance', 'attendance') ?>"/>	
                                        <img src="assets/img/icon/attendence.png"/>
                                        </a>
                                        <a href="<?= make_admin_url('report', 'attendance', 'attendance') ?>" class="title">Attendance Reports</a>
                                    </div>
                                </div>
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('report', 'exam', 'exam') ?>"/>
                                        <img src="assets/img/icon/exams-icon.png"/>
                                        </a>
                                        <a href="<?= make_admin_url('report', 'exam', 'exam') ?>" class="title">Examination Reports</a>
                                    </div>
                                </div>
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('report', 'fee', 'fee') ?>"/>
                                        <img src="assets/img/icon/Finance.png"/>
                                        </a>
                                        <a href="<?= make_admin_url('report', 'fee', 'fee') ?>" class="title">Fee Reports</a>
                                    </div>
                                </div>
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('report', 'salary', 'salary') ?>"/>
                                        <img src="assets/img/icon/human_resource.png"/>
                                        </a>
                                        <a href="<?= make_admin_url('report', 'salary', 'salary') ?>" class="title">Salary Reports</a>
                                    </div>
                                </div>						

                            </div>

                            <div class="row-fluid" id='custom_dashboard'>
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('report', 'vehicle', 'vehicle') ?>"/>
                                        <img src="assets/img/icon/transport.png"/>
                                        </a>
                                        <a href="<?= make_admin_url('report', 'vehicle', 'vehicle') ?>" class="title">Vehicle Reports</a>
                                    </div>
                                </div>	
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('account', 'list', 'list') ?>"/>
                                        <img src="assets/img/icon/EXM6.png"/>
                                        </a>
                                        <a href="<?= make_admin_url('account', 'list', 'list') ?>" class="title">Expense Reports</a>
                                    </div>
                                </div>
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('account', 'update', 'update') ?>"/>
                                        <img src="assets/img/icon/manage_users.png"/>
                                        </a>
                                        <a href="<?= make_admin_url('account', 'update', 'update') ?>" class="title">Income Reports</a>
                                    </div>
                                </div>
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('account', 'report', 'report') ?>"/>
                                        <img src="assets/img/icon/EXM4.png"/>
                                        </a>
                                        <a href="<?= make_admin_url('account', 'report', 'report') ?>" class="title">Full Reports</a>
                                    </div>
                                </div>		
                                <div class='span2'>
                                    <div class="easy-pie-chart">
                                        <a href="<?= make_admin_url('report', 'daily', 'daily') ?>"/>
                                        <img src="assets/img/icon/expense-2.png"/>
                                        </a>
                                        <a href="<?= make_admin_url('report', 'daily', 'daily') ?>" class="title">Daily Collection Report</a>
                                    </div>
                                </div>								
                            </div>


                        </div>
                    </div>
                </div>					
                <!-- END DASHBOARD STATS -->
                <div class="clearfix"></div>
                <br/>
                <div class="clearfix"></div>

                <?php /*
                  if((GOOGLE_AC!='') && (ANALYTIC_USER!='') && (ANALYTIC_PASS!='') && (ANALYTIC_TABLE!='')):
                  //include_once('google_analytics.php');

                  endif;
                 */ ?>
            </div>

            <!-- END PAGE CONTAINER-->    
            <script type="text/javascript" src="https://www.google.com/jsapi"></script>
            <!-- Session Student Chart -->
            <?
            if (!empty($session_st)): $male = '';
                $female = '';
                ?>
                <div class="row-fluid">
                    <div class="portlet box red" style='overflow:hidden;'>
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-user"></i>Current Session Students</div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div style='text-align:right;padding-right:25px;'><strong>Female : <?= $totalFemaleStudents ?>, Male : <?= $totalMaleStudents ?> , Total Students : <?= $totalStudents ?></strong></div>		
                            <div id="pie_chart_7" class="chart"></div>
                        </div>
                    </div>
                </div>		
                <script type="text/javascript">
                    google.load("visualization", "1", {packages: ["corechart"]});
                    google.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Genre', 'Male', 'Female', {role: 'annotation'}],
            <?php
            foreach ($session_st as $k => $v):
                $course = get_object('school_course', $v->course_id);
                if (empty($v->male)): $male = '0';
                else: $male = $v->male;
                endif;
                if (empty($v->female)): $female = '0';
                else: $female = $v->female;
                endif;
                echo "['" . $course->course_name . "', " . $male . "," . $female . ",''" . "],";
            endforeach;
            ?>
                        ]);
                        var options = {
                            title: '',
                            hAxis: {title: 'Classes', titleTextStyle: {color: '#E02222'}},
                            legend: {position: 'right'},
                            isStacked: true,
                        };

                        var chart = new google.visualization.ColumnChart(document.getElementById('pie_chart_7'));
                        chart.draw(data, options);
                    }
                </script>
            <? endif; ?>

            <!-- Fee Chart -->
            <?
            if (!empty($session_fee)): $total_paid = '';
                $total_fee = '';
                ?>
                <br/>
                <div class="row-fluid">
                    <div class="portlet box purple" style='overflow:hidden;'>
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-rupee"></i>Current Month Fee</div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <?
                            foreach ($session_fee as $pk => $pv):
                                $total_paid = $total_paid + $pv['total_paid'];
                                $total_fee = $total_fee + $pv['total_fee'];
                            endforeach;
                            ?>	
                            <div style='text-align:right;padding-right:25px;'><strong>Total Fee : <?= number_format($total_fee, 2) ?>, Total Paid : <?= number_format($total_paid, 2) ?> , Total Balance : <?= number_format($total_fee - $total_paid, 2) ?></strong></div>		
                            <div id="pie_chart_6" class="chart" ></div>

                        </div>
                    </div>
                </div>	

                <script type="text/javascript">
                    google.load("visualization", "1", {packages: ["corechart"]});
                    google.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Genre', 'Total Fee', 'Total Paid', 'Balance', {role: 'annotation'}],
            <?php
            foreach ($session_fee as $kk => $vv):
                $course = get_object('school_course', $vv['course_id']);
                if (empty($vv['total_fee'])): $total_fee = '0';
                else: $total_fee = $vv['total_fee'];
                endif;
                if (empty($vv['total_paid'])): $total_paid = '0';
                else: $total_paid = $vv['total_paid'];
                endif;
                echo "['" . $course->course_name . "', " . $total_fee . "," . $total_paid . "," . ($total_fee - $total_paid) . ",''" . "],";
            endforeach;
            ?>
                        ]);
                        var options = {
                            title: '',
                            hAxis: {title: 'Classes', titleTextStyle: {color: '#E02222'}},
                            legend: {position: 'right'},
                            isStacked: false,
                            colors: ['blue', 'green', 'red'],
                        };

                        var chart = new google.visualization.ColumnChart(document.getElementById('pie_chart_6'));
                        chart.draw(data, options);
                    }
                </script>


            <? endif; ?>
            <? if (!empty($dailyAttendance)): ?>		
                <div class="row-fluid">
                    <div class="portlet box yellow" style='overflow:hidden;'>
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-calendar"></i>Today Attendance Chart</div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <?
                            $Present = '';
                            $Absent = '';
                            $Leave = '';
                            foreach ($dailyAttendance as $Ak => $Av):
                                $Present = $Present + $Av['present'];
                                $Absent = $Absent + $Av['absent'];
                                $Leave = $Leave + $Av['leave'];
                            endforeach;
                            ?>	
                            <div style='text-align:right;padding-right:25px;'><strong>Present : <?= $Present ?>, Absent : <?= $Absent ?> , Leave : <?= $Leave ?></strong></div>		
                            <div id="pie_chart_10" class="chart"></div>								
                        </div>
                    </div>
                </div>		
                <script type="text/javascript">
                    google.load("visualization", "1", {packages: ["corechart"]});
                    google.setOnLoadCallback(drawChart);
                    function drawChart() {
                        var data = google.visualization.arrayToDataTable([
                            ['Genre', 'Present', 'Absent', 'Leave', {role: 'annotation'}],
            <?php
            foreach ($dailyAttendance as $kk => $vv):
                $course = get_object('school_course', $vv['course_id']);
                echo "['" . $course->course_name . "', " . $vv['present'] . "," . $vv['absent'] . "," . $vv['leave'] . ",''" . "],";
            endforeach;
            ?>
                        ]);
                        var options = {
                            title: '',
                            hAxis: {title: 'Classes', titleTextStyle: {color: '#E02222'}},
                            legend: {position: 'right'},
                            isStacked: true,
                            colors: ['green', '#FFA500', 'red'],
                        };

                        var chart = new google.visualization.ColumnChart(document.getElementById('pie_chart_10'));
                        chart.draw(data, options);
                    }
                </script>
            <? endif; ?>		
        </div>      
        <?
        break;
    case 'insert':
        break;
    case 'update':
        break;
    case 'delete':
        break;
    default:break;
endswitch;
?>