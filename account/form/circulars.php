<!-- section coding -->
<?php
/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'view':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/view.php');
        break;
    case 'student':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student.php');
        break;
    case 'insert':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create.php');
        break;
    case 'update':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/edit.php');
        break;
    case 'route':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/route.php');
        break;
    case 'create_route':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create_route.php');
        break;
    case 'update_route':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/update_route.php');
        break;
    case 'stoppages':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/stoppages.php');
        break;
    case 'update_stoppage':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/update_stoppage.php');
        break;
    case 'expenses':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/expenses.php');
        break;
    case 'create_expense':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create_expense.php');
        break;
    case 'update_expense':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/update_expense.php');
        break;
    case 'thrash':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/thrash.php');
        break;
    default:break;
endswitch;
?>
	