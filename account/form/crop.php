<script type="text/javascript">
    <!--//--><![CDATA[//><!--
      $(document).ready(function () {
        // Apply jrac on some image.
        $('.pane img').jrac({
            'crop_width': <?php echo $crop_width; ?>, //thumb width 
            'crop_height': <?php echo $crop_height; ?>, // thumb height
            'crop_x': 100, // keep it
            'crop_y': 100, // keep it
            'crop_resize': false,
            'viewport_onload': function () {
                var $viewport = this;
                var inputs = $viewport.$container.parent('.pane').find('.coords input:text');
                var events = ['jrac_crop_x', 'jrac_crop_y', 'jrac_crop_width', 'jrac_crop_height', 'jrac_image_width', 'jrac_image_height'];
                for (var i = 0; i < events.length; i++) {
                    var event_name = events[i];
                    // Register an event with an element.
                    $viewport.observator.register(event_name, inputs.eq(i));
                    // Attach a handler to that event for the element.
                    inputs.eq(i).bind(event_name, function (event, $viewport, value) {
                        $(this).val(value);
                    })
                            // Attach a handler for the built-in jQuery change event, handler
                            // which read user input and apply it to relevent viewport object.
                            .change(event_name, function (event) {
                                var event_name = event.data;
                                $viewport.$image.scale_proportion_locked = $viewport.$container.parent('.pane').find('.coords input:checkbox').is(':checked');
                                $viewport.observator.set_property(event_name, $(this).val());
                            });
                }
                $viewport.$container.append('<div style="margin:auto; text-align:center;">Image natual size: '
                        + $viewport.$image.originalWidth + ' x '
                        + $viewport.$image.originalHeight + '</div>')
            }
        })
                // React on all viewport events.
                .bind('jrac_events', function (event, $viewport) {
                    var inputs = $(this).parents('.pane').find('.coords input');
                    inputs.css('background-color', ($viewport.observator.crop_consistent()) ? 'chartreuse' : 'salmon');
                });
    });
    //--><!]]>
</script>	
<style>
    .box input{background:none;}  
</style>    


<?
switch ($section):
    case 'list':
        ?>

        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">

                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Crop Photo
                    </h3>



                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-resize-full"></i>Crop Photo</div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div id="image_crop">
                                <div class="pane clearfix">
                                    <!-- Image Load  -->
                                    <?php $image_obj = new imageManipulation(); ?>  

                                    <img src="<?php $image_obj->get_image($type, 'large', $image); ?>"/>
                                    <div style="clear:both;height:20px;"></div><br/>
                                    <div style="width:800px;text-align:center; margin:auto;">
                                        <form action="<?= make_admin_url('crop', 'crop', 'crop', 'type=' . $type) ?>" method="post" id="form-data">                                     
                                            <input type="hidden" value="crop" name="action" />
                                            <input type="hidden" value="list" name="section" />    
                                            <input type="hidden" name="size" value="<?php echo $size; ?>"  />
                                            <input type="hidden" name="type" value="<?php echo $type; ?>"  />
                                            <input type="hidden" name="image" value="<?php echo $image; ?>"  />
                                            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"  />
                                            <input type="hidden" name="query_concate" value="<?php echo $query_concate; ?>"  />
                                            <input type="hidden" name="query_concate1" value="<?php echo $query_concate1; ?>"  />
                                            <input type="hidden" name="redirect_section" value="<?php echo $redirect_section; ?>"  />
                                            <input type="hidden" name="redirect_action" value="<?php echo $redirect_action; ?>"  />


                                            <table class="coords" align="center" width="600">
                                                <tr>
                                                    <td width="120px">crop x</td>
                                                    <td><input style="width:70px;" type="text" name="x"/></td>
                                                    <td>crop y</td>
                                                    <td><input style="width:70px;" type="text" name="y" /></td>
                                                </tr>
                                                <tr>
                                                    <td>crop width</td>
                                                    <td><input style="width:70px;" type="text" name="w" readonly/></td>
                                                    <td>crop height</td>
                                                    <td><input style="width:70px;" type="text" name="h" readonly/></td>
                                                </tr>
                                                <tr>
                                                    <td>image width</td>
                                                    <td><input style="width:70px;" type="text" name="iw"/></td>
                                                    <td>image height</td>
                                                    <td><input style="width:70px;" type="text" name="ih"/></td>
                                                </tr>
                                                <!--<tr>
                                                        <td>lock proportion</td>
                                                        <td><input type="checkbox" checked="checked" readonly /></td>
                                                </tr>-->

                                            </table>
                                            <br/>


                                            <input class="btn green big btn-block" type="submit"  name="crop" value="CLICK TO CROP"/>

                                        </form>	

                                    </div>	
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>

            <div class="clearfix"></div>


        </div>
        <!-- END PAGE CONTAINER--> 


        <?
        break;

    default: break;
endswitch;
?>