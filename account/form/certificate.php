<!-- section coding -->
<?php
/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'insert':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create.php');
        break;
    case 'update':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/edit.php');
        break;
    case 'create_indicators':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create_indicators.php');
        break;
    case 'update_indicators':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/update_indicators.php');
        break;
    case 'list_indicators':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list_indicators.php');
        break;
    case 'delete_indicators':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/delete_indicators.php');
        break;
    case 'thrash':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/thrash.php');
        break;
    default:break;
endswitch;
?>
	