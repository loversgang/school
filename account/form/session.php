<!-- section coding -->
<?php
/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'view':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/view.php');
        break;
    case 'print':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/print.php');
        break;
    case 'previous':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/previous.php');
        break;
    case 'insert':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create.php');
        break;
    case 'update':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/edit.php');
        break;
    case 'section':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/section.php');
        break;
    case 'stuck_off':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/stuck_off.php');
        break;
    case 'other':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/other.php');
        break;
    case 'session_sec':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/session_sec.php');
        break;
    case 'session_fee':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/session_fee.php');
        break;
    case 'thrash':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/thrash.php');
        break;
    default:break;
endswitch;
?>
	