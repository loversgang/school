<!-- section coding -->
<?php
/* sections */
switch ($section):
    case 'list':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/list.php');
	    break; 
    case 'insert':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/create.php');
            break;  
    case 'update':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/edit.php');
            break; 	
    case 'cat_list':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/cat_list.php');
	    break; 
    case 'cat_insert':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/cat_create.php');
            break;  
    case 'cat_update':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/cat_edit.php');
            break; 			
     case 'thrash':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/thrash.php');
           break;            
    default:break;
endswitch;
?>

	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {    
            $(".check_valid").live("click",function(){
             // Validation engine
                    $(".validation").validationEngine({
                         promptPosition : "topLeft"
                        
                    });
          	});  
		});
	</script>