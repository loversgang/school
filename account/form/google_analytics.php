<?php

/* 
 *************** Google Analytics Php Class ***********************

*/

	include_once(DIR_FS_SITE.ADMIN_FOLDER.'/include/googleAnalytics/class.analytics.php');

	define('ga_email',ANALYTIC_USER);
	define('ga_password',ANALYTIC_PASS);
	define('ga_profile_id',ANALYTIC_TABLE);

	// Start date and end date is optional
	// if not given it will get data for the current month
          
	 if($g_sort=='1'):
		$start_date = date('Y-m-01');
		$end_date = date('Y-m-t');	
	 else: 		
		$end_date = date('Y-m-t');		
		$ct_date=date('Y-m-d');
		$start_date= date('Y-m-01',strtotime("-1 year", strtotime($ct_date)));
		$start_date = date('Y-m-01',strtotime("+1 month", strtotime($start_date)));
		
	 endif;
	   

	$init = new fetchAnalytics(ga_email,ga_password,ga_profile_id,$start_date,$end_date);

	$trafficCount = $init->trafficCount();
	$referralTraffic = $init->referralCount();
	$trafficCountNum = $init->sourceCountNum();
	$trafficCountPer = $init->sourceCountPer();
	$perDayCount = $init->perDayCount();
	$perMonthCount = $init->perMonthCount();
	
	

	$new_month_data=array();
	foreach($perDayCount as $key1 => $value1){
			$str2 = substr($key1, 4);
			
			if(empty($value1)){$value1='0';} 			
			$m_name=trim(substr($key1, 0, 4));
			if(array_key_exists($m_name, $new_month_data)):
				$new_month_data[$m_name]=$new_month_data[$m_name]+$value1;
			else:
				$new_month_data[$m_name]=$value1;
			endif;
		} 


?>
<style>
#right_action {
    background: none repeat scroll 0 0 #F5F5F5;
    border: 2px solid #CCCCCC;
    border-radius: 3px 3px 3px 3px;
    float: right;
    padding: 7px 0;  
}
#g_selected{
    background: none repeat scroll 0 0 #B5AFAF;
    border-radius: 0 0 0 0;
    box-shadow: 0 0 2px #474747;
}

.action_span {
    background: none repeat scroll 0 0 #EFEFEF;
}
</style>
<div class="box threethirds">	
	 <div class="boxheading clearfix"><h3><img src="images/icons/fatcow-hosting-icons/32x32/chart_bar.png" alt="" />Visitor Statistics</h3><a class="move"></a></div>
	 <section>
			<div class="container_inner" style="padding:0px 45px 0px 45px;">
				<!-- Monthly page view-->
				<?php 
					 if($g_sort=='1'): 
						 echo $init->graphPerDayCount('chart_div','505050','058dc7','e6f4fa',600,200);
					else: 
						 echo $init->graphPerMonthsCount("chart_div") ; 
					 endif;
				?>
				<div class="chart_div" style="width:970px;">
				<div class="left_headding" style="width:40%;float:left;">
				<h4>Monthly Traffic</h4>
				</div>
				<div id="right_action" >
					<a href="<?php echo make_admin_url('home','list','list&g_sort=1')?>">
					<span style="padding:7px;" class="action_span" <?php if($g_sort=='1'): echo "id='g_selected'";endif;?>>Monthly</span>
					</a>				
					<a href="<?php echo make_admin_url('home','list','list&g_sort=0')?>" >
					<span style="padding:7px;" class="action_span" <?php if($g_sort!='1'): echo "id='g_selected'"; endif;?>>Yearly</span>
					</a>
				</div>
				<div style="clear:both;"></div>
				
				
				
				
				<div class="blocks" id="main_chart">
				<div id="chart_div" style="width:100%; height:330px;"></div>		
				</div>
				<div style="clear: both; border-top: 1px solid rgb(223, 223, 223); height: 27px; margin-top: 20px;"></div>
				
				
				<!-- start of first Block -->
				<div class="first_block">	
				<div class="blocks" style="float:left;width:50%;">
				<h4>Traffic Count </h4>				
				<?php  echo $init->graphSourceCount() ; ?>
				</div>
				<div class="blocks" style="float:right;width:50%;">
				<h4>&nbsp;</h4>				
				<table style="top: 5px; right: 5px; font-size: smaller; color: rgb(84, 84, 84); width: 90%; float: right;border-left: 1px solid #DFDFDF;">
				<tr>
				<th> Type </th>
				<th> Values </th>
				</tr>
				<?php foreach($trafficCountNum as $key => $value) { ?>
				<tr>
				<td class="legendLabel" ><?php echo ucfirst($key); ?></td>
				<td><?=$value?></td>
				</tr>
				<?php } ?>
				</table>			
				</div>
				
				</div>
				<!-- End of first Block -->
				<div style="clear: both; border-bottom: 1px solid rgb(223, 223, 223); margin-top: 20px; margin-bottom: 30px; height: 16px;"></div>	

				<!-- Start of Second Block -->
				<div class="Second_block">	
				<div class="blocks" style="float:left;width:50%;">
				<h4>Traffic Type </h4>
				<?php  echo $init->graphVisitorType(); ?>
				</div>
				<div class="blocks" style="float:left;width:50%;">
				<h4>&nbsp;</h4>				
				<table style="top: 5px; right: 5px; font-size: smaller; color: rgb(84, 84, 84); width: 90%; float: right;border-left: 1px solid #DFDFDF;">
				<tr>
				<th> Type </th>
				<th> Values </th>
				</tr>
				<?php foreach($trafficCount[0] as $key => $value) { ?>
				<tr>
				<td class="legendLabel" ><?php echo ucfirst(preg_replace('/(?<!\ )[A-Z]/', ' $0', $key)); ?></td>
				<td align='center'>  <?php if(is_float($value)): echo number_format($value,2); else: echo $value; endif;?>
				</td>
				</tr>
				<?php	}?>
				</table>
				</div>
				<div style="clear:both;"></div>	
				</div>
				<!-- end second blocks -->	
				
				<div style="clear:both"></div>
			</div>
			<div style="clear:both"></div>
		</section>
	</div>
