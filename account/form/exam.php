<!-- section coding -->
<?php
/* sections */
switch ($section):
    case 'list':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/list.php');
	    break;    
    case 'view':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/view.php');
            break;  
    case 'print':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/print.php');
            break; 		
    case 'student':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/student.php');
            break; 	
    case 'result':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/result.php');
            break; 				
    case 'insert':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/create.php');
            break;  
    case 'update':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/edit.php');
            break; 		
    case 'group':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/group.php');
            break; 	
    case 'add_group':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/add_group.php');
            break; 	
    case 'edit_group':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/edit_group.php');
            break; 			
     case 'thrash':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/thrash.php');
           break;            
    default:break;
endswitch;
?>

	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {    
            $(".check_valid").live("click",function(){
             // Validation engine
                    $(".validation").validationEngine({
                         promptPosition : "topLeft"
                        
                    });
          	});  
		});
	</script>