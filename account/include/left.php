<div class="page-sidebar nav-collapse collapse">
    <ul class="page-sidebar-menu">
        <li style="margin-bottom:10px;">
            <div class="sidebar-toggler hidden-phone"></div>
        </li>
        <li class="start <?php echo ($Page == 'home') ? 'active' : '' ?> ">
            <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">
                <i class="icon-home"></i> 
                <span class="title">Dashboard</span>
                <span class="selected"></span>
            </a>
        </li>
        <li class="start <?php echo ($Page == 'messages') ? 'active' : '' ?> ">
            <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>">
                <i class="icon-envelope"></i> 
                <span class="title">Messages</span>
                <span class="selected"></span>
            </a>
        </li>
        <li class="start <?php echo ($Page == 'time_table') ? 'active' : '' ?> ">
            <a href="<?php echo make_admin_url('time_table', 'list', 'list'); ?>">
                <i class="icon-calendar"></i> 
                <span class="title">Time Table</span>
                <span class="selected"></span>
            </a>
        </li>
        <li class="start <?php echo ($Page == 'complaints') ? 'active' : '' ?> ">
            <a href="<?php echo make_admin_url('complaints', 'list', 'list'); ?>">
                <i class="icon-adn"></i> 
                <span class="title">Complaints</span>
                <span class="selected"></span>
            </a>
        </li>
        <li class="<?php echo ($Page == 'enquiry') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/enquiry.php'); ?>
        </li> 
        <li class="<?php echo ($Page == 'applications' || $Page == 'leave_types') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/applications.php'); ?>
        </li> 
        <li class="<?= ($Page == 'course' || $Page == 'subject' || $Page == 'fee_head' || $Page == 'holiday' || $Page == 'porfile' || $Page == 'syllabus' || $Page == 'event') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/institute.php'); ?>
        </li>
        <li class="<?php echo ($Page == 'salary' || $Page == 'staff' || $Page == 'category' || $Page == 'designation' || $Page == 'staff_attendance' || $Page == 'staff_pay_rule' || $Page == 'bank') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/staff.php'); ?>
        </li>				
        <li class="<?php echo ($Page == 'session' || $Page == 'session_info') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/session.php'); ?>
        </li>  
        <li class="<?php echo ($Page == 'student' || $Page == 'view' || $Page == 'attendance' || $Page == 'exam' || $Page == 'grade' || $Page == 'fee') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/student.php'); ?>
        </li>


        <li class="<?php echo ($Page == 'vehicles') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/vehicles.php'); ?>
        </li>				
        <li class="<?php echo ($Page == 'circulars') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/circulars.php'); ?>
        </li>				
        <li class="<?php echo ($Page == 'school_sales' || $Page == 'school_sales_category' || $Page == 'stock') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/school_sales.php'); ?>
        </li>				
        <li class="<?php echo ($Page == 'expense' || $Page == 'income') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/account.php'); ?>
        </li> 

        <li class="<?php echo ($Page == 'certificate') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/certificate.php'); ?>
        </li>			
        <li class="<?php echo ($Page == 'report' || $Page == 'account') ? 'active' : '' ?> ">
            <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">
                <i class="icon-bar-chart"></i> 
                <span class="title">Reports</span>
                <span class="selected"></span>
            </a>
        </li>
        <li class="<?php echo ($Page == 'user') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/user.php'); ?>
        </li>	
        <li class="<?php echo ($Page == 'sms') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/sms.php'); ?>
        </li>					
        <li class="<?php echo ($Page == 'gallery' || $Page == 'notice' || $Page == 'content' || $Page == 'time' || $Page == 'social') ? 'active' : '' ?>">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/left/website.php'); ?>
        </li> 				
        <li class="<?php echo ($Page == 'logout') ? 'active' : '' ?>">
            <a href="<?php echo make_admin_url('logout', 'list', 'list'); ?>">
                <i class="icon-lock"></i> 
                <span class="title">Logout</span>
                <span class="selected"></span>
            </a>
        </li>				
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->

<!-- BEGIN PAGE -->
<div class="page-content">

