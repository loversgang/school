<!DOCTYPE html>
<!--
--- About Us  ---
Content Management System
Developed by:- cWebConsultants India
http://www.cwebconsultants.com
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>
            <?php
            if (defined('SITE_NAME')):
                echo 'Management Panel | ' . SITE_NAME;
            else:
                echo "Management Panel";
            endif;
            ?>
        </title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/jqplot/jquery.jqplot.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
        <link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
        <link rel="stylesheet" type="text/css" href="assets/plugins/toastr/toastr.min.css"/>
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES for datatables-->
        <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
        <link rel="stylesheet" href="assets/plugins/data-tables/DT_bootstrap.css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES for validation -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/validation/validationEngine.jquery.css" />
        <!-- END PAGE LEVEL STYLES -->
        <!--fancybox-->
        <link href="assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
        <!--fancybox ends-->

        <!-- Image Croping Css -->
        <link href="assets/css/jrac/style.jrac.css" rel="stylesheet" type="text/css" media="all"></link>		
        <link href="assets/css/jrac/base-jquery-ui.css" rel="stylesheet" type="text/css" media="all"></link>
        <!--jrac ends-->
        <!--inbox-->
        <link href="assets/css/pages/inbox.css" rel="stylesheet" type="text/css" />
        <!--inbox ends-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
        <script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/plugins/toastr/toastr.min.js" type="text/javascript"></script>
        <!--[if lt IE 9]>
        <script src="assets/plugins/excanvas.min.js"></script>
        <script src="assets/plugins/respond.min.js"></script>  
        <![endif]-->   
        <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
        <script src="assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
        <script src="assets/plugins/jqplot/jquery.jqplot.min.js" type="text/javascript" ></script>
        <script src="assets/plugins/jqplot/jqplot.donutRenderer.min.js" type="text/javascript" ></script>
        <script src="assets/plugins/jqplot/jqplot.pieRenderer.min.js" type="text/javascript" ></script>
        <script src="assets/plugins/jqplot/jqplot.highlighter.min.js" type="text/javascript" ></script>
        <!-- END CORE PLUGINS -->
        <script type="text/javascript" src="assets/plugins/ckeditor/ckeditor.js"></script>  
        <script type="text/javascript" src="assets/plugins/ckfinder/ckfinder.js"></script>
        <script type="text/javascript" src="assets/plugins/ckeditor/adapters/jquery.js"></script> 
        <script>
            jQuery(document).ready(function () {

                var config = {
                    toolbar:
                            [
                                ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
                                ['UIColor', 'Image', 'Format', 'TextColor', 'Source', 'PasteText']
                            ]
                };

                var config2 = {
                    toolbar:
                            [
                                ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', '-', 'Source'],
                                ['UIColor', 'Image', 'TextColor', 'BGColor', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],
                                ['Styles', 'Format', 'Font', 'FontSize'],
                                ['-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'],
                                ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']
                            ]
                };

                $('.editor').ckeditor(config);
                $('.editor_full').ckeditor(config2);
                CKFinder.setupCKEditor(null, 'assets/plugins/ckfinder/');
            });
        </script>
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <?php
    include_once(DIR_FS_SITE . 'include/functionClass/messageClass.php');
    include_once(DIR_FS_SITE . 'include/functionClass/applicationClass.php');
    $total_unread_msgs = school_header::getInboxCountUnread($school->id, $_SESSION['admin_session_secure']['user_id'], 'admin');
    $total_appliation_count = application:: countStaffApplication();
    $staff_appliation_count = application:: countStaffApplication('staff');
    $student_appliation_count = application:: countStaffApplication('student');
    ?>
    <body class="page-header-fixed">
        <div class="header navbar navbar-inverse navbar-fixed-top hidden-print">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="brand" style="margin-left:5px;color:white;width:auto" href="<?php echo make_admin_url('home', 'list', 'list'); ?>">
                        <?= SITE_NAME ?>
                    </a>
                    <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                        <img src="assets/img/menu-toggler.png" alt="" />
                    </a>          
                    <ul class="nav pull-right">
                        <li class="dropdown" id="header_inbox_bar">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <?php if ($total_appliation_count > 0) { ?>
                                    <span class="badge badge-default">
                                        <span id="decrease_notification">
                                            <?php echo $total_appliation_count ?>
                                        </span>
                                    </span>
                                <?php } ?>
                                <i class="icon-bell"></i>
                            </a>
                            <ul class="dropdown-menu extended notification">
                                <li>
                                    <a href="<?php echo make_admin_url('applications', 'staff', 'staff') ?>"><span class="label label-important" style='width:13px;'><i class="icon-male"></i></span>
                                        You have <strong><?php echo $staff_appliation_count ?></strong>  new Applications of Staff
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo make_admin_url('applications', 'student', 'student') ?>"><span class="label label-important" style='width:13px;'><i class="icon-male"></i></span>
                                        You have <strong><?php echo $student_appliation_count ?></strong> new Application of Student
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown" id="header_inbox_bar">
                            <a href="<?php echo make_admin_url('messages'); ?>" class="dropdown-toggle">
                                <?php if ($total_unread_msgs > '0') { ?>
                                    <span class="badge badge-default">
                                        <?php echo $total_unread_msgs; ?>
                                    </span>
                                <?php } ?>
                                <i class="icon-comment"></i>
                            </a>
                            <ul class="dropdown-menu extended notification"></ul>
                        </li>
                        <?php
                        $QueryObjAl = new vehicle();
                        $alerts = $QueryObjAl->getPermitInsuranceExpiry($school->id);
                        ?>
                        <?php if ($alerts['total'] > 0): ?>
                            <li class="dropdown" id="header_notification_bar">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-warning-sign"></i>
                                    <span class="badge"><?= $alerts['total'] ?></span>
                                </a>
                                <ul class="dropdown-menu extended notification">
                                    <li>
                                        <p>You have <?= $alerts['total'] ?> new notifications</p>
                                    </li>
                                    <?php
                                    if (count($alerts['insurance']) > 0):
                                        foreach ($alerts['insurance'] as $i_kk => $i_vv):
                                            ?>
                                            <li>
                                                <a href="<?= make_admin_url('vehicles', 'update', 'update&id=' . $i_vv->id) ?>">
                                                    <span class="label label-important" style='width:13px;'><i class="icon-bolt"></i></span>
                                                    <span class="time"><?= $i_vv->vehicle_number ?></span> Insurance Expire on <?= date('d,M Y', strtotime($i_vv->insurance_expiry)) ?>.
                                                </a>
                                            </li>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>							
                                    <?php
                                    if (count($alerts['permit']) > 0 && $alerts['permit'] != ''):
                                        foreach ($alerts['permit'] as $p_kk => $p_vv):
                                            ?>
                                            <li>
                                                <a href="<?= make_admin_url('vehicles', 'update', 'update&id=' . $p_vv->id) ?>">
                                                    <span class="label label-warning" style='width:13px;'><i class="icon-bell"></i></span>
                                                    <span class="time"><?= $p_vv->vehicle_number ?></span> Permit Expire on <?= date('d,M Y', strtotime($p_vv->permit_expiry)) ?>.
                                                </a>
                                            </li>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>		
                                </ul>
                            </li>
                        <?php endif; ?>
                        <li class="dropdown user">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                <?
                                if (!empty($school->logo)):
                                    $im_obj = new imageManipulation()
                                    ?>
                                    <img src="<?= $im_obj->get_image('school', 'medium', $school->logo); ?>" style='width:29px;' />
                                <? else: ?>	
                                    <img alt="" src="assets/img/avatar.png" style='width:29px;'/>
                                <? endif; ?>	
                                <span class="username"><?php echo ucfirst($_SESSION['admin_session_secure']['username']); ?></span>
                                <i class="icon-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo make_admin_url('profile'); ?>"><i class="icon-user"></i> My Profile</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo make_admin_url('profile', 'update', 'update'); ?>"><i class="icon-user"></i> Change Password</a></li>
                                <li class="divider"></li>
                                <? if ($school->is_website == '1'): ?>
                                    <li><a href="<?php echo DIR_WS_SITE . 'site/' . $school->id; ?>/" target='_blank'><i class="icon-won"></i> View Website</a></li> 
                                    <li class="divider"></li>
                                <? endif; ?>
                                <li><a href="<?php echo make_admin_url('logout'); ?>"><i class="icon-key"></i> Log Out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="page-container">