

        <div class="tile bg-purple <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('expense', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-arrow-left"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Back To Expenses
                        </div>
                </div>
            </a>   
        </div>

        <div class="tile bg-green <?php echo ($section=='cat_list')?'selected':''?>">
            <a href="<?php echo make_admin_url('expense', 'cat_list', 'cat_list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-retweet"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Expense Categories
                        </div>
                </div>
            </a>   
        </div>


        <div class="tile bg-blue <?php echo ($section=='cat_insert')?'selected':''?>">
            <a href="<?php echo make_admin_url('expense', 'cat_insert', 'cat_insert');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                New Category
                        </div>
                </div>
            </a> 
        </div>

