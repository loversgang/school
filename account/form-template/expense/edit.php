<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage School Expenses
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>  
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('expense', 'list', 'list'); ?>">List Expenses</a>
                    <i class="icon-angle-right"></i>                                       
                </li>									
                <li class="last">
                    Edit Expenses
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption"><i class="icon-rupee"></i>Edit Expense</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <form class="form-horizontal" action="<?php echo make_admin_url('expense', 'update', 'update') ?>" method="POST" enctype="multipart/form-data" id="validation">
                <!-- / Box -->
                <div class="row-fluid">
                    <div class="span12">
                        <div class="span4">
                            <div class="control-group">
                                <label class="control-label" for="title">Title<span class="required">*</span></label>
                                <div class="controls">
                                    <input type="text" name="title" value="<?= $object->title ?>" id="title" class="m-wrap validate[required]" />

                                </div>
                            </div>  
                            <div class="control-group">
                                <label class="control-label" for="category">Expense Category<span class="required">*</span></label>
                                <div class="controls">
                                    <select name='category' id='category' class="select2 m-wrap validate[required]" placeholder='Select Exp Type'>
                                        <?php
                                        $sr = 1;
                                        while ($cat = $QueryCat->GetObjectFromRecord()):
                                            ?>
                                            <option value='<?= $cat->id ?>' <?= ($cat->id == $object->category) ? 'selected' : '' ?>><?= $cat->title ?></option>
                                            <?
                                            $sr++;
                                        endwhile;
                                        ?>							
                                    </select>                                                  
                                </div>
                            </div>							
                            <div class="control-group">
                                <label class="control-label" for="on_date">On Date<span class="required">*</span></label>
                                <div class="controls">
                                    <input type="text" name="on_date" value="<?= $object->on_date ?>"  class="upto_current_date m-wrap validate[required]" />
                                </div>
                            </div> 
                            <div class="control-group">
                                <label class="control-label" for="amount">Amount (<?= CURRENCY_SYMBOL ?>)<span class="required">*</span></label>
                                <div class="controls">
                                    <input type="text" name="amount"  value="<?= $object->amount ?>" id="amount" class="m-wrap validate[required]" />
                                </div>
                            </div> 				
                            <div class="control-group">
                                <label class="control-label" for="remarks">Description<span class="required"></span></label>
                                <div class="controls">
                                    <textarea type="text" name="remarks" id="remarks" class="m-wrap"><?= $object->remarks ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="span8">
                            <?php if (!$expense_docs) { ?>
                                <div class="row-fluid" id="more_docs">
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label" for="title">Doc Title</label>
                                            <div class="controls">
                                                <input type="text" name="doc_title[]" value="" id="title" class="span12 m-wrap"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label span4" for="doc">Attachment</label>
                                            <div class="controls span8">
                                                <input type="file" name="doc[]" value="" id="doc" class="span12 m-wrap"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div id="more_docs">
                                    <?php foreach ($expense_docs as $expense_doc) { ?>
                                        <div class="row-fluid" id="doc_single_<?= $expense_doc->id ?>">
                                            <div class="span6">
                                                <div class="control-group">
                                                    <label class="control-label" for="title">Doc Title</label>
                                                    <div class="controls">
                                                        <input type="text" name="title_old[]" value="<?= $expense_doc->title ?>" id="title" class="span12 m-wrap"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span6">
                                                <div class="control-group">
                                                    <label class="control-label span4" for="doc">Attachment</label>
                                                    <div class="controls span8">
                                                        <input type="file" name="doc_old[]" value="" id="doc" class="span9 m-wrap"/><button type="button"  id="delete_doc" data-id="<?= $expense_doc->id ?>" class="btn red"><i class="icon-trash"></i></button>
                                                        <input type="hidden" name="doc_id[]" value="<?= $expense_doc->id ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <div class="control-group">
                                <label class="control-label span8" for="doc"></label>
                                <div class="controls">
                                    <button type="button" class="btn green span4" id="add_more_fields">Add More</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 
                    <input type="hidden" name="id" value="<?= $object->id ?>" tabindex="7" /> 													 
                    <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                    <a href="<?php echo make_admin_url('expense', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                </div>                                   
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->
<script>
    $(document).on('click', '#add_more_fields', function () {
        $('#more_docs').append('<div class="row-fluid"><div class="span6"><div class="control-group"><label class="control-label" for="title">Doc Title</label><div class="controls"><input type="text" name="doc_title[]" value="" id="title" class="span12 m-wrap"/></div></div></div><div class="span6"><div class="control-group"><label class="control-label span4" for="doc">Attachment</label><div class="controls span8"><input type="file" name="doc[]" value="" id="doc" class="span9 m-wrap"/><button type="button" id="remove_field" class="btn red"><i class="icon-trash"></i></button></div></div></div></div>');
    });
    $(document).on('click', '#remove_field', function () {
        $(this).closest('.row-fluid').hide(400, function () {
            $(this).remove();
        });
    });
    $(document).on('click', '#delete_doc', function () {
        var doc_id = $(this).attr('data-id');
        if (confirm("Are You Sure? You want to delete this document?")) {
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=expense'); ?>", {act: 'delete_doc', doc_id: doc_id}, function (data) {
                if (data === 'success') {
                    $('#doc_single_' + doc_id).hide(400, function () {
                        $(this).remove();
                    });
                    toastr.success('Document Deleted Successfully!', 'Success');
                }
            });
        }
    });
</script>

