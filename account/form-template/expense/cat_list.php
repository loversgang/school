
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Expense Categories
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        List Categories
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut_cat.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i>Expense Categories</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
												<th style="width:45px;">Sr. No</th>
												<th style='width:70%;'>Title</th>
												<th >Action</th>
										</tr>
									</thead>
                                        <? if($QueryObj->GetNumRows()!=0):?>
										<tbody>
                                            <?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
                                                <tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td><?=$object->title?></td>
													<td>
													<a class="btn blue icn-only tooltips" href="<?php echo make_admin_url('expense', 'cat_update', 'cat_update', 'id='.$object->id)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
													<a class="btn red icn-only tooltips" href="<?php echo make_admin_url('expense', 'cat_delete', 'cat_delete', 'id='.$object->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
													</td>
												</tr>
                                            <?php $sr++;
												endwhile;?>
										</tbody>
									   <?php endif;?>  
								</table>
                            </form>    
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



