	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
<style>
body {
    color: #000000;
    direction: ltr;
    font-family: 'Open Sans';
    font-size: 13px;
	background-color: #FFF !important;
}

.portlet.box.yellow {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color #FCCB7E #FCCB7E;
    border-image: none;
    border-right: 1px solid #FCCB7E;
    border-style: none solid solid;
    border-width: 0 1px 1px;
}
.portlet.box {
    padding: 0 !important;
}
.portlet-body.yellow, .portlet.yellow {
    background-color: #FFB848 !important;
}
.portlet {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}

.caption{
    display: inline-block;
    float: left;
    font-size: 18px;
    font-weight: 400;
    margin: 0 0 7px;
    padding: 0 0 8px;
	background:#D9EDF7;
	padding:5px;
}
table th{
	background:#BF504D;
	color:#fff;
}
.title{
	background:#D9EDF7;
	padding-left:5px;
	padding-right:5px;
	padding-top:5px;
}
.main_title1{
	margin-top:5px;
}

</style>

<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');


isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';
isset($_REQUEST['session_id'])?$session_id=$_REQUEST['session_id']:$session_id='';
isset($_REQUEST['ct_sec'])?$ct_sec=$_REQUEST['ct_sec']:$ct_sec='';
isset($_REQUEST['exam'])?$exam=$_REQUEST['exam']:$exam='';
isset($_REQUEST['subject'])?$subject=$_REQUEST['subject']:$subject='';
isset($_REQUEST['ex_type'])?$ex_type=$_REQUEST['ex_type']:$ex_type='all';

#Get exam Students result
$QueryOb=new examination();
$record=$QueryOb->examination_result($exam);
	
$session=get_object('session',$session_id);

#Get exam Grades
$QueryGrOb=new examGrade();
$all_grade=$QueryGrOb->listAll($school->id);

	if($ex_type=='term'):
			#groups
			$QueryObjExam=new examinationGroup();
			$groups=$QueryObjExam->getTermExamSessionSection($session_id,$ct_sec);		
		
			if(!empty($groups) && empty($exam)):
				$exam=$groups['0']->id;
			endif;
		
			#get exam Info
			$QueryG=new examinationGroup();
			$group=$QueryG->getRecord($exam);			
			
			if(!is_object($group)): $group->exam_id='0'; endif;
			#get Subject names
			$QueryExam=new examination();
			$exams=$QueryExam->groupExaminations($group->exam_id);			
			
			#Get exam Students result
			$QueryOb=new examinationMarks();
			$record=$QueryOb->getExamStudents($group->exam_id);

	elseif($ex_type=='all'):
			#get exam groups whose subject id is selected
			$QueryG=new examinationGroup();
			$groups=$QueryG->getTermExamSessionSection($session_id,$ct_sec);	

			if(!empty($groups)):
				foreach($groups as $keyy=>$vall):
				$all_exam_array[]=$vall->exam_id;
				endforeach;
			endif;
			$all_exams=implode(',',$all_exam_array);
			
			if(empty($all_exams)):
				$all_exams='0';
			endif;
			
			#Get exam Students result
			$QueryOb=new examinationMarks();
			$record=$QueryOb->getExamStudents($all_exams);
	
	elseif($ex_type=='term_subject'):

		$session_info=get_object('session',$session_id);	
			
			#Get Subject List
			$QuerySubject=new subject();
			$sessionSubjects=$QuerySubject->listSessionSubject($session_info->compulsory_subjects,$session_info->elective_subjects,'1','array');		
			
			if(empty($subject)):
				$subject=$sessionSubjects['0']->id;
			endif;
			
			$subject_detail=get_object('subject_master',$subject);
			
			#get exam groups whose subject id is selected
			$QueryG=new examinationGroup();
			$record=$QueryG->getSelectedSubjectGroup($session_id,$ct_sec,$subject);	
			
	elseif($ex_type=='subject'):
			#here subject will be the exam id and exam will be the group;
			if(!empty($subject)):			
				#Get exam Students result
				$QueryOb=new examinationMarks();
				$record=$QueryOb->getExamStudents($subject);
			
				#get exam group Info
				$QueryG=new examinationGroup();
				$group=$QueryG->getRecord($exam);
				
				#get Exam detail with its subject
				$QueryExam=new examination();
				$exams=$QueryExam->groupExaminations($group->exam_id);	
			endif;
			
			#All groups
			$QueryObjExam=new examinationGroup();
			$groups=$QueryObjExam->getTermExamSessionSection($session_id,$ct_sec);		
		
			if(!empty($groups) && empty($exam)):
				$exam=$groups['0']->id;
			endif;			
			
			if(!empty($exam)): 
				$group_detail=get_object('student_examination_group',$exam);
				
				#get Session Info
				$QueryExsub=new examinationGroup();
				$AllSubject=$QueryExsub->getSubjectByExams($group_detail->exam_id);	
				
			endif;			
	endif;


?>

<? 

if(!empty($record)):

	if($ex_type=='all'):?> 
	
	<!-- BEGIN PAGE CONTAINER-->
		<div style='margin:20px;border:1px solid;'>
            <!-- BEGIN PAGE HEADER-->							
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;"><br/>CHECK LIST OF ALL UNIT TERMS EXAM REPORT</td></tr>
								<tr><td class='title'><strong>SESSION:- <?=$session->title?></strong></td><td class='title' style='text-align:right;'><strong>SECTION:- <?=$ct_sec?></strong></td></tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover">
									<thead>
											<tr>
												<th class="hidden-480" style='vertical-align:middle;'>SR. NO.</th>
												<th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>STUDENT NAME</th>
												<th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>FATHER NAME</th>
												<th class="hidden-480" style='vertical-align:middle;'>ROLL NO.</th>
												<th class="hidden-480" style='vertical-align:middle;text-align:center;'>SECTION</th>
												<? if(!empty($groups)):
													foreach($groups as $e=>$v):?>
													<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?=$v->title?><br/>
														<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>Total</th> <th style='border:none;background:#BF504D;text-align:center;'>Obtain</th> </tr></table>
													</th>
												<? 	endforeach; ?>
													<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>ALL TERMS<br/>
														<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>Total</th> <th style='border:none;background:#BF504D;text-align:center;'>Obtain</th> </tr></table>
													</th>	
													<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>POSITION<br/>
														<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>%Age</th> <th style='border:none;background:#BF504D;text-align:center;'>Grade</th> </tr></table>
													</th>	
												<?	endif;?>
											</tr>
									</thead>
                                        <? if(!empty($record)): $sum=''; $sr='1';?>
												<tbody>
										<?php $sr=1;foreach($record as $sk=>$stu): $checked='';?>
											<tr class="odd gradeX">
												<td class="hidden-480"><?=$sr?>.</td>
												<td class="hidden-480"><?=$stu->first_name." ".$stu->last_name?></td>
												<td class=''><?=ucfirst($stu->father_name)?></td>
												<td class="hidden-480"><?=$stu->roll_no?></td>
												<td class="hidden-480" style='text-align:center;'><?=$stu->section?></td>
												<? if(!empty($groups)):  $tm='';$om='';
													foreach($groups as $ee=>$vv): 
													#get Student result
													$QueryRes=new examinationMarks();
													$Result=$QueryRes->get_student_exam_marks_in_multpale($vv->exam_id,$stu->id);
														if(!empty($Result)):?> 
															<td style='text-align:center' class="hidden-480"><?=$Result['total']?></td>
															<td style='text-align:center' class="hidden-480"><?=$Result['obtain']?></td>
												<? 		$tm=$tm+$Result['total'];   $om=$om+$Result['obtain']; 
														else:
															echo "<td></td><td></td>";
														endif;
													endforeach;
													endif;?>
												<td style='text-align:center' class="hidden-480"><?=$tm?></td>
												<td style='text-align:center' class="hidden-480"><?=$om?></td>
												<? if(!empty($all_grade)): $grade='';
													foreach($all_grade as $kg=>$kv):
														if(((($om/$tm)*100)>=$kv->minimum) && ((($om/$tm)*100)<$kv->maximum)):  $grade=$kv->title;  endif;
													endforeach;	?>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=number_format(($om/$tm)*100,2)?>%</td>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$grade?></td>
												<? endif;?>									
											</tr>
										<? $sr++;
											endforeach;?>								
												</tbody>
									   <?php endif;?>  
								</table>
	
                            </form> 									
								</div>
							</div>
						
					
	
			</div>
	<? 
	
	elseif($ex_type=='term'):?> 
	<!-- BEGIN PAGE CONTAINER-->
		<div style='margin:20px;border:1px solid;'>
            <!-- BEGIN PAGE HEADER-->							
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;"><br/>CHECK LIST OF TERMS EXAM MARKS REPORT</td></tr>
								<tr><td class='title'><strong>SESSION:- <?=$session->title?></strong></td><td class='title' style='text-align:right;'><strong>SECTION:- <?=$ct_sec?></strong></td></tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover">
									<thead>
											<tr>
												<th class="hidden-480" style='vertical-align:middle;'>SR. NO.</th>
												<th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>STUDENT NAME</th>
												<th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>FATHER NAME</th>
												<th class="hidden-480" style='vertical-align:middle;'>ROLL NO.</th>
												<th class="hidden-480" style='vertical-align:middle;text-align:center;'>SECTION</th>
												<? if(!empty($exams)):
													foreach($exams as $e=>$v):?>
													<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?=$v->subject?><br/>
														<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>Total</th> <th style='border:none;background:#BF504D;text-align:center;'>Obtain</th> </tr></table>
													</th>
												<? 	endforeach; ?>
													<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>ALL SUBJECTS<br/>
														<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>Total</th> <th style='border:none;background:#BF504D;text-align:center;'>Obtain</th> </tr></table>
													</th>	
													<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>POSITION<br/>
														<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>%Age</th> <th style='border:none;background:#BF504D;text-align:center;'>Grade</th> </tr></table>
													</th>	
												<?	endif;?>
											</tr>
									</thead>
                                        <? if(!empty($record)): $sum=''; $sr='1';?>
												<tbody>
										<?php $sr=1;foreach($record as $sk=>$stu): $checked='';?>
											<tr class="odd gradeX">
												<td class="hidden-480"><?=$sr?>.</td>
												<td class="hidden-480"><?=$stu->first_name." ".$stu->last_name?></td>
												<td class=''><?=ucfirst($stu->father_name)?></td>
												<td class="hidden-480"><?=$stu->roll_no?></td>
												<td class="hidden-480" style='text-align:center;'><?=$stu->section?></td>
												<? if(!empty($exams)):  $tm='';$om='';
													foreach($exams as $ee=>$vv): 
													#get Student result
													$QueryRes=new examinationMarks();
													$Result=$QueryRes->get_student_exam_marks($vv->id,$stu->id);
														if(is_object($Result)):?> 
															<td style='text-align:center' class="hidden-480"><?=$vv->maximum_marks?></td>
															<td style='text-align:center' class="hidden-480"><?=$Result->marks_obtained?></td>
												<? 		$tm=$tm+$vv->maximum_marks;   $om=$om+$Result->marks_obtained; 
														else:
															echo "<td></td><td></td>";
														endif;
													endforeach;
													endif;?>
												<td style='text-align:center' class="hidden-480"><?=$tm?></td>
												<td style='text-align:center' class="hidden-480"><?=$om?></td>
												<? if(!empty($all_grade)): $grade='';
													foreach($all_grade as $kg=>$kv):
														if(((($om/$tm)*100)>=$kv->minimum) && ((($om/$tm)*100)<$kv->maximum)):  $grade=$kv->title;  endif;
													endforeach;	?>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=number_format(($om/$tm)*100,2)?>%</td>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$grade?></td>
												<? endif;?>									
											</tr>
										<? $sr++;
											endforeach;?>								
												</tbody>
									   <?php endif;?>  
								</table>
	
                            </form> 									
								</div>
							</div>
		</div>
<?		
	elseif($ex_type=='term_subject'):?> 
	<!-- BEGIN PAGE CONTAINER-->
		<div style='margin:20px;border:1px solid;'>
            <!-- BEGIN PAGE HEADER-->							
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;"><br/>CHECK LIST OF REGISTERED CANDIDATE WITH SUBJECT</td></tr>
								<tr><td class='title'><strong>SESSION:- <?=$session->title?></strong></td><td class='title' style='text-align:right;'><strong>SECTION:- <?=$ct_sec?></strong></td></tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover">
									<thead>
											<tr>
												<th class="hidden-480" style='vertical-align:middle;'>SR. NO.</th>
												<th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>STUDENT NAME</th>
												<th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>FATHER NAME</th>
												<th class="hidden-480" style='vertical-align:middle;'>ROLL NO.</th>
												<th class="hidden-480" style='vertical-align:middle;text-align:center;'>SECTION</th>
												<? if(!empty($record['terms'])):
													foreach($record['terms'] as $e=>$v):?>
														<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?=$v?> (<?=$subject_detail->name?>)
															<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>Total</th> <th style='border:none;background:#BF504D;text-align:center;'>Obtain</th> </tr></table>
														</th>
												<? 	endforeach;?>
														<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>All Term<br/>
															<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>Total</th> <th style='border:none;background:#BF504D;text-align:center;'>Obtain</th> </tr></table>
														</th>	
														<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>Position<br/>
															<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>%AGE</th> <th style='border:none;background:#BF504D;text-align:center;'>GRADE</th> </tr></table>
														</th>													
												<?	endif;?>						

											</tr>
									</thead>
										<? if(!empty($record['record'])):?>
										<tbody>
										<?php $sr=1;foreach($record['record'] as $sk=>$stu): $checked='';	?>	
											<tr class="odd gradeX">
												<td class="hidden-480"><?=$sr?>.</td>
												<td class="hidden-480"><?=$stu->first_name." ".$stu->last_name?></td>
												<td class=''><?=ucfirst($stu->father_name)?></td>
												<td class="hidden-480"><?=$stu->roll_no?></td>
												<td class="hidden-480" style='text-align:center;'><?=$stu->section?></td>
												<? if(!empty($record['terms'])):  $tm='';$om='';
													foreach($record['terms'] as $ee=>$vv): 
													#get Student result
													$QueryRes=new examinationMarks();
													$Result=$QueryRes->getStudentResult($ee,$stu->id);
														if(is_object($Result)):?> 
															<td style='text-align:center' class="hidden-480"><?=$Result->maximum_marks?></td>
															<td style='text-align:center' class="hidden-480"><?=$Result->marks_obtained?></td>
													<? 	$tm=$tm+$Result->maximum_marks;   $om=$om+$Result->marks_obtained; 
														else:
															echo "<td></td><td></td>";
														endif;
													endforeach;
													endif;?>
												<td style='text-align:center' class="hidden-480"><?=$tm?></td>
												<td style='text-align:center' class="hidden-480"><?=$om?></td>
												<? if(!empty($all_grade)): $grade='';
													foreach($all_grade as $kg=>$kv):
														if(((($om/$tm)*100)>=$kv->minimum) && ((($om/$tm)*100)<$kv->maximum)):  $grade=$kv->title;  endif;
													endforeach;	?>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=number_format(($om/$tm)*100,2)?>%</td>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$grade?></td>
												<? endif;?>									
											</tr>
										<? $sr++;
											endforeach;?>								
												</tbody>
									   <?php endif;?>  
								</table>
	
                            </form> 									
								</div>
							</div>
		</div>		
		
<?		
	elseif($ex_type=='subject'):?> 
	<!-- BEGIN PAGE CONTAINER-->
		<div style='margin:20px;border:1px solid;'>
            <!-- BEGIN PAGE HEADER-->							
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;"><br/>CHECK LIST OF SUBJECT WISE MARKS DETAIL</td></tr>
								<tr><td class='title'><strong>SESSION:- <?=$session->title?></strong></td><td class='title' style='text-align:right;'><strong>Term:- <?=$group_detail->title?></strong></td></tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover">
									<thead>
											<tr>
												<th class="hidden-480" style='vertical-align:middle;'>SR. NO.</th>
												<th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>STUDENT NAME</th>
												<th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>FATHER NAME</th>
												<th class="hidden-480" style='vertical-align:middle;'>ROLL NO.</th>
												<th class="hidden-480" style='vertical-align:middle;text-align:center;'>SECTION</th>
												<? if(!empty($exams)):
														foreach($exams as $e=>$v):
															if($v->id==$subject):?>
															<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?=$v->subject?><br/>
																<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>Total</th> <th style='border:none;background:#BF504D;text-align:center;'>Obtain</th> </tr></table>
															</th>
														<? 	endif;
														endforeach; ?>
													<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>All Subjects<br/>
														<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>Total</th> <th style='border:none;background:#BF504D;text-align:center;'>Obtain</th> </tr></table> 
													</th>	
													<th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>Position<br/>
														<table style='width:100%;'><tr><th style='border:none;background:#BF504D;text-align:center;'>%AGE</th> <th style='border:none;background:#BF504D;text-align:center;'>GRADE</th> </tr></table> 
													</th>													
												<?	endif;?>
											</tr>
									</thead>
										<? if(!empty($record)):?>
										<tbody>
										<?php $sr=1;foreach($record as $sk=>$stu): $checked='';	?>	
											<tr class="odd gradeX">
												<td class="hidden-480"><?=$sr?>.</td>
												<td class="hidden-480"><?=$stu->first_name." ".$stu->last_name?></td>
												<td class=''><?=ucfirst($stu->father_name)?></td>
												<td class="hidden-480"><?=$stu->roll_no?></td>
												<td class="hidden-480" style='text-align:center;'><?=$stu->section?></td>
												<? if(!empty($exams)):  $tm='';$om='';
													foreach($exams as $ee=>$vv): 
														if($vv->id==$subject):
														#get Student result
														$QueryRes=new examinationMarks();
														$Result=$QueryRes->get_student_exam_marks($vv->id,$stu->id);
															if(is_object($Result)):?> 
																<td style='text-align:center' class="hidden-480"><?=$vv->maximum_marks?></td>
																<td style='text-align:center' class="hidden-480"><?=$Result->marks_obtained?></td>
													<? 		$tm=$tm+$vv->maximum_marks;   $om=$om+$Result->marks_obtained; 
															else:
																echo "<td></td><td></td>";
															endif;
														endif;	
													endforeach;
													endif;?>
												<td style='text-align:center' class="hidden-480"><?=$tm?></td>
												<td style='text-align:center' class="hidden-480"><?=$om?></td>
												<? if(!empty($all_grade)): $grade='';
													foreach($all_grade as $kg=>$kv):
														if(((($om/$tm)*100)>=$kv->minimum) && ((($om/$tm)*100)<$kv->maximum)):  $grade=$kv->title;  endif;
													endforeach;	?>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=number_format(($om/$tm)*100,2)?>%</td>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$grade?></td>
												<? endif;?>									
											</tr>
										<? $sr++;
											endforeach;?>								
												</tbody>
									   <?php endif;?>  
								</table>
	
                            </form> 									
								</div>
							</div>
		</div>		
	<? endif;?>			
<? endif;?>	