
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Salary Reports
            </h3>
            <ul class="breadcrumb hidden-print">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li class="last">
                    Salary Reports
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right hidden-print">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>  			
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-left row-fluid hidden-print">
        <form class="" action="<?php echo make_admin_url('report', 'salary', 'salary') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
            <input type='hidden' name='Page' value='report'/>
            <input type='hidden' name='action' value='salary'/>
            <input type='hidden' name='section' value='salary'/>

            <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                <div class="row-fluid">	
                    <div class='span4'>
                        <div class='span12' style='margin-left: 5px;'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Staff Category</label>
                            <select class="select2_category span11" data-placeholder="Select Session Students" name='cat' >
                                <option value="">Select All</option>
                                <?php
                                $session_sr = 1;
                                while ($StCat = $QuerySalCat->GetObjectFromRecord()):
                                    ?>
                                    <option value='<?= $StCat->id ?>' <?
                                    if ($StCat->id == $cat) {
                                        echo 'selected';
                                    }
                                    ?> ><?= ucfirst($StCat->name) ?></option>
                                            <?
                                            $session_sr++;
                                        endwhile;
                                        ?>
                            </select>
                        </div>
                    </div>
                    <?
                    /* Get section Pages for current session */
                    $QuerySec = new studentSession();
                    $QuerySec->listAllSessionSection($session_id);

                    #get Session Info
                    $QueryS = new session();
                    $session = $QueryS->getRecord($session_id);
                    ?>
                    <div class='span5' style='margin-left: 5px;'>
                        <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                            <div class='span5' style='margin-left: 5px;'>
                                <label class="control-label">Select Month </label>
                                <div class="controls">
                                    <select name='month' class='span11'>
                                        <?= getMonthsOnly(date('Y-m-d', strtotime(date('Y-m-d') . ' -1 year')), date('Y-m-d'), $month); ?>
                                    </select>
                                </div>
                            </div>	
                            <div class='span6' style='margin-left: 5px;' >
                                <label class="control-label">Report Type</label>
                                <div class="controls">
                                    <select name='s_report_type' class='span10'>
                                        <? foreach ($salary_report_array as $rp_key => $rp_val): ?>
                                            <option value='<?= $rp_key ?>' <?
                                            if ($s_report_type == $rp_key): echo "selected";
                                            endif;
                                            ?>><?= $rp_val ?></option>
                                                <? endforeach; ?>											
                                    </select>
                                </div>
                            </div>									
                        </div>
                    </div>

                    <div class='span3' style='margin-left: 0px;'>
                        <div class='span12' style='margin-left: 5px;'>
                            <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                            <div class='span6' style='margin-left: 0px;'>
                                <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                            </div>
                            <div class='span6'>
                                <? if (!empty($record)): ?>
                                    <a class='btn yellow medium span6' href="<?= make_admin_url('report', 'download', 'download&cat=' . $cat . '&month=' . $month . '&s_report_type=' . $s_report_type . '&tmp=salary') ?>">PDF</a>
                                    <!--<a class='btn blue medium span6' href="<?= make_admin_url('report', 'excel', 'excel&cat=' . $cat . '&month=' . $month . '&s_report_type=' . $s_report_type . '&report_type=fee&tmp=salary') ?>">Excel</a> -->
                                <? endif; ?>
                            </div>
                        </div>	
                    </div>							
                </div>

                <div class="clearfix"></div>
            </div>

        </form>										
    </div>	


    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">	
            <? if ($s_report_type == "pending"): ?>	
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $salary_report_array[$s_report_type] ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Staff Name</th>
                                        <th>Staff Category</th>
                                        <th>Present</th>
                                        <th>Absent</th>
                                        <th>Leave</th>
                                        <th>Half Day</th>
                                    </tr>
                                </thead>
                                <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $key => $object):
                                            $Q_obj = new staffAttendance();
                                            $r_atten = $Q_obj->GetStaffAttendace($object->id, $first_date_of_month, $last_date_of_month);
                                            $t_attend = array_sum($r_atten);
                                            ?>
                                            <tr class="odd gradeX">	
                                                <td><?= $sr ?>.</td>												
                                                <td><?= $object->title . ' ' . ucfirst($object->first_name) . ' ' . $object->last_name ?></td>
                                                <td class="hidden-480 sorting_disabled"><?= $object->category ?></td>
                                                <td style='text-align:center'><?
                                                    if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $r_atten['P'];
                                                    else: echo '0';
                                                    endif;
                                                    ?></td>
                                                <td style='text-align:center'><?
                                                    if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
                                                    else: echo '0';
                                                    endif;
                                                    ?></td>
                                                <td style='text-align:center'><?
                                                    if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
                                                    else: echo '0';
                                                    endif;
                                                    ?></td>
                                                <td style='text-align:center'><?
                                                    if (!empty($r_atten) && array_key_exists('H', $r_atten)): echo $r_atten['H'];
                                                    else: echo '0';
                                                    endif;
                                                    ?></td>
                                                <?php
                                                $sr++;
                                            endforeach;
                                            ?>
                                    </tbody>
                                <?php endif; ?>  
                            </table>			
                        </div>
                    </div>
                </div>
            <? else: ?>	
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $salary_report_array[$s_report_type] ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Staff Name</th>
                                        <th>Staff Category</th>
                                        <th>Present</th>
                                        <th>Absent</th>
                                        <th>Leave</th>
                                        <th>Half Day</th>
                                        <th style='text-align:center'>Pending Date</th>
                                        <th style='text-align:right;'>Deduction (<?= CURRENCY_SYMBOL ?>)</th>
                                        <th style='text-align:right;'>Salary (<?= CURRENCY_SYMBOL ?>)</th>
                                    </tr>
                                </thead>
                                <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $key => $object):
                                            $Q_obj = new staffAttendance();
                                            $r_atten = $Q_obj->GetStaffAttendace($object->id, $first_date_of_month, $last_date_of_month);
                                            $t_attend = array_sum($r_atten);
                                            ?>
                                            <tr class="odd gradeX">													
                                                <td><?= $object->title . ' ' . ucfirst($object->first_name) . ' ' . $object->last_name ?></td>
                                                <td class="hidden-480 sorting_disabled"><?= $object->category ?></td>
                                                <td style='text-align:center'><?
                                                    if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $r_atten['P'];
                                                    else: echo '0';
                                                    endif;
                                                    ?></td>
                                                <td style='text-align:center'><?
                                                    if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
                                                    else: echo '0';
                                                    endif;
                                                    ?></td>
                                                <td style='text-align:center'><?
                                                    if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
                                                    else: echo '0';
                                                    endif;
                                                    ?></td>
                                                <td style='text-align:center'><?
                                                    if (!empty($r_atten) && array_key_exists('H', $r_atten)): echo $r_atten['H'];
                                                    else: echo '0';
                                                    endif;
                                                    ?></td>
                                                <td style='text-align:center' class="hidden-480 sorting_disabled"><?= $object->payment_date ?></td>
                                                <td style='text-align:right;'><?= CURRENCY_SYMBOL . " " . number_format($object->deduction, 2) ?></td>
                                                <td style='text-align:right;'><?= CURRENCY_SYMBOL . " " . number_format($object->amount, 2) ?></td>													
                                            </tr>
                                            <?php
                                            $sr++;
                                            $sum = $sum + $object->amount;
                                        endforeach;
                                        ?>
                                    </tbody>
                                <?php endif; ?>  
                            </table>		
                        </div>
                    </div>
                </div>						
            <? endif; ?>					

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>
</div>
<script>
    /* Print Document */
    $("#print").live("click", function () {
        var cat = '<?= $cat ?>';
        var month = '<?= $month ?>';
        var report_type = '<?= $s_report_type ?>';
        if (report_type === '') {
            alert("Sorry, You can't print this document..!");
            return false;
        }
        var dataString = '&cat=' + cat + '&month=' + month + '&s_report_type=' + report_type;

        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_salary_report', 'ajax_salary_report&temp=report'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
</script>