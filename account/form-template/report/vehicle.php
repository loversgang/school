
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Vehicle Reports
            </h3>
            <ul class="breadcrumb hidden-print">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li class="last">
                    Vehicle Reports
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right hidden-print">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>  			
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-left row-fluid hidden-print">
        <form class="" action="<?php echo make_admin_url('report', 'vehicle', 'vehicle') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
            <input type='hidden' name='Page' value='report'/>
            <input type='hidden' name='action' value='vehicle'/>
            <input type='hidden' name='section' value='vehicle'/>

            <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                <div class="row-fluid">	
                    <?
                    /* Get section Pages for current session */
                    $QuerySec = new studentSession();
                    $QuerySec->listAllSessionSection($session_id);
                    ?>
                    <div class='span4'>
                        <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Report Type</label>
                            <? if (!empty($vehicle_report_array)): ?>
                                <select name="v_report_type" id="vehicle" class="select2_category span11">
                                    <? foreach ($vehicle_report_array as $rp_key => $rp_val): ?>
                                        <option value='<?= $rp_key ?>' <?
                                        if ($v_report_type == $rp_key): echo "selected";
                                        endif;
                                        ?>><?= $rp_val ?></option>
                                <? endforeach; ?>
                                </select>
                            <? else: ?>
                                <input type='text' value='Sorry, No Vehicle Found.' disabled />
<? endif; ?>	
                        </div>
                    </div>
                    <div class='span8' style='margin-left: 0px;'>
                        <div class='span12' style='margin-left: 5px;'>
                            <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                            <div class='span3' style='margin-left: 0px;'>
                                <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                            </div>
                            <div class='span9'>
<? if (!empty($record)): ?>
                                    <a class='btn yellow medium span3' href="<?= make_admin_url('report', 'download', 'download&v_report_type=' . $v_report_type . '&tmp=vehicle') ?>"><i class="icon-download"></i> PDF</a>
                                    <a class='btn blue medium span3' href="<?= make_admin_url('report', 'excel', 'download&v_report_type=' . $v_report_type . '&tmp=vehicle') ?>"><i class="icon-download"></i> Excel</a>
<? endif; ?>
                            </div>
                        </div>	
                    </div>							
                </div>

                <div class="clearfix"></div>
            </div>

        </form>										
    </div>	


    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">					
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
<? if ($v_report_type == "vehicle_info"): ?>	
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $vehicle_report_array[$v_report_type] ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Model</th>
                                        <th style='text-align:center' class="hidden-480">Company</th>
                                        <th style='text-align:center' class="hidden-480">Vehicle Number</th>
                                        <th style='text-align:center' class="hidden-480 sorting_disabled">Color</th>
                                        <th style='text-align:center' class="hidden-480 sorting_disabled">Vehicle Type</th>
                                        <th style='text-align:center' class="hidden-480 sorting_disabled">Action</th>
                                    </tr>
                                </thead>
                                    <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $key => $object):
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $sr ?>.</td>
                                                <td><?= $object->model ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $object->make ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $object->vehicle_number ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $object->color ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $object->type ?></td>
                                                <td style='text-align:center' class="hidden-480">
                                                    <a class="btn mini red icn-only tooltips" title="" href="<?= make_admin_url('vehicles', 'view', 'view&id=' . $object->id) ?>" data-original-title="click here to view students">Students in this bus</a>
                                                    <a class="btn mini blue icn-only tooltips" title="" href="<?= make_admin_url('vehicles', 'expenses', 'expenses&vehicle_id=' . $object->id) ?>" data-original-title="click here to view expenses">Expenses</a>
                                                </td>												</td>													
                                            </tr>
                                            <?php
                                            $sr++;
                                        endforeach;
                                        ?>
                                    </tbody>
    <?php endif; ?>  
                            </table>

                        </div>
                    </div>
                </div>
<? elseif ($v_report_type == "vendor"): ?>	
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $vehicle_report_array[$v_report_type] ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Model</th>
                                        <th style='text-align:center' class="hidden-480">Company</th>
                                        <th style='text-align:center' class="hidden-480">Vehicle Number</th>
                                        <th style='text-align:center' class="hidden-480">Vendor OR Owned</th>
                                    </tr>
                                </thead>
                                    <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $key => $object):
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $sr ?>.</td>
                                                <td><?= $object->model ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $object->make ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $object->vehicle_number ?></td>
                                                <td style='text-align:center' class="hidden-480"><?
                                                    if ($object->vendor == ""): echo "Owned";
                                                    else: echo $object->vendor;
                                                    endif;
                                                    ?></td>
                                            </tr>
                                            <?php
                                            $sr++;
                                        endforeach;
                                        ?>
                                    </tbody>
    <?php endif; ?>  
                            </table>			
                        </div>
                    </div>
                </div>		
<? elseif ($v_report_type == "vendor_info"): ?>	
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $vehicle_report_array[$v_report_type] ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Model</th>
                                        <th style='text-align:center' class="hidden-480">Company</th>
                                        <th style='text-align:center' class="hidden-480">Vehicle Number</th>
                                        <th style='text-align:center' class="hidden-480">Vendor Information</th>
                                    </tr>
                                </thead>
                                    <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $key => $object):
                                            if ($object->vendor != 'Owned'):
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td><?= $sr ?>.</td>
                                                    <td><?= $object->model ?></td>
                                                    <td style='text-align:center' class="hidden-480"><?= $object->make ?></td>
                                                    <td style='text-align:center' class="hidden-480"><?= $object->vehicle_number ?></td>
                                                    <td style='text-align:center' class="hidden-480"><?= $object->vendor; ?></td>
                                                </tr>
                                                <?php
                                                $sr++;
                                            endif;

                                        endforeach;
                                        ?>
                                    </tbody>
    <?php endif; ?>  
                            </table>			
                        </div>
                    </div>
                </div>		
<? elseif ($v_report_type == "vendor_reg"): ?>	
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $vehicle_report_array[$v_report_type] ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Model</th>
                                        <th style='text-align:center' class="hidden-480">Company</th>
                                        <th style='text-align:center' class="hidden-480">Vehicle Number</th>
                                        <th style='text-align:center' class="hidden-480">Seating Capacity</th>
                                        <th style='text-align:center' class="hidden-480">Insurance</th>
                                    </tr>
                                </thead>
                                    <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
        <?php
        $sr = 1;
        foreach ($record as $key => $object):
            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $sr ?>.</td>
                                                <td><?= $object->model ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $object->make ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $object->vehicle_number ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $object->seating_capacity ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $object->insurance ?></td>
                                            </tr>
                                        <?php
                                        $sr++;
                                    endforeach;
                                    ?>
                                    </tbody>
                <?php endif; ?>  
                            </table>			
                        </div>
                    </div>
                </div>						
<? endif; ?>	
        </div>
    </div>

    <div class="clearfix"></div>
</div>
<script>
    /* Print Document */
    $("#print").live("click", function () {
        var report_type = '<?= $v_report_type ?>';
        if (report_type === '') {
            alert("Sorry, You can't print this document..!");
            return false;
        }
        var dataString = '&v_report_type=' + report_type;

        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_vehicle_report', 'ajax_vehicle_report&temp=report'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
</script>