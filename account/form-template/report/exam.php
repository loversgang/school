<!-- BEGIN PAGE CONTAINER-->
<!-- BEGIN PAGE CONTAINER-->
<? if ($ex_type == 'term'): ?>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Student Examination Reports
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Examination Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'exam', 'exam'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Exam Reports</div></div>
                </a> 
            </div>								
        </div>  			
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid">
            <form class="" action="<?php echo make_admin_url('report', 'exam', 'exam') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='exam'/>
                <input type='hidden' name='section' value='exam'/>
                <input type='hidden' name='ex_type' value='term'/>
                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                    <div class="row-fluid">	
                        <div class='span4'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                                <select class="select2_category span11 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                    <option>Select Section</option>
                                    <?php
                                    $current = '';
                                    $session_sr = 1;
                                    foreach ($session_array as $sess_key => $session):
                                        ?>
                                        <option value='<?= $session->id ?>' <?
                                        if ($session->id == $session_id) {
                                            echo 'selected';
                                        }
                                        ?> ><?= ucfirst($session->title) ?></option>
                                                <?
                                                $session_sr++;
                                            endforeach;
                                            ?>
                                </select>
                            </div>
                        </div>
                        <?
                        /* Get section Pages for current session */
                        $QuerySec = new studentSession();
                        $QuerySec->listAllSessionSection($session_id);

                        #get Session Info
                        $QueryS = new session();
                        $session = $QueryS->getRecord($session_id);
                        ?>
                        <? if ($QuerySec->GetNumRows() > 0): ?>
                            <div class='span4' style='margin-left: 5px;'>
                                <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                                    <div class='span4' style='margin-left: 5px;'>
                                        <label class="control-label">Select Section </label>
                                        <div class="controls">
                                            <select class="exam_section_filter span8" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                <?php
                                                $session_sc = 1;
                                                while ($sec = $QuerySec->GetObjectFromRecord()):
                                                    ?>
                                                    <option value='<?= $sec->section ?>' <?
                                                            if ($ct_sec == $sec->section): echo 'selected';
                                                            endif;
                                                            ?>><?= ucfirst($sec->section) ?></option>
                                                            <?
                                                            $session_sc++;
                                                        endwhile;
                                                        ?>
                                            </select>
                                        </div>
                                    </div>	
                                    <div class='span7' style='margin-left: 5px;' >
                                        <label class="control-label">Select Term Exam</label>
                                        <div class="controls">
                                                <? if (!empty($groups)): ?>
                                                <select class="span11" name='exam' id='exam'>
                                                            <?php foreach ($groups as $ex_key => $ex_val): ?>
                                                        <option value='<?= $ex_val->id ?>' <?
                                                if ($exam == $ex_val->id): echo 'selected';
                                                endif;
                                                ?>><?= ucfirst($ex_val->title) ?></option>
                                                <?php endforeach; ?>
                                                </select>
        <? else: ?>	
                                                <input type='text' class='span11' value='Sorry, No Exam Found.' disabled />	
                            <? endif; ?>
                                        </div>
                                    </div>									
                                </div>
                            </div>
    <? else: ?>
                            <div class='span4'>
                                <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                                    <label class="control-label">Select Section </label>
                                    <div class="controls">						
                                        <input type='text' value='Sorry, No Section Found.' disabled />	
                                    </div>
                                </div>
                            </div>		
    <? endif; ?>							
                        <div class='span4' style='margin-left: 0px;'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                                <div class='span5' style='margin-left: 0px;'>
                                    <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                                </div>
                                <div class='span7'>
    <? if (!empty($record)): ?>
                                        <a class='btn yellow medium span6' href="<?= make_admin_url('report', 'download', 'download&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&exam=' . $exam . '&ex_type=term&tmp=exam') ?>">Download</a>
                                        <!--<a class='btn blue medium span6' href="<?= make_admin_url('report', 'excel', 'excel&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&exam=' . $exam . '&tmp=exam') ?>"><i class="icon-download"></i> Excel</a> -->
    <? endif; ?>
                                </div>
                            </div>	
                        </div>							
                    </div>

                    <div class="clearfix"></div>
                </div>

            </form>										
        </div>	


        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
            <div class="span12">					
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Term Exam Marks Details</div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover sample_2" id="sample_2">
                                <thead>
                                    <tr>
                                        <th class="hidden-480" style='vertical-align:middle;'>Roll No.</th>
                                        <th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>Name</th>
                                        <th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>Father Name</th>
    <?
    if (!empty($exams)):
        foreach ($exams as $e => $v):
            ?>
                                                <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?= $v->subject ?><br/>
                                        <div class='span6' style='margin-left:0px;'>Total</div> <div class='span6' style='margin-left:0px;'>Obtain</div> 
                                        </th>
        <? endforeach; ?>
                                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>All Subjects<br/>
                                    <div class='span6' style='margin-left:0px;'>Total</div> <div class='span6' style='margin-left:0px;'>Obtain</div> 
                                    </th>	
                                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>Position<br/>
                                    <div class='span6' style='margin-left:0px;'>%Age</div> <div class='span6' style='margin-left:0px;'>Grade</div> 
                                    </th>													
                                <? endif; ?>

                                </tr>
                                </thead>
                                    <? if (!empty($record)): ?>
                                    <tbody>
        <?php
        $sr = 1;
        foreach ($record as $sk => $stu): $checked = '';
            ?>
                                            <tr class="odd gradeX">
                                                <td class="hidden-480 main_title"><?= $stu->roll_no ?></td>
                                                <td class="hidden-480 main_title"><?= $stu->first_name . " " . $stu->last_name ?></td>
                                                <td class='main_title'><?= ucfirst($stu->father_name) ?></td>
                                                <?
                                                if (!empty($exams)): $tm = '';
                                                    $om = '';
                                                    foreach ($exams as $ee => $vv):
                                                        #get Student result
                                                        $QueryRes = new examinationMarks();
                                                        $Result = $QueryRes->get_student_exam_marks($vv->id, $stu->id);
                                                        if (is_object($Result)):
                                                            ?> 
                                                            <td style='text-align:center' class="hidden-480"><?= $vv->maximum_marks ?></td>
                                                            <td style='text-align:center' class="hidden-480"><?= $Result->marks_obtained ?></td>
                                                            <?
                                                            $tm = $tm + $vv->maximum_marks;
                                                            $om = $om + $Result->marks_obtained;
                                                        else:
                                                            echo "<td></td><td></td>";
                                                        endif;
                                                    endforeach;
                                                endif;
                                                ?>
                                                <td style='text-align:center' class="hidden-480"><?= $tm ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $om ?></td>
                                                <?
                                                if (!empty($all_grade)): $grade = '';
                                                    foreach ($all_grade as $kg => $kv):
                                                        if (((($om / $tm) * 100) >= $kv->minimum) && ((($om / $tm) * 100) < $kv->maximum)): $grade = $kv->title;
                                                        endif;
                                                    endforeach;
                                                    ?>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= number_format(($om / $tm) * 100, 2) ?>%</td>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $grade ?></td>
            <? endif; ?>													
                                            </tr>
        <? endforeach; ?>									
                                    </tbody>
    <? endif; ?>
                            </table>

                        </div>
                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

    <!-- BEGIN PAGE CONTAINER-->
<? elseif ($ex_type == 'all'): ?>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Student Examination Reports
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Examination Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right">
    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'exam', 'exam'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Exam Reports</div></div>
                </a> 
            </div>								
        </div>  			
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid">
            <form class="" action="<?php echo make_admin_url('report', 'exam', 'exam') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='exam'/>
                <input type='hidden' name='section' value='exam'/>
                <input type='hidden' name='ex_type' value='all'/>
                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                    <div class="row-fluid">	
                        <div class='span4'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                                <select class="select2_category span12 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                    <option>Select Session</option>
                                    <?php
                                    $current = '';
                                    $session_sr = 1;
                                    foreach ($session_array as $sess_key => $session):
                                        ?>
                                        <option value='<?= $session->id ?>' <?
                                                if ($session->id == $session_id) {
                                                    echo 'selected';
                                                }
                                                ?> ><?= ucfirst($session->title) ?></option>
        <?
        $session_sr++;
    endforeach;
    ?>
                                </select>
                            </div>
                        </div>
                        <?
                        /* Get section Pages for current session */
                        $QuerySec = new studentSession();
                        $QuerySec->listAllSessionSection($session_id);

                        #get Session Info
                        $QueryS = new session();
                        $session = $QueryS->getRecord($session_id);
                        ?>
                                        <? if ($QuerySec->GetNumRows() > 0): ?>
                            <div class='span3' id='ajex_section'>
                                <div class='span11' style='margin-left: 5px;'>
                                    <label class="control-label">Select Section </label>
                                    <div class="controls">
                                        <select class="exam_section_filter span8" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                    <?php
                                                    $session_sc = 1;
                                                    while ($sec = $QuerySec->GetObjectFromRecord()):
                                                        ?>
                                                <option value='<?= $sec->section ?>' <?
                                            if ($ct_sec == $sec->section): echo 'selected';
                                            endif;
                                            ?>><?= ucfirst($sec->section) ?></option>
            <?
            $session_sc++;
        endwhile;
        ?>
                                        </select>
                                    </div>
                                </div>							
                            </div>

                            <div class='span4' style='margin-left: 0px;'>
                                <div class='span12' style='margin-left: 5px;'>
                                    <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                                    <div class='span6' style='margin-left: 0px;'>
                                        <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                                    </div>
                                    <div class='span6'>
        <? if (!empty($record)): ?>
                                            <a class='btn yellow medium span11' href="<?= make_admin_url('report', 'download', 'download&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&exam=' . $exam . '&tmp=exam&ex_type=all') ?>">Download</a>
                                            <!--<a class='btn blue medium span6' href="<?= make_admin_url('report', 'excel', 'excel&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&exam=' . $exam . '&tmp=exam') ?>"><i class="icon-download"></i> Excel</a> -->
        <? endif; ?>
                                    </div>
                                </div>	
                            </div>									
                        <? else: ?>
                            <div class='span3'>
                                <div class='span11' style='margin-left: 5px;' id='ajex_section'>
                                    <label class="control-label">Select Section </label>
                                    <div class="controls">						
                                        <input type='text' value='Sorry, No Section Found.' disabled />	
                                    </div>
                                </div>
                            </div>		
    <? endif; ?>	
                    </div>

                    <div class="clearfix"></div>
                </div>

            </form>										
        </div>	


        <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
            <div class="span12">					
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>All Terms Subject Wise Marks Details</div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover sample_2" id="sample_2">
                                <thead>
                                    <tr>
                                        <th class="hidden-480" style='vertical-align:middle;'>Roll No.</th>
                                        <th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>Name</th>
                                        <th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>Father Name</th>
    <?
    if (!empty($groups)):
        foreach ($groups as $e => $v):
            ?>
                                                <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?= $v->title ?><br/>
                                        <div class='span6' style='margin-left:0px;'>Total</div> <div class='span6' style='margin-left:0px;'>Obtain</div> 
                                        </th>
        <? endforeach; ?>
                                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>All Terms<br/>
                                    <div class='span6' style='margin-left:0px;'>Total</div> <div class='span6' style='margin-left:0px;'>Obtain</div> 
                                    </th>	
                                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>Position<br/>
                                    <div class='span6' style='margin-left:0px;'>%Age</div> <div class='span6' style='margin-left:0px;'>Grade</div> 
                                    </th>														
    <? endif; ?>
                                </tr>
                                </thead>
                                        <? if (!empty($record)): ?>
                                    <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($record as $sk => $stu): $checked = '';
                                                ?>
                                            <tr class="odd gradeX">
                                                <td class="hidden-480 main_title"><?= $stu->roll_no ?></td>
                                                <td class="hidden-480 main_title"><?= $stu->first_name . " " . $stu->last_name ?></td>
                                                <td class='main_title'><?= ucfirst($stu->father_name) ?></td>
            <?
            if (!empty($groups)): $tm = '';
                $om = '';
                foreach ($groups as $ee => $vv):
                    #get Student result
                    $QueryRes = new examinationMarks();
                    $Result = $QueryRes->get_student_exam_marks_in_multpale($vv->exam_id, $stu->id);
                    if (!empty($Result)):
                        ?> 
                                                            <td style='text-align:center' class="hidden-480"><?= $Result['total'] ?></td>
                                                            <td style='text-align:center' class="hidden-480"><?= $Result['obtain'] ?></td>
                        <?
                        $tm = $tm + $Result['total'];
                        $om = $om + $Result['obtain'];
                    else:
                        echo "<td></td><td></td>";
                    endif;
                endforeach;
            endif;
            ?>
                                                <td style='text-align:center' class="hidden-480"><?= $tm ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $om ?></td>
                                                <?
                                                if (!empty($all_grade)): $grade = '';
                                                    foreach ($all_grade as $kg => $kv):
                                                        if (((($om / $tm) * 100) >= $kv->minimum) && ((($om / $tm) * 100) < $kv->maximum)): $grade = $kv->title;
                                                        endif;
                                                    endforeach;
                                                    ?>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= number_format(($om / $tm) * 100, 2) ?>%</td>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $grade ?></td>
            <? endif; ?>					
                                            </tr>
        <? endforeach; ?>									
                                    </tbody>
    <? endif; ?>
                            </table>

                        </div>
                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <div class="clearfix"></div>
    </div>


    <!-- BEGIN PAGE CONTAINER-->
<? elseif ($ex_type == 'subject'): ?>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Student Examination Reports
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Examination Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right">
    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'exam', 'exam'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Exam Reports</div></div>
                </a> 
            </div>								
        </div>  			
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid">
            <form class="" action="<?php echo make_admin_url('report', 'exam', 'exam') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='exam'/>
                <input type='hidden' name='section' value='exam'/>
                <input type='hidden' name='ex_type' value='subject'/>
                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                    <div class="row-fluid">	
                        <div class='span3'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                                <select class="select2_category span12 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                    <option>Select Section</option>
                                            <?php
                                            $current = '';
                                            $session_sr = 1;
                                            foreach ($session_array as $sess_key => $session):
                                                ?>
                                        <option value='<?= $session->id ?>' <?
                                        if ($session->id == $session_id) {
                                            echo 'selected';
                                        }
                                                ?> ><?= ucfirst($session->title) ?></option>
                            <?
                            $session_sr++;
                        endforeach;
                        ?>
                                </select>
                            </div>
                        </div>
                        <?
                        /* Get section Pages for current session */
                        $QuerySec = new studentSession();
                        $QuerySec->listAllSessionSection($session_id);

                        #get Session Info
                        $QueryS = new session();
                        $session = $QueryS->getRecord($session_id);
                        ?>
                                            <? if ($QuerySec->GetNumRows() > 0): ?>
                            <div class='span6' style='margin-left: 5px;'>
                                <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                                    <div class='span3' style='margin-left: 5px;'>
                                        <label class="control-label">Select Section </label>
                                        <div class="controls">
                                            <select class="exam_section_filter span8" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
        <?php
        $session_sc = 1;
        while ($sec = $QuerySec->GetObjectFromRecord()):
            ?>
                                                    <option value='<?= $sec->section ?>' <?
                            if ($ct_sec == $sec->section): echo 'selected';
                            endif;
                            ?>><?= ucfirst($sec->section) ?></option>
                                                    <?
                                                    $session_sc++;
                                                endwhile;
                                                ?>
                                            </select>
                                        </div>
                                    </div>	
        <? if (!empty($groups)): ?>
                                        <div class='span5' style='margin-left: 5px;' >
                                            <label class="control-label">Select Term Exam</label>
                                            <div class="controls">
                                                <select class="span11 exam_subject_filter" name='exam' id='exam'>
                                                    <?php foreach ($groups as $ex_key => $ex_val): ?>
                                                        <option value='<?= $ex_val->id ?>' <?
                                        if ($exam == $ex_val->id): echo 'selected';
                                        endif;
                                                        ?>><?= ucfirst($ex_val->title) ?></option>
                                                            <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>	
                                        <div class='span4' style='margin-left: 5px;' id='subject_list'>
                                            <label class="control-label">Select Subject</label>
                                            <div class="controls">			
                                        <? if (!empty($AllSubject)): ?>
                                                    <select class="span11" name='subject' id='subject'>
                <?php foreach ($AllSubject as $s_key => $s_val): ?>
                                                            <option value='<?= $s_val->exam ?>' <?
                    if ($subject == $s_val->exam): echo "selected";
                    endif;
                    ?>><?= ucfirst($s_val->name) ?></option>
                                            <?php endforeach; ?>
                                                    </select>	
                                <? else: ?>	
                                                    <input type='text' class='span11'  placeholder='Sorry, No Subject Found.' disabled />														
            <? endif; ?>
                                            </div>
                                        </div>
        <? else: ?>	
                                        <div class='span5' style='margin-left: 5px;' >
                                            <label class="control-label">Select Term Exam</label>
                                            <div class="controls">									
                                                <input type='text' class='span11' id='exam' placeholder='Sorry, No Exam Found.' disabled />	
                                            </div>
                                        </div>
        <? endif; ?>									
                                </div>
                            </div>
    <? else: ?>
                            <div class='span3'>
                                <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                                    <label class="control-label">Select Section </label>
                                    <div class="controls">						
                                        <input type='text' value='Sorry, No Section Found.' disabled />	
                                    </div>
                                </div>
                            </div>		
    <? endif; ?>							
                        <div class='span3' style='margin-left: 0px;'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                                <div class='span6' style='margin-left: 0px;'>
                                    <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                                </div>
                                <div class='span6'>
    <? if (!empty($record)): ?>
                                        <a class='btn yellow medium span12' href="<?= make_admin_url('report', 'download', 'download&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&exam=' . $exam . '&subject=' . $subject . '&ex_type=subject&tmp=exam') ?>">Download</a>
                                        <!--<a class='btn blue medium span6' href="<?= make_admin_url('report', 'excel', 'excel&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&exam=' . $exam . '&tmp=exam') ?>"><i class="icon-download"></i> Excel</a> -->
        <? endif; ?>
                                </div>
                            </div>	
                        </div>							
                    </div>

                    <div class="clearfix"></div>
                </div>

            </form>										
        </div>	


        <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
            <div class="span12">					
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Term Subjects Wise Marks Details</div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover sample_2" id="sample_2">
                                <thead>
                                    <tr>
                                        <th class="hidden-480" style='vertical-align:middle;'>Roll No.</th>
                                        <th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>Name</th>
                                        <th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>Father Name</th>
                                <?
                                if (!empty($exams)):
                                    foreach ($exams as $e => $v):
                                        if ($v->id == $subject):
                                            ?>
                                                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?= $v->subject ?><br/>
                                            <div class='span6' style='margin-left:0px;'>Total</div> <div class='span6' style='margin-left:0px;'>Obtain</div> 
                                            </th>
                                            <?
                                        endif;
                                    endforeach;
                                    ?>
                                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>All Subjects<br/>
                                    <div class='span6' style='margin-left:0px;'>Total</div> <div class='span6' style='margin-left:0px;'>Obtain</div> 
                                    </th>
                                        <? if (!empty($all_grade)): $grade = ''; ?>													
                                        <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>Position<br/>
                                        <div class='span6' style='margin-left:0px;'>%Age</div> <div class='span6' style='margin-left:0px;'>Grade</div> 
                                        </th>													
            <?
        endif;
    endif;
    ?>

                                </tr>
                                </thead>
                                        <? if (!empty($record)): ?>
                                    <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($record as $sk => $stu): $checked = '';
                                                ?>
                                            <tr class="odd gradeX">
                                                <td class="hidden-480 main_title"><?= $stu->roll_no ?></td>
                                                <td class="hidden-480 main_title"><?= $stu->first_name . " " . $stu->last_name ?></td>
                                                <td class='main_title'><?= ucfirst($stu->father_name) ?></td>
                                                <?
                                                if (!empty($exams)): $tm = '';
                                                    $om = '';
                                                    foreach ($exams as $ee => $vv):
                                                        if ($vv->id == $subject):
                                                            #get Student result
                                                            $QueryRes = new examinationMarks();
                                                            $Result = $QueryRes->get_student_exam_marks($vv->id, $stu->id);
                                                            if (is_object($Result)):
                                                                ?> 
                                                                <td style='text-align:center' class="hidden-480"><?= $vv->maximum_marks ?></td>
                                                                <td style='text-align:center' class="hidden-480"><?= $Result->marks_obtained ?></td>
                                                                <?
                                                                $tm = $tm + $vv->maximum_marks;
                                                                $om = $om + $Result->marks_obtained;
                                                            else:
                                                                echo "<td></td><td></td>";
                                                            endif;
                                                        endif;
                                                    endforeach;
                                                endif;
                                                ?>
                                                <td style='text-align:center' class="hidden-480"><?= $tm ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $om ?></td>
            <?
            if (!empty($all_grade)): $grade = '';
                foreach ($all_grade as $kg => $kv):
                    if (((($om / $tm) * 100) >= $kv->minimum) && ((($om / $tm) * 100) < $kv->maximum)): $grade = $kv->title;
                    endif;
                endforeach;
                ?>												
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= number_format(($om / $tm) * 100, 2) ?>%</td>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $grade ?></td>												
            <? endif; ?>
                                            </tr>
        <? endforeach; ?>									
                                    </tbody>
    <? endif; ?>
                            </table>

                        </div>
                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <div class="clearfix"></div>
    </div>



    <!-- BEGIN PAGE CONTAINER-->
<? elseif ($ex_type == 'term_subject'): ?>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Student Examination Reports
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Examination Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right">
    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'exam', 'exam'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Exam Reports</div></div>
                </a> 
            </div>								
        </div>  			
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid">
            <form class="" action="<?php echo make_admin_url('report', 'exam', 'exam') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='exam'/>
                <input type='hidden' name='section' value='exam'/>
                <input type='hidden' name='ex_type' value='term_subject'/>
                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                    <div class="row-fluid">	
                        <div class='span4'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                                <select class="select2_category span11 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                    <option value=''>Select Session</option>
    <?php
    $current = '';
    $session_sr = 1;
    foreach ($session_array as $sess_key => $session):
        ?>
                                        <option value='<?= $session->id ?>' <?
                            if ($session->id == $session_id) {
                                echo 'selected';
                            }
                            ?> ><?= ucfirst($session->title) ?></option>
                            <?
                            $session_sr++;
                        endforeach;
                        ?>
                                </select>
                            </div>
                        </div>
                                            <?
                                            /* Get section Pages for current session */
                                            $QuerySec = new studentSession();
                                            $QuerySec->listAllSessionSection($session_id);

                                            #get Session Info
                                            $QueryS = new session();
                                            $session = $QueryS->getRecord($session_id);
                                            ?>
                                                    <? if ($QuerySec->GetNumRows() > 0): ?>
                            <div class='span4' style='margin-left: 5px;'>
                                <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                                    <div class='span4' style='margin-left: 5px;'>
                                        <label class="control-label">Select Section </label>
                                        <div class="controls">
                                            <select class="exam_section_filter span8" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                            <?php
                                            $session_sc = 1;
                                            while ($sec = $QuerySec->GetObjectFromRecord()):
                                                ?>
                                                    <option value='<?= $sec->section ?>' <?
                                                            if ($ct_sec == $sec->section): echo 'selected';
                                                            endif;
                                                            ?>><?= ucfirst($sec->section) ?></option>
                                                <?
                                                $session_sc++;
                                            endwhile;
                                            ?>
                                            </select>
                                        </div>
                                    </div>	
                                    <div class='span7' style='margin-left: 5px;' >
                                        <label class="control-label">Select Subject</label>
                                        <div class="controls">
        <? if (!empty($sessionSubjects)): ?>
                                                <select class="span11" name='subject' id='exam'>
            <?php foreach ($sessionSubjects as $ex_key => $ex_val): ?>
                                                        <option value='<?= $ex_val->id ?>' <?
                if ($subject == $ex_val->id): echo 'selected';
                endif;
                ?>><?= ucfirst($ex_val->name) ?></option>
            <?php endforeach; ?>
                                                </select>
        <? else: ?>	
                                                <input type='text' class='span11' value='Sorry, No Subject Found.' disabled />	
        <? endif; ?>
                                        </div>
                                    </div>									
                                </div>
                            </div>
                                    <? else: ?>
                            <div class='span4'>
                                <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                                    <label class="control-label">Select Section </label>
                                    <div class="controls">						
                                        <input type='text' value='Sorry, No Section Found.' disabled />	
                                    </div>
                                </div>
                            </div>		
    <? endif; ?>							
                        <div class='span4' style='margin-left: 0px;'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                                <div class='span5' style='margin-left: 0px;'>
                                    <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                                </div>
                                <div class='span7'>
        <? if (!empty($record['record'])): ?>
                                        <a class='btn yellow medium span6' href="<?= make_admin_url('report', 'download', 'download&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&subject=' . $subject . '&exam=' . $exam . '&ex_type=term_subject&tmp=exam') ?>">Download</a>
                                        <!--<a class='btn blue medium span6' href="<?= make_admin_url('report', 'excel', 'excel&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&exam=' . $exam . '&tmp=exam') ?>"><i class="icon-download"></i> Excel</a> -->
    <? endif; ?>
                                </div>
                            </div>	
                        </div>							
                    </div>

                    <div class="clearfix"></div>
                </div>

            </form>										
        </div>	


        <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
            <div class="span12">					
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>All Terms Subject Wise Marks Details</div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover sample_2" id="sample_2">
                                <thead>
                                    <tr>
                                        <th class="hidden-480" style='vertical-align:middle;'>Roll No.</th>
                                        <th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>Name</th>
                                        <th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>Father Name</th>
                                    <?
                                    if (!empty($record['terms'])):
                                        foreach ($record['terms'] as $e => $v):
                                            ?>
                                                <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?= $v ?> (<?= $subject_detail->name ?>)
                                        <div class='span6' style='margin-left:0px;'>Total</div> <div class='span6' style='margin-left:0px;'>Obtain</div> 
                                        </th>
                                            <? endforeach; ?>
                                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>All Term<br/>
                                    <div class='span6' style='margin-left:0px;'>Total</div> <div class='span6' style='margin-left:0px;'>Obtain</div> 
                                    </th>	
                                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>Position<br/>
                                    <div class='span6' style='margin-left:0px;'>%Age</div> <div class='span6' style='margin-left:0px;'>Grade</div> 
                                    </th>													
                                        <? endif; ?>

                                </tr>
                                </thead>
                                        <? if (!empty($record['record'])): ?>
                                    <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($record['record'] as $sk => $stu): $checked = '';
                                                ?>																		
                                            <tr class="odd gradeX">
                                                <td class="hidden-480 main_title"><?= $stu->roll_no ?></td>
                                                <td class="hidden-480 main_title"><?= $stu->first_name . " " . $stu->last_name ?></td>
                                                <td class='main_title'><?= ucfirst($stu->father_name) ?></td>
            <?
            if (!empty($record['terms'])): $tm = '';
                $om = '';
                foreach ($record['terms'] as $ee => $vv):
                    #get Student result
                    $QueryRes = new examinationMarks();
                    $Result = $QueryRes->getStudentResult($ee, $stu->id);
                    if (is_object($Result)):
                        ?> 
                                                            <td style='text-align:center' class="hidden-480"><?= $Result->maximum_marks ?></td>
                                                            <td style='text-align:center' class="hidden-480"><?= $Result->marks_obtained ?></td>
                                                            <?
                                                            $tm = $tm + $Result->maximum_marks;
                                                            $om = $om + $Result->marks_obtained;
                                                        else:
                                                            echo "<td></td><td></td>";
                                                        endif;
                                                    endforeach;
                                                endif;
                                                ?>
                                                <td style='text-align:center' class="hidden-480"><?= $tm ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= $om ?></td>
                                                <td style='text-align:center' class="hidden-480"><?= number_format($om / $tm * 100, 2) ?>%</td>
            <?
            if (!empty($all_grade)): $grade = '';
                foreach ($all_grade as $kg => $kv):
                    if (((($om / $tm) * 100) >= $kv->minimum) && ((($om / $tm) * 100) < $kv->maximum)): $grade = $kv->title;
                    endif;
                endforeach;
                ?>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $grade ?></td>
            <? endif; ?>												

                                            </tr>
        <? endforeach; ?>									
                                    </tbody>
    <? endif; ?>
                            </table>

                        </div>
                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <div class="clearfix"></div>
    </div>


<? else: ?>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Student Examination Reports
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Examination Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right">
    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
        </div>  			
        <!-- END PAGE HEADER-->

        <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

        <div class="tiles pull-left row-fluid">
            <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;padding-left:0px;'>
                <br/>
                <div class="row-fluid" id='custom_dashboard'>
                    <div class='span3'>
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'exam', 'exam&ex_type=term') ?>">
                                <img src="assets/img/icon/list.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'exam', 'exam&ex_type=term') ?>" class='title'>Term Exam <br/>Marks Details</a>
                        </div>
                    </div>							
                    <div class='span3'>
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'exam', 'exam&ex_type=subject') ?>">
                                <img src="assets/img/icon/exams-icon.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'exam', 'exam&ex_type=subject') ?>" class='title'>Subject Wise <br/>Marks Details</a>
                        </div>
                    </div>	
                    <div class='span3'>
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'exam', 'exam&ex_type=all') ?>">
                                <img src="assets/img/icon/sales-by-employee.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'exam', 'exam&ex_type=all') ?>" class='title'>All Terms Exam<br/>Marks Details</a>
                        </div>
                    </div>	
                    <div class='span3'>
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'exam', 'exam&ex_type=term_subject') ?>">
                                <img src="assets/img/icon/icon-mock-tests.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'exam', 'exam&ex_type=term_subject') ?>" class='title'>All Terms Subject<br/>Wise Marks Details</a>
                        </div>
                    </div>							
                </div>
                <div class="clearfix"></div>

            </div>	
        </div>        

        <div class="clearfix"></div>
    </div>

<? endif; ?>
<script type="text/javascript">
    $(".session_filter,.exam_section_filter").live("change", function () {
        var session_id = $("#session_id").val();
        var ct_sec = $("#ct_sec").val();

        if (session_id.length > 0) {
            var id = session_id;
            $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id + '&ct_sec=' + ct_sec;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section_exam_subject&temp=report'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_section").html(data);
                }
            });
        }
        else {
            return false;
        }
    });

    $(".exam_subject_filter").live("change", function () {
        var session_id = $("#session_id").val();
        var ct_sec = $("#ct_sec").val();
        var exam = $("#exam").val();

        if (session_id.length > 0) {
            var id = session_id;
            $("#subject_list").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id + '&ct_sec=' + ct_sec + '&exam=' + exam;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_exam_subject_only&temp=report'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#subject_list").html(data);
                }
            });
        }
        else {
            return false;
        }
    });

    /* Print Document */
    $("#print").live("click", function () {
        var session_id = '<?= $session_id ?>';
        var ct_sec = '<?= $ct_sec ?>';
        var exam = '<?= $exam ?>';
        var subject = '<?= $subject ?>';
        var report_type = '<?= $ex_type ?>';
        if (report_type === '') {
            alert("Sorry, You can't print this document..!");
            return false;
        }
        var dataString = '&session_id=' + session_id + '&ct_sec=' + ct_sec + '&exam=' + exam + '&subject=' + subject + '&ex_type=' + report_type;

        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_exam_all', 'ajax_section_exam_all&temp=report'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
</script>