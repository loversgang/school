<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');

if($_POST):
isset($_POST['id'])?$id=$_POST['id']:$id='';
isset($_POST['ct_sec'])?$ct_sec=$_POST['ct_sec']:$ct_sec='A';
isset($_POST['exam'])?$exam=$_POST['exam']:$exam='';
	
		/* Get session Pages*/
		 $QuerySec=new studentSession();
         $QuerySec->listAllSessionSection($id);
		 
		#get Session Info
		$QueryS=new session();
		$session=$QueryS->getRecord($id);	

		#Get exam List
		$QueryObjExam=new examinationGroup();
        $GroupExams=$QueryObjExam->getTermExamSessionSection($id,$ct_sec);		

		if(empty($exam) && !empty($GroupExams)):
				$exam=$GroupExams['0']->id;
		endif;
		
		if(!empty($exam)): 
			$exam_detail=get_object('student_examination_group',$exam);
			
			#get Session Info
			$QueryExsub=new examinationGroup();
			$AllSubject=$QueryExsub->getSubjectByExams($exam_detail->exam_id);				
		endif;

		
	
		
		?>	


		<label class="control-label">Select Subject</label>
		<div class="controls">			
		<? if(!empty($AllSubject)): ?>
				<select class="span11" name='subject' id='subject'>
				<?php foreach($AllSubject as $s_key=>$s_val): ?>
						<option value='<?=$s_val->exam?>'><?=ucfirst($s_val->name)?></option>
				<?php endforeach;?>
				</select>	
		<? else: ?>	
				<input type='text' class='span11'  placeholder='Sorry, No Subject Found.' disabled />														
		<? endif;?>
		</div>
									
<? endif;?>									