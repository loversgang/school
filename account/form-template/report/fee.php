<? if ($f_type == 'month_wise'): ?>
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Manage Student Fee Reports
                </h3>
                <ul class="breadcrumb hidden-print">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Fee Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right hidden-print">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'fee', 'fee'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Fee Reports</div></div>
                </a> 
            </div>								
        </div>            

        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid hidden-print">
            <form class="" action="<?php echo make_admin_url('report', 'fee', 'fee') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='fee'/>
                <input type='hidden' name='section' value='fee'/>
                <input type='hidden' name='f_type' value='month_wise'/>

                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>
                    <div class="row-fluid">
                        <div class='span2'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                            <select style="width:100%" class="select2_category session_filter_more" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                                <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                    <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class='span3'>
                            <div id="session_list_details"></div>
                        </div>
                        <div class='span2'>
                            <div style='margin-left: 5px; padding-right:20px' id='ajex_section'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Section</label>
                                <?php
                                $QuerySec = new studentSession();
                                $QuerySec->listAllSessionSection($session_id);
                                if ($QuerySec->GetNumRows()):
                                    ?>
                                    <select style="width:100%" class="select2_category" data-placeholder="Select Session Students" name='ct_sec'>
                                        <option value="0">ALL</option>
                                        <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                            <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                        <? endwhile; ?>				
                                    </select>
                                <? else: ?>
                                    <input style="width:100%" type='text' value='Sorry, No Section Found.' disabled />
                                <? endif; ?>	
                            </div>
                        </div>
                        <div class='span3'>
                            <div id="get_session_intervals"></div>
                        </div>	
                        <div class="span2">
                            <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                            <input type='submit' name='go' value='View Result' class='btn green medium span10'/>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>										
        </div>	
        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">

            <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-list"></i>Class Wise Fee Detail</div>								
                </div>
                <div class="portlet-body">
                    <div class="clearfix">
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Reg ID</th>
                                    <th>Roll No.</th>
                                    <th>Current Session</th>
                                    <th>Name</th>
                                    <th>Father Name</th>
                                    <th class="hidden-480" >Concession Amount</th>
                                    <th class="hidden-480" >Vehicles Fee</th>
                                    <th class="hidden-480" >Previous Amount</th>
                                    <th class="hidden-480" >Receivable Amount</th>
                                    <th class="hidden-480" >Received Amount</th>	
                                    <th class="hidden-480" >Balance Amount</th>	
                                </tr>
                            </thead>
                            <? if (!empty($record) && !empty($interval)): $sum = ''; ?>
                                <tbody>
                                    <?
                                    foreach ($record as $key => $stu):
                                        $CurrObj = new studentSession();
                                        $current_session = $CurrObj->getStudentCurrentSession($school->id, $stu->id);
                                        #Get interval detail
                                        $SessIntOb = new studentSessionInterval();
                                        $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sec, $stu->id, $interval_id);
                                        //echo '<pre>'; print_r($student_interval_detail); exit;
                                        #get Pending Fee for previous month
                                        $SessObPre = new studentSessionInterval();
                                        $pending_fee = $SessObPre->getPreviousBalanceFee($session_id, $ct_sec, $stu->id, $interval_id);
                                        ?>
                                        <tr class="odd gradeX">
                                            <td ><?= $stu->reg_id ?></td>
                                            <td ><?= $stu->roll_no ?></td>
                                            <td ><?= $current_session ?></td>
                                            <td><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                            <td ><?= $stu->father_name ?></td>
                                            <td class="hidden-480"><?
                                                if (is_object($student_interval_detail)): echo CURRENCY_SYMBOL . " " . number_format($student_interval_detail->concession, 2);
                                                endif;
                                                ?></td>

                                            <td class="hidden-480"><?
                                                if (is_object($student_interval_detail)): echo CURRENCY_SYMBOL . " " . number_format($student_interval_detail->vehicle_fee, 2);
                                                endif;
                                                ?></td>

                                            <td class="hidden-480"><?
                                                if (isset($pending_fee) && !empty($pending_fee)): echo CURRENCY_SYMBOL . " " . number_format($pending_fee, 2);
                                                else: echo CURRENCY_SYMBOL . " " . number_format('0', 2);
                                                endif;
                                                ?></td>
                                            <td class="hidden-480"><?
                                                if (is_object($student_interval_detail)): echo CURRENCY_SYMBOL . " " . number_format($student_interval_detail->total, 2);
                                                endif;
                                                ?></td>
                                            <td class="hidden-480"><?
                                                if (is_object($student_interval_detail)): echo CURRENCY_SYMBOL . " " . number_format($student_interval_detail->paid, 2);
                                                endif;
                                                ?></td>
                                            <td class="hidden-480"><?
                                                if (is_object($student_interval_detail)): echo CURRENCY_SYMBOL . " " . number_format($pending_fee + ($student_interval_detail->total - $student_interval_detail->paid), 2);
                                                endif;
                                                ?></td>
                                        </tr>

                                    <? endforeach; ?>	

                                </tbody>
                            <?php endif; ?>  
                        </table>			
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="clearfix"></div>
    </div>


<? elseif ($f_type == 'head_wise'): ?>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Manage Student Fee Reports
                </h3>
                <ul class="breadcrumb hidden-print">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Fee Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right hidden-print">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'fee', 'fee'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Fee Reports</div></div>
                </a> 
            </div>								
        </div>            

        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid hidden-print">
            <form class="" action="<?php echo make_admin_url('report', 'fee', 'fee') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='fee'/>
                <input type='hidden' name='section' value='fee'/>
                <input type='hidden' name='f_type' value='head_wise'/>

                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                    <div class="row-fluid">
                        <div class='span3'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                            <select style="width:100%" class="select2_category session_filter_more" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                                <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                    <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class='span4'>
                            <div id="session_list_details"></div>
                        </div>
                        <div class='span3'>
                            <div style='margin-left: 5px; padding-right:20px' id='ajex_section'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Section</label>
                                <?php
                                $QuerySec = new studentSession();
                                $QuerySec->listAllSessionSection($session_id);
                                if ($QuerySec->GetNumRows()):
                                    ?>
                                    <select style="width:100%" class="select2_category" data-placeholder="Select Session Students" name='ct_sec'>
                                        <option value="0">ALL</option>
                                        <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                            <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                        <? endwhile; ?>				
                                    </select>
                                <? else: ?>
                                    <input style="width:100%" type='text' value='Sorry, No Section Found.' disabled />
                                <? endif; ?>	
                            </div>
                        </div>
                        <div class="span2">
                            <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                            <input type='submit' name='go' value='View Result' class='btn green medium span10'/>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>

            </form>										
        </div>	


        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">

            <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-list"></i>Head Wise Expected Collection Details</div>								
                </div>
                <div class="portlet-body">
                    <div class="clearfix">
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Reg ID</th>
                                    <th>Name</th>
                                    <?
                                    if (!empty($Sesspay_heads)):
                                        foreach ($Sesspay_heads as $s_key => $s_value):
                                            if ($interval_detail->interval_position == '1'):
                                                ?>
                                                <th class="hidden-480" style='text-align:center;'><?= $s_value['title'] ?></th>
                                                <?
                                            else:
                                                if ($s_value['type'] == 'Regular'):
                                                    ?>
                                                    <th class="hidden-480" style='text-align:center;'><?= $s_value['title'] ?></th>	
                                                    <?
                                                endif;
                                            endif;
                                        endforeach;
                                        ?>
                                        <th class="hidden-480" style='text-align:right;'>Total Amount</th>
                                    <? endif; ?>
                                </tr>
                            </thead>
                            <? if (!empty($record) && !empty($interval)): $sum = ''; ?>
                                <tbody>													
                                    <? foreach ($record as $key => $stu): ?>
                                        <tr class="odd gradeX">
                                            <td ><?= $stu->reg_id ?></td>
                                            <td><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                            <?
                                            if (!empty($Sesspay_heads)): $Head_total = '';
                                                foreach ($Sesspay_heads as $s_kkey => $s_vvalue):
                                                    #Get Head fee detail
                                                    $StuHeadObj = new session_student_fee();
                                                    $HeadAmount = $StuHeadObj->getSingleStudentFeeHeadValue($session_id, $stu->id, $s_vvalue['fee_id']);


                                                    if ($interval_detail->interval_position == '1'):
                                                        $Head_total = $Head_total + $HeadAmount;
                                                        ?>
                                                        <td class="hidden-480" style='text-align:center;'><?= number_format($HeadAmount, 2) ?></td>
                                                        <?
                                                    else:
                                                        if ($s_vvalue['type'] == 'Regular'):
                                                            $Head_total = $Head_total + $HeadAmount;
                                                            ?>
                                                            <td class="hidden-480" style='text-align:center;'><?= number_format($HeadAmount, 2) ?></td>
                                                            <?
                                                        endif;
                                                    endif;
                                                endforeach;
                                                ?>														
                                                <td class="hidden-480" style='text-align:right;'><?= number_format($Head_total, 2); ?></td>
                                            <? endif; ?>
                                        </tr>

                                    <? endforeach; ?>	

                                </tbody>
                            <?php endif; ?>  
                        </table>			
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="clearfix"></div>
    </div>
<? elseif ($f_type == 'head_conc'): ?>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Manage Student Fee Reports
                </h3>
                <ul class="breadcrumb hidden-print">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Fee Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right hidden-print">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'fee', 'fee'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Fee Reports</div></div>
                </a> 
            </div>								
        </div>            

        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid hidden-print">
            <form class="" action="<?php echo make_admin_url('report', 'fee', 'fee') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='fee'/>
                <input type='hidden' name='section' value='fee'/>
                <input type='hidden' name='f_type' value='head_conc'/>

                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                    <div class="row-fluid">
                        <div class='span3'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                            <select style="width:100%" class="select2_category session_filter_more" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                                <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                    <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class='span4'>
                            <div id="session_list_details"></div>
                        </div>
                        <div class='span3'>
                            <div style='margin-left: 5px; padding-right:20px' id='ajex_section'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Section</label>
                                <?php
                                $QuerySec = new studentSession();
                                $QuerySec->listAllSessionSection($session_id);
                                if ($QuerySec->GetNumRows()):
                                    ?>
                                    <select style="width:100%" class="select2_category" data-placeholder="Select Session Students" name='ct_sec'>
                                        <option value="0">ALL</option>
                                        <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                            <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                        <? endwhile; ?>				
                                    </select>
                                <? else: ?>
                                    <input style="width:100%" type='text' value='Sorry, No Section Found.' disabled />
                                <? endif; ?>	
                            </div>
                        </div>
                        <div class="span2">
                            <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                            <input type='submit' name='go' value='View Result' class='btn green medium span10'/>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>

            </form>										
        </div>	


        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">

            <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-list"></i>Head Wise Concession Details</div>								
                </div>
                <div class="portlet-body">
                    <div class="clearfix">
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th>Head Name</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?
                                if (!empty($conc_heads)): $sr = '1';
                                    foreach ($conc_heads as $s_key => $s_value):
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?= $sr ?>.</td>
                                            <td><?= $s_key ?></td>
                                            <td><?
                                                if (array_key_exists($s_key, $record)): echo number_format($record[$s_key], 2);
                                                else: echo number_format($s_value, 2);
                                                endif;
                                                ?></td>													
                                        </tr>	
                                        <?
                                        $sr++;
                                    endforeach;
                                    ?>
                                <? endif; ?>																	
                            </tbody>
                        </table>			
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="clearfix"></div>
    </div>
<? elseif ($f_type == 'class_due'): ?>
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Manage Student Fee Reports
                </h3>
                <ul class="breadcrumb hidden-print">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Fee Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right hidden-print">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'fee', 'fee'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Fee Reports</div></div>
                </a> 
            </div>								
        </div>            

        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid hidden-print">
            <form class="" action="<?php echo make_admin_url('report', 'fee', 'fee') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='fee'/>
                <input type='hidden' name='section' value='fee'/>
                <input type='hidden' name='f_type' value='class_due'/>

                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                    <div class="row-fluid">
                        <div class='span3'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                            <select style="width:100%" class="select2_category session_filter_more" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                                <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                    <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class='span4'>
                            <div id="session_list_details"></div>
                        </div>
                        <div class='span3'>
                            <div style='margin-left: 5px; padding-right:20px' id='ajex_section'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Section</label>
                                <?php
                                $QuerySec = new studentSession();
                                $QuerySec->listAllSessionSection($session_id);
                                if ($QuerySec->GetNumRows()):
                                    ?>
                                    <select style="width:100%" class="select2_category" data-placeholder="Select Session Students" name='ct_sec'>
                                        <option value="0">ALL</option>
                                        <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                            <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                        <? endwhile; ?>				
                                    </select>
                                <? else: ?>
                                    <input style="width:100%" type='text' value='Sorry, No Section Found.' disabled />
                                <? endif; ?>	
                            </div>
                        </div>
                        <div class="span2">
                            <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                            <input type='submit' name='go' value='View Result' class='btn green medium span10'/>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>			
            </form>										
        </div>	


        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
            <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-list"></i>Class Wise Fees Due List With Contact Details</div>								
                </div>
                <div class="portlet-body">
                    <div class="clearfix">
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Reg ID</th>
                                    <th>Current Session</th>
                                    <th>Section</th>
                                    <th>Name</th>
                                    <th>Father Name</th>
                                    <th>Contact No.</th>
                                    <th>Receivable Amount</th>
                                    <th>Received Amount</th>
                                    <th style='text-align:right;'>Balance</th>
                                </tr>
                            </thead>
                            <? if (!empty($record)): $sum = ''; ?>
                                <tbody>							
                                    <?
                                    foreach ($record as $key => $stu):
                                        $CurrObj = new studentSession();
                                        $current_session = $CurrObj->getStudentCurrentSession($school->id, $stu->id);
                                        ?>
                                        <tr class="odd gradeX">
                                            <td ><?= $stu->reg_id ?></td>
                                            <td ><?= $current_session ?></td>
                                            <td ><?= $stu->section ?></td>
                                            <td><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                            <td><?= $stu->father_name ?></td>
                                            <td><?= $stu->family_phone ?></td>
                                            <td><?= number_format($stu->total, 2) ?></td>
                                            <td><?= number_format($stu->paid, 2) ?></td>
                                            <td style='text-align:right;'><?= number_format($stu->total - $stu->paid, 2) ?></td>
                                        </tr>
        <? endforeach; ?>	

                                </tbody>
    <?php endif; ?>  
                        </table>			
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="clearfix"></div>
    </div>



<? else: ?>	
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Manage Student Fee Reports
                </h3>
                <ul class="breadcrumb hidden-print">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Fee Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right hidden-print">
    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
        </div>  

        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="tiles pull-left row-fluid hidden-print">
            <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;padding-left:0px;'>
                <br/>
                <div class="row-fluid" id='custom_dashboard'>
                    <div class='span3'>
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'fee', 'fee&f_type=month_wise') ?>">
                                <img src="assets/img/icon/fee-3.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'fee', 'fee&f_type=month_wise') ?>" class='title'>Class Wise <br/>Monthly Detail</a>
                        </div>
                    </div>	
                    <div class='span3'>	
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'fee', 'fee&f_type=head_wise') ?>">
                                <img src="assets/img/icon/expense-2.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'fee', 'fee&f_type=head_wise') ?>" class='title'>Head Wise Expected <br/>Collection Details</a>
                        </div>
                    </div>
                    <div class='span3'>	
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'fee', 'fee&f_type=head_conc') ?>">
                                <img src="assets/img/icon/sublect-wise.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'fee', 'fee&f_type=head_conc') ?>" class='title'>Head Wise <br/>Concession Details</a>
                        </div>
                    </div>	
                    <div class='span3'>
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'fee', 'fee&f_type=class_due') ?>">
                                <img src="assets/img/icon/bar-chart-icon1.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'fee', 'fee&f_type=class_due') ?>" class='title'>Class Wise Fees Due <br/>List With Contact Details</a>
                        </div>
                    </div>								
                </div>
                <div class="clearfix"></div>

            </div>	
        </div>        

        <div class="clearfix"></div>
    </div>
<? endif; ?>
<script type="text/javascript">
    /* Print Document */
    $("#print").live("click", function () {
        var session_id = '<?= $session_id ?>';
        var ct_sec = '<?= $ct_sec ?>';
        var interval_id = '<?= $interval_id ?>';
        var report_type = '<?= $f_type ?>';
        if (report_type === '') {
            alert("Sorry, You can't print this document..!");
            return false;
        }
        var dataString = '&session_id=' + session_id + '&ct_sec=' + ct_sec + '&interval_id=' + interval_id + '&f_type=' + report_type;

        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_fee_report', 'ajax_fee_report&temp=report'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });

    $(document).on('change', '#sess_int', function () {
        var sess_int = $(this).val();
        $("#session_list_details").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=report'); ?>", {act: 'get_sessions_list', sess_int: sess_int}, function (data) {
            $('#session_list_details').html(data);
            $('#session_id').change();
        });
    }).ready(function () {
        $('#sess_int').change();
    }).on('change', '.session_filter', function () {
        /* Session Students */
        var session_id = $("#session_id").val();
        $("#get_session_intervals").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=report'); ?>", {act: 'get_session_intervals', session_id: session_id}, function (data_new) {
            $("#get_session_intervals").html(data_new);
        });
        if (session_id.length > 0) {
            var id = session_id;
            $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            var dataString = 'id=' + id;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section&temp=report'); ?>",
                data: dataString,
                success: function (data) {
                    $("#ajex_section").html(data);
                }
            });
        }
        else {
            return false;
        }
    });
</script>