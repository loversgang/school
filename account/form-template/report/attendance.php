<!-- BEGIN PAGE CONTAINER-->
<? if ($a_type == 'month_wise'): ?>
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Student Attendance Reports
                </h3>
                <ul class="breadcrumb hidden-print">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Attendance Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right hidden-print">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'attendance', 'attendance'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Attendance Reports</div></div>
                </a> 
            </div>	
        </div>  	
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid hidden-print">
            <form class="" action="<?php echo make_admin_url('report', 'attendance', 'attendance') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>	
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='attendance'/>
                <input type='hidden' name='section' value='attendance'/>
                <input type='hidden' name='a_type' value='month_wise'/>

                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                    <div class="row-fluid">	
                        <!--                        <div class='span4'>
                                                    <div class='span12' style='margin-left: 5px;'>
                                                        <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                                                        <select class="select2_category span11 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                            <option>Select Section</option>
                        <?php
                        $current = '';
                        $session_sr = 1;
                        foreach ($session_array as $sess_key => $session):
                            ?>
                                                                                                                                    <option value='<?= $session->id ?>' <?
                            if ($session->id == $session_id) {
                                echo 'selected';
                            }
                            ?> ><?= ucfirst($session->title) ?></option>
                            <?
                            $session_sr++;
                        endforeach;
                        ?>
                                                        </select>
                                                    </div>
                                                </div>
                        <?
                        /* Get section Pages for current session */
                        $QuerySec = new studentSession();
                        $QuerySec->listAllSessionSection($session_id);

                        #get Session Info
                        $QueryS = new session();
                        $session = $QueryS->getRecord($session_id);
                        ?>
                                                <div class='span4'>
                                                    <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                                                        <label class="control-label">Select Section </label>
                                                        <div class="controls">
                        <? if ($QuerySec->GetNumRows() > 0): ?>
                                                                                                                                    <select class="select2_category span3" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                            <?php
                            $session_sc = 1;
                            while ($sec = $QuerySec->GetObjectFromRecord()):
                                ?>
                                                                                                                                                                                                                <option value='<?= $sec->section ?>'<?
                                if ($sec->section == $ct_sec) {
                                    echo 'selected';
                                }
                                ?>><?= ucfirst($sec->section) ?></option>
                                <?
                                $session_sc++;
                            endwhile;
                            ?>
                                                                                                                                    </select>
                                                                                                                                    <select name='date' class='span6'>
                            <?= getMonthsOnly($session->start_date, $session->end_date, $month); ?>
                                                                                                                                    </select>
                        <? else: ?>
                                                                                                                                    <input type='text' value='Sorry, No Section Found.' disabled />	
                        <? endif; ?>	
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class='span4' style='margin-left: 0px;'>
                                                    <div class='span12' style='margin-left: 5px;'>
                                                        <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                                                        <div class='span5' style='margin-left: 0px;'>
                                                            <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                                                        </div>
                                                        <div class='span7'>
                        <? if (!empty($record)): ?>
                                                                                                                                    <a class='btn yellow medium span6' href="<?= make_admin_url('report', 'download', 'download&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&month=' . $month . '&tmp=attendance') ?>">Download</a>
                                                                                                                                    <a class='btn blue medium span6' href="<?= make_admin_url('report', 'excel', 'excel&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&month=' . $month . '&report_type=' . $report_type . '&tmp=attendance') ?>"><i class="icon-download"></i> Excel</a> 
                        <? endif; ?>
                                                        </div>
                                                    </div>	
                                                </div>	-->


                        <div class='span3'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                            <select class="select2_category session_filter_more" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                                <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                    <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class='span4'>
                            <div id="session_list_details"></div>
                        </div>
                        <?
                        /* Get section Pages for current session */
                        $QuerySec = new studentSession();
                        $QuerySec->listAllSessionSection($session_id);

                        #get Session Info
                        $QueryS = new session();
                        $session = $QueryS->getRecord($session_id);
                        ?>
                        <div class='span3' id='ajex_section'>
                            <label class="control-label" style='width:auto;'>Select Section</label>
                            <? if ($QuerySec->GetNumRows() > 0): ?>
                                <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                    <option value="0">All</option>
                                    <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                        <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                    <? endwhile; ?>	
                                </select>
                            <? else: ?>
                                <input type='text' class="span10" value='No Section Found.' disabled />
                            <? endif; ?>											
                        </div>
                        <div class='span4' style='margin-left: 0px;'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                                <div class='span5' style='margin-left: 0px;'>
                                    <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                                </div>

                                <div class='span7'>
                                    <? if (!empty($record)): ?>
                                        <a class='btn yellow medium span6' href="<?= make_admin_url('report', 'download', 'download&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&month=' . $month . '&tmp=attendance') ?>">Download</a>
                                        <a class='btn blue medium span6' href="<?= make_admin_url('report', 'excel', 'excel&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&month=' . $month . '&report_type=' . $report_type . '&tmp=attendance') ?>"><i class="icon-download"></i> Excel</a> 
                                    <? endif; ?>
                                </div>

                            </div>	
                        </div>	

                    </div>

                    <div class="clearfix"></div>
                </div>

            </form>	
        </div>	


        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
            <div class="span12">	
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Month Wise Attendance Reports </div>	
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover sample_2" id="sample_2">
                                <thead>
                                <th class="hidden-480">SR. NO.</th>
                                <th class="hidden-480">REG NO.</th>
                                <th class="hidden-480" >ROLL No.</th>
                                <th class="hidden-480" >SECTION</th>
                                <th class="hidden-480" >STUDENT NAME</th>
                                <th class="hidden-480" >FATHER NAME</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled">PRESENT</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled">ABSENT</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled">LEAVE</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled">WORKING DAYS</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled">%AGE</th>
                                </thead>
                                <? if (!empty($record)): ?>
                                    <? if (!empty($record)): $sum = '';
                                        ?>
                                        <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($record as $sk => $stu): $checked = '';
                                                $Q_obj = new attendance();
                                                $r_atten = $Q_obj->checkSessionSectionMonthYearAttendace($session_id, $ct_sec, $stu->id, $date, $year);
                                                $t_attend = array_sum($r_atten);

                                                $M_obj = new schoolHolidays();
                                                $M_holiday = $M_obj->checkMonthlyHoliday($school->id, $date, $year);
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td><?= $sr ?>.</td>
                                                    <td><?= $stu->reg_id ?></td>
                                                    <td><?= $stu->roll_no ?></td>
                                                    <td ><?= ucfirst($stu->section) ?></td>
                                                    <td ><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                                    <td><?= ucfirst($stu->father_name) ?></td>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?
                                                        if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $present = $r_atten['P'];
                                                        else: echo $present = '0';
                                                        endif;
                                                        ?></td>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?
                                                        if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
                                                        else: echo '0';
                                                        endif;
                                                        ?></td>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?
                                                        if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
                                                        else: echo '0';
                                                        endif;
                                                        ?></td>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= ($working_days - $M_holiday) ?></td>
                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= number_format(($present / ($working_days - $M_holiday)) * 100, 2) ?>%</td>	
                                                </tr>
                                                <?
                                                $sr++;
                                            endforeach;
                                            ?>	
                                        </tbody>
                                    <?php endif; ?>  

                                <? endif; ?>
                            </table>

                        </div>
                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

    <!--  Class Wise -->
<? elseif ($a_type == 'class_wise'): ?>
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Student Attendance Reports
                </h3>
                <ul class="breadcrumb hidden-print">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Attendance Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right hidden-print">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'attendance', 'attendance'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Attendance Reports</div></div>
                </a> 
            </div>	
        </div>  	
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid hidden-print">
            <form class="" action="<?php echo make_admin_url('report', 'attendance', 'attendance') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>	
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='attendance'/>
                <input type='hidden' name='section' value='attendance'/>
                <input type='hidden' name='a_type' value='<?= $a_type ?>'/>
                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                    <div class="row-fluid">	


                        <div class='span3'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                            <select class="select2_category session_filter_more" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                                <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                    <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class='span3'>
                            <div id="session_list_details"></div>
                        </div>
                        <?
                        /* Get section Pages for current session */
                        $QuerySec = new studentSession();
                        $QuerySec->listAllSessionSection($session_id);

                        #get Session Info
                        $QueryS = new session();
                        $session = $QueryS->getRecord($session_id);
                        ?>
                        <span class='span3' id='ajex_section'>
                            <label class="control-label" style='width:auto;'>Select Section</label>
                            <? if ($QuerySec->GetNumRows() > 0): ?>
                                <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                    <option value="0">All</option>
                                    <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                        <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                    <? endwhile; ?>	
                                </select>
                            <? else: ?>
                                <input type='text' class="span10" value='No Section Found.' disabled />
                            <? endif; ?>											
                        </span>
                        <?
                        #get Session Info
                        $QueryS = new session();
                        $session = $QueryS->getRecord($session_id);
                        ?>
                        <div class='span3'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label">Select Month & Year</label>
                                <div class="controls">
                                    <input type='text' name='date' class='span11 month-picker' value='<?= date('M Y', strtotime('01-' . $month)) ?>'>
                                </div>
                            </div>
                        </div>
                        <div class='span4' style='margin-left: 0px;'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                                <div class='span5' style='margin-left: 0px;'>
                                    <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                                </div>
                                <div class='span7'>
                                    <? if (!empty($record)): ?>
                                        <a class='btn yellow medium span6' href="<?= make_admin_url('report', 'download', 'download&month=' . $month . '&tmp=attendance&a_type=class_wise') ?>">Download</a>
                                        <!--<a class='btn blue medium span6' href="<?= make_admin_url('report', 'excel', 'excel&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&month=' . $month . '&report_type=' . $report_type . '&tmp=attendance') ?>"><i class="icon-download"></i> Excel</a> -->
                                    <? endif; ?>
                                </div>
                            </div>	
                        </div>	
                    </div>

                    <div class="clearfix"></div>
                </div>

            </form>	
        </div>	


        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
            <div class="span12">	
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Class Wise Attendance Reports (<?= date('M Y', strtotime('01-' . $month)) ?>) </div>	
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover sample_2" id="sample_2">
                                <thead>
                                <th class="hidden-480">SR. NO.</th>
                                <th class="hidden-480" >SESSION NAME</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled" >TOTAL STUDENTS</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled">PRESENT</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled">ABSENT</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled">LEAVE</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled">WORKING DAYS</th>
                                <th style='text-align:center;' class="hidden-480 sorting_disabled">%AGE</th>
                                </thead>

                                <? if (!empty($record)): $sum = '';
                                    ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $sk => $object): $checked = '';
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $sr ?>.</td>
                                                <td><?= $object['session_name'] ?></td>
                                                <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $object['total_student'] ?></td>
                                                <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $object['present'] ?></td>
                                                <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $object['absent'] ?></td>
                                                <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $object['leave'] ?></td>
                                                <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $object['total_days'] ?></td>
                                                <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= number_format(($object['present'] / ($working_days - $M_holiday)) * 100, 2) ?>%</td>
                                            </tr>
                                            <?
                                            $sr++;
                                        endforeach;
                                        ?>	
                                    </tbody>
                                <?php endif; ?>  


                            </table>

                        </div>
                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

    <!--  Single student Wise -->
<? elseif ($a_type == 'single'): ?>
    <div class="container-fluid">
        <? if (!empty($student_id)): ?>	
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Student Wise Attendance Reports
                    </h3>
                    <ul class="breadcrumb hidden-print">
                        <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li> 
                        <li>
                            <i class="icon-list"></i>
                            <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                            <i class="icon-angle-right"></i>
                        </li> 
                        <li class="last">
                            Attendance Reports
                        </li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="tiles pull-right hidden-print">
                <div class="tile bg-yellow">
                    <a href="<?php echo make_admin_url('report', 'download', 'download&a_type=single&tmp=attendance&session_id=' . $session_id . "&ct_sec=" . $ct_sec . "&student_id=" . $student_id); ?>">
                        <div class="corner"></div>
                        <div class="tile-body"><i class="icon-list"></i></div>
                        <div class="tile-object"><div class="name">Print</div></div>
                    </a> 
                </div>	
                <div class="tile bg-blue selected">
                    <a href="<?php echo make_admin_url('report', 'attendance', 'attendance&a_type=single'); ?>">
                        <div class="corner"></div>
                        <div class="tile-body"><i class="icon-list"></i></div>
                        <div class="tile-object"><div class="name">Select Student</div></div>
                    </a> 
                </div>	
                <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
                <div class="tile bg-purple selected">
                    <a href="<?php echo make_admin_url('report', 'attendance', 'attendance'); ?>">
                        <div class="corner"></div>
                        <div class="tile-body"><i class="icon-list"></i></div>
                        <div class="tile-object"><div class="name">Attendance Reports</div></div>
                    </a> 
                </div>	
            </div>  	
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
            <div class="row-fluid">
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Student Attendance Record </div>
                    </div>
                    <div class="portlet-body">	
                        <!-- Attendance Table Chart-->
                        <table class="table table-striped table-hover" id='student_attendance_table'>
                            <tr class='brown_td'>
                                <td rowspan='8' style='width:260px;background:#FFFFFF !important;'>
                                    <?
                                    if (!empty($student->photo)):
                                        $im_obj = new imageManipulation();
                                        ?>
                                        <img style="width:260px;" src="<?= $im_obj->get_image('student', 'big', $student->photo); ?>" />
                                    <? else: ?>
                                        <img style="width:260px;" src="assets/img/profile/img.jpg" alt="">
                                    <? endif;
                                    ?>
                                </td>
                                <td>Admission No.</td>
                                <td>Date of Birth</td>
                                <td>Gender</td>
                                <td>Contact No.</td>	
                            </tr>
                            <tr>
                                <td><?= $student->reg_id ?></td>
                                <td><?= $student->date_of_birth ?></td>
                                <td><?= $student->sex ?></td>
                                <td><?= $student->phone ?></td>	
                            </tr>	
                            <tr class='brown_td'>
                                <td>Course</td>
                                <td>Name of Student</td>
                                <td>Category</td>
                                <td>Permanent Address</td>	
                            </tr>
                            <tr>
                                <td><?= $session_info->title ?></td>
                                <td><?= $student->first_name . " " . $student->last_name ?></td>
                                <td><?= $student->category ?></td>
                                <td rowspan='5'>
                                    <?
                                    if (is_object($s_p_ad)):
                                        echo $s_p_ad->address1 . ", " . $s_p_ad->city . ", " . $s_p_ad->state . ", " . $s_p_ad->zip;
                                    endif;
                                    ?>
                                </td>	
                            </tr>	
                            <tr class='brown_td'>
                                <td>Section</td>
                                <td>Father's Name</td>
                                <td>Cast</td>	
                            </tr>
                            <tr>
                                <td><?= $student->section ?></td>
                                <td><?= $student->father_name ?></td>
                                <td><?= $student->cast ?></td>	
                            </tr>
                            <tr class='brown_td'>	
                                <td>Roll No.</td>
                                <td>Mother's Name</td>
                                <td>Religion</td>	
                            </tr>
                            <tr>
                                <td><?= $student->roll_no ?></td>
                                <td><?= $student->mother_name ?></td>
                                <td><?= $student->religion ?></td>	
                            </tr>	
                        </table>
                        <?
                        #set the last date of the session
                        if (strtotime($session_info->end_date) > strtotime(date('Y-m-d'))):
                            $last_Session_date = date('Y-m-d');
                        else:
                            $last_Session_date = $session_info->end_date;
                        endif;

                        $AllMonth = getMonthsArray($session_info->start_date, $last_Session_date);
                        ?>
                        <table id='custom_table'>
                            <tr class='brown_td'>
                                <td rowspan='2' width='70%' style='text-align:center;vertical-align:middle;'> P=Present, A=Absent, L=Leave, H=Holiday</td>
                                <td colspan='4' style='text-align:center;'>Attendance Table</td>	
                            </tr>	
                            <tr>
                                <td><strong>P</strong></td>
                                <td><strong>A</strong></td>
                                <td><strong>L</strong></td>
                                <td><strong>H</strong></td>	
                            </tr>	


                            <?
                            if (!empty($AllMonth)): $P_Total = '0';
                                $A_Total = '0';
                                $L_Total = '0';
                                $H_Total = '0';
                                foreach ($AllMonth as $m_key => $m_val): $st = '1';
                                    $st1 = '1';
                                    $P = '0';
                                    $A = '0';
                                    $L = '0';
                                    $H = '0';
                                    $last_date_of_month = date("d", strtotime("+1 month -1 second", strtotime(date($m_val . '-01'))));
                                    ?>	
                                    <tr><td style='border:none;'>
                                            <table class="table table-striped table-hover" id='atten_inner_table'>	
                                                <tr><td rowspan='2' style='vertical-align: middle; width: 300px;' class='head_td'><?= date('M Y', strtotime($m_val . '-01')) ?></td>
                                                    <?
                                                    while ($st <= $last_date_of_month):
                                                        echo "<td>" . $st . "</td>";
                                                        $st++;
                                                    endwhile;
                                                    while ($st <= 31):
                                                        echo "<td style='visibility:hidden;'>" . $st . "</td>";
                                                        $st++;
                                                    endwhile;
                                                    ?>
                                                </tr><tr>

                                                    <?
                                                    while ($st1 <= $last_date_of_month):
                                                        if (strlen($st1) == '1'):
                                                            $st1 = '0' . $st1;
                                                        endif;
                                                        #Today Attendance
                                                        $QAttObj = new attendance();
                                                        $tdy_att = $QAttObj->get_today_attendance($session_id, $ct_sec, $student_id, $m_val . '-' . $st1);

                                                        #check Holiday
                                                        $M_obj = new schoolHolidays();
                                                        $holiday = $M_obj->checkHoliday($school->id, $m_val . '-' . $st1);

                                                        if ($tdy_att == 'P'): $P++;
                                                            $P_Total = $P_Total + 1;
                                                        elseif ($tdy_att == 'A'): $A++;
                                                            $A_Total = $A_Total + 1;
                                                        elseif ($tdy_att == 'L'): $L++;
                                                            $L_Total = $L_Total + 1;
                                                        endif;
                                                        echo "<td>";
                                                        if ($holiday == 'Holiday'): echo "H";
                                                            $H++;
                                                            $H_Total = $H_Total + 1;
                                                        else: echo $tdy_att . "</td>";
                                                        endif;
                                                        $st1++;
                                                    endwhile;
                                                    while ($st1 <= 31):
                                                        echo "<td style='visibility:hidden;'>" . $st . "</td>";
                                                        $st1++;
                                                    endwhile;
                                                    ?>	
                                                </tr>
                                            </table></td>
                                        <td class='head_td' style='vertical-align:middle;'><?= $P ?></td>
                                        <td class='head_td' style='vertical-align:middle;'><?= $A ?></td>
                                        <td class='head_td' style='vertical-align:middle;'><?= $L ?></td>
                                        <td class='head_td' style='vertical-align:middle;'><?= $H ?></td>
                                    </tr>
                                <? endforeach; ?>

                                <tr>
                                    <td style='vertical-align:middle;border-left:none;border-top:none;border-bottom:none;text-align:right'><strong>Total &nbsp;</strong></td>
                                    <td class='head_td' style='vertical-align:middle;padding:10px 1px 10px 0;' ><?= $P_Total ?></td>
                                    <td class='head_td' style='vertical-align:middle;'><?= $A_Total ?></td>
                                    <td class='head_td' style='vertical-align:middle;'><?= $L_Total ?></td>
                                    <td class='head_td' style='vertical-align:middle;'><?= $H_Total ?></td>
                                </tr>
                            </table>	
                        <? endif; ?>


                        </table>	
                        <!-- Attendance Table End-->
                    </div>
                </div>
            </div>	

        <? else: ?> 
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                <div class="span12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        Student Wise Attendance Reports
                    </h3>
                    <ul class="breadcrumb hidden-print">
                        <li>
                            <i class="icon-home"></i>
                            <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li> 
                        <li>
                            <i class="icon-list"></i>
                            <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                            <i class="icon-angle-right"></i>
                        </li> 
                        <li class="last">
                            Attendance Reports
                        </li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="tiles pull-right">
                <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
                <div class="tile bg-purple selected">
                    <a href="<?php echo make_admin_url('report', 'attendance', 'attendance'); ?>">
                        <div class="corner"></div>
                        <div class="tile-body"><i class="icon-list"></i></div>
                        <div class="tile-object"><div class="name">Attendance Reports</div></div>
                    </a> 
                </div>	
            </div>  	
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
            <div class="tiles pull-left row-fluid hidden-print">
                <form class="" action="<?php echo make_admin_url('report', 'attendance', 'attendance') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>	
                    <input type='hidden' name='Page' value='report'/>
                    <input type='hidden' name='action' value='attendance'/>
                    <input type='hidden' name='section' value='attendance'/>
                    <input type='hidden' name='a_type' value='single'/>

                    <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                        <div class="row-fluid">	
                            <div class='span3'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                                <select class="select2_category session_filter_more" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                                    <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                        <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                            <div class='span4'>
                                <div id="session_list_details"></div>
                            </div>
                            <?
                            /* Get section Pages for current session */
                            $QuerySec = new studentSession();
                            $QuerySec->listAllSessionSection($session_id);

                            #get Session Info
                            $QueryS = new session();
                            $session = $QueryS->getRecord($session_id);
                            ?>
                            <div class='span3' id='ajex_section'>
                                <label class="control-label" style='width:auto;'>Select Section</label>
                                <? if ($QuerySec->GetNumRows() > 0): ?>
                                    <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                        <option value="0">All</option>
                                        <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                            <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                        <? endwhile; ?>	
                                    </select>
                                <? else: ?>
                                    <input type='text' class="span10" value='No Section Found.' disabled />
                                <? endif; ?>											
                            </div>
                            <div class='span4' style='margin-left: 0px;'>
                                <div class='span12' style='margin-left: 5px;'>
                                    <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                                    <div class='span5' style='margin-left: 0px;'>
                                        <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                                    </div>

                                </div>	
                            </div>	
                        </div>

                        <div class="clearfix"></div>
                    </div>

                </form>	
            </div>	


            <div class="clearfix"></div>
            <?php
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
            <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">
                <div class="span12">	
                    <div class="portlet box purple">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-list"></i>Student Wise Attendance Reports</div>	
                        </div>
                        <div class="portlet-body">
                            <div class="clearfix">
                                <form action='<?= make_admin_url('report', 'attendance', 'attendance') ?>' class="form-horizontal" method='get'>
                                    <input type='hidden' name='Page' value='report'/>
                                    <input type='hidden' name='action' value='attendance'/>
                                    <input type='hidden' name='section' value='attendance'/>
                                    <input type='hidden' name='a_type' value='single'/>
                                    <input type='hidden' name='session_id' value='<?= $session_id ?>'/>
                                    <input type='hidden' name='ct_sec' value='<?= $ct_sec ?>'/>	
                                    <table class="table table-striped table-hover sample_2" >
                                        <thead>
                                            <tr>
                                                <th class='hidden-480 sorting_disabled'></th>
                                                <th>Reg ID.</th>
                                                <th class='hidden-480 sorting_disabled'>Roll No.</th>
                                                <th>Name</th>
                                                <th>Father Name</th>

                                            </tr>
                                        </thead>
                                        <? if (!empty($record)): ?>
                                            <tbody>
                                                <?php
                                                $sr = 1;
                                                foreach ($record as $s_k => $object):
                                                    ?>
                                                    <tr class="odd gradeX current_student" rel="<?= $object->id ?>">	
                                                        <td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' <?= ($student_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>	
                                                        <td><?= $object->reg_id ?></td>	
                                                        <td class='hidden-480 sorting_disabled'><?= $object->roll_no ?></td>
                                                        <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                        <td><?= $object->father_name ?></td>	
                                                    </tr>
                                                    <?php
                                                    $sr++;
                                                endforeach;
                                                ?>
                                            </tbody>
                                        <? else: ?>
                                            <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>	
                                        <?php endif; ?>  
                                    </table>
                                    <div class="form-actions NoPaddingCenter">
                                        <? if (!empty($record)): ?>
                                            <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                        <?php endif; ?>  
                                        <a name="cancel" class="btn" href="<?= make_admin_url('report', 'attendance', 'attendance') ?>"> Cancel</a>
                                    </div>	
                            </div>
                        </div>
                    </div>	
                </div>


            </div>
        <? endif; ?>
        <div class="clearfix"></div>
    </div>

    <!--  50% Wise -->
<? elseif ($a_type == 'half'): ?>
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Student Attendance Reports
                </h3>
                <ul class="breadcrumb hidden-print">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Attendance Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right hidden-print">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
            <div class="tile bg-purple selected">
                <a href="<?php echo make_admin_url('report', 'attendance', 'attendance'); ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-list"></i></div>
                    <div class="tile-object"><div class="name">Attendance Reports</div></div>
                </a> 
            </div>	
        </div>  	
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-left row-fluid hidden-print">
            <form class="" action="<?php echo make_admin_url('report', 'attendance', 'attendance') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>	
                <input type='hidden' name='Page' value='report'/>
                <input type='hidden' name='action' value='attendance'/>
                <input type='hidden' name='section' value='attendance'/>
                <input type='hidden' name='a_type' value='<?= $a_type ?>'/>

                <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                    <div class="row-fluid">	
                        <div class='span8'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                                <select class="select2_category span4 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                    <option>Select Section</option>
                                    <?php
                                    $current = '';
                                    $session_sr = 1;
                                    foreach ($session_array as $sess_key => $session):
                                        ?>
                                        <option value='<?= $session->id ?>' <?
                                        if ($session->id == $session_id) {
                                            echo 'selected';
                                        }
                                        ?> ><?= ucfirst($session->title) ?></option>
                                                <?
                                                $session_sr++;
                                            endforeach;
                                            ?>
                                </select>
                            </div>

                            <div class="row-fluid">	
                                <div class='span8'>
                                    <div class='span6' style='margin-left: 5px;'>
                                        <label class="control-label" style='float: none; text-align: left;'>From Date</label>
                                        <input type='text' class="span12 upto_current_date" placeholder="Select date" name='from' value='<?= $from ?>'/>
                                    </div>

                                    <div class='span6' style='margin-left: 5px;'>
                                        <label class="control-label" style='float: none; text-align: left;'>To Date</label>
                                        <input type='text' class="span12 upto_current_date" placeholder="Select date" name='to' value='<?= $to ?>'/>
                                    </div>	
                                </div>
                            </div>
                        </div>
                        <div class='span4' style='margin-left: 0px;'>
                            <div class='span12' style='margin-left: 5px;'>
                                <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                                <div class='span5' style='margin-left: 0px;'>
                                    <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                                </div>
                                <div class='span7'>
                                    <? if (!empty($record)): ?>
                                        <a class='btn yellow medium span6' href="<?= make_admin_url('report', 'download', 'download&session_id=' . $session_id . '&from=' . $from . '&to=' . $to . '&tmp=attendance&a_type=' . $a_type) ?>">Download</a>
                                        <!--
                                        <a class='btn blue medium span6' href="<?= make_admin_url('report', 'excel', 'excel&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&month=' . $month . '&report_type=' . $report_type . '&tmp=attendance') ?>"><i class="icon-download"></i> Excel</a>
                                        -->
                                    <? endif; ?>
                                </div>
                            </div>	
                        </div>	
                    </div>

                    <div class="clearfix"></div>
                </div>

            </form>	
        </div>	


        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->

        <div class="row-fluid">
            <div class="span12">	
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>More Than 50% Absent Attendance Records</div>	
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover sample_2" id="sample_2">
                                <thead>
                                <th class="hidden-480" >REG No.</th>
                                <th class="hidden-480" >ROLL No.</th>
                                <th class="hidden-480" >SESSION</th>
                                <th class="hidden-480" >SECTION</th>
                                <th class="hidden-480" >STUDENT NAME</th>
                                <th class="hidden-480" >FATHER NAME</th>
                                <th class="hidden-480" >PARENT’s EMAIL</th>
                                <th class="hidden-480" >PARENT’s CONTACT NO.</th>
                                </thead>

                                <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $sk => $stu): $checked = '';
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $stu->reg_id ?></td>
                                                <td><?= $stu->roll_no ?></td>
                                                <td><?= $session_info->title ?></td>

                                                <td ><?= ucfirst($stu->section) ?></td>
                                                <td ><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                                <td><?= ucfirst($stu->father_name) ?></td>
                                                <td><?= $stu->parents_email ?></td>
                                                <td><?= $stu->parents_phone ?></td>	
                                            </tr>
                                            <?
                                            $sr++;
                                        endforeach;
                                        ?>	
                                    </tbody>
                                <?php endif; ?>  


                            </table>

                        </div>
                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>

        <div class="clearfix"></div>
    </div>	
<? else: ?>	
    <!--  All Types -->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Student Attendance Reports
                </h3>
                <ul class="breadcrumb hidden-print">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                        <i class="icon-angle-right"></i>
                    </li> 
                    <li class="last">
                        Attendance Reports
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right hidden-print">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
        </div>  

        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="tiles pull-left row-fluid">
            <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;padding-left:0px;'>
                <br/>
                <div class="row-fluid" id='custom_dashboard'>
                    <div class='span3'>
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'attendance', 'attendance&a_type=single') ?>">
                                <img src="assets/img/icon/EXM.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'attendance', 'attendance&a_type=single') ?>" class='title'>Student<br/> Wise Attendance</a>
                        </div>
                    </div>

                    <div class='span3'>
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'attendance', 'attendance&a_type=class_wise') ?>">
                                <img src="assets/img/icon/bar-chart-icon1.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'attendance', 'attendance&a_type=class_wise') ?>" class='title'>Monthly Class<br/> Wise Attendance</a>
                        </div>
                    </div>	
                    <div class='span3'>
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'attendance', 'attendance&a_type=month_wise') ?>"/>
                            <img src="assets/img/icon/attendence.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'attendance', 'attendance&a_type=month_wise') ?>" class='title'>Monthly Student<br/> Attendance Detail</a>
                        </div>
                    </div>	
                    <div class='span3'>
                        <div class="easy-pie-chart">
                            <a href="<?= make_admin_url('report', 'attendance', 'attendance&a_type=half') ?>"/>
                            <img src="assets/img/icon/EXM2.png"/>
                            </a>	
                            <a href="<?= make_admin_url('report', 'attendance', 'attendance&a_type=half') ?>" class='title'>More than 50%<br/> Absent Attendance</a>
                        </div>
                    </div>	
                </div>
                <div class="clearfix"></div>	
            </div>	
        </div>	
        <div class="clearfix"></div>
        <!-- BEGIN PAGE CONTENT-->
    </div>	
<? endif; ?>

<!--/////////////////////////////////////////////////////////// -->
<script type="text/javascript">

    $(".session_filter").change(function () {
        var session_id = $("#session_id").val();

        if (session_id.length > 0) {
            var id = session_id;
            $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id;

            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section_attendance&temp=report'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_section").html(data);
                }
            });
        }
        else {
            return false;
        }
    });
    $(".session_filter_single").change(function () {
        var session_id = $("#session_id").val();

        if (session_id.length > 0) {
            var id = session_id;
            $("#ajex_section_single").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id;

            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_single', 'ajax_section_single&temp=report'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_section_single").html(data);
                }
            });
        }
        else {
            return false;
        }
    });
    /* show doc_preview  */
    $(".current_student").live("click", function () {
        var session_id = $("#session_id").val();
        var ct_sec = $("#ct_sec").val();
        var student_id = $(this).attr('rel');
        $(".current_student").find('span').removeClass('checked');
        $(".current_student").find('td input:radio').prop('checked', false);

        $(this).find('span').addClass('checked');
        $(this).find('td input:radio').prop('checked', true);
    });
</script>
<script>
    /* Print Document */
    $("#print").live("click", function () {
        var session_id = '<?= $session_id ?>';
        var ct_sec = '<?= $ct_sec ?>';
        var student_id = '<?= $student_id ?>';
        var report_type = '<?= $a_type ?>';
        if (report_type === '') {
            alert("Sorry, You can't print this document..!");
            return false;
        }

        var dataString = '&session_id=' + session_id + '&ct_sec=' + ct_sec + '&student_id=' + student_id + '&a_type=' + report_type;

        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_attendance', 'ajax_section_attendance&temp=report'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                //console.log(data);
                //return;
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });

    $(document).on('change', '#sess_int', function () {
        var sess_int = $(this).val();
        $("#session_list_details").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=student'); ?>", {act: 'get_sessions_list', sess_int: sess_int}, function (data) {
            $('#session_list_details').html(data);
        });
    }).ready(function () {
        $('#sess_int').change();
    }).on('change', '.session_filter', function () {
        /* Session Students */
        var session_id = $("#session_id").val();
        if (session_id.length > 0) {
            var id = session_id;
            $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            var dataString = 'id=' + id;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section&temp=student'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_section").html(data);
                }
            });
        }
        else {
            return false;
        }
    });

</script>