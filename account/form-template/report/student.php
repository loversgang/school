<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Student Reports
            </h3>
            <ul class="breadcrumb hidden-print">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li class="last">
                    Student Report
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>

    <div class="clearfix"></div>
    <div class="tiles pull-right hidden-print">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            

    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-left row-fluid hidden-print">
        <form class="" action="<?php echo make_admin_url('report', 'student', 'student') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>
            <input type='hidden' name='Page' value='report'/>
            <input type='hidden' name='action' value='student'/>
            <input type='hidden' name='section' value='student'/>
            <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>
                <div class="row-fluid">
                    <div class="span3">
                        <label class="control-label" style='float: none; text-align: left;'>Select Report Type</label>
                        <select style="width:100%" name="report_type" id="concession" class="select2_category span11">
                            <? foreach ($report_array as $rp_key => $rp_val): ?>
                                <option value='<?= $rp_key ?>' <?
                                if ($report_type == $rp_key): echo "selected";
                                endif;
                                ?>><?= $rp_val ?></option>
                                    <? endforeach; ?>
                        </select>
                    </div>
                    <div class='span2'>
                        <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                        <select style="width:100%" class="select2_category session_filter_more" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                            <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                    <div class='span3'>
                        <div id="session_list_details"></div>
                    </div>
                    <div class='span2'>
                        <div style='margin-left: 5px; padding-right:20px' id='ajex_section'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Section</label>
                            <?php
                            $QuerySec = new studentSession();
                            $QuerySec->listAllSessionSection($session_id);
                            if ($QuerySec->GetNumRows()):
                                ?>
                                <select style="width:100%" class="select2_category" data-placeholder="Select Session Students" name='ct_sec'>
                                    <option value="0">ALL</option>
                                    <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                        <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                    <? endwhile; ?>				
                                </select>
                            <? else: ?>
                                <input style="width:100%" type='text' value='Sorry, No Section Found.' disabled />
                            <? endif; ?>	
                        </div>
                    </div>
                    <div class="span2">
                        <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                        <input type='submit' name='go' value='View Result' class='btn green medium span10'/>
                    </div>
                </div>
                <div class="row-fluid">	
                    <!--
                    <div class='span6'>
                            <div class='span8' style='margin-left: 5px;'>
                                    <label class="control-label" style='float: none; text-align: left;'>Select Student Type</label>
                                    <select name="concession" id="concession" class="select2_category span11">
                                            <option value=''>None</option>
                                            <option value='Full Poverty' <?
                    if ($student_type == 'Full Poverty'): echo "selected";
                    endif;
                    ?>>Full Poverty</option>
                                            <option value='Half Poverty' <?
                    if ($student_type == 'Half Poverty'): echo "selected";
                    endif;
                    ?>>Half Poverty</option>
                                            <option value='Scholarship' <?
                    if ($student_type == 'Scholarship'): echo "selected";
                    endif;
                    ?>>Scholarship</option>
                                            <option value='Special Student' <?
                    if ($student_type == 'Special Student'): echo "selected";
                    endif;
                    ?>>Special Student</option>
                                            <option value='Sports Student' <?
                    if ($student_type == 'Sports Student'): echo "selected";
                    endif;
                    ?>>Sport Student</option>
                                            <option value='Sibling Student' <?
                    if ($student_type == 'Sibling Student'): echo "selected";
                    endif;
                    ?>>Sibling Student</option>
                                    </select>
                            </div>
                    </div>
                    -->
                    <div class="clearfix"></div>
                </div>
        </form>										
    </div>	


    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <? if ($report_type == "admission_detail"): ?>	
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i> <?= $report_array[$report_type] ?> > <?= $session_obj->title; ?> > <?php if ($ct_sec == '0') { ?>All <?php } else { ?><?= $ct_sec; ?><?php } ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th class="hidden-480">SR. NO.</th>
                                        <th class="hidden-480" >DATE OF ADMISSION</th>
                                        <th class="hidden-480" >ADMISSION NO.</th>
                                        <th class="hidden-480" >ROLL NO.</th>
                                        <th class="hidden-480" >SECTION</th>
                                        <th class="hidden-480" >STUDENT NAME</th>
                                        <th class="hidden-480" >GENDER</th>
                                        <th class="hidden-480" >FATHER'S NAME</th>
                                        <th class="hidden-480" >MOTHER'S NAME</th>
                                        <th class="hidden-480" >DATE OF BIRTH</th>
                                        <th class="hidden-480" >CATEGORY</th>
                                    </tr>
                                </thead>
                                <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $sk => $stu):
                                            $QueryP = new studentAddress();
                                            $s_p_ad = $QueryP->getStudentAddressByType($stu->id, 'permanent');
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $sr ?>.</td>
                                                <td><?= date('d M, Y', strtotime($stu->date_of_admission)) ?></td>
                                                <td ><?= $stu->reg_id ?></td>
                                                <td ><?= $stu->roll_no ?></td>
                                                <td ><?= $stu->section ?></td>
                                                <td ><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                                <td><?= $stu->sex ?></td>
                                                <td><?= ucfirst($stu->father_name) ?></td>
                                                <td><?= ucfirst($stu->mother_name) ?></td>
                                                <td><?= date('d M, Y', strtotime($stu->date_of_birth)) ?></td>
                                                <td><?= $stu->category ?></td>
                                            </tr>
                                            <?
                                            $sr++;
                                        endforeach;
                                        ?>
                                    </tbody>
                                <?php endif; ?>
                            </table>
                        </div>
                    </div>
                </div>
            <? elseif ($report_type == "withdrawal_detail"): ?>
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $report_array[$report_type] ?> > <?= $session_obj->title; ?> > <?php if ($ct_sec == '0') { ?>All <?php } else { ?><?= $ct_sec; ?><?php } ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th >SR. NO.</th>
                                        <th class="hidden-480" >DATE OF ADMISSION</th>
                                        <th class="hidden-480" >STUDENT NAME</th>
                                        <th class="hidden-480" >STRUCK OFF DATE</th>
                                    </tr>
                                </thead>
                                <? if (!empty($All_Session)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($All_Session as $sk => $stu):
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $sr ?>.</td>
                                                <td><?= date('d M, Y', strtotime($stu->date_of_admission)) ?></td>
                                                <td ><?= $stu->first_name . ' ' . $stu->last_name ?></td>
                                                <td ><?= $stu->stuck_off_date ?></td>

                                            </tr>
                                            <?
                                            $sr++;
                                        endforeach;
                                        ?>	
                                    </tbody>
                                <?php endif; ?>  
                            </table>

                        </div>
                    </div>
                </div>				
            <? elseif ($report_type == "student_type"): ?>			
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $report_array[$report_type] ?> > <?= $session_obj->title; ?> > <?php if ($ct_sec == '0') { ?>All <?php } else { ?><?= $ct_sec; ?><?php } ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Reg ID</th>
                                        <th>Roll NO.</th>
                                        <th>Student Name</th>
                                        <th class="hidden-480 sorting_disabled" >Father Name</th>
                                        <th class="hidden-480 sorting_disabled">Student Granted</th>
                                        <th class="hidden-480 sorting_disabled">Concession Type</th>
                                        <th class="hidden-480 sorting_disabled">Amount</th>
                                    </tr>
                                </thead>
                                <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $key => $object):
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $object->reg_id ?></td>
                                                <td><?= $object->roll_no ?></td>
                                                <td><?= ucfirst($object->first_name) . " " . $object->last_name ?></td>
                                                <td><?= $object->father_name ?></td>
                                                <td class="hidden-480 sorting_disabled"><?
                                                    if (empty($object->concession)):echo "None";
                                                    else: echo $object->concession;
                                                    endif;
                                                    ?>
                                                </td>
                                                <td class="hidden-480 sorting_disabled">
                                                    <?php echo ucfirst($object->concession_type); ?>
                                                </td>
                                                <td class="hidden-480 sorting_disabled"><?= number_format($object->amount, 2) ?></td>
                                            </tr>
                                            <?php
                                            $sr++;
                                        endforeach;
                                        ?>
                                    </tbody>
                                <?php endif; ?>  
                            </table>

                        </div>
                    </div>
                </div>	

            <? elseif ($report_type == "contact_detail"): ?>			
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $report_array[$report_type] ?> > <?= $session_obj->title; ?> > <?php if ($ct_sec == '0') { ?>All <?php } else { ?><?= $ct_sec; ?><?php } ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Reg ID</th>
                                        <th>Roll NO.</th>
                                        <th>Name</th>
                                        <th>Father Name</th>
                                        <th>Contact No.</th>
                                        <th class="hidden-480 sorting_disabled">Permanent Address</th>
                                        <th>Photo</th>
                                    </tr>
                                </thead>
                                <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $key => $object):
                                            #get student permanent address
                                            $QueryP = new studentAddress();
                                            $s_p_ad = $QueryP->getStudentAddressByType($object->id, 'permanent');

                                            #get student correspondence address
                                            $QueryC = new studentAddress();
                                            $s_c_ad = $QueryC->getStudentAddressByType($object->id, 'correspondence');
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $object->reg_id ?></td>
                                                <td><?= $object->roll_no ?></td>
                                                <td><?= ucfirst($object->first_name) . " " . $object->last_name ?></td>
                                                <td><?= $object->father_name ?></td>
                                                <td><?= $object->phone ?></td>
                                                <td class="hidden-480 sorting_disabled">
                                                    <?
                                                    if (is_object($s_p_ad)):
                                                        echo $s_p_ad->address1 . ", " . $s_p_ad->city . ", " . $s_p_ad->tehsil . ", " . $s_p_ad->district . ", " . $s_p_ad->state . ", " . $s_p_ad->zip;
                                                    endif;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?
                                                    if (!empty($object->photo)):
                                                        $im_obj = new imageManipulation()
                                                        ?>
                                                        <img src="<?= $im_obj->get_image('student', 'big', $object->photo); ?>" style='width:112px;'/>
                                                    <? endif; ?>	
                                                </td>
                                            </tr>
                                            <?php
                                            $sr++;
                                        endforeach;
                                        ?>
                                    </tbody>
                                <?php endif; ?>  
                            </table>

                        </div>
                    </div>
                </div>	

            <? elseif ($report_type == "subject_detail"): ?>			
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $report_array[$report_type] ?> > <?= $session_obj->title; ?> > <?php if ($ct_sec == '0') { ?>All <?php } else { ?><?= $ct_sec; ?><?php } ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Reg ID</th>
                                        <th class="hidden-480" >ROLL NO.</th>
                                        <th class="hidden-480" >DOB</th>
                                        <th>Student Name</th>
                                        <th>Father Name</th>
                                        <?php
                                        $session_detail = get_object('session', $session_id);

                                        $QueryStComSub = new subject();
                                        $cmpSub = $QueryStComSub->GetSessionSubjectsInArray($session_detail->compulsory_subjects);

                                        if (!empty($cmpSub)):
                                            foreach ($cmpSub as $key => $val):
                                                echo "<th>SUB " . ($key + 1) . "</th>";
                                            endforeach;
                                        endif;
                                        ?>
                                        <th class="hidden-480 sorting_disabled">Elective Subjects</th>												
                                    </tr>
                                </thead>
                                <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $key => $object):
                                            #get the student subject list if enter previous
                                            $session = get_object('session', $session_id);

                                            $QueryStElcSub = new studentSubject();
                                            $elcSub = $QueryStElcSub->getStudentSubjectTitle($session_id, $object->id, 'Elective');
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $object->reg_id ?></td>
                                                <td><?= $object->roll_no ?></td>
                                                <td><?= date('d M, Y', strtotime($object->date_of_birth)) ?></td>
                                                <td><?= ucfirst($object->first_name) . " " . $object->last_name ?></td>
                                                <td><?= ucfirst($object->father_name) ?></td>
                                                <?
                                                if (!empty($cmpSub)):
                                                    foreach ($cmpSub as $Ckey => $Cval):
                                                        echo "<td>" . ($Cval) . "</td>";
                                                    endforeach;
                                                endif;
                                                ?>	
                                                <td class="hidden-480 sorting_disabled"><?= $elcSub ?></td>
                                            </tr>
                                            <?php
                                            $sr++;
                                        endforeach;
                                        ?>
                                    </tbody>
                                <?php endif; ?>  
                            </table>

                        </div>
                    </div>
                </div>	


            <? elseif ($report_type == "category_detail"): ?>			
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $report_array[$report_type] ?> > <?= $session_obj->title; ?> > <?php if ($ct_sec == '0') { ?>All <?php } else { ?><?= $ct_sec; ?><?php } ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th class="hidden-480">SR. NO.</th>
                                        <th class="hidden-480" >DATE OF ADMISSION</th>
                                        <th class="hidden-480" >ADMISSION NO.</th>
                                        <th class="hidden-480" >ROLL NO.</th>
                                        <th class="hidden-480" >STUDENT NAME</th>
                                        <th class="hidden-480" >FATHER'S NAME</th>
                                        <th class="hidden-480" >MOTHER'S NAME</th>
                                        <th class="hidden-480" >CATEGORY</th>
                                    </tr>
                                </thead>
                                <? if (!empty($record)): $sum = ''; ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $key => $stu):
                                            ?>
                                            <tr class="odd gradeX">
                                                <td><?= $sr ?>.</td>
                                                <td><?= date('d M, Y', strtotime($stu->date_of_admission)) ?></td>
                                                <td ><?= $stu->reg_id ?></td>
                                                <td ><?= $stu->roll_no ?></td>
                                                <td ><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                                <td><?= ucfirst($stu->father_name) ?></td>
                                                <td><?= ucfirst($stu->mother_name) ?></td>
                                                <td><?= $stu->category ?></td>
                                            </tr>
                                            <?php
                                            $sr++;
                                        endforeach;
                                        ?>
                                    </tbody>
                                <?php endif; ?>  
                            </table>

                        </div>
                    </div>
                </div>	
            <? elseif ($report_type == "address_label"): ?>	

                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i><?= $report_array[$report_type] ?> > <?= $session_obj->title; ?> > <?php if ($ct_sec == '0') { ?>All <?php } else { ?><?= $ct_sec; ?><?php } ?></div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <? if (!empty($record)): $sum = ''; ?>
                                <div class="row-fluid">
                                    <?php
                                    $sr = 1;
                                    foreach ($record as $key => $stu):
                                        #get student permanent address
                                        $QueryP = new studentAddress();
                                        $s_p_ad = $QueryP->getStudentAddressByType($stu->id, 'permanent');
                                        ?>											
                                        <div class='span6'>
                                            <div id='custom_address'>
                                                <strong><?= strtoupper($stu->first_name . " " . $stu->last_name) ?></strong><br/>
                                                S/o. | D/o. <?= strtoupper($stu->father_name) ?><br/><br/>
                                                <div class='span11'>
                                                    <strong>Address</strong><br/>
                                                    <?
                                                    if (is_object($s_p_ad)):
                                                        echo $s_p_ad->address1 . ", " . $s_p_ad->city . "<br/>" . $s_p_ad->state . ", " . $s_p_ad->zip;
                                                    endif;
                                                    ?>
                                                </div><div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php
                                        if ($sr % 2 == '0'): echo "</div><br/><div class='row-fluid'>";
                                        endif;
                                        $sr++;
                                    endforeach;
                                    ?>
                                    <div class="clearfix"></div>
                                </div>	
                            <?php else: ?>
                                Sorry, No Record Found..!
                            <?php endif; ?>  

                        </div>
                    </div>
                </div>						
            <? endif; ?>				
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<script type="text/javascript">
    /* Print Document */
    $("#print").live("click", function () {
        var session_id = '<?= $session_id ?>';
        var ct_sec = '<?= $ct_sec ?>';
        var report_type = '<?= $report_type ?>';
        if (report_type === '') {
            alert("Sorry, You can't print this document..!");
            return false;
        }

        var dataString = '&session_id=' + session_id + '&ct_sec=' + ct_sec + '&report_type=' + report_type;

        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_students_report', 'ajax_students_report&temp=report'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });

    $(document).on('change', '#sess_int', function () {
        var sess_int = $(this).val();
        $("#session_list_details").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=report'); ?>", {act: 'get_sessions_list', sess_int: sess_int}, function (data) {
            $('#session_list_details').html(data);
        });
    }).ready(function () {
        $('#sess_int').change();
    }).on('change', '.session_filter', function () {
        /* Session Students */
        var session_id = $("#session_id").val();
        if (session_id.length > 0) {
            var id = session_id;
            $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            var dataString = 'id=' + id;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section&temp=report'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_section").html(data);
                }
            });
        }
        else {
            return false;
        }
    });

</script>