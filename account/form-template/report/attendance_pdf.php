	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
<style>
body {
    color: #000000;
    direction: ltr;
    font-family: 'Open Sans';
    font-size: 13px;
	background-color: #FFF !important;
}

.portlet.box.yellow {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color #FCCB7E #FCCB7E;
    border-image: none;
    border-right: 1px solid #FCCB7E;
    border-style: none solid solid;
    border-width: 0 1px 1px;
}
.portlet.box {
    padding: 0 !important;
}
.portlet-body.yellow, .portlet.yellow {
    background-color: #FFB848 !important;
}
.portlet {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}

.caption{
    display: inline-block;
    float: left;
    font-size: 18px;
    font-weight: 400;
    margin: 0 0 7px;
    padding: 0 0 8px;
	background:#D9EDF7;
	padding:5px;
}
table th{
	background:#BF504D;
	color:#fff;
}
.title{
	background:#D9EDF7;
	padding-left:5px;
	padding-right:5px;
	padding-top:5px;
}
.main_title1{
	margin-top:5px;
}

</style>

<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');


isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';
isset($_REQUEST['session_id'])?$session_id=$_REQUEST['session_id']:$session_id='';
isset($_REQUEST['ct_sec'])?$ct_sec=$_REQUEST['ct_sec']:$ct_sec='';
isset($_REQUEST['date'])?$date=$_REQUEST['date']:$date='';
isset($_REQUEST['student_id'])?$student_id=$_REQUEST['student_id']:$student_id='';
isset($_REQUEST['a_type'])?$a_type=$_REQUEST['a_type']:$a_type='';
isset($_REQUEST['month'])?$month=$_REQUEST['month']:$month='';
isset($_REQUEST['from'])?$from=$_REQUEST['from']:$from='';
isset($_REQUEST['to'])?$to=$_REQUEST['to']:$to='';

$full_month=$month;

if(!empty($month)): 
$pos = strrpos($month, "-");

	if ($pos == true): 
	   $m_y=explode('-',$month);
	   $date=$m_y['0'];
	   $year=$m_y['1'];
	endif;   
endif;

		if(empty($from) && empty($to)):
			$to=date('Y-m-d');
			$from=date('Y-m-d',strtotime($to.' -1 months'));
		elseif(!empty($from) && empty($to)):	
			$to=date('Y-m-d',strtotime($from.' +1 months'));
		elseif(empty($from) && !empty($to)):	
			$from=date('Y-m-d',strtotime($to.' -1 months'));	
		endif;
		

		if($a_type=='half'):
			#Get Section Students
			$QueryOb=new attendance();
			$record=$QueryOb->getHalfAttendance($session_id,$from,$to);	
		elseif($a_type=='class_wise'):
			if(empty($date)): $date=date('m'); endif;
			if(empty($year)): $year=date('Y'); endif;
			
			$first_date_of_month=$year."-".$date."-01";
			$last_date_of_month=date("Y-m-d",strtotime("+1 month -1 second",strtotime(date($year."-".$date."-1"))));		
			
			#Get Section Students
			$QueryOb=new attendance();
			$record=$QueryOb->getClassWiseAttendance($school->id,$first_date_of_month,$last_date_of_month);		
		elseif($a_type=='single'):
			#Get Section Students
			$QueryOb=new studentSession();
			$record=$QueryOb->sectionSessionStudents($session_id,$ct_sec);

			if(!empty($student_id)):
				#get Student detail	
				$QueryStu = new studentSession();
				$student=$QueryStu->SingleStudentsWithSession($student_id,$session_id,$ct_sec);	
				
				#get Student Permanent Address	
				$QueryP = new studentAddress();
				$s_p_ad=$QueryP->getStudentAddressByType($student_id,'permanent');	
				
				#get Session Info	
				$session_info = get_object('session',$session_id);				
			endif;			
		else:
			#Get Section Students
			$QueryOb=new studentSession();
			$record=$QueryOb->sectionSessionStudents($session_id,$ct_sec);
		endif;
	
	$session=get_object('session',$session_id);

	#count working days
	$working_days='';
	if(!empty($date) && !empty($year)): 
		$first_date_of_month=$year."-".$date."-01";
		$last_date_of_month=date("Y-m-d",strtotime("+1 month -1 second",strtotime(date($year."-".$date."-1"))));
		
		$total_sunday=getTotalSunday($first_date_of_month,$last_date_of_month); 
		$total_month_days=(date('d',strtotime($last_date_of_month))); 
		$working_days=$total_month_days-$total_sunday;
	endif;

?>

<? if(!empty($record)):?>
  <!-- BEGIN PAGE CONTAINER-->
    <div style='margin:20px;border:1px solid;'>
            <!-- BEGIN PAGE HEADER-->

						
	<!--  Class Wise -->
	<? if($a_type=='class_wise'):?>		
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;"><br/>STATEMENT OF CLASS WISE ATTENDANCE REPORTS</td></tr>
								<tr><td class='title'></td><td class='title' style='text-align:right;'><strong>MONTH:- <?=date('M Y',strtotime('01-'.$full_month))?></strong></td></tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="hidden-480">SR. NO.</th>
											<th class="hidden-480" >SESSION NAME</th>
											<th style='text-align:center;' class="hidden-480 sorting_disabled" >TOTAL STUDENTS</th>
											<th style='text-align:center;' class="hidden-480 sorting_disabled">PRESENT</th>
											<th style='text-align:center;' class="hidden-480 sorting_disabled">ABSENT</th>
											<th style='text-align:center;' class="hidden-480 sorting_disabled">LEAVE</th>
											<th style='text-align:center;' class="hidden-480 sorting_disabled">WORKING DAYS</th>											
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';?>
												<tbody>
											<?php $sr=1;foreach($record as $sk=>$object): $checked='';?>
												<tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td><?=$object['session_name']?></td>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$object['total_student']?></td>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$object['present']?></td>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$object['absent']?></td>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$object['leave']?></td>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$object['total_days']?></td>
												</tr>
												<? $sr++;
													endforeach;?>									
												</tbody>
									   <?php endif;?>  
								</table>
	
                            </form> 									
								</div>
							</div>	
	<!--  Class Wise -->
	<? elseif($a_type=='half'):?>		
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;"><br/>More Than 50% Absent Attendance Records</td></tr>
								<tr><td class='title'></td><td class='title' style='text-align:right;'><strong>MONTH:- <?=date('M Y',strtotime('01-'.$full_month))?></strong></td></tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
									<th class="hidden-480" >ROLL No.</th>
									<th class="hidden-480" >SESSION</th>
									<th class="hidden-480" >SECTION</th>
									<th class="hidden-480" >STUDENT NAME</th>
									<th class="hidden-480" >FATHER NAME</th>
									<th class="hidden-480" >PARENT's EMAIL</th>
									<th class="hidden-480" >PARENT's CONTACT NO.</th>											
										</tr>
									</thead>
									<? if(!empty($record)): $sum='';?>
											<tbody>
											<?php $sr=1;foreach($record as $sk=>$stu): $checked='';?>
												<tr class="odd gradeX">
													<td><?=$stu->roll_no?></td>
													<td><?=$session->title?></td>
													
													<td ><?=ucfirst($stu->section)?></td>
													<td ><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
													<td><?=ucfirst($stu->father_name)?></td>
													<td><?=$stu->parents_email?></td>
													<td><?=$stu->parents_phone?></td>													
												</tr>
											<? $sr++;
												endforeach;?>									
											</tbody>
								   <?php endif;?>  
								</table>
	
                            </form> 									
								</div>
							</div>	
	<!--  Student Wise -->
	<? elseif($a_type=='single'):?>		
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:left;margin-top:15px;font-size:22px;"><br/>Student Attendance Record </td></tr>
								
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
							<!-- Attendance Table Chart-->
							<table class="table table-striped table-hover" id='student_attendance_table'>
								<tr class='brown_td'>
									<td rowspan='8' style='width:260px;background:#FFFFFF !important;'>
									<?
									if(!empty($student->photo)):
										$im_obj=new imageManipulation();?>
										<img src="<?=$im_obj->get_image('student','medium',$student->photo);?>" />
									<? else:?>
										<img src="assets/img/profile/img.jpg" alt="">
									<? endif;	
									?>
									</td>
									<td>Admission No.</td>
									<td>Date of Birth</td>
									<td>Gender</td>
									<td>Contact No.</td>									
								</tr>
								<tr>
									<td><?=$student->reg_id?></td>
									<td><?=$student->date_of_birth?></td>
									<td><?=$student->sex?></td>
									<td><?=$student->phone?></td>									
								</tr>								
								<tr class='brown_td'>
									<td>Course</td>
									<td>Name of Student</td>
									<td>Category</td>
									<td>Permanent Address</td>									
								</tr>
								<tr>
									<td><?=$session_info->title?></td>
									<td><?=$student->first_name." ".$student->last_name?></td>
									<td><?=$student->category?></td>
									<td rowspan='5'>
										<? if(is_object($s_p_ad)):
											echo $s_p_ad->address1.",".$s_p_ad->city.", ".$s_p_ad->state.", ".$s_p_ad->zip;
										endif; ?>
									</td>									
								</tr>							
								<tr class='brown_td'>
									<td>Section</td>
									<td>Father's Name</td>
									<td>Cast</td>																		
								</tr>
								<tr>
									<td><?=$student->section?></td>
									<td><?=$student->father_name?></td>
									<td><?=$student->cast?></td>																		
								</tr>
								<tr class='brown_td'>									
									<td>Roll No.</td>
									<td>Mother's Name</td>
									<td>Religion</td>																		
								</tr>
								<tr>
									<td><?=$student->roll_no?></td>
									<td><?=$student->mother_name?></td>
									<td><?=$student->religion?></td>																	
								</tr>						
							</table>
							<? 
							#set the last date of the session
							if(strtotime($session_info->end_date)>strtotime(date('Y-m-d'))):
								$last_Session_date=date('Y-m-d');
							else:
								$last_Session_date=$session_info->end_date;
							endif;
							
							$AllMonth=getMonthsArray($session_info->start_date,$last_Session_date);
							
							?>
							<table id='custom_table'>
								<tr class='brown_td'>
									<td rowspan='2' width='70%' style='text-align:center;vertical-align:middle;'> P=Present, A=Absent, L=Leave, H=Holiday</td>
									<td colspan='4' style='text-align:center;'>Attendance Table</td>									
								</tr>	
								<tr>
									<td><strong>P</strong></td>
									<td><strong>A</strong></td>
									<td><strong>L</strong></td>
									<td><strong>H</strong></td>									
								</tr>	
							
													
							<? if(!empty($AllMonth)): $P_Total='0';$A_Total='0';$L_Total='0';$H_Total='0';
									foreach($AllMonth as $m_key=>$m_val): $st='1'; $st1='1'; $P='0';$A='0';$L='0';$H='0';
										$last_date_of_month=date("d",strtotime("+1 month -1 second",strtotime(date($m_val.'-01'))));?>	
										<tr><td style='border:none;'>
										<table class="table table-striped table-hover" id='atten_inner_table'>	
										<tr><td rowspan='2' style='vertical-align: middle; width: 300px;' class='head_td'><?=date('M Y', strtotime($m_val.'-01'))?></td>
										<? 	while($st<=$last_date_of_month):
												echo "<td>".$st."</td>";
										$st++;	endwhile;
										 while($st<=31):
											echo "<td style='visibility:hidden;'>".$st."</td>";$st++;
										 endwhile;
										?>
										</tr><tr>

										<? 	while($st1<=$last_date_of_month):
													if(strlen($st1)=='1'): 
														$st1='0'.$st1;
													endif;
												#Today Attendance
												$QAttObj= new attendance();
												$tdy_att=$QAttObj->get_today_attendance($session_id,$ct_sec,$student_id,$m_val.'-'.$st1);
												
												#check Holiday
												$M_obj= new schoolHolidays();
												$holiday=$M_obj->checkHoliday($school->id,$m_val.'-'.$st1); 			
												
												if($tdy_att=='P'): $P++; $P_Total=$P_Total+1; elseif($tdy_att=='A'): $A++; $A_Total=$A_Total+1; elseif($tdy_att=='L'): $L++; $L_Total=$L_Total+1; endif;
												echo "<td>"; if($holiday=='Holiday'): echo "H"; $H++; $H_Total=$H_Total+1; else: echo $tdy_att."</td>"; endif;
										$st1++;	endwhile;
										 while($st1<=31):
											echo "<td style='visibility:hidden;'>".$st."</td>";$st1++;
										 endwhile;										
										?>		
										</tr>
										</table></td>
										<td class='head_td' style='vertical-align:middle;'><?=$P?></td>
										<td class='head_td' style='vertical-align:middle;'><?=$A?></td>
										<td class='head_td' style='vertical-align:middle;'><?=$L?></td>
										<td class='head_td' style='vertical-align:middle;'><?=$H?></td>
										</tr>
							<? 		endforeach;?>
							
							<tr>
								<td style='vertical-align:middle;border-left:none;border-top:none;border-bottom:none;text-align:right'><strong style='margin-right:5px;'>Total</strong></td>
										<td class='head_td' style='vertical-align:middle;padding:10px 1px 10px 0;' ><?=$P_Total?></td>
										<td class='head_td' style='vertical-align:middle;'><?=$A_Total?></td>
										<td class='head_td' style='vertical-align:middle;'><?=$L_Total?></td>
										<td class='head_td' style='vertical-align:middle;'><?=$H_Total?></td>
								</tr>
							</table>	
							<?	endif;?>
								
							
							</table>	
							<!-- Attendance Table End-->
	
                            								
								</div>
							</div>	

							
	<? else:?>	
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;"><br/>STATEMENT OF MONTHLY ATTENDANCE REPORT</td></tr>
								<tr><td class='title'><strong>SESSION:- <?=$session->title?></strong></td><td class='title' style='text-align:right;'><strong>MONTH:- <?=date('M Y',strtotime('01-'.$full_month))?></strong></td></tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="hidden-480">SR. NO.</th>
											<th class="hidden-480" >ROLL No.</th>
											<th class="hidden-480" >SECTION</th>
											<th class="hidden-480" >STUDENT NAME</th>
											<th class="hidden-480" >FATHER NAME</th>
											<th style='text-align:center;' class="hidden-480 sorting_disabled">PRESENT</th>
											<th style='text-align:center;' class="hidden-480 sorting_disabled">ABSENT</th>
											<th style='text-align:center;' class="hidden-480 sorting_disabled">LEAVE</th>
											<th style='text-align:center;' class="hidden-480 sorting_disabled">WORKING DAYS</th>
											<th style='text-align:center;' class="hidden-480 sorting_disabled">%AGE</th>											
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';?>
												<tbody>
												<?php $sr=1;foreach($record as $sk=>$stu): $checked='';																			
													$Q_obj= new attendance();
													$r_atten=$Q_obj->checkSessionSectionMonthYearAttendace($session_id,$ct_sec,$stu->id,$date,$year);
													$t_attend=array_sum($r_atten);	
													
													$M_obj= new schoolHolidays();
													$M_holiday=$M_obj->checkMonthlyHoliday($school->id,$date,$year);
												?>
													<tr class="odd gradeX">
														<td><?=$sr?>.</td>
														<td><?=$stu->roll_no?></td>
														<td ><?=ucfirst($stu->section)?></td>
														<td ><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
														<td><?=ucfirst($stu->father_name)?></td>
														<td class="hidden-480 sorting_disabled" style='text-align:center;'><? if(!empty($r_atten) && array_key_exists('P',$r_atten)): echo $present=$r_atten['P']; else: echo $present='0'; endif;?></td>
														<td class="hidden-480 sorting_disabled" style='text-align:center;'><? if(!empty($r_atten) && array_key_exists('A',$r_atten)): echo $r_atten['A']; else: echo '0';endif;?></td>
														<td class="hidden-480 sorting_disabled" style='text-align:center;'><? if(!empty($r_atten) && array_key_exists('L',$r_atten)): echo $r_atten['L']; else: echo '0';endif;?></td>
														<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=($working_days-$M_holiday)?></td>
														<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=number_format(($present/($working_days-$M_holiday))*100,2)?>%</td>																				
													</tr>
												<? $sr++;
													endforeach;?>									
												</tbody>
									   <?php endif;?>  
								</table>
	
                            </form> 									
								</div>
							</div>
						
		<? endif;?>				
	
    </div>
<? endif;?>	