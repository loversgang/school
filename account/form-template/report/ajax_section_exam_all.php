<?php
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');


isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = 'A';
isset($_REQUEST['interval_id']) ? $interval_id = $_REQUEST['interval_id'] : $interval_id = '';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
isset($_REQUEST['report_type']) ? $report_type = $_REQUEST['report_type'] : $report_type = 'admission_detail';
isset($_REQUEST['date']) ? $date = $_REQUEST['date'] : $date = '';
isset($_REQUEST['date1']) ? $date1 = $_REQUEST['date1'] : $date1 = '0';
isset($_REQUEST['month']) ? $month = $_REQUEST['month'] : $month = '';
isset($_REQUEST['exam']) ? $exam = $_REQUEST['exam'] : $exam = '0';
isset($_REQUEST['v_report_type']) ? $v_report_type = $_REQUEST['v_report_type'] : $v_report_type = 'vehicle_info';
isset($_REQUEST['s_report_type']) ? $s_report_type = $_REQUEST['s_report_type'] : $s_report_type = 'recieve';
isset($_REQUEST['f_report_type']) ? $f_report_type = $_REQUEST['f_report_type'] : $f_report_type = 'recieve';
isset($_REQUEST['e_report_type']) ? $e_report_type = $_REQUEST['e_report_type'] : $e_report_type = 'live';
isset($_REQUEST['ex_type']) ? $ex_type = $_REQUEST['ex_type'] : $ex_type = '';
isset($_REQUEST['a_type']) ? $a_type = $_REQUEST['a_type'] : $a_type = '';
isset($_REQUEST['f_type']) ? $f_type = $_REQUEST['f_type'] : $f_type = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['subject']) ? $subject = $_REQUEST['subject'] : $subject = '';

isset($_REQUEST['tmp']) ? $tmp = $_REQUEST['tmp'] : $tmp = 'student';


isset($_REQUEST['from']) ? $from = $_REQUEST['from'] : $from = '';
isset($_REQUEST['to']) ? $to = $_REQUEST['to'] : $to = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = '';

if (isset($_POST)) {
    extract($_POST);
    ?>
    <style>
        .print_tables table {
            width: 100%;
            border: 1px solid #000;
        }
        .print_tables td {
            border-top: 1px solid #000;
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables th {
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables table tr td:last-child {
            border-right: none;
        }
        .print_tables table tr th:last-child {
            border-right: none;
        }
    </style>
    <div class="print_tables">
        <?php
        #Get exam Grades
        $QueryGrOb = new examGrade();
        $all_grade = $QueryGrOb->listAll($school->id);

        #get Session Info	
        $session_info = get_object('session', $session_id);
        if ($ex_type == 'term'):
            #groups
            $QueryObjExam = new examinationGroup();
            $groups = $QueryObjExam->getTermExamSessionSection($session_id, $ct_sec);
            if (!empty($groups) && empty($exam)):
                $exam = $groups['0']->id;
            endif;

            #get exam Info
            $QueryG = new examinationGroup();
            $group = $QueryG->getRecord($exam);

            if (!is_object($group)): $group->exam_id = '0';
            endif;
            #get Subject names
            $QueryExam = new examination();
            $exams = $QueryExam->groupExaminations($group->exam_id);

            #Get exam Students result
            $QueryOb = new examinationMarks();
            $record = $QueryOb->getExamStudents($group->exam_id);
            ?>
            <center><h1>Term Exam Marks Details</h1></center>
            <table>
                <tr>
                    <th class="hidden-480" style='vertical-align:middle;'>Roll No.</th>
                    <th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>Name</th>
                    <th class="hidden-480 sorting_disabled" style='vertical-align:middle;'>Father Name</th>
                    <?
                    if (!empty($exams)):
                        foreach ($exams as $e => $v):
                            ?>
                            <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?= $v->subject ?><br/>
                        <div class='span6' style='text-align: center;display: inline;'>Total</div> <div class='span6' style='text-align: center;display: inline;margin-left:0px;'>Obtain</div> 
                        </th>
                    <? endforeach; ?>
                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>All Subjects<br/>
                    <div style="text-align: center;display: inline;"><span style="margin-right: 3px">Total</span></div> <div class='span6' style='text-align: center;display: inline;margin-left:0px;'>Obtain</div> 
                    </th>	
                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>Position<br/>
                    <div class='span6' style='text-align: center;display: inline;margin-left:0px;'>%Age</div> <div class='span6' style='text-align: center;display: inline;margin-left:0px;'>Grade</div> 
                    </th>													
                <? endif; ?>

                </tr>
                <?
                if (!empty($record)):
                    $sr = 1;
                    foreach ($record as $sk => $stu): $checked = '';
                        ?>
                        <tr>
                            <td class="hidden-480 main_title"><?= $stu->roll_no ?></td>
                            <td class="hidden-480 main_title"><?= $stu->first_name . " " . $stu->last_name ?></td>
                            <td class='main_title'><?= ucfirst($stu->father_name) ?></td>
                            <?
                            if (!empty($exams)): $tm = '';
                                $om = '';
                                foreach ($exams as $ee => $vv):
                                    #get Student result
                                    $QueryRes = new examinationMarks();
                                    $Result = $QueryRes->get_student_exam_marks($vv->id, $stu->id);
                                    if (is_object($Result)):
                                        ?> 
                                        <td style='text-align:center' class="hidden-480"><?= $vv->maximum_marks ?></td>
                                        <td style='text-align:center' class="hidden-480"><?= $Result->marks_obtained ?></td>
                                        <?
                                        $tm = $tm + $vv->maximum_marks;
                                        $om = $om + $Result->marks_obtained;
                                    else:
                                        echo "<td></td><td></td>";
                                    endif;
                                endforeach;
                            endif;
                            ?>
                            <td style='text-align:center' class="hidden-480"><?= $tm ?></td>
                            <td style='text-align:center' class="hidden-480"><?= $om ?></td>
                            <?
                            if (!empty($all_grade)): $grade = '';
                                foreach ($all_grade as $kg => $kv):
                                    if (((($om / $tm) * 100) >= $kv->minimum) && ((($om / $tm) * 100) < $kv->maximum)): $grade = $kv->title;
                                    endif;
                                endforeach;
                                ?>
                                <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= number_format(($om / $tm) * 100, 2) ?>%</td>
                                <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $grade ?></td>
                            <? endif; ?>													
                        </tr>
                        <?
                    endforeach;
                endif;
                ?>
            </table>
            <?php
        elseif ($ex_type == 'subject'):
            #here subject will be the exam id and exam will be the group;
            if (!empty($subject)):
                #Get exam Students result
                $QueryOb = new examinationMarks();
                $record = $QueryOb->getExamStudents($subject);

                #get exam group Info
                $QueryG = new examinationGroup();
                $group = $QueryG->getRecord($exam);

                #get Exam detail with its subject
                $QueryExam = new examination();
                $exams = $QueryExam->groupExaminations($group->exam_id);
            endif;

            #All groups
            $QueryObjExam = new examinationGroup();
            $groups = $QueryObjExam->getTermExamSessionSection($session_id, $ct_sec);

            if (!empty($groups) && empty($exam)):
                $exam = $groups['0']->id;
            endif;

            if (!empty($exam)):
                $group_detail = get_object('student_examination_group', $exam);

                #get Session Info
                $QueryExsub = new examinationGroup();
                $AllSubject = $QueryExsub->getSubjectByExams($group_detail->exam_id);

            endif;
            ?>
            <center><h1>Term Subjects Wise Marks Details</h1></center>
            <table>
                <tr>
                    <th>Roll No.</th>
                    <th>Name</th>
                    <th>Father Name</th>
                    <?
                    if (!empty($exams)):
                        foreach ($exams as $e => $v):
                            if ($v->id == $subject):
                                ?>
                                <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?= $v->subject ?><br/>
                            <div class='span6' style='margin-left:0px;display: inline;'>Total</div> <div class='span6' style='display: inline;margin-left:0px;'>Obtain</div> 
                            </th>
                            <?
                        endif;
                    endforeach;
                    ?>
                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>All Subjects<br/>
                    <div class='span6' style='display: inline;margin-left:0px;'>Total</div> <div class='span6' style='display: inline;margin-left:0px;'>Obtain</div> 
                    </th>
                    <? if (!empty($all_grade)): $grade = ''; ?>													
                        <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>Position<br/>
                        <div class='span6' style='margin-left:0px;display: inline;'>%Age</div> <div class='span6' style='display: inline;margin-left:0px;'>Grade</div> 
                        </th>													
                        <?
                    endif;
                endif;
                ?>
                </tr>
                <?
                if (!empty($record)):
                    $sr = 1;
                    foreach ($record as $sk => $stu): $checked = '';
                        ?>
                        <tr>
                            <td><?= $stu->roll_no ?></td>
                            <td><?= $stu->first_name . " " . $stu->last_name ?></td>
                            <td><?= ucfirst($stu->father_name) ?></td>
                            <?
                            if (!empty($exams)): $tm = '';
                                $om = '';
                                foreach ($exams as $ee => $vv):
                                    if ($vv->id == $subject):
                                        #get Student result
                                        $QueryRes = new examinationMarks();
                                        $Result = $QueryRes->get_student_exam_marks($vv->id, $stu->id);
                                        if (is_object($Result)):
                                            ?> 
                                            <td><?= $vv->maximum_marks ?></td>
                                            <td><?= $Result->marks_obtained ?></td>
                                            <?
                                            $tm = $tm + $vv->maximum_marks;
                                            $om = $om + $Result->marks_obtained;
                                        else:
                                            echo "<td></td><td></td>";
                                        endif;
                                    endif;
                                endforeach;
                            endif;
                            ?>
                            <td><?= $tm ?></td>
                            <td><?= $om ?></td>
                            <?
                            if (!empty($all_grade)): $grade = '';
                                foreach ($all_grade as $kg => $kv):
                                    if (((($om / $tm) * 100) >= $kv->minimum) && ((($om / $tm) * 100) < $kv->maximum)): $grade = $kv->title;
                                    endif;
                                endforeach;
                                ?>												
                                <td><?= number_format(($om / $tm) * 100, 2) ?>%</td>
                                <td><?= $grade ?></td>												
                            <? endif; ?>
                        </tr>
                        <?
                    endforeach;
                endif;
                ?>
            </table>
            <?php
        elseif ($ex_type == 'term_subject'):
            #Get Subject List
            $QuerySubject = new subject();
            $sessionSubjects = $QuerySubject->listSessionSubject($session_info->compulsory_subjects, $session_info->elective_subjects, '1', 'array');

            if (empty($subject)):
                $subject = $sessionSubjects['0']->id;
            endif;

            $subject_detail = get_object('subject_master', $subject);

            #get exam groups whose subject id is selected
            $QueryG = new examinationGroup();
            $record = $QueryG->getSelectedSubjectGroup($session_id, $ct_sec, $subject);
            ?>
            <center><h1>Term Subjects Wise Marks Details</h1></center>
            <table>
                <tr>
                    <th>Roll No.</th>
                    <th>Name</th>
                    <th>Father Name</th>
                    <?
                    if (!empty($record['terms'])):
                        foreach ($record['terms'] as $e => $v):
                            ?>
                            <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?= $v ?> (<?= $subject_detail->name ?>)<br/>
                        <div class='span6' style='display: inline;margin-left:0px;'>Total</div> <div class='span6' style='display: inline;margin-left:0px;'>Obtain</div> 
                        </th>
                    <? endforeach; ?>
                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>All Term<br/>
                    <div class='span6' style='display: inline;margin-left:0px;'>Total</div> <div class='span6' style='display: inline;margin-left:0px;'>Obtain</div> 
                    </th>	
                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>Position<br/>
                    <div class='span6' style='display: inline;margin-left:0px;'>%Age</div> <div class='span6' style='display: inline;margin-left:0px;'>Grade</div> 
                    </th>													
                <? endif; ?>
                </tr>
                <?
                if (!empty($record['record'])):
                    $sr = 1;
                    foreach ($record['record'] as $sk => $stu): $checked = '';
                        ?>																		
                        <tr>
                            <td><?= $stu->roll_no ?></td>
                            <td><?= $stu->first_name . " " . $stu->last_name ?></td>
                            <td><?= ucfirst($stu->father_name) ?></td>
                            <?
                            if (!empty($record['terms'])): $tm = '';
                                $om = '';
                                foreach ($record['terms'] as $ee => $vv):
                                    #get Student result
                                    $QueryRes = new examinationMarks();
                                    $Result = $QueryRes->getStudentResult($ee, $stu->id);
                                    if (is_object($Result)):
                                        ?> 
                                        <td><?= $Result->maximum_marks ?></td>
                                        <td><?= $Result->marks_obtained ?></td>
                                        <?
                                        $tm = $tm + $Result->maximum_marks;
                                        $om = $om + $Result->marks_obtained;
                                    else:
                                        echo "<td></td><td></td>";
                                    endif;
                                endforeach;
                            endif;
                            ?>
                            <td><?= $tm ?></td>
                            <td><?= $om ?></td>
                            <td><?= number_format($om / $tm * 100, 2) ?>%</td>
                            <?
                            if (!empty($all_grade)): $grade = '';
                                foreach ($all_grade as $kg => $kv):
                                    if (((($om / $tm) * 100) >= $kv->minimum) && ((($om / $tm) * 100) < $kv->maximum)): $grade = $kv->title;
                                    endif;
                                endforeach;
                                ?>
                                <td><?= $grade ?></td>
                        <? endif; ?>												
                        </tr>
                    <? endforeach;
                endif;
                ?>
            </table>
            <?php
        elseif ($ex_type == 'all'):
            $all_exam_array = array();

            #get exam groups whose subject id is selected
            $QueryG = new examinationGroup();
            $groups = $QueryG->getTermExamSessionSection($session_id, $ct_sec);

            if (!empty($groups)):
                foreach ($groups as $keyy => $vall):
                    $all_exam_array[] = $vall->exam_id;
                endforeach;
            endif;
            $all_exams = implode(',', $all_exam_array);

            if (empty($all_exams)):
                $all_exams = '0';
            endif;

            #Get exam Students result
            $QueryOb = new examinationMarks();
            $record = $QueryOb->getExamStudents($all_exams);
            ?>
            <center><h1>All Terms Subject Wise Marks Details</h1></center>
            <table>
                <tr>
                    <th>Roll No.</th>
                    <th>Name</th>
                    <th>Father Name</th>
                    <?
                    if (!empty($groups)):
                        foreach ($groups as $e => $v):
                            ?>
                            <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'><?= $v->title ?><br/>
                        <div class='span6' style='display: inline;margin-left:0px;'>Total</div> <div class='span6' style='display: inline;margin-left:0px;'>Obtain</div> 
                        </th>
            <? endforeach; ?>
                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>All Terms<br/>
                    <div class='span6' style='display: inline;margin-left:0px;'>Total</div> <div class='span6' style='display: inline;margin-left:0px;'>Obtain</div> 
                    </th>	
                    <th colspan='2' class="hidden-480 sorting_disabled" style='text-align:center;'>Position<br/>
                    <div class='span6' style='display: inline;margin-left:0px;'>%Age</div> <div class='span6' style='display: inline;margin-left:0px;'>Grade</div> 
                    </th>														
                <? endif; ?>
                </tr>
                <?
                if (!empty($record)):
                    $sr = 1;
                    foreach ($record as $sk => $stu): $checked = '';
                        ?>
                        <tr>
                            <td><?= $stu->roll_no ?></td>
                            <td><?= $stu->first_name . " " . $stu->last_name ?></td>
                            <td><?= ucfirst($stu->father_name) ?></td>
                            <?php
                            if (!empty($groups)): $tm = '';
                                $om = '';
                                foreach ($groups as $ee => $vv):
                                    #get Student result
                                    $QueryRes = new examinationMarks();
                                    $Result = $QueryRes->get_student_exam_marks_in_multpale($vv->exam_id, $stu->id);
                                    if (!empty($Result)):
                                        ?> 
                                        <td><?= $Result['total'] ?></td>
                                        <td><?= $Result['obtain'] ?></td>
                                        <?php
                                        $tm = $tm + $Result['total'];
                                        $om = $om + $Result['obtain'];
                                    else:
                                        echo "<td></td><td></td>";
                                    endif;
                                endforeach;
                            endif;
                            ?>
                            <td><?= $tm ?></td>
                            <td><?= $om ?></td>
                            <?php
                            if (!empty($all_grade)): $grade = '';
                                foreach ($all_grade as $kg => $kv):
                                    if (((($om / $tm) * 100) >= $kv->minimum) && ((($om / $tm) * 100) < $kv->maximum)): $grade = $kv->title;
                                    endif;
                                endforeach;
                                ?>
                                <td><?= number_format(($om / $tm) * 100, 2) ?>%</td>
                                <td><?= $grade ?></td>
                        <? endif; ?>					
                        </tr>
                        <?
                    endforeach;
                endif;
                ?>
            </table>
            <?php
        endif;
        ?>
    </div>
<?php } ?>