<?
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');

if ($_POST):
    isset($_POST['id']) ? $id = $_POST['id'] : $id = '';
    isset($_POST['ct_sec']) ? $ct_sec = $_POST['ct_sec'] : $ct_sec = 'A';

    /* Get session Pages */
    $QuerySec = new studentSession();
    $QuerySec->listAllSessionSection($id);

    #get Session Info
    $QueryS = new session();
    $session = $QueryS->getRecord($id);

    #Get Subject List
    $QuerySubject = new subject();
    $sessionSubjects = $QuerySubject->listSessionSubject($session->compulsory_subjects, $session->elective_subjects, '1', 'array');
    ?>


    <? if ($QuerySec->GetNumRows() > 0): ?>
        <div class='span4' style='margin-left: 5px;'>
            <label class="control-label">Select Section </label>
            <div class="controls">
                <select class="exam_section_filter span8" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                    <?php $session_sc = 1;
                    while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                        <option value='<?= $sec->section ?>' <? if ($ct_sec == $sec->section): echo 'selected';
            endif; ?>><?= ucfirst($sec->section) ?></option>
            <? $session_sc++;
        endwhile; ?>
                </select>
            </div>
        </div>	
        <div class='span7' style='margin-left: 5px;' >
            <label class="control-label">Select Subject</label>
            <div class="controls">
                    <? if (!empty($sessionSubjects)): ?>
                    <select class="span11" name='subject' id='exam'>
                        <?php foreach ($sessionSubjects as $s_key => $s_val): ?>
                            <option value='<?= $s_val->id ?>'><?= ucfirst($s_val->name) ?></option>
                    <?php endforeach; ?>
                    </select>
                <? else: ?>	
                    <input type='text' class='span11' value='Sorry, No Subject Found.' disabled />	
        <? endif; ?>
            </div>
        </div>				
    <? else: ?>
        <div class='span5'>
            <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                <label class="control-label">Select Section </label>
                <div class="controls">						
                    <input type='text' value='Sorry, No Section Found.' disabled />	
                </div>
            </div>
        </div>
    <? endif; ?>					

    <?
endif;
?>