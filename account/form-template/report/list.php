
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Reports
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 
                                   
                                    <li class="last">
                                        Manage Reports
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

	
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="span12"><br/>					
						<div class="row-fluid" id='custom_dashboard'>
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('report','enquiry','enquiry')?>"/>
										<img src="assets/img/icon/EXM.png"/>
									</a>
									<a href="<?=make_admin_url('report','enquiry','enquiry')?>" class="title">Enquiry Reports</a>
								</div>
							</div>						
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('report','student','student')?>"/>
										<img src="assets/img/icon/EXM2.png"/>
									</a>	
									<a href="<?=make_admin_url('report','student','student')?>" class="title">Student Reports</a>
								</div>
							</div>
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('report','attendance','attendance')?>"/>	
										<img src="assets/img/icon/attendence.png"/>
									</a>
									<a href="<?=make_admin_url('report','attendance','attendance')?>" class="title">Attendance Reports</a>
								</div>
							</div>
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('report','exam','exam')?>"/>
										<img src="assets/img/icon/exams-icon.png"/>
									</a>
									<a href="<?=make_admin_url('report','exam','exam')?>" class="title">Examination Reports</a>
								</div>
							</div>
						</div>
						
						<div class="row-fluid" id='custom_dashboard'>
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('report','fee','fee')?>"/>
										<img src="assets/img/icon/Finance.png"/>
									</a>
									<a href="<?=make_admin_url('report','fee','fee')?>" class="title">Fee Reports</a>
								</div>
							</div>						
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('report','salary','salary')?>"/>
										<img src="assets/img/icon/human_resource.png"/>
									</a>
									<a href="<?=make_admin_url('report','salary','salary')?>" class="title">Salary Reports</a>
								</div>
							</div>						
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('report','vehicle','vehicle')?>"/>
										<img src="assets/img/icon/transport.png"/>
									</a>
									<a href="<?=make_admin_url('report','vehicle','vehicle')?>" class="title">Vehicle Reports</a>
								</div>
							</div>
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('account','list','list')?>"/>
										<img src="assets/img/icon/EXM6.png"/>
									</a>
									<a href="<?=make_admin_url('account','list','list')?>" class="title">Expense Reports</a>
								</div>
							</div>

						</div>
						
						<div class="row-fluid" id='custom_dashboard'>
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('account','update','update')?>"/>
										<img src="assets/img/icon/manage_users.png"/>
									</a>
									<a href="<?=make_admin_url('account','update','update')?>" class="title">Income Reports</a>
								</div>
							</div>						
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('account','report','report')?>"/>
										<img src="assets/img/icon/EXM4.png"/>
									</a>
									<a href="<?=make_admin_url('account','report','report')?>" class="title">Full Reports</a>
								</div>
							</div>	
							
							<div class='span3'>
								<div class="easy-pie-chart">
									<a href="<?=make_admin_url('report','daily','daily')?>"/>
										<img src="assets/img/icon/expense-2.png"/>
									</a>
									<a href="<?=make_admin_url('report','daily','daily')?>" class="title">Daily Collection Report</a>
								</div>
							</div>								
						</div>
						
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
	