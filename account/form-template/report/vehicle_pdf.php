	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
<style>
body {
    color: #000000;
    direction: ltr;
    font-family: 'Open Sans';
    font-size: 13px;
	background-color: #FFF !important;
}

.portlet.box.yellow {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color #FCCB7E #FCCB7E;
    border-image: none;
    border-right: 1px solid #FCCB7E;
    border-style: none solid solid;
    border-width: 0 1px 1px;
}
.portlet.box {
    padding: 0 !important;
}
.portlet-body.yellow, .portlet.yellow {
    background-color: #FFB848 !important;
}
.portlet {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}

.caption{
    display: inline-block;
    float: left;
    font-size: 18px;
    font-weight: 400;
    margin: 0 0 7px;
    padding: 0 0 8px;
	background:#D9EDF7;
	padding:5px;
}
table th{
	background:#BF504D;
	color:#fff;	
}
.table thead tr th {
    font-size: 11px !important;
    font-weight: 600;
}
.title{
	background:#D9EDF7;
	padding-left:5px;
	padding-right:5px;
	padding-top:5px;
}
.main_title1{
	margin-top:5px;
}
#sum_amt{
	background:#FFFF00;
	color:#FF1B00;
	font-weight:bold;
}
</style>

<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');


isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';

isset($_REQUEST['v_report_type'])?$v_report_type=$_REQUEST['v_report_type']:$v_report_type='vehicle_info';

	/* Get vehicle Pages*/
	$QueryObj=new vehicle();
	$record=$QueryObj->listAll($school->id,'1','array');

?>

<? if(!empty($record)):?>
  <!-- BEGIN PAGE CONTAINER-->
    <div style='margin:20px;border:1px solid;'>
            <!-- BEGIN PAGE HEADER-->

						
					<? if($v_report_type=="vehicle_info"):?>	
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:right;margin-top:15px;font-size:22px;padding:5px;"><br/>STATEMENT OF VEHICLE INFORMATION</td></tr>
								<tr><td class='title'><strong>SCHOOL:- <?=$school->school_name?></strong></td>
								</tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
								
								<table class="table table-striped table-bordered table-hover">
									<thead>
										 <tr>
												<th>Sr. No.</th>
												<th>Model</th>
												<th style='text-align:center' class="hidden-480">Company</th>
												<th style='text-align:center' class="hidden-480">Vehicle Number</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Color</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Vehicle Type</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Purchase Date</th>
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';?>                                        
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):?>
                                                <tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td><?=$object->model?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->make?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->vehicle_number?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->color?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->type?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->purchase_date?></td>													
												</tr>
                                            <?php $sr++; 
												endforeach;?>
										</tbody>
									
									   <?php endif;?>  
									</table>				
								</div>
							</div>
					<? elseif($v_report_type=="vendor"):?>	
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:right;margin-top:15px;font-size:22px;padding:5px;"><br/>STATEMENT OF VEHICLE VENDOR OR OWNED</td></tr>
								<tr><td class='title'><strong>SCHOOL:- <?=$school->school_name?></strong></td>
								</tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
								
								<table class="table table-striped table-bordered table-hover">
									<thead>
										 <tr>
												<th>Sr. No.</th>
												<th>Model</th>
												<th style='text-align:center' class="hidden-480">Company</th>
												<th style='text-align:center' class="hidden-480">Vehicle Number</th>
												<th style='text-align:center' class="hidden-480">Vendor OR Owned</th>
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';?>                                        
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):?>
                                                <tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td><?=$object->model?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->make?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->vehicle_number?></td>
													<td style='text-align:center' class="hidden-480"><? if($object->vendor==""): echo "Owned"; else: echo $object->vendor; endif;?></td>
												</tr>
                                            <?php $sr++; 
												endforeach;?>
										</tbody>
										<?php endif;?> 
										
					<? elseif($v_report_type=="vendor_info"):?>	
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:right;margin-top:15px;font-size:22px;padding:5px;"><br/>STATEMENT OF VEHICLE VENDOR INFORMATION</td></tr>
								<tr><td class='title'><strong>SCHOOL:- <?=$school->school_name?></strong></td>
								</tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
								
								<table class="table table-striped table-bordered table-hover">
									<thead>
										 <tr>
												<th>Sr. No.</th>
												<th>Model</th>
												<th style='text-align:center' class="hidden-480">Company</th>
												<th style='text-align:center' class="hidden-480">Vehicle Number</th>
												<th style='text-align:center' class="hidden-480">Vendor Information</th>
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';?>                                        
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):
											    if($object->vendor!='Owned'):?>
													<tr class="odd gradeX">
														<td><?=$sr?>.</td>
														<td><?=$object->model?></td>
														<td style='text-align:center' class="hidden-480"><?=$object->make?></td>
														<td style='text-align:center' class="hidden-480"><?=$object->vehicle_number?></td>
														<td style='text-align:center' class="hidden-480"><?=$object->vendor;?></td>
													</tr>
												<?php 
												$sr++; 
													endif;
												endforeach;
												?>
										</tbody>										
									   <?php endif;?>  
									</table>				
								</div>
							</div>	
					<? elseif($v_report_type=="vendor_reg"):?>	
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:right;margin-top:15px;font-size:22px;padding:5px;"><br/>STATEMENT OF VEHICLE REGISTRATION</td></tr>
								<tr><td class='title'><strong>SCHOOL:- <?=$school->school_name?></strong></td>
								</tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
								
								<table class="table table-striped table-bordered table-hover">
									<thead>
										 <tr>
												<th>Sr. No.</th>
												<th>Model</th>
												<th style='text-align:center' class="hidden-480">Company</th>
												<th style='text-align:center' class="hidden-480">Vehicle Number</th>
												<th style='text-align:center' class="hidden-480">Seating Capacity</th>
												<th style='text-align:center' class="hidden-480">Insurance</th>
										 </tr>
									</thead>
                                        <? if(!empty($record)): $sum='';?>                                        
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):?>
                                                <tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td><?=$object->model?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->make?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->vehicle_number?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->seating_capacity?></td>
													<td style='text-align:center' class="hidden-480"><?=$object->insurance?></td>
												</tr>
                                            <?php $sr++; 
												endforeach;?>
										</tbody>										
									   <?php endif;?>  
									</table>				
								</div>
							</div>								
					<?php endif;?> 					
	
    </div>
<? endif;?>	