	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
<style>
body {
    color: #000000;
    direction: ltr;
    font-family: 'Open Sans';
    font-size: 13px;
	background-color: #FFF !important;
}

.portlet.box.yellow {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color #FCCB7E #FCCB7E;
    border-image: none;
    border-right: 1px solid #FCCB7E;
    border-style: none solid solid;
    border-width: 0 1px 1px;
}
.portlet.box {
    padding: 0 !important;
}
.portlet-body.yellow, .portlet.yellow {
    background-color: #FFB848 !important;
}
.portlet {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}

.caption{
    display: inline-block;
    float: left;
    font-size: 18px;
    font-weight: 400;
    margin: 0 0 7px;
    padding: 0 0 8px;
	background:#D9EDF7;
	padding:5px;
}
table th{
	background:#BF504D;
	color:#fff;	
}
.table thead tr th {
    font-size: 11px !important;
    font-weight: 600;
}
.title{
	background:#D9EDF7;
	padding-left:5px;
	padding-right:5px;
	padding-top:5px;
}
.main_title1{
	margin-top:5px;
}
#sum_amt{
	background:#FFFF00;
	color:#FF1B00;
	font-weight:bold;
}
</style>

<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');


isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';
isset($_REQUEST['cat'])?$cat=$_REQUEST['cat']:$cat='';

isset($_REQUEST['month'])?$month=$_REQUEST['month']:$month='';
isset($_REQUEST['s_report_type'])?$s_report_type=$_REQUEST['s_report_type']:$s_report_type='recieve';

	$category=get_object('staff_category_master',$cat);

	#Get Records for salary
	$QueryObj = new staffSalary();
	$record=$QueryObj->SalaryReportMonthWise($school->id,$month,$s_report_type,$cat);
	
		$first_date_of_month= date('Y-m-d', strtotime('01-'.$month));
		$last_date_of_month=date("Y-m-d",strtotime("+1 month -1 second",strtotime(date($first_date_of_month))));
	
		$total_sunday=getTotalSunday($first_date_of_month,$last_date_of_month); 
		$total_month_days=(date('d',strtotime($last_date_of_month))); 
		$working_days=$total_month_days-$total_sunday;
			
		$M_obj= new schoolHolidays();
		$M_holiday=$M_obj->checkMonthlyHoliday($school->id,date('m',strtotime($first_date_of_month)),date('Y',strtotime($first_date_of_month)));	

?>

<? if(!empty($record)):?>
  <!-- BEGIN PAGE CONTAINER-->
    <div style='margin:20px;border:1px solid;'>
            <!-- BEGIN PAGE HEADER-->

						
					<? if($s_report_type=="recieve"):?>	
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:right;margin-top:15px;font-size:22px;padding:5px;"><br/>STATEMENT OF PAID SALARY</td></tr>
								<tr><td class='title'><strong>SCHOOL:- <?=$school->school_name?></strong></td>
								<td class='title' style='text-align:right;'><strong>MONTH & YEAR:- <?=date('M, Y',strtotime("01-".$month))?></strong></td>
								</tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
								
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
												<th>SR. NO.</th>
												<th>STAFF ID</th>
												<th>STAFF NAME</th>
												<th>STAFF CATEGORY</th>
												<th>STAFF DESIGNATION</th>
												<th>PRESENT</th>
												<th>ABSENT</th>
												<th>LEAVE</th>
												<th>HALF DAY</th>
												<th style='text-align:center'>PAYMENT DATE</th>
												<th style='text-align:right;'>DEDUCTION</th>
												<th style='text-align:right;'>SALARY</th>										
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum=''; $ded='';?>                                        
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):
													$Q_obj= new staffAttendance();
													$r_atten=$Q_obj->GetStaffAttendace($object->id,$first_date_of_month,$last_date_of_month);
													$t_attend=array_sum($r_atten);		
													$desi=get_object('staff_designation_master',$object->designation);	
											?>
                                                <tr class="odd gradeX">	
													<td><?=$sr?>.</td>	
													<td><?=$object->id?></td>														
													<td><?=$object->title.' '.ucfirst($object->first_name).' '.$object->last_name?></td>
													<td class="hidden-480 sorting_disabled"><?=$object->category?></td>
													<td class="hidden-480 sorting_disabled"><?=$desi->name?></td>
													<td style='text-align:center'><? if(!empty($r_atten) && array_key_exists('P',$r_atten)): echo $r_atten['P']; else: echo '0'; endif;?></td>
													<td style='text-align:center'><? if(!empty($r_atten) && array_key_exists('A',$r_atten)): echo $r_atten['A']; else: echo '0'; endif;?></td>
													<td style='text-align:center'><? if(!empty($r_atten) && array_key_exists('L',$r_atten)): echo $r_atten['L']; else: echo '0'; endif;?></td>
													<td style='text-align:center'><? if(!empty($r_atten) && array_key_exists('H',$r_atten)): echo $r_atten['H']; else: echo '0'; endif;?></td>
													<td style='text-align:center' class="hidden-480 sorting_disabled"><?=$object->payment_date?></td>
													<td style='text-align:right;'><?=number_format($object->deduction,2)?></td>
													<td style='text-align:right;'><?=number_format($object->amount,2)?></td>													
												</tr>
                                            <?php $sr++; $sum=$sum+$object->amount; $ded=$ded+$object->deduction;
												endforeach;?>
												<tr><td></td><td></td><td></td><td><td></td><td></td><td><td></td><td></td><td> TOTAL </td>
												<td id='sum_amt' style='text-align:right;'><?=number_format($ded,2)?></td>
												<td id='sum_amt' style='text-align:right;'><?=number_format($sum,2)?></td>
										</tbody>
									
									   <?php endif;?>  
									</table>				
								</div>
							</div>

					<? else:?>	
							<div class="portlet-body">
							
							<table width='100%'>
								<tr><td colspan='2' class='main_title1'  style="text-align:right;margin-top:15px;font-size:22px;padding:5px;"><br/>STATEMENT OF PENDING SALARY</td></tr>
								<tr><td class='title'><strong>SCHOOL:- <?=$school->school_name?></strong></td>
								<td class='title' style='text-align:right;'><strong>MONTH & YEAR:- <?=date('M, Y',strtotime("01-".$month))?></strong></td>
								</tr>
							</table>
							<div style='clear:both;'></div>							
								<div class="clearfix">
								
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>SR. NO.</th>
											<th>STAFF ID</th>
											<th>STAFF NAME</th>
											<th>STAFF CATEGORY</th>
											<th>STAFF DESIGNATION</th>
											<th>PRESENT</th>
											<th>ABSENT</th>
											<th>LEAVE</th>
											<th>HALF DAY</th>
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';?>                                        
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):
													$Q_obj= new staffAttendance();
													$r_atten=$Q_obj->GetStaffAttendace($object->id,$first_date_of_month,$last_date_of_month);
													$t_attend=array_sum($r_atten);		
													$desi=get_object('staff_designation_master',$object->designation);	
											?>
                                                <tr class="odd gradeX">	
													<td><?=$sr?>.</td>	
													<td><?=$object->id?></td>														
													<td><?=$object->title.' '.ucfirst($object->first_name).' '.$object->last_name?></td>
													<td class="hidden-480 sorting_disabled"><?=$object->category?></td>
													<td class="hidden-480 sorting_disabled"><?=$desi->name?></td>
													<td style='text-align:center'><? if(!empty($r_atten) && array_key_exists('P',$r_atten)): echo $r_atten['P']; else: echo '0'; endif;?></td>
													<td style='text-align:center'><? if(!empty($r_atten) && array_key_exists('A',$r_atten)): echo $r_atten['A']; else: echo '0'; endif;?></td>
													<td style='text-align:center'><? if(!empty($r_atten) && array_key_exists('L',$r_atten)): echo $r_atten['L']; else: echo '0'; endif;?></td>
													<td style='text-align:center'><? if(!empty($r_atten) && array_key_exists('H',$r_atten)): echo $r_atten['H']; else: echo '0'; endif;?></td>
												</tr>
                                            <?php $sr++;
												endforeach;?>
										</tbody>
									
									   <?php endif;?>  
									</table>				
								</div>
							</div>	
					<?php endif;?> 					
	
    </div>
<? endif;?>	