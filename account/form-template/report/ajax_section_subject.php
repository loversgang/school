<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');

if($_POST):
isset($_POST['id'])?$id=$_POST['id']:$id='';
isset($_POST['ct_sec'])?$ct_sec=$_POST['ct_sec']:$ct_sec='A';
isset($_POST['exam'])?$exam=$_POST['exam']:$exam='';
	
		/* Get session Pages*/
		 $QuerySec=new studentSession();
         $QuerySec->listAllSessionSection($id);
		 
		#get Session Info
		$QueryS=new session();
		$session=$QueryS->getRecord($id);	

		#Get exam List
		$QueryObjExam=new examinationGroup();
        $GroupExams=$QueryObjExam->getTermExamSessionSection($id,$ct_sec);		

		if(empty($exam) && !empty($GroupExams)):
				$exam=$GroupExams['0']->id;
		endif;
		
		if(!empty($exam)): 
			$exam_detail=get_object('student_examination_group',$exam);
			
			#get Session Info
			$QueryExsub=new examinationGroup();
			$AllSubject=$QueryExsub->getSubjectByExams($exam_detail->exam_id);				
		endif;

		
	
		
		?>	

			<? if($QuerySec->GetNumRows()>0): ?>
									<div class='span3' style='margin-left: 5px;'>
										<label class="control-label">Select Section </label>
										<div class="controls">
											<select class="exam_section_filter span8" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
												<?php $session_sc=1;while($sec=$QuerySec->GetObjectFromRecord()): ?>
														<option value='<?=$sec->section?>' <? if($ct_sec==$sec->section): echo 'selected';endif;?>><?=ucfirst($sec->section)?></option>
												<? $session_sc++; endwhile;?>
											</select>
										</div>
									</div>	

								<? if(!empty($GroupExams)): ?>
									<div class='span5' style='margin-left: 5px;' >
										<label class="control-label">Select Term Exam</label>
										<div class="controls">										
											<select class="span11 exam_subject_filter" name='exam' id='exam'>
												<?php foreach($GroupExams as $ex_key=>$ex_val): ?>
														<option value='<?=$ex_val->id?>'><?=ucfirst($ex_val->title)?></option>
												<?php endforeach;?>
											</select>
										</div>
									</div>		
	
									<div class='span4' style='margin-left: 5px;' id='subject_list'>
										<label class="control-label">Select Subject</label>
										<div class="controls">			
										<? if(!empty($AllSubject)): ?>
												<select class="span11" name='subject' id='subject'>
												<?php foreach($AllSubject as $s_key=>$s_val): ?>
														<option value='<?=$s_val->exam?>'><?=ucfirst($s_val->name)?></option>
												<?php endforeach;?>
												</select>	
										<? else: ?>	
												<input type='text' class='span11'  placeholder='Sorry, No Subject Found.' disabled />														
										<? endif;?>
										</div>
									</div>	
								<? else: ?>	
									<div class='span7' style='margin-left: 5px;' >
										<label class="control-label">Select Term Exam</label>										
											<input type='text' class='span11' id='exam' placeholder='Sorry, No Exam Found.' disabled />	
										</div>
									</div>	
								<? endif; ?>
				
					<? else:?>
							<div class='span5'>
								<div class='span12' style='margin-left: 5px;' id='ajex_section'>
										<label class="control-label">Select Section </label>
										<div class="controls">						
										<input type='text' id='ct_sec' value='' placeholder='Sorry, No Section Found.' disabled />	
										<input type='hidden' id='exam'/>
										</div>
								</div>
							</div>
					<? endif;?>					
					
<?		
endif;
?>