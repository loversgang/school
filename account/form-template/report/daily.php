<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Daily Collection Report
            </h3>
            <ul class="breadcrumb hidden-print">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li class="last">
                    Daily Collection Report
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="clearfix"></div>


    <div class="tiles pull-left hidden-print">
        <form class="form-horizontal" action="<?php echo make_admin_url('report', 'daily', 'daily') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
            <input type='hidden' name='Page' value='report'/>
            <input type='hidden' name='action' value='daily'/>
            <input type='hidden' name='section' value='daily'/>

            <div class="control-group alert alert-success span5" style='margin-left:0px;padding-right:0px;'>
                <div class='span2' style='margin-left: 5px;'>
                    <label class="control-label" style='float: none; text-align: left;'>Date</label>
                    <input type='text' class="span2 upto_current_date" placeholder="Select date" name='to' value='<?= $to ?>'/>
                </div>	



                <div class='span2' style='margin-left: 5px; padding-right:20px'>
                    <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                    <input type='submit' name='go' value='Go' class='btn blue'/>
                </div>
                <div class="clearfix"></div>
                &nbsp;&nbsp;
            </div>
        </form>										
    </div>	
    <div class="tiles pull-right hidden-print">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> <br/>


        <? if (!empty($record)): ?>
            <br/>
            <a href='<?= make_admin_url('report', 'download', 'download&tmp=daily&type=daily&to=' . $to) ?>' class="btn yellow"><i class="icon-download"></i> PDF</a>
        <? endif; ?>			

    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <form class="form-horizontal" action="<?php echo make_admin_url('report', 'lsit', 'lsit') ?>" method="POST" enctype="multipart/form-data" id="validation">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-rupee"></i>Daily Collection Report</div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                            <form action="<?php echo make_admin_url('expense', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >		
                                <table class="table table-striped table-bordered table-hover" id="sample_2">
                                    <thead>
                                        <tr>
                                            <th>Sr. No.</th>
                                            <th>Reg. No.</th>
                                            <th>Student Name</th>
                                            <th>Father Name</th>
                                            <th class="hidden-480 sorting_disabled">Session</th>
                                            <th class="hidden-480 sorting_disabled">Receipt No.</th>
                                            <th style='text-align:center;width:15%;' class="hidden-480 sorting_disabled">Date</th>
                                            <th>Extra Fee</th>
                                            <th class="hidden-480 sorting_disabled" style='width:15%;text-align:right;'>Amount (<?= CURRENCY_SYMBOL ?>)</th>
                                        </tr>
                                    </thead>
                                    <? if (!empty($record)): $sum = ''; ?>
                                        <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($record as $key => $object):
                                                //echo '<pre>'; print_r($record); exit;
                                                ?>

                                                <tr class="odd gradeX">
                                                    <td><?= $sr ?>.</td>                                  <td><?= $object->reg_id ?></td>
                                                    <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                    <td><?= $object->father_name ?></td>
                                                    <td><?= $object->title ?></td>
                                                    <td class="hidden-480 sorting_disabled"><a href="<?php echo make_admin_url('fee', 'thrash', 'thrash', 'type=fee&fee_id='.$object->id) ?>"><?= $object->id ?></a></td>
                                                    <td style='text-align:center' class="hidden-480 sorting_disabled"><?= $object->payment_date ?></td>
                                                    <td><?php echo CURRENCY_SYMBOL . " " . studentFeeRecord::getExtraFee($object->student_id, $object->total_amount, $to); ?></td>
                                                    <td style='text-align:right;'><?= CURRENCY_SYMBOL . " " . number_format($object->total_amount, 2) ?></td>													
                                                </tr>
                                                <?php
                                                $sr++;
                                                $sum = $sum + $object->total_amount;
                                            endforeach;
                                            ?>
                                        </tbody>
                                    <?php endif; ?>  
                                </table>
                                <? if (!empty($record)): ?>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width:10%px;"></th>
                                                <th style='width:40%;'></th>
                                                <th class="hidden-480 sorting_disabled" style='width:20%;'></th>
                                                <th style='text-align:right;width:20%;padding-right:1px;' class="hidden-480 sorting_disabled" >Total (<?= CURRENCY_SYMBOL ?>) =</th>
                                                <th class="hidden-480 sorting_disabled" style='width:20%;text-align:right;'><?= CURRENCY_SYMBOL . " " . number_format($sum, 2) ?></th>
                                            </tr>
                                        </thead>	
                                    </table>
                                <?php endif; ?> 	
                            </form> 									
                        </div>
                    </div>
                </div>
            </form>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<script type="text/javascript">
    $("#print").live("click", function () {
        var from = '<?= $from ?>';
        var to = '<?= $to ?>';
        var dataString = 'from=' + from + '&to=' + to;
        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_daily_report', 'ajax_daily_report&temp=report'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var DocumentContainer = document.getElementById(data);
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
</script>			