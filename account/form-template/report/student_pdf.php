<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
<style>
    body {
        color: #000000;
        direction: ltr;
        font-family: 'Open Sans';
        font-size: 13px;
        background-color: #FFF !important;
    }

    .portlet.box.yellow {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        border-color: -moz-use-text-color #FCCB7E #FCCB7E;
        border-image: none;
        border-right: 1px solid #FCCB7E;
        border-style: none solid solid;
        border-width: 0 1px 1px;
    }
    .portlet.box {
        padding: 0 !important;
    }
    .portlet-body.yellow, .portlet.yellow {
        background-color: #FFB848 !important;
    }
    .portlet {
        clear: both;
        margin-bottom: 25px;
        margin-top: 0;
        padding: 0;
    }

    .caption{
        display: inline-block;
        float: left;
        font-size: 18px;
        font-weight: 400;
        margin: 0 0 7px;
        padding: 0 0 8px;
        background:#D9EDF7;
        padding:5px;
    }
    table th{
        background:#BF504D;
        color:#fff;	
    }
    .table thead tr th {
        font-size: 11px !important;
        font-weight: 600;
    }
    .title{
        background:#D9EDF7;
        padding-left:5px;
        padding-right:5px;
        padding-top:5px;
    }
    .main_title1{
        margin-top:5px;
    }
    #custom_address{
        border: 1px solid #EAEAEA;
        border-radius: 16px !important;
        height: 118px;
        margin-left: 15px;
        margin-top: 15px;
        padding: 16px;
        width: 80%;
    }
</style>

<?php
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');


isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['date']) ? $date = $_REQUEST['date'] : $date = '';
isset($_REQUEST['student']) ? $student = $_REQUEST['student'] : $student = '';
isset($_REQUEST['report_type']) ? $report_type = $_REQUEST['report_type'] : $report_type = 'admission_detail';

$session = get_object('session', $session_id);

#get Student Report 
$StudentObj = new studentSession();
$record = $StudentObj->get_various_student_report($school->id, $session_id, $ct_sec, $report_type);

if ($report_type == 'category_detail' && !empty($record)):
    foreach ($record as $key => $value):
        if (($value->category == '') || ($value->category == 'None') || ($value->category == 'GEN')):
            unset($record[$key]);
        endif;
    endforeach;
endif;
?>

<? if (!empty($record)): ?>
    <!-- BEGIN PAGE CONTAINER-->
    <div style='margin:20px;border:1px solid;'>
        <!-- BEGIN PAGE HEADER-->


        <? if ($report_type == "admission_detail"): ?>	
            <div class="portlet-body">

                <table width='100%'>
                    <tr><td colspan='2' class='main_title1'  style="text-align:right;margin-top:15px;font-size:22px;padding:5px;"><br/>CHECK LIST DETAIL OF ADMISSION DETAIL & DOB</td></tr>
                    <tr><td class='title'><strong>SCHOOL:- <?= $school->school_name ?></strong></td><td class='title' style='text-align:right;'><strong>SESSION:- <?= $session->title ?>, SECTION:- <?= $ct_sec ?></strong></td></tr>
                </table>
                <div style='clear:both;'></div>							
                <div class="clearfix">

                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="hidden-480">SR. NO.</th>
                                <th class="hidden-480" >DATE OF ADMISSION</th>
                                <th class="hidden-480" >ADMISSION NO.</th>
                                <th>ROLL NO.</th>
                                <th class="hidden-480" >STUDENT NAME</th>
                                <th class="hidden-480" >FATHER'S NAME</th>
                                <th class="hidden-480" >MOTHER'S NAME</th>
                                <th class="hidden-480" >ADDRESS</th>
                                <th class="hidden-480" >DATE OF BIRTH</th>
                            </tr>
                        </thead>
                        <? if (!empty($record)): $sum = ''; ?>
                            <tbody>
                                <?php
                                $sr = 1;
                                foreach ($record as $sk => $stu):
                                    $QueryP = new studentAddress();
                                    $s_p_ad = $QueryP->getStudentAddressByType($stu->id, 'permanent');
                                    ?>																		

                                    <tr class="odd gradeX">
                                        <td><?= $sr ?>.</td>
                                        <td><?= date('d M, Y', strtotime($stu->date_of_admission)) ?></td>
                                        <td ><?= $stu->reg_id ?></td>
                                        <td ><?= $stu->roll_no ?></td>
                                        <td ><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                        <td><?= ucfirst($stu->father_name) ?></td>
                                        <td><?= ucfirst($stu->mother_name) ?></td>
                                        <td>
                                            <?
                                            if (is_object($s_p_ad)):
                                                echo $s_p_ad->address1 . ", " . $s_p_ad->city . ", " . $s_p_ad->tehsil . ", " . $s_p_ad->district . ", " . $s_p_ad->state . ", " . $s_p_ad->zip;
                                            endif;
                                            ?>														
                                        </td>
                                        <td><?= date('d M, Y', strtotime($stu->date_of_birth)) ?></td>
                                    </tr>
                                <? $sr++;
                            endforeach;
                            ?>									
                            </tbody>
            <?php endif; ?>  
                    </table>				
                </div>
            </div>

    <? elseif ($report_type == "withdrawal_detail"): ?>	
            <div class="portlet-body">

                <table width='100%'>
                    <tr><td colspan='2' class='main_title1'  style="text-align:right;margin-top:15px;font-size:22px;padding:5px;"><br/>ADMISSION & WITHDRAWAL REGISTER OF THE STUDENTS</td></tr>
                    <tr><td class='title'><strong>SCHOOL:- <?= $school->school_name ?></strong></td><td class='title' style='text-align:right;'><strong>SESSION:- <?= $session->title ?>, SECTION:- <?= $ct_sec ?></strong></td></tr>
                </table>
                <div style='clear:both;'></div>							
                <div class="clearfix">

                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="hidden-480">SR. NO.</th>
                                <th class="hidden-480" >DATE OF ADMISSION</th>
                                <th class="hidden-480" >ADMISSION NO.</th>
                                <th>ROLL NO.</th>
                                <th class="hidden-480" >STUDENT NAME</th>
                                <th class="hidden-480" >DATE OF BIRTH</th>
                                <th class="hidden-480" >FATHER'S NAME</th>
                                <th class="hidden-480" >MOTHER'S NAME</th>
                                <th class="hidden-480" >FATHER'S OCCUPATION</th>
                                <th class="hidden-480" >CATEGORY</th>
                                <th class="hidden-480" >RESIDENCE OF ADDRESS</th>
                            </tr>
                        </thead>
                            <? if (!empty($record)): $sum = ''; ?>
                            <tbody>
            <?php
            $sr = 1;
            foreach ($record as $sk => $stu):
                $QueryP = new studentAddress();
                $s_p_ad = $QueryP->getStudentAddressByType($stu->id, 'permanent');
                ?>																		

                                    <tr class="odd gradeX">
                                        <td><?= $sr ?>.</td>
                                        <td><?= date('d M, Y', strtotime($stu->date_of_admission)) ?></td>
                                        <td ><?= $stu->reg_id ?></td>
                                        <td ><?= $stu->roll_no ?></td>
                                        <td ><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                        <td><?= date('d M, Y', strtotime($stu->date_of_birth)) ?></td>
                                        <td><?= ucfirst($stu->father_name) ?></td>
                                        <td><?= ucfirst($stu->mother_name) ?></td>
                                        <td><?= ucfirst($stu->father_occupation) ?></td>
                                        <td><?= ucfirst($stu->category) ?></td>
                                        <td>
                                    <?
                                    if (is_object($s_p_ad)):
                                        echo $s_p_ad->address1 . "," . $s_p_ad->city . ", " . $s_p_ad->state . ", " . $s_p_ad->zip;
                                    endif;
                                    ?>														
                                        </td>
                                    </tr>
                <? $sr++;
            endforeach;
            ?>									
                            </tbody>
        <?php endif; ?>  
                    </table>				
                </div>
            </div>	
    <? elseif ($report_type == "category_detail"): ?>	
            <div class="portlet-body">

                <table width='100%'>
                    <tr><td colspan='2' class='main_title1'  style="text-align:right;margin-top:15px;font-size:22px;padding:5px;"><br/>CHECK LIST DETAIL STATEMENT OF SC,ST & OBC STUDENTS</td></tr>
                    <tr><td class='title'><strong>SCHOOL:- <?= $school->school_name ?></strong></td><td class='title' style='text-align:right;'><strong>SESSION:- <?= $session->title ?>, SECTION:- <?= $ct_sec ?></strong></td></tr>
                </table>
                <div style='clear:both;'></div>							
                <div class="clearfix">

                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="hidden-480">SR. NO.</th>
                                <th class="hidden-480" >DATE OF ADMISSION</th>
                                <th class="hidden-480" >ADMISSION NO.</th>
                                <th>ROLL NO.</th>
                                <th class="hidden-480" >STUDENT NAME</th>
                                <th class="hidden-480" >FATHER'S NAME</th>
                                <th class="hidden-480" >MOTHER'S NAME</th>
                                <th class="hidden-480" >ADDRESS</th>
                                <th class="hidden-480" >CATEGORY</th>
                            </tr>
                        </thead>
        <? if (!empty($record)): $sum = ''; ?>
                            <tbody>
            <?php
            $sr = 1;
            foreach ($record as $sk => $stu):
                $QueryP = new studentAddress();
                $s_p_ad = $QueryP->getStudentAddressByType($stu->id, 'permanent');
                ?>																		

                                    <tr class="odd gradeX">
                                        <td><?= $sr ?>.</td>
                                        <td><?= date('d M, Y', strtotime($stu->date_of_admission)) ?></td>
                                        <td ><?= $stu->reg_id ?></td>
                                        <td ><?= $stu->roll_no ?></td>
                                        <td ><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                        <td><?= ucfirst($stu->father_name) ?></td>
                                        <td><?= ucfirst($stu->mother_name) ?></td>
                                        <td>
                <?
                if (is_object($s_p_ad)):
                    echo $s_p_ad->address1 . ", " . $s_p_ad->city . ", " . $s_p_ad->state . ", " . $s_p_ad->zip;
                endif;
                ?>														
                                        </td>
                                        <td><?= $stu->category ?></td>
                                    </tr>
                <? $sr++;
            endforeach;
            ?>									
                            </tbody>
                    <?php endif; ?>  
                    </table>				
                </div>
            </div>	
                <? elseif ($report_type == "subject_detail"): ?>	
            <div class="portlet-body">

                <table width='100%'>
                    <tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;padding:5px;"><br/>CHECK LIST OF REGISTERED CANDIDATE WITH SUBJECTS</td></tr>
                    <tr><td class='title'><strong>SCHOOL:- <?= $school->school_name ?></strong></td><td class='title' style='text-align:right;'><strong>SESSION:- <?= $session->title ?>, SECTION:- <?= $ct_sec ?></strong></td></tr>
                </table>
                <div style='clear:both;'></div>							
                <div class="clearfix">
        <?
        #get the student subject list if enter previous
        $session = get_object('session', $session_id);

        $QueryStComSub = new subject();
        $cmpSub = $QueryStComSub->GetSessionSubjectsInArray($session->compulsory_subjects);

        #get the student subject list if enter previous
        $QueryStELcSubb = new subject();
        $elcSub = $QueryStELcSubb->GetSessionSubjectsInArray($session->elective_subjects);
        ?>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="hidden-480" >REG. ID.</th>
                                <th class="hidden-480" >ROLL NO.</th>
                                <th class="hidden-480" >STUDENT NAME</th>
                                <th class="hidden-480" >FATHER'S NAME</th>
                                <th class="hidden-480" >MOTHER'S NAME</th>
                                <th class="hidden-480" >DOB</th>
                                <th class="hidden-480" >CATEGORY</th>
                                <th class="hidden-480" >RELIGION</th>
                                <th class="hidden-480" >GENDER</th>
        <?
        if (!empty($cmpSub)):
            foreach ($cmpSub as $key => $val):
                echo "<th>SUB " . ($key + 1) . "</th>";
            endforeach;
        endif;

        if (!empty($elcSub)):
            foreach ($elcSub as $Ekey => $Eval):
                echo "<th>ELE SUB " . ($Ekey + 1) . "</th>";
            endforeach;
        endif;
        $ttl_elc = count($elcSub);
        ?>	
                                <th class="hidden-480" >STUDENT PHOTO</th>											
                            </tr>
                        </thead>
        <? if (!empty($record)): $sum = ''; ?>
                            <tbody>
            <?php
            $sr = 1;
            foreach ($record as $sk => $stu):
                $QueryStElcSub = new studentSubject();
                $StelcSub = $QueryStElcSub->getStudentSubjectTitleInArray($session_id, $stu->id, 'Elective');

                $im_obj = new imageManipulation();
                if ($stu->photo):
                    $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $stu->photo) . "' style='width:80px'/>";
                else:
                    $student_image = '<img src="assets/img/profile/img-1.jpg" style="width:80px"/>';
                endif;
                ?>																		

                                    <tr class="odd gradeX">
                                        <td ><?= $stu->reg_id ?></td>
                                        <td ><?= $stu->roll_no ?></td>
                                        <td ><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                        <td><?= ucfirst($stu->father_name) ?></td>
                                        <td><?= ucfirst($stu->mother_name) ?></td>														
                                        <td><?= date('d M, Y', strtotime($stu->date_of_birth)) ?></td>
                                        <td><?= ucfirst($stu->category) ?></td>
                                        <td><?= ucfirst($stu->religion) ?></td>
                                        <td ><?= $stu->sex ?></td>
                                        <?
                                        if (!empty($cmpSub)):
                                            foreach ($cmpSub as $Kkey => $Vval):
                                                echo "<td>" . ($Vval) . "</td>";
                                            endforeach;
                                        endif;

                                        if (!empty($StelcSub)): $el_st = '1';
                                            foreach ($StelcSub as $Skey => $Sval):
                                                echo "<td>" . ($Sval) . "</td>";
                                                $el_st++;
                                            endforeach;
                                            while ($el_st <= $ttl_elc):
                                                echo "<td></td>";
                                                $el_st++;
                                            endwhile;
                                        else: $el_st = '1';
                                            while ($el_st <= $ttl_elc):
                                                echo "<td></td>";
                                                $el_st++;
                                            endwhile;
                                        endif;
                                        ?>
                                        <td><?= $student_image ?></td>
                                    </tr>
                <? $sr++;
            endforeach;
            ?>									
                            </tbody>
        <?php endif; ?>  
                    </table>				
                </div>
            </div>	
    <? elseif ($report_type == "contact_detail"): ?>	
            <div class="portlet-body">

                <table width='100%'>
                    <tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;padding:5px;"><br/>LIST OF PHOTO, ADDRESS & CONTACT NO. DETAIL</td></tr>
                    <tr><td class='title'><strong>SCHOOL:- <?= $school->school_name ?></strong></td><td class='title' style='text-align:right;'><strong>SESSION:- <?= $session->title ?>, SECTION:- <?= $ct_sec ?></strong></td></tr>
                </table>
                <div style='clear:both;'></div>							
                <div class="clearfix">								
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>SR. NO.</th>
                                <th>REG ID</th>
                                <th>ROLL NO.</th>
                                <th>STUDENT NAME</th>
                                <th>FATHER NAME</th>											
                                <th class="hidden-480 sorting_disabled">CONTACT NO.</th>
                                <th class="hidden-480 sorting_disabled">PERMANENT ADDRESS</th>
                                <th>PHOTO</th>											
                            </tr>
                        </thead>
                            <? if (!empty($record)): $sum = ''; ?>
                            <tbody>
            <?php
            $sr = 1;
            foreach ($record as $sk => $object):

                #get student permanent address
                $QueryP = new studentAddress();
                $s_p_ad = $QueryP->getStudentAddressByType($object->id, 'permanent');

                #get student correspondence address
                $QueryC = new studentAddress();
                $s_c_ad = $QueryC->getStudentAddressByType($object->id, 'correspondence');

                $im_obj = new imageManipulation();
                if ($object->photo):
                    $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $object->photo) . "' style='width:80px'/>";
                else:
                    $student_image = '';
                endif;
                ?>																		

                                    <tr class="odd gradeX">
                                        <td><?= $sr ?>.</td>
                                        <td><?= $object->reg_id ?></td>
                                        <td><?= $object->roll_no ?></td>
                                        <td><?= ucfirst($object->first_name) . " " . $object->last_name ?></td>
                                        <td><?= $object->father_name ?></td>
                                        <td><?= $object->phone ?></td>
                                        <td class="hidden-480 sorting_disabled">
                    <?
                    if (is_object($s_p_ad)):
                        echo $s_p_ad->address1 . ", " . $s_p_ad->city . ", " . $s_p_ad->state . ", " . $s_p_ad->zip;
                    endif;
                    ?>
                                        </td>
                                        <td><?= $student_image ?></td>
                                    </tr>
                <? $sr++;
            endforeach;
            ?>									
                            </tbody>
        <?php endif; ?>  
                    </table>				
                </div>
            </div>							
    <?
    elseif ($report_type == "student_type"):

        $session = get_object('session', $session_id);

        if (strtotime($session->end_date) < strtotime(date('Y-m-d'))):
            $l_ast = $session->end_date;
        else:
            $l_ast = date('Y-m-d');
        endif;
        ?>	
            <div class="portlet-body">

                <table width='100%'>
                    <tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;padding:5px;"><br/>CHECK LIST STATEMENT GRANTED CONCESSION OF THE STUDENTS</td></tr>
                    <tr><td class='title'><strong>SCHOOL:- <?= $school->school_name ?></strong></td><td class='title' style='text-align:right;'><strong>SESSION:- <?= $session->title ?>, SECTION:- <?= $ct_sec ?></strong></td></tr>
                </table>
                <div style='clear:both;'></div>							
                <div class="clearfix">

                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="hidden-480">REG. ID.</th>
                                <th class="hidden-480" >ROLL NO.</th>
                                <th class="hidden-480" >STUDENT NAME</th>
                                <th class="hidden-480" >CLASS</th>
                                <th class="hidden-480" >SECTIOIN</th>
                                <th class="hidden-480" >STUDENT GRANTED</th>
                                <th class="hidden-480" >CONCESSION</th>											
                            </tr>
                        </thead>
        <? if (!empty($record)): $sum = ''; ?>
                            <tbody>														
            <? foreach ($record as $key => $stu):
                ?>	
                                    <tr class="odd gradeX">
                                        <td ><?= $stu->reg_id ?></td>
                                        <td ><?= $stu->roll_no ?></td>
                                        <td ><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                        <td><?= $session->title ?></td>
                                        <td><?= $stu->section ?></td>
                                        <td><? if (empty($stu->concession)):echo "None";
                else: echo $stu->concession;
                endif; ?></td>
                                        <td><?= number_format($stu->concession_type, 2) ?></td>
                                    </tr>
                                        <? endforeach; ?>									
                            </tbody>
        <?php endif; ?>  
                    </table>				
                </div>
            </div>	
                                    <?
                                    elseif ($report_type == "address_label"):
                                        $session = get_object('session', $session_id);
                                        ?>	
            <div class="portlet-body">

                <table width='100%'>
                    <tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;padding:5px;"><br/>STATEMENT OF STUDENTS ADDRESS LABEL</td></tr>
                    <tr><td class='title'><strong>SCHOOL:- <?= $school->school_name ?></strong></td><td class='title' style='text-align:right;'><strong>SESSION:- <?= $session->title ?>, SECTION:- <?= $ct_sec ?></strong></td></tr>
                </table>
                <div style='clear:both;'></div>							
                <div class="clearfix">

                    <? if (!empty($record)): $sum = ''; ?>
                        <div class="row-fluid">
                            <table style='width:100%;'><tr>
                        <?php
                        $sr = 1;
                        foreach ($record as $key => $stu):
                            #get student permanent address
                            $QueryP = new studentAddress();
                            $s_p_ad = $QueryP->getStudentAddressByType($stu->id, 'permanent');
                            ?>											
                                        <td style='width:50%;'>
                                            <div id='custom_address'> 
                                                <strong><?= strtoupper($stu->first_name . " " . $stu->last_name) ?></strong><br/>
                                                S/o. | D/o. <?= strtoupper($stu->father_name) ?><br/><br/>

                                                <strong>Address</strong><br/>
                <?
                if (is_object($s_p_ad)):
                    echo $s_p_ad->address1 . ", " . $s_p_ad->city . "<br/>" . $s_p_ad->state . ", " . $s_p_ad->zip;
                endif;
                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                        </td>
                <?php
                if ($sr % 2 == '0'): echo "</tr><tr>";
                endif;
                $sr++;
            endforeach;
            ?>
                                </tr>
                            </table>
                        </div>	
                        <div class="clearfix"></div>
        <?php else: ?>
                        Sorry, No Record Found..!
        <?php endif; ?> 

                </div>
            </div>							
    <?php endif; ?> 					

    </div>
<? endif; ?>	