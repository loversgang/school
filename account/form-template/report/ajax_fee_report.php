<?php
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');


isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = 'A';
isset($_REQUEST['interval_id']) ? $interval_id = $_REQUEST['interval_id'] : $interval_id = '';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
isset($_REQUEST['report_type']) ? $report_type = $_REQUEST['report_type'] : $report_type = 'admission_detail';
isset($_REQUEST['date']) ? $date = $_REQUEST['date'] : $date = '';
isset($_REQUEST['date1']) ? $date1 = $_REQUEST['date1'] : $date1 = '0';
isset($_REQUEST['month']) ? $month = $_REQUEST['month'] : $month = '';
isset($_REQUEST['exam']) ? $exam = $_REQUEST['exam'] : $exam = '0';
isset($_REQUEST['v_report_type']) ? $v_report_type = $_REQUEST['v_report_type'] : $v_report_type = 'vehicle_info';
isset($_REQUEST['s_report_type']) ? $s_report_type = $_REQUEST['s_report_type'] : $s_report_type = 'recieve';
isset($_REQUEST['f_report_type']) ? $f_report_type = $_REQUEST['f_report_type'] : $f_report_type = 'recieve';
isset($_REQUEST['e_report_type']) ? $e_report_type = $_REQUEST['e_report_type'] : $e_report_type = 'live';
isset($_REQUEST['ex_type']) ? $ex_type = $_REQUEST['ex_type'] : $ex_type = '';
isset($_REQUEST['a_type']) ? $a_type = $_REQUEST['a_type'] : $a_type = '';
isset($_REQUEST['f_type']) ? $f_type = $_REQUEST['f_type'] : $f_type = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['subject']) ? $subject = $_REQUEST['subject'] : $subject = '';

isset($_REQUEST['tmp']) ? $tmp = $_REQUEST['tmp'] : $tmp = 'student';


isset($_REQUEST['from']) ? $from = $_REQUEST['from'] : $from = '';
isset($_REQUEST['to']) ? $to = $_REQUEST['to'] : $to = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = '';

if (isset($_POST)) {
    extract($_POST);
    ?>
    <style>
        .print_tables table {
            width: 100%;
            border: 1px solid #000;
        }
        .print_tables td {
            border-top: 1px solid #000;
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables th {
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables table tr td:last-child {
            border-right: none;
        }
        .print_tables table tr th:last-child {
            border-right: none;
        }
    </style>
    <div class="print_tables">
        <?php
        if ($f_type == 'month_wise'):
            /* Get session Pages */
            $QuerySec = new studentSession();
            $QuerySec->listAllSessionSection($session_id);
            //echo $ct_sec; exit;
            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);

            $session = get_object('session', $session_id);

            #get Session Interval
            $QueryInt = new session_interval();
            $interval = $QueryInt->listOfInterval($session_id);

            $current_date = date('Y-m-d');
            $current = '0';
            ?>
            <center><h1>Class Wise Fee Detail</h1></center>
            <table>
                <tr>
                    <th>Reg ID</th>
                    <th>Name</th>
                    <th>Concession Amount</th>
                    <th>Vehicles Fee</th>
                    <th>Previous Amount</th>
                    <th>Receivable Amount</th>
                    <th>Received Amount</th>	
                    <th>Balance Amount</th>	
                </tr>
                <?
                if (!empty($record) && !empty($interval)): $sum = '';
                    foreach ($record as $key => $stu):
                        #Get interval detail
                        $SessIntOb = new studentSessionInterval();
                        $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sec, $stu->id, $interval_id);
                        $SessObPre = new studentSessionInterval();
                        $pending_fee = $SessObPre->getPreviousBalanceFee($session_id, $ct_sec, $stu->id, $interval_id);
                        ?>
                        <tr>
                            <td><?= $stu->reg_id ?></td>
                            <td><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                            <td><?
                                if (is_object($student_interval_detail)): echo CURRENCY_SYMBOL . " " . number_format($student_interval_detail->concession, 2);
                                endif;
                                ?>
                            </td>
                            <td><?
                                if (is_object($student_interval_detail)): echo CURRENCY_SYMBOL . " " . number_format($student_interval_detail->vehicle_fee, 2);
                                endif;
                                ?>
                            </td>
                            <td><?
                                if (isset($pending_fee) && !empty($pending_fee)): echo CURRENCY_SYMBOL . " " . number_format($pending_fee, 2);
                                else: echo CURRENCY_SYMBOL . " " . number_format('0', 2);
                                endif;
                                ?>
                            </td>
                            <td><?
                                if (is_object($student_interval_detail)): echo CURRENCY_SYMBOL . " " . number_format($student_interval_detail->total, 2);
                                endif;
                                ?>
                            </td>
                            <td><?
                                if (is_object($student_interval_detail)): echo CURRENCY_SYMBOL . " " . number_format($student_interval_detail->paid, 2);
                                endif;
                                ?>
                            </td>
                            <td><?
                                if (is_object($student_interval_detail)): echo CURRENCY_SYMBOL . " " . number_format($pending_fee + ($student_interval_detail->total - $student_interval_detail->paid), 2);
                                endif;
                                ?>
                            </td>
                        </tr>
                        <?
                    endforeach;
                endif;
                ?>  
            </table>
            <?php
        elseif ($f_type == 'head_wise'):
            /* Get session Pages */
            $QuerySec = new studentSession();
            $QuerySec->listAllSessionSection($session_id);

            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);

            $session = get_object('session', $session_id);

            #get Session Interval
            $QueryInt = new session_interval();
            $interval = $QueryInt->listOfInterval($session_id);

            #Get Session Fee Heads
            $QuerySess = new session_fee_type();
            $Sesspay_heads = $QuerySess->sessionPayHeadsWithKey($session_id);

            $current_date = date('Y-m-d');
            $current = '0';

            $interval_detail = get_object('session_interval', $interval_id);
            ?>
            <center><h1>Head Wise Expected Collection Details</h1></center>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>Reg ID</th>
                        <th>Name</th>
                        <?
                        if (!empty($Sesspay_heads)):
                            foreach ($Sesspay_heads as $s_key => $s_value):
                                if ($interval_detail->interval_position == '1'):
                                    ?>
                                    <th class="hidden-480" style='text-align:center;'><?= $s_value['title'] ?></th>
                                    <?
                                else:
                                    if ($s_value['type'] == 'Regular'):
                                        ?>
                                        <th class="hidden-480" style='text-align:center;'><?= $s_value['title'] ?></th>	
                                        <?
                                    endif;
                                endif;
                            endforeach;
                            ?>
                            <th class="hidden-480" style='text-align:right;'>Total Amount</th>
                        <? endif; ?>
                    </tr>
                </thead>
                <? if (!empty($record) && !empty($interval)): $sum = ''; ?>
                    <tbody>													
                        <? foreach ($record as $key => $stu): ?>
                            <tr class="odd gradeX">
                                <td ><?= $stu->reg_id ?></td>
                                <td><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                <?
                                if (!empty($Sesspay_heads)): $Head_total = '';
                                    foreach ($Sesspay_heads as $s_kkey => $s_vvalue):
                                        #Get Head fee detail
                                        $StuHeadObj = new session_student_fee();
                                        $HeadAmount = $StuHeadObj->getSingleStudentFeeHeadValue($session_id, $stu->id, $s_vvalue['fee_id']);


                                        if ($interval_detail->interval_position == '1'):
                                            $Head_total = $Head_total + $HeadAmount;
                                            ?>
                                            <td class="hidden-480" style='text-align:center;'><?= number_format($HeadAmount, 2) ?></td>
                                            <?
                                        else:
                                            if ($s_vvalue['type'] == 'Regular'):
                                                $Head_total = $Head_total + $HeadAmount;
                                                ?>
                                                <td class="hidden-480" style='text-align:center;'><?= number_format($HeadAmount, 2) ?></td>
                                                <?
                                            endif;
                                        endif;
                                    endforeach;
                                    ?>														
                                    <td class="hidden-480" style='text-align:right;'><?= number_format($Head_total, 2); ?></td>
                                <? endif; ?>
                            </tr>

                        <? endforeach; ?>	

                    </tbody>
                <?php endif; ?>  
            </table>
            <?php
        elseif ($f_type == 'class_due'):
            /* Get session Pages */
            $QuerySec = new studentSession();
            $QuerySec->listAllSessionSection($session_id);

            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->getIntervalDuePendingFee($session_id, $ct_sec);
            ?>
            <center><h1>Class Wise Fees Due List With Contact Details</h1></center>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>Reg ID</th>
                        <th>Name</th>
                        <th>Father Name</th>
                        <th>Contact No.</th>
                        <th>Receivable Amount</th>
                        <th>Received Amount</th>
                        <th style='text-align:right;'>Balance</th>
                    </tr>
                </thead>
                <? if (!empty($record)): $sum = ''; ?>
                    <tbody>													
                        <? foreach ($record as $key => $stu): ?>
                            <tr class="odd gradeX">
                                <td ><?= $stu->reg_id ?></td>
                                <td><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                <td><?= $stu->father_name ?></td>
                                <td><?= $stu->family_phone ?></td>
                                <td><?= number_format($stu->total, 2) ?></td>
                                <td><?= number_format($stu->paid, 2) ?></td>
                                <td style='text-align:right;'><?= number_format($stu->total - $stu->paid, 2) ?></td>
                            </tr>
                        <? endforeach; ?>	

                    </tbody>
                <?php endif; ?>  
            </table>	
            <?php
        elseif ($f_type == 'head_conc'):
            #concession list array
            $conc_heads = array('Full Poverty' => '0', 'Half Poverty' => '0', 'Scholarship' => '0', 'Special Student' => '0', 'Sports Student' => '0', 'Sibling Student' => '0');

            /* Get session Pages */
            $QuerySec = new studentSession();
            $QuerySec->listAllSessionSection($session_id);

            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->getAllConcessionWiseAmount($session_id, $ct_sec);

            $session = get_object('session', $session_id);

            $current_date = date('Y-m-d');
            $current = '0';
            ?>
            <center><h1>Head Wise Concession Details</h1></center>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Head Name</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <?
                    if (!empty($conc_heads)): $sr = '1';
                        foreach ($conc_heads as $s_key => $s_value):
                            ?>
                            <tr class="odd gradeX">
                                <td><?= $sr ?>.</td>
                                <td><?= $s_key ?></td>
                                <td><?
                                    if (array_key_exists($s_key, $record)): echo number_format($record[$s_key], 2);
                                    else: echo number_format($s_value, 2);
                                    endif;
                                    ?></td>													
                            </tr>	
                            <?
                            $sr++;
                        endforeach;
                        ?>
                    <? endif; ?>																	
                </tbody>
            </table>
            <?php
        endif;
        ?>
    </div>
<?php } ?>