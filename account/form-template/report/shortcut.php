
<div class="tile bg-green selected">
    <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">
        <div class="corner"></div>
        <div class="tile-body"><i class="icon-arrow-left"></i></div>
        <div class="tile-object"><div class="name">All Reports</div></div>
    </a> 
</div>
<? if (!empty($report_type)): ?>
    <div class="tile bg-yellow" id='print_document'>							
        <a class="hidden-print" id='print'>
            <div class="corner"></div>
            <div class="tile-body"><i class="icon-print"></i></div>
            <div class="tile-object"><div class="name">Print</div></div>
        </a>
    </div>	
<? endif; ?>
