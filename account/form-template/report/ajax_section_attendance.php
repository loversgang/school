<?php
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');


isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = 'A';
isset($_REQUEST['interval_id']) ? $interval_id = $_REQUEST['interval_id'] : $interval_id = '';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
isset($_REQUEST['report_type']) ? $report_type = $_REQUEST['report_type'] : $report_type = 'admission_detail';
isset($_REQUEST['date']) ? $date = $_REQUEST['date'] : $date = '';
isset($_REQUEST['date1']) ? $date1 = $_REQUEST['date1'] : $date1 = '0';
isset($_REQUEST['month']) ? $month = $_REQUEST['month'] : $month = '';
isset($_REQUEST['exam']) ? $exam = $_REQUEST['exam'] : $exam = '0';
isset($_REQUEST['v_report_type']) ? $v_report_type = $_REQUEST['v_report_type'] : $v_report_type = 'vehicle_info';
isset($_REQUEST['s_report_type']) ? $s_report_type = $_REQUEST['s_report_type'] : $s_report_type = 'recieve';
isset($_REQUEST['f_report_type']) ? $f_report_type = $_REQUEST['f_report_type'] : $f_report_type = 'recieve';
isset($_REQUEST['e_report_type']) ? $e_report_type = $_REQUEST['e_report_type'] : $e_report_type = 'live';
isset($_REQUEST['ex_type']) ? $ex_type = $_REQUEST['ex_type'] : $ex_type = '';
isset($_REQUEST['a_type']) ? $a_type = $_REQUEST['a_type'] : $a_type = '';
isset($_REQUEST['f_type']) ? $f_type = $_REQUEST['f_type'] : $f_type = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['subject']) ? $subject = $_REQUEST['subject'] : $subject = '';

isset($_REQUEST['tmp']) ? $tmp = $_REQUEST['tmp'] : $tmp = 'student';


isset($_REQUEST['from']) ? $from = $_REQUEST['from'] : $from = '';
isset($_REQUEST['to']) ? $to = $_REQUEST['to'] : $to = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = '';

if (isset($_POST)) {
    extract($_POST);
    ?>
    <style>
        .print_tables table {
            width: 100%;
            border: 1px solid #000;
        }
        .print_tables td {
            border-top: 1px solid #000;
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables th {
            border-right: 1px solid #000;
            border-top: 1px solid #000;
            text-align: center;
        }
        .print_tables th:nth-child(1) {
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables table tr td:last-child {
            border-right: none;
        }
        .print_tables table tr th:last-child {
            border-right: none;
        }
    </style>
    <div class="print_tables">
        <?php
        if (!($date)): $date = date('m');
        endif;
        if (!isset($year)): $year = date('Y');
        endif;

#count working days
        $working_days = '';
        if (!empty($date) && !empty($year)):
            $first_date_of_month = $year . "-" . $date . "-01";
            $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($year . "-" . $date . "-1"))));

            $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
            $total_month_days = (date('d', strtotime($last_date_of_month)));
            $working_days = $total_month_days - $total_sunday;
        endif;

        $session_info = get_object('session', $session_id);

        if ($a_type == 'month_wise'):
            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);
            ?>
        <center><h1>Month Wise Attendance Report</h1></center>
            <table>
                <tr>
                    <th>SR. NO.</th>
                    <th>ROLL No.</th>
                    <th>SECTION</th>
                    <th>STUDENT NAME</th>
                    <th>FATHER NAME</th>
                    <th>PRESENT</th>
                    <th>ABSENT</th>
                    <th>LEAVE</th>
                    <th>WORKING DAYS</th>
                    <th>%AGE</th>
                </tr>
                <? if (!empty($record)): ?>
                    <?
                    if (!empty($record)): $sum = '';
                        $sr = 1;
                        foreach ($record as $sk => $stu): $checked = '';
                            $Q_obj = new attendance();
                            $r_atten = $Q_obj->checkSessionSectionMonthYearAttendace($session_id, $ct_sec, $stu->id, $date, $year);
                            $t_attend = array_sum($r_atten);

                            $M_obj = new schoolHolidays();
                            $M_holiday = $M_obj->checkMonthlyHoliday($school->id, $date, $year);
                            ?>
                            <tr>
                                <td><?= $sr ?>.</td>
                                <td><?= $stu->roll_no ?></td>
                                <td><?= ucfirst($stu->section) ?></td>
                                <td><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                <td><?= ucfirst($stu->father_name) ?></td>
                                <td><?php
                                    if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $present = $r_atten['P'];
                                    else: echo $present = '0';
                                    endif;
                                    ?></td>
                                <td><?php
                                    if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
                                    else: echo '0';
                                    endif;
                                    ?></td>
                                <td><?php
                                    if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
                                    else: echo '0';
                                    endif;
                                    ?></td>
                                <td><?= ($working_days - $M_holiday) ?></td>
                                <td><?= number_format(($present / ($working_days - $M_holiday)) * 100, 2) ?>%</td>																				
                            </tr>
                            <?
                            $sr++;
                        endforeach;
                    endif;
                    ?>  
                <? endif; ?>
            </table>
            <?php
        endif;
        if ($a_type == 'half'):
            #Get Section Students
            $QueryOb = new attendance();
            $record = $QueryOb->getHalfAttendance($session_id, $from, $to);
            ?>
            <center><h1>More Than 50% Absent Attendance Records</h1></center>
            <table>
                <th>ROLL No.</th>
                <th>SESSION</th>
                <th>SECTION</th>
                <th>STUDENT NAME</th>
                <th>FATHER NAME</th>
                <th>PARENT’s EMAIL</th>
                <th>PARENT’s CONTACT NO.</th>

                <?
                if (!empty($record)): $sum = '';
                    $sr = 1;
                    foreach ($record as $sk => $stu): $checked = '';
                        ?>
                        <tr>
                            <td><?= $stu->roll_no ?></td>
                            <td><?= $session_info->title ?></td>
                            <td><?= ucfirst($stu->section) ?></td>
                            <td><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                            <td><?= ucfirst($stu->father_name) ?></td>
                            <td><?= $stu->parents_email ?></td>
                            <td><?= $stu->parents_phone ?></td>													
                        </tr>
                        <?
                        $sr++;
                    endforeach;
                endif;
                ?>  
            </table>
            <?php
        endif;
        if ($a_type == 'class_wise'):
            if (empty($date)): $date = date('m');
            endif;
            if (empty($year)): $year = date('Y');
            endif;

            $first_date_of_month = $year . "-" . $date . "-01";
            $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($year . "-" . $date . "-1"))));

            #Get Section Students
            $QueryOb = new attendance();
            $record = $QueryOb->getClassWiseAttendance($school->id, $first_date_of_month, $last_date_of_month);
            ?>
            <center><h1>Class Wise Attendance Report</h1></center>
            <table>
                <tr>
                    <th>SR. NO.</th>
                    <th>SESSION NAME</th>
                    <th>TOTAL STUDENTS</th>
                    <th>PRESENT</th>
                    <th>ABSENT</th>
                    <th>LEAVE</th>
                    <th>WORKING DAYS</th>
                </tr>
                <?
                if (!empty($record)): $sum = '';
                    $sr = 1;
                    foreach ($record as $sk => $object): $checked = '';
                        ?>
                        <tr>
                            <td><?= $sr ?>.</td>
                            <td><?= $object['session_name'] ?></td>
                            <td><?= $object['total_student'] ?></td>
                            <td><?= $object['present'] ?></td>
                            <td><?= $object['absent'] ?></td>
                            <td><?= $object['leave'] ?></td>
                            <td><?= $object['total_days'] ?></td>
                        </tr>
                        <?
                        $sr++;
                    endforeach;
                endif;
                ?>  
            </table>
            <?php
        endif;
        if ($a_type == 'single' && !empty($student_id)):
            #Get Section Students
            $QueryOb = new studentSession();
            $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);

            if (!empty($student_id)):
                #get Student detail	
                $QueryStu = new studentSession();
                $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

                #get Student Permanent Address	
                $QueryP = new studentAddress();
                $s_p_ad = $QueryP->getStudentAddressByType($student_id, 'permanent');

                #get Session Info	
                $session_info = get_object('session', $session_id);
            endif;
            ?>
            <center><h1><?= $student->first_name . " " . $student->last_name ?> Attendance Report</h1></center>
            <table>
                <tr>
                    <td rowspan='8' valign="top">
                        <?
                        if (!empty($student->photo)):
                            $im_obj = new imageManipulation();
                            ?>
                            <img src="<?= $im_obj->get_image('student', 'medium', $student->photo); ?>" />
                        <? else: ?>
                            <img src="assets/img/profile/img.jpg" alt="">
                        <? endif;
                        ?>
                    </td>
                    <th>Admission No.</th>
                    <th>Date of Birth</th>
                    <th>Gender</th>
                    <th>Contact No.</th>									
                </tr>
                <tr>
                    <td><?= $student->reg_id ?></td>
                    <td><?= $student->date_of_birth ?></td>
                    <td><?= $student->sex ?></td>
                    <td><?= $student->phone ?></td>									
                </tr>								
                <tr>
                    <th>Course</th>
                    <th>Name of Student</th>
                    <th>Category</th>
                    <th>Permanent Address</th>									
                </tr>
                <tr>
                    <td><?= $session_info->title ?></td>
                    <td><?= $student->first_name . " " . $student->last_name ?></td>
                    <td><?= $student->category ?></td>
                    <td rowspan='5'>
                        <?
                        if (is_object($s_p_ad)):
                            echo $s_p_ad->address1 . ", " . $s_p_ad->city . ", " . $s_p_ad->state . ", " . $s_p_ad->zip;
                        endif;
                        ?>
                    </td>									
                </tr>							
                <tr>
                    <th>Section</th>
                    <th>Father's Name</th>
                    <th style="border-right: 1px solid #000">Cast</th>																		
                </tr>
                <tr>
                    <td><?= $student->section ?></td>
                    <td><?= $student->father_name ?></td>
                    <td style="border-right: 1px solid #000"><?= $student->cast ?></td>																		
                </tr>
                <tr>									
                    <th>Roll No.</th>
                    <th>Mother's Name</th>
                    <th style="border-right: 1px solid #000">Religion</th>																		
                </tr>
                <tr>
                    <td><?= $student->roll_no ?></td>
                    <td><?= $student->mother_name ?></td>
                    <td style="border-right: 1px solid #000"><?= $student->religion ?></td>																	
                </tr>						
            </table>
            <?
            #set the last date of the session
            if (strtotime($session_info->end_date) > strtotime(date('Y-m-d'))):
                $last_Session_date = date('Y-m-d');
            else:
                $last_Session_date = $session_info->end_date;
            endif;

            $AllMonth = getMonthsArray($session_info->start_date, $last_Session_date);
            ?>
            <table>
                <tr>
                    <td rowspan='2' width='70%' style='text-align:center;vertical-align:middle;'><b>P=Present, A=Absent, L=Leave, H=Holiday</b></td>
                    <td colspan='4' style='text-align:center;'>Attendance Table</td>									
                </tr>	
                <tr>
                    <td><strong>P</strong></td>
                    <td><strong>A</strong></td>
                    <td><strong>L</strong></td>
                    <td><strong>H</strong></td>									
                </tr>	


                <?
                if (!empty($AllMonth)): $P_Total = '0';
                    $A_Total = '0';
                    $L_Total = '0';
                    $H_Total = '0';
                    foreach ($AllMonth as $m_key => $m_val): $st = '1';
                        $st1 = '1';
                        $P = '0';
                        $A = '0';
                        $L = '0';
                        $H = '0';
                        $last_date_of_month = date("d", strtotime("+1 month -1 second", strtotime(date($m_val . '-01'))));
                        ?>	
                        <tr><td style='border:none;'>
                                <table class="table_inner">	
                                    <tr><td rowspan='2' style='vertical-align: middle; width: 300px;' class='head_td'><?= date('M Y', strtotime($m_val . '-01')) ?></td>
                                        <?
                                        while ($st <= $last_date_of_month):
                                            echo "<td>" . $st . "</td>";
                                            $st++;
                                        endwhile;
                                        while ($st <= 31):
                                            echo "<td style='visibility:hidden;'>" . $st . "</td>";
                                            $st++;
                                        endwhile;
                                        ?>
                                    </tr><tr>

                                        <?
                                        while ($st1 <= $last_date_of_month):
                                            if (strlen($st1) == '1'):
                                                $st1 = '0' . $st1;
                                            endif;
                                            #Today Attendance
                                            $QAttObj = new attendance();
                                            $tdy_att = $QAttObj->get_today_attendance($session_id, $ct_sec, $student_id, $m_val . '-' . $st1);

                                            #check Holiday
                                            $M_obj = new schoolHolidays();
                                            $holiday = $M_obj->checkHoliday($school->id, $m_val . '-' . $st1);

                                            if ($tdy_att == 'P'): $P++;
                                                $P_Total = $P_Total + 1;
                                            elseif ($tdy_att == 'A'): $A++;
                                                $A_Total = $A_Total + 1;
                                            elseif ($tdy_att == 'L'): $L++;
                                                $L_Total = $L_Total + 1;
                                            endif;
                                            echo "<td>";
                                            if ($holiday == 'Holiday'): echo "H";
                                                $H++;
                                                $H_Total = $H_Total + 1;
                                            else: echo $tdy_att . "</td>";
                                            endif;
                                            $st1++;
                                        endwhile;
                                        while ($st1 <= 31):
                                            echo "<td style='border-top: none'>" . $st . "</td>";
                                            $st1++;
                                        endwhile;
                                        ?>		
                                    </tr>
                                </table></td>
                            <td class='head_td' style='vertical-align:middle;'><?= $P ?></td>
                            <td class='head_td' style='vertical-align:middle;'><?= $A ?></td>
                            <td class='head_td' style='vertical-align:middle;'><?= $L ?></td>
                            <td class='head_td' style='vertical-align:middle;'><?= $H ?></td>
                        </tr>
                    <? endforeach; ?>

                    <tr>
                        <td style='vertical-align:middle;border-left:none;border-top:none;border-bottom:none;text-align:right'><strong>Total &nbsp;</strong></td>
                        <td class='head_td' style='vertical-align:middle;padding:10px 1px 10px 0;' ><?= $P_Total ?></td>
                        <td class='head_td' style='vertical-align:middle;'><?= $A_Total ?></td>
                        <td class='head_td' style='vertical-align:middle;'><?= $L_Total ?></td>
                        <td class='head_td' style='vertical-align:middle;'><?= $H_Total ?></td>
                    </tr>
                <? endif; ?>
            </table>
        <?php endif; ?>
    </div>
<?php } ?>