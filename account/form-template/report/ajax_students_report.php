<?php
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');


isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = 'A';
isset($_REQUEST['interval_id']) ? $interval_id = $_REQUEST['interval_id'] : $interval_id = '';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
isset($_REQUEST['report_type']) ? $report_type = $_REQUEST['report_type'] : $report_type = 'admission_detail';
isset($_REQUEST['date']) ? $date = $_REQUEST['date'] : $date = '';
isset($_REQUEST['date1']) ? $date1 = $_REQUEST['date1'] : $date1 = '0';
isset($_REQUEST['month']) ? $month = $_REQUEST['month'] : $month = '';
isset($_REQUEST['exam']) ? $exam = $_REQUEST['exam'] : $exam = '0';
isset($_REQUEST['v_report_type']) ? $v_report_type = $_REQUEST['v_report_type'] : $v_report_type = 'vehicle_info';
isset($_REQUEST['s_report_type']) ? $s_report_type = $_REQUEST['s_report_type'] : $s_report_type = 'recieve';
isset($_REQUEST['f_report_type']) ? $f_report_type = $_REQUEST['f_report_type'] : $f_report_type = 'recieve';
isset($_REQUEST['e_report_type']) ? $e_report_type = $_REQUEST['e_report_type'] : $e_report_type = 'live';
isset($_REQUEST['ex_type']) ? $ex_type = $_REQUEST['ex_type'] : $ex_type = '';
isset($_REQUEST['a_type']) ? $a_type = $_REQUEST['a_type'] : $a_type = '';
isset($_REQUEST['f_type']) ? $f_type = $_REQUEST['f_type'] : $f_type = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['subject']) ? $subject = $_REQUEST['subject'] : $subject = '';

isset($_REQUEST['tmp']) ? $tmp = $_REQUEST['tmp'] : $tmp = 'student';


isset($_REQUEST['from']) ? $from = $_REQUEST['from'] : $from = '';
isset($_REQUEST['to']) ? $to = $_REQUEST['to'] : $to = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = '';

#report type array
$report_array = array(
    "withdrawal_detail" => "Admission & Withdrawal Register",
    "student_type" => "Granted Concession Students Detail",
    "contact_detail" => "Photo,Address And Contact Detail",
    "admission_detail" => "Registration & Date Of Birth",
    "category_detail" => "SC,ST & OBC Student Detail",
    "address_label" => "Students Address Label",
    "subject_detail" => "Subjects Detail"
);

#get_all session 
$QuerySession = new session();
$session_array = $QuerySession->listAll($school->id, '1', 'array');

// Get Session Detail By Id
if (!empty($session_id)):
    $obj = new session;
    $session_obj = $obj->getRecord($session_id);
else:
    $obj = new session;
    $session_obj = $obj->getRecord($session_array['0']->id);
endif;

if (empty($session_id)):
    if (!empty($session_array)):
        $session_id = $session_array['0']->id;
    endif;
endif;

#get Student Report 
$StudentObj = new studentSession();
$record = $StudentObj->get_various_student_report($school->id, $session_id, $ct_sec, $report_type);

if ($report_type == 'category_detail' && !empty($record)):
    foreach ($record as $key => $value):
        if (($value->category == '') || ($value->category == 'None') || ($value->category == 'GEN')):
            unset($record[$key]);
        endif;
    endforeach;
endif;


if (isset($_POST)) {
    ?>
    <style>
        .print_tables table {
            width: 100%;
            border: 1px solid #000;
        }
        .print_tables td {
            border-top: 1px solid #000;
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables th {
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables table tr td:last-child {
            border-right: none;
        }
        .print_tables table tr th:last-child {
            border-right: none;
        }
    </style>

    <div class="print_tables">
        <? if ($report_type == "admission_detail"): ?>	
            <center>
                <h1><?= $report_array[$report_type] ?></h1>
                <h2><?= $session_obj->title; ?> (<?= $ct_sec; ?>)</h2>
            </center>								
            <table>
                <tr>
                    <th>SR. NO.</th>
                    <th>DATE OF ADMISSION</th>
                    <th>ADMISSION NO.</th>
                    <th>ROLL NO.</th>
                    <th>STUDENT NAME</th>
                    <th>GENDER</th>
                    <th>FATHER'S NAME</th>
                    <th>MOTHER'S NAME</th>
                    <th>DATE OF BIRTH</th>
                    <th>CATEGORY</th>
                </tr>
                <?
                if (!empty($record)): $sum = '';
                    $sr = 1;
                    foreach ($record as $sk => $stu):
                        $QueryP = new studentAddress();
                        $s_p_ad = $QueryP->getStudentAddressByType($stu->id, 'permanent');
                        ?>																		

                        <tr>
                            <td><?= $sr ?>.</td>
                            <td><?= date('d M, Y', strtotime($stu->date_of_admission)) ?></td>
                            <td><?= $stu->reg_id ?></td>
                            <td><?= $stu->roll_no ?></td>
                            <td><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                            <td><?= $stu->sex ?></td>
                            <td><?= ucfirst($stu->father_name) ?></td>
                            <td><?= ucfirst($stu->mother_name) ?></td>
                            <td><?= date('d M, Y', strtotime($stu->date_of_birth)) ?></td>
                            <td><?= $stu->category ?></td>
                        </tr>
                        <?
                        $sr++;
                    endforeach;
                endif;
                ?>  
            </table>
        <?php elseif ($report_type == "withdrawal_detail"): ?>	
            <center>
                <h1><?= $report_array[$report_type] ?></h1>
                <h2><?= $session_obj->title; ?> (<?= $ct_sec; ?>)</h2>
            </center>								
            <table>
                <tr>
                    <th>SR. NO.</th>
                    <th>DATE OF ADMISSION</th>
                    <th>ADMISSION NO.</th>
                    <th>ROLL NO.</th>
                    <th>STUDENT NAME</th>
                    <th>GENDER</th>
                    <th>FATHER'S NAME</th>
                    <th>MOTHER'S NAME</th>
                    <th>DATE OF BIRTH</th>
                    <th>CATEGORY</th>
                </tr>
                <? if (!empty($record)): $sum = ''; ?>
                    <?php
                    $sr = 1;
                    foreach ($record as $sk => $stu):
                        ?>																		

                        <tr class="odd gradeX">
                            <td><?= $sr ?>.</td>
                            <td><?= date('d M, Y', strtotime($stu->date_of_admission)) ?></td>
                            <td><?= $stu->reg_id ?></td>
                            <td><?= $stu->roll_no ?></td>
                            <td><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                            <td><?= $stu->sex ?></td>
                            <td><?= ucfirst($stu->father_name) ?></td>
                            <td><?= ucfirst($stu->mother_name) ?></td>
                            <td><?= date('d M, Y', strtotime($stu->date_of_birth)) ?></td>
                            <td><?= $stu->category ?></td>
                        </tr>
                        <?
                        $sr++;
                    endforeach;
                    ?>	
                <?php endif; ?>  
            </table>
        <? elseif ($report_type == "student_type"): ?>			
            <center>
                <h1><?= $report_array[$report_type] ?></h1>
                <h2><?= $session_obj->title; ?> (<?= $ct_sec; ?>)</h2>
            </center>						
            <table>
                <tr>
                    <th>Reg ID</th>
                    <th>Roll NO.</th>
                    <th>Student Name</th>
                    <th>Father Name</th>
                    <th>Student Granted</th>
                    <th>Concession</th>
                </tr>
                <? if (!empty($record)): $sum = ''; ?>
                    <?php
                    $sr = 1;
                    foreach ($record as $key => $object):
                        ?>
                        <tr class="odd gradeX">
                            <td><?= $object->reg_id ?></td>
                            <td><?= $object->roll_no ?></td>
                            <td><?= ucfirst($object->first_name) . " " . $object->last_name ?></td>
                            <td><?= $object->father_name ?></td>
                            <td class="hidden-480 sorting_disabled"><?
                                if (empty($object->concession)):echo "None";
                                else: echo $object->concession;
                                endif;
                                ?></td>
                            <td><?= number_format($object->concession_type, 2) ?></td>
                        </tr>
                        <?php
                        $sr++;
                    endforeach;
                    ?>
                <?php endif; ?>  
            </table>
        <? elseif ($report_type == "contact_detail"): ?>			
            <center>
                <h1><?= $report_array[$report_type] ?></h1>
                <h2><?= $session_obj->title; ?> (<?= $ct_sec; ?>)</h2>
            </center>						
            <table>
                <tr>
                    <th>Reg ID</th>
                    <th>Roll NO.</th>
                    <th>Name</th>
                    <th>Father Name</th>
                    <th>Contact No.</th>
                    <th class="hidden-480 sorting_disabled">Permanent Address</th>
                    <th>Photo</th>
                </tr>
                <? if (!empty($record)): $sum = ''; ?>
                    <?php
                    $sr = 1;
                    foreach ($record as $key => $object):
                        #get student permanent address
                        $QueryP = new studentAddress();
                        $s_p_ad = $QueryP->getStudentAddressByType($object->id, 'permanent');

                        #get student correspondence address
                        $QueryC = new studentAddress();
                        $s_c_ad = $QueryC->getStudentAddressByType($object->id, 'correspondence');
                        ?>
                        <tr>
                            <td><?= $object->reg_id ?></td>
                            <td><?= $object->roll_no ?></td>
                            <td><?= ucfirst($object->first_name) . " " . $object->last_name ?></td>
                            <td><?= $object->father_name ?></td>
                            <td><?= $object->phone ?></td>
                            <td>
                                <?
                                if (is_object($s_p_ad)):
                                    echo $s_p_ad->address1 . ", " . $s_p_ad->city . ", " . $s_p_ad->tehsil . ", " . $s_p_ad->district . ", " . $s_p_ad->state . ", " . $s_p_ad->zip;
                                endif;
                                ?>
                            </td>
                            <td>
                                <?
                                if (!empty($object->photo)):
                                    $im_obj = new imageManipulation()
                                    ?>
                                    <img src="<?= $im_obj->get_image('student', 'thumb', $object->photo); ?>" style='width:112px;'/>
                                <? endif; ?>	
                            </td>
                        </tr>
                        <?php
                        $sr++;
                    endforeach;
                    ?>
                <?php endif; ?>  
            </table>
        <? elseif ($report_type == "subject_detail"): ?>			
            <center>
                <h1><?= $report_array[$report_type] ?></h1>
                <h2><?= $session_obj->title; ?> (<?= $ct_sec; ?>)</h2>
            </center>								
            <table>
                <tr>
                    <th>Reg ID</th>
                    <th>ROLL NO.</th>
                    <th>Student Name</th>
                    <th>Father Name</th>												
                    <?
                    $session_detail = get_object('session', $session_id);

                    $QueryStComSub = new subject();
                    $cmpSub = $QueryStComSub->GetSessionSubjectsInArray($session_detail->compulsory_subjects);

                    if (!empty($cmpSub)):
                        foreach ($cmpSub as $key => $val):
                            echo "<th>SUB " . ($key + 1) . "</th>";
                        endforeach;
                    endif;
                    ?>
                    <th>Elective Subjects</th>												
                </tr>
                <? if (!empty($record)): $sum = ''; ?>
                    <?php
                    $sr = 1;
                    foreach ($record as $key => $object):
                        #get the student subject list if enter previous
                        $session = get_object('session', $session_id);

                        $QueryStElcSub = new studentSubject();
                        $elcSub = $QueryStElcSub->getStudentSubjectTitle($session_id, $object->id, 'Elective');
                        ?>
                        <tr>
                            <td><?= $object->reg_id ?></td>
                            <td><?= $object->roll_no ?></td>
                            <td><?= ucfirst($object->first_name) . " " . $object->last_name ?></td>
                            <td><?= ucfirst($object->father_name) ?></td>
                            <?
                            if (!empty($cmpSub)):
                                foreach ($cmpSub as $Ckey => $Cval):
                                    echo "<td>" . ($Cval) . "</td>";
                                endforeach;
                            endif;
                            ?>	
                            <td><?= $elcSub ?></td>
                        </tr>
                        <?php
                        $sr++;
                    endforeach;
                    ?>
                <?php endif; ?>  
            </table>
        <? elseif ($report_type == "category_detail"): ?>			
            <center>
                <h1><?= $report_array[$report_type] ?></h1>
                <h2><?= $session_obj->title; ?> (<?= $ct_sec; ?>)</h2>
            </center>								
            <table>
                <tr>
                    <th>SR. NO.</th>
                    <th>DATE OF ADMISSION</th>
                    <th>ADMISSION NO.</th>
                    <th>ROLL NO.</th>
                    <th>STUDENT NAME</th>
                    <th>FATHER'S NAME</th>
                    <th>MOTHER'S NAME</th>
                    <th>CATEGORY</th>
                </tr>
                <? if (!empty($record)): $sum = ''; ?>
                    <?php
                    $sr = 1;
                    foreach ($record as $key => $stu):
                        ?>
                        <tr>
                            <td><?= $sr ?>.</td>
                            <td><?= date('d M, Y', strtotime($stu->date_of_admission)) ?></td>
                            <td ><?= $stu->reg_id ?></td>
                            <td ><?= $stu->roll_no ?></td>
                            <td ><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                            <td><?= ucfirst($stu->father_name) ?></td>
                            <td><?= ucfirst($stu->mother_name) ?></td>
                            <td><?= $stu->category ?></td>
                        </tr>
                        <?php
                        $sr++;
                    endforeach;
                    ?>
                <?php endif; ?>  
            </table>
        <? elseif ($report_type == "address_label"): ?>	
            <center>
                <h1><?= $report_array[$report_type] ?></h1>
                <h2><?= $session_obj->title; ?> (<?= $ct_sec; ?>)</h2>
            </center>								
            <? if (!empty($record)): $sum = ''; ?>
                <div class="row-fluid">
                    <?php
                    $sr = 1;
                    foreach ($record as $key => $stu):
                        #get student permanent address
                        $QueryP = new studentAddress();
                        $s_p_ad = $QueryP->getStudentAddressByType($stu->id, 'permanent');
                        ?>											
                        <div class='span6'>
                            <div id='custom_address'>
                                <strong><?= strtoupper($stu->first_name . " " . $stu->last_name) ?></strong><br/>
                                S/o. | D/o. <?= strtoupper($stu->father_name) ?><br/><br/>
                                <div class='span11'>
                                    <strong>Address</strong><br/>
                                    <?
                                    if (is_object($s_p_ad)):
                                        echo $s_p_ad->address1 . ", " . $s_p_ad->city . "<br/>" . $s_p_ad->state . ", " . $s_p_ad->zip;
                                    endif;
                                    ?>
                                </div><div class="clearfix"></div>
                            </div>
                        </div>
                        <?php
                        if ($sr % 2 == '0'): echo "</div><br/><div class='row-fluid'>";
                        endif;
                        $sr++;
                    endforeach;
                    ?>
                    <div class="clearfix"></div>
                </div>	
            <?php else: ?>
                Sorry, No Record Found..!
            <?php endif; ?> 
        <? endif; ?>
    </div>
<?php } ?>