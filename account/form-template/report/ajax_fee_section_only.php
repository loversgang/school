<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');

if($_POST):
isset($_POST['session_id'])?$session_id=$_POST['session_id']:$session_id='';
	
		/* Get session Pages*/
		$QuerySec=new studentSession();
        $QuerySec->listAllSessionSection($session_id);
		 
		#get Session Info
		$QueryS=new session();
		$session=$QueryS->getRecord($session_id);		 

		#get Session Interval
		$QueryInt=new session_interval();
		$interval=$QueryInt->listOfInterval($session_id);	
		
		$current_date=date('Y-m-d');
		$current='0';
 ?>

					<? if($QuerySec->GetNumRows()>0): ?>
						<div class='span8'>
							<div class="control-group" >
							<label class="control-label">Select Section </label>
								<div class="controls">								
								<select class="select2_category span12" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
									<?php $session_sc=1;while($sec=$QuerySec->GetObjectFromRecord()): ?>
											<option value='<?=$sec->section?>'><?=ucfirst($sec->section)?></option>
									<? $session_sc++; endwhile;?>
								</select>
								</div>
							</div>
						</div>	
					<? else:?>
							<div class='span7'>
								<div class="control-group" >
								<label class="control-label">&nbsp;</label>
									<div class="controls">							
										<input type='text' value='Sorry, No Section Found.' disabled />
									</div>	
								</div>	
							</div>	
					<? endif;?>

				<div id='show_students'></div>
<?		
endif;
?>