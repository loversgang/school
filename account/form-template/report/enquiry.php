
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Enquiry Reports
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li class="last">
                    Enquiry Reports
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>  			
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-left row-fluid">
        <form class="" action="<?php echo make_admin_url('report', 'enquiry', 'enquiry') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
            <input type='hidden' name='Page' value='report'/>
            <input type='hidden' name='action' value='enquiry'/>
            <input type='hidden' name='section' value='enquiry'/>

            <div class="control-group alert alert-success span12" style='margin-left:0px;padding-right:0px;'>

                <div class="row-fluid">	

                    <div class='span4'>
                        <div class='span12' style='margin-left: 5px;' id='ajex_section'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Report Type</label>
                            <? if (!empty($enquiry_report_array)): ?>
                                <select name="e_report_type" id="enquiry" class="select2_category span11">
                                    <? foreach ($enquiry_report_array as $rp_key => $rp_val): ?>
                                        <option value='<?= $rp_key ?>' <?
                                        if ($e_report_type == $rp_key): echo "selected";
                                        endif;
                                        ?>><?= $rp_val ?></option>
                                            <? endforeach; ?>
                                </select>
                            <? else: ?>
                                <input type='text' value='Sorry, No Enquiry Found.' disabled />
                            <? endif; ?>	
                        </div>
                    </div>
                    <div class='span8' style='margin-left: 0px;'>
                        <div class='span12' style='margin-left: 5px;'>
                            <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
                            <div class='span3' style='margin-left: 0px;'>
                                <input type='submit' name='go' value='View Result' class='btn green medium span11'/>
                            </div>
                            <div class='span9'>
                                <? if (!empty($record)): ?>
                                    <a class='btn yellow medium span3' href="<?= make_admin_url('report', 'download', 'download&e_report_type=' . $e_report_type . '&tmp=enquiry') ?>"><i class="icon-download"></i> PDF</a>

                                <? endif; ?>
                            </div>
                        </div>	
                    </div>							
                </div>

                <div class="clearfix"></div>
            </div>

        </form>										
    </div>	


    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">					
            <!-- BEGIN EXAMPLE TABLE PORTLET-->

            <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-list"></i><?= $enquiry_report_array[$e_report_type] ?></div>								
                </div>
                <div class="portlet-body">
                    <div class="clearfix">
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th class="hidden-480">Name</th>
                                    <th class="hidden-480">Father Name</th>
                                    <th style='text-align:center' class="hidden-480">On Date</th>
                                    <th class="hidden-480 sorting_disabled">City</th>
                                    <th style='text-align:center' class="hidden-480 sorting_disabled">Mobile NO.</th>												
                                </tr>
                            </thead>
                            <? if (!empty($record)): $sum = ''; ?>
                                <tbody>
                                    <?php
                                    $sr = 1;
                                    foreach ($record as $key => $object):
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?= $sr ?>.</td>
                                            <td class="hidden-480"><?= $object->name ?></td>
                                            <td class="hidden-480"><?= $object->father_name ?></td>
                                            <td style='text-align:center' class="hidden-480"><?= date('d M, Y', strtotime($object->on_date)) ?></td>
                                            <td class="hidden-480"><?= $object->city ?></td>														
                                            <td style='text-align:center' class="hidden-480"><?= $object->phone ?></td>

                                        </tr>
                                        <?php
                                        $sr++;
                                    endforeach;
                                    ?>
                                </tbody>
                            <?php endif; ?>  
                        </table>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="clearfix"></div>
</div>
<script type="text/javascript">

    /* Print Document */
    $("#print").live("click", function () {
        var session_id = '<?= $session_id ?>';
        var ct_sec = '<?= $ct_sec ?>';
        var report_type = '<?= $e_report_type ?>';
        if (report_type === '') {
            alert("Sorry, You can't print this document..!");
            return false;
        }

        var dataString = '&session_id=' + session_id + '&ct_sec=' + ct_sec + '&e_report_type=' + report_type;

        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_enquiry', 'ajax_enquiry&temp=report'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
</script>