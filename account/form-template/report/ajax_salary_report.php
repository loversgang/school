<?php
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');


isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = 'A';
isset($_REQUEST['interval_id']) ? $interval_id = $_REQUEST['interval_id'] : $interval_id = '';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
isset($_REQUEST['report_type']) ? $report_type = $_REQUEST['report_type'] : $report_type = 'admission_detail';
isset($_REQUEST['date']) ? $date = $_REQUEST['date'] : $date = '';
isset($_REQUEST['date1']) ? $date1 = $_REQUEST['date1'] : $date1 = '0';
isset($_REQUEST['month']) ? $month = $_REQUEST['month'] : $month = '';
isset($_REQUEST['exam']) ? $exam = $_REQUEST['exam'] : $exam = '0';
isset($_REQUEST['v_report_type']) ? $v_report_type = $_REQUEST['v_report_type'] : $v_report_type = 'vehicle_info';
isset($_REQUEST['s_report_type']) ? $s_report_type = $_REQUEST['s_report_type'] : $s_report_type = 'recieve';
isset($_REQUEST['f_report_type']) ? $f_report_type = $_REQUEST['f_report_type'] : $f_report_type = 'recieve';
isset($_REQUEST['e_report_type']) ? $e_report_type = $_REQUEST['e_report_type'] : $e_report_type = 'live';
isset($_REQUEST['ex_type']) ? $ex_type = $_REQUEST['ex_type'] : $ex_type = '';
isset($_REQUEST['a_type']) ? $a_type = $_REQUEST['a_type'] : $a_type = '';
isset($_REQUEST['f_type']) ? $f_type = $_REQUEST['f_type'] : $f_type = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['subject']) ? $subject = $_REQUEST['subject'] : $subject = '';

isset($_REQUEST['tmp']) ? $tmp = $_REQUEST['tmp'] : $tmp = 'student';


isset($_REQUEST['from']) ? $from = $_REQUEST['from'] : $from = '';
isset($_REQUEST['to']) ? $to = $_REQUEST['to'] : $to = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = '';

if (isset($_POST)) {
    extract($_POST);
    ?>
    <style>
        .print_tables table {
            width: 100%;
            border: 1px solid #000;
        }
        .print_tables td {
            border-top: 1px solid #000;
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables th {
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables table tr td:last-child {
            border-right: none;
        }
        .print_tables table tr th:last-child {
            border-right: none;
        }
    </style>
    <div class="print_tables">
        <?php
        #Get Staff Category
        $QuerySalCat = new staffCategory();
        $QuerySalCat->listAll($school->id);

        #Get Records for salary
        $QueryObj = new staffSalary();
        $record = $QueryObj->SalaryReportMonthWise($school->id, $month, $s_report_type, $cat);

        $first_date_of_month = date('Y-m-d', strtotime('01-' . $month));
        $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($first_date_of_month))));

        $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
        $total_month_days = (date('d', strtotime($last_date_of_month)));
        $working_days = $total_month_days - $total_sunday;

        $M_obj = new schoolHolidays();
        $M_holiday = $M_obj->checkMonthlyHoliday($school->id, date('m', strtotime($first_date_of_month)), date('Y', strtotime($first_date_of_month)));
        if ($s_report_type == 'pending'):
            ?>
            <center><h1>Pending Salary</h1></center>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Staff Name</th>
                        <th>Staff Category</th>
                        <th>Present</th>
                        <th>Absent</th>
                        <th>Leave</th>
                        <th>Half Day</th>
                    </tr>
                </thead>
                <? if (!empty($record)): $sum = ''; ?>
                    <tbody>
                        <?php
                        $sr = 1;
                        foreach ($record as $key => $object):
                            $Q_obj = new staffAttendance();
                            $r_atten = $Q_obj->GetStaffAttendace($object->id, $first_date_of_month, $last_date_of_month);
                            $t_attend = array_sum($r_atten);
                            ?>
                            <tr class="odd gradeX">	
                                <td><?= $sr ?>.</td>												
                                <td><?= $object->title . ' ' . ucfirst($object->first_name) . ' ' . $object->last_name ?></td>
                                <td class="hidden-480 sorting_disabled"><?= $object->category ?></td>
                                <td style='text-align:center'><?
                                    if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $r_atten['P'];
                                    else: echo '0';
                                    endif;
                                    ?></td>
                                <td style='text-align:center'><?
                                    if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
                                    else: echo '0';
                                    endif;
                                    ?></td>
                                <td style='text-align:center'><?
                                    if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
                                    else: echo '0';
                                    endif;
                                    ?></td>
                                <td style='text-align:center'><?
                                    if (!empty($r_atten) && array_key_exists('H', $r_atten)): echo $r_atten['H'];
                                    else: echo '0';
                                    endif;
                                    ?></td>
                                <?php
                                $sr++;
                            endforeach;
                            ?>
                    </tbody>
                <?php endif; ?>  
            </table>
            <?php
        else:
            ?>
            <center><h1>Paid Salary</h1></center>
            <table>
                <thead>
                    <tr>
                        <th>Staff Name</th>
                        <th>Staff Category</th>
                        <th>Present</th>
                        <th>Absent</th>
                        <th>Leave</th>
                        <th>Half Day</th>
                        <th style='text-align:center'>Date</th>
                        <th style='text-align:right;'>Deduction (Rs.)</th>
                        <th style='text-align:right;'>Salary (Rs.)</th>
                    </tr>
                </thead>
                <? if (!empty($record)): $sum = ''; ?>
                    <tbody>
                        <?php
                        $sr = 1;
                        foreach ($record as $key => $object):
                            $Q_obj = new staffAttendance();
                            $r_atten = $Q_obj->GetStaffAttendace($object->id, $first_date_of_month, $last_date_of_month);
                            $t_attend = array_sum($r_atten);
                            ?>
                            <tr class="odd gradeX">													
                                <td><?= $object->title . ' ' . ucfirst($object->first_name) . ' ' . $object->last_name ?></td>
                                <td class="hidden-480 sorting_disabled"><?= $object->category ?></td>
                                <td style='text-align:center'><?
                                    if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $r_atten['P'];
                                    else: echo '0';
                                    endif;
                                    ?></td>
                                <td style='text-align:center'><?
                                    if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
                                    else: echo '0';
                                    endif;
                                    ?></td>
                                <td style='text-align:center'><?
                                    if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
                                    else: echo '0';
                                    endif;
                                    ?></td>
                                <td style='text-align:center'><?
                                    if (!empty($r_atten) && array_key_exists('H', $r_atten)): echo $r_atten['H'];
                                    else: echo '0';
                                    endif;
                                    ?></td>
                                <td style='text-align:center' class="hidden-480 sorting_disabled"><?= $object->payment_date ?></td>
                                <td style='text-align:right;'>Rs.<?= number_format($object->deduction, 2) ?></td>
                                <td style='text-align:right;'>Rs.<?= number_format($object->amount, 2) ?></td>													
                            </tr>
                            <?php
                            $sr++;
                            $sum = $sum + $object->amount;
                        endforeach;
                        ?>
                    </tbody>
                <?php endif; ?>  
            </table>
        <?php endif; ?>
    </div>
<?php } ?>