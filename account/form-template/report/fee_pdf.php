	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
<style>
	body {
		color: #000000;
		direction: ltr;
		font-family: 'Open Sans';
		font-size: 12px;
		background-color: #FFF !important;
	}

.portlet.box.yellow {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color #FCCB7E #FCCB7E;
    border-image: none;
    border-right: 1px solid #FCCB7E;
    border-style: none solid solid;
    border-width: 0 1px 1px;
}
.portlet.box {
    padding: 0 !important;
}
.portlet-body.yellow, .portlet.yellow {
    background-color: #FFB848 !important;
}
.portlet {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}

.caption{
    display: inline-block;
    float: left;
    font-size: 18px;
    font-weight: 400;
    margin: 0 0 7px;
    padding: 0 0 8px;
	background:#D9EDF7;
	padding:5px;
}
table th{
	background:#BF504D;
	color:#fff;	
}
#small_table th{
	background:#BF504D;
	color:#fff;	
	border:1px solid;
	line-height: 11px;
}
.table thead tr th {
    font-size: 11px !important;
    font-weight: 600;
}
.title{
	background:#D9EDF7;
	padding-left:5px;
	padding-right:5px;
	padding-top:5px;
}
.main_title1{
	margin-top:5px;
}
#small_table_new td{
    border: 1px solid;
    width: 25.4%;
}
#sum_amt{
	background:#FFFF00;
	color:#FF1B00;
	font-weight:bold;
}
</style>

<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');


isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['page'])?$page=$_GET['page']:$page='1';
isset($_REQUEST['session_id'])?$session_id=$_REQUEST['session_id']:$session_id='';
isset($_REQUEST['ct_sect'])?$ct_sect=$_REQUEST['ct_sect']:$ct_sect='A';
isset($_REQUEST['ct_sec'])?$ct_sec=$_REQUEST['ct_sec']:$ct_sec='A';
isset($_REQUEST['interval_id'])?$interval_id=$_REQUEST['interval_id']:$interval_id='';
isset($_REQUEST['student_id'])?$student_id=$_REQUEST['student_id']:$student_id='';
isset($_REQUEST['f_type'])?$f_type=$_REQUEST['f_type']:$f_type='';

	$session=get_object('session',$session_id);

		if($f_type=='month_wise'):
			if(empty($session_id)):
					if(!empty($session_array)):
						//$session_id=$session_array['0']->id;
					endif;
				endif;
				
				/* Get session Pages*/
				$QuerySec=new studentSession();
				$QuerySec->listAllSessionSection($session_id);	
				
				#Get Section Students
				$QueryOb=new studentSession();
				$record=$QueryOb->sectionSessionStudents($session_id,$ct_sec);
					
				$session=get_object('session',$session_id);
					
				#get Session Interval
				$QueryInt=new session_interval();
				$interval=$QueryInt->listOfInterval($session_id);	
				
				$current_date=date('Y-m-d');
				$current='0';
				
		elseif($f_type=='head_wise'):
			if(empty($session_id)):
					if(!empty($session_array)):
						//$session_id=$session_array['0']->id;
					endif;
				endif;
				
				/* Get session Pages*/
				$QuerySec=new studentSession();
				$QuerySec->listAllSessionSection($session_id);	
				
				#Get Section Students
				$QueryOb=new studentSession();
				$record=$QueryOb->sectionSessionStudents($session_id,$ct_sec);
				
				$session=get_object('session',$session_id);
					
				#get Session Interval
				$QueryInt=new session_interval();
				$interval=$QueryInt->listOfInterval($session_id);	
				
				#Get Session Fee Heads
				$QuerySess = new session_fee_type();
				$Sesspay_heads=$QuerySess->sessionPayHeadsWithKey($session_id);	
				
				$current_date=date('Y-m-d');
				$current='0';		

				$interval_detail=get_object('session_interval',$interval_id);	
				
		elseif($f_type=='class_due'):
			if(empty($session_id)):
					if(!empty($session_array)):
						//$session_id=$session_array['0']->id;
					endif;
				endif;
				
				/* Get session Pages*/
				$QuerySec=new studentSession();
				$QuerySec->listAllSessionSection($session_id);	
				
				#Get Section Students
				$QueryOb=new studentSession();
				$record=$QueryOb->getIntervalDuePendingFee($session_id,$ct_sec);					
				
		elseif($f_type=='head_conc'):
				#concession list array
				$conc_heads=array('Full Poverty'=>'0','Half Poverty'=>'0','Scholarship'=>'0','Special Student'=>'0','Sports Student'=>'0','Sibling Student'=>'0');
		
				if(empty($session_id)):
					if(!empty($session_array)):
						$session_id=$session_array['0']->id;
					endif;
				endif;
				
				/* Get session Pages*/
				$QuerySec=new studentSession();
				$QuerySec->listAllSessionSection($session_id);	
				
				#Get Section Students
				$QueryOb=new studentSession();
				$record=$QueryOb->getAllConcessionWiseAmount($session_id,$ct_sec);
				
				$session=get_object('session',$session_id);
				
				$current_date=date('Y-m-d');
				$current='0';
		endif;

?>

<? if(!empty($record)):?>
  <!-- BEGIN PAGE CONTAINER-->
    <div style='margin:20px;border:1px solid;'>
            <!-- BEGIN PAGE HEADER-->

						
					<? if($f_type=="class_due"):
							?>	
							<div class="portlet-body">
								<table width='100%'>
									<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;padding:5px;"><br/>CHECK LIST OF CLASS WISE FEES DUE LIST WITH CONTACT DETAILS</td></tr>
									<tr><td class='title'><strong>SESSION:- <?=$session->title?>, SECTION:- <?=$ct_sec?></strong></td><td class='title' style='text-align:right;'><strong></strong></td></tr>
								</table>
							
							<div style='clear:both;'></div>
							
								<div class="clearfix">
								
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="hidden-480">REG. NO.</th>
											<th class="hidden-480" >STUDENT NAME</th>
											<th class="hidden-480" >FATHER NAME</th>
											<th class="hidden-480" >CONTACT NO.</th>
											<th class="hidden-480" >RECEIVABLE AMOUNT</th>
											<th class="hidden-480" >RECEIVED AMOUNT</th>	
											<th class="hidden-480" style='text-align:right;'>BALANCE AMOUNT</th>											
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';  $balance='';?>
												<tbody>														
												<? foreach($record as $key=>$stu):?>
													<tr class="odd gradeX">
														<td ><?=$stu->reg_id?></td>
														<td><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
														<td><?=$stu->father_name?></td>
														<td><?=$stu->family_phone?></td>
														<td><?=number_format($stu->total,2)?></td>
														<td><?=number_format($stu->paid,2)?></td>
														<td style='text-align:right;'><?=number_format($stu->total-$stu->paid,2)?></td>
													</tr>
												<? 	$sum=$sum+($stu->total-$stu->paid);
													endforeach;?>	
													<tr><td></td><td></td><td></td>
																										
													<td></td><td></td><td id='sum_amt'> TOTAL</td>
													<td id='sum_amt' style='text-align:right;'><?=number_format($sum,2)?></td>
													</tr>													
												</tbody>
									   <?php endif;?>  
									</table>				
								</div>
							</div>	
					<? elseif($f_type=="head_conc"):
							?>	
							<div class="portlet-body">
								<table width='100%'>
									<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;padding:5px;"><br/>CHECK LIST OF HEAD WISE CONCESSION DETAILS</td></tr>
									<tr><td class='title'><strong>SESSION:- <?=$session->title?>, SECTION:- <?=$ct_sec?></strong></td><td class='title' style='text-align:right;'><strong></strong></td></tr>
								</table>
							
							<div style='clear:both;'></div>
							
								<div class="clearfix">
								
								<table class="table table-striped table-bordered table-hover">
									<thead>
										 <tr>
											<th>SR NO.</th>
											<th>HEAD NAME</th>
											<th>AMOUNT</th>
										</tr>
									</thead>
										<tbody>
										<? if(!empty($conc_heads)): $sr='1'; $sum=''; $ct_total='';
											foreach($conc_heads as $s_key=>$s_value):?>
												<tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td><?=$s_key?></td>
													<td><? if(array_key_exists($s_key,$record)): $ct_total=$record[$s_key]; echo number_format($ct_total,2); else:  $ct_total=$s_value; echo number_format($ct_total,2); endif;?></td>													
												</tr>	
										<?	$sum=$sum+$ct_total;		$sr++;
											endforeach;?>
												<tr><td></td><td id='sum_amt'> TOTAL</td>
													<td id='sum_amt'><?=number_format($sum,2)?></td>
												</tr>							
										<?	endif;?>																	
										</tbody>
								</table>			
								</div>
							</div>	
					
					
					<? elseif($f_type=="head_wise"):
							?>	
							<div class="portlet-body">
								<table width='100%'>
									<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;padding:5px;"><br/>CHECK LIST OF HEAD EXPECTED COLLECTION DETAILS</td></tr>
									<tr><td class='title'><strong>SESSION:- <?=$session->title?>, SECTION:- <?=$ct_sec?></strong></td><td class='title' style='text-align:right;'><strong></strong></td></tr>
								</table>
							
							<div style='clear:both;'></div>
							
								<div class="clearfix">
								
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>SR. NO.</th>
											<th>Reg ID</th>
											<th>Student Name</th>
											<th>Father Name</th>
											<? if(!empty($Sesspay_heads)):
													foreach($Sesspay_heads as $s_key=>$s_value):
														if($interval_detail->interval_position=='1'):?>
															<th class="hidden-480" style='text-align:center;'><?=$s_value['title']?></th>
											<? 			else:
															if($s_value['type']=='Regular'):?>
																<th class="hidden-480" style='text-align:center;'><?=$s_value['title']?></th>	
											<?				endif;
														endif;
													endforeach;?>
												<th class="hidden-480" style='text-align:right;'>Total Amount</th>
											<?	endif;?>										
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';  $balance='';$sr='1';?>
												<tbody>														
												<? foreach($record as $key=>$stu):?>
													<tr class="odd gradeX">
														<td><?=$sr?>.</td>
														<td ><?=$stu->reg_id?></td>
														<td><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
														<td ><?=$stu->father_name?></td>
														<? if(!empty($Sesspay_heads)): $Head_total=''; 
																foreach($Sesspay_heads as $s_kkey=>$s_vvalue):
																		#Get Head fee detail
																		$StuHeadObj= new session_student_fee();
																		$HeadAmount=$StuHeadObj->getSingleStudentFeeHeadValue($session_id,$stu->id,$s_vvalue['fee_id']); 
																		
																	if($interval_detail->interval_position=='1'):
																		$Head_total=$Head_total+$HeadAmount;?>
																		<td class="hidden-480" style='text-align:center;'><?=number_format($HeadAmount,2)?></td>
														<? 			else:
																		if($s_vvalue['type']=='Regular'):
																		  $Head_total=$Head_total+$HeadAmount;?>
																			<td class="hidden-480" style='text-align:center;'><?=number_format($HeadAmount,2)?></td>
														<?				endif;
																	endif;
																endforeach;?>														
															<td class="hidden-480" style='text-align:right;'><?=number_format($Head_total,2);?></td>
														<?	endif;?>
													</tr>
												<? 	$sum=$sum+$Head_total; $sr++;
													endforeach;?>	
													<tr><td></td><td></td><td></td>
														<? if(!empty($Sesspay_heads)):
																foreach($Sesspay_heads as $s_key=>$s_value):
																	if($interval_detail->interval_position=='1'):?>
																		<td></td>
														<? 			else:
																		if($s_value['type']=='Regular'):?>
																			<td></td>	
														<?				endif;
																	endif;
																endforeach;
														endif;		?>													
													<td id='sum_amt'> TOTAL</td>
													<td id='sum_amt' style='text-align:right;'><?=number_format($sum,2)?></td>
													</tr>													
												</tbody>
									   <?php endif;?>  
									</table>				
								</div>
							</div>	

					<? elseif($f_type=="month_wise"):
							?>	
							<div class="portlet-body">
								<table width='100%'>
									<tr><td colspan='2' class='main_title1'  style="text-align:center;margin-top:15px;font-size:22px;padding:5px;"><br/>CHECK LIST OF CLASS WISE COLLECTION DETAILS</td></tr>
									<tr><td class='title'><strong>SESSION:- <?=$session->title?>, SECTION:- <?=$ct_sec?></strong></td><td class='title' style='text-align:right;'><strong></strong></td></tr>
								</table>
							
							<div style='clear:both;'></div>
							
								<div class="clearfix">
								
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>SR. NO.</th>
											<th>REG ID</th>
											<th>STUDENT NAME</th>
											<th class="hidden-480" >CONCESSION AMOUNT</th>
											<th class="hidden-480" >PREVIOUS AMOUNT</th>
											<th class="hidden-480" >RECEIVABLE AMOUNT</th>
											<th class="hidden-480" >RECEIVED AMOUNT</th>	
											<th class="hidden-480" >BALANCE AMOUNT</th>											
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';  $balance='';$sr='1'; $ttl_con='';$ttl_total='';$ttl_paid='';$ttl_pen='';$ttl_bal='';?>
												<tbody>														
												<? foreach($record as $key=>$stu):
													#Get interval detail
													$SessIntOb= new studentSessionInterval();
													$student_interval_detail=$SessIntOb->getDetail($session_id,$ct_sec,$stu->id,$interval_id);	
													
													#get Pending Fee for previous month
													$SessObPre= new studentSessionInterval();
													$pending_fee=$SessObPre->getPreviousBalanceFee($session_id,$ct_sec,$stu->id,$interval_id);?>
													
													
													<tr class="odd gradeX">
														<td ><?=$sr?>.</td>
														<td ><?=$stu->reg_id?></td>
														<td><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
														<td class="hidden-480"><? if(is_object($student_interval_detail)): $con=$student_interval_detail->concession; echo number_format($con,2); else: $con='0'; endif;?></td>
														<td class="hidden-480"><? if(isset($pending_fee) && !empty($pending_fee)): echo number_format($pending_fee,2); else: echo number_format('0',2); endif;?></td>
														<td class="hidden-480"><? if(is_object($student_interval_detail)): $total=$student_interval_detail->total; echo number_format($total,2); else: $total='0'; endif;?></td>
														<td class="hidden-480"><? if(is_object($student_interval_detail)): $paid=$student_interval_detail->paid; echo number_format($paid,2); else: $paid='0'; endif;?></td>
														<td class="hidden-480"><? if(is_object($student_interval_detail)): $bal=$pending_fee+($student_interval_detail->total-$student_interval_detail->paid); echo number_format($bal,2); else: $bal='0'; endif;?></td>
													</tr>
												<? 	$ttl_con=$ttl_con+$con;
													$ttl_pen=$ttl_pen+$pending_fee;
													$ttl_total=$ttl_total+$total;
													$ttl_paid=$ttl_paid+$paid;
													$ttl_bal=$ttl_bal+$bal;
													$sr++;
													endforeach;?>	
													<tr><td></td><td></td>
																								
													<td id='sum_amt'> TOTAL</td>
													<td id='sum_amt'><?=number_format($ttl_con,2)?></td>
													<td id='sum_amt'><?=number_format($ttl_pen,2)?></td>
													<td id='sum_amt'><?=number_format($ttl_total,2)?></td>
													<td id='sum_amt'><?=number_format($ttl_paid,2)?></td>
													<td id='sum_amt'><?=number_format($ttl_bal,2)?></td>
													</tr>													
												</tbody>
									   <?php endif;?>  
									</table>				
								</div>
							</div>	
							
					<?php endif;?> 	
					
	
    </div>
<? endif;?>	