<?php
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');


isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['page']) ? $page = $_GET['page'] : $page = '1';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = 'A';
isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = 'A';
isset($_REQUEST['interval_id']) ? $interval_id = $_REQUEST['interval_id'] : $interval_id = '';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
isset($_REQUEST['report_type']) ? $report_type = $_REQUEST['report_type'] : $report_type = 'admission_detail';
isset($_REQUEST['date']) ? $date = $_REQUEST['date'] : $date = '';
isset($_REQUEST['date1']) ? $date1 = $_REQUEST['date1'] : $date1 = '0';
isset($_REQUEST['month']) ? $month = $_REQUEST['month'] : $month = '';
isset($_REQUEST['exam']) ? $exam = $_REQUEST['exam'] : $exam = '0';
isset($_REQUEST['v_report_type']) ? $v_report_type = $_REQUEST['v_report_type'] : $v_report_type = 'vehicle_info';
isset($_REQUEST['s_report_type']) ? $s_report_type = $_REQUEST['s_report_type'] : $s_report_type = 'recieve';
isset($_REQUEST['f_report_type']) ? $f_report_type = $_REQUEST['f_report_type'] : $f_report_type = 'recieve';
isset($_REQUEST['e_report_type']) ? $e_report_type = $_REQUEST['e_report_type'] : $e_report_type = 'live';
isset($_REQUEST['ex_type']) ? $ex_type = $_REQUEST['ex_type'] : $ex_type = '';
isset($_REQUEST['a_type']) ? $a_type = $_REQUEST['a_type'] : $a_type = '';
isset($_REQUEST['f_type']) ? $f_type = $_REQUEST['f_type'] : $f_type = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['subject']) ? $subject = $_REQUEST['subject'] : $subject = '';

isset($_REQUEST['tmp']) ? $tmp = $_REQUEST['tmp'] : $tmp = 'student';
isset($_REQUEST['from']) ? $from = $_REQUEST['from'] : $from = '';
isset($_REQUEST['to']) ? $to = $_REQUEST['to'] : $to = '';
isset($_REQUEST['cat']) ? $cat = $_REQUEST['cat'] : $cat = '';
isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = '';

if (isset($_POST)) {
    extract($_POST);
    ?>
    <style>
        .print_tables table {
            width: 100%;
            border: 1px solid #000;
        }
        .print_tables td {
            border-top: 1px solid #000;
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables th {
            border-right: 1px solid #000;
            text-align: center;
        }
        .print_tables table tr td:last-child {
            border-right: none;
        }
        .print_tables table tr th:last-child {
            border-right: none;
        }
    </style>
    <div class="print_tables">
        <?php
        $QueryOb = new studentFeeRecord();
        $record = $QueryOb->SingleDayReport($school->id, $to);
        ?>
        <center><h1>Daily Collection Report</h1></center>
        <table class="table table-striped table-bordered table-hover" id="sample_2">
            <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Reg. No.</th>
                    <th>Student Name</th>
                    <th>Father Name</th>
                    <th class="hidden-480 sorting_disabled">Session</th>
                    <th class="hidden-480 sorting_disabled">Receipt No.</th>
                    <th style='text-align:center;width:15%;' class="hidden-480 sorting_disabled">Date</th>
                    <th class="hidden-480 sorting_disabled" style='width:15%;text-align:right;'>Amount (Rs)</th>
                </tr>
            </thead>
            <? if (!empty($record)): $sum = ''; ?>
                <tbody>
                    <?php
                    $sr = 1;
                    foreach ($record as $key => $object):
                        ?>

                        <tr class="odd gradeX">
                            <td><?= $sr ?>.</td>
                            <td><?= $object->reg_id ?></td>
                            <td><?= $object->first_name . " " . $object->last_name ?></td>
                            <td><?= $object->father_name ?></td>
                            <td><?= $object->title ?></td>
                            <td class="hidden-480 sorting_disabled"><?= $object->id ?></td>
                            <td style='text-align:center' class="hidden-480 sorting_disabled"><?= $object->payment_date ?></td>
                            <td style='text-align:right;'>Rs.<?= number_format($object->total_amount, 2) ?></td>													
                        </tr>
                        <?php
                        $sr++;
                        $sum = $sum + $object->total_amount;
                    endforeach;
                    ?>
                </tbody>
            <?php endif; ?>  
        </table>
        <? if (!empty($record)): ?>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th style="width:10%px;border:none"></th>
                        <th style='width:40%;border:none'></th>
                        <th style='width:20%;border:none'></th>
                        <th style='width:20%;border:none'></th>
                        <th class="hidden-480 sorting_disabled" style='border:none;width:20%;text-align:right;'>Total = Rs.<?php echo number_format($sum, 2) ?></th>
                    </tr>
                </thead>	
            </table>
        <?php endif; ?> 
    </div>
<?php } ?>