<?php
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEventClass.php');

if (isset($_POST)) {
    extract($_POST);
    if ($act == 'get_sections') {
        if ($session_id == '0') {
            ?>
            <div class="control-group">
                <label class="control-label">Select Section</label>
                <div class="controls">
                    <input type='text' name="section" class="span6" value='All' disabled />
                </div>
            </div>
            <?php
        } else {
            /* Get session Pages */
            $QuerySec = new studentSession();
            $QuerySec->listAllSessionSection($session_id);
            ?>
            <div class="control-group">
                <label class="control-label">Select Section</label>
                <div class="controls">
                    <? if ($QuerySec->GetNumRows() >= 0) { ?>	
                        <select class="span6" data-placeholder="Select Sections" name='section' id='section'>
                            <?php
                            $session_sc = 1;
                            while ($sec = $QuerySec->GetObjectFromRecord()) {
                                ?>
                                <option value='<?= $sec->section ?>'><?= ucfirst($sec->section) ?></option>
                                <?
                                $session_sc++;
                            }
                            ?>
                        </select>
                    <? } else { ?>
                        <input type='text' class="span6" value='No Section.' disabled />
                        <?
                    }
                    ?>
                </div>
            </div>
            <?php
        }
    }
    if ($act == 'get_sections_edit') {
        $obj = new event;
        $event = $obj->getRecord($event_id);
        if ($session_id == '0') {
            ?>
            <div class="control-group">
                <label class="control-label">Select Section</label>
                <div class="controls">
                    <input type='text' name="section" class="span6" value='All' disabled />
                </div>
            </div>
            <?php
        } else {
            /* Get session Pages */
            $QuerySec = new studentSession();
            $QuerySec->listAllSessionSection($session_id);
            ?>
            <div class="control-group">
                <label class="control-label">Select Section</label>
                <div class="controls">
                    <? if ($QuerySec->GetNumRows() >= 0) { ?>	
                        <select class="span6" data-placeholder="Select Sections" name='section' id='section'>
                            <?php
                            $session_sc = 1;
                            while ($sec = $QuerySec->GetObjectFromRecord()) {
                                ?>
                                <option value='<?= $sec->section ?>' <?= $event->section == $sec->section ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                <?
                                $session_sc++;
                            }
                            ?>
                        </select>
                    <? } else { ?>
                        <input type='text' class="span6" value='No Section.' disabled />
                        <?
                    }
                    ?>
                </div>
            </div>
            <?php
        }
    }
}
?>