
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Events
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>Events</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>		
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-file-text"></i>Manage Events</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('event', 'list', 'list'); ?>" method="post" id="form_data" name="form_data" >	
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                            <th>Sr. No</th>
                            <th>Session</th>
                            <th>Section</th>
                            <th>Category</th>
                            <th>Event</th>
                            <th>Event Date</th>
                            <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sr = 1;
                                foreach ($events as $event) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="hidden-480"><?php echo $sr++; ?></td>
                                        <td class="hidden-480">
                                            <?php
                                            if ($event->session_id == '0') {
                                                echo "All Classes";
                                            } else {
                                                $session = get_object('session', $event->session_id);
                                                $course = get_object('school_course', $session->course_id);
                                                echo $course->course_name;
                                            }
                                            ?>
                                            <?= $event->session_year ?>
                                        </td>
                                        <td><?= $event->section ?></td>
                                        <td>
                                            <?php $cat = get_object('school_events_cat', $event->event_cat); ?>
                                            <?= $cat->title ?>
                                        </td>
                                        <td><?= $event->event ?></td>
                                        <td><?= $event->on_date ?></td>
                                        <td>
                                            <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('event', 'update', 'update', 'id=' . $event->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                            <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('event', 'delete', 'delete', '&id=' . $event->id . '&delete=1') ?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>                          
                        </table>
                    </form>    
                </div>
            </div>                                                
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    



