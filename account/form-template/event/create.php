
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Events
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-calendar"></i>
                    <a href="<?php echo make_admin_url('event', 'list', 'list'); ?>">List Events</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add Event
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>	            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('event', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-calendar"></i>Add Event</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <input type="hidden" name="session_year" value="<?php echo $sess_int; ?>"/>
                        <div class="control-group">
                            <label class="control-label">Select Session</label>
                            <div class="controls">
                                <select class="span6 select2 m-wrap validate[required]" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                    <option value="0">All</option>
                                    <?php
                                    while ($object = $QueryObj->GetObjectFromRecord()):
                                        $course = get_object('school_course', $object->course_id);
                                        ?>
                                        <option value="<?php echo $object->id; ?>"><?php echo $course->course_name; ?></option>
                                    <?php endwhile; ?>
                                </select>
                            </div>
                        </div>
                        <div id="list_all_sections"></div>
                        <div class="control-group">
                            <label class="control-label" for="event_cat">Event Category</label> 
                            <div class="controls">
                                <select class="span6 m-wrap validate[required]" name="event_cat">
                                    <?php foreach ($cats as $cat) { ?>
                                        <option value="<?= $cat->id ?>"><?= $cat->title ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="event">Event Title</label> 
                            <div class="controls">
                                <input type="text" name="event" id="event" class="span6 validate[required]"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="description">Description<span class="required"></span></label>
                            <div class="controls">
                                <textarea type="text" name="description" id="description" style='width:47%;'></textarea>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="on_date">Event Date</label> 
                            <div class="controls">
                                <input type="text" name="on_date" id="on_date" class="span6 new_format add-on validate[required]"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="is_active">Make Active</label> 
                            <div class="controls">
                                <input type="checkbox" name="is_active" id="is_active" value="1" />
                            </div>
                        </div> 
                        <div class="form-actions">
                            <input type='hidden' name="school_id" value='<?= $school->id ?>'/>	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('time_table', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->
<script>
    $(document).ready(function () {
        $('#session_id').change();
    });
    $(document).on('change', '#session_id', function () {
        var session_id = $(this).val();
        $('#list_all_sections').html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=event'); ?>", {act: 'get_sections', session_id: session_id}, function (data) {
            $('#list_all_sections').html(data);
        });
    });
</script>