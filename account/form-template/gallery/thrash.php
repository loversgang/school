  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Gallery
                                      <a class="btn large grey" href="<?php echo make_admin_url('gallery', 'list', 'insert');?>"><i class="icon-plus"></i> Add New Image</a>
                            </h3>
                            <!--
                             <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                     <li>
                                        <i class="icon-picture"></i>
                                               <a href="<?php echo make_admin_url('gallery', 'list', 'list');?>">List Gallery Images</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                     <li class="last">
                                        Trash
                                    </li>

                            </ul>-->
                           

                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php #  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-picture"></i>Trash</div>
								
							</div>
							<div class="portlet-body">
							        <table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
                                                                                 <tr>
                                                                          
                                                                                        <th class="hidden-480">Sr. No</th>
                                                                                        <th>Caption</th>
                                                                                        <th>Image</th>
                                                                                        <th class="hidden-480">Position </th>
                                                                                        <th>Show on Website</th>
                                                                                        <th >Action</th>
                                                                                        

                                                                                </tr>
									</thead>
									 <tbody>
                                                                            
                                                                              <? if($QueryObj->GetNumRows()!=0):?>
                                                                             <?php $sr=1;while($gallery=$QueryObj->GetObjectFromRecord()):?>
                                                                                    <tr class="odd gradeX">
                                                                                     
                                                                                        
                                                                                        <td class="hidden-480"><?php echo $sr++;?></td>
											<td><?php echo $gallery->caption?></td>
                                                                                        <td>
                                                                                                <?php
                                                                                                  $image_obj=new imageManipulation();
                                                                                                  if($gallery->image && (file_exists(DIR_FS_SITE_UPLOAD.'photo/gallery/large/'.$gallery->image))):?>
                                                                                                 
                                                                                                     <a href="<?=$image_obj->get_image('gallery','big', $gallery->image);?>" class="tooltips fancybox-button" title="View Image">
                                                                                                     
                                                                                                         <img style="width:50px" src="<?=$image_obj->get_image('gallery','thumb', $gallery->image);?>"/> 
                                                                                               
                                                                                                     </a>
                                                                                            
                                                                                            <?php else:?>   
                                                                                                  <img style="width:50px" src="assets/img/noimage.jpg"/>
                                                                                            <?php endif;?>
                                                                                        </td>
											<td class="hidden-480">
                                                                                           <?php echo $gallery->position?> 
                                                                                        </td>
											<td>
                                                                                            <span class="label label-<?php echo ($gallery->is_active=='1')?'success':'important';?>"><?php echo ($gallery->is_active=='1')?'Show':'Hide';?></span>
                                                                                        </td>
											<td>
                                                                                            
                                                                                               <a class="btn green icn-only tooltips" href="<?php echo make_admin_url('gallery', 'restore', 'restore', 'id='.$gallery->id)?>" class="tipTop smallbtn" title="click here to restore this record"><i class="icon-undo icon-white"></i></a>&nbsp;&nbsp;
                                                                                               <a class="btn red icn-only tooltips" href="<?php echo make_admin_url('gallery', 'permanent_delete', 'permanent_delete', 'id='.$gallery->id.'&delete=1')?>" onclick="return confirm('Are you deleting this record permanently?.');" class="tipTop smallbtn" title="click here to delete this record permanently" ><i class="icon-remove icon-white"></i></a>  
                                                                                         </td>
											

                                                                                    </tr>
                                                                             <?php endwhile;?>
                                                                           

									</tbody>
                                                                      
                                                                       <?php endif;?>  
								</table>
                                                            
                                                            
                              
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>