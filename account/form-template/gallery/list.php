 <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Gallery Images
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Gallery Images
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-picture"></i>Manage Gallery Images</div>
								<div class="tools">
								</div>
							</div>
							<div class="portlet-body">
                                                            
                                                            
                                                                
                                                                    
                                                                     <? if($QueryObj->GetNumRows()!=0):?>
                                                                             <?php $sr=1;$new_slide=0;while($gallery=$QueryObj->GetObjectFromRecord()):?>
                                                                                   
                                                                                    <?php if($sr==1 || ($sr-1)%4==0):?>
                                                            
                                                                                       <div class="row-fluid">
                                                                                        
                                                                                    <?php endif;?>        
                                                                                                <div class="span3">
                                                                                                        <div class="item">
                                                                                                            <?php
                                                                                                              $image_obj=new imageManipulation();
                                                                                                              if($gallery->image && (file_exists(DIR_FS_SITE_UPLOAD.'photo/gallery/large/'.$gallery->image))):?>
                                                                                                                    <a class="tooltips fancybox-button" data-rel="fancybox-button" title="<?php echo $gallery->caption?>" href="<?=$image_obj->get_image('gallery','big', $gallery->image);?>">
                                                                                                                            <div class="zoom">
                                                                                                                                    <img class="<?php echo $gallery->is_active==0?'hidden_image':''?>" src="<?=$image_obj->get_image('gallery','large', $gallery->image);?>" alt="<?php echo $gallery->caption?>"/> 

                                                                                                                                    <div class="zoom-icon"></div>
                                                                                                                            </div>
                                                                                                                    </a>
                                                                                                       <?php else:?>
                                                                                                             <img  src="assets/img/noimage.jpg"/>
                                                                                                       <?php endif;?>

                                                                                                                <div class="details">
                                                                                                                       
                                                                                                                        
                                                                                                                        
                                                                                                                        <a href="<?php echo make_admin_url('gallery', 'update', 'update', 'id='.$gallery->id)?>" title="click here to edit this image" class="icon">Edit</i></a>|
                                                                                                                        <a href="<?php echo make_admin_url('gallery', 'delete', 'list', 'id='.$gallery->id.'&delete=1')?>" title="click here to delete this image" onclick="return confirm('Are you sure? You are deleting this record.');" class="icon">Delete</a>    
                                                                                                                </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                                
                                                                                                
                                                                                        <?php if($sr==(($QueryObj->GetNumRows())) || $sr++%4==0 ):?>
                                                            
                                                                                        <div class="clearfix"></div>

                                                                                        </div>
                                                                                       <?php endif;?>  
                                                                                        

                                                                            <?php endwhile;?>
                                                                           
                                                                                <div class="row-fluid">
                                                                                     <div class="span3">
                                                                                            <div class="item">
                                                                                                <a class="btn large grey" style='padding:7px 0px;'  href="<?php echo make_admin_url('gallery', 'list', 'insert');?>">
                                                                                                   <img  src="assets/img/upload_new_image.png"/>
                                                                                               </a>    
                                                                                            </div>
                                                                                     </div>   
                                                                                </div>  
                                                                         
                                                                           
                                                                           <?php
                                                                           if($total_pages>1):
                                                                               
                                                                               echo pagingAdmin($p,$total_pages,$total_records,'gallery');
                                                                               
                                                                           endif;
                                                                           ?>
                                                                    <?php else:?>
                                                                            <div style="text-align:center">
                                                                                                <a class="btn large grey" href="<?php echo make_admin_url('gallery', 'list', 'insert');?>">
                                                                                                   <img style="text-align:center" src="assets/img/upload_new_image.png"/>
                                                                                               </a>  
                                                                               <br/>
                                                                                <div style="clear:both;height:10px">  </div>
                                                                               <p>
                                                                                   No gallery image found, 
                                                                                   <a style="text-decoration:underline" class="black" href="<?php echo make_admin_url('gallery', 'list', 'insert');?>"> click to add new gallery image.</a>
                                                                               </p>
                                                                               <br/>
                                                                           </div>
                                                                    <?php endif;?>
                                                                    
                                                                   <div class="clearfix"></div>
                                                      
                                                                  <div class="clearfix"></div>

                                                                     
                                                            
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



