 <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Gallery Images
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
                                    <li>
                                            <i class="icon-list"></i>
                                            <a href="<?php echo make_admin_url('gallery', 'list', 'list');?>">List Gallery Images</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>   
                                    <li class="last">
                                        Add Gallery Images
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('gallery', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                         <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-picture"></i>New Gallery Image</div>
                                            <div class="tools">
                                            </div>
                                            
                                    </div>
                                    <div class="portlet-body form">

                                             <div class="control-group">
                                                    <label class="control-label" for="caption">Caption<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="caption" value="" id="caption" class="span6 m-wrap validate[required]" />
                                                    </div>  
                                            </div>
                                          
                                            <div class="control-group">
                                                    <label for="image" class="control-label">Image</label>
                                                    <div class="controls">
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                                    </div>
                                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                    <div>
                                                                            <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                                            <span class="fileupload-exists">Change</span>
                                                                            <input type="file" id="image" name="image" class="default" /></span>
                                                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                                    </div>
                                                            </div>
                                                           
                                                    </div>
                                            </div>
               
                                           <input type="hidden" name="is_active" id="is_active" value="1"  />
                                            <div class="control-group">
                                                    <label class="control-label" for="position">Position<span class="required">*</span></label>
                                                    <div class="controls">
                                                         <input style="width:10%" type="text" id="position"  name="position" value="" class="span6 m-wrap validate[required]" />
                                                    </div>  
                                            </div>

                                            <div class="form-actions">
													 <input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" /> 
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('gallery', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
                                         
                                
                              </div> 
                            </div>
                        </div>
                          
                            
                           

                     </form>
                 
                   
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



