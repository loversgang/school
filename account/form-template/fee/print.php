	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>


<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');

isset($_REQUEST['id'])?$id=$_REQUEST['id']:$id='';
isset($_REQUEST['doc_id'])?$doc_id=$_REQUEST['doc_id']:$doc_id='';


		#Get Document
		$QueryDoc = new document();
		$Doc=$QueryDoc->getRecord($doc_id);	
	
		#Get Fee Entry
		$QueryFee = new studentFee();
		$Fee=$QueryFee->getRecord($id);	
		
		#Get Student
		$QueryStu = new studentSession();
		$student=$QueryStu->SingleStudentsWithSession($Fee->student_id,$Fee->session_id,$Fee->section) ; 
		
		#Get Fee Heads paid
		$QueryFeeHeads = new studentFeeRecord();
		$FeeRecord=$QueryFeeHeads->listAll($id);
		$rec_head=''; $sub_total='';
		
		$object=get_object('session',$Fee->session_id);
		if(strtotime($object->end_date)<strtotime(date('Y-m-d'))):
			$l_ast=$object->end_date;
		else:
			$l_ast=date('Y-m-d');
		endif;
		
		#Get Fee Interval
		$QueryFeeInt = new session();
		$FeeInterval=$QueryFeeInt->GetSessionFeeInterval($Fee->session_id,$Fee->section,$Fee->student_id,$object->start_date,$l_ast,$object->frequency);	
		
		#Get Fee From And To
		if(empty($from) && empty($to)):
			foreach($FeeInterval[0] as $key=>$val):
				$from=$key;
				$to=$val;
			endforeach;
		endif;	
		
		#Get Student Previous Fee List
		$QueryFirst = new studentFee();
		$Total_paid=$QueryFirst->check_fee_in_intervalTotal($Fee->session_id,$Fee->section,$Fee->student_id,$from,date('Y-m-d'),$Fee->id);		
		
		if(!empty($FeeRecord)):
			foreach($FeeRecord as $key=>$val):
				$rec_head= $rec_head."<tr>
					<th style='text-align:left;width:10%;'></th>
					<th style='text-align:right;width:50%;font-weight: normal;'>".$val->fee_type."</th>
					<th style='text-align:right;width:20%;font-weight: normal;'>".CURRENCY_SYMBOL.' '.number_format($val->amount,2)."</th>
				</tr>";					
				$sub_total=$sub_total+$val->amount;	
			endforeach;
		else:
				$rec_head= $rec_head."<tr>
					<th style='text-align:left;width:10%;'></th>
					<th style='text-align:right;width:50%;font-weight: normal;'>Fee</th>
					<th style='text-align:right;width:20%;font-weight: normal;'>".CURRENCY_SYMBOL.' '.number_format($Fee->total_amount,2)."</th>
				</tr>";		
				$sub_total->$Fee->total_amount;
		endif;	
		
		if(!empty($Total_paid)):
				$rec_head= $rec_head."<tr>
					<th style='text-align:left;width:10%;'></th>
					<th style='text-align:right;width:50%;font-weight: normal;'>Previous Payment</th>
					<th style='text-align:right;width:20%;font-weight: normal;'><strong>- </strong>".CURRENCY_SYMBOL.' '.number_format($Total_paid,2)."</th>
				</tr>";	
				$sub_total=$sub_total-$Total_paid;
		endif;		


?>

<? if(!empty($Doc)):

		$replace=array('SCHOOL_NAME'=>$school->school_name,
					  'DATE'=>date('d M, Y',strtotime($Fee->payment_date)),
					  'NAME'=>$student->first_name." ".$student->last_name,
					  'CLASS'=>$student->session_name,
					  'SECTION'=>$student->section,
					  'ROLL_NO'=>$student->roll_no,
					  'REG_ID'=>$student->reg_id,
					  'FEE_ADDED'=>"<table style='width: 100%; text-align: center; line-height: 22px;'>
										<tbody>".$rec_head."</tbody>
									</table>",
									'SUB_TOTAL'=>CURRENCY_SYMBOL.' '.number_format($sub_total,2),
									'TOTAL'=>CURRENCY_SYMBOL.' '.number_format($Fee->total_amount,2),
									'BALANCE'=>CURRENCY_SYMBOL.' '.number_format(($sub_total-$Fee->total_amount),2)
						);

						$content=$Doc->format;
							if(count($replace)):
								foreach($replace as $k=>$v):
									$literal='{'.trim(strtoupper($k)).'}';
									$content=html_entity_decode(str_replace($literal, $v, $content));
									$content=str_replace('&nbsp;','',$content);	
								endforeach;
							endif;	

	echo html_entity_decode($content);
endif;?>	