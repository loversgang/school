<?

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');

if ($_POST):
    isset($_POST['reg_id']) ? $reg_id = $_POST['reg_id'] : $reg_id = '';
    isset($_POST['ct_school']) ? $ct_school = $_POST['ct_school'] : $ct_school = '';


    #Get Section Students
    $QuerySec = new studentSession();
    $records = $QuerySec->findCurrentYearStudentsByRegId($ct_school, $reg_id);
    if (empty($records)) {
        echo "error";
    } else {
        echo json_encode($records);
    }
endif;
?>