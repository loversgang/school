<?
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');

if ($_POST):
    isset($_POST['session_id']) ? $session_id = $_POST['session_id'] : $session_id = '';

    /* Get session Pages */
    $QuerySec = new studentSession();
    $QuerySec->listAllSessionSection($session_id);

    #get Session Info
    $QueryS = new session();
    $session = $QueryS->getRecord($session_id);

    #get Session Interval
    $QueryInt = new session_interval();
    $interval = $QueryInt->listOfInterval($session_id);

    $current_date = date('Y-m-d');
    $current = '0';
    ?>

    <? if ($QuerySec->GetNumRows() > 0): ?>
        <div class='span3'>
            <div class="control-group" >
                <label class="control-label">Select Section </label>
                <div class="controls">								
                    <select class="select2_category span12" data-placeholder="Select Session Students" name='ct_sect' id='ct_sect'>
                        <?php $session_sc = 1;
                        while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                            <option value='<?= $sec->section ?>'><?= ucfirst($sec->section) ?></option>
            <? $session_sc++;
        endwhile; ?>
                    </select>
                </div>
            </div>
        </div>	

        <?php if (!empty($interval)): ?>
            <div class='span6'>
                <div class="control-group" >
                    <label class="control-label">Select Payment Interval </label>
                    <div class="controls">	
                        <select class="span12" name='interval_id' id='interval_id'>
                                <? foreach ($interval as $i_k => $i_v):
                                    if ((strtotime($current_date) >= strtotime($i_v->from_date)) && (strtotime($current_date) <= strtotime($i_v->to_date))): $current = '1';
                                    endif;
                                    ?>
                                <option value='<?= $i_v->id ?>' <? if ($current == '1'): echo 'selected';
                    endif; ?> >
                                    <? if (getTotalDays($i_v->from_date, $i_v->to_date) <= '31'): ?>
                                    <?= date('F', strtotime($i_v->from_date)) ?>
                                <? else: ?>
                    <?= date('F', strtotime($i_v->from_date)) . "  to  " . date('F,Y', strtotime($i_v->to_date)) ?>
                <? endif; ?>
                <? if ($current == '1'): break;
                endif; ?>
                                </option>
            <? endforeach; ?>
                        </select>
                    </div>	
                </div>
            </div>	

            <div class="span2" style="margin-left:5px;">
                <div class="control-group" >
                    <label class="control-label">&nbsp;</label>
                    <div class="controls">								
                        <button name='submit' class='btn blue' value='Submit'>&nbsp;&nbsp;Go&nbsp;&nbsp;</button>
                    </div>
                </div>
            </div>

        <? else: ?>
            <div class='span9'>
                <div class="control-group" >
                    <label class="control-label">Payment Interval </label>
                    <div class="controls">								
                        <input type='text' class='span12' value='No Interval Found. Please update the session to get interval.' disabled style='font-size:13px;'/>
                    </div>	
                </div>							
            </div>
        <? endif; ?>
    <? else: ?>
        <div class='span9'>
            <div class="control-group" >
                <label class="control-label">&nbsp;</label>
                <div class="controls">							
                    <input type='text' value='Sorry, No Section Found.' disabled />
                </div>	
            </div>	
        </div>	
    <? endif; ?>

    <div id='show_students'></div>
    <?
endif;
?>