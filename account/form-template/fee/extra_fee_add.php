<?
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsEmailClass.php');
if ($_POST):
    isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
    isset($_REQUEST['ct_sect']) ? $ct_sect = $_REQUEST['ct_sect'] : $ct_sect = '';
    isset($_REQUEST['extra_fee']) ? $extra_fee = $_REQUEST['extra_fee'] : $extra_fee = '';
    isset($_REQUEST['extra_fee_name']) ? $extra_fee_name = $_REQUEST['extra_fee_name'] : $extra_fee_name = '';
    isset($_REQUEST['extra_type']) ? $extra_type = $_REQUEST['extra_type'] : $extra_type = '';
    isset($_REQUEST['type']) ? $type = $_REQUEST['type'] : $type = 'select';
    isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
    isset($_REQUEST['interval_id']) ? $interval_id = $_REQUEST['interval_id'] : $interval_id = '';

    #Get interval detail
    $SessIntOb = new studentSessionInterval();
    $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sect, $student_id, $interval_id);


    if (isset($_POST['act']) && $_POST['act'] == 'cancel_new_extra_fee') :
        $ObjQuery = new query('student_session_interval');
        extract($_POST);
        #here is the extra field
        if ($extra_type == "+"):
            $total = ($student_interval_detail->total - $extra_val);
            $extra_type = '-';
        elseif ($extra_type == "-"):
            $total = ($student_interval_detail->total + $extra_val);
            $extra_type = '+';
        endif;
        $ObjQuery->Data['total'] = $total;
        $ObjQuery->Data['other_fee_type'] = '';
        $ObjQuery->Data['other_fee'] = '';
        $ObjQuery->Data['other_fee_name'] = '';
        $ObjQuery->Data['id'] = $student_interval_detail->id;
        $ObjQuery->Update();
    else:
        $ObjQuery = new query('student_session_interval');
        if (!empty($extra_fee) && is_numeric($extra_fee)):
            #here is the extra field
            if ($extra_type == "minus"):
                $total = ($student_interval_detail->total - $extra_fee);
                $extra_type = '-';
            elseif ($extra_type == "plus"):
                $total = ($student_interval_detail->total + $extra_fee);
                $extra_type = '+';
            endif;

            if ($student_interval_detail->other_fee_type == '+'):
                $total = $total - $student_interval_detail->other_fee;
            elseif ($student_interval_detail->other_fee_type == '-'):
                $total = $total + $student_interval_detail->other_fee;
            endif;

            $ObjQuery->Data['total'] = $total;
            $ObjQuery->Data['other_fee_type'] = $extra_type;
            $ObjQuery->Data['other_fee'] = $extra_fee;
            $ObjQuery->Data['other_fee_name'] = $extra_fee_name;
            $ObjQuery->Data['id'] = $student_interval_detail->id;
            $ObjQuery->Update();
        endif;
    endif;
    /* Again Get the info */
    #Get student pay heads students
    $SessStuFeeObj = new session_student_fee();
    $pay_heads = $SessStuFeeObj->GetStudentPayHeads($session_id, $student_id, 'array');

    #fill the previous interval fee if not update then insert
    $SessOb = new studentSessionInterval();
    $SessOb->CheckPreviousFeeRecord($session_id, $ct_sect, $student_id, $interval_id, $pay_heads);

    #Get interval detail
    $SessIntOb = new studentSessionInterval();
    $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sect, $student_id, $interval_id);

    #Get Session Interval In array
    $SessIntOb = new session_interval();
    $interval = $SessIntOb->listOfIntervalInArray($session_id);

    #get Student Info
    $QueryStu = new studentSession();
    $student_info = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sect);

    #get first date and last list of this interval		
    $from = $student_interval_detail->from_date;
    $to = $student_interval_detail->to_date;

    #Get Student Previous Fee List in this interval
    $QueryFirst = new studentFee();
    $Fee_list = $QueryFirst->listOfPreviousRecord($session_id, $ct_sect, $student_id, $from, $to);

    #Get Last Or Current Interval	
    $SessIntOb = new session_interval();
    $last_int_id = $SessIntOb->getCurrentLastIntervalId($session_id);

    if (empty($Fee_list)):
        # if empty update total paid
        $SessObFee = new studentSessionInterval();
        $SessObFee->updateTotalPaidFeeToZero($student_interval_detail->id);

        # Again Get interval detail
        $SessIntOb = new studentSessionInterval();
        $student_interval_detail = $SessIntOb->getDetail($session_id, $ct_sect, $student_id, $interval_id);
    endif;

    #Get Total paid in this interval
    $total_paid = $student_interval_detail->paid;

    #check the absent fee if not the first fee
    if ($student_interval_detail->interval_position == '1'):
        $Absent = '';
    else:
        foreach ($interval as $ik => $iv):
            if ($iv->id == $interval_id):
                break;
            endif;
            $atten_from = $iv->from_date;
            $atten_to = $iv->to_date;
        endforeach;

        #get Student Absent Now for previous date
        $QurAtten = new attendance();
        $Absent = $QurAtten->getStudentAbsent($session_id, $ct_sect, $student_id, $atten_from, $atten_to);
    endif;

    #get Pending Fee for previous month
    $SessObPre = new studentSessionInterval();
    $pending_fee = $SessObPre->getPreviousBalanceFee($session_id, $ct_sect, $student_id, $interval_id);
    ?>

    <div class="portlet-title green">
        <div class="caption">Calculate Student Fee</div>
        <div class="tools">
            <div class="actions">
                <a class="btn success mini" href="<?= make_admin_url('session', 'section', 'section&ct_session=' . $session_id . '&id=' . $student_info->rel_id . '&ct_section=' . $student_info->section) ?>" title='Click here to edit student fee'><i class="icon-pencil"></i> Edit</a>
            </div>
        </div>
    </div>
    <table class="table table-striped table-bordered table-advance table-hover" id='fee_table'>
        <thead>
            <tr><th style='width:50%;'>Fee Head Name</th><th style='width:50%;text-align:right;'>Amount (<?= CURRENCY_SYMBOL ?>)</th>
            </tr></thead>
        <tbody>
            <?
            $t_am = '0';
            $new_added = 0;
            if (!empty($pay_heads)): $st = '0';
                /* Put hete pending fee  */
                if (!empty($pending_fee)):
                    echo "<tr><td id='black_font_fee'>Previous Balance</td><td id='black_font_fee' style='text-align:right'>" . CURRENCY_SYMBOL . number_format($pending_fee, 2) . "</td></tr>";
                    echo "<input type='hidden' name='fee_record[Previous Balance]' value='" . $pending_fee . "'/>";
                    $t_am = $t_am + $pending_fee;
                endif;

                foreach ($pay_heads as $p_k => $p_v):
                    /* Show all Regular fee */
                    if ($p_v['type'] == 'Regular'):
                        echo "<tr><td>" . $p_v['title'] . "&nbsp;(" . ucfirst($p_v['type']) . ")</td><td style='text-align:right'>" . CURRENCY_SYMBOL . number_format($p_v['amount'], 2) . "</td></tr>";
                        echo "<input type='hidden' name='fee_record[" . $p_v['title'] . " (" . ucfirst($p_v['type']) . ")]' value='" . ($p_v['amount']) . "'/>";
                        $t_am = $t_am + $p_v['amount'];
                    else:
                        /* we Show all Other one time and optional fee if first time fee */
                        if ($student_interval_detail->interval_position == '1'):
                            echo "<tr><td>" . $p_v['title'] . "&nbsp;(" . ucfirst($p_v['type']) . ")</td><td style='text-align:right'>" . CURRENCY_SYMBOL . number_format($p_v['amount'], 2) . "</td></tr>";
                            echo "<input type='hidden' name='fee_record[" . $p_v['title'] . " (" . ucfirst($p_v['type']) . ")]' value='" . ($p_v['amount']) . "'/>";
                            $t_am = $t_am + $p_v['amount'];
                        endif;
                    endif;
                endforeach;

                #add Vehicle Fee if available 
                if (!empty($student_interval_detail->vehicle_fee)):
                    echo "<tr><td id='black_font_fee'>Vehicle Fee</td><td style='text-align:right' id='black_font_fee'>" . CURRENCY_SYMBOL . number_format($student_interval_detail->vehicle_fee, 2) . "</td></tr>";
                    echo "<input type='hidden' name='vehicle_fee' value='" . ($student_interval_detail->vehicle_fee) . "'/>";
                    $t_am = $t_am + $student_interval_detail->vehicle_fee;
                else:
                    echo "<input type='hidden' value='0' id='vehicle_fine_charge'/>";
                endif;

                #Session Total  
                echo "<tr><td class='total'>Sub Total</td><td class='total' colspan='2' style='text-align:right;'>" . CURRENCY_SYMBOL . " <span class='make_total_new'>" . number_format($t_am, 2) . "</span></td></tr>";
                $AmountTotal = $t_am;

                #add Attendance Fine if available 
                if (!empty($student_interval_detail->absent_fine)):
                    $AmountTotal = ($AmountTotal + ($student_interval_detail->absent_fine));
                    echo "<tr><td>Attendance Fine</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;+&nbsp;<input type='text' id='absent_fine_charge' class='span5 total_change' add='0' p_val='" . ($student_interval_detail->absent_fine) . "' name='absent_fine' value='" . $student_interval_detail->absent_fine . "' readonly/> <input type='hidden' value='" . $Absent . "' name='absent_days'/></td></tr>";
                else:
                    echo "<input type='hidden' value='0' id='absent_fine_charge'/>";
                endif;

                #Add Late Fee Fine if available 
                if (!empty($student_interval_detail->late_fee)):
                    $AmountTotal = ($AmountTotal + $student_interval_detail->late_fee);
                    echo "<tr><td id='black_font_fee'>Late Fee Fine</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;+&nbsp;<strong><input type='text' id='late_fee_charge' class='span5 total_change' name='late_fee' value='" . $student_interval_detail->late_fee . "' readonly/> </td></tr>";
                else:
                    echo "<input type='hidden' value='0' id='late_fee_charge'/>";
                endif;



                # Less concession 
                if (!empty($student_interval_detail->concession)):
                    echo "<tr><td id='black_font_fee'>Concession (" . $student_info->concession . ")</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;-&nbsp;<strong><input type='text' id='consession_fee' class='span5 total_change' name='concession' value='" . $student_info->concession_type . "' readonly/> </td></tr>";
                    echo "<input type='hidden' value='" . $student_interval_detail->concession . "' name='concession'/>";
                    $AmountTotal = $AmountTotal - $student_interval_detail->concession;
                else:
                    echo "<input type='hidden' value='0' id='consession_fee'/>";
                endif;

                # Less Total Paid 
                if (!empty($total_paid)):
                    $AmountTotal = $AmountTotal - $total_paid;
                endif;
                echo "<input type='hidden' value='" . $AmountTotal . "' id='session_total' name='session_total'/>";

                #here is the extra field
                if ($student_interval_detail->other_fee_type == "-"):
                    $AmountTotal = ($AmountTotal - $student_interval_detail->other_fee);
                elseif ($student_interval_detail->other_fee_type == "+"):
                    $AmountTotal = ($AmountTotal + $student_interval_detail->other_fee);
                endif;


                #here is the extra field
                echo "<tr id='extra_fee_div'><td id='black_font_fee'>
										<input type='text' class='span8 total_change extra_fee_name' name='other_fee_name' placeholder='Other fee Name' value='" . $student_interval_detail->other_fee_name . "'/>
										<a class='btn green mini' id='new_value_save'>Save</a>";
                if (!empty($student_interval_detail->other_fee && $student_interval_detail->other_fee_type && $student_interval_detail->other_fee_name)) {
                    echo '<a id="cancel_new_value" class="btn red mini" style="float: right; margin-top: 5px;margin-right: -32px;">Clear</a>';
                }
                echo "</td><td colspan='2' style='text-align:right'>
											<select name='other_fee_type' class='span3 total_change' id='extra_type'>";
                ?>
            <option value='-' <?= $student_interval_detail->other_fee_type == '-' ? "selected" : "" ?>><strong class='large_text'>-&nbsp;&nbsp;<strong></option>
                    <option value='+'<?= $student_interval_detail->other_fee_type == '+' ? "selected" : "" ?>><strong class='large_text'>+&nbsp;&nbsp;<strong></option>
                            <?
                            echo "</select>	
									<input type='text' id='extra' class='span5 total_change validate[custom[onlyNumberSp]]' name='other_fee' placeholder='Other fee' value='" . $student_interval_detail->other_fee . "'/> </td></tr>";

                            # Show Less Total Paid 
                            if (!empty($total_paid)):
                                echo "<tr><td class='total_paid'>Total Paid</td><td colspan='2' style='text-align:right'><strong class='total_paid'>&nbsp;-&nbsp;</strong>" . CURRENCY_SYMBOL . " <span class='total_paid'>" . number_format($total_paid, 2) . "</td></tr>";
                            endif;

                            # Total Amount 
                            echo "<tr><td class='total'>Total</td>
                                                                <td class='total' colspan='2' style='text-align:right'>" . CURRENCY_SYMBOL . " <span class='AmountTotal'>" .number_format($AmountTotal, 2) . "</span>
								</td></tr>";
                        else:
                            echo "<tr><td colspan='2'>Sorry, No Record Found..!</td></tr>";
                        endif;
                        ?>
                        </tbody>
                        </table>
                        <?
                    endif;
                    ?>