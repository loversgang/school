

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Students Fee
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Add Student Fee
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>

		
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
			<? if($type!='select'):?>
				<div class="tiles pull-right">
							<div class="tile bg-green <?php echo ($section=='update')?'selected':''?>">
								<a href="<?php echo make_admin_url('fee', 'insert', 'insert');?>">
									<div class="corner"></div>
									<div class="tile-body"><i class="icon-arrow-left"></i></div>
									<div class="tile-object"><div class="name"> Change Session</div></div>
								</a> 
							</div>	
			         
				
							<div class="tile bg-purple <?php echo ($section=='update')?'selected':''?>">
								<a href="<?php echo make_admin_url('fee', 'insert', 'insert&type=list&session_id='.$session_id.'&ct_sect='.$ct_sect.'&month='.$month.'&year='.$year);?>">
									<div class="corner"></div>
									<div class="tile-body"><i class="icon-list"></i></div>
									<div class="tile-object"><div class="name"> Section Student</div></div>
								</a> 
							</div>	
				</div>   		
			<? endif;?>
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
             
			<div class="row-fluid">
					<div class="span12">
						<div id="form_wizard_1" class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-reorder"></i> Student Fee Wizard - <span class="step-title">Step 1 of 3</span>
								</div>
							</div>
							<div class="portlet-body form">
								<div class="form-wizard">						
										<!-- Main steps -->
										<div class="navbar steps">
											<div class="navbar-inner">
												<ul class="row-fluid nav nav-pills">
													<li class="span4 <?=($type=='select')?'active':'';?>">
														<a class="step <?=($type=='select')?'active':'';?>">
														<span class="number">1</span>
														<span class="desc"><i class="icon-ok"></i> Select Student </span>   
														</a>
													</li>
													
													<li class="span4 <?=($type=='list')?'active':'';?>">
														<a class="step <?=($type=='list')?'active':'';?>">
														<span class="number">2</span>
														<span class="desc"><i class="icon-ok"></i> Section Students</span>   
														</a>
													</li>
													
													<li class="span4 <?=($type=='fee')?'active':'';?>">
														<a class="step <?=($type=='fee')?'active':'';?>">
														<span class="number">3</span>
														<span class="desc"><i class="icon-ok"></i> Add Student Fee</span>   
														</a>
													</li>													
												</ul>
											</div>
										</div>
										<!-- Main End -->
										<div class="progress progress-success progress-striped" id="bar">
											<div class="bar" style="width: <?=$per?>"></div>
										</div>
										
										
										<div class="tab-content">
										
											<!-- First steps -->
											<div id="tab1" class="tab-pane <?=($type=='select')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='list'/>											
											<div class="row-fluid">
											<div class="span5">		
												<h3 class="block">Select Session & Section</h3>
													<div class="alert alert-block alert-success">
														<div class="row-fluid">
																						
																<div class="control-group">
																	<label class="control-label">Select Session</label>
																	<div class="controls">
																		<select class="select2_category session_filter_for_single span9" data-placeholder="Select Session Students" name='session_id' id='session_id'>
																			<option value="">Select Session</option>
																			<?php $session_sr=1;while($session=$QuerySession->GetObjectFromRecord()): ?>
																					<option value='<?=$session->id?>' <? if($session->id==$session_id){ echo 'selected'; }?> ><?=ucfirst($session->title)?></option>
																			<? $session_sr++; endwhile;?>
																		</select>
																	</div>
																</div>
															
																<div id='dynamic_model'>			
																	<input type='hidden' class='section_filter' value='A' id='ct_s'/>
																</div>															
														</div>
													</div>	
													<div class="clearfix"></div>
												</form>	
												</div>	
												<div class="span2">	<h3 style='text-align: center; margin-top: 105px;'>OR</h3></div>
													
												<div class="span5">			
													<h3 class="block">Select Registration Id</h3>
													<div class="alert alert-block alert-success">
														<div class="row-fluid">
																						
																<div class="control-group">
																	<label class="control-label">Reg Id of Current Session's Student</label>
																	<div class="controls">
																		<input type="text" id='reg_id' style='background:#fff;' placeholder='Type Registration Id' class="span8 m-wrap" />
																		<div class='span2 btn blue' id='select_registration' style='float: right; margin-left: 0px; margin-right: 50px;'>Go</div>
																	</div>																	
																</div>
																							
														</div>														
														<div class="clearfix"></div>
														</div>											
												</div>
											</div>
											<div id='dynamic_model_reg'>	</div>												
													<div class="form-actions clearfix" >
															<a href="<?php echo make_admin_url('fee', 'list', 'list');?>" class="btn red" name="cancel" > Cancel</a>
													</div>
												</form>	
											</div>
											<!-- First steps End -->
											
											
											
											<!-- Second steps -->
											<div id="tab2" class="tab-pane <?=($type=='list')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='fee'/>
												<input type='hidden' name='session_id' value='<?=$session_id?>'/>
												<input type='hidden' name='ct_sect' value='<?=$ct_sect?>'/>												
												<h3 id="title_show" class="block alert alert-info"><?=$object->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?> </h3>
													
													<div class="span12" style='margin-left:0px;'>				
														<div class="controls">
																	<table id="sample" class="table table-striped table-bordered table-advance table-hover">
																		<thead>
																			<tr>
																				<th style='text-align:center;' class="hidden-480 sorting_disabled"></th>
																				<th class="hidden-480">Reg ID</th>
																				<th class="hidden-480" >Roll No.</th>
																				<th class="hidden-480" >Name</th>
																				<th class="hidden-480" >Father Name</th>
																			</tr>
																		</thead>
																		<tbody>
																		<? if(!empty($records)):?>																		
																		<?php $sr=1;foreach($records as $sk=>$stu): $checked='';?>
																			<tr class="odd gradeX">
																				<td style='text-align:center;background:#F9F9F9;'>
																				<input type="radio" name='student' checked value='<?=$stu->id?>' style='margin-left:0px;'/>
																				</td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->reg_id?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->roll_no?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->father_name)?></td>
																			</tr>
																		<? $sr++; endforeach;
																		   else:	?>
																		<tr class="odd gradeX"><td></td><td colspan='4'>Sorry, No Record Found..!</td></tr>
																	<? endif;?>
																		</tbody>
																	</table>
														</div>
													</div>
													<div class="clearfix"></div>
									
													<div class="form-actions clearfix" >
															<a href="<?php echo make_admin_url('fee', 'insert', 'insert');?>" class="btn red" name="cancel" > Cancel</a>
															<input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
													</div>
												</form>	
											</div>
											<!-- Second steps End -->
											
											
											
											<!-- Third steps -->
											<div id="tab3" class="tab-pane <?=($type=='fee')?'active':'';?>">
												<h3 id="title_show" class="block alert alert-info"><?=$object->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?> <font class="green_color"> &gt; </font> <?=ucfirst($student_info->first_name).' '.$student_info->last_name?>  </h3>
														<div class="row-fluid">
															<div class="span12">
																	<div class="portlet box blue">
																		<div class="portlet-title"> 
																			<div class="caption">Fee Information : </div>
																		</div>
																		<div class="portlet-body">
																		 <form class="form-horizontal validation" action="<?php echo make_admin_url('fee', 'insert', 'insert&type=fee&month='.$month)?>" method="POST" enctype="multipart/form-data" id="validation">
																				<br/>
		
																			
																			<div class="clearfix"></div>
																			<!-- More Entry Case -->
																			<? if(!empty($Fee_list)):?>	
																				<div class="row-fluid">																				
																					<div class="span6"> 
																						<div class="row-fluid">	
																						<div class="alert alert-block alert-success fade in">	
																								<div class="row-fluid">	
																									<div class='span4' style='line-height: 26px;'><strong>Student Type: </strong></div>
																									<div class='span6' style='line-height: 26px;'><?=$student_info->concession?></div>												
																								</div>	
																								<? if(!empty($student_info->concession_type)):?>
																									<div class="row-fluid">	
																										<div class='span4' style='line-height: 26px;'><strong>Concession: </strong></div>
																										<div class='span6' style='line-height: 26px;'><?=$student_info->concession_type?></div>												
																									</div>	
																								<? endif;?>	
																						</div>
																						</div>																					
																						<div class="alert alert-block alert-success fade in">																						
																								<div class="row-fluid">	
																									<div class='span5'>Session: </div>
																									<div class='span6'><?=$object->title?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span5'>Section: </div>
																									<div class='span6'><?=$ct_sect?></div>												
																								</div>																									
																								<div class="row-fluid">	
																									<div class='span5'>Reg Id: </div>
																									<div class='span6'><?=$student_info->reg_id?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span5'>Name: </div>
																									<div class='span6'><?=ucfirst($student_info->first_name).' '.$student_info->last_name?></div>												
																								</div>	
																								<div class="row-fluid">	
																									<div class='span5'>Roll No.: </div>
																									<div class='span6'><?=$student_info->roll_no?></div>												
																								</div>

																								<? if(is_object($Vehicle_Fee)):?>
																									<div class="row-fluid" >	
																										<div class='span5' id='color_red'>Vehicle Fee: </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($Vehicle_Fee->amount,2)?></div>												
																									</div>																									
																								<? endif;?>																									
																								
																								<? if($Absent>0):?>
																								<div class="row-fluid" >	
																									<div class='span5' id='color_red'>Attendance Fine: </div>
																									<div class='span6' id='color_red'>
																									<? if(!empty($AbsentDetail)):
																											echo "<table id='absent_table'><tr><th>Date</th><th style='text-align:right;'>Fine</th></tr>";
																											foreach($AbsentDetail as $a_key=>$a_v):
																												echo "<tr><td>".date('d, M Y',strtotime($a_v->date))."</td><td style='text-align:right;'>".CURRENCY_SYMBOL.number_format($object->absent_fine,2)."</td></tr>";
																											endforeach;
																											echo "<tr id='total_line'><td>Total</td><td style='text-align:right;'>".CURRENCY_SYMBOL.number_format(($object->absent_fine*$Absent),2)."</td></tr>";
																											echo "</table>";
																										endif;?><br/>							
																									</div>	
																								</div>	
																								<? endif;?>	

																								
																								<? if(!empty($LateFee)):?>
																									<div class="row-fluid" >	
																										<div class='span5' id='color_red'>Late Fee Fine: </div>
																										<div class='span6' id='color_red'>
																												<? 	echo "<table id='absent_table'><tr><th>Fee Dates</th><th style='text-align:right;'>Fine</th></tr>";
																														foreach($LateFee as $l_k=>$l_v):
																															foreach($l_v as $l_kk=>$l_vv):
																																echo "<tr><td>".date('d, M Y',strtotime($l_kk)).' - '.date('d, M Y',strtotime($l_vv))."</td><td style='text-align:right;'>".CURRENCY_SYMBOL.number_format($object->late_fee_rate,2)."</td></tr>";
																																$LateFeeFine=$LateFeeFine+$object->late_fee_rate;
																															endforeach;
																														endforeach;
																														echo "<tr id='total_line'><td>Total</td><td style='text-align:right;'>".CURRENCY_SYMBOL.number_format(($LateFeeFine),2)."</td></tr>";
																														echo "</table>";
																													?><br/>	
																										</div>												
																									</div>																									
																								<? endif;?>					
																								

																								<div class="row-fluid" >	
																										<div class='span5'><strong>Total Paid :</strong> </div>
																										<div class='span6'><strong><?=CURRENCY_SYMBOL.' '.number_format(($Total_paid),2)?></strong></div>												
																								</div>	
																						</div>
																						<!-- Student Info End-->
																					
																						

																						
																					</div>
																					
																					
																					<div class='span6'>
																						<div class="portlet-title green">
																							<div class="caption">Calculate Student Fee</div>
																							<div class="tools">
																									<div class="actions">
																										<a class="btn success mini" href="<?=make_admin_url('session','section','section&ct_session='.$session_id.'&id='.$student_info->rel_id.'&ct_section='.$student_info->section)?>" title='Click here to edit student fee'><i class="icon-pencil"></i> Edit</a>
																									</div>
																							</div>
																						</div>
																						<table class="table table-striped table-bordered table-advance table-hover">
																						<thead>
																						<tr><th style='width:50%;'>Fee Head Name</th style='width:30%;'><th></th><th style='width:20%;text-align:right;'>Amount (<?=CURRENCY_SYMBOL?>)</th>
																						</tr></thead>
																						<tbody>
																								<? $t_am='0'; $new_added=0;
																									if(!empty($pay_heads) && !empty($Sesspay_heads)): $st='0'; 
																										foreach($pay_heads as $p_k=>$p_v): 
																										 if(array_key_exists($p_v['fee_head'],$Sesspay_heads)):
																											$a=$p_v['fee_head'];
																											unset($Sesspay_heads[$a]);		
																										 endif;	?>
																											<tr class='delete_row<?=$p_v['fee_head']?>'><td><?=$p_v['title']?> &nbsp;(<?=ucfirst($p_v['type'])?>) </td><td style='line-height:29px;text-align:right;'>
																											<?php if($p_v['type']=='Regular'): echo $object->frequency." X ".$p_v['amount'].' ='; else: echo "1 X ".$p_v['amount'].' ='; endif;?></td><td style='text-align:right'>
																											<input class="span10" type="text" placeholder="0.00" value="<?php if($p_v['type']=='Regular'): echo $object->frequency*$p_v['amount']; else: echo 1*$p_v['amount']; endif;?>" readonly />
																											<input class="new_row change_value" type="hidden" placeholder="0.00" name='fee_heads[<?=$p_v['fee_id']?>]' value="<?=$p_v['amount']?>" add="<? if($p_v['type']=='Regular'): echo '1'; else: echo '0'; endif; ?>" readonly /></td></tr>
																								<? 		
																											if(ucfirst($p_v['type'])=='Regular'): 																
																												$ct_amount=$p_v['amount']*$object->frequency;																
																											else:
																												$ct_amount=$p_v['amount'];
																											endif;
																											$t_am=$t_am+$ct_amount;
																										endforeach;
																										# Now display if new heads of array
																												if(!empty($Sesspay_heads)): $new_added='1';
																													foreach($Sesspay_heads as $new_key=>$new_value):?>
																													<tr class='delete_row<?=$new_value['fee_head']?> removed'><td><?=$new_value['title']?> &nbsp;(<?=ucfirst($new_value['type'])?>) </td><td></td><td style='text-align:right'>
																														<input class="span10" type="text" placeholder="0.00" name='fee_heads[<?=$new_value['fee_id']?>]' value="<?=$new_value['amount']?>" add="<? if($new_value['type']=='Regular'): echo '1'; else: echo '0'; endif; ?>" readonly /></td></tr>
																														<? 		
																														if(ucfirst($new_value['type'])=='Regular'): 																
																															$ct_amount=$new_value['amount']*$object->frequency;																
																														else:
																															$ct_amount=$new_value['amount'];
																														endif;
																														$t_am=$t_am+$ct_amount;																
																													endforeach;
																												endif;
																												
																										#add Vehicle Fee if available 
																										if(is_object($Vehicle_Fee)):
																											$t_am=($t_am+($Vehicle_Fee->amount*$object->frequency));
																											echo "<tr><td>Vehicle Fee </td><td style='line-height:29px;text-align:right;'>".$object->frequency." X ".$Vehicle_Fee->amount."&nbsp;=</td><td style='text-align:right'><input class='span10' type='text' placeholder='0.00' value="; echo $Vehicle_Fee->amount*$object->frequency; echo " readonly /><input type='hidden' class='new_row change_value' add='1'  p_val='".($Vehicle_Fee->amount)."' name='vehicle_fee' value='".$Vehicle_Fee->amount."'/> </td></tr>";
																										endif;	
																													
																										
																										#add Total 
																										echo "<tr><td class='total'>Total Session Fee</td><td colspan='2' style='text-align:right' class='total'>".CURRENCY_SYMBOL." <span class='make_total_new'>".number_format($t_am,2)."</span></td></tr>";
																										
																										echo "<input type='hidden' value='".$t_am."' id='session_total'/>";
																										$AmountTotal=$t_am;
																										#add Attendance Fine if available 
																										if($Absent!='0'):
																											$AmountTotal=($AmountTotal+($object->absent_fine*$Absent));
																										echo "<tr><td class='total_paid'>Attendance Fine</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;+&nbsp;</strong><input type='text' class='span5 total_change' add='0' p_val='".($object->absent_fine*$Absent)."' name='absent_fine' value='".$object->absent_fine*$Absent."'/> <input type='hidden' value='".$Absent."' name='absent_days'/></td></tr>";
																										endif;															
																										
																										#Add Late Fee Fine if available 
																										if($LateFeeFine):
																											$AmountTotal=($AmountTotal+$LateFeeFine);
																										echo "<tr><td class='total_paid'>late Fee Fine</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;+&nbsp;<strong><input type='text' class='span5 total_change'  name='late_fee' value='".$LateFeeFine."'/> </td></tr>";
																										endif;	
																										
																										echo "<tr><td class='total_paid'>Total Paid </td><td colspan='2' style='text-align:right' class='total_paid'><strong class='large_text'>&nbsp;-&nbsp;</strong>".CURRENCY_SYMBOL." <span class='total_paid'>".number_format($Total_paid,2)."</span></td></tr>";
																											$AmountTotal=$AmountTotal-$Total_paid;									
																										
																										# Total Amount 
																										echo "<tr><td class='total'>Total</td><td class='total' colspan='2' style='text-align:right'>".CURRENCY_SYMBOL." <span class='AmountTotal'>".number_format($AmountTotal,2)."</span></td></tr>";
																																		
																								
																								else:
																										echo "<tr><td colspan='3'>Sorry, No Record Found..!</td></tr>";														
																								endif;?>
																						</tbody>
																						</table>
																					</div>
																					<input type='hidden' id="paid" value='<?=$Total_paid?>'/>
																				</div>
																				
																				<br/>
																				<div class=" portlet box blue">
																					<div class="portlet-title"><div class="caption">Add Fee</div></div>
																					<div class="portlet-body">
																					<div class="span6">
																					<div class="control-group">
																						<label for="payment_date" class="control-label">Date <span class="required">*</span></label>
																								<div class="controls">
																								  <input type="text" class="span10 m-wrap validate[required] new_format" id="payment_date" value="<?=date('Y-m-d')?>" name="payment_date" >
																								</div>
																						</div> 
																						<div class="control-group">
																								<label for="total_amount" class="control-label">Amount Paying <span class="required">*</span></label>
																								<div class="controls">
																								  <input type="text" class="span10 m-wrap validate[required],max[<?=$balance_amount?>]" id="total_amount"  value="" name="total_amount">
																								</div>
																						</div> 	
																					</div>
																					<div class="span6">	
																						<div class="control-group">
																									<label for="remarks" class="control-label">Remarks</label>
																									<div class="controls">
																									  <textarea name="remarks" class="span10 m-wrap"></textarea>
																									</div>
																						</div>	
																					</div>	
																					 <div class="clearfix"></div>
																					</div>

																				</div>
																					
																						<div class="form-actions clearfix">
																							<input type='hidden' value="<?=$object->frequency?>" id='frequency'/>
																							<input type='hidden' name='section' value='<?=$ct_sect?>'/>
																							<input type='hidden' name='student_id' value='<?=$student?>'/>
																							<input type='hidden' name='student' value='<?=$student?>'/>
																							<input type='hidden' name='session_id' value='<?=$session_id?>'/>
																																														
																							<a href='<?=make_admin_url('fee','insert','insert')?>' class="btn red" ><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
																							<input class="btn green check_valid" type="submit" value="Add Fee" name="submit_fee" >
																						</div>
																					
																			
																			
																			
																			<!-- First Time Entry-->
																			<? else: ?>
																				<div class="row-fluid">	
																					<!-- First Entry of the months -->	
																					<div class="span6">
																						<!-- Student Info -->
																						<div class="row-fluid">	
																						<div class="alert alert-block alert-success fade in">	
																								<div class="row-fluid">	
																									<div class='span4' style='line-height: 26px;'><strong>Student Type: </strong></div>
																									<div class='span6' style='line-height: 26px;'><?=$student_info->concession?></div>												
																								</div>	
																								<? if(!empty($student_info->concession_type)):?>
																									<div class="row-fluid">	
																										<div class='span4' style='line-height: 26px;'><strong>Concession: </strong></div>
																										<div class='span6' style='line-height: 26px;'><?=$student_info->concession_type?></div>												
																									</div>	
																								<? endif;?>	
																						</div>
																						</div>
																						<div class="alert alert-block alert-success fade in">																							
																								<div class="row-fluid">	
																									<div class='span4'>Session: </div>
																									<div class='span6'><?=$object->title?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Section: </div>
																									<div class='span6'><?=$ct_sect?></div>												
																								</div>																									
																								<div class="row-fluid">	
																									<div class='span4'>Reg Id: </div>
																									<div class='span6'><?=$student_info->reg_id?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Name: </div>
																									<div class='span6'><?=ucfirst($student_info->first_name).' '.$student_info->last_name?></div>												
																								</div>	
																								<div class="row-fluid">	
																									<div class='span4'>Roll No.: </div>
																									<div class='span6'><?=$student_info->roll_no?></div>												
																								</div>

																								
																								<? if(is_object($Vehicle_Fee)):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Vehicle Fee: </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($Vehicle_Fee->amount,2)?></div>												
																									</div>																									
																								<? endif;?>	

																								<? if($Absent>0):?>
																								<div class="row-fluid" >	
																									<div class='span5' id='color_red'>Attendance Fine: </div>
																									<div class='span6' id='color_red'>
																									<? if(!empty($AbsentDetail)):
																											echo "<table id='absent_table'><tr><th>Date</th><th style='text-align:right;'>Fine</th></tr>";
																											foreach($AbsentDetail as $a_key=>$a_v):
																												echo "<tr><td>".date('d, M Y',strtotime($a_v->date))."</td><td style='text-align:right;'>".CURRENCY_SYMBOL.number_format($object->absent_fine,2)."</td></tr>";
																											endforeach;
																											echo "<tr id='total_line'><td>Total</td><td style='text-align:right;'>".CURRENCY_SYMBOL.number_format(($object->absent_fine*$Absent),2)."</td></tr>";
																											echo "</table>";
																										endif;?><br/>							
																									</div>	
																								</div>	
																								<? endif;?>		
																								
																								<? if(!empty($LateFee)):?>
																									<div class="row-fluid" >	
																										<div class='span5' id='color_red'>Late Fee Fine: </div>
																										<div class='span6' id='color_red'>
																												<? 	echo "<table id='absent_table'><tr><th>Fee Dates</th><th style='text-align:right;'>Fine</th></tr>";
																														foreach($LateFee as $l_k=>$l_v):
																															foreach($l_v as $l_kk=>$l_vv):
																																echo "<tr><td>".date('d, M Y',strtotime($l_kk)).' - '.date('d, M Y',strtotime($l_vv))."</td><td style='text-align:right;'>".CURRENCY_SYMBOL.number_format($object->late_fee_rate,2)."</td></tr>";
																																$LateFeeFine=$LateFeeFine+$object->late_fee_rate;
																															endforeach;
																														endforeach;
																														echo "<tr id='total_line'><td>Total</td><td style='text-align:right;'>".CURRENCY_SYMBOL.number_format(($LateFeeFine),2)."</td></tr>";
																														echo "</table>";
																													?><br/>	
																										</div>												
																									</div>																									
																								<? endif;?>	
																						</div>
																						<!-- Student Info End-->
																					

																					</div>
																					<div class='span6'>
																						<div class="portlet-title green">
																							<div class="caption">Calculate Student Fee</div>
																							<div class="tools">
																									<div class="actions">
																										<a class="btn success mini" href="<?=make_admin_url('session','section','section&ct_session='.$session_id.'&id='.$student_info->rel_id.'&ct_section='.$student_info->section)?>" title='Click here to edit student fee'><i class="icon-pencil"></i> Edit</a>
																									</div>
																							</div>
																						</div>
																						<table class="table table-striped table-bordered table-advance table-hover">
																						<thead>
																						<tr><th style='width:50%;'>Fee Head Name</th style='width:30%;'><th></th><th style='width:20%;text-align:right;'>Amount (<?=CURRENCY_SYMBOL?>)</th>
																						</tr></thead>
																						<tbody>
																								<? $t_am='0'; $new_added=0;
																									if(!empty($pay_heads) && !empty($Sesspay_heads)): $st='0'; 
																										foreach($pay_heads as $p_k=>$p_v): 
																										 if(array_key_exists($p_v['fee_head'],$Sesspay_heads)):
																											$a=$p_v['fee_head'];
																											unset($Sesspay_heads[$a]);		
																										 endif;	?>
																											<tr class='delete_row<?=$p_v['fee_head']?>'><td><?=$p_v['title']?> &nbsp;(<?=ucfirst($p_v['type'])?>) </td><td style='line-height:29px;text-align:right;'>
																											<?php if($p_v['type']=='Regular'): echo $object->frequency." X ".$p_v['amount'].' ='; else: echo "1 X ".$p_v['amount'].' ='; endif;?></td><td style='text-align:right'>
																											<input class="span10" type="text" placeholder="0.00" value="<?php if($p_v['type']=='Regular'): echo $object->frequency*$p_v['amount']; else: echo 1*$p_v['amount']; endif;?>" readonly />
																											<input class="new_row change_value" type="hidden" placeholder="0.00" name='fee_heads[<?=$p_v['fee_id']?>]' value="<?=$p_v['amount']?>" add="<? if($p_v['type']=='Regular'): echo '1'; else: echo '0'; endif; ?>" readonly /></td></tr>
																								<? 		
																											if(ucfirst($p_v['type'])=='Regular'): 																
																												$ct_amount=$p_v['amount']*$object->frequency;																
																											else:
																												$ct_amount=$p_v['amount'];
																											endif;
																											$t_am=$t_am+$ct_amount;
																										endforeach;
																											# Now display if new heads of array
																													if(!empty($Sesspay_heads)): $new_added='1';
																														foreach($Sesspay_heads as $new_key=>$new_value):?>
																														<tr class='delete_row<?=$new_value['fee_head']?> removed'><td><?=$new_value['title']?> &nbsp;(<?=ucfirst($new_value['type'])?>)</td><td>
																															<input class="span10 new_row change_value" type="text" placeholder="0.00" name='fee_heads[<?=$new_value['fee_id']?>]' value="<?=$new_value['amount']?>" add="<? if($new_value['type']=='Regular'): echo '1'; else: echo '0'; endif; ?>" readonly /></td></tr>
																															<? 		
																															if(ucfirst($new_value['type'])=='Regular'): 																
																																$ct_amount=$new_value['amount']*$object->frequency;																
																															else:
																																$ct_amount=$new_value['amount'];
																															endif;
																															$t_am=$t_am+$ct_amount;																
																														endforeach;
																													endif;
																													
																											#add Vehicle Fee if available 
																											if(is_object($Vehicle_Fee)):
																												$t_am=($t_am+$Vehicle_Fee->amount*$object->frequency);
																											echo "<tr><td>Vehicle Fee</td><td style='line-height:29px;text-align:right;'>".$object->frequency." X ".$Vehicle_Fee->amount."&nbsp;=</td><td style='text-align:right'><input type='text' class='span10 new_row change_value' add='1'  p_val='".($Vehicle_Fee->amount)."' name='vehicle_fee' value='".$Vehicle_Fee->amount."' readonly /> </td></tr>";
																											endif;	
																											
																											#Session Total  
																											echo "<tr><td class='total'>Total Session Fee</td><td class='total' colspan='2' style='text-align:right;'>".CURRENCY_SYMBOL." <span class='make_total_new'>".number_format($t_am,2)."</span></td></tr>";
																											
																											echo "<input type='hidden' value='".$t_am."' id='session_total'/>";
																											$AmountTotal=$t_am;
																											#add Attendance Fine if available 
																											if($Absent!='0'):
																												$AmountTotal=($AmountTotal+($object->absent_fine*$Absent));
																											echo "<tr><td>Attendance Fine</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;+&nbsp;<input type='text' class='span5 total_change' add='0' p_val='".($object->absent_fine*$Absent)."' name='absent_fine' value='".$object->absent_fine*$Absent."'/> <input type='hidden' value='".$Absent."' name='absent_days'/></td></tr>";
																											endif;															
																											
																											#Add Late Fee Fine if available 
																											if(!empty($LateFeeFine)):
																												$AmountTotal=($AmountTotal+$LateFeeFine);
																											echo "<tr><td>late Fee Fine</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;+&nbsp;<strong><input type='text' class='span5 total_change'  name='late_fee' value='".$LateFeeFine."'/> </td></tr>";
																											endif;	
																											
																											# Total Amount 
																											echo "<tr><td class='total'>Total</td><td class='total' colspan='2' style='text-align:right'>".CURRENCY_SYMBOL." <span class='AmountTotal'>".number_format($AmountTotal,2)."</span></td></tr>";
																									
																									else:
																											echo "<tr><td colspan='3'>Sorry, No Record Found..!</td></tr>";														
																									endif;?>

																						</tbody>
																						</table>
																					</div>
																					<input type='hidden' id="paid" value='0'/>
																				</div>	
																				<br/>
																				<div class=" portlet box blue">
																					<div class="portlet-title"><div class="caption">Add Fee</div></div>
																					<div class="portlet-body">
																					<div class="span6">	
																						<div class="control-group">
																								<label for="payment_date" class="control-label">Date <span class="required">*</span></label>
																								<div class="controls">
																								  <input type="text" class="span10 m-wrap validate[required] new_format" id="payment_date" value="<?=date('Y-m-d')?>" name="payment_date" >
																								</div>
																						</div> 
																						<div class="control-group">
																								<label for="total_amount" class="control-label">Amount Paying  <span class="required">*</span></label>
																								<div class="controls">
																								  <input type="text" class="span10 m-wrap validate[required]" id="total_amount"  value="" name="total_amount">
																								</div>
																						</div>
																					</div>		
																					<div class="span6">		
																						<div class="control-group">
																									<label for="remarks" class="control-label">Remarks</label>
																									<div class="controls">
																									  <textarea name="remarks" class="span10 m-wrap"></textarea>
																									</div>
																						</div>																						
																					</div>
																					<div class="clearfix"></div>
																					</div>
																					</div>
																				</div>
																				
																					<div class="form-actions clearfix">
																						<input type='hidden' value="<?=$object->frequency?>" id='frequency'/>
																						<input type='hidden' name='section' value='<?=$ct_sect?>'/>
																						<input type='hidden' name='student_id' value='<?=$student?>'/>
																						<input type='hidden' name='student' value='<?=$student?>'/>
																						<input type='hidden' name='session_id' value='<?=$session_id?>'/>
																							
																						<a href='<?=make_admin_url('fee','insert','insert')?>' class="btn red" ><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
																						<input class="btn green check_valid" type="submit" value="Add Fee" name="submit_fee" >
																					</div>
																				<? endif;?>																					
	
																		</form>		
																		</div>
																	</div>
															</div>
																									
												<div class="clearfix"></div>
												
												
												
												<!-- ******************************************* -->
													<div class="row-fluid">
															<div class="span12">
																<!-- BEGIN EXAMPLE TABLE PORTLET-->
																<div class="portlet box green">
																	<div class="portlet-title">
																		<div class="caption"><i class="icon-list"></i>Previous Paid Fee List:</div>
																		<div class="tools"></div>
																	</div>
																	<div class="portlet-body">
																	<table class="table table-striped table-bordered table-hover" id="sample_2">
																			<thead>
																				<tr>
																					<th class="hidden-480" >Sr. No.</th>
																					<th class="hidden-480" style='text-align:center;'>Payment Date</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Amount Paid (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Action</th>
																				</tr>
																			</thead>
																			<? if(!empty($Fee_list)):?>
	
																			<tbody>
																			<?php $sr=1;foreach($Fee_list as $sk=>$rec): $checked='';?>
																				<tr class="odd gradeX">
																					<td><?=$sr?>.</td>
																					<td style='text-align:center;'><?=date('d M, Y',strtotime($rec->payment_date))?></td>
																					<td style='text-align:right;'><?=CURRENCY_SYMBOL.' '.number_format($rec->total_amount,2)?></td>
																					<td style='text-align:right;'>
																					<a class="btn blue icn-only tooltips mini" href="<?php echo make_admin_url('fee', 'update', 'update', 'id='.$rec->id.'&student='.$student.'&session_id='.$session_id.'&ct_sect='.$ct_sect.'&type='.$type)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
																					<a class="btn red icn-only tooltips mini" href="<?php echo make_admin_url('fee', 'delete', 'delete', 'id='.$rec->id.'&delete=1&student='.$student.'&session_id='.$session_id.'&ct_sect='.$ct_sect.'&type='.$type)?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
																					<? if(is_object($slip)):?>
																						&nbsp;&nbsp; <a class="btn black icn-only tooltips mini print_start" rec_id='<?=$rec->id?>' doc_id='<?=$slip->id?>' title="click here to print receipt">Print</a>
																					<? endif;?>
																					</td>
																				</tr>
																				<?

																					$sr++;
																				endforeach;?>									
																			</tbody>
																		<? endif;?>
																		</table>
																
																	<div class="clearfix"></div>
																	
																	</div>
																</div>
																						
																<!-- END EXAMPLE TABLE PORTLET-->
															</div>
														</div>	
												</div>					
											</div>
											<!-- Forth steps End -->
										</div>
										

									</div>
								
							</div>
						</div>
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

<script type="text/javascript">
	$(".session_filter_for_single").live("change", function(){
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_s").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section','ajax_section&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});
	$("#GetStudents").live("click", function(){ 
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_sect").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#show_students").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_with_st','ajax_section_with_st&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#show_students").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
	 
	$("#select_registration").live("click", function(){	 
			var reg_id=$('#reg_id').val();
			var ct_school=<?=$school->id;?>;
			
			if(reg_id.length > 0){					   
					   $("#dynamic_model_reg").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'reg_id='+reg_id+'&ct_school='+ct_school;
						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_for_reg_find','ajax_section_for_reg_find&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model_reg").html(data);
						$("#dynamic_model").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
</script> 


	<script>
		jQuery(document).ready(function() {    
            $(".check_valid").live("click",function(){
             // Validation engine
                    $(".validation").validationEngine({
                         promptPosition : "topLeft"
                        
                    });
          	});  
			
            $(".remove_field").live("click",function(){            
								var row_id=$(this).attr("field_id");
								var total=$('#make_total').html();
								total=total.replace(",","");
								var ct_val=$('.row-'+row_id).find('td input:text').val();
								ct_val=ct_val.replace(",","");
								
								$(this).removeClass('icon-remove');
								$(this).removeClass('remove_field');
								$(this).addClass('icon-plus');
								$(this).addClass('add_field');
								$('.row-'+row_id).addClass('removed');
								$('.row-'+row_id).find('td input:text').attr('disabled', true);
								$('.row-'+row_id).find('.head').attr('disabled', true);
								
								var new_val=total-ct_val;
								$('#make_total').html(new_val);	
								$('#amount_val').val(new_val);								
          	}); 
            $(".add_field").live("click",function(){            
								var row_id=$(this).attr("field_id");
								var total=$('#make_total').html();
								total=total.replace(",","");
								var ct_val=$('.row-'+row_id).find('td input:text').val();								
								ct_val=ct_val.replace(",","");
								
								$(this).removeClass('icon-plus');
								$(this).removeClass('add_field');
								$(this).addClass('icon-remove');
								$(this).addClass('remove_field');
								$('.row-'+row_id).removeClass('removed');
								$('.row-'+row_id).find('td input:text').removeAttr('disabled');
								$('.row-'+row_id).find('.head').removeAttr('disabled');
								
								var new_val=parseInt(ct_val) + parseInt(total);
								$('#make_total').html(new_val);	
								$('#amount_val').val(new_val);									
          	}); 
 				
		});
		
	</script>
	
	 <script type="text/javascript">        
		jQuery(document).ready(function() {
		$(".change_value").live("change",function(){	
					GetValue();								
		});	
		});
	 
	 
        jQuery(document).ready(function() {
		$(".total_change").live("change",function(){	
					GetValueForTotal();								
		});	
		});
	
		function GetValue(){
		
			var Contain = "0";
			var frequency=0;
			var balance='0';
			frequency=$("#frequency").val();
			var paid=$("#paid").val();
			if(frequency==""){
				frequency=1;			
			}
		
			$(".new_row").each(function(){	
				if($(this).val()!=''){
					if($(this).attr('add')=="1"){
							Contain1 = parseInt(frequency) * parseInt($(this).val());							
					}
					else{						
							Contain1 = parseInt($(this).val());							
						} 	
				}					
					Contain=parseInt(Contain)+parseInt(Contain1);					
			});	

			balance=parseInt(Contain)-parseInt(paid);
			
			$('.make_total_new').html(Contain);	
			$('.total_bal_show').html(balance);
		}	
		
		function GetValueForTotal(){
			var newamount='';
			var Contain='';
			var paid=$("#paid").val();
			var session_total=$("#session_total").val();
			
			if(frequency==""){
				frequency=1;			
			}
		
			$(".total_change").each(function(){						
					newamount = parseInt($(this).val());
					session_total=parseInt(session_total)+parseInt(newamount);					
			});	
			session_total=session_total-paid;	
			$('.AmountTotal').html(session_total);	
			
		}

	$(".print_start").live("click",function(){  
				var id=$(this).attr('rec_id');
				var doc_id=$(this).attr('doc_id');				
				var dataString = 'id='+id+'&doc_id='+doc_id;				
				$.ajax({
				type: "POST",						
				url: "<?=make_admin_url_window('ajax_calling','print','print&temp=fee');?>",
				data: dataString,
				success: function(data, textStatus) {
					var DocumentContainer = document.getElementById(data);
					var WindowObject = window.open('', "PrintWindow", "");
					WindowObject.document.writeln(data);
					WindowObject.document.close();
					WindowObject.focus();
					WindowObject.print();
					WindowObject.close();
				return false;
				}				
				});
	});		
 </script>		