<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');

if($_POST):
isset($_POST['reg_id'])?$reg_id=$_POST['reg_id']:$reg_id='';
isset($_POST['ct_school'])?$ct_school=$_POST['ct_school']:$ct_school='';
		

			#Get Section Students
			$QuerySec=new studentSession();
			$records=$QuerySec->findCurrentYearStudentsByRegId($ct_school,$reg_id);
			
			
?>			
			
				<div class="span12">
				<div class="control-group" >
					<label class="control-label">Select Student </label>
					<div class="controls">
								<table id="sample" class="table table-striped table-bordered table-advance table-hover">
									<thead>
										<tr>
											<th style='text-align:center;' class="hidden-480 sorting_disabled"></th>
											<th>Reg ID</th>
											<th class="hidden-480" >Name</th>
											<th class="hidden-480" >Father Name</th>
											<th class="hidden-480" >Session Name</th>
											<th class="hidden-480" style='text-align:center;'>Section</th>
											<th class="hidden-480" style='text-align:center;'>Roll No.</th>	
											<th class="hidden-480" style='text-align:center;'></th>												
										</tr>
									</thead>
									
									<? if(!empty($records)):?>
									<tbody>
									<?php $sr=1;foreach($records as $sk=>$stu): $checked='';?>
										<tr class="odd gradeX">
											<td style='text-align:center;background:#F9F9F9;'>
													<input type="radio" name='student' checked value='<?=$stu->id?>'/>
											</td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->reg_id?></td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->father_name)?></td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->session_name?></td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:center;'><?=$stu->section?></td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:center;'><?=$stu->roll_no?></td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:center;'>
											<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
											<input type='hidden' name='Page' value='fee'/>
											<input type='hidden' name='action' value='insert'/>
											<input type='hidden' name='section' value='insert'/>														
											<input type='hidden' name='type' value='fee'/>
											<input type='hidden' name='session_id' value='<?=$stu->session_id?>'/>
											<input type='hidden' name='ct_sect' value='<?=$stu->section?>'/>
											<input type='hidden' name='student' value='<?=$stu->student_id?>'/>
											<button type='submit' value='Submit' class='btn green'>Pay Now</button>
											</form>
											</td>
											</tr>
									<? $sr++; endforeach;?>									
									</tbody>
									<? else:?>
									<tr class="odd gradeX"><td></td><td colspan='6'>Sorry, No Record Found..!</td></tr>
									<? endif;?>
								</table>
					</div>
				</div>
				</div>	
			
<?		
endif;
?>