

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Students Fee
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Update Student Fee
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
			<? if($type!='select'):?>
				<div class="tiles pull-right">
							<div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
								<a href="<?php echo make_admin_url('fee', 'list', 'list');?>">
									<div class="corner"></div>
									<div class="tile-body"><i class="icon-arrow-left"></i></div>
									<div class="tile-object"><div class="name"> Change Session</div></div>
								</a> 
							</div>
				</div>   		
			<? endif;?>		
			
          
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
             
			<div class="row-fluid">
					<div class="span12">
						<div id="form_wizard_1" class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-reorder"></i> Student Fee Wizard - <span class="step-title">Step 1 of 2</span>
								</div>
							</div>
							<div class="portlet-body form">
								<div class="form-wizard">						
										<!-- Main steps -->
										<div class="navbar steps">
											<div class="navbar-inner">
												<ul class="row-fluid nav nav-pills">
													<li class="span4 <?=($type=='select')?'active':'';?>">
														<a class="step <?=($type=='select')?'active':'';?>">
														<span class="number">1</span>
														<span class="desc"><i class="icon-ok"></i> Select Student </span>   
														</a>
													</li>
													
													<li class="span5 <?=($type=='fee')?'active':'';?>">
														<a class="step <?=($type=='fee')?'active':'';?>">
														<span class="number">2</span>
														<span class="desc"><i class="icon-ok"></i> Update Student Fee</span>   
														</a>
													</li>
												</ul>
											</div>
										</div>
										<!-- Main End -->
										<div class="progress progress-success progress-striped" id="bar">
											<div class="bar" style="width: <?=$per?>"></div>
										</div>
										
										
										<div class="tab-content">
										
											<!-- First steps -->
											<div id="tab1" class="tab-pane <?=($type=='select')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'list', 'list')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='list'/>
												<input type='hidden' name='section' value='list'/>
												<input type='hidden' name='type' value='list'/>											
											
												<h3 class="block">Select Session & Section</h3>
													<div class="alert alert-block alert-success">
														<div class="row-fluid">
															<div class="span6">								
																<div class="control-group">
																	<label class="control-label">Select Session</label>
																	<div class="controls">
																		<select class="select2_category session_filter_for_single span9" data-placeholder="Select Session Students" name='session_id' id='session_id'>
																			<option value="">Select Session</option>
																			<?php $session_sr=1;while($session=$QuerySession->GetObjectFromRecord()): ?>
																					<option value='<?=$session->id?>' <? if($session->id==$session_id){ echo 'selected'; }?> ><?=ucfirst($session->title)?></option>
																			<? $session_sr++; endwhile;?>
																		</select>
																	</div>
																</div>
															</div>
																<div id='dynamic_model' class='span6'>			
																	<input type='hidden' class='section_filter' value='A' id='ct_s'/>
																</div>															
														</div>
													</div>	
													<div class="clearfix"></div>
													
													<div class="form-actions clearfix" >
															<a href="<?php echo make_admin_url('fee', 'list', 'list');?>" class="btn red" name="cancel" > Cancel</a>
															
													</div>
												</form>	
											</div>
											<!-- First steps End -->
											
											<!-- Second steps -->
											<div id="tab2" class="tab-pane <?=($type=='list')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='fee'/>
												<input type='hidden' name='session_id' value='<?=$session_id?>'/>
												<input type='hidden' name='ct_sect' value='<?=$ct_sect?>'/>												
												<input type='hidden' name='month' value='<?=$month?>'/>	
												<input type='hidden' name='year' value='<?=$year?>'/>
												<h3 id="title_show" class="block alert alert-info"><?=$object->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?></h3>
													
													<div class="span12" style='margin-left:0px;'>				
														<div class="controls">
																	<table id="sample" class="table table-striped table-bordered table-advance table-hover">
																		<thead>
																			<tr>
																				<th class="hidden-480">Reg ID</th>
																				<th class="hidden-480" >Roll No.</th>
																				<th class="hidden-480" >Name</th>
																				<th class="hidden-480" >Father Name</th>
																					
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total Session Fee (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total Paid (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Balance (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'></th>				
																			</tr>
																		</thead>
																		<tbody>
																		<? if(!empty($records)):?>
																		<?php $sr=1;foreach($records as $sk=>$stu): $checked=''; ?>
																			<tr class="odd gradeX">
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->reg_id?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->roll_no?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->father_name)?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:center;'><?=CURRENCY_SYMBOL." ".number_format($stu->total_fee,2)?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:center;'><?=CURRENCY_SYMBOL." ".number_format($stu->paid,2)?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:right;'><span class='A'><?=CURRENCY_SYMBOL." ".number_format($stu->total_fee-$stu->paid,2)?></span></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:right;'>
																				<? if($stu->total_fee-$stu->paid>0):?>
																						<a href='<?=make_admin_url('fee','insert','insert&type=fee&session_id='.$session_id.'&ct_sect='.$ct_sect.'&student='.$stu->id)?>' class="btn green mini">Pay Now</a>
																				<? endif;?></td>
																				</tr>
																		<? $sr++; endforeach;
																		   else:	?>
																		<tr class="odd gradeX"><td></td><td colspan='7'>Sorry, No Record Found..!</td></tr>
																	<? endif;?>
																		</tbody>
																	</table>
														</div>
													</div>
													<div class="clearfix"></div>
									
													<!--
													<div class="form-actions clearfix" >
															<a href="<?php echo make_admin_url('fee', 'insert', 'insert');?>" class="btn red" name="cancel" > Cancel</a>
															<input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
													</div>
													-->
												</form>	
											</div>
											<!-- Second steps End -->											
											
											<!-- Third steps -->
											<div id="tab3" class="tab-pane <?=($type=='fee')?'active':'';?>">
												<h3 class="block">Student Fee</h3>
														<div class="row-fluid">
															<div class="span12">
																	<div class="portlet box blue">
																		<div class="portlet-title">
																			<div class="caption"><i class="icon-user"></i>Student Detail</div>
																		</div>
																		<div class="portlet-body">
																		 <br/>
																				<div class="row-fluid">	
																					<div class="span12 "> 
																						<!-- Student Info -->
																						<div class="alert alert-block alert-success fade in">
																							<button data-dismiss="alert" class="close" type="button"></button>
																								<div class="row-fluid">	
																									<div class='span4'>Session: </div>
																									<div class='span6'><?=$object->title?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Section: </div>
																									<div class='span6'><?=$ct_sect?></div>												
																								</div>																								
																								<div class="row-fluid">	
																									<div class='span4'>Reg Id: </div>
																									<div class='span6'><?=$student_info->reg_id?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Name: </div>
																									<div class='span6'><?=ucfirst($student_info->first_name).' '.$student_info->last_name?></div>												
																								</div>	
																								<div class="row-fluid">	
																									<div class='span4'>Roll No.: </div>
																									<div class='span6'><?=$student_info->roll_no?></div>												
																								</div>																					
																						</div>
																						<!-- Student Info End-->
																					
																					</div>
																					
																				</div>	
																		
																		</div>
																	</div>
															</div>
														</div>												
												<div class="clearfix"></div>
												
												
												
												<!-- ******************************************* -->
													<div class="row-fluid">
															<div class="span12">
																<!-- BEGIN EXAMPLE TABLE PORTLET-->
																<div class="portlet box green">
																	<div class="portlet-title">
																		<div class="caption"><i class="icon-list"></i>Previous Fee List</div>
																		<div class="tools"></div>
																	</div>
																	<div class="portlet-body">
																	<table class="table table-striped table-bordered table-hover" id="sample_2">
																			<thead>
																				<tr>
																					<th class="hidden-480" >Sr. No.</th>
																					<th class="hidden-480" style='text-align:center;'>On Date</th>
																					<th class="hidden-480" style='vertical-align:top;'>Fee Types</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" >Action</th>
																				</tr>
																			</thead>
																			<? if(!empty($Fee_list)):?>
																			<tbody>
																			<?php $sr=1;foreach($Fee_list as $sk=>$rec): $checked='';?>
																				<tr class="odd gradeX">
																					<td><?=$sr?>.</td>
																					<td style='text-align:center;'><?=$rec->payment_date?></td>
																					<td>
																					<?
																						$QueryFeeType = new session_fee_type();
																						$fee_head_array=$QueryFeeType->sessionPayHeadsNameArray($rec->session_id);
																					    if(!empty($fee_head_array)): echo implode(', ',$fee_head_array);endif;
																					?></td>
																					<td style='text-align:right;'><?=CURRENCY_SYMBOL.' '.number_format($rec->amount,2)?></td>
																					<td>
																							<a class="btn blue icn-only tooltips" href="<?php echo make_admin_url('fee', 'update', 'update', 'type=fee&session_id='.$session_id.'&ct_sect='.$ct_sect.'&student='.$student.'&id='.$rec->id)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
																							<a class="btn red icn-only tooltips" href="<?php echo make_admin_url('fee', 'delete', 'delete', 'type=fee&session_id='.$session_id.'&ct_sect='.$ct_sect.'&student='.$student.'&id='.$rec->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this payment record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
																					</td>
																				</tr>
																			<? $sr++;
																				endforeach;?>									
																			</tbody>
																		<? endif;?>
																		</table>
																	<div class="clearfix"></div>
																	</div>
																</div>
																						
																	<div class="form-actions clearfix">																																							
																		<a href='<?=make_admin_url('fee','list','list')?>' class="btn red" ><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Go Back</a>
																	</div>																	<!-- END EXAMPLE TABLE PORTLET-->
															</div>
														</div>											
											</div>
											<!-- Third steps End -->
										</div>
										

									</div>
								
							</div>
						</div>
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

<script type="text/javascript">
	$(".session_filter_for_single").live("change", function(){
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_s").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section','ajax_section&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});
	$("#GetStudents").live("click", function(){ 
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_sect").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#show_students").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_with_st','ajax_section_with_st&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#show_students").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
	 
	$("#select_registration").live("click", function(){	 
			var reg_id=$('#reg_id').val();
			var ct_school=<?=$school->id;?>;
			
			if(reg_id.length > 0){					   
					   $("#dynamic_model_reg").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'reg_id='+reg_id+'&ct_school='+ct_school;
						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_for_reg_find','ajax_section_for_reg_find&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model_reg").html(data);
						$("#dynamic_model").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
</script> 


	<script>
		jQuery(document).ready(function() {    
            $(".check_valid").live("click",function(){
             // Validation engine
                    $(".validation").validationEngine({
                         promptPosition : "topLeft"
                        
                    });
          	});  
		});
	</script>