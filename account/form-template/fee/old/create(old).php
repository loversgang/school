

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Students Fee
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Add Student Fee
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
		
			
          
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
             
			<div class="row-fluid">
					<div class="span12">
						<div id="form_wizard_1" class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-reorder"></i> Student Fee Wizard - <span class="step-title">Step 1 of 3</span>
								</div>
							</div>
							<div class="portlet-body form">
								<div class="form-wizard">						
										<!-- Main steps -->
										<div class="navbar steps">
											<div class="navbar-inner">
												<ul class="row-fluid nav nav-pills">
													<li class="span4 <?=($type=='select')?'active':'';?>">
														<a class="step <?=($type=='select')?'active':'';?>">
														<span class="number">1</span>
														<span class="desc"><i class="icon-ok"></i> Select Student </span>   
														</a>
													</li>
													
													<li class="span4 <?=($type=='list')?'active':'';?>">
														<a class="step <?=($type=='list')?'active':'';?>">
														<span class="number">2</span>
														<span class="desc"><i class="icon-ok"></i> Section Student</span>   
														</a>
													</li>
													
													<li class="span4 <?=($type=='fee')?'active':'';?>">
														<a class="step <?=($type=='fee')?'active':'';?>">
														<span class="number">3</span>
														<span class="desc"><i class="icon-ok"></i> Add Student Fee</span>   
														</a>
													</li>													
												</ul>
											</div>
										</div>
										<!-- Main End -->
										<div class="progress progress-success progress-striped" id="bar">
											<div class="bar" style="width: <?=$per?>"></div>
										</div>
										
										
										<div class="tab-content">
										
											<!-- First steps -->
											<div id="tab1" class="tab-pane <?=($type=='select')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='list'/>											
											
												<h3 class="block">Select Session & Section</h3>
													<div class="alert alert-block alert-success">
														<div class="row-fluid">
															<div class="span6">								
																<div class="control-group">
																	<label class="control-label">Current Session</label>
																	<div class="controls">
																		<select class="select2_category session_filter_for_single span9" data-placeholder="Select Session Students" name='session_id' id='session_id'>
																			<option value="">Select Session</option>
																			<?php $session_sr=1;while($session=$QuerySession->GetObjectFromRecord()): ?>
																					<option value='<?=$session->id?>' <? if($session->id==$session_id){ echo 'selected'; }?> ><?=ucfirst($session->title)?></option>
																			<? $session_sr++; endwhile;?>
																		</select>
																	</div>
																</div>
															</div>
																<div id='dynamic_model'>			
																	<input type='hidden' class='section_filter' value='A' id='ct_s'/>
																</div>															
														</div>
													</div>	
													
													
													<div class="form-actions clearfix" >
															<a href="<?php echo make_admin_url('fee', 'list', 'list');?>" class="btn red" name="cancel" > Cancel</a>
															<input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
													</div>
												</form>	
											</div>
											<!-- First steps End -->
											
											
											
											<!-- Second steps -->
											<div id="tab2" class="tab-pane <?=($type=='list')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='fee'/>
												<input type='hidden' name='session_id' value='<?=$session_id?>'/>
												<input type='hidden' name='ct_sect' value='<?=$ct_sect?>'/>												
												<input type='hidden' name='month' value='<?=$month?>'/>	
												
												<h3 id="title_show" class="block alert alert-info"><?=$object->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?> <font class="green_color"> &gt; </font> <?=date('M Y',strtotime(date('Y').'-'.$month.'-'.date('d')))?></h3>
													<div class="span12">				
														<div class="controls">
																	<table id="sample" class="table table-striped table-bordered table-advance table-hover">
																		<thead>
																			<tr>
																				<th style='text-align:center;' class="hidden-480 sorting_disabled"></th>
																				<th class="hidden-480">Reg ID</th>
																				<th class="hidden-480" >Roll No.</th>
																				<th class="hidden-480" >Name</th>
																				<th class="hidden-480" >Father Name</th>
																			</tr>
																		</thead>
																		<tbody>
																		<? if(!empty($records)):?>
																		
																		<?php $sr=1;foreach($records as $sk=>$stu): $checked='';?>
																			<tr class="odd gradeX">
																				<td style='text-align:center;background:#F9F9F9;'>
																				<input type="radio" name='student' checked value='<?=$stu->id?>' style='margin-left:0px;'/>
																				</td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->reg_id?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->roll_no?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->father_name)?></td>
																				</tr>
																		<? $sr++; endforeach;
																		   else:	?>
																		<tr class="odd gradeX"><td></td><td colspan='4'>Sorry, No Record Found..!</td></tr>
																	<? endif;?>
																		</tbody>
																	</table>
														</div>
													</div>
													
									
													<div class="form-actions clearfix" >
															<a href="<?php echo make_admin_url('fee', 'insert', 'insert');?>" class="btn red" name="cancel" > Cancel</a>
															<input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
													</div>
												</form>	
											</div>
											<!-- Second steps End -->
											
											
											<!-- Third steps -->
											<div id="tab2" class="tab-pane <?=($type=='fee')?'active':'';?>">
												<h3 id="title_show" class="block alert alert-info"><?=$object->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?>  <font class="green_color"> &gt; </font> <?=date('M Y',strtotime(date('Y').'-'.$month.'-'.date('d')))?></h3>
														<div class="row-fluid">
															<div class="span12">
																	<div class="portlet box blue">
																		<div class="portlet-title"> 
																			<div class="caption">Fee Information : </div>
																		</div>
																		<div class="portlet-body">
																		 <form class="form-horizontal validation" action="<?php echo make_admin_url('fee', 'insert', 'insert&type=fee&month='.$month)?>" method="POST" enctype="multipart/form-data" id="validation">
																				<br/>
																				<div class="row-fluid">
																				<? if(!empty($Fee_list)):?>																
																					<div class="span6 "> 
																						<!-- Student Info -->
																						<div class="alert alert-block alert-success fade in">																							
																								<div class="row-fluid">	
																									<div class='span4'>Session: </div>
																									<div class='span6'><?=$object->title?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Section: </div>
																									<div class='span6'><?=$ct_sect?></div>												
																								</div>																									
																								<div class="row-fluid">	
																									<div class='span4'>Reg Id: </div>
																									<div class='span6'><?=$student_info->reg_id?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Name: </div>
																									<div class='span6'><?=ucfirst($student_info->first_name).' '.$student_info->last_name?></div>												
																								</div>	
																								<div class="row-fluid">	
																									<div class='span4'>Roll No.: </div>
																									<div class='span6'><?=$student_info->roll_no?></div>												
																								</div>
																								<? if(($Absent*$object->absent_fine)>$balance_absent_fine):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Attendance Fine: </div>
																										<div class='span6' id='color_red'><?=$Absent?> * <?=CURRENCY_SYMBOL.''.number_format($object->absent_fine,2)?></div>												
																									</div>	
																								<? endif;?>	
																								<? if(date('d')>=$object->late_fee_days):?>
																									<? if(($object->late_fee_rate)>$balance_late_fee):?>
																										<div class="row-fluid" >	
																											<div class='span4' id='color_red'>Late Fee Fine: </div>
																											<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format(($object->late_fee_rate-$balance_late_fee),2)?></div>												
																										</div>																									
																									<? endif;?>
																								<? endif;?>
																								<div class="row-fluid" >	
																											<div class='span4' id='color_red'>Balance Due: </div>
																											<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.' '.number_format(($balance_amount),2)?></div>												
																								</div>	
																						</div>
																						<!-- Student Info End-->
																					
																						<? if(!empty($balance_amount)):?>
																						<div class="control-group">
																								<label for="payment_date" class="control-label">Date <span class="required">*</span></label>
																								<div class="controls">
																								  <input type="text" class="span10 m-wrap normal_date validate[required]" id="payment_date" value="<?=date('Y/m/d')?>" name="payment_date">
																								</div>
																						</div> 
																						<div class="control-group">
																								<label for="amount" class="control-label">Amount Paid <span class="required">*</span></label>
																								<div class="controls">
																								  <input type="text" class="span10 m-wrap validate[required],max[<?=$balance_amount?>]" id="amount"  value="" name="amount">
																								</div>
																						</div> 																						
																						<div class="control-group">
																									<label for="remarks" class="control-label">Remarks</label>
																									<div class="controls">
																									  <textarea name="remarks" class="span10 m-wrap"></textarea>
																									</div>
																						</div>	
																						<? endif;?>
																					</div>
																					
																					<? if(!empty($balance_amount)):?>
																					<div class='span6'>
																						<div class="portlet-title green">
																							<div class="caption">Calculate Fee</div>
																							<div class="tools"></div>
																						</div>
																						<table class="table table-striped table-bordered table-advance table-hover">
																						<thead>
																						<tr><th style='width:60%;'>Fee Head Name</th><th>Amount (<?=CURRENCY_SYMBOL?>)</th><th>Action</th>
																						</tr></thead>
																						<tbody>
																								<? if(!empty($all_heads)): $srr=1;
																									foreach($all_heads as $fee_key=>$fee_value):
																									$input_class=''; $tr_class=''; $icon_class=''; $icon_field='';
																									
																									$input_class=(in_array($fee_value['fee_id'],$paid_heads))?'readonly=""':'';
																									$tr_class=(in_array($fee_value['fee_id'],$paid_heads))?'removed':'';
																									$icon_class=(in_array($fee_value['fee_id'],$paid_heads))?'plus':'remove';
																									$icon_field=(in_array($fee_value['fee_id'],$paid_heads))?'add_field':'remove_field';
																									$icon_disable=(in_array($fee_value['fee_id'],$paid_heads))?'disabled=""':'';
																									
																									?>	
																									<? if(empty($Fee_list)):?>
																									<tr class='row-<?=$srr?> <?=$tr_class?>'><td><?=ucfirst($fee_value['title'])?> &nbsp;(<?=ucfirst($fee_value['type'])?>)</td>
																									<td>
																										<input type="text" name='fee_heads[<?=$fee_value['fee_id']?>]' value="<?=number_format($fee_value['amount'],2)?>"  placeholder="0.00" class="span7" <?=$input_class?> <?=$icon_disable?>>
																									</td>
																									<td><i class="icon-<?=$icon_class?> <?=$icon_field?>" field_id='<?=$srr?>' style='font-size: 20px; cursor: pointer;'></i></td>
																									</tr>
																									
																									
																								<?  if(!in_array($fee_value['fee_id'],$paid_heads)): 
																											$amount=$amount+$fee_value['amount']; 
																									endif; $srr++;
																									endif;
																									
																									
																									endforeach;
																										
																										#Attendance Fine
																										if(($Absent*$object->absent_fine)>$balance_absent_fine):	
																											echo "<tr><td>Attendance Fine</td><td><input type='text' class='span7' name='absent_fine' value='".number_format($object->absent_fine*$Absent,2)."'/> <input type='hidden' value='".$Absent."' name='absent_days'/></td><td>&nbsp;</td></tr>";
																										$amount=($amount+($object->absent_fine*$Absent));
																										endif;
																										
																										#Late fee Fine
																										if(date('d')>=$object->late_fee_days):
																											if(($object->late_fee_rate)>$balance_late_fee):
																												echo "<tr><td>Late Fee Fine</td><td><input type='text' name='late_fee' class='span7' value='".number_format($object->late_fee_rate-$balance_late_fee,2)."'/></td><td>&nbsp;</td></tr>";																										
																												$amount=$amount+$object->late_fee_rate;
																											endif;	
																										endif;
																										
																										#Balance
																										$amount=$amount+$balance_amount;
																										echo "<tr><td>Balance Due:</td><td><input type='text' readonly class='span7' value='".number_format($balance_amount,2)."'/></td><td>&nbsp;</td></tr>";
																										
																										echo "<tr><td class='total'>Total</td><td class='total'>".CURRENCY_SYMBOL." <span id='make_total'>".number_format($amount,2)."</span></td><td class='total'></td></tr>";
																									else:
																										 echo "<tr><td colspan='3'>Sorry, No Record Found..!</td></tr>";	
																								endif;?>
																						</tbody>
																						</table>
																					</div>
																					<? endif;?>
																					
																				<? else:?>
																				
																					<!-- First Entry of the months -->	
																					<div class="span6">
																						<!-- Student Info -->
																						<div class="alert alert-block alert-success fade in">
																							<button data-dismiss="alert" class="close" type="button"></button>
																								<div class="row-fluid">	
																									<div class='span4'>Session: </div>
																									<div class='span6'><?=$object->title?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Section: </div>
																									<div class='span6'><?=$ct_sect?></div>												
																								</div>																									
																								<div class="row-fluid">	
																									<div class='span4'>Reg Id: </div>
																									<div class='span6'><?=$student_info->reg_id?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Name: </div>
																									<div class='span6'><?=ucfirst($student_info->first_name).' '.$student_info->last_name?></div>												
																								</div>	
																								<div class="row-fluid">	
																									<div class='span4'>Roll No.: </div>
																									<div class='span6'><?=$student_info->roll_no?></div>												
																								</div>
																								<div class="row-fluid" >	
																									<div class='span4' id='color_red'>Attendance Fine: </div>
																									<div class='span6' id='color_red'><?=$Absent?> * <?=CURRENCY_SYMBOL.''.number_format($object->absent_fine,2)?></div>												
																								</div>																								
						
																								<? if(date('d')>=$object->late_fee_days):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Late Fee Fine: </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($object->late_fee_rate,2)?></div>												
																									</div>																									
																								<? endif;?>
																						</div>
																						<!-- Student Info End-->
																					
																						<div class="control-group">
																								<label for="payment_date" class="control-label">Date <span class="required">*</span></label>
																								<div class="controls">
																								  <input type="text" class="span10 m-wrap normal_date validate[required]" id="payment_date" value="<?=date('Y/m/d')?>" name="payment_date">
																								</div>
																						</div> 
																						<div class="control-group">
																								<label for="amount" class="control-label">Amount Paid <span class="required">*</span></label>
																								<div class="controls">
																								  <input type="text" class="span10 m-wrap validate[required]" id="amount"  value="" name="amount">
																								</div>
																						</div> 																						
																						<div class="control-group">
																									<label for="remarks" class="control-label">Remarks</label>
																									<div class="controls">
																									  <textarea name="remarks" class="span10 m-wrap"></textarea>
																									</div>
																						</div>	
																					</div>
																					<div class='span6'>
																						<div class="portlet-title green">
																							<div class="caption">Calculate Fee</div>
																							<div class="tools"></div>
																						</div>
																						<table class="table table-striped table-bordered table-advance table-hover">
																						<thead>
																						<tr><th style='width:60%;'>Fee Head Name</th><th>Amount (<?=CURRENCY_SYMBOL?>)</th><th>Action</th>
																						</tr></thead>
																						<tbody>
																								<? if(!empty($all_heads)): $srr=1;
																									foreach($all_heads as $fee_key=>$fee_value):
																									$input_class=''; $tr_class=''; $icon_class=''; $icon_field='';
																									
																									$input_class=($fee_value['type']=='One Time')?'readonly=""':'';
																									$tr_class=($fee_value['type']=='One Time')?'removed':'';
																									$icon_class=($fee_value['type']=='One Time')?'plus':'remove';
																									$icon_field=($fee_value['type']=='One Time')?'add_field':'remove_field';
																									$icon_disable=($fee_value['type']=='One Time')?'disabled=""':'';
																									
																									?>	
																									<? if(empty($Fee_list)):?>
																									<tr class='row-<?=$srr?> <?=$tr_class?>'><td><?=ucfirst($fee_value['title'])?> &nbsp;(<?=ucfirst($fee_value['type'])?>)</td>
																									<td>
																										<input type="text" name='fee_heads[<?=$fee_value['fee_id']?>]' value="<?=number_format($fee_value['amount'],2)?>"  placeholder="0.00" class="span7" <?=$input_class?> <?=$icon_disable?>>
																										
																									</td>
																									<td><i class="icon-<?=$icon_class?> <?=$icon_field?>" field_id='<?=$srr?>' style='font-size: 20px; cursor: pointer;'></i></td>
																									</tr>
																									
																									
																										<?  if($fee_value['type']!='One Time'): 
																													$amount=$amount+$fee_value['amount']; 
																											endif; $srr++;
																									endif;
																									
																									endforeach;
																											$amount=($amount+($object->absent_fine*$Absent));
																											echo "<tr><td>Attendance Fine</td><td><input type='text' class='span7' name='absent_fine' value='".number_format($object->absent_fine*$Absent,2)."'/> <input type='hidden' value='".$Absent."' name='absent_days'/></td><td>&nbsp;</td></tr>";
																										if(date('d')>=$object->late_fee_days):
																											echo "<tr><td>Late Fee Fine</td><td><input type='text' name='late_fee' class='span7' value='".number_format($object->late_fee_rate,2)."'/></td><td>&nbsp;</td></tr>";																										
																											$amount=$amount+$object->late_fee_rate;
																										endif;
																											echo "<tr><td class='total'>Total</td><td class='total'>".CURRENCY_SYMBOL." <span id='make_total'>".number_format($amount,2)."</span></td><td class='total'></td></tr>";
																									else:
																										 echo "<tr><td colspan='3'>Sorry, No Record Found..!</td></tr>";	
																								endif;?>
																						</tbody>
																						</table>
																					</div>
																				<? endif;?>	
																				</div>	

																				<? if(!empty($Fee_list)):?> 	
																					<? if(!empty($balance_amount)):?>
																						<div class="form-actions clearfix">
																							<input type='hidden' name='section' value='<?=$ct_sect?>'/>
																							<input type='hidden' name='student_id' value='<?=$student?>'/>
																							<input type='hidden' name='student' value='<?=$student?>'/>
																							<input type='hidden' name='session_id' value='<?=$session_id?>'/>
																							<input type='hidden' name='total_amount' id='amount_val' value='<?=$amount?>'/>																						
																								<a href='<?=make_admin_url('fee','insert','insert')?>' class="btn red" ><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
																							<input class="btn green check_valid" type="submit" value="Add Fee" name="submit_fee" >
																						</div>
																					<? endif;?>	
																				<? else:?>
																						<div class="form-actions clearfix">
																							<input type='hidden' name='section' value='<?=$ct_sect?>'/>
																							<input type='hidden' name='month' value='<?=$month?>'/>
																							<input type='hidden' name='student_id' value='<?=$student?>'/>
																							<input type='hidden' name='student' value='<?=$student?>'/>
																							<input type='hidden' name='session_id' value='<?=$session_id?>'/>
																							<input type='hidden' name='total_amount' id='amount_val' value='<?=$amount?>'/>																						
																								<a href='<?=make_admin_url('fee','insert','insert')?>' class="btn red" ><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
																							<input class="btn green check_valid" type="submit" value="Add Fee" name="submit_fee" >
																						</div>																				
																				<? endif;?>		
																		</form>		
																		</div>
																	</div>
															</div>
														</div>												
												<div class="clearfix"></div>
												
												
												
												<!-- ******************************************* -->
													<div class="row-fluid">
															<div class="span12">
																<!-- BEGIN EXAMPLE TABLE PORTLET-->
																<div class="portlet box green">
																	<div class="portlet-title">
																		<div class="caption"><i class="icon-list"></i>Previous Fee List</div>
																		<div class="tools"></div>
																	</div>
																	<div class="portlet-body">
																	<table class="table table-striped table-bordered table-hover" id="sample_2">
																			<thead>
																				<tr>
																					<th class="hidden-480" >Sr. No.</th>
																					<th class="hidden-480" style='text-align:center;'>On Date</th>
																					<th class="hidden-480" style='vertical-align:top;text-align:center'>Late Fee</th>
																					<th class="hidden-480" style='vertical-align:top;text-align:center'>Attendance Fine</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total Amount (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total Paid (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Balance (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Action</th>
																				</tr>
																			</thead>
																			<? if(!empty($Fee_list)):?>
																			<tbody>
																			<?php $sr=1;foreach($Fee_list as $sk=>$rec): $checked='';?>
																				<tr class="odd gradeX">
																					<td><?=$sr?>.</td>
																					<td style='text-align:center;'><?=$rec->payment_date?></td>
																					<td style='text-align:center;'><?=CURRENCY_SYMBOL.' '.number_format($rec->late_fee,2)?></td>
																					<td style='text-align:center;'><?=CURRENCY_SYMBOL.' '.number_format($rec->absent_fine,2)?></td>
																					<td style='text-align:right;'><?=CURRENCY_SYMBOL.' '.number_format($rec->total_amount,2)?></td>
																					<td style='text-align:right;'><?=CURRENCY_SYMBOL.' '.number_format($rec->amount,2)?></td>
																					<td style='text-align:right;'><span class='A'><strong><?=CURRENCY_SYMBOL.' '.number_format($rec->balance,2)?></strong></span></td>
																					<td style='text-align:right;'>
																					<a class="btn blue icn-only tooltips mini" href="<?php echo make_admin_url('fee', 'update', 'update', 'id='.$rec->id.'&student='.$student.'&session_id='.$session_id.'&month='.$month.'&ct_sect='.$ct_sect.'&type='.$type)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
																					<a class="btn red icn-only tooltips mini" href="<?php echo make_admin_url('fee', 'delete', 'delete', 'id='.$rec->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
																					</td>																					
																				</tr>
																			<? $sr++;
																				endforeach;?>									
																			</tbody>
																		<? endif;?>
																		</table>
																
																	<div class="clearfix"></div>
																	
																	</div>
																</div>
																						
																<!-- END EXAMPLE TABLE PORTLET-->
															</div>
														</div>											
											</div>
											<!-- Forth steps End -->
										</div>
										

									</div>
								
							</div>
						</div>
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

<script type="text/javascript">
	$(".session_filter_for_single").live("change", function(){
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_s").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section','ajax_section&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});
	$("#GetStudents").live("click", function(){ 
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_sect").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#show_students").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_with_st','ajax_section_with_st&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#show_students").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
	 
	$("#select_registration").live("click", function(){	 
			var reg_id=$('#reg_id').val();
			var ct_school=<?=$school->id;?>;
			
			if(reg_id.length > 0){					   
					   $("#dynamic_model_reg").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'reg_id='+reg_id+'&ct_school='+ct_school;
						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_for_reg_find','ajax_section_for_reg_find&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model_reg").html(data);
						$("#dynamic_model").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
</script> 


	<script>
		jQuery(document).ready(function() {    
            $(".check_valid").live("click",function(){
             // Validation engine
                    $(".validation").validationEngine({
                         promptPosition : "topLeft"
                        
                    });
          	});  
			
            $(".remove_field").live("click",function(){            
								var row_id=$(this).attr("field_id");
								var total=$('#make_total').html();
								var ct_val=$('.row-'+row_id).find('td input:text').val();

								$(this).removeClass('icon-remove');
								$(this).removeClass('remove_field');
								$(this).addClass('icon-plus');
								$(this).addClass('add_field');
								$('.row-'+row_id).addClass('removed');
								$('.row-'+row_id).find('td input:text').attr('disabled', true);
								$('.row-'+row_id).find('.head').attr('disabled', true);
								
								var new_val=total-ct_val;
								$('#make_total').html(new_val);	
								$('#amount_val').val(new_val);								
          	}); 
            $(".add_field").live("click",function(){            
								var row_id=$(this).attr("field_id");
								var total=$('#make_total').html();
								var ct_val=$('.row-'+row_id).find('td input:text').val();								
								
								$(this).removeClass('icon-plus');
								$(this).removeClass('add_field');
								$(this).addClass('icon-remove');
								$(this).addClass('remove_field');
								$('.row-'+row_id).removeClass('removed');
								$('.row-'+row_id).find('td input:text').removeAttr('disabled');
								$('.row-'+row_id).find('.head').removeAttr('disabled');
								
								var new_val=parseInt(ct_val) + parseInt(total);
								$('#make_total').html(new_val);	
								$('#amount_val').val(new_val);									
          	}); 			
				
		});
		
	</script>