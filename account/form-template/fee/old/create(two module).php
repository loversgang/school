<?
if($setting_module=='1'):

################################################################################################
################################################################################################
############################# 							       	  ##############################
#############################         First Fee Module            ##############################
############################# 		                  			  ##############################
################################################################################################
################################################################################################

 ?>




  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Students Fee
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Add Student Fee
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>

		
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
			<? if($type!='select'):?>
				<div class="tiles pull-right">
							<div class="tile bg-green <?php echo ($section=='update')?'selected':''?>">
								<a href="<?php echo make_admin_url('fee', 'insert', 'insert');?>">
									<div class="corner"></div>
									<div class="tile-body"><i class="icon-arrow-left"></i></div>
									<div class="tile-object"><div class="name"> Change Session</div></div>
								</a> 
							</div>	

				</div>   		
			<? endif;?>
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
            
			<div class="row-fluid">
					<div class="span12">
						<div id="form_wizard_1" class="portlet box blue">
							<div class="portlet-title">
							<div class="caption"><i class="icon-reorder"></i> Student Fee Wizard - <span class="step-title">Step 1 of 3</span></div>
							</div>
							
							<div class="portlet-body form">
								<div class="form-wizard">						
										<!-- Main steps -->
										<div class="navbar steps">
											<div class="navbar-inner">
												<ul class="row-fluid nav nav-pills">
													<li class="span4 <?=($type=='select')?'active':'';?>">
														<a class="step <?=($type=='select')?'active':'';?>">
														<span class="number">1</span>
														<span class="desc"><i class="icon-ok"></i> Select Student </span>   
														</a>
													</li>
													
													<li class="span4 <?=($type=='list')?'active':'';?>">
														<a class="step <?=($type=='list')?'active':'';?>">
														<span class="number">2</span>
														<span class="desc"><i class="icon-ok"></i> Section Students</span>   
														</a>
													</li>
													
													<li class="span4 <?=($type=='fee')?'active':'';?>">
														<a class="step <?=($type=='fee')?'active':'';?>">
														<span class="number">3</span>
														<span class="desc"><i class="icon-ok"></i> Add Student Fee</span>   
														</a>
													</li>													
												</ul>
											</div>
										</div>
										<!-- Main End -->
										<div class="progress progress-success progress-striped" id="bar">
											<div class="bar" style="width: <?=$per?>"></div>
										</div>
										
										
										<div class="tab-content">
											<!-- First steps -->
											<div id="tab1" class="tab-pane <?=($type=='select')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='list'/>											
												
												<div class="row-fluid">
													<div class="span12">		
														<h3 class="block">Select Session & Section</h3>
															<div class="alert alert-block alert-success">
																<div class="row-fluid">	
																	<div class="span5">														
																		<div class="control-group">
																		<label class="control-label">Select Session</label>
																		<div class="controls">
																		<select class="select2_category session_filter_for_single span9" data-placeholder="Select Session Students" name='session_id' id='session_id'>
																		<option value="">Select Session</option>
																			<?php $session_sr=1;while($object=$QuerySession->GetObjectFromRecord()): ?>
																				<option value='<?=$object->id?>' <? if($object->id==$session_id){ echo 'selected'; }?> ><?=ucfirst($object->title)?></option>
																			<? $session_sr++; endwhile;?>
																		</select>
																		</div>
																		</div>
																	</div>																		
																	<div class="span7" id='dynamic_model'>
																			<input type='hidden' class='section_filter' value='A' id='ct_s'/>
																	</div>		
																</div>
																<div class="row-fluid">	
																	<span class="help-block"> Note:- Select any previous payment period if you want to edit the previous fee heads to adjust previous fee payments.</span>
																</div>
															</div>	
															<div class="clearfix"></div>
								
													<h3 class="block" style='text-align:center;'>OR</h3>
													<div class="alert alert-block alert-success">
														<div class="row-fluid">
																						
																<div class="control-group span5">
																	<label class="control-label">Reg ID</label>
																	<div class="controls">
																		<input type="text" id='reg_id' style='background:#fff;' placeholder='Type Registration ID' class="span9 m-wrap" />
																		<div class='span2 btn blue' id='select_registration' style='float: right; margin-left: 0px; margin-right: 20px;'>Go</div>
																	</div>																	
																</div>
																							
														</div>														
														<div class="clearfix"></div>
													</div>
													<div id='dynamic_model_reg'>	</div>									
															
													</form>	
													</div>	
												</div>
																						
												
											</div>
											<!-- First steps End -->
											
											
									
											<!-- Second steps -->
											<div id="tab2" class="tab-pane <?=($type=='list')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='fee'/>
												<input type='hidden' name='session_id' value='<?=$session_id?>'/>
												<input type='hidden' name='ct_sect' value='<?=$ct_sect?>'/>
												<input type='hidden' name='interval_id' value='<?=$interval_id?>'/>													
												<h3 id="title_show" class="block alert alert-info"><?=$session->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?> </h3>
													
													<div class="span12" style='margin-left:0px;'>				
														<div class="controls">
																	<table id="<? if(!empty($interval_id) && empty($student_id)): echo "sample_2"; endif; ?>" class="table table-striped table-bordered table-hover">
																		<thead>
																			<tr>
																				<th class="hidden-480">Reg ID</th>
																				<th class="hidden-480" >Roll No.</th>
																				<th class="hidden-480" >Name</th>
																				<th class="hidden-480" >Father Name</th>																					
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total Paid (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'></th>				
																			</tr>
																		</thead>
																		<tbody>
																		<? if(!empty($records)):?>
																		<?php $sr=1;foreach($records as $sk=>$stu): $checked=''; 
																					#Get interval detail
																					$SessIntOb= new studentSessionInterval();
																					$student_interval_detail=$SessIntOb->getDetail($session_id,$ct_sect,$stu->id,$interval_id); 
																					?>
																				<tr class="odd gradeX">
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->reg_id?></td>
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->roll_no?></td>
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->father_name)?></td>
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:right;'><? if(is_object($student_interval_detail)): echo CURRENCY_SYMBOL." ".number_format($student_interval_detail->paid,2); endif;?></td>
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:right;'>
																					<a href='<?=make_admin_url('fee','insert','insert&type=fee&session_id='.$session_id.'&ct_sect='.$ct_sect.'&interval_id='.$interval_id.'&student_id='.$stu->id)?>' class="btn green mini">View Fee Detail</a>
																				</tr>
																		<? $sr++; endforeach;
																		   else:	?>
																		<tr class="odd gradeX"><td></td><td colspan='7'>Sorry, No Record Found..!</td></tr>
																	<? endif;?>
																		</tbody>
																	</table>
														</div>
													</div>
													<div class="clearfix"></div>
									
													<div class="form-actions clearfix" >
															<a href="<?php echo make_admin_url('fee', 'insert', 'insert');?>" class="btn red" name="cancel" > Cancel</a>
															<input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
													</div>
												</form>	
											</div>
											<!-- Second steps End -->									
											
											
											<!-- Third steps -->
											<div id="tab3" class="tab-pane <?=($type=='fee')?'active':'';?>">
												<h3 id="title_show" class="block alert alert-info"><?=$session->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?> <font class="green_color"> &gt; </font> <?=ucfirst($student_info->first_name).' '.$student_info->last_name?>  </h3>
														<div class="row-fluid">
															<div class="span12">
																	<div class="portlet box blue">
																		<div class="portlet-title"> 
																			<div class="caption">Fee Information : </div>
																			<div class="tools">
																				<div class="actions"><?=date('d M, Y',strtotime($from))?> to <?=date('d M, Y',strtotime($to))?></div>
																			</div>
																		</div>
																		<div class="portlet-body">
																		 <form class="form-horizontal validation" action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" id="validation">
																			
																			<div class="clearfix"></div>
																			<!-- More Entry Case -->
																				<div class="row-fluid">	
																					<!-- First Entry of the months -->	
																					<div class="span6">
																						<!-- Student Info -->
																						<div class="row-fluid">	
																						<div class="alert alert-block alert-success fade in">	
																								<div class="row-fluid">	
																									<div class='span4' style='line-height: 26px;'><strong>Student Granted: </strong></div>
																									<div class='span6' style='line-height: 26px;'><?=$student_info->concession?></div>												
																								</div>	
																								<? if(!empty($student_info->concession_type)):?>
																									<div class="row-fluid">	
																										<div class='span4' style='line-height: 26px;'><strong>Concession: </strong></div>
																										<div class='span6' style='line-height: 26px;'><?=CURRENCY_SYMBOL.' '.number_format($student_info->concession_type,2)?></div>												
																									</div>	
																								<? endif;?>	
																						</div>
																						</div>
																						<div class="alert alert-block alert-success fade in" >																							
																								<div class="row-fluid">	
																									<div class='span4'>Session: </div>
																									<div class='span6'><?=$session->title?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Section: </div>
																									<div class='span6'><?=$ct_sect?></div>												
																								</div>																									
																								<div class="row-fluid">	
																									<div class='span4'>Reg Id: </div>
																									<div class='span6'><?=$student_info->reg_id?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Name: </div>
																									<div class='span6'><?=ucfirst($student_info->first_name).' '.$student_info->last_name?></div>												
																								</div>	
																								<div class="row-fluid">	
																									<div class='span4'>Roll No.: </div>
																									<div class='span6'><?=$student_info->roll_no?></div>												
																								</div>

																								
																								<? if(!empty($student_interval_detail->vehicle_fee)):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Vehicle Fee: </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($student_interval_detail->vehicle_fee,2)?></div>												
																									</div>																									
																								<? endif;?>	

																								<? if($student_interval_detail->absent_fine>0):?>
																								<div class="row-fluid" >	
																									<div class='span4' id='color_red'>Attendance Fine: </div>
																									<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($student_interval_detail->absent_fine,2)?></div>
																								</div>	
																								<? endif;?>		
																								
																								<? if(!empty($student_interval_detail->late_fee)):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Late Fee Fine: </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($student_interval_detail->late_fee,2)?></div>												
																									</div>																									
																								<? endif;?>
																								
																								<? if(isset($Absent_last) && !empty($Absent_last)):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Attendance Fine: (<?=date('M,Y',strtotime($student_interval_detail->from_date)).' - '.date('M,Y',strtotime($student_interval_detail->to_date))?>) </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($Absent_last*$session->absent_fine,2)?></div>												
																									</div>	
																									<div class="row-fluid" >
																										<strong>Note:-</strong> Please add this fine manually.
																									</div>	
																								<? endif;?>																								
																								
																								
																								<? if(!empty($total_paid)):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Total Paid: </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($total_paid,2)?></div>												
																									</div>																									
																								<? endif;?>																								
																						</div>
																						<!-- Student Info End-->
																					</div>
																					
																					<div class='span6' id='right_calculation'>
																						<div class="portlet-title green">
																							<div class="caption">Calculate Student Fee</div>
																							<div class="tools">
																									<div class="actions">
																										<?
																										#only Update in last interval
																										if($interval_id==$last_int_id):?>
																											<a class="btn success mini" href="<?=make_admin_url('session','section','section&ct_session='.$session_id.'&id='.$student_info->rel_id.'&ct_section='.$student_info->section)?>" title='Click here to edit student fee'><i class="icon-pencil"></i> Edit</a>
																										<? endif; ?>
																									</div>
																							</div>
																						</div>
																						<table class="table table-striped table-bordered table-advance table-hover" id='fee_table'>
																						<thead>
																						<tr><th style='width:50%;'>Fee Head Name</th><th style='width:50%;text-align:right;'>Amount (<?=CURRENCY_SYMBOL?>)</th>
																						</tr></thead>
																						<tbody>
																								<? $t_am='0'; $new_added=0;
																									$heads=html_entity_decode($student_interval_detail->heads); 
																									$heads=unserialize($heads);
																										
																									if(!empty($heads)): $st='0';																
																												/* Put hete pending fee  */
																												if(!empty($pending_fee)):
																													echo "<tr><td id='black_font_fee'>Previous Balance</td><td id='black_font_fee' style='text-align:right'>".CURRENCY_SYMBOL.number_format($pending_fee,2)."</td></tr>";
																													echo "<input type='hidden' name='fee_record[Previous Balance]' value='".$pending_fee."'/>";
																													$t_am=$t_am+$pending_fee;																									
																												endif;
 																									
																												foreach($heads as $p_k=>$p_v):																												
																														echo "<tr><td>".$p_v['title']."&nbsp;(".ucfirst($p_v['type']).")</td><td style='text-align:right'>".CURRENCY_SYMBOL.number_format($p_v['amount'],2)."</td></tr>";
																														echo "<input type='hidden' name='fee_record[".$p_v['title']." (".ucfirst($p_v['type']).")]' value='".($p_v['amount'])."'/>";
																														$t_am=$t_am+$p_v['amount'];																													
																												endforeach;

																												#add Vehicle Fee if available 
																												if(!empty($student_interval_detail->vehicle_fee)):
																													echo "<tr><td id='black_font_fee'>Vehicle Fee</td><td style='text-align:right' id='black_font_fee'>".CURRENCY_SYMBOL.number_format($student_interval_detail->vehicle_fee,2)."</td></tr>";
																													echo "<input type='hidden' name='vehicle_fee' value='".($student_interval_detail->vehicle_fee)."'/>";
																													$t_am=$t_am+$student_interval_detail->vehicle_fee;	
																												else:
																													echo "<input type='hidden' value='0' id='vehicle_fine_charge'/>";
																												endif;	
																											
																												#Session Total  
																												echo "<tr><td class='total'>Sub Total</td><td class='total' colspan='2' style='text-align:right;'>".CURRENCY_SYMBOL." <span class='make_total_new'>".number_format($t_am,2)."</span></td></tr>";
																												$AmountTotal=$t_am;
						
																												#add Attendance Fine if available 
																												if(!empty($student_interval_detail->absent_fine)):
																														$AmountTotal=($AmountTotal+($student_interval_detail->absent_fine));
																														echo "<tr><td>Attendance Fine</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;+&nbsp;<input type='text' id='absent_fine_charge' class='span5 total_change' add='0' p_val='".($student_interval_detail->absent_fine)."' name='absent_fine' value='".$student_interval_detail->absent_fine."' readonly/> <input type='hidden' value='".$Absent."' name='absent_days'/></td></tr>";
																												else:
																													echo "<input type='hidden' value='0' id='absent_fine_charge'/>";	
																												endif;	
																																																						
																												#Add Late Fee Fine if available 
																												if(!empty($student_interval_detail->late_fee)):
																													$AmountTotal=($AmountTotal+$student_interval_detail->late_fee);
																													echo "<tr><td id='black_font_fee'>Late Fee Fine</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;+&nbsp;<strong><input type='text' id='late_fee_charge' class='span5 total_change' name='late_fee' value='".$student_interval_detail->late_fee."' readonly/> </td></tr>";
																												else:
																													echo "<input type='hidden' value='0' id='late_fee_charge'/>";
																												endif;	
																											
																												
																											
																												# Less concession 
																												if(!empty($student_interval_detail->concession)):
																													echo "<tr><td id='black_font_fee'>Concession (".$student_info->concession.")</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;-&nbsp;<strong><input type='text' id='consession_fee' class='span5 total_change' name='concession' value='".$student_info->concession_type."' readonly/> </td></tr>";
																													echo "<input type='hidden' value='".$student_interval_detail->concession."' name='concession'/>";
																													$AmountTotal=$AmountTotal-$student_interval_detail->concession;
																												else:
																													echo "<input type='hidden' value='0' id='consession_fee'/>";
																												endif;
																												
																												# Less Total Paid 
																												if(!empty($total_paid)):
																													$AmountTotal=$AmountTotal-$total_paid;																												
																												endif;
																												echo "<input type='hidden' value='".$AmountTotal."' id='session_total' name='session_total'/>";
																												
																												#here is the extra field
																												if($student_interval_detail->other_fee_type=="-"):
																													$AmountTotal=($AmountTotal-$student_interval_detail->other_fee);	
																												elseif($student_interval_detail->other_fee_type=="+"):
																													$AmountTotal=($AmountTotal+$student_interval_detail->other_fee);	
																												endif;																								    
																												
																												
																												#here is the extra field
																												echo "<tr><td id='black_font_fee'>
																													<input type='text' class='span8 total_change extra_fee_name' name='other_fee_name' placeholder='Other fee Name' value='".$student_interval_detail->other_fee_name."'/>
																													";
																													#only Update in last interval
																													//if($interval_id==$last_int_id): 
																														echo "<div class='btn green span3' style='float:right;' id='new_value_save'>Save</div>";
																													///endif;
																													
																													echo "</td><td colspan='2' style='text-align:right'>
																														<select name='other_fee_type' class='span3 total_change' id='extra_type'>";?>
																															<option value='-' <?=$student_interval_detail->other_fee_type=='-'?"selected":""?>><strong class='large_text'>-&nbsp;&nbsp;<strong></option>
																															<option value='+' <?=$student_interval_detail->other_fee_type=='+'?"selected":""?>><strong class='large_text'>+&nbsp;&nbsp;<strong></option>
																														<?
																														
																														echo "</select>	
																												<input type='text' id='extra' class='span5 total_change validate[custom[onlyNumberSp]]' name='other_fee' placeholder='Other fee' value='".$student_interval_detail->other_fee."'/> </td></tr>";
																											
																											# Show Less Total Paid 
																												if(!empty($total_paid)):
																													echo "<tr><td class='total_paid'>Total Paid</td><td colspan='2' style='text-align:right'><strong class='total_paid'>&nbsp;-&nbsp;</strong>".CURRENCY_SYMBOL." <span class='total_paid'>".number_format($total_paid,2)."</td></tr>";
																												endif;																		
																											
																											# Total Amount 
																											echo "<tr><td class='total'>Total</td>
																											<td class='total' colspan='2' style='text-align:right'>".CURRENCY_SYMBOL." <span class='AmountTotal'>".number_format($AmountTotal,2)."</span>
																											</td></tr>";
																									else:
																											echo "<tr><td colspan='2'>Sorry, No Record Found..!</td></tr>";														
																									endif;?>
																						</tbody>
																						</table>
																					</div>
																					<input type='hidden' id="paid" value='0'/>
																				</div>	
																				<br/>
																					<!-- Paid only last current fee term -->
																					<? //if($interval_id==$last_int_id): ?>
																					<div class=" portlet box blue">
																						<div class="portlet-title"><div class="caption">Add Fee</div></div>
																						<div class="portlet-body">
																						<div class="span6">	
																							<div class="control-group">
																									<label for="payment_date" class="control-label">Date <span class="required">*</span></label>
																									<div class="controls">
																									  <input type="text" class="span10 m-wrap validate[required] new_format" id="payment_date" value="<?=date('Y-m-d')?>" name="payment_date" >
																									</div>
																							</div> 
																							<div class="control-group">
																									<label for="total_amount" class="control-label">Amount Paying  <span class="required">*</span></label>
																									<div class="controls">
																									  <input type="text" class="span10 m-wrap validate[required]" id="total_amount"  value="" name="total_amount">
																									</div>
																							</div>
																						</div>		
																						<div class="span6">		
																							<div class="control-group">
																										<label for="remarks" class="control-label">Remarks</label>
																										<div class="controls">
																										  <textarea name="remarks" class="span10 m-wrap"></textarea>
																										</div>
																							</div>																						
																						</div>
																						<div class="clearfix"></div>
																						</div>
																					</div>

																				
																					<div class="form-actions clearfix">
																						<input type='hidden' name='section' value='<?=$ct_sect?>'/>
																						<input type='hidden' name='student_id' value='<?=$student_id?>'/>
																						<input type='hidden' name='session_id' value='<?=$session_id?>'/>
																						<input type='hidden' name='interval_id' value='<?=$interval_id?>'/>	
																						<a href='<?=make_admin_url('fee','insert','insert')?>' class="btn red" ><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
																						<input class="btn green check_valid" type="submit" value="Add Fee" name="submit_fee" >
																					</div>
																					<? //endif;?>
																				</div>																					
																		</form>		
																		</div>
																	</div>
															</div>
																									
												<div class="clearfix"></div>
												
												
												
												<!-- ******************************************* -->
													<div class="row-fluid">
															<div class="span12">
																<!-- BEGIN EXAMPLE TABLE PORTLET-->
																<div class="portlet box green">
																	<div class="portlet-title">
																		<div class="caption"><i class="icon-list"></i>Previous Paid Fee List:</div>
																		<div class="tools"></div>
																	</div>
																	<div class="portlet-body">
																	<table class="table table-striped table-bordered table-hover" id="sample_2">
																			<thead>
																				<tr>
																					<th class="hidden-480" >Sr. No.</th>
																					<th class="hidden-480" style='text-align:center;'>Payment Date</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Amount Paid (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Action</th>
																				</tr>
																			</thead>
																			<? if(!empty($Fee_list)): ?>
	
																			<tbody>
																			<?php $sr=1;foreach($Fee_list as $sk=>$rec): $checked='';?>
																				<tr class="odd gradeX">
																					<td><?=$sr?>.</td>
																					<td style='text-align:center;'><?=date('d M, Y',strtotime($rec->payment_date))?></td>
																					<td style='text-align:right;'><?=CURRENCY_SYMBOL.' '.number_format($rec->total_amount,2)?></td>
																					<td style='text-align:right;'>
																					<? if($interval_id==$last_int_id): ?>
																					<a class="btn blue icn-only tooltips mini" href="<?php echo make_admin_url('fee', 'update', 'update', 'id='.$rec->id.'&student_id='.$student_id.'&interval_id='.$interval_id.'&session_id='.$session_id.'&ct_sect='.$ct_sect.'&type='.$type)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
																					<a class="btn red icn-only tooltips mini" href="<?php echo make_admin_url('fee', 'delete', 'delete', 'id='.$rec->id.'&delete=1&student_id='.$student_id.'&interval_id='.$interval_id.'&session_id='.$session_id.'&ct_sect='.$ct_sect.'&type='.$type)?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
																					<? endif;?>
																					<!--
																					<? if(is_object($slip)):?>
																						&nbsp;&nbsp; <a class="btn black icn-only tooltips mini print_start" rec_id='<?=$rec->id?>' doc_id='<?=$slip->id?>' title="click here to print receipt">Print</a>
																					<? endif;?>
																					-->
																					</td>
																				</tr>
																				<?

																					$sr++;
																				endforeach;?>									
																			</tbody>
																		<? endif;?>
																		</table>
																
																	<div class="clearfix"></div>
																	
																	</div>
																</div>
																						
																<!-- END EXAMPLE TABLE PORTLET-->
															</div>
														</div>	
												</div>					
											</div>
											<!-- Forth steps End -->
										</div>
										

									</div>
								
							</div>
						</div>
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

<script type="text/javascript">
	$(".session_filter_for_single").live("change", function(){
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_s").val();
			
			if(session_id.length > 0){
					   $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'session_id='+session_id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section','ajax_section&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});
	$("#GetStudents").live("click", function(){ 
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_sect").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#show_students").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_with_st','ajax_section_with_st&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#show_students").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
	 
	$("#select_registration").live("click", function(){	 
			var reg_id=$('#reg_id').val();
			var ct_school=<?=$school->id;?>;
			
			if(reg_id.length > 0){					   
					   $("#dynamic_model_reg").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'reg_id='+reg_id+'&ct_school='+ct_school;
						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_for_reg_find','ajax_section_for_reg_find&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model_reg").html(data);
						$("#dynamic_model").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
</script> 

	
	 <script type="text/javascript">     
        jQuery(document).ready(function() {
            $(".check_valid").live("click",function(){
             // Validation engine
                    $(".validation").validationEngine({
                         promptPosition : "topLeft"                        
                    });
          	});
			
		$(".total_change").live("change",function(){	
					GetValueForTotal();								
		});	
		});
		
		function GetValueForTotal(){
			var session_total=$("#session_total").val();
			var extra=$("#extra").val();
			var extra_type=$("#extra_type").val();
			if(extra!=='')
			{
			
				if(extra_type=='-'){ 
					session_total=parseInt(session_total)-parseInt(extra);
				}
				if(extra_type=='+'){
					session_total=parseInt(session_total)+parseInt(extra);
				}
			}		
			$('.AmountTotal').html(session_total);	
		}

	$(".print_start").live("click",function(){  
				var id=$(this).attr('rec_id');
				var doc_id=$(this).attr('doc_id');				
				var dataString = 'id='+id+'&doc_id='+doc_id;				
				$.ajax({
				type: "POST",						
				url: "<?=make_admin_url_window('ajax_calling','print','print&temp=fee');?>",
				data: dataString,
				success: function(data, textStatus) {
					var DocumentContainer = document.getElementById(data);
					var WindowObject = window.open('', "PrintWindow", "");
					WindowObject.document.writeln(data);
					WindowObject.document.close();
					WindowObject.focus();
					WindowObject.print();
					WindowObject.close();
				return false;
				}				
				});
	});	
	
	$("#new_value_save").live("click",function(){  
				var session_id='<?=$session_id?>';
				var ct_sect='<?=$ct_sect?>';
				var interval_id='<?=$interval_id?>';
				var student_id='<?=$student_id?>';
				var extra_fee=$('#extra').val();
				var extra_type=$('#extra_type').val();
				var extra_fee_name=$('.extra_fee_name').val();	
				
				if(extra_type=='+'){
					var sign='plus';
				}
				else{
					var sign='minus';
				}	
				
			if(extra_fee.length > 0){	
				$('#extra').css("border","1px solid #CCCCCC");
				$('.extra_fee_name').css("border","1px solid #CCCCCC");					
				if(extra_fee_name.length > 0){	
					$('#extra').css("border","1px solid #CCCCCC");	
				}
				else{
					$('.extra_fee_name').css("border","1px solid #F7023B");	
					return false;
				}
				
				var dataString = 'session_id='+session_id+'&ct_sect='+ct_sect+'&interval_id='+interval_id+'&student_id='+student_id+'&extra_fee='+extra_fee+'&extra_fee_name='+extra_fee_name+'&extra_type='+sign;	
				
				$("#right_calculation").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
										
				$.ajax({
				type: "POST",						
				url: "<?=make_admin_url_window('ajax_calling','extra_fee_add','extra_fee_add&temp=fee');?>",
				data: dataString,
				success: function(data, textStatus) {
					$("#right_calculation").html(data);
					return false;
					}				
					});
				}
			else{			
				$('#extra').css("border","1px solid #F7023B");
				return false;
			}			
	});	

	
 </script>		
 
 
 

<?
################################################################################################
################################################################################################
################################################################################################
################################################################################################
################################################################################################
################################################################################################
############################# 							       	  ##############################
#############################         Second Fee Module           ##############################
############################# 		                  			  ##############################
################################################################################################
################################################################################################
################################################################################################
################################################################################################
################################################################################################


else:
 ?>
 
   <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Students Fee
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Add Student Fee
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>

		
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
			<? if($type!='select'):?>
				<div class="tiles pull-right">
							<div class="tile bg-green <?php echo ($section=='update')?'selected':''?>">
								<a href="<?php echo make_admin_url('fee', 'insert', 'insert');?>">
									<div class="corner"></div>
									<div class="tile-body"><i class="icon-arrow-left"></i></div>
									<div class="tile-object"><div class="name"> Change Session</div></div>
								</a> 
							</div>	

				</div>   		
			<? endif;?>
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
            
			<div class="row-fluid">
					<div class="span12">
						<div id="form_wizard_1" class="portlet box blue">
							<div class="portlet-title">
							<div class="caption"><i class="icon-reorder"></i> Student Fee Wizard - <span class="step-title">Step 1 of 3</span></div>
							</div>
							
							<div class="portlet-body form">
								<div class="form-wizard">						
										<!-- Main steps -->
										<div class="navbar steps">
											<div class="navbar-inner">
												<ul class="row-fluid nav nav-pills">
													<li class="span4 <?=($type=='select')?'active':'';?>">
														<a class="step <?=($type=='select')?'active':'';?>">
														<span class="number">1</span>
														<span class="desc"><i class="icon-ok"></i> Select Student </span>   
														</a>
													</li>
													
													<li class="span4 <?=($type=='list')?'active':'';?>">
														<a class="step <?=($type=='list')?'active':'';?>">
														<span class="number">2</span>
														<span class="desc"><i class="icon-ok"></i> Section Students</span>   
														</a>
													</li>
													
													<li class="span4 <?=($type=='fee')?'active':'';?>">
														<a class="step <?=($type=='fee')?'active':'';?>">
														<span class="number">3</span>
														<span class="desc"><i class="icon-ok"></i> Add Student Fee</span>   
														</a>
													</li>													
												</ul>
											</div>
										</div>
										<!-- Main End -->
										<div class="progress progress-success progress-striped" id="bar">
											<div class="bar" style="width: <?=$per?>"></div>
										</div>
										
										
										<div class="tab-content">
											<!-- First steps -->
											<div id="tab1" class="tab-pane <?=($type=='select')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='list'/>											
												
												<div class="row-fluid">
													<div class="span12">		
														<h3 class="block">Select Session & Section</h3>
															<div class="alert alert-block alert-success">
																<div class="row-fluid">	
																	<div class="span5">														
																		<div class="control-group">
																		<label class="control-label">Select Session</label>
																		<div class="controls">
																		<select class="select2_category session_filter_for_single span9" data-placeholder="Select Session Students" name='session_id' id='session_id'>
																		<option value="">Select Session</option>
																			<?php $session_sr=1;while($object=$QuerySession->GetObjectFromRecord()): ?>
																				<option value='<?=$object->id?>' <? if($object->id==$session_id){ echo 'selected'; }?> ><?=ucfirst($object->title)?></option>
																			<? $session_sr++; endwhile;?>
																		</select>
																		</div>
																		</div>
																	</div>																		
																	<div class="span7" id='dynamic_model'>
																			<input type='hidden' class='section_filter' value='A' id='ct_s'/>
																	</div>		
																</div>
																<div class="row-fluid">	
																	<span class="help-block"> Note:- Select any previous payment period if you want to edit the previous fee heads to adjust previous fee payments.</span>
																</div>
															</div>	
															<div class="clearfix"></div>
								
													<h3 class="block" style='text-align:center;'>OR</h3>
													<div class="alert alert-block alert-success">
														<div class="row-fluid">
																						
																<div class="control-group span5">
																	<label class="control-label">Reg ID</label>
																	<div class="controls">
																		<input type="text" id='reg_id' style='background:#fff;' placeholder='Type Registration ID' class="span9 m-wrap" />
																		<div class='span2 btn blue' id='select_registration' style='float: right; margin-left: 0px; margin-right: 20px;'>Go</div>
																	</div>																	
																</div>
																							
														</div>														
														<div class="clearfix"></div>
													</div>
													<div id='dynamic_model_reg'>	</div>									
															
													</form>	
													</div>	
												</div>
																						
												
											</div>
											<!-- First steps End -->
											
											
									
											<!-- Second steps -->
											<div id="tab2" class="tab-pane <?=($type=='list')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='fee'/>
												<input type='hidden' name='session_id' value='<?=$session_id?>'/>
												<input type='hidden' name='ct_sect' value='<?=$ct_sect?>'/>
												<input type='hidden' name='interval_id' value='<?=$interval_id?>'/>													
												<h3 id="title_show" class="block alert alert-info"><?=$session->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?> </h3>
													
													<div class="span12" style='margin-left:0px;'>				
														<div class="controls">
																	<table id="<? if(!empty($interval_id) && empty($student_id)): echo "sample_2"; endif; ?>" class="table table-striped table-bordered table-hover">
																		<thead>
																			<tr>
																				<th class="hidden-480">Reg ID</th>
																				<th class="hidden-480" >Roll No.</th>
																				<th class="hidden-480" >Name</th>
																				<th class="hidden-480" >Father Name</th>																					
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total Paid (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'></th>				
																			</tr>
																		</thead>
																		<tbody>
																		<? if(!empty($records)):?>
																		<?php $sr=1;foreach($records as $sk=>$stu): $checked=''; 
																					#Get interval detail
																					$SessIntOb= new studentSessionInterval();
																					$student_interval_detail=$SessIntOb->getDetail($session_id,$ct_sect,$stu->id,$interval_id); 
																					?>
																				<tr class="odd gradeX">
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->reg_id?></td>
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->roll_no?></td>
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->father_name)?></td>
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:right;'><? if(is_object($student_interval_detail)): echo CURRENCY_SYMBOL." ".number_format($student_interval_detail->paid,2); endif;?></td>
																					<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:right;'>
																					<a href='<?=make_admin_url('fee','insert','insert&type=fee&session_id='.$session_id.'&ct_sect='.$ct_sect.'&interval_id='.$interval_id.'&student_id='.$stu->id)?>' class="btn green mini">View Fee Detail</a>
																				</tr>
																		<? $sr++; endforeach;
																		   else:	?>
																		<tr class="odd gradeX"><td></td><td colspan='7'>Sorry, No Record Found..!</td></tr>
																	<? endif;?>
																		</tbody>
																	</table>
														</div>
													</div>
													<div class="clearfix"></div>
									
													<div class="form-actions clearfix" >
															<a href="<?php echo make_admin_url('fee', 'insert', 'insert');?>" class="btn red" name="cancel" > Cancel</a>
															<input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
													</div>
												</form>	
											</div>
											<!-- Second steps End -->
											
											<!-- Third steps -->
											<div id="tab3" class="tab-pane <?=($type=='fee')?'active':'';?>">
												<h3 id="title_show" class="block alert alert-info"><?=$session->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?> <font class="green_color"> &gt; </font> <?=ucfirst($student_info->first_name).' '.$student_info->last_name?>  </h3>
														<div class="row-fluid">
															<div class="span12">
																	<div class="portlet box blue">
																		<div class="portlet-title"> 
																			<div class="caption">Fee Information : </div>
																		</div>
																		<div class="portlet-body">
																		 <form class="form-horizontal validation" action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" id="validation">
																			
																			<div class="clearfix"></div>
																			<!-- More Entry Case -->
																				<div class="row-fluid">	
																					<!-- First Entry of the months -->	
																					<div class="span12">

																					</div>
																				</div>	
																				
																				<div class="row-fluid" >
																					<div class='span9' id='right_calculation'>
																						<!-- Student Info -->

																						<div class="alert alert-block alert-success fade in" >																							
																								<div class="row-fluid">
																									<div class='span3'>																								
																										<div class=''><strong>Receipt No :</strong> </div>
																										<div><input type='text' class='span10' disabled value='<?=$ReceiptNo?>'/></div>
																									</div>	
																									
																									<div class='span3'>																								
																										<div><strong>Session :</strong> </div>
																										<div><input type='text' class='span10' disabled value='<?=$session->title?>'/></div>
																									</div>	
																									
																									<div class='span3'>																								
																										<div class=''><strong>Section :</strong> </div>
																										<div><input type='text' class='span10' disabled value='<?=$ct_sect?>'/></div>
																									</div>	
																									<div class='span3'>																								
																										<div class=''><strong><strong>Student Granted: </strong></strong> </div>
																										<div><input type='text' class='span10' disabled value='<? if(!empty($student_info->concession)): echo $student_info->concession; else: echo "None"; endif;?>'/></div>																										
																									</div>																									
																								</div>																								
																								
																								<div style='clear:both;height:10px'></div>	
																								<div class="row-fluid">
																									<div class='span3'>																								
																										<div class=''><strong><strong>Student Name : </strong></strong> </div>
																										<div><input type='text' class='span10' disabled value='<?=ucfirst($student_info->first_name).' '.$student_info->last_name?>'/></div>
																									</div>
																									
																									<div class='span3'>																								
																										<div class=''><strong>Roll No :</strong> </div>
																										<div><input type='text' class='span10' disabled value='<?=$student_info->roll_no?>'/></div>
																									</div>		
										
																									<div class='span3'>																								
																										<div class=''><strong>Reg ID :</strong> </div>
																										<div><input type='text' class='span10' disabled value='<?=$student_info->reg_id?>'/></div>
																									</div>																									
																								</div>
																								<? if(isset($Absent_last) && !empty($Absent_last)):?>
																									<div style='clear:both;height:10px'></div>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Attendance Fine: (<?=date('M,Y',strtotime($student_interval_detail->from_date)).' - '.date('M,Y',strtotime($student_interval_detail->to_date))?>) </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($Absent_last*$session->absent_fine,2)?></div>												
																									</div>	
																									<div class="row-fluid" >
																										<strong>Note:-</strong> Please add this fine manually.
																									</div>	
																								<? endif;?>																									
																								
																								<div style='clear:both;'></div>	
																						</div>
																						<!-- Student Info End-->
																					
																						<table class="table table-striped table-bordered table-advance table-hover multi_month_table" id='fee_table'>
																						<thead>
																						<tr><th>Fee</th>
																							<? if(!empty($interval_values)): 
																								foreach($interval_values as $iv_key=>$iv_val):?>
																									<th> <?
																										if(getTotalDays($iv_val->from_date,$iv_val->to_date)<='31'):
																											echo date('M-y',strtotime($iv_val->from_date));
																										else:
																											echo date('M',strtotime($iv_val->from_date))."  to  ".date('M,y',strtotime($iv_val->to_date));
																										endif;					
																										?>
																									</th>
																							<?	endforeach; 
																							endif;?>
																							<th style='text-align:right;'>Total &nbsp;</th>
																						</tr></thead>
																						<tbody>
																							<? 
																								foreach($int_heads as $p_key=>$p_val): ?>
																								<tr><td><?=$p_val->name?></td>
																									<? $c_total='0'; 
																										$total_amt=array();
																										$paid_amt=array();
																										if(!empty($interval_values)):
																										foreach($interval_values as $iv_key=>$iv_val):?>	
																											<td><?  //interval_id
																												$heads1=html_entity_decode($iv_val->heads); 
																												$head1=unserialize($heads1);
																												
																												if(!empty($head1)): $st='0';
																													foreach($head1 as $p_k=>$p_v): //echo "<pre>"; print_r($iv_val); exit; 
																														if($p_v['title']==$p_val->name):
																																echo number_format($p_v['amount'],2);
																																$c_total=$c_total+$p_v['amount'];
																																	if($iv_val->interval_position!='1'):
																																	//echo '===';
																																	//echo array_sum($total_amt)-array_sum($paid_amt);
																																		//echo "per===".array_sum($total_amt)-array_sum($paid_amt); 
																																		//$total_amt[]=$iv_val->total;
																																		//$paid_amt[]=$iv_val->paid;
																																	endif;
																																break;
																														endif;
																													endforeach;
																												endif;?>
																											</td>
																											
																									<?  endforeach;
																									   endif;?>
																									   <td style='text-align:right;'><?=CURRENCY_SYMBOL.number_format($c_total,2)?></td>
																								</tr>																						
																							<? endforeach;?>
																								<tr><td>Vehicle Fee</td>
																												<? $c_total='0';
																												if(!empty($interval_values)): 
																													foreach($interval_values as $iv_key=>$iv_val): 
																														if(!empty($iv_val->vehicle_fee)):
																															echo "<td>".number_format($iv_val->vehicle_fee,2)."</td>";
																															$c_total=$c_total+$iv_val->vehicle_fee;
																														else: 
																															echo "<td></td>";
																														endif;
																													endforeach;
																												endif;?>																								
																										<td style='text-align:right;'><?=CURRENCY_SYMBOL.number_format($c_total,2)?></td>
																								</tr>
																								<tr><td>Late Fee</td>
																												<? $c_total='0';
																												if(!empty($interval_values)): 
																													foreach($interval_values as $iv_key=>$iv_val): 
																														if(!empty($iv_val->late_fee)):
																															echo "<td>".number_format($iv_val->late_fee,2)."</td>";
																															$c_total=$c_total+$iv_val->late_fee;
																														else: 
																															echo "<td></td>";
																														endif;
																													endforeach;
																												endif;?>																								
																										<td style='text-align:right;'><?=CURRENCY_SYMBOL.number_format($c_total,2)?></td>
																								</tr>
																								<tr><td>Absent Fine</td>
																												<? $c_total='0';
																												if(!empty($interval_values)): 
																													foreach($interval_values as $iv_key=>$iv_val): 
																														if(!empty($iv_val->absent_fine)):
																															echo "<td>".CURRENCY_SYMBOL.number_format($iv_val->absent_fine,2)."</td>";
																															$c_total=$c_total+$iv_val->absent_fine;
																														else: 
																															echo "<td></td>";
																														endif;
																													endforeach;
																												endif;?>																								
																										<td style='text-align:right;'><?=CURRENCY_SYMBOL.number_format($c_total,2)?></td>																								
																								</tr>
																								<tr><td>Concession</td>
																												<? $c_total='0';
																												if(!empty($interval_values)): 
																													foreach($interval_values as $iv_key=>$iv_val): 
																														if(!empty($iv_val->concession)):
																															echo "<td>".CURRENCY_SYMBOL.number_format($iv_val->concession,2)."</td>";
																															$c_total=$c_total+$iv_val->concession;
																														else: 
																															echo "<td></td>";
																														endif;
																													endforeach;
																												endif;?>																								
																										<td style='text-align:right;'><?=CURRENCY_SYMBOL.number_format($c_total,2)?></td>																								
																								</tr>																								
																								<tr><td>Other Fee</td>
																												<? $c_total='0';
																												if(!empty($interval_values)): 
																													foreach($interval_values as $iv_key=>$iv_val): 
																														if(!empty($iv_val->other_fee)):
																															echo "<td>".$iv_val->other_fee_type.number_format($iv_val->other_fee,2)."</td>";
																															$c_total=$c_total+$iv_val->other_fee;
																														else: 
																															echo "<td></td>";
																														endif;
																													endforeach;
																												endif;?>																								
																										<td style='text-align:right;'><?=CURRENCY_SYMBOL.number_format($c_total,2)?></td>																								
																								</tr>
																								
																								<? if(!empty($interval_values)): ?>
																									<tr height='20'><tr>
																									<td><strong>Total Fee</strong></td>
																									<? 	$c_total='0';
																										foreach($interval_values as $iv_key=>$iv_val): ?>
																										<td><strong><?=number_format($iv_val->total,2)?></strong>  <? $c_total=$c_total+$iv_val->total;?></td>
																									<? endforeach; ?>
																									<td style='text-align:right;'><strong><?=CURRENCY_SYMBOL.number_format($c_total,2)?></strong></td>
																									</tr>
																									<tr><td>Paid</td>
																									<? $c_total='0';
																									   foreach($interval_values as $iv_key=>$iv_val): ?>
																										<td><?=number_format($iv_val->paid,2)?><? $c_total=$c_total+$iv_val->paid;?></td>
																									<? endforeach; ?>
																									<td style='text-align:right;'><?=CURRENCY_SYMBOL.number_format($c_total,2)?></td>
																									</tr>	
																									<tr><td><strong>Balance</strong></td>
																									<? $c_total='0'; 
																									   foreach($interval_values as $iv_key=>$iv_val): ?>
																										<td><strong><?=number_format($iv_val->total-$iv_val->paid,2)?></strong>  <? $c_total=$c_total+$iv_val->total-$iv_val->paid;?></td>
																									<? endforeach; ?>
																									<td style='text-align:right;'><strong><?=CURRENCY_SYMBOL.number_format($c_total,2)?></strong></td>
																									</tr>																									
																								<? endif;?>
																								

																						</tbody>
																						</table>		

																					</div>
																							
																					<input type='hidden' id="current_url" value='<?=make_admin_url('fee','insert','insert&type=fee&session_id='.$session_id.'&ct_sect='.$ct_sect.'&interval_id='.$interval_id.'&student_id='.$student_id)?>'/>
																					<div class='span3'>
																						<div class=" portlet box blue">
																						<div class="portlet-title"><div class="caption">Previous Fee Details</div></div>
																						<div class="portlet-body">
																						<table class='fee_month_table'>
																							<tr>
																								<th width='20px;'></th><th>For Months</th>
																							</tr>																						
																							<?  ########### Previous Inteval Month Detail
																								if(!empty($PrevInt)):?>
																									<? foreach($PrevInt as $pack_key=>$p_val):?>
																											<?
																												if(getTotalDays($p_val->from_date,$p_val->to_date)<='31'): 
																													$show_mth=date('F, Y',strtotime($p_val->from_date));
																												else:
																													$show_mth=date('F',strtotime($p_val->from_date))."  to  ".date('F,Y',strtotime($p_val->to_date));
																												endif;
																												$p_amt=$p_val->total-$p_val->paid;
																											//if($p_amt>0):?>
																												<tr id='<?=(in_array($p_val->id,$multi_inverals))?'green_row':'red_row'?>'><td><input class='monthCheckBox' type='checkbox' value='<?=$p_val->id?>' <? if(in_array($p_val->id,$multi_inverals)){ echo 'checked';}?>/></td><td><?=$show_mth?></td></tr>
																										<?	//else: ?>
																											<!--	<tr id='<? //=($p_val->id==$interval_id)?'green_row':''?>'><td></td><td><? //=$show_mth?></td></tr> -->
																										<?	//endif; ?>
																									<? endforeach;
																										
																										########### Current Inteval Month Detail
																										if(getTotalDays($student_interval_detail->from_date,$student_interval_detail->to_date)<='31'): 
																												$ct_mth=date('F, Y',strtotime($student_interval_detail->from_date));
																											else:
																												$ct_mth=date('F',strtotime($student_interval_detail->from_date))."  to  ".date('F,Y',strtotime($student_interval_detail->to_date));
																										endif;							
																									?>
																							<? endif; ?>
																							</table>
																						</div>																
																					</div>
																				</div>	
																				<div style='clear:both;height:10px;'></div>
																					<!-- Paid only last current fee term -->
														
																				<div class="alert alert-block alert-success fade in" style='padding-right:14px;'>																							
																					<div class="row-fluid">	
																						<div class='span3'>																								
																							<div class='span5'><strong>Receipt Amount</strong> </div>
																							<div class='span7'><input type='text' class='span11 total_change' value='<?=number_format($c_total)?>' id='receipt_amount'/></div>
																						</div>
																						
																						<div class='span3'>																								
																							<div class='span5'><strong>Concession :</strong> </div>
																							<div class='span7'><input type='text' name='concession' class='span10 total_change' value='' id='concession'/></div>
																						</div>	

																						<div class='span6'>																								
																							<div class='span2'><strong>Remarks</strong> </div>
																							<div class='span10'><textarea type='text' name='remarks' class='span12' value=''></textarea></div>
																						</div>	
		
																					</div>
																					<div style='clear:both;height:10px;'></div>
																					<div class="row-fluid">
																						<div class='span3'>																								
																							<div class='span5'><strong>Receipt Date</strong> </div>
																							<div class='span7'><input type='text' class="span11 validate[required] new_format" id="payment_date" value="<?=date('Y-m-d')?>" name="payment_date"/></div>
																						</div>	
																						<div class='span3'>	
																						</div>
																						<div class='span3'>	
																						</div>
																						<div class='span3'>																								
																							<div class='span5'><strong>Net Amount :</strong> </div>
																							<div class='span7'><input type='text' class='span12 validate[required] total_change' id="AmountTotal"  value="<?=number_format($c_total)?>" name="total_amount"/></div>
																						</div>																							
																					</div>
																				
																				
																				</div>		
																												
																					<div class="form-actions clearfix">
																						<input type='hidden' name='multi_int' value='<?=$multi_int?>'/>
																						<input type='hidden' name='section' value='<?=$ct_sect?>'/>
																						<input type='hidden' name='student_id' value='<?=$student_id?>'/>
																						<input type='hidden' name='session_id' value='<?=$session_id?>'/>
																						<input type='hidden' name='interval_id' value='<?=$interval_id?>'/>	
																						<a href='<?=make_admin_url('fee','insert','insert')?>' class="btn red" ><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
																						<input class="btn green check_valid" type="submit" value="Add Fee" name="submit_fee" >
																					</div>
																					
																				</div>																					
																		</form>		
																		</div>
																	</div>
															</div>
												</div>													
												<div class="clearfix"></div>
												
												
																
											</div>
											<!-- Forth steps End -->
										</div>
										

									</div>
								
							</div>
						</div>
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

<script type="text/javascript">
	$(".session_filter_for_single").live("change", function(){
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_s").val();
			
			if(session_id.length > 0){
					   $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'session_id='+session_id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section','ajax_section&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});
	$("#GetStudents").live("click", function(){ 
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_sect").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#show_students").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_with_st','ajax_section_with_st&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#show_students").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
	 
	$("#select_registration").live("click", function(){	 
			var reg_id=$('#reg_id').val();
			var ct_school=<?=$school->id;?>;
			
			if(reg_id.length > 0){					   
					   $("#dynamic_model_reg").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'reg_id='+reg_id+'&ct_school='+ct_school;
						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_for_reg_find','ajax_section_for_reg_find&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model_reg").html(data);
						$("#dynamic_model").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
</script> 

	
	 <script type="text/javascript">     
        jQuery(document).ready(function() {
            $(".check_valid").live("click",function(){
             // Validation engine
                    $(".validation").validationEngine({
                         promptPosition : "topLeft"                        
                    });
          	});
			
		$(".total_change").live("keyup",function(){	
					GetValueForTotal();								
		});	
		});
		
		function GetValueForTotal(){
			var total_amount=$("#receipt_amount").val();
			total_amount=total_amount.replace(',', '');				
			var concession=$("#concession").val();
			concession=concession.replace(',', '');
			if(concession=='')
			{
				concession='0';
			}	
			
			var total=(parseFloat(total_amount)-parseFloat(concession)).toFixed(2);
				
			$('#AmountTotal').val(total);	
		}

	$(".print_start").live("click",function(){  
				var id=$(this).attr('rec_id');
				var doc_id=$(this).attr('doc_id');				
				var dataString = 'id='+id+'&doc_id='+doc_id;				
				$.ajax({
				type: "POST",						
				url: "<?=make_admin_url_window('ajax_calling','print','print&temp=fee');?>",
				data: dataString,
				success: function(data, textStatus) {
					var DocumentContainer = document.getElementById(data);
					var WindowObject = window.open('', "PrintWindow", "");
					WindowObject.document.writeln(data);
					WindowObject.document.close();
					WindowObject.focus();
					WindowObject.print();
					WindowObject.close();
				return false;
				}				
				});
	});	
	
	$("#new_value_save").live("click",function(){  
				var session_id='<?=$session_id?>';
				var ct_sect='<?=$ct_sect?>';
				var interval_id='<?=$interval_id?>';
				var student_id='<?=$student_id?>';
				var extra_fee=$('#extra').val();
				var extra_type=$('#extra_type').val();
				var extra_fee_name=$('.extra_fee_name').val();	
				
				if(extra_type=='+'){
					var sign='plus';
				}
				else{
					var sign='minus';
				}	
				
			if(extra_fee.length > 0){	
				$('#extra').css("border","1px solid #CCCCCC");
				$('.extra_fee_name').css("border","1px solid #CCCCCC");					
				if(extra_fee_name.length > 0){	
					$('#extra').css("border","1px solid #CCCCCC");	
				}
				else{
					$('.extra_fee_name').css("border","1px solid #F7023B");	
					return false;
				}
				
				var dataString = 'session_id='+session_id+'&ct_sect='+ct_sect+'&interval_id='+interval_id+'&student_id='+student_id+'&extra_fee='+extra_fee+'&extra_fee_name='+extra_fee_name+'&extra_type='+sign;	
				
				$("#right_calculation").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
										
				$.ajax({
				type: "POST",						
				url: "<?=make_admin_url_window('ajax_calling','extra_fee_add','extra_fee_add&temp=fee');?>",
				data: dataString,
				success: function(data, textStatus) {
					$("#right_calculation").html(data);
					return false;
					}				
					});
				}
			else{			
				$('#extra').css("border","1px solid #F7023B");
				return false;
			}			
	});	

	$(".monthCheckBox").live("click",function(){  	
		var current_url=$("#current_url").val();
		
			var extra='multi_int=';
						$(".monthCheckBox").each(function() {
							if ($(this).is(':checked')) {
									var id=$(this).val();
									extra=extra+id+',';
							}
						});
			if(extra!='multi_int='){
				extra = extra.slice(0, - 1);
			}
			
			window.location.href = current_url+extra;				
	});	
 </script>		
 
 
 
 
 
 
 
 
<? endif;?> 
 