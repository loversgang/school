

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Students Fee
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Add Student Fee
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>

		
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
			<? if($type!='select'):?>
				<div class="tiles pull-right">
							<div class="tile bg-green <?php echo ($section=='update')?'selected':''?>">
								<a href="<?php echo make_admin_url('fee', 'insert', 'insert');?>">
									<div class="corner"></div>
									<div class="tile-body"><i class="icon-arrow-left"></i></div>
									<div class="tile-object"><div class="name"> Change Session</div></div>
								</a> 
							</div>	

				</div>   		
			<? endif;?>
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
            
			<div class="row-fluid">
					<div class="span12">
						<div id="form_wizard_1" class="portlet box blue">
							<div class="portlet-title">
							<div class="caption"><i class="icon-reorder"></i> Student Fee Wizard - <span class="step-title">Step 1 of 3</span></div>
							</div>
							
							<div class="portlet-body form">
								<div class="form-wizard">						
										<!-- Main steps -->
										<div class="navbar steps">
											<div class="navbar-inner">
												<ul class="row-fluid nav nav-pills">
													<li class="span4 <?=($type=='select')?'active':'';?>">
														<a class="step <?=($type=='select')?'active':'';?>">
														<span class="number">1</span>
														<span class="desc"><i class="icon-ok"></i> Select Student </span>   
														</a>
													</li>
													
													<li class="span4 <?=($type=='list')?'active':'';?>">
														<a class="step <?=($type=='list')?'active':'';?>">
														<span class="number">2</span>
														<span class="desc"><i class="icon-ok"></i> Section Students</span>   
														</a>
													</li>
													
													<li class="span4 <?=($type=='fee')?'active':'';?>">
														<a class="step <?=($type=='fee')?'active':'';?>">
														<span class="number">3</span>
														<span class="desc"><i class="icon-ok"></i> Add Student Fee</span>   
														</a>
													</li>													
												</ul>
											</div>
										</div>
										<!-- Main End -->
										<div class="progress progress-success progress-striped" id="bar">
											<div class="bar" style="width: <?=$per?>"></div>
										</div>
										
										
										<div class="tab-content">
											<!-- First steps -->
											<div id="tab1" class="tab-pane <?=($type=='select')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='list'/>											
												
												<div class="row-fluid">
													<div class="span12">		
														<h3 class="block">Select Session & Section</h3>
															<div class="alert alert-block alert-success">
																<div class="row-fluid">	
																	<div class="span5">														
																		<div class="control-group">
																		<label class="control-label">Select Session</label>
																		<div class="controls">
																		<select class="select2_category session_filter_for_single span9" data-placeholder="Select Session Students" name='session_id' id='session_id'>
																		<option value="">Select Session</option>
																			<?php $session_sr=1;while($object=$QuerySession->GetObjectFromRecord()): ?>
																				<option value='<?=$object->id?>' <? if($object->id==$session_id){ echo 'selected'; }?> ><?=ucfirst($object->title)?></option>
																			<? $session_sr++; endwhile;?>
																		</select>
																		</div>
																		</div>
																	</div>																		
																	<div class="span7" id='dynamic_model'>
																			<input type='hidden' class='section_filter' value='A' id='ct_s'/>
																	</div>		
																</div>
																<div class="row-fluid">	
																	<span class="help-block"> Note:- Select any previous payment period if you want to edit the previous fee heads to adjust previous fee payments.</span>
																</div>
															</div>	
															<div class="clearfix"></div>
													</form>	
													</div>	
												</div>
																						
												<div class="form-actions clearfix" >
														<a href="<?php echo make_admin_url('fee', 'list', 'list');?>" class="btn red" name="cancel" > Cancel</a>
												</div>
											</div>
											<!-- First steps End -->
											
											
									
											<!-- Second steps -->
											<div id="tab2" class="tab-pane <?=($type=='list')?'active':'';?>">
												<form action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='fee'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='fee'/>
												<input type='hidden' name='session_id' value='<?=$session_id?>'/>
												<input type='hidden' name='ct_sect' value='<?=$ct_sect?>'/>
												<input type='hidden' name='interval_id' value='<?=$interval_id?>'/>													
												<h3 id="title_show" class="block alert alert-info"><?=$session->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?> </h3>
													
													<div class="span12" style='margin-left:0px;'>				
														<div class="controls">
																	<table id="<? if(!empty($interval_id) && empty($student_id)): echo "sample_2"; endif; ?>" class="table table-striped table-bordered table-hover">
																		<thead>
																		<tr>
																			<th style='text-align:center;' class="hidden-480 sorting_disabled"></th>
																			<th class="hidden-480">Reg ID</th>
																			<th class="hidden-480" >Roll No.</th>
																			<th class="hidden-480" >Name</th>
																			<th class="hidden-480" >Father Name</th>
																		</tr>
																		</thead>
																		<tbody>
																		<? if(!empty($records)):?>																		
																		<?php $sr=1;foreach($records as $sk=>$stu): $checked='';?>
																			<tr class="odd gradeX">
																				<td style='text-align:center;background:#F9F9F9;'>
																				<input type="radio" name='student_id' checked value='<?=$stu->id?>' style='margin-left:0px;'/>
																				</td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->reg_id?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->roll_no?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
																				<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->father_name)?></td>
																			</tr>
																		<? $sr++; endforeach;
																		   else:	?>
																		<tr class="odd gradeX"><td></td><td colspan='4'>Sorry, No Record Found..!</td></tr>
																	<? endif;?>
																		</tbody>
																	</table>
														</div>
													</div>
													<div class="clearfix"></div>
									
													<div class="form-actions clearfix" >
															<a href="<?php echo make_admin_url('fee', 'insert', 'insert');?>" class="btn red" name="cancel" > Cancel</a>
															<input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
													</div>
												</form>	
											</div>
											<!-- Second steps End -->									
											
											
											<!-- Third steps -->
											<div id="tab3" class="tab-pane <?=($type=='fee')?'active':'';?>">
												<h3 id="title_show" class="block alert alert-info"><?=$session->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?> <font class="green_color"> &gt; </font> <?=ucfirst($student_info->first_name).' '.$student_info->last_name?>  </h3>
														<div class="row-fluid">
															<div class="span12">
																	<div class="portlet box blue">
																		<div class="portlet-title"> 
																			<div class="caption">Fee Information : </div>
																			<div class="tools">
																				<div class="actions"><?=date('d M, Y',strtotime($from))?> to <?=date('d M, Y',strtotime($to))?></div>
																			</div>
																		</div>
																		<div class="portlet-body">
																		 <form class="form-horizontal validation" action="<?php echo make_admin_url('fee', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" id="validation">
																			
																			<div class="clearfix"></div>
																			<!-- More Entry Case -->
																				<div class="row-fluid">	
																					<!-- First Entry of the months -->	
																					<div class="span6">
																						<!-- Student Info -->
																						<div class="row-fluid">	
																						<div class="alert alert-block alert-success fade in">	
																								<div class="row-fluid">	
																									<div class='span4' style='line-height: 26px;'><strong>Student Granted: </strong></div>
																									<div class='span6' style='line-height: 26px;'><?=$student_info->concession?></div>												
																								</div>	
																								<? if(!empty($student_info->concession_type)):?>
																									<div class="row-fluid">	
																										<div class='span4' style='line-height: 26px;'><strong>Concession: </strong></div>
																										<div class='span6' style='line-height: 26px;'><?=CURRENCY_SYMBOL.' '.number_format($student_info->concession_type,2)?></div>												
																									</div>	
																								<? endif;?>	
																						</div>
																						</div>
																						<div class="alert alert-block alert-success fade in" >																							
																								<div class="row-fluid">	
																									<div class='span4'>Session: </div>
																									<div class='span6'><?=$session->title?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Section: </div>
																									<div class='span6'><?=$ct_sect?></div>												
																								</div>																									
																								<div class="row-fluid">	
																									<div class='span4'>Reg Id: </div>
																									<div class='span6'><?=$student_info->reg_id?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Name: </div>
																									<div class='span6'><?=ucfirst($student_info->first_name).' '.$student_info->last_name?></div>												
																								</div>	
																								<div class="row-fluid">	
																									<div class='span4'>Roll No.: </div>
																									<div class='span6'><?=$student_info->roll_no?></div>												
																								</div>

																								
																								<? if(!empty($student_interval_detail->vehicle_fee)):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Vehicle Fee: </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($student_interval_detail->vehicle_fee,2)?></div>												
																									</div>																									
																								<? endif;?>	

																								<? if($student_interval_detail->absent_fine>0):?>
																								<div class="row-fluid" >	
																									<div class='span4' id='color_red'>Attendance Fine: </div>
																									<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($student_interval_detail->absent_fine,2)?></div>
																								</div>	
																								<? endif;?>		
																								
																								<? if(!empty($student_interval_detail->late_fee)):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Late Fee Fine: </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($student_interval_detail->late_fee,2)?></div>												
																									</div>																									
																								<? endif;?>
																								
																								<? if(isset($Absent_last) && !empty($Absent_last)):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Attendance Fine: (<?=date('M,Y',strtotime($student_interval_detail->from_date)).' - '.date('M,Y',strtotime($student_interval_detail->to_date))?>) </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($Absent_last*$session->absent_fine,2)?></div>												
																									</div>	
																									<div class="row-fluid" >
																										<strong>Note:-</strong> Please add this fine manually.
																									</div>	
																								<? endif;?>																								
																								
																								
																								<? if(!empty($total_paid)):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Total Paid: </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format($total_paid,2)?></div>												
																									</div>																									
																								<? endif;?>																								
																						</div>
																						<!-- Student Info End-->
																					</div>
																					
																					<div class='span6' id='right_calculation'>
																						<div class="portlet-title green">
																							<div class="caption">Calculate Student Fee</div>
																							<div class="tools">
																									<div class="actions">
																										<a class="btn success mini" href="<?=make_admin_url('session','section','section&ct_session='.$session_id.'&id='.$student_info->rel_id.'&ct_section='.$student_info->section)?>" title='Click here to edit student fee'><i class="icon-pencil"></i> Edit</a>
																									</div>
																							</div>
																						</div>
																						<table class="table table-striped table-bordered table-advance table-hover" id='fee_table'>
																						<thead>
																						<tr><th style='width:50%;'>Fee Head Name</th><th style='width:50%;text-align:right;'>Amount (<?=CURRENCY_SYMBOL?>)</th>
																						</tr></thead>
																						<tbody>
																								<? $t_am='0'; $new_added=0;
																									$heads=html_entity_decode($student_interval_detail->heads); 
																									$heads=unserialize($heads);
																										
																									if(!empty($heads)): $st='0';																
																												/* Put hete pending fee  */
																												if(!empty($pending_fee)):
																													echo "<tr><td id='black_font_fee'>Previous Balance</td><td id='black_font_fee' style='text-align:right'>".CURRENCY_SYMBOL.number_format($pending_fee,2)."</td></tr>";
																													echo "<input type='hidden' name='fee_record[Previous Balance]' value='".$pending_fee."'/>";
																													$t_am=$t_am+$pending_fee;																									
																												endif;
 																									
																												foreach($heads as $p_k=>$p_v):																												
																														echo "<tr><td>".$p_v['title']."&nbsp;(".ucfirst($p_v['type']).")</td><td style='text-align:right'>".CURRENCY_SYMBOL.number_format($p_v['amount'],2)."</td></tr>";
																														echo "<input type='hidden' name='fee_record[".$p_v['title']." (".ucfirst($p_v['type']).")]' value='".($p_v['amount'])."'/>";
																														$t_am=$t_am+$p_v['amount'];																													
																												endforeach;

																												#add Vehicle Fee if available 
																												if(!empty($student_interval_detail->vehicle_fee)):
																													echo "<tr><td id='black_font_fee'>Vehicle Fee</td><td style='text-align:right' id='black_font_fee'>".CURRENCY_SYMBOL.number_format($student_interval_detail->vehicle_fee,2)."</td></tr>";
																													echo "<input type='hidden' name='vehicle_fee' value='".($student_interval_detail->vehicle_fee)."'/>";
																													$t_am=$t_am+$student_interval_detail->vehicle_fee;	
																												else:
																													echo "<input type='hidden' value='0' id='vehicle_fine_charge'/>";
																												endif;	
																											
																												#Session Total  
																												echo "<tr><td class='total'>Sub Total</td><td class='total' colspan='2' style='text-align:right;'>".CURRENCY_SYMBOL." <span class='make_total_new'>".number_format($t_am,2)."</span></td></tr>";
																												$AmountTotal=$t_am;
						
																												#add Attendance Fine if available 
																												if(!empty($student_interval_detail->absent_fine)):
																														$AmountTotal=($AmountTotal+($student_interval_detail->absent_fine));
																														echo "<tr><td>Attendance Fine</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;+&nbsp;<input type='text' id='absent_fine_charge' class='span5 total_change' add='0' p_val='".($student_interval_detail->absent_fine)."' name='absent_fine' value='".$student_interval_detail->absent_fine."' readonly/> <input type='hidden' value='".$Absent."' name='absent_days'/></td></tr>";
																												else:
																													echo "<input type='hidden' value='0' id='absent_fine_charge'/>";	
																												endif;	
																																																						
																												#Add Late Fee Fine if available 
																												if(!empty($student_interval_detail->late_fee)):
																													$AmountTotal=($AmountTotal+$student_interval_detail->late_fee);
																													echo "<tr><td id='black_font_fee'>Late Fee Fine</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;+&nbsp;<strong><input type='text' id='late_fee_charge' class='span5 total_change' name='late_fee' value='".$student_interval_detail->late_fee."' readonly/> </td></tr>";
																												else:
																													echo "<input type='hidden' value='0' id='late_fee_charge'/>";
																												endif;	
																											
																												
																											
																												# Less concession 
																												if(!empty($student_interval_detail->concession)):
																													echo "<tr><td id='black_font_fee'>Concession (".$student_info->concession.")</td><td colspan='2' style='text-align:right'><strong class='large_text'>&nbsp;-&nbsp;<strong><input type='text' id='consession_fee' class='span5 total_change' name='concession' value='".$student_info->concession_type."' readonly/> </td></tr>";
																													echo "<input type='hidden' value='".$student_interval_detail->concession."' name='concession'/>";
																													$AmountTotal=$AmountTotal-$student_interval_detail->concession;
																												else:
																													echo "<input type='hidden' value='0' id='consession_fee'/>";
																												endif;
																												
																												# Less Total Paid 
																												if(!empty($total_paid)):
																													$AmountTotal=$AmountTotal-$total_paid;																												
																												endif;
																												echo "<input type='hidden' value='".$AmountTotal."' id='session_total' name='session_total'/>";
																												
																												#here is the extra field
																												if($student_interval_detail->other_fee_type=="-"):
																													$AmountTotal=($AmountTotal-$student_interval_detail->other_fee);	
																												elseif($student_interval_detail->other_fee_type=="+"):
																													$AmountTotal=($AmountTotal+$student_interval_detail->other_fee);	
																												endif;																								    
																												
																												
																												#here is the extra field
																												echo "<tr><td id='black_font_fee'>
																													<input type='text' class='span8 total_change extra_fee_name' name='other_fee_name' placeholder='Other fee Name' value='".$student_interval_detail->other_fee_name."'/>
																													";
																													#only Update in last interval
																													if($interval_id==$last_int_id): 
																														echo "<div class='btn green span3' style='float:right;' id='new_value_save'>Save</div>";
																													endif;
																													
																													echo "</td><td colspan='2' style='text-align:right'>
																														<select name='other_fee_type' class='span3 total_change' id='extra_type'>";?>
																															<option value='-' <?=$student_interval_detail->other_fee_type=='-'?"selected":""?>><strong class='large_text'>-&nbsp;&nbsp;<strong></option>
																															<option value='+' <?=$student_interval_detail->other_fee_type=='+'?"selected":""?>><strong class='large_text'>+&nbsp;&nbsp;<strong></option>
																														<?
																														
																														echo "</select>	
																												<input type='text' id='extra' class='span5 total_change validate[custom[onlyNumberSp]]' name='other_fee' placeholder='Other fee' value='".$student_interval_detail->other_fee."'/> </td></tr>";
																											
																											# Show Less Total Paid 
																												if(!empty($total_paid)):
																													echo "<tr><td class='total_paid'>Total Paid</td><td colspan='2' style='text-align:right'><strong class='total_paid'>&nbsp;-&nbsp;</strong>".CURRENCY_SYMBOL." <span class='total_paid'>".number_format($total_paid,2)."</td></tr>";
																												endif;																		
																											
																											# Total Amount 
																											echo "<tr><td class='total'>Total</td>
																											<td class='total' colspan='2' style='text-align:right'>".CURRENCY_SYMBOL." <span class='AmountTotal'>".number_format($AmountTotal,2)."</span>
																											</td></tr>";
																									else:
																											echo "<tr><td colspan='2'>Sorry, No Record Found..!</td></tr>";														
																									endif;?>
																						</tbody>
																						</table>
																					</div>
																					<input type='hidden' id="paid" value='0'/>
																				</div>	
																				<br/>
																					<!-- Paid only last current fee term -->
																					<? if($interval_id==$last_int_id): ?>
																					<div class=" portlet box blue">
																						<div class="portlet-title"><div class="caption">Add Fee</div></div>
																						<div class="portlet-body">
																						<div class="span6">	
																							<div class="control-group">
																									<label for="payment_date" class="control-label">Date <span class="required">*</span></label>
																									<div class="controls">
																									  <input type="text" class="span10 m-wrap validate[required] new_format" id="payment_date" value="<?=date('Y-m-d')?>" name="payment_date" >
																									</div>
																							</div> 
																							<div class="control-group">
																									<label for="total_amount" class="control-label">Amount Paying  <span class="required">*</span></label>
																									<div class="controls">
																									  <input type="text" class="span10 m-wrap validate[required]" id="total_amount"  value="" name="total_amount">
																									</div>
																							</div>
																						</div>		
																						<div class="span6">		
																							<div class="control-group">
																										<label for="remarks" class="control-label">Remarks</label>
																										<div class="controls">
																										  <textarea name="remarks" class="span10 m-wrap"></textarea>
																										</div>
																							</div>																						
																						</div>
																						<div class="clearfix"></div>
																						</div>
																					</div>

																				
																					<div class="form-actions clearfix">
																						<input type='hidden' name='section' value='<?=$ct_sect?>'/>
																						<input type='hidden' name='student_id' value='<?=$student_id?>'/>
																						<input type='hidden' name='session_id' value='<?=$session_id?>'/>
																						<input type='hidden' name='interval_id' value='<?=$interval_id?>'/>	
																						<a href='<?=make_admin_url('fee','insert','insert')?>' class="btn red" ><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
																						<input class="btn green check_valid" type="submit" value="Add Fee" name="submit_fee" >
																					</div>
																					<? endif;?>
																				</div>																					
																		</form>		
																		</div>
																	</div>
															</div>
																									
												<div class="clearfix"></div>
												
												
												
												<!-- ******************************************* -->
													<div class="row-fluid">
															<div class="span12">
																<!-- BEGIN EXAMPLE TABLE PORTLET-->
																<div class="portlet box green">
																	<div class="portlet-title">
																		<div class="caption"><i class="icon-list"></i>Previous Paid Fee List:</div>
																		<div class="tools"></div>
																	</div>
																	<div class="portlet-body">
																	<table class="table table-striped table-bordered table-hover" id="sample_2">
																			<thead>
																				<tr>
																					<th class="hidden-480" >Sr. No.</th>
																					<th class="hidden-480" style='text-align:center;'>Payment Date</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Amount Paid (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Action</th>
																				</tr>
																			</thead>
																			<? if(!empty($Fee_list)): ?>
	
																			<tbody>
																			<?php $sr=1;foreach($Fee_list as $sk=>$rec): $checked='';?>
																				<tr class="odd gradeX">
																					<td><?=$sr?>.</td>
																					<td style='text-align:center;'><?=date('d M, Y',strtotime($rec->payment_date))?></td>
																					<td style='text-align:right;'><?=CURRENCY_SYMBOL.' '.number_format($rec->total_amount,2)?></td>
																					<td style='text-align:right;'>
																					<? if($interval_id==$last_int_id): ?>
																					<a class="btn blue icn-only tooltips mini" href="<?php echo make_admin_url('fee', 'update', 'update', 'id='.$rec->id.'&student_id='.$student_id.'&interval_id='.$interval_id.'&session_id='.$session_id.'&ct_sect='.$ct_sect.'&type='.$type)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
																					<a class="btn red icn-only tooltips mini" href="<?php echo make_admin_url('fee', 'delete', 'delete', 'id='.$rec->id.'&delete=1&student_id='.$student_id.'&interval_id='.$interval_id.'&session_id='.$session_id.'&ct_sect='.$ct_sect.'&type='.$type)?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
																					<? endif;?>
																					<!--
																					<? if(is_object($slip)):?>
																						&nbsp;&nbsp; <a class="btn black icn-only tooltips mini print_start" rec_id='<?=$rec->id?>' doc_id='<?=$slip->id?>' title="click here to print receipt">Print</a>
																					<? endif;?>
																					-->
																					</td>
																				</tr>
																				<?

																					$sr++;
																				endforeach;?>									
																			</tbody>
																		<? endif;?>
																		</table>
																
																	<div class="clearfix"></div>
																	
																	</div>
																</div>
																						
																<!-- END EXAMPLE TABLE PORTLET-->
															</div>
														</div>	
												</div>					
											</div>
											<!-- Forth steps End -->
										</div>
										

									</div>
								
							</div>
						</div>
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

<script type="text/javascript">
	$(".session_filter_for_single").live("change", function(){
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_s").val();
			
			if(session_id.length > 0){
					   $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'session_id='+session_id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section','ajax_section&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});
	$("#GetStudents").live("click", function(){ 
			var session_id=$("#session_id").val();
			var ct_s=$("#ct_sect").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#show_students").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id+'&ct_s='+ct_s;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_with_st','ajax_section_with_st&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#show_students").html(data);
						$("#dynamic_model_reg").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
	 
	$("#select_registration").live("click", function(){	 
			var reg_id=$('#reg_id').val();
			var ct_school=<?=$school->id;?>;
			
			if(reg_id.length > 0){					   
					   $("#dynamic_model_reg").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'reg_id='+reg_id+'&ct_school='+ct_school;
						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_for_reg_find','ajax_section_for_reg_find&temp=fee');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model_reg").html(data);
						$("#dynamic_model").html('');
						}				
						});
			}
			else{
				return false;
			}
	});	
</script> 

	
	 <script type="text/javascript">     
        jQuery(document).ready(function() {
            $(".check_valid").live("click",function(){
             // Validation engine
                    $(".validation").validationEngine({
                         promptPosition : "topLeft"                        
                    });
          	});
			
		$(".total_change").live("change",function(){	
					GetValueForTotal();								
		});	
		});
		
		function GetValueForTotal(){
			var session_total=$("#session_total").val();
			var extra=$("#extra").val();
			var extra_type=$("#extra_type").val();
			if(extra!=='')
			{
			
				if(extra_type=='-'){ 
					session_total=parseInt(session_total)-parseInt(extra);
				}
				if(extra_type=='+'){
					session_total=parseInt(session_total)+parseInt(extra);
				}
			}		
			$('.AmountTotal').html(session_total);	
		}

	$(".print_start").live("click",function(){  
				var id=$(this).attr('rec_id');
				var doc_id=$(this).attr('doc_id');				
				var dataString = 'id='+id+'&doc_id='+doc_id;				
				$.ajax({
				type: "POST",						
				url: "<?=make_admin_url_window('ajax_calling','print','print&temp=fee');?>",
				data: dataString,
				success: function(data, textStatus) {
					var DocumentContainer = document.getElementById(data);
					var WindowObject = window.open('', "PrintWindow", "");
					WindowObject.document.writeln(data);
					WindowObject.document.close();
					WindowObject.focus();
					WindowObject.print();
					WindowObject.close();
				return false;
				}				
				});
	});	
	
	$("#new_value_save").live("click",function(){  
				var session_id='<?=$session_id?>';
				var ct_sect='<?=$ct_sect?>';
				var interval_id='<?=$interval_id?>';
				var student_id='<?=$student_id?>';
				var extra_fee=$('#extra').val();
				var extra_type=$('#extra_type').val();
				var extra_fee_name=$('.extra_fee_name').val();	
				
				if(extra_type=='+'){
					var sign='plus';
				}
				else{
					var sign='minus';
				}	
				
			if(extra_fee.length > 0){	
				$('#extra').css("border","1px solid #CCCCCC");
				$('.extra_fee_name').css("border","1px solid #CCCCCC");					
				if(extra_fee_name.length > 0){	
					$('#extra').css("border","1px solid #CCCCCC");	
				}
				else{
					$('.extra_fee_name').css("border","1px solid #F7023B");	
					return false;
				}
				
				var dataString = 'session_id='+session_id+'&ct_sect='+ct_sect+'&interval_id='+interval_id+'&student_id='+student_id+'&extra_fee='+extra_fee+'&extra_fee_name='+extra_fee_name+'&extra_type='+sign;	
				
				$("#right_calculation").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
										
				$.ajax({
				type: "POST",						
				url: "<?=make_admin_url_window('ajax_calling','extra_fee_add','extra_fee_add&temp=fee');?>",
				data: dataString,
				success: function(data, textStatus) {
					$("#right_calculation").html(data);
					return false;
					}				
					});
				}
			else{			
				$('#extra').css("border","1px solid #F7023B");
				return false;
			}			
	});	

	
 </script>		