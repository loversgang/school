

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Students Fee
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Add Student Fee
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>

		
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
			
				<div class="tiles pull-right">
				
							<div class="tile bg-purple <?php echo ($section=='view')?'selected':''?>">
								<a href="<?php echo make_admin_url('fee', 'list', 'list&type=list&session_id='.$session_id.'&ct_sect='.$ct_sect.'&month='.$month);?>">
									<div class="corner"></div>
									<div class="tile-body"><i class="icon-arrow-left"></i></div>
									<div class="tile-object"><div class="name"> Section Student</div></div>
								</a> 
							</div>	
				</div>   		
			
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
             
			<div class="row-fluid">
					<div class="span12">
							<div class="portlet-body form">
								<div class="form-wizard">	
										
										<div class="tab-content">											
											
											<!-- Third steps -->
											<div id="tab3" class="tab-pane active">
												<div class="row-fluid">
															<div class="span12">
																	<div class="portlet box blue">
																		<div class="portlet-title"> 
																			<div class="caption">Fee Information : </div>
																		</div>
																		<div class="portlet-body">
																		 <form class="form-horizontal validation" action="<?php echo make_admin_url('fee', 'insert', 'insert&type=fee&month='.$month)?>" method="POST" enctype="multipart/form-data" id="validation">
																				<br/>
																				
																			<!-- More Entry Case -->
																				
																				<div class="row-fluid">																				
																					<div class="span6"> 																						
																						<div class="alert alert-block alert-success fade in">																							
																								<div class="row-fluid">	
																									<div class='span4'>Session: </div>
																									<div class='span6'><?=$object->title?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Section: </div>
																									<div class='span6'><?=$ct_sect?></div>												
																								</div>																									
																								<div class="row-fluid">	
																									<div class='span4'>Reg Id: </div>
																									<div class='span6'><?=$student_info->reg_id?></div>												
																								</div>
																								<div class="row-fluid">	
																									<div class='span4'>Name: </div>
																									<div class='span6'><?=ucfirst($student_info->first_name).' '.$student_info->last_name?></div>												
																								</div>	
																								<div class="row-fluid">	
																									<div class='span4'>Roll No.: </div>
																									<div class='span6'><?=$student_info->roll_no?></div>												
																								</div>
																								<? if(($Absent*$object->absent_fine)>$Month_entry->absent_fine):?>
																									<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Attendance Fine: </div>
																										<div class='span6' id='color_red'><?=$Absent?> * <?=CURRENCY_SYMBOL.''.number_format(($Absent*$object->absent_fine)-$Month_entry->absent_fine,2)?></div>												
																									</div>	
																								<? endif;?>	
																								<? if(date('d')>=$object->late_fee_days):?>
																									<? if(($object->late_fee_rate)>$Month_entry->late_fee):?>
																										<div class="row-fluid" >	
																											<div class='span4' id='color_red'>Late Fee Fine: </div>
																											<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.''.number_format(($object->late_fee_rate-$Month_entry->late_fee),2)?></div>												
																										</div>																									
																									<? endif;?>
																								<? endif;?>
																								<div class="row-fluid" >	
																										<div class='span4' id='color_red'>Balance Due: </div>
																										<div class='span6' id='color_red'><?=CURRENCY_SYMBOL.' '.number_format(($balance_amount),2)?></div>												
																								</div>	
																						</div>
																						<!-- Student Info End-->
																					
																						
																					</div>
																					
																					<? if(!empty($balance_amount)):?>
																					<div class='span6'>
																						<div class="portlet-title green">
																							<div class="caption">Calculate Fee</div>
																							<div class="tools"></div>
																						</div>
																						<table class="table table-striped table-bordered table-advance table-hover">
																						<thead>
																						<tr><th style='width:60%;'>Fee Head Name</th><th>Amount (<?=CURRENCY_SYMBOL?>)</th>
																						</tr></thead>
																						<tbody>
																								<? if(!empty($all_heads)): $srr=1;
																									foreach($all_heads as $fee_key=>$fee_value):
																									$input_class=''; $tr_class=''; $icon_class=''; $icon_field='';
																									
																									$input_class=(in_array($fee_value['fee_id'],$paid_heads))?'readonly=""':'';
																									$tr_class=(in_array($fee_value['fee_id'],$paid_heads))?'removed':'';
																									$icon_class=(in_array($fee_value['fee_id'],$paid_heads))?'plus':'remove';
																									$icon_field=(in_array($fee_value['fee_id'],$paid_heads))?'add_field':'remove_field';
																									$icon_disable=(in_array($fee_value['fee_id'],$paid_heads))?'disabled=""':'';																									
																										if(in_array($fee_value['fee_id'],$paid_heads)):?>
																											<tr class='row-<?=$srr?> <?=$tr_class?>'><td><?=ucfirst($fee_value['title'])?> &nbsp;(<?=ucfirst($fee_value['type'])?>)</td>
																											<td><input type="text" name='fee_heads[<?=$fee_value['fee_id']?>]' value="<?=number_format($fee_value['amount'],2)?>"  placeholder="0.00" class="span7" <?=$input_class?> <?=$icon_disable?>></td>
																											</tr>
																											<?  if(!in_array($fee_value['fee_id'],$paid_heads)): 
																														$amount=$amount+$fee_value['amount']; 
																												endif; $srr++;
																										endif;
																									endforeach;
																										
																										#Attendance Fine Paid
																										if(!empty($Month_entry->absent_fine)):
																											echo "<tr class='removed'><td>Attendance Fine</td><td><input type='text' class='span7' value='".number_format($Month_entry->absent_fine,2)."' disabled=''/></td></tr>";
																										endif;
																										
																										#Late Fee Paid
																										if(!empty($Month_entry->late_fee)):
																											echo "<tr class='removed'><td>Late Fee Fine</td><td><input type='text' class='span7' value='".number_format($Month_entry->late_fee,2)."' disabled=''/></td></tr>";
																										endif;
																										#Balance
																										$amount=$amount+$balance_amount;
																										echo "<tr><td class='total'>Total</td><td class='total'>".CURRENCY_SYMBOL." <span id='make_total'>".number_format($Month_entry->total_amount,2)."</span></td></tr>";
																									else:
																										 echo "<tr><td colspan='2'>Sorry, No Record Found..!</td></tr>";	
																								endif;?>
																						</tbody>
																						</table>
																					</div>
																					<? endif;?>	
																				</div>
																									
																		</div>
																	</div>
															</div>
														</div>												
												<div class="clearfix"></div>
												
												
												
												<!-- ******************************************* -->
													<div class="row-fluid">
															<div class="span12">
																<!-- BEGIN EXAMPLE TABLE PORTLET-->
																<div class="portlet box green">
																	<div class="portlet-title">
																		<div class="caption"><i class="icon-list"></i><?=date('M Y',strtotime(date('Y').'-'.$month.'-'.date('d')))?> Fee:</div>
																		<div class="tools"></div>
																	</div>
																	<div class="portlet-body">
																	<table class="table table-striped table-bordered table-hover" id="sample_2">
																			<thead>
																				<tr>
																					<th class="hidden-480" >Sr. No.</th>
																					<th class="hidden-480" style='text-align:center;'>On Date</th>
																					<th class="hidden-480" style='vertical-align:top;text-align:center'>Late Fee</th>
																					<th class="hidden-480" style='vertical-align:top;text-align:center'>Attendance Fine</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total Amount (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total Paid (<?=CURRENCY_SYMBOL?>)</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Balance (<?=CURRENCY_SYMBOL?>)</th>
																					
																				</tr>
																			</thead>
																			<? if(!empty($Fee_list)):?>
																			<? 	$T_amount=$Month_entry->total_amount;
																				$bal=$Month_entry->total_amount;
																				$fine_amount=$Month_entry->absent_fine;
																				$late_fee_amount=$Month_entry->late_fee;																		
																			?>	
																			<tbody>
																			<?php $sr=1;foreach($Fee_list as $sk=>$rec): $checked='';?>
																				<? $bal=$bal-$rec->amount;?>	
																				<tr class="odd gradeX">
																					<td><?=$sr?>.</td>
																					<td style='text-align:center;'><?=$rec->on_date?></td>
																					<td style='text-align:center;'><?=CURRENCY_SYMBOL.' '.number_format($late_fee_amount,2)?></td>
																					<td style='text-align:center;'><?=CURRENCY_SYMBOL.' '.number_format($fine_amount,2)?></td>
																					<td style='text-align:right;'><?=CURRENCY_SYMBOL.' '.number_format($T_amount,2)?></td>
																					<td style='text-align:right;'><?=CURRENCY_SYMBOL.' '.number_format($rec->amount,2)?></td>
																					<td style='text-align:right;'><span class='A'><strong><?=CURRENCY_SYMBOL.' '.number_format($bal,2)?></strong></span></td>
																					
																				</tr>
																				<?
																					$T_amount=$bal;
																					if($fine_amount>0): $fine_amount=$fine_amount-$Month_entry->absent_fine; endif;
																					if($late_fee_amount>0): $late_fee_amount=$late_fee_amount-$Month_entry->late_fee; endif;
																					
																					$sr++;
																				endforeach;?>									
																			</tbody>
																		<? endif;?>
																		</table>
																
																	<div class="clearfix"></div>
																	
																	</div>
																</div>
																						
																<!-- END EXAMPLE TABLE PORTLET-->
															</div>
														</div>											
											</div>
											
										</div>
										

									</div>
								
							</div>
						
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

