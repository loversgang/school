
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Students Fee
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Edit Student Fee
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<div class="tile bg-purple <?php echo ($section=='update')?'selected':''?>">
								<a href="<?php echo make_admin_url('fee', 'insert', 'insert&type=fee&session_id='.$session_id.'&ct_sect='.$ct_sect.'&student_id='.$student_id.'&interval_id='.$interval_id);?>">
									<div class="corner"></div>
									<div class="tile-body"><i class="icon-arrow-left"></i></div>
									<div class="tile-object"><div class="name"> Go Back</div></div>
								</a> 
							</div>	
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
		  
			  
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('fee', 'update', 'update&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Edit Student Fee</div>
                                            <div class="tools">
                                                  <div class="actions"><?=date('d M, Y',strtotime($student_interval_detail->from_date))?> to <?=date('d M, Y',strtotime($student_interval_detail->to_date))?></div>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">      
									<!-- Basic Info  -->
										<h4 class="form-section"></h4>
										<div class="row-fluid">	
												<div class="span6 "> 
													<!-- Student Info -->
													<div class="alert alert-block alert-success fade in">
														
															<div class="row-fluid">	
																<div class='span4'>Session: </div>
																<div class='span6'><?=$session->title?></div>												
															</div>
															<div class="row-fluid">	
																<div class='span4'>Section: </div>
																<div class='span6'><?=$ct_sect?></div>												
															</div>																									
															<div class="row-fluid">	
																<div class='span4'>Reg Id: </div>
																<div class='span6'><?=$student_info->reg_id?></div>												
															</div>
															<div class="row-fluid">	
																<div class='span4'>Name: </div>
																<div class='span6'><?=ucfirst($student_info->first_name).' '.$student_info->last_name?></div>												
															</div>	
															<div class="row-fluid">	
																<div class='span4'>Roll No.: </div>
																<div class='span6'><?=$student_info->roll_no?></div>												
															</div>

													</div>
													<!-- Student Info End-->
												

												</div>
												<div class='span6'>
													<div class="control-group">
															<label for="payment_date" class="control-label">Date <span class="required">*</span></label>
															<div class="controls">
															  <input type="text" name="payment_date" value="<?=$record->payment_date?>" id="payment_date" class="span10 m-wrap validate[required] new_format">
															</div>
													</div> 
													<div class="control-group">
															<label for="total_amount" class="control-label">Amount Paid <span class="required">*</span></label>
															<div class="controls">
															  <input type="text" class="span10 m-wrap validate[required]" id="total_amount"  value="<?=$record->total_amount?>" name="total_amount">
															  <input type='hidden' name='prev_amount' value='<?=$record->total_amount?>'/>	
															</div>
													</div> 
															
													<div class="control-group">
																<label for="remarks" class="control-label">Remarks</label>
																<div class="controls">
																  <textarea name="remarks" class="span10 m-wrap"><?=$record->remarks?></textarea>
																</div>
													</div>	
											</div>
										</div>	
                                            <div class="form-actions">
													 <input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" />
													 <input type="hidden" name="id" value="<?=$record->id?>" tabindex="7" />
													 <input type="hidden" name="session_id" value="<?=$session_id?>" tabindex="7" />
													 <input type="hidden" name="ct_sect" value="<?=$ct_sect?>" tabindex="7" />
													 <input type="hidden" name="section" value="<?=$ct_sect?>" tabindex="7" />
													 <input type="hidden" name="interval_id" value="<?=$interval_id?>" tabindex="7" />
													 <input type="hidden" name="student_id" value="<?=$student_id?>" tabindex="7" /> 
													 <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                            </div>
                              </div>
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

	
	<script>
		jQuery(document).ready(function() {    
            $(".check_valid").live("click",function(){
             // Validation engine
                    $(".validation").validationEngine({
                         promptPosition : "topLeft"
                        
                    });
          	});  
			
            $(".remove_field").live("click",function(){            
								var row_id=$(this).attr("field_id");
								var total=$('#make_total').html();
								total=total.replace(",","");
								var ct_val=$('.row-'+row_id).find('td input:text').val();
								ct_val=ct_val.replace(",","");

								$(this).removeClass('icon-remove');
								$(this).removeClass('remove_field');
								$(this).addClass('icon-plus');
								$(this).addClass('add_field');
								$('.row-'+row_id).addClass('removed');
								$('.row-'+row_id).find('td input:text').attr('disabled', true);
								$('.row-'+row_id).find('.head').attr('disabled', true);
								
								var new_val=total-ct_val;
								$('#make_total').html(new_val);	
								$('#amount_val').val(new_val);								
          	}); 
            $(".add_field").live("click",function(){            
								var row_id=$(this).attr("field_id");
								var total=$('#make_total').html();
								total=total.replace(",","");
								var ct_val=$('.row-'+row_id).find('td input:text').val();								
								ct_val=ct_val.replace(",","");							
								
								$(this).removeClass('icon-plus');
								$(this).removeClass('add_field');
								$(this).addClass('icon-remove');
								$(this).addClass('remove_field');
								$('.row-'+row_id).removeClass('removed');
								$('.row-'+row_id).find('td input:text').removeAttr('disabled');
								$('.row-'+row_id).find('.head').removeAttr('disabled');
								
								var new_val=parseInt(ct_val) + parseInt(total);
								$('#make_total').html(new_val);	
								$('#amount_val').val(new_val);									
          	}); 	
            $(".dynamic_amount").live("change",function(){            
								var thisval=$(this).val();
								
								thisval=thisval.replace(",","");
								if(!isNaN(thisval)) { } else { return false;}
								var thisPval=$(this).attr('p_val');
								thisPval=thisPval.replace(",","");
								
								
								var total=$('#make_total').html();
								total=total.replace(",","");
								new_val=parseInt(total) - parseInt(thisPval);
								
								new_val=parseInt(new_val) + parseInt(thisval);
								$(this).removeAttr('p_val');
								$(this).attr('p_val',thisval);
								
								$('#make_total').html(new_val);	
								$('#amount_val').val(new_val);								
								
          	}); 			
				
		});
		
	</script>	
 
 