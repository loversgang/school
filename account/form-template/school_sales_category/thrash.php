<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Circulars
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <i class="icon-user"></i>
                <li><a href="<?php echo make_admin_url('circulars', 'list', 'list') ?>">List Circulars</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>Thrash Circulars</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>

    <div class="clearfix"></div>
    <div style="clear:both;"></div>	
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-file-text"></i>Manage Thrash</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('category', 'list', 'list'); ?>" method="post" id="form_data" name="form_data" >	
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>                                                                                      
                                    <th class="hidden-480">Sr. No</th>																								
                                    <th style='width:60%;' class="hidden-480">Title</th>												
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <?php foreach ($lists as $key => $list) { ?>
                                <tr class="odd gradeX">
                                    <td class="hidden-480"><?php echo $key + 1 ?></td>												
                                    <td class="hidden-480"><?php echo $list['circular_title'] ?></td>                                                										
                                    <td style='text-align:right;'>
                                        <a class="btn mini green icn-only tooltips" href="<?php echo make_admin_url('circulars', 'restore', 'restore', '&id=' . $list['id']) ?>" title="click here to restore this record"><i class="icon-edit icon-white"></i></a>
                                        <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('circulars', 'delete_permanent', 'delete_permanent', '&id=' . $list['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="click here to permanently delete this record"><i class="icon-remove icon-white"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>                          
                        </table>
                    </form>    
                </div>
            </div>                                                
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



