<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Product Category
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>List Product Category</li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>
    <div class="clearfix"></div>
    <div style="clear:both;"></div>	
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-file-text"></i>Manage Product Category</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('category', 'list', 'list'); ?>" method="post" id="form_data" name="form_data" >	
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>                                                                                      <th>Sr. No</th>					<th>Title</th>					<th>Action</th>

                                </tr>
                            </thead>
                            <?php foreach ($lists as $key => $list) { ?>
                                <tr class="odd gradeX">
                                    <td><?php echo $key + 1 ?></td>					<td><?php echo $list['title'] ?></td>                                                           <td>
                                        <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('school_sales_category', 'update', 'update', 'id=' . $list['id']) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                        <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('school_sales_category', 'delete', 'delete', '&id=' . $list['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>                          
                        </table>
                    </form>    
                </div>
            </div>                                                
        </div>
    </div>
    <div class="clearfix"></div>
</div>