<?

include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');

if($_POST):
isset($_POST['session_id'])?$session_id=$_POST['session_id']:$session_id='';
isset($_POST['section'])?$section=$_POST['section']:$section='';

	
	#Get Section Students
	$QueryOb=new examination();
	$records=$QueryOb->listAllSessionSectionExam($session_id,$section);			
?>			
		<table class="table table-striped table-bordered table-hover">
			<thead>			
				<tr>
						<th style='text-align:center' class="hidden-480">On Date</th>
						<th>Examination Title</th>
						<th class="hidden-480">Teacher Name</th>
						<th class="hidden-480">Subject Name</th>
						<th style='text-align:center' class="hidden-480 sorting_disabled"></th>
				</tr>
			</thead>
			<tbody>
				<? if(!empty($records)):				
					$sr=1;foreach($records as $key=>$object): ?>
							<tr>	
								<td style='text-align:center' class="hidden-480"><?=$object->date?></td>
								<td><?=$object->title?></td>
								<td class="hidden-480"><?=$object->staff_title." ".$object->first_name." ".$object->last_name?></td>
								<td class="hidden-480"><?=$object->subject?></td>
								<td style='text-align:center' class="hidden-480 sorting_disabled"><a class="btn red icn-only tooltips mini" href="<?php echo make_admin_url('exam', 'view', 'view', 'id='.$object->id)?>" title="click here to see the Result">Result</a> </td>
							</tr>	
						<? $sr++; endforeach;?>
				<? else:?>
					<tr><td colspan='5'>Sorry, No Record Found..!</td></tr>
			<? endif;?>																			
			</tbody>
		</table>
<?		
endif;
?>