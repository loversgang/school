<?

include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');

if($_POST):
isset($_POST['session_id'])?$session_id=$_POST['session_id']:$session_id='';
isset($_POST['section'])?$section=$_POST['section']:$section='';
isset($_POST['month'])?$month=$_POST['month']:$month='';
isset($_POST['year'])?$year=$_POST['year']:$year='';		

	if(!empty($month)): 
		$pos = strrpos($month, "-");
		if ($pos == true): 
		   $m_y=explode('-',$month);
		   $month=$m_y['0'];
		   $year=$m_y['1'];
		endif;   
	endif;

	if(!empty($month) && !empty($year)): 
			$first_date_of_month=$year."-".$month."-01";
			$last_date_of_month=date("Y-m-d",strtotime("+1 month -1 second",strtotime(date($year."-".$month."-1"))));
			
			$total_sunday=getTotalSunday($first_date_of_month,$last_date_of_month); 
			$total_month_days=(date('d',strtotime($last_date_of_month))); 
			$working_days=$total_month_days-$total_sunday;
	endif;

	#Get Section Students
	$QueryOb=new studentSession();
	$records=$QueryOb->sectionSessionStudents($session_id,$section);
		
?>			
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th class="hidden-480">Reg ID</th>
					<th class="hidden-480" >Roll No.</th>
					<th class="hidden-480" >Name</th>
					<th class="hidden-480" >Father Name</th>
					<? foreach($Attendance_array as $key=>$value):?>
						<th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='<?=$key?>'>&nbsp;<?=$key?></font></th>
					<? endforeach;?>
					<th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='NM'>&nbsp;N M</font></th>
					<th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='W'>&nbsp;W D</font></th>																					
				</tr>
			</thead>
			<tbody>
				<? if(!empty($records)):				
					$sr=1;foreach($records as $sk=>$stu): $checked='';																			
					$Q_obj= new attendance();
					$r_atten=$Q_obj->checkSessionSectionMonthYearAttendace($session_id,$section,$stu->id,$month,$year);
					$t_attend=array_sum($r_atten); ?>																			
				
					<tr class="odd gradeX">
						<td class='main_title'><?=$stu->reg_id?></td>
						<td class='main_title' style='text-align:center;'><?=$stu->roll_no?></td>
						<td class='main_title'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
						<td class='main_title'><?=ucfirst($stu->father_name)?></td>
						<td class="hidden-480 sorting_disabled" style='text-align:center;'><? if(!empty($r_atten) && array_key_exists('P',$r_atten)): echo $r_atten['P']; else: echo '0'; endif;?></td>
						<td class="hidden-480 sorting_disabled" style='text-align:center;'><? if(!empty($r_atten) && array_key_exists('A',$r_atten)): echo $r_atten['A']; else: echo '0';endif;?></td>
						<td class="hidden-480 sorting_disabled" style='text-align:center;'><? if(!empty($r_atten) && array_key_exists('L',$r_atten)): echo $r_atten['L']; else: echo '0';endif;?></td>
						<td class="hidden-480 sorting_disabled" style='text-align:center;'><? if(!empty($r_atten) && array_key_exists('H',$r_atten)): echo $r_atten['H']; else: echo '0';endif;?></td>
						<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=($working_days-$t_attend)?></td>
						<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$working_days?></td>																				
					</tr>
				<? endforeach;?>									
			<? else:?>
				<tr><td colspan='10'>Sorry, No Record Found..!</td></tr>
			<? endif;?>																			
			</tbody>
		</table>
<?		
endif;
?>