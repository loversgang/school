<?

include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');

if($_POST):
isset($_POST['session_id'])?$session_id=$_POST['session_id']:$session_id='';
isset($_POST['section'])?$section=$_POST['section']:$section='';
isset($_POST['interval_id'])?$interval_id=$_POST['interval_id']:$interval_id='0';
	

	#Get Section Students
	$QueryOb=new studentSession();
	$records=$QueryOb->sectionSessionStudents($session_id,$section);	
		
?>			
		<table class="table table-striped table-bordered table-hover">
			<thead>			
				<tr>
					<th class="hidden-480">Reg ID</th>
					<th class="hidden-480" style='text-align:center;' >Roll No.</th>
					<th class="hidden-480" >Name</th>
					<th class="hidden-480" >Father Name</th>
						<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total</th>
						<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Paid</th>
				</tr>
			</thead>
			<tbody>
				<? if(!empty($records)):				
					$sr=1;foreach($records as $sk=>$stu): $checked='';
							#Get interval detail
							$SessIntOb= new studentSessionInterval();
							$student_interval_detail=$SessIntOb->getDetail($session_id,$section,$stu->id,$interval_id); 
							
							#get Pending Fee for previous month
							$SessObPre= new studentSessionInterval();
							
							if(!empty($pending_fee)): $pending=$pending_fee; else: $pending='0'; endif; ?>
							
							<tr class="odd gradeX">
								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->reg_id?></td>
								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:center;'><?=$stu->roll_no?></td>
								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->father_name)?></td>
								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:right;'><? if(is_object($student_interval_detail)): echo CURRENCY_SYMBOL." ".number_format($student_interval_detail->total+$pending,2); endif;?></td>
								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:right;'><? if(is_object($student_interval_detail)): echo CURRENCY_SYMBOL." ".number_format($student_interval_detail->paid,2); endif;?></td>
								</tr>
						<? $sr++; endforeach;?>
			<? else:?>
				<tr><td colspan='10'>Sorry, No Record Found..!</td></tr>
			<? endif;?>																			
			</tbody>
		</table>
<?		
endif;
?>