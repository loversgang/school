<?

include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');

if($_POST):
isset($_POST['session_id'])?$session_id=$_POST['session_id']:$session_id='';
isset($_POST['section'])?$section=$_POST['section']:$section='';
		

		#Get Section Students
		$QueryOb=new studentSession();
		$AllStu=$QueryOb->sectionSessionStudents($session_id,$section);
		
?>			
		<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th class="hidden-480">Reg ID</th>
					<th class="hidden-480">Roll No</th>
					<th class="hidden-480">Name</th>
					<th class="hidden-480">Father Name</th>
					<th class="hidden-480">Gender</th>												
					<th class="hidden-480">Section</th>											
				</tr>
			</thead>
			<tbody>
			<? if(!empty($AllStu)):?>
			<?php $sr=1;foreach($AllStu as $sk=>$stu): $checked=''; ?>
				<tr class="odd gradeX" >
					<td><?=$stu->reg_id?></td>
					<td><?=$stu->roll_no?></td>
					<td><?=$stu->first_name." ".$stu->last_name?></td>
					<td class="hidden-480"><?=$stu->father_name?></td>
					<td class="center hidden-480"><?=$stu->sex?></td>
					<td class="center hidden-480"><?=$stu->section?></td>											
				</tr>
			<? endforeach;?>
			<? else:?>
				<tr><td colspan='6'>Sorry, No Record Found..!</td></tr>
			<? endif;?>																			
			</tbody>
		</table>
<?		
endif;
?>