<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/vehicleClass.php');

if($_POST):
isset($_POST['session_id'])?$session_id=$_POST['session_id']:$session_id='';
isset($_POST['section'])?$section=$_POST['section']:$section='';

	
	#Get Section Students
	$QueryOb=new vehicleStudent();
	$records=$QueryOb->sessionSectionVehicles($session_id,$section);			
?>			
		<table class="table table-striped table-bordered table-hover">
			<thead>			
				<tr>
						<th style='text-align:center' class="hidden-480">Model</th>
						<th class="hidden-480">Make</th>
						<th class="hidden-480">Color</th>
						<th>Vehicle Number</th>
						<th class="hidden-480">Driver Name</th>
						<th style='text-align:center' class="hidden-480 sorting_disabled"></th>
				</tr>
			</thead>
			<tbody>
				<? if(!empty($records)):				
					$sr=1;foreach($records as $key=>$object): ?>
							<tr>	
								<td style='text-align:center' class="hidden-480"><?=$object->model?></td>
								<td><?=$object->make?></td>
								<td class="hidden-480"><?=$object->color?></td>
								<td class="hidden-480"><?=$object->vehicle_number?></td>
								<td class="hidden-480"><?=$object->title." ".ucfirst($object->first_name)." ".$object->last_name?></td>
								<td style='text-align:center' class="hidden-480 sorting_disabled"><a class="btn red icn-only tooltips mini" href="<?php echo make_admin_url('vehicles', 'view', 'view', 'id='.$object->id)?>" title="click here to see vehicle student">Students</a> </td>
							</tr>	
						<? $sr++; endforeach;?>
				<? else:?>
					<tr><td colspan='6'>Sorry, No Record Found..!</td></tr>
			<? endif;?>																			
			</tbody>
		</table>
<?		
endif;
?>