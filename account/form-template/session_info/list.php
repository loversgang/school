
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Session Information
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('session', 'list', 'list');?>">List Sessions</a>
                                         <i class="icon-angle-right"></i>                                       
                                    </li>
                                    <li class="last">
                                        Session Information
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
					
            </div>
			<h3 id="title_show" class="block alert alert-info"><?=$session->title?></h3>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
						<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div> 		            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
			  
			<div class="row-fluid">
					<div class="span12">
						<div id="form_wizard_1" class="portlet box green">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-certificate"></i> Session Information </span>
								</div>
								<div class="tools hidden-phone">
									
								</div>
							</div>
							
							<div class="portlet-body form">
									<div class="form-wizard">
										<div class="tab-content">
											
											<!-- First Step  session Detail-->
											<div id="tab1" class="tab-pane <?=($type=='info')?'active':'';?>">
												<div class="row-fluid">													
													<ul class="unstyled profile-nav span3" id='left_user_info'>
														<li><a class='<?=($type=='info')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=info&id='.$id)?>">Basic Information</a></li>
														<li><a class='<?=($type=='student')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=student&id='.$id)?>">Student Information</a></li>
														<li><a class='<?=($type=='attendance')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=attendance&id='.$id)?>">Student Attendance</a></li>
														<li><a class='<?=($type=='fee')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=fee&id='.$id)?>">Student Fee Information</a></li>
														<li><a class='<?=($type=='exam')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=exam&id='.$id)?>">Session Examination</a></li>
														<li><a class='<?=($type=='vehicle')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=vehicle&id='.$id)?>">Vehicle Information</a></li>
													</ul>	
													<div class="span9">
															<!-- Basic Info -->
															<div class="row-fluid">
															<div class="portlet box grey">
																<div class="portlet-title">
																<div class="caption"><i class="icon-reorder"></i>Session Information</div>
																<div class="actions">
																	<a class="btn blue mini tooltips" href="<?=make_admin_url('session','update','update&id='.$id)?>#s_d" title='Click here to edit session information'><i class="icon-pencil"></i> Edit</a>
																</div>
																</div>
																<div class="portlet-body">															
																<ul class="unstyled span5" id='user_info'>
																	<li><span>Title :</span> <?=$session->title?></li>
																	
																	<li><span>Start Date :</span> <?=$session->start_date?></li>																	
																	<li><span>Total Section :</span> <?=$session->total_sections_allowed?></li>										
																</ul>
																<ul class="unstyled span5" id='user_info'>	
																	<li><span></span></li>
																	<li><span>End Date :</span> <?=$session->end_date?></li>
																	<li><span>Maximum students in a section :</span><?=$session->max_sections_students?></li>
																</ul>
																</div>
																<div class="clearfix"></div>
															</div>	
															</div>																
															<div class="clearfix"></div>
															
															<!-- Subject -->
															<div class="row-fluid">
																<div class="portlet box grey">
																<div class="portlet-title">
																	<div class="caption"><i class="icon-reorder"></i>Section Information</div>
																<div class="actions">
																	<a class="btn blue mini tooltips" href="<?=make_admin_url('session','session_sec','session_sec&id='.$id)?>#sf_d" title='Click here to edit session information'><i class="icon-pencil"></i> Edit</a>
																</div>																
																</div>
																
																<div class="portlet-body">															
																	<div class="span3">
																	</div>
																	<div class="span9">
																		<table class="table table-striped table-bordered table-advance table-hover">
																		<thead>
																		<tr><th>Section</th><th>In-charge Teacher</th>
																		</tr></thead>
																					<tbody>
																					<? if(!empty($AllSect)):
																						foreach($AllSect as $sec_key=>$sec_val):?>
																							<tr><td><?=ucfirst($sec_val->section)?></td><td><?=ucfirst($sec_val->first_name).' '.$sec_val->last_name?></td></tr>
																					<?  endforeach;?>
																						<?  else:?>
																						<tr><td colspan='2'>Sorry, No Record Found..!</td></tr>
																					<? endif;?>
																					</tbody>
																		</table>
																	</div>
																	<div class="clearfix"></div>
																</div>
																</div>
															</div>															
															<div class="clearfix"></div>							
															
															

															<!-- Subjects  Info -->
															<div class="row-fluid">
																<div class="portlet box grey">
																<div class="portlet-title">
																	<div class="caption"><i class="icon-reorder"></i>Subjects Information</div>
																	<div class="actions">
																		<a class="btn blue mini tooltips" href="<?=make_admin_url('session','update','update&id='.$id)?>#sb_d" title='Click here to edit session information'><i class="icon-pencil"></i> Edit</a>
																	</div>																
																</div>
																																
																<div class="portlet-body">
																	<div class="span6">
																		<div class="span6" style='padding-left:15px;'><strong>Compulsory Subjects</strong></div>
																		<ul class="unstyled span6" id='user_info'>
																			<? if(!empty($compulsory_subjects)):
																					foreach($compulsory_subjects as $c_key=>$c_val):?>
																						<li><span><?=ucfirst($c_val->name)?> </span> </li>
																			<? 		endforeach;
																				else:?>
																				<li><span>Sorry, No Record Found..! </span> </li>
																			<? endif;?>																				
																		</ul>
																	</div>
																	
																	<div class="span6">
																		<div class="span6" style='padding-left:15px;'><strong>Elective Subjects</strong></div>
																		<ul class="unstyled span6" id='user_info'>
																			<? if(!empty($elective_subjects)):
																					foreach($elective_subjects as $e_key=>$e_val):?>
																						<li><span><?=ucfirst($e_val->name)?> </span> </li>
																			<? 		endforeach;
																				else:?>
																				<li><span>Sorry, No Record Found..! </span> </li>
																			<? endif;?>	
																		</ul>
																	</div>
																	</div>
																	<div class="clearfix"></div>																	
																</div>
															</div>															
															<div class="clearfix"></div>	

															<!-- Fee  Info -->
															<div class="row-fluid">
																<div class="portlet box grey">
																<div class="portlet-title">
																	<div class="caption"><i class="icon-reorder"></i>Fee Heads Information</div>
																<div class="actions">
																	<a class="btn blue mini tooltips" href="<?=make_admin_url('session','update','update&id='.$id)?>#se_d" title='Click here to edit session information'><i class="icon-pencil"></i> Edit</a>
																</div>															
																</div>
																
																<div class="portlet-body">															
																	<div class="span3">
																	</div>
																	<div class="span9">
																		<table class="table table-striped table-bordered table-advance table-hover">
																		<thead>
																		<tr><th>Fee Heads Name</th><th>Amount (₹)</th>
																		</tr></thead>
																					<tbody>
																					<? if(!empty($pay_heads)):
																						foreach($pay_heads as $pay_key=>$pay_val):?>
																							<tr><td><?=ucfirst($pay_val->title)?> &nbsp; (<?=$pay_val->type?>)</td><td><?=CURRENCY_SYMBOL.' '.number_format($pay_val->amount,2)?></td></tr>
																					<?  endforeach;?>
																						<tr><td>Absent Fine (Per Day)</td><td><?=CURRENCY_SYMBOL.' '.number_format($session->absent_fine,2)?></td></tr>
																						<tr><td>Late Fee Days</td><td><?=$session->late_fee_days?></td></tr>
																					<?  else:?>
																						<tr><td colspan='2'>Sorry, No Record Found..!</td></tr>
																					<? endif;?>
																					</tbody>
																		</table>
																	</div>
																	<div class="clearfix"></div>
																</div>
																</div>
															</div>															
															<div class="clearfix"></div>
													</div>		
													</div>
											</div>
											
											
											<!-- Second Step  student info-->
											<div id="tab2" class="tab-pane <?=($type=='student')?'active':'';?>">
												<div class="row-fluid">													
													<ul class="unstyled profile-nav span3" id='left_user_info'>
														<li><a class='<?=($type=='info')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=info&id='.$id)?>">Basic Information</a></li>
														<li><a class='<?=($type=='student')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=student&id='.$id)?>">Student Information</a></li>
														<li><a class='<?=($type=='attendance')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=attendance&id='.$id)?>">Student Attendance</a></li>
														<li><a class='<?=($type=='fee')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=fee&id='.$id)?>">Student Fee Information</a></li>
														<li><a class='<?=($type=='exam')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=exam&id='.$id)?>">Session Examination</a></li>
														<li><a class='<?=($type=='vehicle')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=vehicle&id='.$id)?>">Vehicle Information</a></li>
													</ul>	
													<div class="span9">
															<!-- student Info -->
															<div class="row-fluid">
															<div class="portlet box grey">
																<div class="portlet-title">
																	<div class="caption"><i class="icon-user"></i>Session Students</div>
																	<div class="actions">
																		<a class="btn blue mini tooltips" href="<?=make_admin_url('session','view','view&id='.$id)?>" title='Click here to edit session students'><i class="icon-pencil"></i> Edit</a>
																	</div>																
																</div>
																<div class="portlet-body">	
																<div class="span12">
																	<div class="control-group alert alert-success" style="margin-left:0px;padding-right:0px;">
																		<div class="span5">
																			<label class="control-label">Select Section</label>
																			<div class="controls">
																			<select name="section" data-placeholder="Select Session Section" class="select2_category section_filter span11">
																				<option value="">Select Section</option>
																				<? if(!empty($all_section)):?>
																					<? foreach($all_section as $sec_key=>$sec_val):?>
																						<option value="<?=$sec_val->section?>" <?=($sec_val->section==$ct_sect)?'selected':''?>><?=$sec_val->section?></option>
																					<? endforeach;
																					endif;?>																				
																			</select>
																			</div>
																		</div>
																		<div class="clearfix"></div>
																	</div>
																</div>	
																	<div class='filter_record'>
																		<table class="table table-striped table-bordered table-hover">
																			<thead>
																				<tr>
																					<th class="hidden-480">Reg ID</th>
																					<th class="hidden-480">Roll No</th>
																					<th class="hidden-480">Name</th>
																					<th class="hidden-480">Father Name</th>
																					<th class="hidden-480">Gender</th>												
																					<th class="hidden-480">Section</th>											
																				</tr>
																			</thead>
																			<tbody>
																			<? if(!empty($AllStu)):?>
																			<?php $sr=1;foreach($AllStu as $sk=>$stu): $checked=''; ?>
																				<tr class="odd gradeX">
																					<td><?=$stu->reg_id?></td>
																					<td><?=$stu->roll_no?></td>
																					<td><?=$stu->first_name." ".$stu->last_name?></td>
																					<td class="hidden-480"><?=$stu->father_name?></td>
																					<td class="center hidden-480"><?=$stu->sex?></td>
																					<td class="center hidden-480"><?=$stu->section?></td>											
																				</tr>
																			<? endforeach;?>
																			<? else:?>
																				<tr><td colspan='6'>Sorry, No Record Found..!</td></tr>
																			<? endif;?>																			
																			</tbody>
																		</table>
																	</div>
																</div>
																<div class="clearfix"></div>
															</div>	
															</div>																
															<div class="clearfix"></div>															
													</div>		
													</div>
											</div>
											
											<!-- Third Step student info-->
											<div id="tab3" class="tab-pane <?=($type=='attendance')?'active':'';?>">
												<div class="row-fluid">													
													<ul class="unstyled profile-nav span3" id='left_user_info'>
														<li><a class='<?=($type=='info')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=info&id='.$id)?>">Basic Information</a></li>
														<li><a class='<?=($type=='student')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=student&id='.$id)?>">Student Information</a></li>
														<li><a class='<?=($type=='attendance')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=attendance&id='.$id)?>">Student Attendance</a></li>
														<li><a class='<?=($type=='fee')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=fee&id='.$id)?>">Student Fee Information</a></li>
														<li><a class='<?=($type=='exam')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=exam&id='.$id)?>">Session Examination</a></li>
														<li><a class='<?=($type=='vehicle')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=vehicle&id='.$id)?>">Vehicle Information</a></li>
													</ul>	
													<div class="span9">
															<!-- student Info -->
															<div class="row-fluid">
															<div class="portlet box grey">
																<div class="portlet-title">
																	<div class="caption"><i class="icon-calendar"></i>Session Attendance</div>
																	<div class="actions">
																		<a class="btn blue mini tooltips" href="<?=make_admin_url('attendance','list','list&select=multiple&type=session&')?>" title='Click here to view session student attendance'><i class="icon-search"></i> View</a>
																	</div>																	
																</div>
																<div class="portlet-body">	

																<div class="span12">
																	<div class="control-group alert alert-success" style="margin-left:0px;padding-right:0px;">
																		<div class="span12">
																			<div class='span9'>
																			<label class="control-label">Select Section</label>
																			<div class="controls">
																			<select name="section" data-placeholder="Select Session Section" class="select2_category section_attendance_filter span4" id='attendance_section'>
																				<option value="">Select Section</option>
																				<? if(!empty($all_section)):?>
																					<? foreach($all_section as $sec_key=>$sec_val):?>
																						<option value="<?=$sec_val->section?>" <?=($sec_val->section==$ct_sect)?'selected':''?>><?=$sec_val->section?></option>
																					<? endforeach;
																					endif;?>																				
																			</select>
																			<select name='month' class='span4 section_attendance_filter' id='attendance_month'>
																				<?=getMonths($session->start_date,$session->end_date);?>
																			</select>								
																			</div>
																			</div>
																			<div class='span3'>
																				<!--
																				<? foreach($Attendance_array as $key=>$value):?>
																					<div style='line-height:35px;'><font class='<?=$key?>'>&nbsp;<?=$key?></font><strong>&nbsp;=&nbsp;</strong><font class='<?=$key?>'>&nbsp;<?=$value?></font></div>
																				<? endforeach;?>
																				-->
																				<div style='line-height:35px;'><font class='NM'>&nbsp;N M</font><strong>&nbsp;=&nbsp;</strong><font class='NM'>&nbsp;Not Marked</font></div>
																				<div style='line-height:35px;'><font class='W'>&nbsp;W D</font><strong>&nbsp;=&nbsp;</strong><font class='W'>&nbsp;Working Days</font></div>
																			<div class="clearfix"></div>
																			</div>
																		</div>
																		<div class="clearfix"></div>
																	</div>
																</div>	
																	<div class='filter_record'>
																		<table class="table table-striped table-bordered table-hover">
																			<thead>
																				<tr>
																					<th class="hidden-480">Reg ID</th>
																					<th class="hidden-480" >Roll No.</th>
																					<th class="hidden-480" >Name</th>
																					<th class="hidden-480" >Father Name</th>
																					<? foreach($Attendance_array as $key=>$value):?>
																						<th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='<?=$key?>'>&nbsp;<?=$key?></font></th>
																					<? endforeach;?>
																					<th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='NM'>&nbsp;N M</font></th>
																					<th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='W'>&nbsp;W D</font></th>																					
																				</tr>
																			</thead>
																			<tbody>
																				<? if(!empty($records)):				
																					$sr=1;foreach($records as $sk=>$stu): $checked='';																			
																					$Q_obj= new attendance();
																					$r_atten=$Q_obj->checkSessionSectionMonthYearAttendace($id,$ct_sect,$stu->id,$month,$year);
																					$t_attend=array_sum($r_atten); 
																					
																					$M_obj= new schoolHolidays();
																					$M_holiday=$M_obj->checkMonthlyHoliday($school->id,date('m'),$year);	
													
																					?>																			
																				
																					<tr class="odd gradeX">
																						<td class='main_title'><?=$stu->reg_id?></td>
																						<td class='main_title' style='text-align:center;'><?=$stu->roll_no?></td>
																						<td class='main_title'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
																						<td class='main_title'><?=ucfirst($stu->father_name)?></td>
																						<td class="hidden-480 sorting_disabled" style='text-align:center;'><? if(!empty($r_atten) && array_key_exists('P',$r_atten)): echo $r_atten['P']; else: echo '0'; endif;?></td>
																						<td class="hidden-480 sorting_disabled" style='text-align:center;'><? if(!empty($r_atten) && array_key_exists('A',$r_atten)): echo $r_atten['A']; else: echo '0';endif;?></td>
																						<td class="hidden-480 sorting_disabled" style='text-align:center;'><? if(!empty($r_atten) && array_key_exists('L',$r_atten)): echo $r_atten['L']; else: echo '0';endif;?></td>
																						<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=($working_days-$t_attend-$M_holiday)?></td>
																						<td class="hidden-480 sorting_disabled" style='text-align:center;'><?=$working_days-$M_holiday?></td>																				
																					</tr>
																				<? endforeach;?>									
																			<? else:?>
																				<tr><td colspan='10'>Sorry, No Record Found..!</td></tr>
																			<? endif;?>																			
																			</tbody>
																		</table>
																	</div>
																</div>
																<div class="clearfix"></div>
															</div>	
															</div>																
															<div class="clearfix"></div>															
													</div>		
													</div>
											</div>
											
											
											
											<!-- Forth Step student fee info-->
											<div id="tab4" class="tab-pane <?=($type=='fee')?'active':'';?>">
												<div class="row-fluid">													
													<ul class="unstyled profile-nav span3" id='left_user_info'>
														<li><a class='<?=($type=='info')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=info&id='.$id)?>">Basic Information</a></li>
														<li><a class='<?=($type=='student')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=student&id='.$id)?>">Student Information</a></li>
														<li><a class='<?=($type=='attendance')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=attendance&id='.$id)?>">Student Attendance</a></li>
														<li><a class='<?=($type=='fee')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=fee&id='.$id)?>">Student Fee Information</a></li>
														<li><a class='<?=($type=='exam')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=exam&id='.$id)?>">Session Examination</a></li>
														<li><a class='<?=($type=='vehicle')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=vehicle&id='.$id)?>">Vehicle Information</a></li>
													</ul>	
													<div class="span9">
															<!-- student Info -->
															<div class="row-fluid">
															<div class="portlet box grey">
																<div class="portlet-title">
																	<div class="caption"><i class="icon-rupee"></i>Session Fee Information</div>
																	<div class="actions">
																		<a class="btn blue mini tooltips" href="<?=make_admin_url('fee','list','list')?>" title='Click here to view session student fee'><i class="icon-search"></i> View</a>
																	</div>																	
																</div>
																<div class="portlet-body">	

																<div class="span12">
																	<div class="control-group alert alert-success" style="margin-left:0px;padding-right:0px;">
																		<div class="span9">
																			<div class='span4'>
																			<label class="control-label">Select Section</label>
																			<div class="controls span10">
																			<select name="section" data-placeholder="Select Session Section" class="select2_category section_fee_filter span12" id='fee_section'>
																				<option value="">Select Section </option>
																				<? if(!empty($all_section)):?>
																					<? foreach($all_section as $sec_key=>$sec_val):?>
																						<option value="<?=$sec_val->section?>" <?=($sec_val->section==$ct_sect)?'selected':''?>><?=$sec_val->section?></option>
																					<? endforeach;
																					endif;?>																				
																			</select>
																			</div>
																			</div>
																			<div class='span8'>
																			<label class="control-label">Payment Interval</label>
																			<div class="controls span10" style='margin-left:0px;'>																
																			<? if(!empty($interval)):?>
																				<select name='interval_id' class='span12 section_fee_filter' id='interval_id'>
																					<?	foreach($interval as $i_k=>$i_v):
																							if((strtotime($current_date)>=strtotime($i_v->from_date)) && (strtotime($current_date)<=strtotime($i_v->to_date))): $current='1'; endif; ?>
																							<option value='<?=$i_v->id?>' <? if($interval_id==$i_v->id): echo 'selected'; endif;?> >
																								<?=date('d M,Y',strtotime($i_v->from_date))."  to  ".date('d M,Y',strtotime($i_v->to_date))?>
																							<? if($current=='1'): break; 																								
																							endif;?>
																							</option>
																					<? 	endforeach; ?>
																				</select>
																			<? else:?>	
																					<input type='text' value='Sorry, No Interval Found..!'/>
																					<input type='hidden' name='interval_id' value='0'/>
																			<? endif;?>
																			</div>							
																			</div>
																		</div>
																		<div class="clearfix"></div>
																	</div>
																</div>			
																	<div class='filter_record'>	
																		<table class="table table-striped table-bordered table-hover">
																			<thead>			
																				<tr>
																					<th class="hidden-480">Reg ID</th>
																					<th class="hidden-480" style='text-align:center;' >Roll No.</th>
																					<th class="hidden-480" >Name</th>
																					<th class="hidden-480" >Father Name</th>
																						<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total Session Fee</th>
																						<th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:right'>Total Paid</th>																						
																				</tr>
																			</thead>
																			<tbody>
																				<? if(!empty($records)):				
																					$sr=1;foreach($records as $sk=>$stu): $checked='';
																							#Get interval detail
																							$SessIntOb= new studentSessionInterval();
																							$student_interval_detail=$SessIntOb->getDetail($id,$ct_sect,$stu->id,$interval_id); 
																							
																							#get Pending Fee for previous month
																							$SessObPre= new studentSessionInterval();
																							$pending_fee=$SessObPre->getPreviousBalanceFee($id,$ct_sect,$stu->id,$interval_id);
																							if(!empty($pending_fee)): $pending=$pending_fee; else: $pending='0'; endif; ?>
																												
																						
																							<tr class="odd gradeX">
																								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->reg_id?></td>
																								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:center;'><?=$stu->roll_no?></td>
																								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
																								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->father_name)?></td>
																								
																								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:right;'><? if(is_object($student_interval_detail)): echo CURRENCY_SYMBOL." ".number_format($student_interval_detail->total+$pending,2); endif;?></td>
																								<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;text-align:right;'><? if(is_object($student_interval_detail)): echo CURRENCY_SYMBOL." ".number_format($student_interval_detail->paid,2); endif;?></td>
																							</tr>
																						<? $sr++; endforeach;?>
																			<? else:?>
																				<tr><td colspan='10'>Sorry, No Record Found..!</td></tr>
																			<? endif;?>																			
																			</tbody>
																		</table>
																	</div>	
																</div>
																<div class="clearfix"></div>
															</div>	
															</div>																
															<div class="clearfix"></div>															
													</div>		
													</div>
											</div>
											
											
											
											<!-- Fifth Step exam info-->
											<div id="tab5" class="tab-pane <?=($type=='exam')?'active':'';?>">
												<div class="row-fluid">													
													<ul class="unstyled profile-nav span3" id='left_user_info'>
														<li><a class='<?=($type=='info')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=info&id='.$id)?>">Basic Information</a></li>
														<li><a class='<?=($type=='student')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=student&id='.$id)?>">Student Information</a></li>
														<li><a class='<?=($type=='attendance')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=attendance&id='.$id)?>">Student Attendance</a></li>
														<li><a class='<?=($type=='fee')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=fee&id='.$id)?>">Student Fee Information</a></li>
														<li><a class='<?=($type=='exam')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=exam&id='.$id)?>">Session Examination</a></li>
														<li><a class='<?=($type=='vehicle')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=vehicle&id='.$id)?>">Vehicle Information</a></li>
													</ul>	
													<div class="span9">
															<!-- student Info -->
															<div class="row-fluid">
															<div class="portlet box grey">
																<div class="portlet-title">
																<div class="caption"><i class="icon-copy"></i>Session Exam Information</div>
																</div>
																<div class="portlet-body">
																<div class="span12">
																	<div class="control-group alert alert-success" style="margin-left:0px;padding-right:0px;">
																		<div class="span5">
																			<label class="control-label">Select Section</label>
																			<div class="controls">
																				<select name="section" data-placeholder="Select Session Section" class="select2_category exam_filter span11">
																					<option value="">Select Section</option>
																					<? if(!empty($all_section)):?>
																						<? foreach($all_section as $sec_key=>$sec_val):?>
																							<option value="<?=$sec_val->section?>" <?=($sec_val->section==$ct_sect)?'selected':''?>><?=$sec_val->section?></option>
																						<? endforeach;
																						endif;?>																				
																				</select>
																			</div>
																		</div>
																		<div class="clearfix"></div>
																	</div>
																</div>
																	<div class='filter_record'>
																		<table class="table table-striped table-bordered table-hover">
																			<thead>			
																				<tr>
																						<th style='text-align:center' class="hidden-480">On Date</th>
																						<th>Examination Title</th>
																						<th class="hidden-480">Teacher Name</th>
																						<th class="hidden-480">Subject Name</th>
																						<th style='text-align:center' class="hidden-480 sorting_disabled"></th>
																				</tr>
																			</thead>
																			<tbody>
																				<? if(!empty($records)):				
																					$sr=1;foreach($records as $key=>$object): ?>
																							<tr>	
																								<td style='text-align:center' class="hidden-480"><?=$object->date?></td>
																								<td><?=$object->title?></td>
																								<td class="hidden-480"><?=$object->staff_title." ".$object->first_name." ".$object->last_name?></td>
																								<td class="hidden-480"><?=$object->subject?></td>
																								<td style='text-align:center' class="hidden-480 sorting_disabled"><a class="btn red icn-only tooltips mini" href="<?php echo make_admin_url('exam', 'view', 'view', 'id='.$object->id)?>" title="click here to see the Result">Result</a> </td>
																							</tr>	
																						<? $sr++; endforeach;?>
																				<? else:?>
																					<tr><td colspan='5'>Sorry, No Record Found..!</td></tr>
																			<? endif;?>																			
																			</tbody>
																		</table>
																	</div>
																</div>
																<div class="clearfix"></div>
															</div>	
															</div>																
															<div class="clearfix"></div>															
													</div>		
													</div>
											</div>	

											
											
											<!-- sixth Step exam info-->
											<div id="tab6" class="tab-pane <?=($type=='vehicle')?'active':'';?>">
												<div class="row-fluid">													
													<ul class="unstyled profile-nav span3" id='left_user_info'>
														<li><a class='<?=($type=='info')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=info&id='.$id)?>">Basic Information</a></li>
														<li><a class='<?=($type=='student')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=student&id='.$id)?>">Student Information</a></li>
														<li><a class='<?=($type=='attendance')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=attendance&id='.$id)?>">Student Attendance</a></li>
														<li><a class='<?=($type=='fee')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=fee&id='.$id)?>">Student Fee Information</a></li>
														<li><a class='<?=($type=='exam')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=exam&id='.$id)?>">Session Examination</a></li>
														<li><a class='<?=($type=='vehicle')?'selected':'';?>' href="<?=make_admin_url('session_info','list','list&type=vehicle&id='.$id)?>">Vehicle Information</a></li>
													</ul>	
													<div class="span9">
															<!-- student Info -->
															<div class="row-fluid">
															<div class="portlet box grey">
																<div class="portlet-title">
																<div class="caption"><i class="icon-truck"></i>Session Vehicle Information</div>
																</div>
																<div class="portlet-body">	

																<div class="span12">
																	<div class="control-group alert alert-success" style="margin-left:0px;padding-right:0px;">
																		<div class="span5">
																			<label class="control-label">Select Section</label>
																			<div class="controls">
																				<select name="section" data-placeholder="Select Session Section" class="select2_category vehicle_filter span11">
																					<option value="">Select Section</option>
																					<? if(!empty($all_section)):?>
																						<? foreach($all_section as $sec_key=>$sec_val):?>
																							<option value="<?=$sec_val->section?>" <?=($sec_val->section==$ct_sect)?'selected':''?>><?=$sec_val->section?></option>
																						<? endforeach;
																						endif;?>																				
																				</select>
																			</div>
																		</div>
																		<div class="clearfix"></div>
																	</div>
																</div>			
																	<div class='filter_record'>	
																		<table class="table table-striped table-bordered table-hover">
																			<thead>			
																				<tr>
																						<th style='text-align:center' class="hidden-480">Model</th>
																						<th class="hidden-480">Make</th>
																						<th class="hidden-480">Color</th>
																						<th>Vehicle Number</th>
																						<th class="hidden-480">Driver Name</th>
																						<th style='text-align:center' class="hidden-480 sorting_disabled"></th>
																				</tr>
																			</thead>
																			<tbody>
																				<? if(!empty($records)):				
																					$sr=1;foreach($records as $key=>$object): ?>
																							<tr>	
																								<td style='text-align:center' class="hidden-480"><?=$object->model?></td>
																								<td><?=$object->make?></td>
																								<td class="hidden-480"><?=$object->color?></td>
																								<td class="hidden-480"><?=$object->vehicle_number?></td>
																								<td class="hidden-480"><?=$object->title." ".ucfirst($object->first_name)." ".$object->last_name?></td>
																								<td style='text-align:center' class="hidden-480 sorting_disabled"><a class="btn red icn-only tooltips mini" href="<?php echo make_admin_url('vehicles', 'view', 'view', 'id='.$object->id)?>" title="click here to see vehicle student">Students</a> </td>
																							</tr>	
																						<? $sr++; endforeach;?>
																				<? else:?>
																					<tr><td colspan='6'>Sorry, No Record Found..!</td></tr>
																			<? endif;?>																			
																			</tbody>
																		</table>
																	</div>	
																</div>
																<div class="clearfix"></div>
															</div>	
															</div>																
															<div class="clearfix"></div>															
													</div>		
													</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
	
	
<script type="text/javascript">
	$(".section_filter").live("change", function(){
			var section=$(this).val();
			var session_id=<?=$id?>;
			if(section.length > 0){
					   $(".filter_record").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'session_id='+session_id+'&section='+section;						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','section_student','section_student&temp=session_info');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$(".filter_record").html(data);						
						}				
						});				
			}
			else{
				return false;
			}	
	});
	$(".section_attendance_filter").live("change", function(){	
			var section=$('#attendance_section').val();
			var month=$('#attendance_month').val();
			var session_id=<?=$id?>;
			
			if(section.length > 0){
					   $(".filter_record").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'session_id='+session_id+'&section='+section+'&month='+month;						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','section_attendance','section_attendance&temp=session_info');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$(".filter_record").html(data);						
						}				
						});				
			}
			else{
				return false;
			}	
	});	
	$(".section_fee_filter").live("change", function(){	
			var section=$('#fee_section').val();
			var interval_id=$('#interval_id').val();
			var session_id=<?=$id?>;
			
			if(section.length > 0){
					   $(".filter_record").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'session_id='+session_id+'&section='+section+'&interval_id='+interval_id;						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','section_fee','section_fee&temp=session_info');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$(".filter_record").html(data);						
						}				
						});				
			}
			else{
				return false;
			}	
	});	
	
	$(".exam_filter").live("change", function(){
			var section=$(this).val();
			var session_id=<?=$id?>;
			if(section.length > 0){
					   $(".filter_record").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'session_id='+session_id+'&section='+section;						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','section_exam','section_exam&temp=session_info');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$(".filter_record").html(data);						
						}				
						});				
			}
			else{
				return false;
			}	
	});	
	
	$(".vehicle_filter").live("change", function(){
			var section=$(this).val();
			var session_id=<?=$id?>;
			if(section.length > 0){
					   $(".filter_record").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'session_id='+session_id+'&section='+section;						
						$.ajax({ 
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','section_vehicle','section_vehicle&temp=session_info');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$(".filter_record").html(data);						
						}				
						});				
			}
			else{
				return false;
			}	
	});	
	
</script>	