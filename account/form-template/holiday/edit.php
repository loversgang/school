<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Academy Holidays
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>  
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('holiday', 'list', 'list'); ?>">List Holidays</a>
                    <i class="icon-angle-right"></i>                                       
                </li>									
                <li class="last">
                    Edit Academy Holiday
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal validation" action="<?php echo make_admin_url('holiday', 'update', 'update') ?>" method="POST" enctype="multipart/form-data" id="validation">	
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-calendar"></i>Edit Holiday</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="control-group">
                            <label class="control-label" for="title">Title<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="title" value="<?= $object->title ?>" id="title" class="span6 m-wrap validate[required]" />
                            </div>
                        </div>  
                        <div class="control-group">
                            <label class="control-label" for="title">Type</label>
                            <div class="controls">
                                <input type="text" name="type"  value="<?= $object->type ?>" id="type" class="span6 m-wrap " />
                            </div>
                        </div><div class="control-group">
                            <label class="control-label" for="holiday_date">On Date<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="holiday_date" id="holiday_date" value="<?= $object->holiday_date ?>"  class="span6 new_format m-wrap validate[required]" />
                            </div>
                        </div> 		
                        <div class="control-group">
                            <label class="control-label" for="remarks">Description<span class="required"></span></label>
                            <div class="controls">
                                <textarea type="text" name="remarks" id="remarks" style='width:47%;'><?= $object->remarks ?></textarea>
                            </div>
                        </div>  
                        <div class="control-group">
                            <label class="control-label" for="alert">Alert For Students</label>
                            <div class="controls">
                                <input type="checkbox" name="alert" id="alert" value="1" <?= ($object->alert == '1') ? 'checked' : '' ?>/>
                            </div>
                        </div>
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 
                            <input type="hidden" name="id" value="<?= $object->id ?>" tabindex="7" /> 													 
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('holiday', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>                                   
                    </div> 

                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>