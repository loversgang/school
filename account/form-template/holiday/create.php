  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Academy Holidays
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>  
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('holiday', 'list', 'list');?>">List Holidays</a>
                                         <i class="icon-angle-right"></i>                                       
                                    </li>									
                                    <li class="last">
                                        New Holiday
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
				
					<form class="form-horizontal validation" action="<?php echo make_admin_url('holiday', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" id="validation">	
						
					<!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-calendar"></i>Add Holiday</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">
      
                                            <div class="control-group">
                                                    <label class="control-label" for="title">Title<span class="required">*</span></label>
                                                     <div class="controls">
                                                       <input type="text" name="title"  value="" id="title" class="span6 m-wrap validate[required]" />
                                           
                                                     </div>
                                            </div>    
	                                        <div class="control-group">
                                                    <label class="control-label" for="title">Type</label>
                                                     <div class="controls">
                                                       <input type="text" name="type"  value="" id="type" class="span6 m-wrap " />
													 </div>
                                            </div>  
                                            <div class="control-group">
                                                    <label class="control-label" for="holiday_date">On Date<span class="required">*</span></label>
                                                     <div class="controls">
                                                       <input type="text" name="holiday_date" id="holiday_date" class="span6 new_format m-wrap validate[required]" />
													</div>
                                            </div> 											
                                            <div class="control-group">
                                                    <label class="control-label" for="remarks">Description<span class="required"></span></label>
                                                    <div class="controls">
                                                        <textarea type="text" name="remarks" id="remarks" style='width:47%;'></textarea>
                                                    </div>
                                            </div>  

                                            <div class="control-group">
                                                    <label class="control-label" for="alert">Alert For Students</label>
                                                    <div class="controls">
                                                       <input type="checkbox" checked name="alert" id="alert" value="1" />
                                                    </div>
                                            </div>                
                                            <div class="form-actions">
													 <input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" /> 	
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('holiday', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
                                   
                              </div> 

                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

