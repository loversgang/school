
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Edit Fee Heads
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('fee_head', 'list', 'list');?>">List Fee Heads</a>
                                         <i class="icon-angle-right"></i>                                       
                                    </li>
                                    <li class="last">
                                        Edit Fee Heads
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
						<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
				</div>	            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('fee_head', 'update', 'update&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Edit Fee Head</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">
      
                                            <div class="control-group">
                                                    <label class="control-label" for="Title">Title<span class="required">*</span></label>
                                                     <div class="controls">
                                                       <input type="text" name="name"  value="<?=$object->name?>" id="Title" class="span6 m-wrap validate[required]" />
                                                     </div>
                                            </div>  
                                             <div class="control-group">
                                                    <label class="control-label" for="type">Type<span class="required">*</span></label>
                                                     <div class="controls">
														<select name="type" id="type" class="span6 m-wrap validate[required]">
															<option value='One Time' <?=($object->type=='One Time')?'Selected':''?>>One Time</option>
															<option value='Regular' <?=($object->type=='Regular')?'Selected':''?>>Regular</option>
															<option value='Optional' <?=($object->type=='Optional')?'Selected':''?>>Optional</option>
														</select>
													 </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="is_active">Position</label> 
													<div class="controls">
                                                        <input type="text" name="position" value="<?=$object->position?>" class='span1' style='text-align:center;'/>
                                                    </div>
                                            </div> 											
											
                                            <div class="control-group">
                                                    <label class="control-label" for="is_active">Make Active</label> 
													<div class="controls">
                                                        <input type="checkbox" name="is_active" id="is_active" value="1" <?=($object->is_active=='1')?'checked':'';?>/>
                                                    </div>
                                            </div> 
                                    
                                            <div class="form-actions">
											<input type='hidden' name='school_id' value='<?=$school->id?>'/>
													 <input type='hidden' name='id' value='<?=$object->id?>'/>
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('fee_head', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
                                     
                              </div> 

                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

