
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Slider
                                    <a class="btn large grey" href="<?php echo make_admin_url('slider', 'list', 'insert');?>"><i class="icon-plus"></i> Add New Slide </a>
                            </h3>
                            <!--
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-play-circle"></i>
                                               <a href="<?php echo make_admin_url('slider', 'list', 'list');?>">List Slider Images</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        Edit Slider Image
                                    </li>

                            </ul>
                            -->

                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php #  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('slider', 'update', 'list')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-play-circle"></i>Edit Slider Image</div>
                                            
                                    </div>
                                    <div class="portlet-body form">

                                             <div class="control-group">
                                                    <label class="control-label" for="title">Title<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="title" value="<?=$slider->title?>" id="caption" class="span12 m-wrap validate[required]" />
                                                    </div>  
                                            </div>
                                            
                                            <div class="control-group">
                                                    <label for="image" class="control-label">Image</label>
                                                    <div class="controls">
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="fileupload-new thumbnail" style="max-width: 200px;position:relative ">
                                                                          <?php if($slider->image && (file_exists(DIR_FS_SITE_UPLOAD.'photo/slider/large/'.$slider->image))):
                                                                                 $image_obj=new imageManipulation();?>
                                                                                 <img style="max-width:200px" src="<?=$image_obj->get_image('slider','large', $slider->image);?>">
                                                                                 
                                                                                 <div style="position:absolute;top:4px;right:4px;"  class="btn-group" data-toggle="buttons-radio">
                                                                                   <a onclick="return confirm('Image shall be permanently deleted. Are you sure?');" title="delete image" href="<?php echo make_admin_url('slider', 'delete_image', 'delete_image', 'id='.$slider->id);?>" class="grey btn icn-only mini"><i class="icon-white icon-remove"></i></a>
                                                                                   &nbsp;
                                                                                   <a href="<?=$image_obj->get_image('slider','large',  $slider->image);?>"  title="View Original" class="grey btn icn-only mini fancybox-button"><i class="icon-white icon-zoom-in"></i></a>
                                                                                 </div>
                                                                          
                                                                                 
                                                                                
                                                                          <?php else:?>
                                                                                  <img src="assets/img/noimage.jpg"/> 
                                                                          <?php endif;?>
                                                                       
                                                                    </div>
                                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                    <div>
                                                                            <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                                            <span class="fileupload-exists">Change</span>
                                                                            <input type="file" id="image" name="image" class="default" /></span>
                                                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                                    </div>
                                                            </div>
                                                           
                                                    </div>
                                            </div>
                                          
                                            <div class="control-group">
                                                    <label class="control-label" for="link">Link</label>
                                                    <div class="controls">
                                                      <input type="text" name="link" value="<?=$slider->link?>" id="link" class="span12 m-wrap" />
                                                    </div>  
                                            </div>
                                        
                                             <div class="control-group">
                                                    <label class="control-label" for="short_description">About slide in short</label>
                                                    <div class="controls">
                                                        <textarea id="short_description" class="span12 m-wrap" name="short_description" rows="6"><?php echo html_entity_decode($slider->short_description);?></textarea>
                                                       
                                                    </div>
                                             </div>
                                           <!--
                                            <div class="control-group">
                                                    <label class="control-label" for="is_active">Show on Website</label>
                                                    <div class="controls">
                                                        <input type="checkbox" name="is_active" id="is_active" value="1" <?=($slider->is_active=='1')?'checked':'';?> />
                                                    </div>  
                                            </div>-->
                                           <?php
                                             if($slider->is_active=='1'):?>
                                                <input type="hidden" name="is_active"  value="1" /> 
                                            <?php endif;
                                            ?>
                                            <div class="control-group">
                                                    <label class="control-label" for="position">Position<span class="required">*</span></label>
                                                    <div class="controls">
                                                         <input style="width:10%" type="text" id="position"  name="position" value="<?=$slider->position?>" class="span6 m-wrap validate[required]" />
                                                    </div>  
                                            </div>

                                            <input type="hidden" name="id" value="<?php echo $slider->id?>" />

                                           
                                        
                                   
                                            <div class="form-actions">
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('slider', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                                    
                                            </div>
                                         
                                          
                                  
                              </div> 
                            </div>
                        </div>
                          
                            
                     </form>
                 
                   
                 
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



