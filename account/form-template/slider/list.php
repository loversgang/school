

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Slider
                                     <a class="btn large grey" href="<?php echo make_admin_url('slider', 'list', 'insert');?>"><i class="icon-plus"></i> Add New Slide</a>
                            </h3>
                            <!--
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List Slider Images</li>

                            </ul>
                            -->

                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php # include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-play-circle"></i>Manage Slides</div>
								
							</div>
							<div class="portlet-body">
							    
                                                                
                                                                    
                                                                     <? if($QueryObj->GetNumRows()!=0):?>
                                                                             <?php $sr=1;while($slider=$QueryObj->GetObjectFromRecord()):?>
                                                                                   
                                                                                   <?php if($sr==1 || ($sr-1)%4==0):?>
                                                            
                                                                                       <div class="row-fluid">
                                                                                        
                                                                                    <?php endif;?>        
                                                                                                <div class="span3">
                                                                                                        <div class="item">
                                                                                                            <?php
                                                                                                              $image_obj=new imageManipulation();
                                                                                                              if($slider->image && (file_exists(DIR_FS_SITE_UPLOAD.'photo/slider/large/'.$slider->image))):?>
                                                                                                                    <a class="tooltips fancybox-button" data-rel="fancybox-button" title="<?php echo $slider->title?>" href="<?=$image_obj->get_image('slider','big', $slider->image);?>">
                                                                                                                            <div class="zoom">
                                                                                                                                    <img class="<?php echo $slider->is_active==0?'hidden_image':''?>" src="<?=$image_obj->get_image('slider','large', $slider->image);?>" alt="<?php echo $slider->title?>"/> 

                                                                                                                                    <div class="zoom-icon"></div>
                                                                                                                            </div>
                                                                                                                    </a>
                                                                                                       <?php else:?>
                                                                                                             <img  src="assets/img/noimage.jpg"/>
                                                                                                       <?php endif;?>

                                                                                                                <div class="details">
                                                                                                                         <a href="<?php echo make_admin_url('slider', 'update', 'update', 'id='.$slider->id)?>" title="click here to edit this image" class="icon">Edit</i></a>|
                                                                                                                       
                                                                                                                        <a href="<?php echo make_admin_url('slider', 'change_status', 'list', 'id='.$slider->id)?>" title="click here to <?php echo $slider->is_active==1?'hide':'show'?> this image" class="icon"><?php echo $slider->is_active==1?'Hide':'Show'?></a>|
                                                                                                                        
                                                                                                                        <a href="<?php echo make_admin_url('slider', 'delete', 'list', 'id='.$slider->id.'&delete=1')?>" title="click here to delete this image" onclick="return confirm('Are you sure? You are deleting this record.');" class="icon">Delete</a> 
                                                                                                                        
                                                                                                                </div>
                                                                                                        </div>
                                                                                                </div>
                                                                                           
                                                                                        <?php if($sr==$QueryObj->GetNumRows() || $sr++%4==0 ):?>
                                                            
                                                                                        <div class="clearfix"></div>


                                                                                        </div>
                                                                                        
                                                                                       <?php endif;?>  
                                                                                        

                                                                            <?php endwhile;?>
                                                                            <?php if($total_pages==$p):?>
                                                                             <div class="row-fluid">
                                                                                 <div class="span3">
                                                                                        <div class="item">
                                                                                            <a class="btn large grey" href="<?php echo make_admin_url('slider', 'list', 'insert');?>">
                                                                                               <img  src="assets/img/upload_new_slide.png"/>
                                                                                           </a>    
                                                                                        </div>
                                                                                 </div>   
                                                                            </div> 
                                                                            <?php endif;?>
                                                                            <?php
                                                                           if($total_pages>1):
                                                                               
                                                                               echo pagingAdmin($p,$total_pages,$total_records,'slider');
                                                                               
                                                                           endif;
                                                                           ?>
                                                                    <?php else:?>
                                                                             <div style="text-align:center">  
                                                                                
                                                                                     
                                                                                                <a class="btn large grey" href="<?php echo make_admin_url('slider', 'list', 'insert');?>">
                                                                                                   <img style="text-align:center" src="assets/img/upload_new_slide.png"/>
                                                                                               </a>    
                                                                                    
                                                                               
                                                                               <br/>
                                                                                <div style="clear:both;height:10px">  </div>
                                                                               <p>
                                                                                   No slide found, 
                                                                                   <a style="text-decoration:underline" class="black" href="<?php echo make_admin_url('slider', 'list', 'insert');?>"> click to add new slide.</a>
                                                                               </p>
                                                                               <br/>
                                                                           </div>
                                                                          
                                                                    <?php endif;?>
                                                                    
                                                                   <div class="clearfix"></div>
                                                                   <div class="btn-group pull-right" data-toggle="buttons-radio">
                                                                    <a  href="<?php echo make_admin_url('slider', 'thrash', 'thrash');?>" class="btn mini red pull-right"><i class="icon-red icon-trash"></i> &nbsp;view trash</a>
                                                                   </div>
                                                                  <div class="clearfix"></div>
                                                            
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



