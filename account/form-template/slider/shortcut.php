<div class="tiles pull-right">

        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('slider', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                List Slider Images
                        </div>
                </div>
            </a>   
        </div>




        <div class="tile bg-blue <?php echo ($section=='insert')?'selected':''?>">
            <a href="<?php echo make_admin_url('slider', 'list', 'insert');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                New Slider Image
                        </div>
                </div>
            </a> 
        </div>

        <div class="tile bg-red <?php echo ($section=='thrash')?'selected':''?>">
            <a href="<?php echo make_admin_url('slider', 'thrash', 'thrash');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-trash"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Trash
                        </div>
                </div>
            </a> 
        </div>

</div>