<div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
    <a href="<?php echo ($action !== 'list') ? make_admin_url('circulars', 'list', 'list') : 'javascript:;'; ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                List Circular
            </div>
        </div>
    </a>   
</div>
<div class="tile bg-blue <?php echo ($section == 'insert') ? 'selected' : '' ?>">
    <a href="<?php echo ($action !== 'insert') ? make_admin_url('circulars', 'insert', 'insert') : 'javascript:;'; ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-plus"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                New Circular
            </div>
        </div>
    </a> 
</div>