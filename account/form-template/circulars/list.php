<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Circulars
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>List Circulars</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>

    <div class="clearfix"></div>
    <div style="clear:both;"></div>	
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-file-text"></i>Manage Circulars</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('category', 'list', 'list'); ?>" method="post" id="form_data" name="form_data" >	
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>                                                                                      
                                    <th class="hidden-480">Sr. No</th>																								
                                    <th class="hidden-480">Title</th>												
                                    <th>Description</th>												
                                    <th>On Date</th>												
                                    <th>Action</th>

                                </tr>
                            </thead>
                            <?php foreach ($lists as $key => $list) { ?>
                                <tr class="odd gradeX">
                                    <td class="hidden-480"><?php echo $key + 1 ?></td>												
                                    <td class="hidden-480"><?php echo $list['circular_title'] ?></td>                                                										
                                    <td>
                                        <div class="span6">
                                            <?php
                                            if (strlen($list['description']) > 10) {
                                                echo substr($list['description'], 0, 10) . '.....';
                                            } else {
                                                echo $list['description'];
                                            }
                                            ?>
                                        </div>
                                        <div class="span6">
                                            <button type="button" data-title="<?php echo $list['circular_title'] ?>" data-detail="<?php echo $list['description'] ?>" data-valid="<?php echo $list['file'] ?>" data-file="<?php echo DIR_WS_SITE_UPLOAD . 'file/circulars/' . $list['file'] ?>" class="btn blue icn-only tooltips mini" id="view">View File</button>
                                        </div>
                                    </td>  
                                    <td><?php echo $list['on_date'] ?></td>
                                    <td>
                                        <div class="span4">
                                            <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('circulars', 'update', 'update', 'id=' . $list['id']) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>
                                        </div>
                                        <div class="span4">
                                            <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('circulars', 'delete', 'delete', '&id=' . $list['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                        </div>
                                        <div class="span4"></div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>                          
                        </table>
                    </form>    
                </div>
            </div>                                                
        </div>
    </div>
    <?php require 'model.php' ?>
    <div class="clearfix"></div>
</div>

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<?php require 'script.php' ?>