<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Referers
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    All Referers
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box <?
            if ($type == 'confirm'): echo 'green';
            else: echo 'blue';
            endif;
            ?>">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-male"></i>Manage Referers</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <?php if (count($all_referers) !== 0) { ?>
                        <div style="overflow-y: hidden">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Staff</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php foreach ($all_referers as $key => $referer) { ?>
                                    <tr>
                                        <td>
                                            <?php
                                            $staff = get_object('staff', $referer['staff_id']);
                                            echo $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $referer['description'] ?>
                                        </td>
                                        <td><?php echo date('d M Y', strtotime($referer['date'])) ?></td>
                                        <td>

                                            <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('complaints', 'message', 'message&id=' . $id . '&staff_id=' . $referer['staff_id']) ?>"  data-original-title="Click here to send messages to <?php echo $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name ?>">Messages</a>
                                        </td>

                                    </tr>
                                <?php } ?>
                                </tbody>                          
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-info">No Referers found..!</div>
                    <?php } ?>
                </div></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>