<div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('complaints', 'list', 'list'); ?>">
        <div class="corner"></div>
        <div class="tile-body">
            <i class="icon-list fa-6x"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                <center>List Complaints</center>
            </div>
        </div>
    </a>   
</div>
<?php if ($section == 'referer_list' || $section == 'message') { ?>
    <div class="tile bg-blue <?php echo ($section == 'list') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('complaints', 'referer_list', 'referer_list&id=' . $id); ?>">
            <div class="corner"></div>
            <div class="tile-body">
                <i class="icon-list fa-6x"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    <center>List Referers</center>
                </div>
            </div>
        </a>   
    </div>
    <div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('complaints', 'referer', 'referer&id=' . $id); ?>">
            <div class="corner"></div>
            <div class="tile-body">
                <i class="icon-list fa-6x"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    <center>Send Referers</center>
                </div>
            </div>
        </a>   
    </div>
<?php } ?>