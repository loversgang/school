<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Referers
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('referers', 'list', 'list'); ?>">List Referers</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add Referers
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data" id="validation">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Add New Referers</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="control-group">
                            <label class="control-label" for="Staff">Select Staff<span class="required">*</span></label>
                            <div class="controls">
                                <select class="span6 m-wrap validate[required]" name="staff_id[]" id="staff_id">
                                    <option value="">Select Staff</option>
                                    <?php foreach ($all_staff as $key => $staff) { ?>
                                        <option value="<?php echo $key ?>"><?php echo $staff ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div id="more_staff_input"></div>
                        <div class="control-group">
                            <div class="controls">
                                <input type="button" class="m-wrap btn red" id="add_more_staff" value="Add More">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="description">Description<span class="required"></span></label>
                            <div class="controls">
                                <textarea type="text" name="description" id="description"  class="span6 m-wrap validate[required]"></textarea>
                            </div>
                        </div>  
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                            <input type="hidden" name="date" value="<?php echo time() ?>" tabindex="7" /> 	
                            <input type="hidden" name="complaint_id" value="<?php echo $id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('complaints', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>

<script>
    $(document).on('click', '#add_more_staff', function () {
        $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=complaints'); ?>', {action: 'input_for_more_staff'}, function (data) {
            $('#more_staff_input').append(data);
        });
    });

    $(document).on('click', '#remove_field', function () {
        $(this).parents('.control-group').remove();
        });
</script>

