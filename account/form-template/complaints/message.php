<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Referers Message
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('complaints', 'list', 'list'); ?>">List Referer Message</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add Referers Message
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Add New Referer Message</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row-fluid">
                        <form class="form-inline" method="POST" id="validation">
                            <label class="control-label" for="message" style="margin-left: 25px;">Referers Message<span class="required"></span></label>
                            <input type="text" name="message" id="feedback" class="span4 m-wrap validate[required]" />
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <input class="btn blue" type="hidden" name="complaint_id" value="<?php echo $_GET['id'] ?>" /> 
                            <input class="btn blue" type="hidden" name="admin" value="<?php echo 1 ?>" /> 
                            <input class="btn blue" type="hidden" name="staff_id" value="<?php echo $_GET['staff_id'] ?>" /> 
                            <input class="btn blue" type="hidden" name="admin" value="1" /> 
                            <input class="btn blue" type="hidden" name="school_id" value="<?php echo $school->id ?>" /> 
                            <input class="btn blue" type="hidden" name="date" value="<?php echo time() ?>" /> 
                        </form>
                        <br /><br />
                        <div class="span6">
                            <?php foreach ($referers_messages as $referers_message) { ?>
                                <div class="row-fluid">
                                    <div class="span6">
                                        <?php
                                        if ($referers_message['admin'] == 0) {
                                            $staff = get_object('staff', $referers_message['staff_id']);
                                            echo '<a href="javascript:;">' . $staff->first_name . ' ' . $staff->last_name . '</a><br /><br />';
                                        } else {
                                            echo '<a href="javascript:;">Admin</a><br /><br />';
                                        }
                                        ?>
                                    </div>
                                    <div class="span6">
                                        <?php echo date('d-m-Y : h m i', $referers_message['date']) ?>
                                    </div>
                                </div>
                                <span style="word-wrap: break-word;"><?php echo $referers_message['message'] ?></span>
                                <br />
                                <hr />
                                <br />
                            <?php } ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>