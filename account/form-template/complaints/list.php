<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Complaints
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    All Complaints
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box <?
            if ($type == 'confirm'): echo 'green';
            else: echo 'blue';
            endif;
            ?>">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-male"></i>Manage Complaints</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <?php if (count($lists_complaints) !== 0) { ?>
                        <div style="overflow-y: hidden">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <!--<th>Title</th>-->
                                        <th>Description</th>
                                        <th>Against</th>
                                        <th>Complaint By</th>
                                        <th>Complaint Date</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <?php foreach ($lists_complaints as $key => $list_complaint) { ?>
                                    <tr>

                                        <!--<td><?php echo $list_complaint['title'] ?></td>-->
                                        <td>
                                            <?php
                                            if (strlen($list_complaint['description']) > 10) {
                                                ?>
                                                <div class="span6">
                                                    <?php
                                                    echo substr($list_complaint['description'], 0, 10) . '.....';
                                                    ?>
                                                </div>
                                                <div class="span6">
                                                    <button type="button" data-title="<?php echo $list_complaint['title'] ?>" data-detail="<?php echo $list_complaint['description'] ?>"  class="btn blue icn-only tooltips mini" id="view">View</button>
                                                </div>
                                                <?php
                                            } else {
                                                echo $list_complaint['description'];
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $list_complaint['against'] ?>
                                        </td>
                                        <td>
                                            <?php
                                            $student = get_object('student', $list_complaint['student_id']);
                                            echo $student->first_name . ' ' . $student->last_name;
                                            ?>
                                        </td>
                                        <td><?php echo date('d M Y', strtotime($list_complaint['date'])) ?></td>
                                        <td>

                                            <?php
                                            if ($list_complaint['status'] == 0) {
                                                echo 'Pending';
                                            } else {
                                                echo 'Solved';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php if ($list_complaint['status'] == 0) { ?>
                                                <a href="<?php echo make_admin_url('complaints', 'resolve', 'resolve&complaint_id=' . $list_complaint['id']) ?>" class="btn mini blue tooltips" title="Click Here to Set Resolved">Resolve</a>
                                            <?php } else { ?>
                                                <a href="<?php echo make_admin_url('complaints', 'resolved', 'resolved&complaint_id=' . $list_complaint['id']) ?>" class="btn mini blue tooltips" title="Click Here to Set Resolved">Resolved</a>
                                            <?php } ?>

                                            <a class="btn mini green icn-only tooltips" href="<?php echo make_admin_url('complaints', 'feedback', 'feedback&id=' . $list_complaint['id'] . '&student_id=' . $list_complaint['student_id']) ?>"  data-original-title="click here to send feedback to Parent">Feedback</a>
                                            <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('complaints', 'referer_list', 'referer_list&id=' . $list_complaint['id']) ?>"  data-original-title="click here to View Referers and Send Messages to  Referer staff">Referers</a>

                                            <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('complaints', 'delete', 'delete&id=' . $list_complaint['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="" data-original-title="click here to delete this record"><i class="icon-remove"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>                          
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-info">No Complaints found..!</div>
                    <?php } ?>
                </div></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php require 'model.php'; ?>
</div>
<script>
    $(document).on('click', '#view', function () {
        var data_detail = $(this).attr('data-detail');
        var data_title = $(this).attr('data-title');
        $('#data-detail').html(data_detail);
        $('#data-title').html(data_title);
        $('#myModal').modal();
    });
</script>