<?php
include_once(DIR_FS_SITE . 'include/functionClass/complaintsClass.php');
if (isset($_POST)) {
    extract($_POST);
    ?>

    <?php
    if ($action == 'input_for_more_staff') {
        $query = new referers;
        $all_staff = $query->get_staff();
        ?>
        <div class="control-group">
            <label class="control-label" for="Staff">Select Staff<span class="required">*</span></label>
            <div class="controls">
                <select class="span6 m-wrap validate[required]" name="staff_id[]" id="staff_id">
                    <option value="">Select Staff</option>
                    <?php foreach ($all_staff as $key => $staff) { ?>
                        <option value="<?php echo $key ?>"><?php echo $staff ?></option>
                    <?php } ?>
                </select>
                <button type="button" id="remove_field" class="btn red"><i class="icon-trash"></i></button>
            </div>
        </div>
        <?php
    }
}
?>