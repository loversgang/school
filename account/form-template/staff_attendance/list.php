

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Staff Attendance
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li class="last">
                    View Attendance
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>


    <!-- First steps -->
    <? if ($type == 'select'): ?>
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-right">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
        </div>	
        <div class="clearfix"></div>
        <?php
        /* display message */
        display_message(1);
        $error_obj->errorShow();
        ?>

        <div class="clearfix"></div>			
        <div class="row-fluid">
            <form action="<?php echo make_admin_url('staff_attendance', 'list', 'list') ?>" method="GET" enctype="multipart/form-data" id="validation">
                <input type='hidden' name='Page' value='staff_attendance'/>
                <input type='hidden' name='action' value='list'/>
                <input type='hidden' name='section' value='list'/>
                <input type='hidden' name='type' value='attendance'/>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>View Staff Attendance</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <h3 class="block">Select Staff Category & Date</h3>
                        <div class="alert alert-block alert-success">
                            <div class="row-fluid">
                                <div class="span6">								
                                    <div class="control-group">
                                        <label class="control-label">Select Staff Category</label>
                                        <div class="controls">
                                            <select class="select2_category span8" data-placeholder="Select Session Students" name='cat' >
                                                <option value="">Select All</option>
                                                <?php $session_sr = 1;
                                                while ($StCat = $QuerySalCat->GetObjectFromRecord()): ?>
                                                    <option value='<?= $StCat->id ?>' <? if ($StCat->id == $cat) {
                                                echo 'selected';
                                            } ?> ><?= ucfirst($StCat->name) ?></option>
        <? $session_sr++;
    endwhile; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="span6">
                                    <div class="control-group" >
                                        <label class="control-label">Select Month & Year</label>
                                        <div class="controls">
                                            <input type='text' value='<?= date('M Y') ?>' name='atten_date' class='month-picker span8 validate[required]' />																		
                                        </div>
                                    </div>
                                </div>
                            </div>									
                        </div>	
                        <div class="form-actions clearfix" >
                            <a href="<?php echo make_admin_url('staff_attendance', 'list', 'list'); ?>" class="btn red" name="cancel" > Cancel</a>
                            <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
                        </div>
                    </div> 
                </div>
            </form>
        </div>
        <!-- First steps End -->

<? elseif ($type == 'attendance'): ?>	
        <!-- END PAGE HEADER-->
        <div class="clearfix"></div>
        <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
        </div>	
        <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

        <div class="clearfix"></div>			
        <!-- Second steps -->
        <h3 id="title_show" class="block alert alert-info"><? if (is_object($category)): echo $category->name;
    else: echo "All";
    endif; ?>: Staff </h3>
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-calendar"></i>View Staff Attendance</div>
                        <div class="tools">
                            <font style='font-size:22px;'><?= date('M, Y', strtotime($atten_date)) ?></font>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th>Staff Name</th>
                                    <th class="hidden-480 sorting_disabled">Staff Category</th>

                            <? foreach ($Attendance_array as $key => $value): ?>
                                        <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='<?= $key ?>'>&nbsp;<?= $key ?></font></th>
                                <? endforeach; ?>
                                    <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='H'>&nbsp;Half Day</font></th>
                                    <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='W'>&nbsp;Working Days</font></th>																					

                                </tr>
                            </thead>

                                <? if (!empty($record)): ?>
                                <tbody>
        <?php
        $sr = 1;
        foreach ($record as $sk => $object): $checked = '';
            $Q_obj = new staffAttendance();
            $r_atten = $Q_obj->GetStaffAttendace($object->id, $first_date_of_month, $last_date_of_month);
            $t_attend = array_sum($r_atten);

            $M_obj = new schoolHolidays();
            $M_holiday = $M_obj->checkMonthlyHoliday($school->id, date('m', strtotime($first_date_of_month)), date('Y', strtotime($first_date_of_month)));
            ?>

                                        <tr class="odd gradeX">
                                            <td><?= $sr ?>.</td>
                                            <td><?= $object->title . ' ' . ucfirst($object->first_name) . ' ' . $object->last_name ?></td>
                                            <td class="hidden-480 sorting_disabled"><?= $object->staff_category ?></td>
                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $r_atten['P'];
                        else: echo '0';
                        endif; ?></td>
                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
                        else: echo '0';
                        endif; ?></td>
                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
                        else: echo '0';
                        endif; ?></td>
                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('H', $r_atten)): echo $r_atten['H'];
                        else: echo '0';
                        endif; ?></td>
                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $working_days - $M_holiday ?></td>
                                        </tr>
            <? $sr++;
        endforeach;
        ?>									
                                </tbody>
    <? endif; ?>

                        </table>

                        <div class="clearfix"></div>

                    </div>
                </div>

                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- Second steps End -->

<? else: ?>
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption"><i class="icon-user"></i>Staff Attendance</div>								
            </div>
            <div class="portlet-body">		
                <div class="alert alert-block alert-success"><br/>
                    <div class="row-fluid">
                        <div class='span6' style='text-align:center;'>
                            <a href='<?= make_admin_url('staff_attendance', 'list', 'list&type=select') ?>'>								
                                <div class="large_tile selected bg-grey" style='float: none; text-align: center; margin: auto;'>
                                    <div class="corner"></div>
                                    <div class="tile-body">
                                        <i class="icon-user"></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <button class='btn green'>View Attendance</button>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class='span6' style='text-align:center;'>
                            <a href='<?= make_admin_url('staff_attendance', 'insert', 'insert') ?>'>

                                <div class="large_tile selected bg-grey" style='float: none; text-align: center; margin: auto;'>
                                    <div class="corner"></div>
                                    <div class="tile-body">
                                        <i class="icon-user "></i>
                                    </div>
                                    <div class="tile-object">
                                        <div class="name">
                                            <button class='btn blue'>Add Attendance</button>
                                        </div>
                                    </div>
                                </div>	

                            </a>			
                        </div>
                    </div>
                </div>
            </div>
        </div>					
<? endif; ?>							


</div>
</div>
</div>
</div>

<div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    


