

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Staff Attendance
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
                                    <li class="last">
										<a href="<?php echo make_admin_url('staff_attendance', 'list', 'list');?>">
                                       List All</a>
									   <i class="icon-angle-right"></i>
                                    </li>
                                    <li class="last">
                                        Add Attendance
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
						<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
				</div>	
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
             

			
			<? if($type=='attendance'):?>												
			<!-- Second steps -->
				<h3 id="title_show" class="block alert alert-info"><? if(is_object($category)): echo $category->name; else: echo "All"; endif;?>: Staff </h3>
					<div class="row-fluid">
							<div class="span12">
								<!-- BEGIN EXAMPLE TABLE PORTLET-->
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption"><i class="icon-calendar"></i>Add Attendance</div>
										<div class="tools">
											<font style='font-size:22px;'><?=date('d M, Y',strtotime($atten_date))?></font>
										</div>
									</div>
									<div class="portlet-body">
									<form action="<?php echo make_admin_url('staff_attendance', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" id=''>									
										<table class="table table-striped table-bordered table-hover attendance_table" id="sample_2">
											<thead>
												<tr>
													<th>Sr. No.</th>
													<th>Staff Name</th>
													<th class="hidden-480 sorting_disabled">Staff Category</th>
													<th class="hidden-480 sorting_disabled" style='text-align:center;'>
													<div>Status</div><div>
																<label class="radio">
																	<span><input type="radio" class='master_checker' value="P" checked></span> Present
																</label>
																<label class="radio">
																	<span><input type="radio" class='master_checker' value="A"></span> Absent
																</label>
																<label class="radio">
																	<span><input type="radio" class='master_checker' value="L" ></span> Leave
																</label>
																<label class="radio">
																	<span><input type="radio" class='master_checker' value="H"></span> Half Day
																</label>
													</div>
													</th>
												</tr>
											</thead>
											
											<? if(!empty($record)):?>
											<tbody>
											<?php $sr=1;foreach($record as $sk=>$object): $checked='';
												#Add Multiple attendance into Session
												$QuerySt= new staffAttendance();
												$atten=$QuerySt->GetStaffAttendanceSingleDay($object->id,$atten_date);?>
												
												<tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td><?=$object->title.' '.ucfirst($object->first_name).' '.$object->last_name?></td>
													<td class="hidden-480 sorting_disabled"><?=$object->staff_category?></td>
													<td class="hidden-480 sorting_disabled" style='text-align:center;'>
														<? foreach($Attendance_array as $a_k=>$a_v):?>
																	<label class="radio <?=$a_k?>">
																	<div class="radio"><span><input name='attendance[<?=$sr?>][status]' type="radio" value="<?=$a_k?>" class='all_atten' <? if($a_k==$atten): echo 'checked'; elseif($a_k=='P'): echo 'checked'; endif; ?>></span></div>
																	<?=$a_v?>
																	</label>
														<? endforeach; ?>	
																<label class="radio H">
																	<span><input type="radio" name='attendance[<?=$sr?>][status]' class='all_atten' value="H" <? if('H'==$atten): echo 'checked'; endif; ?>></span> Half Day
																</label>														
													</td>
													<input type='hidden' name='attendance[<?=$sr?>][staff_id]' value='<?=$object->id?>'/>
													<input type='hidden' name='attendance[<?=$sr?>][date]' value='<?=$atten_date?>'/>																														
												</tr>
											<? $sr++;
												endforeach;?>									
											</tbody>
										<? endif;?>
										
										</table>
									<div class='span12' style='padding-right:35px; padding-top:10px;'>
										<input type='hidden' name='date' value='<?=$atten_date?>'/>
									</div>
		
									<div class="form-actions clearfix" style='text-align:center;'>
										<a href='<?=make_admin_url('staff_attendance','insert','insert')?>' class="btn red" ><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
										<? if(!empty($record)):?>
											<input class="btn green" type="submit" value="Add Attendance" name="multi_select"/>&nbsp;&nbsp;
										<? endif;?>									
									</div>
									<div class="clearfix"></div>
									</form>	
									</div>
								</div>
														
								<!-- END EXAMPLE TABLE PORTLET-->
							</div>
						</div>
			<!-- Second steps End -->
			<? else:?>
			
			<!-- First steps -->
			
            <div class="row-fluid">
				<form action="<?php echo make_admin_url('staff_attendance', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
				<input type='hidden' name='Page' value='staff_attendance'/>
				<input type='hidden' name='action' value='insert'/>
				<input type='hidden' name='section' value='insert'/>
				<input type='hidden' name='type' value='attendance'/>
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					 <div class="portlet box blue">
							<div class="portlet-title">
									<div class="caption"><i class="icon-user"></i>Add Staff Attendance</div>
									<div class="tools">
											<a href="javascript:;" class="collapse"></a>
									</div>
							</div>
							<div class="portlet-body form">
							<h3 class="block">Select Staff Category & Date</h3>
								<div class="alert alert-block alert-success">
									<div class="row-fluid">
										<div class="span6">								
											<div class="control-group">
												<label class="control-label">Select Staff Category</label>
												<div class="controls">
													<select class="select2_category span8" data-placeholder="Select Session Students" name='cat' >
														<option value="">Select All</option>
														<?php $session_sr=1;while($StCat=$QuerySalCat->GetObjectFromRecord()): ?>
																<option value='<?=$StCat->id?>' <? if($StCat->id==$cat){ echo 'selected'; }?> ><?=ucfirst($StCat->name)?></option>
														<? $session_sr++; endwhile;?>
													</select>
												</div>
											</div>
										</div>
											<div class="span6">
											<div class="control-group" >
												<label class="control-label">Select Date </label>
												<div class="controls">
													<input type='text' value='<?=date('Y-m-d')?>' name='atten_date' class='upto_current_date span8 validate[required]' />																		
												</div>
											</div>
											</div>
									</div>									
								</div>	
								<div class="form-actions clearfix" >
										<a href="<?php echo make_admin_url('staff_attendance', 'list', 'list');?>" class="btn red" name="cancel" > Cancel</a>
										<input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
								</div>
					  </div> 
					</div>
				</form>
			</div>
			<!-- First steps End -->			
			<? endif;?>							
									
								
							</div>
						</div>
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

	
	
	
<script type="text/javascript">

	$(".master_checker").click(function(){	 
			var ct_val=$(this).val();
			
			 if (!$(this).is(':checked')) {
					//$('.all_atten').val('P');
					$('.master_checker').removeAttr('checked');
					$('.master_checker').removeClass('radio');					
					$(".all_atten").each(function() {
						 if($(this).val()=='P'){
						   $(this).attr("checked",true);
						   $(this).parent().addClass('checked');
						}
						else{
						   $(this).removeAttr("checked");
						   $(this).parent().removeClass('checked');						
						}
					});					
				}
			else
				{
					$('.master_checker').removeAttr('checked');
					$('.master_checker').removeClass('radio');	
					$(".all_atten").each(function() {
						 if($(this).val()==ct_val){
						   $(this).attr("checked",true);
						   $(this).parent().addClass('checked');
						}
						else{
						   $(this).removeAttr("checked");
						   $(this).parent().removeClass('checked');						
						}
					});
					
					$(this).attr('checked',true); 
					$(this).parent().addClass('checked');	
					$(".master_checker").each(function() {
						 if($(this).val()!=ct_val){
						   $(this).removeAttr("checked");
						   $(this).parent().removeClass('checked');	
						}
					});					
					
				}			
	});	
</script> 
