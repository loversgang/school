

        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('staff_attendance', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-arrow-left"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Back
                        </div>
                </div>
            </a>   
        </div>

        <div class="tile bg-blue <?php echo ($section=='insert')?'selected':''?>">
            <a href="<?php echo make_admin_url('staff_attendance', 'insert', 'insert');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Add Attendance
                        </div>
                </div>
            </a> 
        </div>

