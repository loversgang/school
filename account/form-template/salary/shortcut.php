<div class="tiles pull-right">
    <div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-arrow-left"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    Back To Salary
                </div>
            </div>
        </a>   
    </div>
    <div class="tile bg-blue <?php echo ($section == 'insert') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('salary', 'insert', 'insert&staff_id=' . $staff_id); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-plus"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    Add New Salary
                </div>
            </div>
        </a> 
    </div>
</div> 