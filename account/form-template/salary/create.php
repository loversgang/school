<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Add New Salary
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a>
                    <i class="icon-angle-right"></i>
                </li>  
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id); ?>">List Staff Salary</a>
                    <i class="icon-angle-right"></i>                                               </li>		
                <li class="last">
                    New Staff Salary
                </li>
            </ul>
        </div>
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>           
        <div class="clearfix"></div>
        <?php
        display_message(1);
        $error_obj->errorShow();
        ?>
        <? if ($type == 'select'): ?>
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/type/select.php'); ?>           
        </div>
    <? elseif ($type == 'list'): ?>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption"></i>Salary Information : <?= $staff->title . ' ' . ucfirst($staff->first_name) . ' ' . $staff->last_name ?>
                </div>
                <div class="tools">
                    <i class="icon-money"></i>  Add Staff Salary For <?= date('M, Y', strtotime($date)) ?>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row-fluid">
                    <div class="span6">
                        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/type/attendance.php'); ?>           
                    </div>
                    <div class="span6">
                        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/type/list.php'); ?>           
                    </div>
                </div>
            </div>
        <? endif; ?>  
    </div>
</div>
<script>
    $(document).on('click', '#calculate_salary', function () {
        var total_amount = $('#total_amount').val();
        var options_div = $('#other_options_div');
        var type = options_div.find('#other_option').val();
        var other_amount = options_div.find('#other_amount').val();
        if (other_amount) {
            other_amount = options_div.find('#other_amount').val();
        } else {
            other_amount = 0;
        }
        var amount = 0;
        if (other_amount > 0) {
            if (type === 'other_earning') {
                amount = (parseFloat(total_amount) + parseFloat(other_amount));
            }
            if (type === 'other_deduction') {
                amount = (parseFloat(total_amount) - parseFloat(other_amount));
            }
        } else {
            amount = (parseFloat(total_amount));
        }
        $('#amount').val(amount);
    });

    $(document).on('click', '#remove_other_amount', function () {
        var options_div = $('#other_options_div');
        options_div.find('#other_name').val('');
        options_div.find('#other_amount').val('');
        $('#calculate_salary').click();
    });
</script>