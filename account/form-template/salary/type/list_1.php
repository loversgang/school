<form class="form-horizontal" action="<?php echo make_admin_url('salary', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption"><i class="icon-money"></i>Add Staff Salary For <?= date('M, Y', strtotime($date)) ?></div>
            <div class="tools">
                <font style='font-size:22px;'><?= date('M, Y', strtotime($date)) ?></font>
            </div>
        </div>
        <div class="portlet-body form">
            <h3 class="form-section">Salary Information</h3>
            <div class="control-group">
                <label class="control-label" for="staff_id">Staff Name<span class="required">*</span></label>
                <div class="controls">
                    <input type="text" id='staff_id' class="m-wrap span10 validate[required]" readonly value='<?= $staff->title . ' ' . ucfirst($staff->first_name) . ' ' . $staff->last_name ?>'> 
                    <input type="hidden" name='staff_id' id='staff_id' class="m-wrap span6" value=<?= $staff_id ?>>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <h4 class="form-section hedding_inner">Allowance</h4>
                    <?php $earning_for = array() ?>
                    <?php $total_earning = 0; ?>
                    <?php if ($earning_heads) { ?>
                        <?php foreach ($earning_heads as $k => $earning) { ?>
                            <?php
                            $earning_amount = 0;
                            if ($earning['type'] == 'percentage') {
                                $earning_amount = ($staff->salary * ($earning['amount'] / 100));
                            } else {
                                $earning_amount = $earning['amount'];
                                $earning_amount_cal[] = $earning['amount'];
                            }
                            $total_earning += $earning_amount;
                            $earning_title = staffPayHeads::getFieldDetail($k, 'title');
                            $earning_for[$earning_title] = $earning_amount;
                            ?>
                            <div class="control-group" id="dyanamic_salary">
                                <label class="control-label">
                                    <?php
                                    echo staffPayHeads::getFieldDetail($k, 'title');
                                    $allowance[] = staffPayHeads::getFieldDetail($k, 'title');
                                    ?>
                                </label>
                                <div class="controls">
                                    <input type="text" value='<?= number_format($earning_amount, 2); ?>' class="span10 m-wrap " readonly />
                                </div>
                            </div>
                        <?php } ?>
                        <?php
                        $deduction_for = array();
                        $total_deduction = 0;
                        if ($deduction_heads) {
                            foreach ($deduction_heads as $k => $deduction) {

                                $deduction_amount = 0;
                                if ($deduction['type'] == 'percentage') {
                                    foreach ($earning_for as $key => $ef) {

                                        if ($key == $deduction['deduction_for']) {
                                            $deduction_amount = (($ef * $deduction['amount']) / 100);
                                        }
                                    }
                                } else {
                                    $deduction_amount = $deduction['amount'];
                                }
                                $total_deduction += $deduction_amount;

                                staffPayHeads::getFieldDetail($k, 'title');
                                $deduction[] = staffPayHeads::getFieldDetail($k, 'title');
                                $deduction_for[$deduction['deduction_for']] = $deduction_amount;
                            }
                        }
                        ?>
                    <?php } else { ?>
                        No Record found..!<br/> <span class='help-block'><strong>Note: </strong><a href='<?= make_admin_url('designation') ?>' target='_blank'>Click here</a> to add/edit salary heads.</span>
                    <?php } ?>
                    <?php
                    $ed = array();
                    foreach ($earning_for as $k => $e) {
                        foreach ($deduction_for as $key => $d) {
                            if ($k == $key) {
                                $ed[$k] = $e - $d;
                            }
                        }
                    }
                    ?>
                </div>
                <div class="span12">
                    <h4 class="form-section hedding_inner">Deduction</h4>
                    <?php $total_deduction = 0; ?>
                    <?php if ($deduction_heads) { ?>
                        <?php foreach ($deduction_heads as $k => $deduction) { ?>
                            <?php
                            $deduction_amount = 0;
                            if ($deduction['type'] == 'percentage') {
                                foreach ($earning_for as $key => $ef) {

                                    if ($key == $deduction['deduction_for']) {
                                        $deduction_amount = (($ef * $deduction['amount']) / 100);
                                    }
                                }
                            } else {
                                $deduction_amount = $deduction['amount'];
                            }
                            $total_deduction += $deduction_amount;
                            ?>
                            <div class="control-group" id='dyanamic_salary'>
                                <label class="control-label">
                                    <?php
                                    echo staffPayHeads::getFieldDetail($k, 'title');
                                    ?>
                                </label>
                                <div class="controls">
                                    <input type="text"  value='<?= number_format($deduction_amount, 2); ?>' class="span10 m-wrap " readonly />
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        No Record found..!<br/> <span class='help-block'><strong>Note: </strong><a href='<?= make_admin_url('designation') ?>' target='_blank'>Click here</a> to add/edit salary heads.</span>

                    <?php } ?>
                </div>
                <div class="span12">
                    <h4 class="form-section hedding_inner">Total</h4>
                    <?php
                    $total_amount = 0;
                    foreach ($ed as $key => $total_ed) {
                        $total_amount += $total_ed;
                        ?>
                        <div class="control-group" id='dyanamic_salary'>
                            <label class="control-label">
                                <?php
                                echo $key;
                                ?>
                            </label>
                            <div class="controls">
                                <input type="text"  value="<?php echo number_format($total_ed, 2) ?>" class="span10 m-wrap " readonly />
                            </div>
                        </div>
                    <?php } ?>
                    <input type="hidden" value="<?php echo $total_amount; ?>" id="total_amount"/>

                </div>
                <?php $total_salary_amount = ($staff->salary + $total_earning - $total_deduction) ?>


                <div class="clearfix"></div>
                <h4 class="form-section hedding_inner">Other Details</h4>
                <div class="row-fluid" id="other_options_div">
                    <div class="span12">
                        <div class="control-group">
                            <label class="control-label" for="deduction">Current Earning (<?= CURRENCY_SYMBOL ?>)</label>
                            <div class="controls">
                                <div class="span5">
                                    <input type="text" name="earning_name"  value='' id="earning_name" class="span10 validate[required]" placeholder="Name"/>
                                </div>
                                <div class="span5">
                                    <input type="number" name="earning_amount"  value='' id="earning_amount" class="span10" placeholder="Amount" />
                                </div>
                                <div class="span2"></div>
                            </div>
                        </div>	
                        <div class="control-group">
                            <label class="control-label" for="deduction">Current Deduction (<?= CURRENCY_SYMBOL ?>)</label>
                            <div class="controls">
                                <div class="span5">
                                    <input type="text" name="deduction_name"  value='' id="deduction_name" class="span10 validate[required]" placeholder="Name"/>
                                </div>
                                <div class="span5">
                                    <input type="number" name="deduction_amount"  value='' id="deduction_amount" class="span10 m-wrap validate[custom[onlyNumberSp]]" placeholder="Amount" />
                                </div>
                                <div class="span2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="span12">
                        <?php if (count($list_loans_not_paid) > 0) { ?>
                            <h4 class="form-section hedding_inner">Loan Details</h4>
                            <div style="overflow-y: hidden">

                                <table class="table table-bordered">
                                    <thead>
                                    <th>Title</th>
                                    <th>Amount</th>
                                    <th>Installments</th>
                                    <th>Start Time</th>
                                    <th>Total Installments</th>
                                    <th>Installments Paid</th>
                                    <th>Installments Left</th>
                                    <th>Current Installment</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $installment_amount = 0;
                                        foreach ($list_loans_not_paid as $loan) {
                                            if ($loan->installments_paid < $loan->total_installments) {
                                                $amount = $loan->amount / $loan->total_installments;
                                                $installment_amount += $amount;
                                                ?>
                                                <tr>
                                                    <td><?php echo $loan->type ?></td>
                                                    <td><?php echo $loan->amount ?></td>
                                                    <td><?php echo strtoupper($loan->installment) ?></td>
                                                    <td><?php echo $loan->start_time ?></td>
                                                    <td><?php echo $loan->total_installments ?></td>
                                                    <td><?php echo $loan->installments_paid ?></td>
                                                    <td><?php echo $loan->total_installments - $loan->installments_paid ?></td>
                                                    <td>
                                                        <?php
                                                        echo number_format($amount, 2);
                                                        ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                                <input type="hidden" id="installment_amount" value="<?php echo $installment_amount ?>" />
                            </div>
                        <?php } ?>
                    </div>
                    <button id="add_other_options" type="button" class="btn blue span12" style="margin-top: 20px;margin-left: 0px;">Calculate Salary</button>
                    <div class="span2"></div>
                </div>
                <input type="hidden" id="total_basic_amount" value="<?php echo $total_salary_amount; ?>"/>
                <div class="control-group" id='dyanamic_salary'>
                    <label class="control-label" for="amount">Total Amount (<?= CURRENCY_SYMBOL ?>)<span class="required">*</span></label>
                    <div class="controls">
                        <input type="text" name="amount"  value="" id="amount" class="span10 m-wrap validate[required]" readonly/>
                    </div>
                </div> 			<div class="control-group">
                    <label class="control-label" for="payment_date">Payment Date<span class="required">*</span></label>
                    <div class="controls">
                        <input type="text" name="payment_date" id="payment_date"  class="span10 upto_current_date m-wrap validate[required]" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="remarks">Description<span class="required"></span></label>
                    <div class="controls">
                        <textarea type="text" name="remarks" id="remarks" style='width:79%;'></textarea>
                    </div>
                </div>  
            </div>
            <div class="clearfix"></div>
            <div class="form-actions">
                <input type="hidden" name="month_year" value="<?= date('Y-m-d', strtotime('01 ' . $date)) ?>" tabindex="7" />
                <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                <a href="<?php echo make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id); ?>" class="btn" name="cancel" > Cancel</a>
            </div>                                   
        </div> 
    </div>
</form>