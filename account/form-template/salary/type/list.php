<form class="form-horizontal" action="<?php echo make_admin_url('salary', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">Salary Information</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <input type="hidden" name='staff_id' id='staff_id' class="m-wrap span6" value=<?= $staff_id ?>>
            <div class="row-fluid">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th style="width:50%;">Allowance</th>
                            <th style="width:50%;text-align:right;">Amount (<i class="icon-inr"></i>)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $earningHeadTitle = array(); ?>
                        <?php if ($earning_heads) { ?>
                            <?php foreach ($earning_heads as $k => $earning) { ?>
                                <?php
                                $earning_amount = $earning['amount'];
                                $earning_title = staffPayHeads::getFieldDetail($k, 'title');
                                $earningHeadTitle[$earning_title] = $earning_amount;
                                ?>
                                <tr>
                                    <td style="text-transform: uppercase">
                                        <?php echo $earning_title; ?>
                                    </td>
                                    <td>
                                        <span class="pull-right sal_left">
                                            <i class="icon-inr"></i> <?= number_format($earning_amount, 2); ?>
                                        </span>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                </table>
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th style="width:50%;">Deduction</th>
                            <th style="width:50%;text-align:right;">Amount (<i class="icon-inr"></i>)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $ed = array(); ?>
                        <?php if ($deduction_heads) { ?>
                            <?php foreach ($deduction_heads as $k => $deduction) { ?>
                                <?php
                                $deduction_amount = 0;
                                $$deduction['deduction_for'] = !isset($$deduction['deduction_for']) ? 0 : $$deduction['deduction_for'];
                                if ($deduction['type'] == 'percentage') {
                                    $deduction_amount = (($deduction['amount'] * $earningHeadTitle[$deduction['deduction_for']]) / 100);
                                } else {
                                    $deduction_amount = $deduction['amount'];
                                }
                                $$deduction['deduction_for'] += $deduction_amount;
                                $ed[$deduction['deduction_for']] = $$deduction['deduction_for'];
                                $deduction_title = staffPayHeads::getFieldDetail($k, 'title');
                                $erdd = staffPayHeads::getFieldDetail($k, 'earning_deduction');
                                ?>
                                <tr>
                                    <td style="text-transform: uppercase">
                                        <?php echo $deduction_title; ?> (<span style="text-transform: capitalize"><?php echo $erdd; ?></span>)
                                    </td>
                                    <td>
                                        <span class="pull-right sal_left"> - <i class="icon-inr"></i> <?= number_format($deduction_amount, 2); ?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            No Record found..!<br/>
                        <span class='help-block'>
                            <strong>Note: </strong>
                            <a href='<?= make_admin_url('designation') ?>' target='_blank'>Click here</a> to add/edit salary heads.
                        </span>
                    <?php } ?>
                    </tbody>
                </table>
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th style="width:50%;">Total</th>
                            <th style="width:50%;text-align:right;">Amount (<i class="icon-inr"></i>)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total_amount = 0; ?>
                        <?php if ($earning_heads) { ?>
                            <?php foreach ($earning_heads as $k => $earning) { ?>
                                <?php
                                $earning_title = staffPayHeads::getFieldDetail($k, 'title');
                                $earning_amount = array_key_exists($earning_title, $ed) ? ($earning['amount'] - $ed[$earning_title]) : $earning['amount'];
                                $total_amount += $earning_amount;
                                ?>
                                <tr>
                                    <td style="text-transform: uppercase">
                                        <?php echo $earning_title; ?>
                                    </td>
                                    <td>
                                        <span class="pull-right sal_left">
                                            <i class="icon-inr"></i> <?php echo number_format($earning_amount, 2); ?>
                                        </span>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        <!-- Loan Details -->
                        <?php $installment_amount = 0; ?>
                        <?php if (count($list_loans_not_paid) > 0) { ?>
                            <?php
                            foreach ($list_loans_not_paid as $loan) {
                                if ($loan->installments_paid < $loan->total_installments) {
                                    $amount = $loan->amount / $loan->total_installments;
                                    $installment_amount += $amount;
                                    ?>
                                    <tr>
                                        <td style="text-transform: uppercase">
                                            <?php echo $loan->type ?>
                                        </td>
                                        <td>
                                            <span class="pull-right sal_left"> - <i class="icon-inr"></i> <?php echo number_format($amount, 2); ?></span>
                                        </td>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <?php $AmountTotal = ($total_amount - $installment_amount); ?>
                        <tr>
                            <td class="total">Sub Total</td>
                            <td class="total" colspan="2" style="text-align:right;">
                                <i class="icon-inr"></i> <span class="make_total_new"><?php echo $AmountTotal; ?></span>
                                <input type="hidden" value="<?php echo $AmountTotal; ?>" id="total_amount"/>
                            </td>
                        </tr>  
                    </tbody>
                </table>
            </div>
            <div class="row-fluid" id="other_options_div">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th style="width:35%">Other</th>
                            <th style="width:35%">Type</th>
                            <th colspan="2" style="width:30%;text-align:right;">Amount (<i class="icon-inr"></i>)</th>
                        </tr>
                    </thead>
                    <tbody>
                    <td>
                        <input class="span12" type="text" name="other_name" value="" id="other_name" placeholder="Other Name"/>
                    </td>
                    <td>
                        <select name="other_type" id="other_option" class="span12">
                            <option value="other_earning">Earning</option>
                            <option value="other_deduction">Deduction</option>
                        </select>
                    </td>
                    <td>
                        <input class="span12" type="number" step="any" name="other_amount" value="" id="other_amount" placeholder="Amount"/>
                    </td>
                    <td>
                        <button id="remove_other_amount" type="button" class="btn red"><i class="icon-trash"></i></button>
                    </td>
                    </tbody>
                </table>
            </div>
            <br/>
            <br/>
            <div class="clearfix"></div>
            <div class="row-fluid">
                <button id="calculate_salary" type="button" class="btn blue span12" style="margin-top: -30px;margin-left: 0px;">Calculate Salary</button>
            </div>
            <div class="clearfix"></div>
            <br/>
            <div class="row-fluid">
                <div class="control-group" id='dyanamic_salary'>
                    <label class="control-label" for="amount">Total Amount (<?= CURRENCY_SYMBOL ?>)<span class="required">*</span></label>
                    <div class="controls">
                        <input type="text" name="amount"  value="" id="amount" class="span10 m-wrap validate[required]" readonly/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="payment_date">Payment Date<span class="required">*</span></label>
                    <div class="controls">
                        <input type="text" name="payment_date" id="payment_date"  class="span10 upto_current_date m-wrap validate[required]" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="remarks">Description<span class="required"></span></label>
                    <div class="controls">
                        <textarea type="text" name="remarks" id="remarks" style='width:79%;'></textarea>
                    </div>
                </div>  
            </div>
            <div class="clearfix"></div>
            <div class="row-fluid">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th style="width:35%">Payment Method</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    <td>
                        <select name="payment_method" id="payment_method" class="span12">
                            <option value="By Cash">By Cash</option>
                            <option value="By Check">By Check</option>
                            <option value="Account">Account</option>
                        </select>
                    </td>
                    <td>
                        <div id="bank"></div>
                    </td>
                    </tbody>
                </table>
            </div>
            <div class="form-actions">
                <input type="hidden" name="month_year" value="<?= date('Y-m-d', strtotime('01 ' . $date)) ?>" tabindex="7" />
                <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                <a href="<?php echo make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id); ?>" class="btn" name="cancel" > Cancel</a>
            </div>                                   
        </div> 
    </div> 
</form>

<script>
    $(document).on('change', '#payment_method', function () {
        var payment_method = $(this).val();
        if (payment_method == 'By Check' || payment_method == 'Account') {
            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=salary'); ?>', {action: 'get_by_check'}, function (data) {
                $('#bank').html(data);
            });
        } else {
            $('#bank').find('select').remove();
        }
    });
</script>