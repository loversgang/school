<div class="container-fluid">
    <div class="hidden-print" >
        <div class="row-fluid">
            <div class="span12">
                <h3 class="page-title">
                    Manage Staff & Salary 
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li>                                   

                    <li class="last">
                        Salary Slip
                    </li>
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="tiles pull-right">
            <div class="tile bg-purple <?php echo ($section == 'view') ? 'selected' : '' ?>" >							
                <a class="hidden-print" href="<?= make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id) ?>">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-arrow-left"></i></div>
                    <div class="tile-object"><div class="name">Back To List</div></div>
                </a>
            </div>	

            <div class="tile bg-yellow <?php echo ($section == 'list') ? 'selected' : '' ?>" id='print_document'>							
                <a class="hidden-print" onclick="javascript:window.print();">
                    <div class="corner"></div>
                    <div class="tile-body"><i class="icon-print"></i></div>
                    <div class="tile-object"><div class="name">Print</div></div>
                </a>
            </div>							
        </div>            
        <div class="clearfix"></div>
        <?php
        display_message(1);
        $error_obj->errorShow();
        ?>
    </div>   
    <br/>	
    <div class="clearfix"></div>
    <div class="row-fluid">		
        <?= html_entity_decode($content) ?>	
    </div>
    <br/><br/><br/>	
</div>
<script type="text/javascript">
    $("#print_document").live("click", function () {
        var id = new_entry();
    });
    function new_entry() {
        var abc = "<?
        $newObj = new query('school_document_print_history');
        $newObj->Data['school_id'] = $school->id;
        $newObj->Data['staff_id'] = $staff_id;
        $newObj->Data['doc_type'] = '7';
        $newObj->Data['document_id'] = $doc_id;
        $newObj->Data['on_date'] = date('Y-m-d');
        $newObj->Insert();
        ?>";
    }
</script>