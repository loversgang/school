<?
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');

if ($_POST):
    isset($_POST['id']) ? $id = $_POST['id'] : $id = '';

    /* Get session Pages */

    $record = get_object('staff', $id);
    ?>
    <label class="control-label" for="amount">Amount (<?= CURRENCY_SYMBOL ?>)<span class="required">*</span></label>
    <div class="controls">
        <input type="text" name="amount"  value='<?= $record->salary ?>' id="amount" class="span6 m-wrap validate[required]" />
    </div>
    </div>	
    <?
endif;
?>