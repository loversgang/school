<?
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');

if ($_POST):
    isset($_POST['id']) ? $id = $_POST['id'] : $id = '';

    /* Get session Pages */
    $QuerySec = new staff();
    $record = $QuerySec->getCatStaff($school->id, $id);
    ?>
    <label class="control-label" for="staff_id">Teacher Name<span class="required">*</span></label>
    <div class="controls">
    <? if (!empty($record)): ?>
            <select name="staff_id"  class="staff_id span6 m-wrap salary_staff">
                <option value=''>Select Staff</option>
                <? foreach ($record as $k => $obj): ?>
                    <option value='<?= $obj->id ?>'><?= $obj->title . " " . $obj->first_name . " " . $obj->last_name ?></option>
            <? endforeach; ?>																													
            </select> 
        <? else: ?>
            <input type="text" name='staff_id' id='staff_id' class="m-wrap span6 validate[required]" readonly placholder='No Staff Found..!'> 
    <? endif; ?>	
    </div>	
    <?
endif;
?>