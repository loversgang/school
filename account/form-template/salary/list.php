<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                <?= $staff->title . ' ' . ucfirst($staff->first_name) . ' ' . $staff->last_name ?> :- Salary
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('staff', 'list', 'list'); ?>"> List Staff </a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li> Salary Detail
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-rupee"></i>Salary Detail</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div style="overflow-y: hidden">
                        <form action="<?php echo make_admin_url('salary', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >		
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Payment Date</th>
                                        <th>For Month & Year</th>
                                        <th>Amount (<?= CURRENCY_SYMBOL ?>)</th>
                                        <th>Payment Method</th>
                                        <th>Bank</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <? if (!empty($record)): ?>
                                    <tbody>
                                        <?php
                                        $sr = 1;
                                        foreach ($record as $key => $object):
                                            ?>
                                            <tr>
                                                <td><?= $sr ?>.</td>
                                                <td><?= date('d M, Y', strtotime($object->payment_date)) ?></td>
                                                <td><?
                                                    if (!empty($object->month_year)): echo date('M, Y', strtotime($object->month_year));
                                                    endif;
                                                    ?></td>
                                                <td><?= CURRENCY_SYMBOL . " " . number_format($object->amount, 2) ?></td>
                                                <td><?php echo $object->payment_method ?> </td>
                                                <td>
                                                    <?php
                                                    $bank = get_object('bank', $object->bank_id);
                                                    if (is_object($bank)) {
                                                        echo $bank->bank;
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <? if (is_object($slip)): ?>                <div class="span6">
                                                            <a class="btn mini yellow icn-only tooltips" href="<?php echo make_admin_url('salary', 'view', 'view', 'id=' . $object->id . '&staff_id=' . $staff_id . '&doc_id=' . $slip->id) ?>" title="click here to print the salary slip"><span class="hidden-320 hidden-480">Salary</span> Slip</a></div>
                                                    <? else: ?>
                                                        <div class="span6"></div>
                                                    <? endif; ?>
                                                    <div class="span3">
                                                        <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('salary', 'update', 'update', 'id=' . $object->id . '&staff_id=' . $staff_id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>
                                                    </div>
                                                    <div class="span3">
                                                        <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('salary', 'delete', 'delete', 'id=' . $object->id . '&staff_id=' . $staff_id . '&delete=1') ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                            $sr++;
                                        endforeach;
                                        ?>
                                    </tbody>
                                <?php endif; ?>  
                            </table>
                        </form>
                    </div>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    


<script type="text/javascript">
    $("#print").live("click", function () {
        var id = '<?= $staff_id ?>';

        var dataString = 'id=' + id;
        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'create_rep', 'print_rec&temp=salary'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var DocumentContainer = document.getElementById(data);
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
</script>
