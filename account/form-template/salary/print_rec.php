	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>


<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');

isset($_REQUEST['from'])?$from=$_REQUEST['from']:$from='';
isset($_REQUEST['to'])?$to=$_REQUEST['to']:$to='';
isset($_REQUEST['cat'])?$cat=$_REQUEST['cat']:$cat='';
isset($_REQUEST['id'])?$id=$_REQUEST['id']:$id='';

			#list all salary.
			$QueryDet = new staff();
			$staff=$QueryDet->getStaffDetail($school->id,$id);

 			#list all salary.
			$QueryObj = new staffSalary();
			$record=$QueryObj->getSingleSraffSalary($school->id,$id);
?>

<? if(!empty($record)):?>
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    <?=$school->school_name?>
                            </h3>
                            <ul class="breadcrumb hidden-print">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'update', 'update');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 

                                    <li class="last">
                                        Salary Report
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-left">
				
				<div class="control-group alert alert-success span4" style='margin-left:0px;padding-right:0px;'>
					<label class="control-label" style='float: none; text-align: left;'> Name : &nbsp;&nbsp; <?=$staff->title.' '.$staff->first_name.' '.$staff->last_name;?> </label>
					<label class="control-label" style='float: none; text-align: left;'>Category : &nbsp;&nbsp; <?=$staff->staff_category?> </label>
					<label class="control-label" style='float: none; text-align: left;'>Designation : &nbsp;&nbsp; <?=$staff->designation?> </label>

					
				</div>
													
				</div>	
				
          
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<form class="form-horizontal" action="<?php echo make_admin_url('account', 'lsit', 'lsit')?>" method="POST" enctype="multipart/form-data" id="validation">
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption"><i class="icon-rupee"></i>Salary Reports</div>								
							</div>
							<div class="portlet-body">
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover" id="">
									<thead>
										 <tr>
												<th style="width:65px;">Sr. No.</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Payment Date</th>
												<th style='text-align:right' class="hidden-480 sorting_disabled">Amount (<?=CURRENCY_SYMBOL?>)</th>
										</tr>
									</thead>
                                        <? if(!empty($record)):?>
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):?>
                                                <tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td style='text-align:center' class="hidden-480 sorting_disabled"><?=$object->payment_date?></td>
													<td style='text-align:right'><?=CURRENCY_SYMBOL." ".number_format($object->amount,2)?></td>													
													
												</tr>
                                            <?php $sr++;
												endforeach;?>
										</tbody>
									   <?php else:?> 
                                                <tr class="odd gradeX">
													<td></td>
													<td colspan='2' style='text-align:center' class="hidden-480 sorting_disabled">Sorry, No Record Found..!</td>
												</tr>									
									<?php endif;?> 	
                            </form> 									
								</div>
							</div>
						</div>
						</form>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
<? endif;?>	