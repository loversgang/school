<?php
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');

if (isset($_POST)) {
    extract($_POST);

    if ($action == 'get_by_check') {
        $query = new bank;
        $banks = $query->banks();
        ?>
        <select name="bank_id" id="bank_id" class="span12">
            <?php foreach ($banks as $bank) { ?>
                <option value="<?php echo $bank['id'] ?>"><?php echo $bank['bank'] ?></option>
            <?php } ?>
        </select>
        <?php
    }
}
?>