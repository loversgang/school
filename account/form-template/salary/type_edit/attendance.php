<h3 class="form-section">Attendance Information</h3>
<div class="alert alert-block alert-success">
    <div class="row-fluid">	
        <div class='span5'>Total Days: </div>
        <div class='span6'><?= $total_month_days ?></div>
    </div>
    <div class="row-fluid">	
        <div class='span5'>Working Days: </div>
        <div class='span6'><?= $working_days - $M_holiday ?></div>
    </div>					
    <div class="row-fluid">	
        <div class='span5'>Present: </div>
        <div class='span6'><?
            if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $r_atten['P'];
            else: echo '0';
            endif;
            ?>
        </div>					
    </div>
    <div class="row-fluid">	
        <div class='span5'>Leave: </div>
        <div class='span6'><?
            if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
            else: echo '0';
            endif;
            ?>
        </div>					
    </div>					
    <div class="row-fluid">	
        <div class='span5' id='color_red'>Absent: </div>
        <div class='span6' id='color_red'><?
            if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
            else: echo '0';
            endif;
            ?>
        </div>					
    </div>
    <div class="row-fluid">	
        <div class='span5' id='color_red'>Half Day: </div>
        <div class='span6' id='color_red'><?
            if (!empty($r_atten) && array_key_exists('H', $r_atten)): echo $r_atten['H'];
            else: echo '0';
            endif;
            ?>
        </div>					
    </div>
</div>