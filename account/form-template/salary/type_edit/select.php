<form action="<?php echo make_admin_url('salary', 'insert', 'insert') ?>" method="GET" enctype="multipart/form-data" id="validation">
    <input type='hidden' name='Page' value='salary'/>
    <input type='hidden' name='action' value='insert'/>
    <input type='hidden' name='section' value='insert'/>
    <input type='hidden' name='staff_id' value='<?= $staff_id ?>'/>
    <input type='hidden' name='type' value='list'/>
    <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption"><i class="icon-user"></i>Add New Staff Salary</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
            </div>
        </div>
        <div class="portlet-body form">
            <h3 class="block">Select Staff Category & Date</h3>
            <div class="alert alert-block alert-success">
                <div class="row-fluid">
                    <div class="span6">
                        <div class="control-group" >
                            <label class="control-label">Select Month & Year</label>
                            <div class="controls">
                                <input type='text' value='<?= date('M Y') ?>' name='date' class='month-picker span8 validate[required]' />						</div>
                        </div>
                    </div>
                </div>			
            </div>	
            <div class="form-actions clearfix" >
                <a href="<?php echo make_admin_url('salary', 'list', 'list&staff_id=' . $staff_id); ?>" class="btn red" name="cancel" > Cancel</a>
                <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
            </div>
        </div> 
    </div>
</form>