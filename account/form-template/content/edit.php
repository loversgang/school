
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Content Pages
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                  
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('content', 'list', 'list');?>">List Pages</a>
                                         <i class="icon-angle-right"></i>                                       
                                    </li>
                                    <li class="last">
                                        Edit Page
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('content', 'update', 'update&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Edit Page</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">      
                                            <div class="control-group">
                                                    <label class="control-label" for="name">Page Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <input type="text" name="name"  value="<?=$object->name?>" id="name" class="span6 m-wrap validate[required]" />
													</div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="name">Urlname<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <input type="text" name="urlname"  value="<?=$object->urlname?>" id="name" class="span6 m-wrap validate[required]" />
													</div>
                                            </div>											
                                            <div class="control-group">
                                                    <label class="control-label" for="description">Page Content</label>
                                                    <div class="controls">
                                                        <textarea id="description" class="span12 ckeditor m-wrap" name="description" rows="6">
														<?=html_entity_decode($object->description)?>
														</textarea>
													</div>
                                             </div> 

                                            
											<h4 class="form-section hedding_inner">SEO Information</h4>
                                            <div class="control-group">
                                                   <label class="control-label" for="meta_name">Meta Title</label>
                                                   <div class="controls">
                                                       <input type="text" name="meta_name" id="meta_name" class="span12 m-wrap" value="<?=$object->meta_name?>"> 
													</div>  
                                            </div>  
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_keyword">Meta Keywords</label>
                                                    <div class="controls">
                                                    <input type="text" name="meta_keyword" id="meta_keyword" class="span12 m-wrap" value="<?=$object->meta_keyword?>">
													</div>
                                            </div>          
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_description">Meta Description</label>
                                                    <div class="controls">
                                                    <textarea rows="3" class="span12 m-wrap" style=" height: 82px;" id="meta_description" name="meta_description"><?=html_entity_decode($object->meta_description)?></textarea>
													</div>
                                           </div>                                  
                                            <div class="form-actions">
													 <input type="hidden" name="id" value="<?=$object->id?>" tabindex="7" />
													 <input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" /> 	
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('content', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
                              </div>
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

