<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Compose Message
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-inbox"></i>
                    <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>">Inbox</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    Compose Message
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="row-fluid">
        <div class="col-sm-12 widget">
            <div class="well" style="margin-top: -20px;">
                <div id="email-view" class="email-view">
                    <?php if ($_GET['msg_box'] == 'inbox') { ?>
                        <?php if ($header->from_type == 'student') { ?>
                            <span class="pull-right"><a href="<?php echo make_admin_url('messages', 'compose', 'compose', 'user_id=' . $header->from_id . '&user_type=student') ?>"><i class="icon-reply"></i> Reply</a></span>
                        <?php } else { ?>
                            <span class="pull-right"><a href="<?php echo make_admin_url('messages', 'compose', 'compose', 'user_id=' . $header->from_id . '&user_type=staff') ?>"><i class="icon-reply"></i> Reply</a></span>
                        <?php } ?>
                    <?php } ?>
                    <h4><?php echo $header->subject; ?></h4>
                    <div class="email-date">
                        <span style="color: green"><?php echo date('h:i A', strtotime($header->time)); ?></span> . <a href="<?php echo make_admin_url('messages', 'read', 'read', 'act=delete&h_id=' . $header->id . '&msg_box=' . $_GET['msg_box']); ?>"><i style="color: darkred" class="icon-trash"></i></a>
                    </div>
                    <br/>
                    <p><?php echo $message->content; ?></p>
                </div>
            </div>
        </div>
    </div>
</div>