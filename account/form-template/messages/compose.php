<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Compose Message
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-inbox"></i>
                    <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>">Inbox</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    Compose Message
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="row-fluid">
        <form class="form-horizontal">
            <div class="control-group">
                <label class="control-label span2" for="type">User Type<span class="required">*</span></label>
                <div class="controls">
                    <select class="span10 m-wrap" autofocus="" name="user_type" id="user_type">
                        <option value="staff" <?php
                        if (isset($_GET['user_type']) && $_GET['user_type'] == 'staff') {
                            echo "selected";
                        }
                        ?>>Staff</option>
                        <option value="student" <?php
                        if (isset($_GET['user_type']) && $_GET['user_type'] == 'student') {
                            echo "selected";
                        }
                        ?>>Student</option>
                    </select>
                </div>
            </div>
            <?php if (!isset($_GET['user_id'])) { ?>
                <div id="hide_section">
                    <div id="get_sessions"></div>
                    <div id="session_list_details"></div>
                    <div id="get_sections"></div>
                </div>
            <?php } ?>
        </form>
        <form class="form form-horizontal" method="post" action="" id="validation">
            <?php if (isset($_GET['user_id'])) { ?>
                <div class="control-group">
                    <label class="control-label span2" for="name">To<span class="required">*</span></label>
                    <div class="controls">
                        <input type="hidden" name="to_id" value="<?php echo $to_id; ?>">
                        <input disabled type="text" value="<?php echo $to_name ?> <?php echo ($_GET['user_type'] == 'student') ? $current_session->title : '' ?>" class="span10 m-wrap" autofocus="">
                    </div>
                </div>
            <?php } else { ?>
                <div id="get_users"></div>
            <?php } ?>
            <div class="control-group">
                <label class="control-label span2" for="name">Subject:<span class="required">*</span></label>
                <div class="controls">
                    <input name="subject" id="subject" type="text" class="span10 m-wrap validate[required]">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label span2" for="name">Message:<span class="required">*</span></label>
                <div class="controls">
                    <textarea name="content" id="content" rows="10" class="span10 m-wrap validate[required]"></textarea>
                </div>
            </div>
            <div class="span12">
                <div class="form-actions">
                    <input type="hidden" name="school_id" value="<?php echo $school->id; ?>">
                    <input type="hidden" name="from_id" value="<?php echo $id; ?>">
                    <input type="hidden" name="from_type" value="admin">
                    <input type="hidden" name="to_type" id="to_type" value="<?php echo $_GET['user_type']; ?>">

                    <button class="btn blue button-next" type="submit" name="submit" tabindex="7">Send</button> &nbsp;&nbsp;

                    <a href="<?php echo make_admin_url('messages', 'list', 'list'); ?>" class="btn" name="cancel"> Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#user_type').change();
    });
    $(document).on('change', '#user_type', function () {
        var type = $(this).val();
        $('#to_type').val(type);
        if (type === 'student') {
            $('#hide_section').show();
            $("#get_sessions").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_session_years'}, function (data) {
                $("#get_sessions").html(data);
                $('#sess_int').change();
            });
        }
        if (type === 'staff') {
            $('#hide_section').hide();
            $("#get_users").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_staff'}, function (data) {
                $('#get_users').html(data);
            });
        }
    });
    $(document).on('change', '#sess_int', function () {
        var sess_int = $(this).val();
        $("#session_list_details").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_sessions_list', sess_int: sess_int}, function (data) {
            $('#session_list_details').html(data);
            $('#session_id').change();
        });
    });
    $(document).on('change', '#session_id', function () {
        var session_id = $(this).val();
        $("#get_sections").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_sections', session_id: session_id}, function (data) {
            $('#get_sections').html(data);
            $('#section').change();
        });
    });
    $(document).on('change', '#section', function () {
        var section = $(this).val();
        var session_id = $('#session_id').val();
        $("#get_users").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=messages'); ?>", {act: 'get_students', session_id: session_id, section: section}, function (data) {
            $('#get_users').html(data);
        });
    });
</script>