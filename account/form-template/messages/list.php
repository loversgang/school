<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Messages
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    Messages
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="row-fluid">
        <center>
            <?php
            display_message(1);
            $error_obj->errorShow();
            ?>
        </center>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="span2">
                <a href="<?php echo make_admin_url('messages', 'compose', 'compose') ?>" class="btn red btn-sm btn-block" role="button">COMPOSE</a>
                <br/>
                <ul class="nav nav-pills nav-stacked">
                    <li <?= $m_type == 'inbox' ? ' class="active"' : '' ?>>
                        <a style="padding: 10px 10px" href="<?php echo make_admin_url('messages') ?>">
                            <?php if ($inbox_count > 0) { ?>
                                <span style="margin-top: 1px;" class="badge pull-right">
                                    <?php echo $inbox_count; ?>
                                </span>
                            <?php } ?>
                            Inbox </a>
                    </li>
                    <li <?= $m_type == 'outbox' ? ' class="active"' : '' ?>>
                        <a style="padding: 10px 10px" href="<?php echo make_admin_url('messages', 'list', 'list', 'm_type=outbox') ?>">
                            <?php if ($outbox_count > 0) { ?>
                                <span style="margin-top: 1px;" class="badge pull-right">
                                    <?php echo $outbox_count; ?>
                                </span>
                            <?php } ?>
                            Sent Messages </a>
                    </li>
                </ul>
            </div>

            <?php if ($m_type == 'inbox') { ?>
                <div class="span10">
                    <?php if ($admin_inbox_count > 0) { ?>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="home">
                                <div class="list-group">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Inbox
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($inbox_messages as $header) {
                                                $obj = new messages;
                                                $message = $obj->getMessageDetails($header->id);
                                                if ($header->from_type == 'student') {
                                                    $CurrObj = new studentSession();
                                                    $current_session = $CurrObj->getStudentCurrentSession($school->id, $header->from_id);
                                                    $student = get_object('student', $header->from_id);
                                                    $to_name = $student->first_name . ' ' . $student->last_name . '(' . $current_session . ')';
                                                } else {
                                                    $staff = get_object('staff', $header->from_id);
                                                    $to_name = $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name;
                                                }
                                                ?>
                                                <tr>
                                                    <td class="msg <?php echo $message->is_read == '0' ? 'unread' : 'read' ?>">
                                                        <a class="list-group-item msg <?php echo $message->is_read == '0' ? 'unread' : 'read' ?>" href="<?php echo make_admin_url('messages', 'read', 'read', 'h_id=' . $header->id . '&msg_box=inbox') ?>">
                                                            <span style="margin-left:10px;border-radius:10px !important" class="badge pull-right"><?php echo date('d-m-Y', strtotime($header->time)); ?></span>
                                                            <span style="border-radius : 10px !important" class="badge pull-right">Read More</span>
                                                            <span class="name" style="font-weight: bold;display: inline-block;">
                                                                <?php echo $to_name; ?>
                                                            </span>
                                                            <br/>
                                                            <span class=""><?php echo $header->subject; ?></span>
                                                            <span class="text-muted" style="font-size: 11px;"></span>
                                                            <br/>
                                                            <span style="color: green">
                                                                <?php echo date('h:i A', strtotime($header->time)); ?> 
                                                            </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-info">Inbox Empty..!</div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ($m_type == 'outbox') { ?>
                <div class="span10">
                    <?php if ($admin_outbox_count > 0) { ?>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="home">
                                <div class="list-group">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Outbox
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($outbox_messages as $header) {
                                                $obj = new messages;
                                                $message = $obj->getMessageDetails($header->id);
                                                if ($header->to_type == 'student') {
                                                    $CurrObj = new studentSession();
                                                    $current_session = $CurrObj->getStudentCurrentSession($school->id, $header->to_id);
                                                    $student = get_object('student', $header->to_id);
                                                    $to_name = $student->first_name . ' ' . $student->last_name . '(' . $current_session . ')';
                                                } else {
                                                    $staff = get_object('staff', $header->to_id);
                                                    $to_name = $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name;
                                                }
                                                ?>
                                                <tr>
                                                    <td class="msg <?php echo $message->is_read == '0' ? 'unread' : 'read' ?>">
                                                        <a class="list-group-item msg <?php echo $message->is_read == '0' ? 'unread' : 'read' ?>" href="<?php echo make_admin_url('messages', 'read', 'read', 'h_id=' . $header->id . '&msg_box=outbox') ?>">
                                                            <span style="margin-left:10px;border-radius:10px !important" class="badge pull-right"><?php echo date('d-m-Y', strtotime($header->time)); ?></span>
                                                            <span style="border-radius : 10px !important" class="badge pull-right">Read More</span>
                                                            <span class="name" style="display: inline-block;">
                                                                <?php echo $to_name; ?>
                                                            </span>
                                                            <br/>
                                                            <span class=""><?php echo $header->subject; ?></span>
                                                            <span class="text-muted" style="font-size: 11px;"></span>
                                                            <br/>
                                                            <span style="color: green">
                                                                <?php echo date('h:i A', strtotime($header->time)); ?> 
                                                            </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-info">OutBox Empty..!</div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>