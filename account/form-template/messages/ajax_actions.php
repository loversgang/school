<?php
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
$QueryObjQ = new session();
$AllSessYear = $QueryObjQ->listOfYearSess($school->id, 1);
if (empty($sess_int)) {
    $sess_int = $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'];
}

if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'get_session_years') {
        ?>
        <div class="control-group">
            <label class="control-label span2">Session Year</label>
            <div class="controls">
                <select class="select2_category span10 m-wrap" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                    <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                        <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                    <? endforeach; ?>
                </select>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_sessions_list') {
        $dates = explode(',', $sess_int);
        $sess_to = date('Y', strtotime($dates[1]));
        $sess_from = date('Y', strtotime($dates[0]));
        $session_int_array = array($sess_to, $sess_from);
        $session_int = implode('-', $session_int_array);
        $obj = new session;
        $All_Session = $obj->listOfYearSession($school->id, $session_int, 'object');
        ?>
        <div class="control-group">
            <label class="control-label span2">Session</label>
            <div class="controls">
                <select class="span10 m-wrap" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                    <?php
                    if (!empty($All_Session)) {
                        if (empty($session_id) && !empty($All_Session)):
                            $session_id = $All_Session['0']->id;
                        endif;
                        $session_sr = 1;
                        foreach ($All_Session as $key => $session):
                            ?>
                            <option value='<?= $session->id ?>' <?
                            if ($session->id == $session_id):
                                echo 'selected';
                            endif;
                            ?> ><?= ucfirst($session->title) ?></option>
                                    <?
                                    $session_sr++;
                                endforeach;
                            }
                            ?>
                </select>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_sections') {
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);
        ?>
        <div class="control-group">
            <label class="control-label span2">Section</label>
            <div class="controls">
                <? if ($QuerySec->GetNumRows() >= 0) { ?>	
                    <select class="span10 m-wrap" data-placeholder="Select Session Students" name='section' id='section'>
                        <?php while ($sec = $QuerySec->GetObjectFromRecord()) { ?>
                            <option value='<?= $sec->section ?>'><?= ucfirst($sec->section) ?></option>
                        <?php } ?>
                    </select>
                <?php } else { ?>
                    <input type='text' class="span10 m-wrap" value='No Section.' disabled />
                <?php } ?>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_students') {
        $QueryOb = new studentSession();
        $students = $QueryOb->sectionSessionStudents($session_id, $section);
        ?>
        <div class="control-group">
            <label class="control-label span2" for="name">To<span class="required">*</span></label>
            <div class="controls">
                <select class="span10 m-wrap" data-placeholder="Select Student" name='to_id' id='to_id'>
                    <?php
                    foreach ($students as $student) {
                        $CurrObj = new studentSession();
                        $current_session = $CurrObj->getStudentCurrentSession($school->id, $student->id);
                        $to_name = $student->first_name . ' ' . $student->last_name;
                        ?>
                        <option value="<?php echo $student->id; ?>"><?php echo $to_name ?> <?php echo $current_session ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_staff') {
        $QueryObj = new staff();
        $staff_list = $QueryObj->getSchoolStaffDetailActiveDeactive($school->id, '1');
        ?>
        <div class="control-group">
            <label class="control-label span2" for="name">To<span class="required">*</span></label>
            <div class="controls">
                <select class="span10 m-wrap" data-placeholder="Select Student" name='to_id' id='to_id'>
                    <?php
                    foreach ($staff_list as $staff) {
                        $to_name = $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name;
                        ?>
                        <option value="<?php echo $staff->id; ?>"><?php echo $to_name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php
    }
}