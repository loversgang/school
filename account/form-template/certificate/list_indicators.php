
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Indicator List
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>List Indicators</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('certificate', 'list', 'list'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-list"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    List Certificates
                </div>
            </div>
        </a>   
    </div>
    <div class="tile bg-purple <?php echo ($section == 'list_indicators') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('certificate', 'list_indicators', 'list_indicators'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-list"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    List Indicators
                </div>
            </div>
        </a>   
    </div>
    <div class="tile bg-blue <?php echo ($section == 'create_indicators') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('certificate', 'create_indicators', 'create_indicators'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-plus"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    New Indicator
                </div>
            </div>
        </a> 
    </div>
    </div>		
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-file-text"></i>Manage Indicators</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="post" id="form_data" name="form_data" >	
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>                                                                                      
                                    <th class="hidden-480">Sr. No</th>
                                    <th style='width:50%;'>Title</th>
                                    <th class="hidden-480">Sem</th>
                                    <th class="hidden-480">Type</th>
                                    <th class="hidden-480">Action</th>
                                </tr>
                            </thead>
                            <tbody>                                                                            
                                <?
                                $k = 1;
                                foreach ($indicators as $indicator) {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="hidden-480"><?php echo $k++; ?></td>				
                                        <td><?= $indicator->title ?></td>  
                                        <td class="hidden-480"><?= $indicator->sem ?></td>
                                        <td class="hidden-480"><?= ucfirst($indicator->type) ?></td>
                                        <td style='text-align:right;'>
                                            <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('certificate', 'update_indicators', 'update_indicators&indicator_id=' . $indicator->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                            <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('certificate', 'delete_indicators', 'delete_indicators', '&indicator_id=' . $indicator->id . '&delete=1') ?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                        </td>						
                                    </tr>
                                <?php } ?>
                            </tbody>                          
                        </table>
                    </form>    
                </div>
            </div>                                                
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



