<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');

if($_POST):
isset($_POST['session_id'])?$session_id=$_POST['session_id']:$session_id='';
isset($_POST['ct_sec'])?$ct_sec=$_POST['ct_sec']:$ct_sec='A';
	
		/* Get session Pages*/
		 $QuerySec=new studentSession();
         $QuerySec->listAllSessionSection($session_id);
		 
		 /* Get exams sessions*/
		 $QueryExmObj=new examinationGroup();
		 $exams=$QueryExmObj->getGroupExamSessionSection($session_id,$ct_sec);			 
?>
			<div class='span4' >
				<label class="control-label" style='width:auto;'>Select Section</label>
				<? if($QuerySec->GetNumRows()>0): ?>
				<select class="select2_category span10 exam_session_filter" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
					<?php while($sec=$QuerySec->GetObjectFromRecord()): ?>
							<option value='<?=$sec->section?>' <?=($sec->section==$ct_sec)?'selected':''?>><?=ucfirst($sec->section)?></option>
					<? endwhile;?>	
				</select>
				<? else:?>
						<input type='hidden' name='ct_sec' id='ct_sec' value='A' />
						<input type='hidden' name='exam_id' id='exam_id' value='' />
						<input type='text' class="span10" value='No Section.' disabled />
				<? endif;?>											
			</div>
			<div class='span4' >
				<label class="control-label" style='width:auto;'>Select Exam</label>
				
				<? if(!empty($exams)): ?>
					<select class="span11" name='exam_id' id='exam_id'>
						<?php foreach($exams as $ex_key=>$ex_val): ?>
								<option value='<?=$ex_val->id?>'><?=ucfirst($ex_val->title)?></option>
						<?php endforeach;?>
					</select>
				<? else: ?>	
					<input type='hidden' name='exam_id' id='exam_id' value='' />	
					<input type='text' class='span11' value='Sorry, No Exam Found.' disabled />	
				<? endif; ?>
				
			</div>		
			<div class='span2'>	
				<label class="control-label">&nbsp;</label>
				<span class='btn medium green' id='go_exam'>Go</span>									
			</div>						
<?		
endif;
?>