<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');
if (isset($_POST['act']) && $_POST['act'] == 'save_yearly_dmc_optional_data') {
    extract($_POST);
    $obj = new studentDMC;
    $obj->Where = "where student_id='$student_id' and type='$type'";
    $obj->Delete_where();
    foreach ($final_array as $array) {
        // Save Data For Sem I
        $sem1 = $array['semesters']['sem1'];
        $arr1['school_id'] = $school_id;
        $arr1['student_id'] = $student_id;
        $arr1['session_id'] = $session_id;
        $arr1['section'] = $section;
        $arr1['type'] = $type;
        $arr1['sem'] = 1;
        $arr1['title'] = $array['title'];
        $arr1['description'] = $sem1['description'];
        $arr1['grade'] = $sem1['grade'];
        $obj = new studentDMC;
        $obj->saveDMCData($arr1);

        // Save Data For Sem I
        $sem2 = $array['semesters']['sem2'];
        $arr2['school_id'] = $school_id;
        $arr2['student_id'] = $student_id;
        $arr2['session_id'] = $session_id;
        $arr2['section'] = $section;
        $arr2['type'] = $type;
        $arr2['sem'] = 2;
        $arr2['title'] = $array['title'];
        $arr2['description'] = $sem2['description'];
        $arr2['grade'] = $sem2['grade'];
        $obj = new studentDMC;
        $obj->saveDMCData($arr2);
    }
    echo "success";
}
if (isset($_POST['act']) && $_POST['act'] == 'save_yearly_dmc_cultural_data') {
    extract($_POST);
    $obj = new studentDMC;
    $obj->Where = "where student_id='$student_id' and type='$type'";
    $obj->Delete_where();
    foreach ($final_array as $array) {
        // Save Data For Sem I
        $sem1 = $array['semesters']['sem1'];
        $arr1['school_id'] = $school_id;
        $arr1['student_id'] = $student_id;
        $arr1['session_id'] = $session_id;
        $arr1['section'] = $section;
        $arr1['type'] = $type;
        $arr1['sem'] = 1;
        $arr1['title'] = $array['title'];
        $arr1['description'] = $sem1['description'];
        $obj = new studentDMC;
        $obj->saveDMCData($arr1);

        // Save Data For Sem I
        $sem2 = $array['semesters']['sem2'];
        $arr2['school_id'] = $school_id;
        $arr2['student_id'] = $student_id;
        $arr2['session_id'] = $session_id;
        $arr2['section'] = $section;
        $arr2['type'] = $type;
        $arr2['sem'] = 2;
        $arr2['title'] = $array['title'];
        $arr2['description'] = $sem2['description'];
        $obj = new studentDMC;
        $obj->saveDMCData($arr2);
    }
    echo "success";
}
if (isset($_POST['act']) && $_POST['act'] == 'save_yearly_dmc_personality_data') {
    extract($_POST);
    $obj = new studentDMC;
    $obj->Where = "where student_id='$student_id' and type='$type'";
    $obj->Delete_where();
    foreach ($final_array as $array) {
        // Save Data For Sem I
        $sem1 = $array['semesters']['sem1'];
        $arr1['school_id'] = $school_id;
        $arr1['student_id'] = $student_id;
        $arr1['session_id'] = $session_id;
        $arr1['section'] = $section;
        $arr1['type'] = $type;
        $arr1['sem'] = 1;
        $arr1['title'] = $array['title'];
        $arr1['description'] = $sem1['description'];
        $obj = new studentDMC;
        $obj->saveDMCData($arr1);

        // Save Data For Sem I
        $sem2 = $array['semesters']['sem2'];
        $arr2['school_id'] = $school_id;
        $arr2['student_id'] = $student_id;
        $arr2['session_id'] = $session_id;
        $arr2['section'] = $section;
        $arr2['type'] = $type;
        $arr2['sem'] = 2;
        $arr2['title'] = $array['title'];
        $arr2['description'] = $sem2['description'];
        $obj = new studentDMC;
        $obj->saveDMCData($arr2);
    }
    echo "success";
}
?>