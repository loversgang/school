<?
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSmsEmailClass.php');

isset($_REQUEST['doc'])?$doc=$_REQUEST['doc']:$doc='';
isset($_REQUEST['doc_id'])?$doc_id=$_REQUEST['doc_id']:$doc_id='';
isset($_REQUEST['staff_id'])?$staff_id=$_REQUEST['staff_id']:$staff_id='';

	
	if($doc=='icard'):
		$icard=get_object('document_master',$doc_id);
		if($doc=='icard' && is_object($icard) && !empty($staff_id)):
			#Get Staff
			$QueryStaff = new staff();
			$staff=$QueryStaff->getStaff_detail($staff_id);
		
			$im_obj=new imageManipulation();
			if($staff->photo):			
				$staff_image="<img src='".$im_obj->get_image_echo('staff','medium',$staff->photo)."'/>";
			else:
				$staff_image='<img src="assets/img/profile/img-1.jpg"/>';
			endif;
			if($staff->school_logo):			
				$school_logo="<img src='".$im_obj->get_image_echo('school','medium',$staff->school_logo)."' style='max-width:86px;max-height:89px;'/>";
			else:
				$school_logo='<img src="assets/img/invoice/webgarh.png"/>';
			endif;		
			
			$replace=array('SCHOOL'=>$staff->school_name,
						  'STAFF_NAME'=>$staff->title." ".$staff->first_name." ".$staff->last_name,
						  'DESIGNATION'=>$staff->designation,
						  'DEPARTMENT'=>$staff->staff_category,
						  'STAFF_PHOTO'=>$staff_image,
						  'LOGO'=>$school_logo
						  );		
				$content=$icard->format;
					if(count($replace)):
						foreach($replace as $k=>$v):
							$literal='{'.trim(strtoupper($k)).'}';
							$content=html_entity_decode(str_replace($literal, $v, $content));					
						endforeach;
					endif;
				echo $content; 
				echo "<input type='hidden' id='staff_id' value='".$staff_id."'/>";
		endif;
	endif;?>