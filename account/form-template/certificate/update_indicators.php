
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Indicator List
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('certificate', 'list_indicator', 'list_indicator'); ?>">List Indicators</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Edit Indicator
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('certificate', 'list', 'list'); ?>">
                <div class="corner"></div>

                <div class="tile-body">
                    <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        List Certificates
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-purple <?php echo ($section == 'list_indicators') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('certificate', 'list_indicators', 'list_indicators'); ?>">
                <div class="corner"></div>

                <div class="tile-body">
                    <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        List Indicators
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-blue <?php echo ($section == 'create_indicators') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('certificate', 'create_indicators', 'create_indicators'); ?>">
                <div class="corner"></div>

                <div class="tile-body">
                    <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        New Indicator
                    </div>
                </div>
            </a> 
        </div>
    </div>	            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-book"></i>Edit Indicator</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">

                        <div class="control-group">
                            <label class="control-label" for="Title">Title<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="title"  value="<?= $indicator->title ?>" id="Title" class="span6 m-wrap validate[required]" />
                            </div>
                        </div>  
                        <div class="control-group">
                            <label class="control-label" for="sem">Semester<span class="required">*</span></label>
                            <div class="controls">
                                <select name='sem' id='sem' class="span6 select2 m-wrap validate[required]" placeholder='Select Exp Type'>
                                    <option value='SEM1' <?= $indicator->sem == 'SEM1' ? 'selected' : '' ?>>SEM 1</option>
                                    <option value='SEM2'<?= $indicator->sem == 'SEM2' ? 'selected' : '' ?>>SEM 2</option>
                                </select>                                                  
                            </div>
                        </div>   
                        <div class="control-group">
                            <label class="control-label" for="type">Semester<span class="required">*</span></label>
                            <div class="controls">
                                <select name='type' id='type' class="span6 select2 m-wrap validate[required]" placeholder='Select Exp Type'>
                                    <option value='optional' <?= $indicator->type == 'optional' ? 'selected' : '' ?>>Assessment</option>
                                    <option value='cultural'<?= $indicator->type == 'cultural' ? 'selected' : '' ?>>Social</option>
                                    <option value='personality'<?= $indicator->type == 'personality' ? 'selected' : '' ?>>Personality Development</option>
                                </select>                                                  
                            </div>
                        </div>   
                        <div class="form-actions">
                            <input type='hidden' name="school_id" value='<?= $school->id ?>'/>	
                            <input type='hidden' name="id" value='<?= $indicator->id ?>'/>	
                            <input class="btn blue" type="submit" name="submit" value="Submit" /> 
                            <a href="<?php echo make_admin_url('certificate', 'list_indicators', 'list_indicators'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>

                    </div> 

                </div>
            </div>
        </form>
        <div class="clearfix"></div>

    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    

