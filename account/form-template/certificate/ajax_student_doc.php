<?
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSmsEmailClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');


isset($_REQUEST['doc'])?$doc=$_REQUEST['doc']:$doc='';
isset($_REQUEST['doc_id'])?$doc_id=$_REQUEST['doc_id']:$doc_id='';
isset($_REQUEST['student_id'])?$student_id=$_REQUEST['student_id']:$student_id='';
isset($_REQUEST['session_id'])?$session_id=$_REQUEST['session_id']:$session_id='';
isset($_REQUEST['ct_sec'])?$ct_sec=$_REQUEST['ct_sec']:$ct_sec='';
isset($_REQUEST['exam_id'])?$exam_id=$_REQUEST['exam_id']:$exam_id='';
	
	if($doc=='icard'):
		$icard=get_object('document_master',$doc_id);
		if($doc=='icard' && is_object($icard) && !empty($student_id)):
			/* Get Last Entry */	
			$QueryDocHis = new SchoolDocumentPrintHistory();
			$last_his=$QueryDocHis->getLastPrintDoc($school->id,$session_id,$ct_sec,$icard->id,$student_id);		
			
			/* Load the last updated doc if is it */	
			if(is_object($last_his)): 
				$link=make_admin_url('view','print','print&id='.$last_his->id);
				$content='
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							<strong>Note:-</strong>
							You have already print this document on '.date('d M, Y').'. <a href="'.$link.'" target="_blank">Click here</a> to reprint this document.
						</div>';
				echo $content;	
			else:		
				#get Student detail	
				$QueryStu = new studentSession();
				$student=$QueryStu->SingleStudentsWithSession($student_id,$session_id,$ct_sec);	
				
				#get session detail	
				$session=get_object('session',$session_id);
			
				$im_obj=new imageManipulation();
				if($student->photo): 			
					$student_image="<img src='".$im_obj->get_image_echo('student','medium',$student->photo)."' style='max-width:165px'/>";
				else:
					$student_image='<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
				endif;
				if($school->logo):			
					$school_logo="<img src='".$im_obj->get_image_echo('school','medium',$school->logo)."' style='max-width:165px'/>";
				else:
					$school_logo='<img src="assets/img/invoice/webgarh.png"  />';
				endif;		
						
				$replace=array('SCHOOL'=>$school->school_name,
							  'NAME'=>$student->first_name." ".$student->last_name,
							  'FATHER_NAME'=>$student->father_name,
							  'CLASS'=>$student->session_name,
							  'SECTION'=>$student->section,
							  'ROLL_NO'=>$student->roll_no,
							  'GENDER'=>$student->sex,
							  'DATE_OF_BIRTH'=>$student->date_of_birth,
							  'SESSION'=>$session->start_date.' to '.$session->end_date,
							  'PHOTO'=>$student_image,
							  'LOGO'=>$school_logo
							  );		
				
				$content='';
				if(is_object($icard)):
				$content=$icard->format;
					if(count($replace)):
						foreach($replace as $k=>$v):
							$literal='{'.trim(strtoupper($k)).'}';
							$content=html_entity_decode(str_replace($literal, $v, trim($content)));					
						endforeach;
						echo $content; 
						echo "<input type='hidden' id='student_id' value='".$student_id."'/>";
						echo "<input type='hidden' id='doc' value='icard'/>";						
					endif;					  
				endif;				
			endif;			
		endif;	
	elseif($doc=='report'):
		$dmc=get_object('document_master',$doc_id);
		if($doc=='report' && is_object($dmc) && !empty($student_id)):
			/* Get Last Entry */	
			$QueryDocHis = new SchoolDocumentPrintHistory();
			$last_his=$QueryDocHis->getLastPrintDoc($school->id,$session_id,$ct_sec,$dmc->id,$student_id);		
			
			/* Load the last updated doc if is it */	
			if(is_object($last_his)): 
				$link=make_admin_url('view','print','print&id='.$last_his->id);
				$content='
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							<strong>Note:-</strong>
							You have already print this document on '.date('d M, Y').'. <a href="'.$link.'" target="_blank">Click here</a> to reprint this document.
						</div>';
				echo $content;	
			else:	
				#get exam Info
				$QueryG=new examinationGroup();
				$group=$QueryG->getRecord($exam_id);
			
				#get Student detail	
				$QueryStu = new studentSession();
				$student=$QueryStu->SingleStudentsWithSession($student_id,$session_id,$ct_sec);	
				
				#get session detail	
				$session=get_object('session',$session_id);					
				
							#get Student result
							$QueryRes=new examination();
							$Result=$QueryRes->single_student_all_examination_result($group->exam_id,$student_id);	
							
							$result_data='';$minimum='0'; $obtain='0'; $maximum='0';$sr='1';
							if(!empty($Result)):
								$result_data.="<table style='width: 100%; text-align: center; line-height: 40px;'>
												<tr><th style='text-align:left;width:10%;border-bottom:1px solid;' >Sr. No.</th><th style='text-align: left; width: 30%;border-bottom:1px solid;'>Subject Name</th><th style='width:20%;border-bottom:1px solid;'>Passing Marks</th><th style='width:20%;border-bottom:1px solid;'>Minimum Obtained</th><th style='width:20%;border-bottom:1px solid;'>Maximum Marks</th></tr>
												";
								foreach($Result as $r_k=>$r_v):					
										$result_data.="<tr><td style='text-align:left;width:10%;' >".$sr.".</td><td style='text-align: left; width: 30%;'>".$r_v->subject."</td><td style='width:20%;'>".$r_v->minimum_marks."</td><td style='width:20%;'>".$r_v->marks_obtained."</td><td style='width:20%;'>".$r_v->maximum_marks."</td></tr>";
										
										$minimum=$minimum+$r_v->minimum_marks;
										$obtain=$obtain+$r_v->marks_obtained;
										$maximum=$maximum+$r_v->maximum_marks;
										$sr++;
								endforeach;
								$result_data.="</table>";
							endif;		
						
							$replace=array('SCHOOL'=>$school->school_name,
										  'TITLE'=>$group->title,
										  'NAME'=>$student->first_name." ".$student->last_name,
										  'CLASS'=>$student->session_name,
										  'SECTION'=>$student->section,
										  'ROLL_NO'=>$student->roll_no,
										  'GENDER'=>$student->sex,
										  'MINIMUM_TOTAL'=>$minimum,
										  'OBTAIN_TOTAL'=>$obtain,
										  'MAXIMUM_TOTAL'=>$maximum,
										  'MARKS_RESULT'=>$result_data,
										  'SESSION'=>date('d M, Y',strtotime($session->start_date)).' - '.date('d M, Y',strtotime($session->end_date))
										  );
							$content='';
							if(is_object($dmc)):
							$content=$dmc->format;
								if(count($replace)):
									foreach($replace as $k=>$v):
										$literal='{'.trim(strtoupper($k)).'}';
										$content=html_entity_decode(str_replace($literal, $v, $content));
										$content=str_replace('&nbsp;','',$content);	
									endforeach;
									echo $content; 
									echo "<input type='hidden' id='student_id' value='".$student_id."'/>";	
									echo "<input type='hidden' id='doc' value='report'/>";										
								endif;					  
							endif;										  
			
			endif;			
		endif;
		
	elseif($doc=='character'):
		$character=get_object('document_master',$doc_id);
		if($doc=='character' && is_object($character) && !empty($student_id)):
			/* Get Last Entry */	
			$QueryDocHis = new SchoolDocumentPrintHistory();
			$last_his=$QueryDocHis->getLastPrintDoc($school->id,$session_id,$ct_sec,$character->id,$student_id);		
			
			/* Load the last updated doc if is it */	
			if(is_object($last_his)): 
				$link=make_admin_url('view','print','print&id='.$last_his->id);
				$content='
						<div class="alert alert-error">
							<button class="close" data-dismiss="alert"></button>
							<strong>Note:-</strong>
							You have already print this document on '.date('d M, Y').'. <a href="'.$link.'" target="_blank">Click here</a> to reprint this document.
						</div>';
				echo $content;	
			else:		
				#get Student detail	
				$QueryStu = new studentSession();
				$student=$QueryStu->SingleStudentsWithSession($student_id,$session_id,$ct_sec);	
				
				#get session detail	
				$session=get_object('session',$session_id);
			
				$im_obj=new imageManipulation();
				if($student->photo): 			
					$student_image="<img src='".$im_obj->get_image_echo('student','medium',$student->photo)."' style='max-width:165px'/>";
				else:
					$student_image='<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
				endif;
				if($school->logo):			
					$school_logo="<img src='".$im_obj->get_image_echo('school','medium',$school->logo)."' style='max-width:165px'/>";
				else:
					$school_logo='<img src="assets/img/invoice/webgarh.png"  />';
				endif;		
						
				$replace=array('SCHOOL'=>$school->school_name,
							  'NAME'=>$student->first_name." ".$student->last_name,
							  'FATHER_NAME'=>$student->father_name,
							  'CLASS'=>$student->session_name,
							  'SECTION'=>$student->section,
							  'ROLL_NO'=>$student->roll_no,
							  'GENDER'=>$student->sex,
							  'DATE_OF_BIRTH'=>$student->date_of_birth,
							  'SESSION'=>$session->start_date.' to '.$session->end_date,
							  'PHOTO'=>$student_image,
							  'LOGO'=>$school_logo
							  );		
				
				$content='';
				if(is_object($character)):
				$content=$character->format;
					if(count($replace)):
						foreach($replace as $k=>$v):
							$literal='{'.trim(strtoupper($k)).'}';
							$content=html_entity_decode(str_replace($literal, $v, trim($content)));					
						endforeach;
						echo $content; 
						echo "<input type='hidden' id='student_id' value='".$student_id."'/>";	
						echo "<input type='hidden' id='doc' value='character'/>";							
					endif;					  
				endif;				
			endif;			
		endif;		
	endif;?>