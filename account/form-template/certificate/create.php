  
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Staff Certificates
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    Staff Certificates
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>			
    <div class="clearfix"></div>			
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>	            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <? if ($type == 'select'): ?>
        <div class="row-fluid">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Staff Certificates </div>
                    <div class="tools">
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                    </div>
                </div>
                <div class="portlet-body"><br/>
                    <div class="row-fluid" >

                        <? if (array_key_exists('icard', $staff_certificates)): ?>
                            <div class='span6'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'insert', 'insert&type=staff&doc=icard') ?>">
                                        <img src="assets/img/icon/EXM2.png"/>
                                    </a>	
                                    <a href="<?= make_admin_url('certificate', 'insert', 'insert&type=staff&doc=icard') ?>" class='title'>Staff ID Card</a>
                                </div>					
                            </div>	
                        <? endif; ?>
                        <? if (array_key_exists('appri', $staff_certificates)): ?>
                            <div class='span6'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'insert', 'insert&type=staff&doc=appreciation') ?>">
                                        <img src="assets/img/icon/fee-2.png"/>
                                    </a>	
                                    <a href="<?= make_admin_url('certificate', 'insert', 'insert&type=staff&doc=appreciation') ?>" class='title'>Staff Appreciation Certificate</a>
                                </div>					
                            </div>					
                        <? endif; ?>
                        <? if (array_key_exists('teacher_experience', $staff_certificates)): ?>
                            <div class='span6'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'insert', 'insert&type=staff&doc=teacher_experience') ?>">
                                        <img src="assets/img/icon/list.png"/>
                                    </a>	
                                    <a href="<?= make_admin_url('certificate', 'insert', 'insert&type=staff&doc=teacher_experience') ?>" class='title'>Teacher Experience Certificate</a>
                                </div>					
                            </div>					
                        <? endif; ?>
                    </div>	
                </div>				
            </div>
        </div>	
    <? elseif ($type == 'staff'): ?>
        <? if ($doc == 'icard'): ?>
            <? if (!empty($staff_id)): ?>	
                <!-- END PAGE HEADER-->
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <div class="tile bg-blue">							
                        <a class="hidden-print" href='<?= make_admin_url('certificate', 'insert', 'insert&type=staff&doc=icard') ?>'>
                            <div class="corner"></div>
                            <div class="tile-body"><i class="icon-arrow-left"></i></div>
                            <div class="tile-object"><div class="name">Back</div></div>
                        </a>
                    </div>								
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/staff_shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>				
                <div class="row-fluid">						
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-user"></i>Staff ID Card </div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>	
                        <div class="portlet-body" id='doc_preview'>							
                            <!-- Default Format -->
                            <?= $content ?>
                            <input type='hidden' id='staff_id' value='<?= $staff_id ?>'/>		
                        </div>	
                    </div>
                    <? if (is_object($icard)): ?>					
                        <input type='hidden' id='doc_id' value='<?= $icard->id ?>'/>
                    <? endif; ?>
                    <input type='hidden' id='doc' value='icard'/>						
                </div>	
            <? else: ?>
                <!-- END PAGE HEADER-->
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/staff_shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>				
                <div class="row-fluid">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-user"></i>Staff List </div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>	
                        <div class="portlet-body">	
                            <form action='<?= make_admin_url('certificate', 'insert', 'insert') ?>' class="form-horizontal" method='get'>
                                <input type='hidden' name='Page' value='certificate'/>
                                <input type='hidden' name='action' value='insert'/>
                                <input type='hidden' name='section' value='insert'/>
                                <input type='hidden' name='type' value='staff'/>
                                <input type='hidden' name='doc' value='icard'/>

                                <table class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th class='hidden-480 sorting_disabled'></th>
                                            <th>Name</th>
                                            <th class="hidden-480 sorting_disabled">Category</th>
                                            <th class="hidden-480 sorting_disabled">Designation</th>												
                                        </tr>
                                    </thead>
                                    <? if (!empty($record)): ?>
                                        <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($record as $s_k => $object):
                                                ?>
                                                <tr class="odd gradeX current_staff" rel="<?= $object->id ?>">
                                                    <td class='hidden-480 sorting_disabled'><input type='radio' style='margin-left:0px;' name='staff_id' <?= ($staff_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                    <td><?= $object->title . " " . $object->first_name . " " . $object->last_name ?></td>											
                                                    <td class="hidden-480 sorting_disabled"><?= $object->staff_category ?></td>
                                                    <td class="hidden-480 sorting_disabled"><?= $object->designation ?></td>
                                                </tr>
                                                <?php
                                                $sr++;
                                            endforeach;
                                            ?>
                                        </tbody>
            <?php endif; ?>  
                                </table>
                                <div class="form-actions NoPaddingCenter">
                                    <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                    <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'insert', 'insert') ?>"> Cancel</a>
                                </div>									
                            </form>	
                        </div>
                    </div>
                </div>				
            <? endif; ?>					
        <? elseif ($doc == 'teacher_experience'): ?>
        <? if (!empty($staff_id)): ?>	
                <!-- END PAGE HEADER-->
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <div class="tile bg-blue">							
                        <a class="hidden-print" href='<?= make_admin_url('certificate', 'insert', 'insert&type=staff&doc=teacher_experience') ?>'>
                            <div class="corner"></div>
                            <div class="tile-body"><i class="icon-arrow-left"></i></div>
                            <div class="tile-object"><div class="name">Back</div></div>
                        </a>
                    </div>

            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/staff_shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>				
                <div class="row-fluid">						
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-user"></i>Teacher Experience Certificate </div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>	
                        <div class="portlet-body" id='doc_preview'>							
                            <!-- Default Format -->
            <?= $content ?>
                            <input type='hidden' id='staff_id' value='<?= $staff_id ?>'/>		
                        </div>	
                    </div>
                    <? if (is_object($teacher_experience)): ?>					
                        <input type='hidden' id='doc_id' value='<?= $teacher_experience->id ?>'/>
            <? endif; ?>
                    <input type='hidden' id='doc' value='teacher_experience'/>						
                </div>	
        <? else: ?>
                <!-- END PAGE HEADER-->
                <div class="clearfix"></div>
                <div class="tiles pull-right">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/staff_shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>				
                <div class="row-fluid">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-user"></i>Staff List </div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>	
                        <div class="portlet-body">	
                            <form action='<?= make_admin_url('certificate', 'insert', 'insert') ?>' class="form-horizontal" method='get'>
                                <input type='hidden' name='Page' value='certificate'/>
                                <input type='hidden' name='action' value='insert'/>
                                <input type='hidden' name='section' value='insert'/>
                                <input type='hidden' name='type' value='staff'/>
                                <input type='hidden' name='doc' value='teacher_experience'/>

                                <table class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th class='hidden-480 sorting_disabled'></th>
                                            <th>Name</th>
                                            <th class="hidden-480 sorting_disabled">Category</th>
                                            <th class="hidden-480 sorting_disabled">Designation</th>												
                                        </tr>
                                    </thead>
                                        <? if (!empty($record)): ?>
                                        <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($record as $s_k => $object):
                                                ?>
                                                <tr class="odd gradeX current_staff" rel="<?= $object->id ?>">
                                                    <td class='hidden-480 sorting_disabled'><input type='radio' style='margin-left:0px;' name='staff_id' <?= ($staff_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                    <td><?= $object->title . " " . $object->first_name . " " . $object->last_name ?></td>											
                                                    <td class="hidden-480 sorting_disabled"><?= $object->staff_category ?></td>
                                                    <td class="hidden-480 sorting_disabled"><?= $object->designation ?></td>
                                                </tr>
                                                <?php
                                                $sr++;
                                            endforeach;
                                            ?>
                                        </tbody>
            <?php endif; ?>  
                                </table>
                                <div class="form-actions NoPaddingCenter">
                                    <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                    <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'insert', 'insert') ?>"> Cancel</a>
                                </div>									
                            </form>	
                        </div>
                    </div>
                </div>				
            <? endif; ?>
    <? elseif ($doc == 'appreciation'): ?>
        <? if (!empty($staff_id)): ?>	
                <!-- END PAGE HEADER-->
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <div class="tile bg-blue">							
                        <a class="hidden-print" href='<?= make_admin_url('certificate', 'insert', 'insert&type=staff&doc=appreciation') ?>'>
                            <div class="corner"></div>
                            <div class="tile-body"><i class="icon-arrow-left"></i></div>
                            <div class="tile-object"><div class="name">Back</div></div>
                        </a>
                    </div>

            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/staff_shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>				
                <div class="row-fluid">						
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-user"></i>Staff Appreciation Certificate </div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>	
                        <div class="portlet-body" id='doc_preview'>							
                            <!-- Default Format -->
            <?= $content ?>
                            <input type='hidden' id='staff_id' value='<?= $staff_id ?>'/>		
                        </div>	
                    </div>
                    <? if (is_object($appri)): ?>					
                        <input type='hidden' id='doc_id' value='<?= $appri->id ?>'/>
                <? endif; ?>
                    <input type='hidden' id='doc' value='appreciation'/>						
                </div>	
        <? else: ?>
                <!-- END PAGE HEADER-->
                <div class="clearfix"></div>
                <div class="tiles pull-right">
            <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/staff_shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>				
                <div class="row-fluid">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-user"></i>Staff List </div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>	
                        <div class="portlet-body">	
                            <form action='<?= make_admin_url('certificate', 'insert', 'insert') ?>' class="form-horizontal" method='get'>
                                <input type='hidden' name='Page' value='certificate'/>
                                <input type='hidden' name='action' value='insert'/>
                                <input type='hidden' name='section' value='insert'/>
                                <input type='hidden' name='type' value='staff'/>
                                <input type='hidden' name='doc' value='appreciation'/>

                                <table class="table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th class='hidden-480 sorting_disabled'></th>
                                            <th>Name</th>
                                            <th class="hidden-480 sorting_disabled">Category</th>
                                            <th class="hidden-480 sorting_disabled">Designation</th>												
                                        </tr>
                                    </thead>
                                        <? if (!empty($record)): ?>
                                        <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($record as $s_k => $object):
                                                ?>
                                                <tr class="odd gradeX current_staff" rel="<?= $object->id ?>">
                                                    <td class='hidden-480 sorting_disabled'><input type='radio' style='margin-left:0px;' name='staff_id' <?= ($staff_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                    <td><?= $object->title . " " . $object->first_name . " " . $object->last_name ?></td>											
                                                    <td class="hidden-480 sorting_disabled"><?= $object->staff_category ?></td>
                                                    <td class="hidden-480 sorting_disabled"><?= $object->designation ?></td>
                                                </tr>
                                                <?php
                                                $sr++;
                                            endforeach;
                                            ?>
                                        </tbody>
            <?php endif; ?>  
                                </table>
                                <div class="form-actions NoPaddingCenter">
                                    <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                    <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'insert', 'insert') ?>"> Cancel</a>
                                </div>									
                            </form>	
                        </div>
                    </div>
                </div>				
        <? endif; ?>				
    <? endif; ?>	
<? endif; ?>
</div>
</div>	


<script type="text/javascript">
    jQuery(document).ready(function () {
        /* show doc_preview  */
        $(".current_staff").live("click", function () {
            var staff_id = $(this).attr('rel');

            $(".current_staff").find('span').removeClass('checked');
            $(".current_staff").find('td input:radio').prop('checked', false);
            $(this).find('span').addClass('checked');

            $(this).find('td input:radio').prop('checked', true);

            var doc_id = $('#doc_id').val();
            var doc = $('#doc').val();

            var dataString = 'staff_id=' + staff_id + '&doc_id=' + doc_id + '&doc=' + doc;
            $("#doc_preview").html('<div style="width:100%;text-align:center;padding-top:50px;"><img src="assets/img/ajax-loading.gif"/></div>');

            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_staff', 'ajax_staff&temp=certificate'); ?>",
                data: dataString,
                success: function (data, textStatus)
                {
                    $('#doc_preview').html(data);
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $("#print").live("click", function () {
        var staff_id = $('#staff_id').val();
        var doc_id = $('#doc_id').val();
        var doc = $('#doc').val();

        var dataString = 'staff_id=' + staff_id + '&doc_id=' + doc_id + '&doc=' + doc;

        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_staff_print', 'ajax_staff_print&temp=certificate'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var DocumentContainer = document.getElementById(data);
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
</script>	