<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentExaminationClass.php');

if($_POST):
isset($_POST['session_id'])?$session_id=$_POST['session_id']:$session_id='';
isset($_POST['ct_sec'])?$ct_sec=$_POST['ct_sec']:$ct_sec='';
isset($_POST['exam_id'])?$exam_id=$_POST['exam_id']:$exam_id='';
		

		 #get exam Info
		 $QueryG=new examinationGroup();
		 $group=$QueryG->getRecord($exam_id);
		 
		if(!empty($group)):
		 #Get exam Students result
		 $QueryOb=new examinationMarks();
		 $record=$QueryOb->getExamStudents($group->exam_id);
		else:
		 $record='';	
		endif; 
?>			
			
			<table class="table table-striped table-hover" >
				<thead>
					 <tr>
							<th class='hidden-480 sorting_disabled'></th>
							<th class='hidden-480 sorting_disabled'>Roll No.</th>
							<th>Name</th>
							<th>Father Name</th>							
							<th class='hidden-480 sorting_disabled'>Exam Titles</th>												
					</tr>
				</thead>
					<? if(!empty($record)):?>
							<tbody>
								<?php $sr=1;foreach($record as $s_k=>$object):
										$ShowObj=new examination();											
										$show=$ShowObj->getExamNames($group->exam_id,$object->id);?>
									<tr class="odd gradeX current_student_exam" rel="<?=$object->id?>">
										<td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' value='<?=$object->id?>' checked /> </td>												
										<td class='hidden-480 sorting_disabled'><?=$object->roll_no?></td>	
										<td><?=$object->first_name." ".$object->last_name?></td>	
										<td><?=$object->father_name?></td>	
										<td style='text-align:left' class="hidden-480"><?=$show?></td>								
									</tr>
								<?php $sr++;
									endforeach;?>
							</tbody>
					<? else:?>
							<tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>													
				   <?php endif;?>  
			</table>	
				<div class="form-actions NoPaddingCenter">
						 <? if(!empty($record)):?>
						 <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
						  <?php endif;?>  
						 <a name="cancel" class="btn" href="<?=make_admin_url('certificate','list','list')?>"> Cancel</a>
				 </div>				
<?		
endif;
?>
