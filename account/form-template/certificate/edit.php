  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Student Certificates
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
									<? if($type!='select'):?>
                                    <li>
                                            <i class="icon-list"></i>
                                            <a href="<?php echo make_admin_url('certificate', 'list', 'list');?>">List</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 									
									<? endif;?>
                                    <li class="last">
                                        Student Certificates
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            					<div class="clearfix"></div>
						<div class="tiles pull-right">
								
								<div class="tile bg-blue">							
									<a class="hidden-print" href='<?=make_admin_url('certificate','list','list&type=student&doc='.$doc.'&session_id='.$session_id.'&ct_sec='.$ct_sec.'&student_id='.$student_id.'&group_id='.$group_id)?>'>
										<div class="corner"></div>
										<div class="tile-body"><i class="icon-arrow-left"></i></div>
										<div class="tile-object"><div class="name">Back</div></div>
									</a>
								</div>	
															
						</div>            
					<div class="clearfix"></div>				
					<div class="row-fluid">						
							<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption"><i class="icon-user"></i>Edit Certificate </div>
									<div class="tools">
										<a href="#portlet-config" data-toggle="modal" class="config"></a>
										<a href="javascript:;" class="reload"></a>
									</div>
								</div>	
								<div class="portlet-body">	
									<form class="form-horizontal" action="<?php echo make_admin_url('certificate', 'update', 'update');?>" method="POST" enctype="multipart/form-data" id="validation">
										<div class="control-group">
													<textarea id="document_text" class="span12 ckeditor m-wrap" name="document_text">
														<?=html_entity_decode($content)?>
													</textarea>
										</div> 
										<div style='clear:both;'></div> 
										<div class="form-actions">
												<input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" /> 
													<input type="hidden" name="session_id" value="<?=$session_id?>" tabindex="7" /> 
													<input type="hidden" name="ct_sec" value="<?=$ct_sec?>" tabindex="7" /> 
													<input type="hidden" name="student_id" value="<?=$student_id?>" tabindex="7" /> 
													<input type="hidden" name="doc" value="<?=$doc?>" tabindex="7" /> 
													<input type="hidden" name="type" value="<?=$type?>" tabindex="7" /> 												 
													<input type="hidden" name="group_id" value="<?=$group_id?>" tabindex="7" /> 												 
												<input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
												<a class="btn" href='<?=make_admin_url('certificate','list','list&type=student&doc='.$doc.'&session_id='.$session_id.'&ct_sec='.$ct_sec.'&student_id='.$student_id.'&group_id='.$group_id)?>'>Cancel</a>
										</div>
									</form>	
								</div>	
									<div style='clear:both;'></div>
							</div>			
					</div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

