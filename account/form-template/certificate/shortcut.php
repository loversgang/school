<div class="tiles pull-right">
    <?php if (isset($_GET['action_perform']) && $_GET['action_perform'] == 'edit_details') { ?>
        <div class="tile bg-blue">							
            <a class="hidden-print" href='<?= make_admin_url('certificate', 'list', 'list&type=student&doc=yearly_dmc&session_id=' . $session_id . '&ct_sec=' . $ct_sec) ?>'>
                <div class="corner"></div>
                <div class="tile-body"><i class="icon-arrow-left"></i></div>
                <div class="tile-object"><div class="name">Back</div></div>
            </a>
        </div>
        <div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('certificate', 'list', 'list'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-list"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    List Certificates
                </div>
            </div>
        </a>   
    </div>
    <div class="tile bg-purple <?php echo ($section == 'list_indicators') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('certificate', 'list_indicators', 'list_indicators'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-list"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    List Indicators
                </div>
            </div>
        </a>   
    </div>
    <div class="tile bg-blue <?php echo ($section == 'create_indicators') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('certificate', 'create_indicators', 'create_indicators'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-plus"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    New Indicator
                </div>
            </div>
        </a> 
    </div>
        <div class="tile bg-green">							
            <a class="hidden-print" href='<?= make_admin_url('certificate', 'list', 'list', 'type=student&doc=yearly_dmc&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&student_id=' . $student_id . '&submit=Submit') ?>'>
                <div class="corner"></div>
                <div class="tile-body"><i class="icon-search"></i></div>
                <div class="tile-object"><div class="name">View DMC</div></div>
            </a>
        </div>
    <?php } ?>
</div>