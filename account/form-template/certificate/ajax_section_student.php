<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');

if($_POST):
isset($_POST['session_id'])?$session_id=$_POST['session_id']:$session_id='';
isset($_POST['ct_sec'])?$ct_sec=$_POST['ct_sec']:$ct_sec='';
		

	#Get Section Students
	$QueryOb=new studentSession();
	$record=$QueryOb->sectionSessionStudents($session_id,$ct_sec);
?>			
		<table class="table table-striped table-hover" id='ajex_student'>
			<thead>
				 <tr>
						<th class='hidden-480 sorting_disabled'></th>
						<th>Reg ID.</th>
						<th class='hidden-480 sorting_disabled'>Roll No.</th>	
						<th>Name</th>
						<th>Father Name</th>																
				</tr>
			</thead>
				<? if(!empty($record)):?>
				<tbody>
					<?php $sr=1;foreach($record as $s_k=>$object):?>
						<tr class="odd gradeX current_student" rel="<?=$object->id?>">	
							<td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' value='<?=$object->id?>' checked /> </td>												
							<td><?=$object->reg_id?></td>	
							<td class='hidden-480 sorting_disabled'><?=$object->roll_no?></td>
							<td><?=$object->first_name." ".$object->last_name?></td>
							<td><?=$object->father_name?></td>												
																			
						</tr>
					<?php $sr++;
						endforeach;?>
				</tbody>
				<? else:?>
				  <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>									
			   <?php endif;?>  
		</table>			
<?		
endif;
?>
