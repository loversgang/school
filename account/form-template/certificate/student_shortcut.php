<div class="tiles pull-right">
    <div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('certificate', 'list', 'list'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-list"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    List Certificates
                </div>
            </div>
        </a>   
    </div>
    <div class="tile bg-purple <?php echo ($section == 'list_indicators') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('certificate', 'list_indicators', 'list_indicators'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-list"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    List Indicators
                </div>
            </div>
        </a>   
    </div>
    <div class="tile bg-blue <?php echo ($section == 'create_indicators') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('certificate', 'create_indicators', 'create_indicators'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-plus"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    New Indicator
                </div>
            </div>
        </a> 
    </div>
    <? if (!empty($student_id) && $printed == '0'): ?>
        <div class="tile bg-yellow" id='print_document'>							
            <a class="hidden-print" id='print'>
                <div class="corner"></div>
                <div class="tile-body"><i class="icon-print"></i></div>
                <div class="tile-object"><div class="name">Print</div></div>
            </a>
        </div>	
    <? endif; ?>
</div>