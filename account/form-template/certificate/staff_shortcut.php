<div class="tiles pull-right">

        <div class="tile bg-green <?php echo ($section=='insert')?'selected':''?>">
            <a href="<?php echo make_admin_url('certificate', 'insert', 'insert');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                List Certificates
                        </div>
                </div>
            </a>   
        </div>
		<? if(!empty($staff_id)):?>
					<div class="tile bg-yellow" id='print_document'>							
						<a class="hidden-print" id='print'>
							<div class="corner"></div>
							<div class="tile-body"><i class="icon-print"></i></div>
							<div class="tile-object"><div class="name">Print</div></div>
						</a>
					</div>	
		<? endif;?>

</div>