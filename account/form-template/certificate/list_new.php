<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">Manage Student Certificates</h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a>
                    <i class="icon-angle-right"></i>
                </li>
                <? if ($type != 'select'): ?>
                    <li>
                        <i class="icon-list"></i>
                        <a href="<?php echo make_admin_url('certificate', 'list', 'list'); ?>">List</a>
                        <i class="icon-angle-right"></i>
                    </li>
                <? endif; ?>
                <li class="last">
                    Student Certificates
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->

    <? if ($type == 'select'): ?>
        <? $total_cer = ''; ?>
        <div class="row-fluid">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i> Student Certificates</div>
                    <div class="tools">
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                        <a href="javascript:;" class="reload"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row-fluid" >
                        <? if (array_key_exists('icard', $certificates)): ?>
                            <? $total_cer++; ?>
                            <div class='span3'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=icard') ?>"><img src="assets/img/icon/EXM2.png"/></a>
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=icard') ?>" class='title'>ID Card</a>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if (array_key_exists('character', $certificates)): ?>
                            <? $total_cer++; ?>
                            <div class='span3'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=character') ?>"><img src="assets/img/icon/fee-1.png"/></a>
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=character') ?>" class='title'>Character Certificate</a>
                                </div>
                            </div>
                        <? endif; ?>
                        <? if (array_key_exists('appri', $certificates)): ?>
                            <? $total_cer++; ?>
                            <div class='span3'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=appreciation') ?>">
                                        <img src="assets/img/icon/fee-2.png"/>
                                    </a>	
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=appreciation') ?>" class='title'>Appreciation Certificate</a>
                                </div>					
                            </div>	
                        <? endif; ?>
                        <?
                        if ($total_cer == '4'):
                            echo '</div><div class="row-fluid">';
                        endif;
                        ?>					
                        <? if (array_key_exists('dmc', $certificates)): ?>
                            <div class='span3'>
                                <? $total_cer++; ?>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=report') ?>">
                                        <img src="assets/img/icon/attendence.png"/>
                                    </a>	
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=report') ?>" class='title'>Report Card</a>
                                </div>					
                            </div>
                        <? endif; ?>	
                        <?
                        if ($total_cer == '4'):
                            echo '</div><div class="row-fluid">';
                        endif;
                        ?>						
                        <? if (array_key_exists('participation', $certificates)): ?>
                            <? $total_cer++; ?>
                            <div class='span3'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=participation') ?>">
                                        <img src="assets/img/icon/groups.png"/>
                                    </a>	
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=participation') ?>" class='title'>Participation Certificate</a>
                                </div>					
                            </div>	
                        <? endif; ?>	
                        <? if ($total_cer == '4'): ?>
                        </div><div class="row-fluid">
                        <? endif; ?>	

                        <? if (array_key_exists('sports', $certificates)): ?>
                            <? $total_cer++; ?>
                            <div class='span3'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=sports') ?>">
                                        <img src="assets/img/icon/sports.png"/>
                                    </a>	
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=sports') ?>" class='title'>Sports Certificate</a>
                                </div>					
                            </div>	
                        <? endif; ?>		
                        <? if ($total_cer == '4'): ?>
                        </div><div class="row-fluid" >
                        <? endif; ?>						
                        <? if (array_key_exists('leaving', $certificates)): ?>
                            <? $total_cer++; ?>
                            <div class='span3'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=leaving') ?>">
                                        <img src="assets/img/icon/fee-3.png"/>
                                    </a>	
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=leaving') ?>" class='title'>School Leaving Certificate</a>
                                </div>					
                            </div>	
                        <? endif; ?>	
                        <? if (array_key_exists('transfer', $certificates)): ?>
                            <? $total_cer++; ?>
                            <div class='span3'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=transfer') ?>">
                                        <img src="assets/img/icon/list.png"/>
                                    </a>	
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=transfer') ?>" class='title'>Transfer Certificate</a>
                                </div>					
                            </div>	
                        <? endif; ?>
                        <? if (array_key_exists('test_report_card', $certificates)): ?>
                            <? $total_cer++; ?>
                            <div class='span3'>
                                <div class="easy-pie-chart">
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=test_report_card') ?>">
                                        <img src="assets/img/icon/attendence.png"/>
                                    </a>	
                                    <a href="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=test_report_card') ?>" class='title'>Test Report Card</a>
                                </div>					
                            </div>
                        <? endif; ?>
                    </div>	
                </div>				
            </div>
        </div>	
    <? elseif ($type == 'student'): ?>
        <? if ($doc == 'icard'): ?>
            <? if (!empty($student_id)): ?>	
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <div class="tile bg-blue">							
                        <a class="hidden-print" href='<?= make_admin_url('certificate', 'list', 'list&type=student&doc=icard&session_id=' . $session_id . '&ct_sec=' . $ct_sec) ?>'>
                            <div class="corner"></div>
                            <div class="tile-body"><i class="icon-arrow-left"></i></div>
                            <div class="tile-object"><div class="name">Back</div></div>
                        </a>
                    </div>							
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>				
                <div class="row-fluid">					
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-user"></i>Student ID Card </div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>	
                        <div class="portlet-body">							
                            <!-- Default Format -->
                            <?= $content ?>
                            <input type='hidden' id='student_id' value='<?= $student_id ?>'/>		
                        </div>	
                    </div>
                    <? if (is_object($icard)): ?>					
                        <input type='hidden' id='doc_id' value='<?= $icard->id ?>'/>
                    <? endif; ?>
                    <input type='hidden' id='doc' value='icard'/>

                </div>	

            <? else: ?>						
                <div class="clearfix"></div>
                <div class="tiles pull-right">
                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                </div>            
                <div class="clearfix"></div>				
                <div class="row-fluid">	
                    <form action='<?= make_admin_url('certificate', 'list', 'list') ?>' class="form-horizontal" method='get'>
                        <input type='hidden' name='Page' value='certificate'/>
                        <input type='hidden' name='action' value='list'/>
                        <input type='hidden' name='section' value='list'/>
                        <input type='hidden' name='type' value='student'/>
                        <input type='hidden' name='doc' value='icard'/>						
                        <!-- select Session And Section -->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-bar-chart"></i>Select Session & Section </div>
                                <div class="tools">
                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                    <a href="javascript:;" class="reload"></a>
                                </div>
                            </div>	
                            <div class="portlet-body">
                                <div class="row-fluid">
                                    <div class='span4'>
                                        <label class="control-label" style='width:auto;'>Select Session</label>
                                        <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                            <?php
                                            if (!empty($All_Session)) :
                                                $session_sr = 1;
                                                foreach ($All_Session as $key => $session):
                                                    ?>
                                                    <option value='<?= $session->id ?>' <?
                                                    if ($session->id == $session_id) :
                                                        echo 'selected';
                                                    endif;
                                                    ?> ><?= ucfirst($session->title) ?></option>
                                                            <?
                                                            $session_sr++;
                                                        endforeach;
                                                    endif;
                                                    ?>
                                        </select>
                                    </div>
                                    <div class='span3' id='ajex_section'>
                                        <label class="control-label" style='width:auto;'>Select Section</label>
                                        <? if ($QuerySec->GetNumRows() > 0): ?>
                                            <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                                    <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                                <? endwhile; ?>	
                                            </select>
                                        <? else: ?>
                                            <input type='text' class="span10" value='No Section Found.' disabled />
                                        <? endif; ?>											
                                    </div>
                                    <div class='span2'>	
                                        <label class="control-label">&nbsp;</label>
                                        <span class='btn medium green' id='go'>Go</span>									
                                    </div>	
                                </div>	
                            </div>
                        </div>		

                        <!-- List OF Student -->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-user"></i>Student List </div>
                                <div class="tools">
                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                    <a href="javascript:;" class="reload"></a>
                                </div>
                            </div>	
                            <div class="portlet-body">	

                                <table class="table table-striped table-hover" id='ajex_student'>
                                    <thead>
                                        <tr>
                                            <th class='hidden-480 sorting_disabled'></th>
                                            <th>Reg ID.</th>
                                            <th class='hidden-480 sorting_disabled'>Roll No.</th>
                                            <th>Name</th>
                                            <th>Father Name</th>																									
                                        </tr>
                                    </thead>
                                    <? if (!empty($record)): ?>
                                        <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($record as $s_k => $object):
                                                ?>
                                                <tr class="odd gradeX current_student" rel="<?= $object->id ?>">	
                                                    <td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' <?= ($student_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                    <td><?= $object->reg_id ?></td>
                                                    <td class='hidden-480 sorting_disabled'><?= $object->roll_no ?></td>															
                                                    <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                    <td><?= $object->father_name ?></td>												

                                                </tr>
                                                <?php
                                                $sr++;
                                            endforeach;
                                            ?>
                                        </tbody>
                                    <? else: ?>
                                        <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>									
                                    <?php endif; ?>  
                                </table>	
                                <div class="form-actions NoPaddingCenter">
                                    <? if (!empty($record)): ?>
                                        <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                    <? endif; ?>
                                    <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'list', 'list') ?>"> Cancel</a>
                                </div>								
                            </div>							
                        </div>
                    </form>		
                </div>	

            <? endif; ?>				
            <!-- Character Certificate-->		
        <? elseif ($doc == 'character'): ?>

            <? if (!empty($student_id)): ?>	                               
                <div class="row-fluid">						
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-user"></i>Character Certificate </div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a href="javascript:;" class="reload"></a>
                            </div>
                        </div>	
                        <div class="portlet-body">
                            <? if (is_object($last_his)): ?>
                                <?= $content ?>
                                <input type='hidden' id='student_id' value='<?= $student_id ?>'/>
                            <? elseif (isset($_POST['character_certificate'])): ?>                                  
                                <div class="clearfix"></div>
                                <div class="tiles pull-right">
                                    <div class="tile bg-blue">							
                                        <a class="hidden-print" href='<?= make_admin_url('certificate', 'list', 'list&type=student&doc=character&session_id=' . $session_id . '&ct_sec=' . $ct_sec) ?>'>
                                            <div class="corner"></div>
                                            <div class="tile-body"><i class="icon-arrow-left"></i></div>
                                            <div class="tile-object"><div class="name">Back</div></div>
                                        </a>
                                    </div>							
                                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                                </div>            
                                <div class="clearfix"></div>				
                                <div class="row-fluid">						
                                    <div class="portlet box green">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Student Character Certificate </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">							
                                            <!-- Default Format -->
                                            <?= $content ?>
                                            <input type='hidden' id='student_id' value='<?= $student_id ?>'/>		
                                        </div>	
                                    </div>
                                    <? if (is_object($character)): ?>					
                                        <input type='hidden' id='doc_id' value='<?= $character->id ?>'/>
                                    <? endif; ?>
                                    <input type='hidden' id='doc' value='character'/>

                                </div>                             





                            <? else: ?>
                                <div class="row-fluid">
                                    <form class="form-horizontal" action="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=character&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&student_id=' . $student_id) ?>" method="POST" enctype="multipart/form-data" id="validation">
                                        <!-- / Box -->
                                        <div class="span12">
                                            <!-- BEGIN EXAMPLE TABLE PORTLET-->

                                            <div class="portlet-body form">      
                                                <h4 class="form-section hedding_inner">Information</h4>
                                                <div class="row-fluid">
                                                    <div class="span6 ">


                                                        <div class="control-group">
                                                            <label class="control-label" for="SCHOOL_NAME">School Name<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="SCHOOL_NAME" value="<?php echo $school->school_name; ?>"  class="span10 m-wrap validate[required]"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="SCHOOL_ADDRESS">School Address<span class="required"></span></label>
                                                            <div class="controls">
                                                                <textarea name="SCHOOL_ADDRESS" class="span10 m-wrap" ><?php echo $school->address1 . ' ' . $school->city . ' ' . $school->district ?></textarea>
                                                            </div>
                                                        </div>


                                                        <div class="control-group">
                                                            <label class="control-label" for="AFF_NUM">Affiliation No.<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="AFF_NUM" value="<?php echo $school->affiliation_code; ?>"  class="span10 m-wrap validate[required]"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="SLC_NUMBER">Reference No.<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="SLC_NUMBER" value=""  class="span10 m-wrap validate[required]"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="S_NUMBER">School No.<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="S_NUMBER" value="23108"  class="span10 m-wrap validate[required]"/>
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="NUMBER">Admission No.<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="NUMBER" value="<?php echo $student->reg_id; ?>"  class="span10 m-wrap validate[required]"/>
                                                            </div>
                                                        </div>

                                                        <div class="control-group">
                                                            <label class="control-label" for="NAME">This is Certify that<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="NAME" value="<?php echo $student->first_name . " " . $student->last_name; ?>"  class="span10 m-wrap validate[required]"/>
                                                            </div>
                                                        </div> 

                                                        <div class="control-group">
                                                            <label class="control-label" for="FATHER_NAME">S/o, D/o Sh.<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="FATHER_NAME" value="<?php echo $student->father_name ?>"  class="span10 m-wrap validate[required]"/>
                                                            </div>
                                                        </div> 
                                                        <div class="control-group">
                                                            <label class="control-label" for="SESSION">He / She Passed / Appeared<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="SESSION" value="<?php echo $session->start_date . ' to ' . $session->end_date ?>"  class="span10 m-wrap validate[required]" />
                                                            </div>
                                                        </div> 


                                                    </div>	
                                                    <div class="span6 ">



                                                        <div class="control-group">
                                                            <label class="control-label" for="ROLL_NO">Examination under Roll No.<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="ROLL_NO" value="<?php echo $student->roll_no; ?>" i class="span10 m-wrap validate[required]"/>
                                                            </div>
                                                        </div>												

                                                        <div class="control-group">
                                                            <label class="control-label" for="">Securing<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="SECURING" value=""  class="span10 m-wrap validate[required]"/>
                                                            </div>
                                                        </div>	


                                                        <div class="control-group">
                                                            <label class="control-label" for="MARKS">Marks in<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="MARKS" value=""  class="span10 m-wrap validate[required]"/>
                                                            </div>    
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="DOB">His /Her date of brith according to school <span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="DOB" value="<?php echo $student->date_of_birth; ?>"  class="span10 m-wrap validate[required]"/>
                                                            </div>    
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="GAME_SPORT">Game & Sport <span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="GAME_SPORT" value=""  class="span10 m-wrap validate[required]" />
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="CHARACTER">Character & Conduct</label>
                                                            <div class="controls">
                                                                <textarea name="CHARACTER"  value="" class="span10 m-wrap"></textarea>                                                                                                            
                                                            </div>
                                                        </div>
                                                        <div class="control-group">
                                                            <label class="control-label" for="REMARKS">Remarks<span class="required"></span></label>
                                                            <div class="controls">
                                                                <input type="text" name="REMARKS" value=""  class="span10 m-wrap validate[required]"/>
                                                            </div>
                                                        </div>




                                                    </div>	
                                                </div>	



                                                <div class="form-actions">

                                                    <input class="btn blue" type="submit" name="character_certificate" value="Submit" tabindex="7" /> 
                                                    <a href="<?php echo make_admin_url('certificate', 'list', 'list&type=student&doc=character&'); ?>" class="btn" name="cancel" > Cancel</a>
                                                </div>
                                            </div>

                                        </div>
                                        <? if (is_object($character)): ?>					
                                            <input type='hidden' name='doc_id' value='<?= $character->id ?>'/>
                                        <? endif; ?>
                                        <input type='hidden' name='doc' value='character'/>
                                    </form>
                                    <div class="clearfix"></div>
                                </div>

                            <? endif; ?>

                        <? else: ?>
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">
                                <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>            
                            <div class="clearfix"></div>				
                            <div class="row-fluid">	
                                <form action='<?= make_admin_url('certificate', 'list', 'list') ?>' class="form-horizontal" method='get'>
                                    <input type='hidden' name='Page' value='certificate'/>
                                    <input type='hidden' name='action' value='list'/>
                                    <input type='hidden' name='section' value='list'/>
                                    <input type='hidden' name='type' value='student'/>
                                    <input type='hidden' name='doc' value='character'/>						

                                    <!-- select Session And Section -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-bar-chart"></i>Select Session & Section </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">
                                            <div class="row-fluid">
                                                <div class='span4'>
                                                    <label class="control-label" style='width:auto;'>Select Session</label>
                                                    <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                        <?php
                                                        if (!empty($All_Session)) :
                                                            $session_sr = 1;
                                                            foreach ($All_Session as $key => $session):
                                                                ?>
                                                                <option value='<?= $session->id ?>' <?
                                                                if ($session->id == $session_id) :
                                                                    echo 'selected';
                                                                endif;
                                                                ?> ><?= ucfirst($session->title) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                    </select>
                                                </div>
                                                <div class='span3' id='ajex_section'>
                                                    <label cl ass="control-label" style='width:auto;'>Select Section</label>
                                                    <? if ($QuerySec->GetNumRows() > 0): ?>
                                                        <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                            <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                                                <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                                            <? endwhile; ?>	
                                                        </select>
                                                    <? else: ?>
                                                        <input type='text' class="span10" value='No Section Found.' disabled />
                                                    <? endif; ?>											
                                                </div>
                                                <div class='span2'>	
                                                    <label class="control-label">&nbsp;</label>
                                                    <span class='btn medium green' id='go'>Go</span>									
                                                </div>	
                                            </div>	
                                        </div>
                                    </div>		

                                    <!-- List OF Student -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Student List </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">								
                                            <table class="table table-striped table-hover" id='ajex_student'>
                                                <thead>
                                                    <tr>
                                                        <th class='hidden-480 sorting_disabled'></th>
                                                        <th>Reg ID.</th>
                                                        <th class='hidden-480 sorting_disabled'>Roll No.</th>
                                                        <th>Name</th>
                                                        <th>Father Name</th>

                                                    </tr>
                                                </thead>
                                                <? if (!empty($record)): ?>
                                                    <tbody>
                                                        <?php
                                                        $sr = 1;
                                                        foreach ($record as $s_k => $object):
                                                            ?>
                                                            <tr class="odd gradeX current_student" rel="<?= $object->id ?>">	
                                                                <td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' <?= ($student_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                                <td><?= $object->reg_id ?></td>	
                                                                <td class='hidden-480 sorting_disabled'><?= $object->roll_no ?></td>
                                                                <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                                <td><?= $object->father_name ?></td>			
                                                            </tr>
                                                            <?php
                                                            $sr++;
                                                        endforeach;
                                                        ?> 
                                                    </tbody>
                                                <? else: ?>
                                                    <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>									
                                                <?php endif; ?>  
                                            </table>	
                                            <div class="form-actions NoPaddingCenter">
                                                <? if (!empty($record)): ?>
                                                    <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                                <?php endif; ?>  
                                                <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'list', 'list') ?>"> Cancel</a>
                                            </div>								
                                        </div>							
                                    </div>
                                </form>		
                            </div>	
                        <? endif; ?>	


                        <!-- Appreciation Certificate-->
                    <? elseif ($doc == 'appreciation'): ?>
                        <? if (!empty($student_id)): ?>
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">
                                <div class="tile bg-blue">
                                    <a class="hidden-print" href='<?= make_admin_url('certificate', 'list', 'list&type=student&doc=appreciation&session_id=' . $session_id . '&ct_sec=' . $ct_sec) ?>'>
                                        <div class="corner"></div>
                                        <div class="tile-body"><i class="icon-arrow-left"></i></div>
                                        <div class="tile-object"><div class="name">Back</div></div>
                                    </a>
                                </div>							
                                <?php include_once (DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>            
                            <div class="clearfix"></div>				
                            <div class="row-fluid">						
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-user"></i>Student Appreciation Certificate </div>
                                        <div class="tools">
                                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                            <a href="javascript:;" class="reload"></a>
                                        </div>
                                    </div>	
                                    <div class="portlet-body">							
                                        <!-- Default Format -->
                                        <?= $content ?>
                                        <input type='hidden' id='student_id' value='<?= $student_id ?>'/>		
                                    </div>	
                                </div>
                                <? if (is_object($appri)): ?>					
                                    <input type='hidden' id='doc_id' value='<?= $appri->id ?>'/>
                                <? endif; ?>
                                <input type='hidden' id='doc' value='appreciation'/>

                            </div>

                        <? else: ?>
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">
                                <?php include_once (DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>            
                            <div class="clearfix"></div>				
                            <div class="row-fluid">	
                                <form action='<?= make_admin_url('certificate', 'list', 'list') ?>' class="form-horizontal" method='get'>
                                    <input type='hidden' name='Page' value='certificate'/>
                                    <input type='hidden' name='action' value='list'/>
                                    <input type='hidden' name='section' value='list'/>
                                    <input type='hidden' name='type' value='student'/>
                                    <input type='hidden' name='doc' value='appreciation'/>						

                                    <!-- select Session And Section -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-bar-chart"></i>Select Session & Section </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">
                                            <div class="row-fluid">
                                                <div class='span4'>
                                                    <label class="control-label" style='width:auto;'>Select Session</label>
                                                    <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                        <?php
                                                        if (!empty($All_Session)) :
                                                            $session_sr = 1;
                                                            foreach ($All_Session as $key => $session):
                                                                ?>
                                                                <option value='<?= $session->id ?>' <?
                                                                if ($session->id == $session_id):
                                                                    echo 'selected';
                                                                endif;
                                                                ?> ><?= ucfirst($session->title) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                    </select>
                                                </div>
                                                <div class='span3' id='ajex_section'>
                                                    <label class="control-label" style='width:auto;'>Select Section</label>
                                                    <? if ($QuerySec->GetNumRows() > 0): ?>
                                                        <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                            <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                                                <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                                            <? endwhile; ?>	
                                                        </select>
                                                    <? else: ?>
                                                        <input type='text' class="span10" value='No Section Found.' disabled />
                                                    <? endif; ?>											
                                                </div>
                                                <div class='span2'>	
                                                    <label class="control-label">&nbsp;</label>
                                                    <span class='btn medium green' id='go'>Go</span>									
                                                </div>	
                                            </div>	
                                        </div>
                                    </div>		

                                    <!-- List OF Student -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Student List </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">								
                                            <table class="table table-striped table-hover" id='ajex_student'>
                                                <thead>
                                                    <tr>
                                                        <th class='hidden-480 sorting_disabled'></th>
                                                        <th>Reg ID.</ th>
                                                        <th class='hidden-480 sorting_disabled'>Roll No.</th>
                                                        <th>Name</th>
                                                        <th>Father Name</th>																									
                                                    </tr>
                                                </thead>
                                                <? if (!empty($record)): ?>
                                                    <tbody>
                                                        <?php
                                                        $sr = 1;
                                                        foreach ($record as $s_k => $object):
                                                            ?>
                                                            <tr class="odd gradeX current_student" rel="<?= $object->id ?>">	
                                                                <td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' <?= ($student_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                                <td><?= $object->reg_id ?></td>	
                                                                <td class='hidden-480 sorting_disabled'><?= $object->roll_no ?></td>
                                                                <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                                <td><?= $object->father_name ?></td>			
                                                            </tr>
                                                            <?php
                                                            $sr++;
                                                        endforeach;
                                                        ?>
                                                    </tbody>
                                                <? else: ?>
                                                    <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>									
                                                <?php endif; ?>  
                                            </table>	
                                            <div class="form-actions NoPaddingCenter">
                                                <? if (!empty($record)): ?>
                                                    <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                                <?php endif; ?>  
                                                <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'list', 'list') ?>"> Cancel</a>
                                            </div>								
                                        </div>							
                                    </div>
                                </form>		
                            </div>	
                        <? endif; ?>	

                        <!-- participation Certificate-->		
                    <? elseif ($doc == 'participation'): ?>				
                        <? if (!empty($student_id)): ?>	
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">

                                <div class="tile bg-blue">							
                                    <a class="hidden-print" href='<?= make_admin_url('certificate', 'list', 'list&type=student&doc=participation&session_id=' . $session_id . '&ct_sec=' . $ct_sec) ?>'>
                                        <div class="corner"></div>
                                        <div class="tile-body"><i class="icon-arrow-left"></i></div>
                                        <div class="tile-object"><div class="name">Back</div></div>
                                    </a>
                                </div>	

                                <div class="tile bg-red">							
                                    <a class="hidden-print" href='<?= make_admin_url('certificate', 'update', 'update&type=student&doc=participation&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&student_id=' . $student_id) ?>'>
                                        <div class="corner"></div>
                                        <div class="tile-body"><i class="icon-edit"></i></div>
                                        <div class="tile-object"><div class="name">Edit</div></div>
                                    </a>
                                </div>									
                                <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>            
                            <div class="clearfix"></div>
                            <?php
                            /* display message */
                            display_message(1);
                            $error_obj->errorShow();
                            ?>						
                            <div class="row-fluid">						
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-user"></i>Student Participation Certificate </div>
                                        <div class="tools">
                                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                            <a href="javascript:;" class="reload"></a>
                                        </div>
                                    </div>	
                                    <div class="portlet-body">							
                                        <!-- Default Format -->
                                        <?= $content ?>
                                        <input type='hidden' id='student_id' value='<?= $student_id ?>'/>		
                                    </div>	
                                </div>
                                <? if (is_object($participation)): ?>					
                                    <input type='hidden' id='doc_id' value='<?= $participation->id ?>'/>
                                <? endif; ?>
                                <input type='hidden' id='doc' value='participation'/>

                            </div>

                        <? else: ?>
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">
                                <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>     
                            <?php
                            /* display message */
                            display_message(1);
                            $error_obj->errorShow();
                            ?>								
                            <div class="clearfix"></div>				
                            <div class="row-fluid">	
                                <form action='<?= make_admin_url('certificate', 'list', 'list') ?>' class="form-horizontal" method='get'>
                                    <input type='hidden' name='Page' value='certificate'/>
                                    <input type='hidden' name='action' value='list'/>
                                    <input type='hidden' name='section' value='list'/>
                                    <input type='hidden' name='type' value='student'/>
                                    <input type='hidden' name='doc' value='participation'/>						

                                    <!-- select Session And Section -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-bar-chart"></i>Select Session & Section </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">
                                            <div class="row-fluid">
                                                <div class='span4'>
                                                    <label class="control-label" style='width:auto;'>Select Session</label>
                                                    <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                        <?php
                                                        if (!empty($All_Session)) :
                                                            $session_sr = 1;
                                                            foreach ($All_Session as $key => $session):
                                                                ?>
                                                                <option value='<?= $session->id ?>' <?
                                                                if ($session->id == $session_id):
                                                                    echo 'selected';
                                                                endif;
                                                                ?> ><?= ucfirst($session->title) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                    </select>
                                                </div>
                                                <div class='span3' id='ajex_section'>
                                                    <label class="control-label" style='width:auto;'>Select Section</label>
                                                    <? if ($QuerySec->GetNumRows() > 0): ?>
                                                        <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                            <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                                                <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                                            <? endwhile; ?>	
                                                        </select>
                                                    <? else: ?>
                                                        <input type='text' class="span10" value='No Section Found.' disabled />
                                                    <? endif; ?>											
                                                </div>
                                                <div class='span2'>	
                                                    <label class="control-label">&nbsp;</label>
                                                    <span class='btn medium green' id='go'>Go</span>									
                                                </div>	
                                            </div>	
                                        </div>
                                    </div>		

                                    <!-- List OF Stu dent --> 
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Student List </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">								
                                            <table class="table table-striped table-hover" id='ajex_student'>
                                                <thead>
                                                    <tr>
                                                        <th class='hidden-480 sorting_disabled'></th>
                                                        <th>Reg ID.</th>
                                                        <th class='hidden-480 sorting_disabled'>Roll No.</th>
                                                        <th>Name</th>
                                                        <th>Father Name</th>																									
                                                    </tr>
                                                </thead>
                                                <? if (!empty($record)): ?>
                                                    <tbody>
                                                        <?php
                                                        $sr = 1;
                                                        foreach ($record as $s_k => $object):
                                                            ?>
                                                            <tr class="odd gradeX current_student" rel="<?= $object->id ?>">	
                                                                <td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' <?= ($student_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                                <td><?= $object->reg_id ?></td>	
                                                                <td class='hidden-480 sorting_disabled'><?= $object->roll_no ?></td>
                                                                <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                                <td><?= $object->father_name ?></td>			
                                                            </tr>
                                                            <?php
                                                            $sr++;
                                                        endforeach;
                                                        ?> 
                                                    </tbody>
                                                <? else: ?>
                                                    <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>									
                                                <?php endif; ?>  
                                            </table>	
                                            <div class="form-actions NoPaddingCenter">
                                                <? if (!empty($record)): ?>
                                                    <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                                <?php endif; ?>  
                                                <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'list', 'list') ?>"> Cancel</a>
                                            </div>								
                                        </div>							
                                    </div>
                                </form>		
                            </div>	
                        <? endif; ?>					

                        <!-- leaving Certificate-->		
                    <? elseif ($doc == 'leaving'): ?>				
                        <? if (!empty($student_id)): ?>	
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">

                                <div class="tile bg-blue">							
                                    <a class="hidden-print" href='<?= make_admin_url('certificate', 'list', 'list&type=student&doc=leaving&session_id=' . $session_id . '&ct_sec=' . $ct_sec) ?>'>
                                        <div class="corner"></div>
                                        <div class="tile-body"><i class="icon-arrow-left"></i></div>
                                        <div class="tile-object"><div class="name">Back</div></div>
                                    </a>
                                </div>	


                                <?php include_once (DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>            
                            <div class="clearfix"></div>
                            <?php
                            /* display message */
                            display_message(1);
                            $error_obj->errorShow();
                            ?>						
                            <div class="row-fluid">						
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-user"></i>School Leaving Certificate </div>
                                        <div class="tools">
                                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                            <a href="javascript:;" class="reload"></a>
                                        </div>
                                    </div>	
                                    <div class="portlet-body">							
                                        <!-- Default Format -->
                                        <?= $content ?>
                                        <input type='hidden' id='student_id' value='<?= $student_id ?>'/>		
                                    </div>	
                                </div>
                                <? if (is_object($leaving)): ?>					
                                    <input type='hidden' id='doc_id' value='<?= $leaving->id ?>'/>
                                <? endif; ?>
                                <input type='hidden' id='doc' value='leaving'/>

                            </div>

                        <? else: ?>
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">
                                <?php include_once (DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>     
                            <?php
                            /* display message */
                            display_message(1);
                            $error_obj->errorShow();
                            ?>								
                            <div class="clearfix"></div>				
                            <div class="row-fluid">	
                                <form action='<?= make_admin_url('certificate', 'list', 'list') ?>' class="form-horizontal" method='get'>
                                    <input type='hidden' name='Page' value='certificate'/>
                                    <input type='hidden' name='action' value='list'/>
                                    <input type='hidden' name='section' value='list'/>
                                    <input type='hidden' name='type' value='student'/>
                                    <input type='hidden' name='doc' value='leaving'/>						

                                    <!-- select Session And Section -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-bar-chart"></i>Select Session & Section </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">
                                            <div class="row-fluid">
                                                <div class='span4'>
                                                    <label class="control-label" style='width:auto;'>Select Session</label>
                                                    <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                        <?php
                                                        if (!empty($All_Session)) :
                                                            $session_sr = 1;
                                                            foreach ($All_Session as $key => $session):
                                                                ?>
                                                                <option value='<?= $session->id ?>' <?
                                                                if ($session->id == $session_id):
                                                                    echo 'selected';
                                                                endif;
                                                                ?> ><?= ucfirst($session->title) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                    </select>
                                                </div>
                                                <div class='span3' id='ajex_section'>
                                                    <label class="control-label" style='width:auto;'>Select Section</label>
                                                    <? if ($QuerySec->GetNumRows() > 0): ?>
                                                        <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                            <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                                                <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                                            <? endwhile; ?>	
                                                        </select>
                                                    <? else: ?>
                                                        <input type='text' class="span10" value='No Section Found.' disabled />
                                                    <? endif; ?>											
                                                </div>
                                                <div class='span2'>	
                                                    <label class="control-label">&nbsp;</label>
                                                    <span class='btn medium green' id='go'>Go</span>									
                                                </div>	
                                            </div>	
                                        </div>
                                    </div>		

                                    <!-- List OF Student -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Student List </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">								
                                            <table class="table table-striped table-hover" id='ajex_student'>
                                                <thead>
                                                    <tr>
                                                        <th class='hidden-480 sorting_disabled'></th>
                                                        <th>Reg ID.</th>
                                                        <th class='hidden-480 sorting_disabled'>Roll No.</th>
                                                        <th>Name</th>
                                                        <th>Father Name</th>																									
                                                    </tr>
                                                </thead>
                                                <? if (!empty($record)): ?>
                                                    <tbody>
                                                        <?php
                                                        $sr = 1;
                                                        foreach ($record as $s_k => $object):
                                                            ?>
                                                            <tr class="odd gradeX current_student" rel="<?= $object->id ?>">	
                                                                <td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' <?= ($student_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                                <td><?= $object->reg_id ?></td>	
                                                                <td class='hidden-480 sorting_disabled'><?= $object->roll_no ?></td>
                                                                <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                                <td><?= $object->father_name ?></td>			
                                                            </tr>
                                                            <?php
                                                            $sr++;
                                                        endforeach;
                                                        ?>
                                                    </tbody>
                                                <? else: ?>
                                                    <tr><td></td><td colspan= '4'>Sorr y No Record Found..!</td></tr>									
                                                <?php endif; ?>  
                                            </table>	
                                            <div class="form-actions NoPaddingCenter">
                                                <? if (!empty($record)): ?>
                                                    <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                                <?php endif; ?>  
                                                <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'list', 'list') ?>"> Cancel</a>
                                            </div>								
                                        </div>							
                                    </div>
                                </form>		
                            </div>	
                        <? endif; ?>					



                        <!-- Transfer Certificate-->		
                    <? elseif ($doc == 'transfer'): ?>				
                        <? if (!empty($student_id)): ?>	
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">

                                <div class="tile bg-blue">							
                                    <a class="hidden-print" href='<?= make_admin_url('certificate', 'list', 'list&type=student&doc=transfer&session_id=' . $session_id . '&ct_sec=' . $ct_sec) ?>'>
                                        <div class="corner"></div>
                                        <div class="tile-body"><i class="icon-arrow-left"></i></div>
                                        <div class="tile-object"><div class="name">Back</div></div>
                                    </a>
                                </div>	

                                <? if (isset($_POST['transfer_certificate'])): ?>							
                                    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                                <? endif; ?>
                            </div>            
                            <div class="clearfix"></div>
                            <?php
                            /* display message */
                            display_message(1);
                            $error_obj->errorShow();
                            ?>						
                            <div class="row-fluid">						
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-user"></i>School Transfer Certificate </div>
                                        <div class="tools">
                                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                            <a href="javascript:;" class="reload"></a>
                                        </div>
                                    </div>	
                                    <div class="portlet-body">	


                                        <? if (is_object($last_his)): ?>
                                            <?= $content ?>
                                            <input type='hidden' id='student_id' value='<?= $student_id ?>'/>	 
                                        <? elseif (isset($_POST['transfer_certificate'])): ?>    
                                            <!-- Default Format -->
                                            <?= $content ?>

                                            <?
                                            #Get document

                                            if (!is_object($last_his)):
                                                $QueryDoc = new document();
                                                $doc = $QueryDoc->getRecord($_POST['doc_id']);
                                                #Add entry to history
                                                $POST['student_id'] = $student_id;
                                                $POST['doc_type'] = $_POST['doc'];
                                                $POST['school_id'] = $school->id;
                                                $POST['session_id'] = $session_id;
                                                $POST['section'] = $ct_sec;
                                                $POST['document_id'] = $_POST['doc_id'];
                                                $POST['document_text'] = $content;
                                                $QuerySess = new SchoolDocumentPrintHistory();
                                                $QuerySess->saveData($POST);
                                            endif;
                                            ?>

                                            <input type='hidden' id='student_id' value='<?= $student_id ?>'/>		
                                        <? else: ?>




                                            <div class="row-fluid">
                                                <form class="form-horizontal" action="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=transfer&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&student_id=' . $student_id) ?>" method="POST" enctype="multipart/form-data" id="validation">
                                                    <!-- / Box -->
                                                    <div class="span12">
                                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->

                                                        <div class="portlet-body form">      
                                                            <h4 class="form-section hedding_inner">Information</h4>
                                                            <div class="row-fluid">
                                                                <div class="span6 ">


                                                                    <div class="control-group">
                                                                        <label class="control-label" for="SCHOOL_NAME">School Name<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="SCHOOL_NAME" value="<?php echo $school->school_name; ?>"  class="span10 m-wrap validate[required]"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="SCHOOL_ADDRESS">School Address<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <textarea name="SCHOOL_ADDRESS" class="span10 m-wrap" ><?php echo $school->address1 . ' ' . $school->city . ' ' . $school->district ?></textarea>
                                                                        </div>
                                                                    </div>


                                                                    <div class="control-group">
                                                                        <label class="control-label" for="AFF_NUM">Affiliation No.<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="AFF_NUM" value="<?php echo $school->affiliation_code; ?>"  class="span10 m-wrap validate[required]"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="SLC_NUMBER">S.L.C. No.<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="SLC_NUMBER" value=""  class="span10 m-wrap validate[required]"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="S_NUMBER">School No.<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="S_NUMBER" value="23108"  class="span10 m-wrap validate[required]"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="NUMBER">Admission No.<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="NUMBER" value="<?php echo $student->reg_id; ?>"  class="span10 m-wrap validate[required]"/>
                                                                        </div>
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <label class="control-label" for="NAME">Name of Pupil<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="NAME" value="<?php echo $student->first_name . " " . $student->last_name; ?>"  class="span10 m-wrap validate[required]"/>
                                                                        </div>
                                                                    </div> 

                                                                    <div class="control-group">
                                                                        <label class="control-label" for="FATHER_NAME">Father's/Guardian Name<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="FATHER_NAME" value="<?php echo $student->father_name ?>"  class="span10 m-wrap validate[required]"/>
                                                                        </div>
                                                                    </div> 	
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="MOTHER_NAME">Mother's Name<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="MOTHER_NAME" value="<?php echo $student->mother_name ?>"  class="span10 m-wrap validate[required]" />
                                                                        </div>
                                                                    </div> 	

                                                                    <div class="control-group">
                                                                        <label class="control-label" for="NATIONALITY">Nationality<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="NATIONALITY" value="" i class="span10 m-wrap validate[required]"/>
                                                                        </div>
                                                                    </div>												

                                                                    <div class="control-group">
                                                                        <label class="control-label" for="">Whether the candidate belongs to Schedule Cast or Schedule Tribe<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="CAST" value="<?php echo $student->cast; ?>" id="phone" class="span10 m-wrap validate[required]"/>
                                                                        </div>
                                                                    </div>	
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="ADDMISSION_DATE">Date of first admission in the school with class<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="ADDMISSION_DATE" value="<?php echo $student->date_of_admission ?>" id="on_date"  class="span10 m-wrap validate[required]"/>
                                                                        </div>   
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <label class="control-label" for="DOB">Date of birth<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="DOB" value="<?php echo $student->date_of_birth; ?>"  class="span10 m-wrap validate[required]"/>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="DOB_IN_WORD">Date of birth(In Word) <span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="DOB_IN_WORD" value="<?php echo date('d M Y', strtotime($student->date_of_birth)); ?>"  class="span10 m-wrap validate[required]"/>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="LAST_CLASS">Class in which the pupils last study <span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="LAST_CLASS" value="<?php echo $student->session_name; ?>"  class="span10 m-wrap validate[required]" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="SUBJECT">Subjects Studied</label>
                                                                        <div class="controls">
                                                                            <textarea name="SUBJECT" class="span10 m-wrap"><?php echo implode(', ', $cmpSub); ?></textarea>                                                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="BOARD_WITH_RESULT">School/Board Annual Examination last taken with result<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="BOARD_WITH_RESULT" value=""  class="span10 m-wrap validate[required]"/>
                                                                        </div>
                                                                    </div>
                                                                </div>	
                                                                <div class="span6 ">

                                                                    <div class="control-group">
                                                                        <label class="control-label" for="HEIGHER_CLASS">Whether qualified for promotion to the higher class<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="HEIGHER_CLASS" value=""  class="span10 m-wrap validate[required]" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="DUES_PAID">Month upto which the (pupil has paid) school dues paid<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="DUES_PAID" value="<?php echo $Balance ?>" id="father_name" class="span10 m-wrap validate[required]" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="CONCESSION">Any fee concession availed of if so, the nature of such concession<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="CONCESSION" value="<?php echo $student->concession_type ?>"  class="span10 m-wrap validate[required]" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="WORKING_DAYS">Total No. of working days<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="WORKING_DAYS" value="<?php echo $total_days; ?>"  class="span10 m-wrap validate[required]" />
                                                                        </div>
                                                                    </div> 

                                                                    <div class="control-group">
                                                                        <label class="control-label" for="PRESENT_WORKING_DAYS">Total No. of working days present<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="PRESENT_WORKING_DAYS" value="<?php echo $present; ?>"  class="span10 m-wrap validate[required]"/>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="OTHER_DETAIL">Whether NCC Cadet/Boy Scout/Vuide(details may be given)<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="OTHER_DETAIL" value=""  class="span10 m-wrap validate[required]"/>
                                                                        </div>    
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="ACHIVEMENT">Games played or extra curricular activities in which the pupil usually took part(mention achievement)<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="ACHIVEMENT" value=""  class="span10 m-wrap validate[required]"/>
                                                                        </div>   
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="GENERAL_CONDUCT">General conduct<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="GENERAL_CONDUCT" value=""  class="span10 m-wrap validate[required]"/>
                                                                        </div>   
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <label class="control-label" for="DATE_OF_APPLICATION">Date of application for certificate<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="DATE_OF_APPLICATION" value=""  class="span10 m-wrap validate[required]"/>
                                                                        </div>   
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <label class="control-label" for="DATE_OF_ISSUE_APPLICATION">Date of issue of certificate<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="DATE_OF_ISSUE_APPLICATION" value="<?php echo date('d M,Y'); ?>"  class="span10 m-wrap validate[required]"/>
                                                                        </div>   
                                                                    </div>

                                                                    <div class="control-group">
                                                                        <label class="control-label" for="REASONS">Reasons for leaving the school<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="REASONS" value=""  class="span10 m-wrap validate[required]"/>
                                                                        </div>  
                                                                    </div>
                                                                    <div class="control-group">
                                                                        <label class="control-label" for="REMARKS">Any other remarks<span class="required"></span></label>
                                                                        <div class="controls">
                                                                            <input type="text" name="REMARKS" value=""  class="span10 m-wrap validate[required]"/>
                                                                        </div>  
                                                                    </div>



                                                                </div>	
                                                            </div>	



                                                            <div class="form-actions">

                                                                <input class="btn blue" type="submit" name="transfer_certificate" value="Submit" tabindex="7" /> 
                                                                <a href="<?php echo make_admin_url('certificate', 'list', 'list&type=student&doc=transfer&'); ?>" class="btn" name="cancel" > Cancel</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <? if (is_object($transfer)): ?>					
                                                        <input type='hidden' name='doc_id' value='<?= $transfer->id ?>'/>
                                                    <? endif; ?>
                                                    <input type='hidden' name='doc' value='transfer'/>

                                                </form>
                                                <div class="clearfix"></div>

                                            </div>

                                            <div class="clearfix"></div>








                                        <? endif; ?>
                                    </div>	
                                </div>
                                <? if (is_object($transfer)): ?>					
                                    <input type='hidden' id='doc_id' value='<?= $transfer->id ?>'/>
                                <? endif; ?>
                                <input type='hidden' id='doc' value='transfer'/>

                            </div>

                        <? else: ?>
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">
                                <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>     
                            <?php
                            /* display message */
                            display_message(1);
                            $error_obj->errorShow();
                            ?>								
                            <div class="clearfix"></div>				
                            <div class="row-fluid">	
                                <form action='<?= make_admin_url('certificate', 'list', 'list') ?>' class="form-horizontal" method='get'>
                                    <input type='hidden' name='Page' value='certificate'/>
                                    <input type='hidden' name='action' value='list'/>
                                    <input type='hidden' name='section' value='list'/>
                                    <input type='hidden' name='type' value='student'/>
                                    <input type='hidden' name='doc' value='transfer'/>						

                                    <!-- select Session And Section -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-bar-chart"></i>Select Session & Section </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">
                                            <div class="row-fluid">
                                                <div class='span4'>
                                                    <label class="control-label" style='width:auto;'>Select Session</label>
                                                    <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                        <?php
                                                        if (!empty($All_Session)) :
                                                            $session_sr = 1;
                                                            foreach ($All_Session as $key => $session):
                                                                ?>
                                                                <option value='<?= $session->id ?>' <?
                                                                if ($session->id == $session_id):
                                                                    echo 'selected';
                                                                endif;
                                                                ?> ><?= ucfirst($session->title) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                    </select>
                                                </div>
                                                <div class='span3' id='ajex_section'>
                                                    <label class="control-label" style='width:auto;'>Select Section</label>
                                                    <? if ($QuerySec->GetNumRows() > 0): ?>
                                                        <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                            <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                                                <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                                            <? endwhile; ?>	
                                                        </select>
                                                    <? else: ?>
                                                        <input type='text' class="span10" value='No Section Found.' disabled />
                                                    <? endif; ?>											
                                                </div>
                                                <div class='span2'>	
                                                    <label class="control-label">&nbsp;</label>
                                                    <span class='btn medium green' id='go'>Go</span>									
                                                </div>	
                                            </div>	
                                        </div>
                                    </div>		

                                    <!-- List OF Student -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Student List </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">								
                                            <table class="table table-striped table-hover" id='ajex_student'>
                                                <thead>
                                                    <tr>
                                                        <th class='hidden-480 sorting_disabled'></th>
                                                        <th>Reg ID.</th>
                                                        <th class='hidden-480 sorting_disabled'>Roll No.</th>
                                                        <th>Name</th>
                                                        <th>Father Name</th>																									
                                                    </tr>
                                                </thead>
                                                <? if (!empty($record)): ?>
                                                    <tbody>
                                                        <?php
                                                        $sr = 1;
                                                        foreach ($record as $s_k => $object):
                                                            ?>
                                                            <tr class="odd gradeX current_student" rel="<?= $object->id ?>">	
                                                                <td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' <?= ($student_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                                <td><?= $object->reg_id ?></td>	
                                                                <td class='hidden-480 sorting_disabled'><?= $object->roll_no ?></td>
                                                                <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                                <td><?= $object->father_name ?></td>			
                                                            </tr>
                                                            <?php
                                                            $sr++;
                                                        endforeach;
                                                        ?>
                                                    </tbody>
                                                <? else: ?>
                                                    <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>									
                                                <?php endif; ?>  
                                            </table>	
                                            <div class="form-actions NoPaddingCenter">
                                                <? if (!empty($record)): ?>
                                                    <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                                <?php endif; ?>  
                                                <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'list', 'list') ?>"> Cancel</a>
                                            </div>								
                                        </div>							
                                    </div>
                                </form>		
                            </div>	
                        <? endif; ?>
                        <!-- Sports Certificate-->		
                    <? elseif ($doc == 'sports'): ?>				
                        <? if (!empty($student_id)): ?>	
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">

                                <div class="tile bg-blue">							
                                    <a class="hidden-print" href='<?= make_admin_url('certificate', 'list', 'list&type=student&doc=sports&session_id=' . $session_id . '&ct_sec=' . $ct_sec) ?>'>
                                        <div class="corner"></div>
                                        <div class="tile-body"><i class="icon-arrow-left"></i></div>
                                        <div class="tile-object"><div class="name">Back</div></div>
                                    </a>
                                </div>	

                                <div class="tile bg-red">							
                                    <a class="hidden-print" href='<?= make_admin_url('certificate', 'update', 'update&type=student&doc=sports&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&student_id=' . $student_id) ?>'>
                                        <div class="corner"></div>
                                        <div class="tile-body"><i class="icon-edit"></i></div>
                                        <div class="tile-object"><div class="name">Edit</div></div>
                                    </a>
                                </div>									
                                <?php include_once (DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>   
                            <?php
                            /* display message */
                            display_message(1);
                            $error_obj->errorShow();
                            ?>							
                            <div class="clearfix"></div>				
                            <div class="row-fluid">						
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-user"></i>Student Sports Certificate </div>
                                        <div class="tools">
                                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                            <a href="javascript:;" class="reload"></a>
                                        </div>
                                    </div>	
                                    <div class="portlet-body">							
                                        <!-- Default Format -->
                                        <?= $content ?>
                                        <input type='hidden' id='student_id' value='<?= $student_id ?>'/>		
                                    </div>	
                                </div>
                                <? if (is_object($sports)): ?>					
                                    <input type='hidden' id='doc_id' value='<?= $sports->id ?>'/>
                                <? endif; ?>
                                <input type='hidden' id='doc' value='sports'/>

                            </div>

                        <? else: ?>
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">
                                <?php include_once (DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>            
                            <div class="clearfix"></div>
                            <?php
                            /* display message */
                            display_message(1);
                            $error_obj->errorShow();
                            ?>						
                            <div class="row-fluid">	
                                <form action='<?= make_admin_url('certificate', 'list', 'list') ?>' class="form-horizontal" method='get'>
                                    <input type='hidden' name='Page' value='certificate'/>
                                    <input type='hidden' name='action' value='list'/>
                                    <input type='hidden' name='section' value='list'/>
                                    <input type='hidden' name='type' value='student'/>
                                    <input type='hidden' name='doc' value='sports'/>						

                                    <!-- select Session And Section -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-bar-chart"></i>Select Session & Section </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">
                                            <div class="row-fluid">
                                                <div class='span4'>
                                                    <label class="control-label" style='width:auto;'>Select Session</label>
                                                    <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                        <?php
                                                        if (!empty($All_Session)) :
                                                            $session_sr = 1;
                                                            foreach ($All_Session as $key => $session):
                                                                ?>
                                                                <option value='<?= $session->id ?>' <?
                                                                if ($session->id == $session_id):
                                                                    echo 'selected';
                                                                endif;
                                                                ?> ><?= ucfirst($session->title) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                    </select>
                                                </div>
                                                <div class='span3' id='ajex_section'>
                                                    <label class="control-label" style='width:auto;'>Select Section</label>
                                                    <? if ($QuerySec->GetNumRows() > 0): ?>
                                                        <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                            <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                                                <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                                            <? endwhile; ?>	
                                                        </select>
                                                    <? else: ?>
                                                        <input type='text' class="span10" value='No Section Found.' disabled />
                                                    <? endif; ?>											
                                                </div>
                                                <div class='span2'>	
                                                    <label class="control-label">&nbsp;</label>
                                                    <span class='btn medium green' id='go'>Go</span>									
                                                </div>	
                                            </div>	
                                        </div>
                                    </div>		

                                    <!-- List OF Student -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Student List </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a  href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">								
                                            <table class="table table-striped table-hover" id='ajex_student'>
                                                <thead>
                                                    <tr>
                                                        <th class='hidden-480 sorting_disabled'></th>
                                                        <th>Reg ID.</th>
                                                        <th class='hidden-480 sorting_disabled'>Roll No.</th>
                                                        <th>Name</th>
                                                        <th>Father Name</th>																									
                                                    </tr>
                                                </thead>
                                                <? if (!empty($record)): ?>
                                                    <tbody>
                                                        <?php
                                                        $sr = 1;
                                                        foreach ($record as $s_k => $object):
                                                            ?>
                                                            <tr class="odd gradeX current_student" rel="<?= $object->id ?>">	
                                                                <td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' <?= ($student_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                                <td><?= $object->reg_id ?></td>	
                                                                <td class='hidden-480 sorting_disabled'><?= $object->roll_no ?></td>
                                                                <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                                <td><?= $object->father_name ?></td>			
                                                            </tr>
                                                            <?php
                                                            $sr++;
                                                        endforeach;
                                                        ?>
                                                    </tbody>
                                                <? else: ?>
                                                    <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>									
                                                <?php endif; ?>  
                                            </table>	
                                            <div class="form-actions NoPaddingCenter">
                                                <? if (!empty($record)): ?>
                                                    <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                                <?php endif; ?>  
                                                <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'list', 'list') ?>"> Cancel</a>
                                            </div>								
                                        </div>							
                                    </div>
                                </form>		
                            </div>	
                        <? endif; ?>					


                    <? elseif ($doc == 'report'): ?>
                        <? if (!empty($student_id)): ?>	
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">
                                <div class="tile bg-blue">							
                                    <a class="hidden-print" href='<?= make_admin_url('certificate', 'list', 'list&type=student&doc=report') ?>'>
                                        <div class="corner"></div>
                                        <div class="tile-body"><i class="icon-arrow-left"></i></div>
                                        <div class="tile-object"><div class="name">Back</div></div>
                                    </a>
                                </div>							
                                <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>            
                            <div class="clearfix"></div>				
                            <div class="row-fluid">						
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-user"></i>Student Report Card </div>
                                        <div class="tools">
                                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                            <a href="javascript:;" class="reload"></a>
                                        </div>
                                    </div>	
                                    <div class="portlet-body" >							
                                        <!-- Default Format -->
                                        <?= $content ?>
                                        <input type='hidden' id='student_id' value='<?= $student_id ?>'/>		
                                    </div>	
                                </div>
                                <? if (is_object($dmc)): ?>					
                                    <input type='hidden' id='doc_id' value='<?= $dmc->id ?>'/>
                                <? endif; ?>
                                <input type='hidden' id='doc' value='report'/>

                            </div>	
                        <? else: ?>
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">
                                <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                            </div>            
                            <div class="clearfix"></div>				
                            <div class="row-fluid">	
                                <form action='<?= make_admin_url('certificate', 'list', 'list') ?>' class="form-horizontal" method='get'>
                                    <input type='hidden' name='Page' value='certificate'/>
                                    <input type='hidden' name='action' value='list'/>
                                    <input type='hidden' name='section' value='list'/>
                                    <input type='hidden' name='type' value='student'/>
                                    <input type='hidden' name='doc' value='report'/>						
                                    <!-- select Session And Section -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-bar-chart"></i>Select Session & Section </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body">
                                            <div class="row-fluid">
                                                <div class='span4'>
                                                    <label class="control-label" style='width:auto;'>Select Session</label>
                                                    <select class="select2_category span10 exam_session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                        <?php
                                                        if (!empty($All_Session)) :
                                                            $session_sr = 1;
                                                            foreach ($All_Session as $key => $session):
                                                                ?>
                                                                <option value='<?= $session->id ?>' <?
                                                                if ($session->id == $session_id):
                                                                    echo 'selected';
                                                                endif;
                                                                ?>><?= ucfirst($session->title) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row-fluid" id='ajex_section_exam'>									
                                                <div class='span4' >
                                                    <label class="control-label" style='width:auto;'>Select Section</label>
                                                    <? if ($QuerySec->GetNumRows() > 0): ?>
                                                        <select class="select2_category span10 exam_session_filter" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                            <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                                                <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                                            <? endwhile; ?>	
                                                        </select>
                                                    <? else: ?>
                                                        <input type='text' class="span10" value='No Section Found.' disabled />
                                                    <? endif; ?>											
                                                </div>
                                                <div class='span4' >
                                                    <label class="control-label" style='width:auto;'>Select Exam</label>

                                                    <? if (!empty($exams)): ?>
                                                        <select class="span11" name='exam_id' id='exam_id'>
                                                            <?php foreach ($exams as $ex_key => $ex_val): ?>
                                                                <option value='<?= $ex_val->id ?>' <?
                                                                if ($exam_id == $ex_val->id): echo 'selected';
                                                                endif;
                                                                ?>><?= ucfirst($ex_val->title) ?></option>
                                                                    <?php endforeach; ?>
                                                        </select>
                                                    <? else: ?>	
                                                        <input type='text' class='span11' value='Sorry, No Exam Found.' disabled />	
                                                    <? endif; ?>

                                                </div>		
                                                <div class='span2'>	
                                                    <label class="control-label">&nbsp;</label>
                                                    <span class='btn medium green' id='go_exam'>Go</span>									
                                                </div>	
                                            </div>	
                                        </div>
                                    </div>		

                                    <!-- List OF Student -->
                                    <div class="portlet box blue">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Student List </div>
                                            <div class="tools">
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                            </div>
                                        </div>	
                                        <div class="portlet-body" id='ajex_student'>	
                                            <table class="table table-striped table-hover" >
                                                <thead>
                                                    <tr>
                                                        <th class='hidden-480 sorting_disabled'></th>
                                                        <th class='hidden-480 sorting_disabled'>R oll No.< /th>
                                                        <th>Name</th>
                                                        <th>Father Name</th>												
                                                        <th class='hidden-480 sorting_disabled'>Exam Titles</th>												
                                                    </tr>
                                                </thead>
                                                <? if (!empty($record)): ?>
                                                    <tbody>
                                                        <?php
                                                        $sr = 1;
                                                        foreach ($record as $s_k => $object):
                                                            $ShowObj = new examination();
                                                            $show = $ShowObj->getExamNames($group->exam_id, $object->id);
                                                            ?>
                                                            <tr class="odd gradeX current_student_exam" rel="<?= $object->id ?>">
                                                                <td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' value='<?= $object->id ?>'/> </td>												
                                                                <td class='hidden-480 sorting_disabled'><?= $object->roll_no ?></td>	
                                                                <td><?= $object->first_name . " " . $object->last_name ?></td>	
                                                                <td><?= $object->father_name ?></td>																

                                                                <td style='text-align:left' class="hidden-480"><?= $show ?></td>								
                                                            </tr>
                                                            <?php
                                                            $sr++;
                                                        endforeach;
                                                        ?>
                                                    </tbody>
                                                <? else: ?>
                                                    <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>													
                                                <?php endif; ?>  
                                            </table>
                                            <div class="form-actions NoPaddingCenter">
                                                <? if (!empty($record)): ?>
                                                    <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
                                                <?php endif; ?>  
                                                <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'list', 'list') ?>"> Cancel</a>
                                            </div>									
                                        </div>
                                    </div>		
                                </form>						
                            </div>						
                        <? endif; ?>
                    <? elseif ($doc == 'test_report_card'): ?>
                        <? if (!empty($student_id)): ?>
                            <div class="clearfix"></div>
                            <div class="tiles pull-right">
                                <a class="hidden-print" href='<? echo make_admin_url('certificate', 'list', 'list&type=student&doc=test_report_card&session_id=' . $session_id . '&ct_sec=' . $ct_sec)  ?>'>
                                    <div class="corner"></div>
                                    <div class="tile-body"><i class="icon-arrow-left"></i></div>
                                    <div class="tile-object"><div class="name">Back</div></div>
                                </a>
                            </div>	
                        </div>            
                        <?
                        //if (isset($_POST['test_report_card'])):
                            include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php');
                        //endif;
                        ?>
                        <div class="clearfix"></div>
                        <?php
                        /* display message */
                        display_message(1);
                        $error_obj->errorShow();
                        ?>						
                        <div class="row-fluid">						
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption"><i class="icon-user"></i>Test Report Card</div>
                                    <div class="tools">
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                    </div>
                                </div>	
                                <div class="portlet-body">
                                        <?php echo $content; ?>
                                       
                                        <input type='hidden' id='student_id' value='<?= $student_id ?>'/>		
            <? else: ?>
                                        <div class="row-fluid">
                                            <form class="form-horizontal" action="<?= make_admin_url('certificate', 'list', 'list&type=student&doc=test_report_card&session_id=' . $session_id . '&ct_sec=' . $ct_sec . '&student_id=' . $student_id) ?>" method="POST" enctype="multipart/form-data" id="validation">
                                                <div class="span12">
                                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                    <div class="portlet-body form">
                                                        <h4 class="form-section hedding_inner">Information</h4>
                                                        <div class="row-fluid">
                                                            <div class="span6">
                                                                <div class="control-group">
                                                                    <label class="control-label" for="SCHOOL_NAME">School Name</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="SCHOOL_NAME" value="<?php echo $school->school_name; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="SCHOOL_ADDRESS">School Address</label>
                                                                    <div class="controls">
                                                                        <textarea name="SCHOOL_ADDRESS" class="span10 m-wrap validate[required]"><?php echo $school->address1 . ' ' . $school->city . ' ' . $school->district; ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="SCHOOL_PHONE">School Phone No.</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="SCHOOL_PHONE" value="<?php echo $school->school_phone; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="SCHOOL_EMAIL">School Email</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="SCHOOL_EMAIL" value="<?php echo $school->email_address; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="SCHOOL_WEB">School Website</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="SCHOOL_WEB" value="<?php echo $school->website_url; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="SCHOOL_AFFL">Affiliation No.</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="SCHOOL_AFFL" value="<?php echo $school->affiliation_code; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="SCHOOL_WORKING_DAYS">School Total Working Days</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="SCHOOL_WORKING_DAYS" value="<?php echo $total_working_days; ?> days" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="span6">
                                                                <div class="control-group">
                                                                    <label class="control-label" for="STUDENT_NAME">Student Name</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="STUDENT_NAME" value="<?php echo $student->first_name . " " . $student->last_name; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="STUDENT_CLASS">Student Class</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="STUDENT_CLASS" value="<?php echo $student->session_name; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="STUDENT_HOUSE">Student House</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="STUDENT_HOUSE" value="<?php echo $student_address->city; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="STUDENT_ADMISSION_NUMBER">Student Admission No.</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="STUDENT_ADMISSION_NUMBER" value="<?php echo $student->reg_id; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="STUDENT_DOB">Student Admission No.</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="STUDENT_DOB" value="<?php echo $student->date_of_birth; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="STUDENT_RATN">Residential Address & Telephone No.</label>
                                                                    <div class="controls">
                                                                        <textarea rows="3" name="STUDENT_RATN" class="span10 m-wrap validate[required]"><?php echo $student_address->address1 . ", " . $student_address->address2 . ", " . $student_address->tehsil . ", " . $student_address->district . ", " . $student_address->city . " (" . $student_address->state . ") - " . $student->phone; ?></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="STUDENT_MOTHER">Student Mother's Name</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="STUDENT_MOTHER" value="<?php echo $student->mother_name; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="STUDENT_FATHER">Student Father's Name</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="STUDENT_FATHER" value="<?php echo $student->father_name; ?>" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                                                                    <label class="control-label" for="STUDENT_ATTENDANCE">Student Total Attendance</label>
                                                                    <div class="controls">
                                                                        <input type="text" name="STUDENT_ATTENDANCE" value="<?php echo $student_attendance; ?> days" class="span10 m-wrap validate[required]"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <input class="btn blue" type="submit" name="test_report_card" value="Submit" tabindex="7" /> 
                                                            <a href="<?php echo make_admin_url('certificate', 'list', 'list&type=student&doc=test_report_card&'); ?>" class="btn" name="cancel" > Cancel</a>
                                                        </div>
                                                    </div>

                                                </div>
                                                <? if (is_object($test_report_card)): ?>
                                                    <input type='hidden' name='doc_id' value='<?= $test_report_card->id ?>'/>
                <? endif; ?>
                                                <input type='hidden' name='doc' value='<?= $test_report_card->type ?>'/>

                                            </form>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
            <? endif; ?>
                                </div>	
                            </div>
                            <? if (is_object($test_report_card)): ?>					
                                <input type='hidden' id='doc_id' value='<?= $test_report_card->id ?>'/>
            <? endif; ?>
                            <input type='hidden' id='doc' value='<?= $test_report_card->type ?>'/>

                        </div>

        <? else: ?>
                        <div class="clearfix"></div>
                        <div class="tiles pull-right">
                        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/student_shortcut.php'); ?>  
                        </div>     
                        <?php
                        /* display message */
                        display_message(1);
                        $error_obj->errorShow();
                        ?>								
                        <div class="clearfix"></div>				
                        <div class="row-fluid">	
                            <form action='<?= make_admin_url('certificate', 'list', 'list') ?>' class="form-horizontal" method='get'>
                                <input type='hidden' name='Page' value='certificate'/>
                                <input type='hidden' name='action' value='list'/>
                                <input type='hidden' name='section' value='list'/>
                                <input type='hidden' name='type' value='student'/>
                                <input type='hidden' name='doc' value='test_report_card'/>						

                                <!-- select Session And Section -->
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-bar-chart"></i>Select Session & Section </div>
                                        <div class="tools">
                                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                            <a href="javascript:;" class="reload"></a>
                                        </div>
                                    </div>	
                                    <div class="portlet-body">
                                        <div class="row-fluid">
                                            <div class='span4'>
                                                <label class="control-label" style='width:auto;'>Select Session</label>
                                                <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                    <?php
                                                    if (!empty($All_Session)) :
                                                        $session_sr = 1;
                                                        foreach ($All_Session as $key => $session):
                                                            ?>
                                                            <option value='<?= $session->id ?>' <?
                                                            if ($session->id == $session_id):
                                                                echo 'selected';
                                                            endif;
                                                            ?> ><?= ucfirst($session->title) ?></option>
                                                                    <?
                                                                    $session_sr++;
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                </select>
                                            </div>
                                            <div class='span3' id='ajex_section'>
                                                <label class="control-label" style='width:auto;'>Select Section</label>
                                                    <? if ($QuerySec->GetNumRows() > 0): ?>
                                                    <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                                        <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                                            <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                                    <? endwhile; ?>	
                                                    </select>
                                                <? else: ?>
                                                    <input type='text' class="span10" value='No Section Found.' disabled />
            <? endif; ?>											
                                            </div>
                                            <div class='span2'>	
                                                <label class="control-label">&nbsp;</label>
                                                <span class='btn medium green' id='go'>Go</span>									
                                            </div>	
                                        </div>	
                                    </div>
                                </div>		

                                <!-- List OF Student -->
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-user"></i>Student List </div>
                                        <div class="tools">
                                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                            <a href="javascript:;" class="reload"></a>
                                        </div>
                                    </div>	
                                    <div class="portlet-body">	
                                        <table class="table table-striped table-hover" id='ajex_student'>
                                            <thead>
                                                <tr>
                                                    <th class='hidden-480 sorting_disabled'></th>
                                                    <th>Reg ID.</th>
                                                    <th class='hidden-480 sorting_disabled'>Roll No.</th>
                                                    <th>Name</th>
                                                </tr>
                                            </thead>
                                                <? if (!empty($record)): ?>
                                                <tbody>
                                                    <?php
                                                    $sr = 1;
                                                    foreach ($record as $s_k => $object):
                                                        ?>
                                                        <tr class="odd gradeX current_student" rel="<?= $object->id ?>">	
                                                            <td class='hidden-480 sorting_disabled'><input type='radio' name='student_id' <?= ($student_id == $object->id) ? 'checked' : ''; ?> value='<?= $object->id ?>' checked /> </td>												
                                                            <td><?= $object->reg_id ?></td>	
                                                            <td class='hidden-480 sorting_disabled'><?= $object->roll_no ?></td>
                                                            <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                            <td><?= $object->father_name ?></td>			
                                                        </tr>
                                                        <?php
                                                        $sr++;
                                                    endforeach;
                                                    ?>
                                                </tbody>
                                            <?php else: ?>
                                                <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>									
            <? endif; ?> 
                                        </table>	
                                        <div class="form-actions NoPaddingCenter">
                                            <? if (!empty($record)): ?>
                                                <input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
            <?php endif; ?>  
                                            <a name="cancel" class="btn" href="<?= make_admin_url('certificate', 'list', 'list') ?>"> Cancel</a>
                                        </div>								
                                    </div>							
                                </div>
                            </form>		
                        </div>	
                    <? endif; ?>
                <? endif; ?>
        </div>
    </div>	


    <script type="text/javascript">
        jQuery(document).ready(function () {
            /* show doc_preview  */
            $(".current_student").live("click", function () {
                var session_id = $("#session_id").val();
                var ct_sec = $("#ct_sec").val();
                var student_id = $(this).attr('rel');
                $(".current_student").find('span').removeClass('checked');
                $(".current_student").find('td input:radio').prop('checked', false);

                $(this).find('span').addClass('checked');
                $(this).find('td input:radio').prop('checked', true);

                var doc_id = $('#doc_id').val();
                var doc = $('#doc').val();
                var dataString = 'student_id=' + student_id + '&doc_id=' + doc_id + '&doc=' + doc + '&session_id=' + session_id + '&ct_sec=' + ct_sec;
                $("#doc_preview").html('<div style="width:100%;text-align:center;padding-top:50px;"><img src="assets/img/ajax-loading.gif"/></div>');

                $.ajax({
                    type: "POST",
                    url: "<?= make_admin_url_window('ajax_calling', 'ajax_student_doc', 'ajax_student_doc&temp=certificate'); ?>",
                    data: dataString,
                    success: function (data, textStatus)
                    {
                        $('#doc_preview').html(data);
                    }
                });
            });
            /* show doc_preview  */
            $(".current_student_exam").live("click", function () {
                var session_id = $("#session_id").val();
                var ct_sec = $("#ct_sec").val();
                var student_id = $(this).attr('rel');
                $(".current_student_exam").find('span').removeClass('checked');
                $(".current_student_exam").find('td input:radio').prop('checked', false);
                $(this).find('span').addClass('checked');

                $(this).find('td input:radio').prop('checked', true);

                var doc_id = $('#doc_id').val();
                var exam_id = $('#exam_id').val();
                var doc = $('#doc').val();
                var dataString = 'student_id=' + student_id + '&doc_id=' + doc_id + '&doc=' + doc + '&session_id=' + session_id + '&ct_sec=' + ct_sec + '&exam_id=' + exam_id;


                $("#doc_preview").html('<div style="width:100%;text-align:center;padding-top:50px;"><img src="assets/img/ajax-loading.gif"/></div>');

                $.ajax({
                    type: "POST",
                    url: "<?= make_admin_url_window('ajax_calling', 'ajax_student_doc', 'ajax_student_doc&temp=certificate'); ?>",
                    data: dataString,
                    success: function (data, textStatus)
                    {
                        $('#doc_preview').html(data);
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        /* Print Document */
        $("#print").live("click", function () {
            var session_id = '<?= $session_id ?>';
            var ct_sec = '<?= $ct_sec ?>';
            var student_id = '<?= $student_id ?>';
            var doc_id = $('#doc_id').val();
            var doc = $('#doc').val();
            var exam_id = '<?= $exam_id ?>';
            if (student_id == '') {
                alert("Sorry, You can't print this document..!");
                return false;
            }

            var dataString = 'student_id=' + student_id + '&doc_id=' + doc_id + '&doc=' + doc + '&session_id=' + session_id + '&ct_sec=' + ct_sec + '&exam_id=' + exam_id;

            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_student_print', 'ajax_student_print&temp=certificate'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    var DocumentContainer = document.getElementById(data);
                    var WindowObject = window.open('', "PrintWindow", "");
                    WindowObject.document.writeln(data);
                    WindowObject.document.close();
                    WindowObject.focus();
                    WindowObject.print();
                    WindowObject.close();
                    return false;
                }
            });
        });
        /* Session Students */
        $(".session_filter").change(function () {
            var session_id = $("#session_id").val();

            if (session_id.length > 0) {
                var id = session_id;
                $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

                var dataString = 'id=' + id;

                $.ajax({
                    type: "POST",
                    url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section&temp=certificate'); ?>",
                    data: dataString,
                    success: function (data, textStatus) {
                        $("#ajex_section").html(data);
                    }
                });
            }
            else {
                return false;
            }
        });

        /* Filter Students */
        $("#go").live("click", function () {
            var session_id = $('#session_id').val();
            var ct_sec = $('#ct_sec').val();
            var dataString = 'session_id=' + session_id + '&ct_sec=' + ct_sec;
            $("#ajex_student").html('<div style="width:100%;text-align:center;padding-top:50px;"><img src="assets/img/ajax-loading.gif"/></div>');

            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_student', 'ajax_section_student&temp=certificate'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_student").html(data);
                    App.init();
                    FormComponents.init();
                    TableManaged.init();
                }
            });
        });

        /* Filter Students */
        $(".exam_session_filter").live("change", function () {
            var session_id = $("#session_id").val();
            var ct_sec = $("#ct_sec").val();

            if (session_id.length > 0) {
                var id = session_id;
                $("#ajex_section_exam").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

                var dataString = 'session_id=' + session_id + '&ct_sec=' + ct_sec;

                $.ajax({
                    type: "POST",
                    url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_exam', 'ajax_section_exam&temp=certificate'); ?>",
                    data: dataString,
                    success: function (data, textStatus) {
                        $("#ajex_section_exam").html(data);
                    }
                });
            }
            else {
                return false;
            }
        });
        /* Filter Students */
        $("#go_exam").live("click", function () {
            var session_id = $('#session_id').val();
            var ct_sec = $('#ct_sec').val();
            var exam_id = $('#exam_id').val();
            var dataString = 'session_id=' + session_id + '&ct_sec=' + ct_sec + '&exam_id=' + exam_id;
            $("#ajex_student").html('<div style="width:100%;text-align:center;padding-top:50px;"><img src="assets/img/ajax-loading.gif"/></div>');

            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_exam_student', 'ajax_exam_student&temp=certificate'); ?>                                                                         ",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_student").html(data);
                    App.init();
                    FormComponents.init();
                    TableManaged.init();
                }
            });
        });
    </script>