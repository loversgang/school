<?php

error_reporting(0);
/* Include Classes */
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentAttendanceClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsEmailClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentExaminationClass.php');

isset($_REQUEST['doc']) ? $doc = $_REQUEST['doc'] : $doc = '';
isset($_REQUEST['doc_id']) ? $doc_id = $_REQUEST['doc_id'] : $doc_id = '';
isset($_REQUEST['student_id']) ? $student_id = $_REQUEST['student_id'] : $student_id = '';
isset($_REQUEST['session_id']) ? $session_id = $_REQUEST['session_id'] : $session_id = '';
isset($_REQUEST['ct_sec']) ? $ct_sec = $_REQUEST['ct_sec'] : $ct_sec = '';
isset($_POST['exam_id']) ? $exam_id = $_POST['exam_id'] : $exam_id = '';
isset($_REQUEST['group_id']) ? $group_id = $_REQUEST['group_id'] : $group_id = '';

if ($doc == 'icard'):
    $icard = get_object('document_master', $doc_id);
    if ($doc == 'icard' && is_object($icard) && !empty($student_id)):
        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $icard->id, $student_id);

        /* Load the last updated doc if is it */
        if (is_object($last_his)):
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($icard)):
                $content = $icard->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
        else:
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($icard)):
                $content = $icard->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
#Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);
#Add entry to history
            $_POST['student_id'] = $student_id;
            $_POST['doc_type'] = $doc->type;
            $_POST['school_id'] = $school->id;
            $_POST['session_id'] = $session_id;
            $_POST['section'] = $ct_sec;
            $_POST['document_id'] = $doc_id;
            $_POST['document_text'] = $content;
            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);
        endif;
    endif;
elseif ($doc == 'character'):
    $character = get_object('document_master', $doc_id);
    if ($doc == 'character' && is_object($character) && !empty($student_id)):
        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $character->id, $student_id);

        /* Load the last updated doc if is it */
        if (is_object($last_his)):
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($character)):
                $content = $character->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
        else:
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($character)):
                $content = $character->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
#Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);
#Add entry to history
            $_POST['student_id'] = $student_id;
            $_POST['doc_type'] = $doc->type;
            $_POST['school_id'] = $school->id;
            $_POST['session_id'] = $session_id;
            $_POST['section'] = $ct_sec;
            $_POST['document_id'] = $doc_id;
            $_POST['document_text'] = $content;
            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);
        endif;
    endif;


elseif ($doc == 'appreciation'):
    $appri = get_object('document_master', $doc_id);
    if ($doc == 'appreciation' && is_object($appri) && !empty($student_id)):
        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $appri->id, $student_id);

        /* Load the last updated doc if is it */
        if (is_object($last_his)):
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'DATE' => date('d M,Y'),
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($appri)):
                $content = $appri->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
        else:
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'DATE' => date('d M,Y'),
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($appri)):
                $content = $appri->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
#Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);
#Add entry to history
            $_POST['student_id'] = $student_id;
            $_POST['doc_type'] = $doc->type;
            $_POST['school_id'] = $school->id;
            $_POST['session_id'] = $session_id;
            $_POST['section'] = $ct_sec;
            $_POST['document_id'] = $doc_id;
            $_POST['document_text'] = $content;
            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);
        endif;
    endif;

elseif ($doc == 'sports'):
    $sports = get_object('document_master', $doc_id);
    if ($doc == 'sports' && is_object($sports) && !empty($student_id)):
        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $sports->id, $student_id);

        /* Load the last updated doc if is it */
        if (is_object($last_his)):
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'DATE' => date('d M,Y'),
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($sports)):
                $content = $sports->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
        else:
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'DATE' => date('d M,Y'),
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($sports)):
                $content = $sports->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
#Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);
#Add entry to history
            $_POST['student_id'] = $student_id;
            $_POST['doc_type'] = $doc->type;
            $_POST['school_id'] = $school->id;
            $_POST['session_id'] = $session_id;
            $_POST['section'] = $ct_sec;
            $_POST['document_id'] = $doc_id;
            $_POST['document_text'] = $content;
            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);
        endif;
    endif;

elseif ($doc == 'participation'):

    $participation = get_object('document_master', $doc_id);
    if ($doc == 'participation' && is_object($participation) && !empty($student_id)):
        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $participation->id, $student_id);

        /* Load the last updated doc if is it */
        if (is_object($last_his)):
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'DATE' => date('d M,Y'),
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($participation)):
                $content = $participation->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
        else:
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($participation)):
                $content = $participation->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
#Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);
#Add entry to history
            $_POST['student_id'] = $student_id;
            $_POST['doc_type'] = $doc->type;
            $_POST['school_id'] = $school->id;
            $_POST['session_id'] = $session_id;
            $_POST['section'] = $ct_sec;
            $_POST['document_id'] = $doc_id;
            $_POST['document_text'] = $content;
            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);
        endif;
    endif;


elseif ($doc == 'transfer'):

    $participation = get_object('document_master', $doc_id);
    if ($doc == 'transfer' && is_object($participation) && !empty($student_id)):
        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $participation->id, $student_id);

        /* Load the last updated doc if is it */

        if (is_object($last_his)):
            $content = $last_his->document_text;
            echo $content;

        else:
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $sessioniNFO = get_object('session', $session_id);

            $QueryStComSub = new subject();
            $cmpSub = $QueryStComSub->GetSessionSubjectsInArray($sessioniNFO->compulsory_subjects);

            $QuerySt = new studentSessionInterval();
            $bal = $QuerySt->getOnlyPreviousSessionBalance($session_id, $student_id);
            if (isset($bal['sum']) && $bal['sum'] > 0):
                $Balance = 'Not Fully Paid';
            else:
                $Balance = 'Fully Paid';
            endif;

            $QuerySt = new attendance();
            $att = $QuerySt->checkSessionStudentAttendace($session_id, $ct_sec, $student_id);
            if (isset($att['P'])):
                $present = $att['P'];
            else:
                $present = '';
            endif;
            $total_days = array_sum($att);
//echo '<pre>'; print_r($att);  echo '<pre>'; print_r($student); exit;


            $replace = array('SCHOOL_NAME' => $school->school_name,
                'SCHOOL_ADDRESS' => $school->address1 . ' ' . $school->city . ' ' . $school->district,
                'AFF_NUM' => $school->affiliation_code,
                'NAME' => $student->first_name . " " . $student->last_name,
                'CAST' => $student->cast,
                'NUMBER' => $student->reg_id,
                'ADDMISSION_DATE' => $student->date_of_admission,
                'DOB' => $student->date_of_birth,
                'DOB_IN_WORD' => date('d M Y', strtotime($student->date_of_birth)),
                'LAST_CLASS' => $student->session_name,
                'CONCESSION' => $student->concession_type,
                'SUBJECT' => implode(',', $cmpSub),
                'DUES_PAID' => $Balance,
                'PRESENT_WORKING_DAYS' => $present,
                'WORKING_DAYS' => $total_days,
                'FATHER_NAME' => $student->father_name,
                'MOTHER_NAME' => $student->mother_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'COURSE' => $student->session_name,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'DATE_OF_ISSUE_APPLICATION' => date('d M,Y'),
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($participation)):
                $content = $participation->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
#Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);
#Add entry to history
            $_POST['student_id'] = $student_id;
            $_POST['doc_type'] = $doc->type;
            $_POST['school_id'] = $school->id;
            $_POST['session_id'] = $session_id;
            $_POST['section'] = $ct_sec;
            $_POST['document_id'] = $doc_id;
            $_POST['document_text'] = $content;
            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);
        endif;
    endif;



elseif ($doc == 'leaving'):

    $leaving = get_object('document_master', $doc_id);
    if ($doc == 'leaving' && is_object($leaving) && !empty($student_id)):
        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $leaving->id, $student_id);

        /* Load the last updated doc if is it */
        if (is_object($last_his)):
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'COURSE' => $student->session_name,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'DATE' => date('d M,Y'),
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($leaving)):
                $content = $leaving->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
        else:
#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'NAME' => $student->first_name . " " . $student->last_name,
                'FATHER_NAME' => $student->father_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'COURSE' => $student->session_name,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'DATE_OF_BIRTH' => $student->date_of_birth,
                'SESSION' => $session->start_date . ' to ' . $session->end_date,
                'PHOTO' => $student_image,
                'LOGO' => $school_logo
            );

            $content = '';
            if (is_object($leaving)):
                $content = $leaving->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
#Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);
#Add entry to history
            $_POST['student_id'] = $student_id;
            $_POST['doc_type'] = $doc->type;
            $_POST['school_id'] = $school->id;
            $_POST['session_id'] = $session_id;
            $_POST['section'] = $ct_sec;
            $_POST['document_id'] = $doc_id;
            $_POST['document_text'] = $content;
            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);
        endif;
    endif;

elseif ($doc == 'report'):
    $dmc = get_object('document_master', $doc_id);
    if ($doc == 'report' && is_object($dmc) && !empty($student_id)):
        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $dmc->id, $student_id);

        /* Load the last updated doc if is it */
        if (is_object($last_his)):
#get exam Info
            $QueryG = new examinationGroup();
            $group = $QueryG->getRecord($exam_id);

#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

#get Student result
            $QueryRes = new examination();
            $Result = $QueryRes->single_student_all_examination_result($group->exam_id, $student_id);

            $result_data = '';
            $minimum = '0';
            $obtain = '0';
            $maximum = '0';
            $sr = '1';
            if (!empty($Result)):
                $result_data.="<table style='width: 100%; text-align: center; line-height: 40px;'>
												<tr><th style='text-align:left;width:10%;border-bottom:1px solid;' >Sr. No.</th><th style='text-align: left; width: 30%;border-bottom:1px solid;'>Subject Name</th><th style='width:20%;border-bottom:1px solid;'>Passing Marks</th><th style='width:20%;border-bottom:1px solid;'>Minimum Obtained</th><th style='width:20%;border-bottom:1px solid;'>Maximum Marks</th></tr>
												";
                foreach ($Result as $r_k => $r_v):
                    $result_data.="<tr><td style='text-align:left;width:10%;' >" . $sr . ".</td><td style='text-align: left; width: 30%;'>" . $r_v->subject . "</td><td style='width:20%;'>" . $r_v->minimum_marks . "</td><td style='width:20%;'>" . $r_v->marks_obtained . "</td><td style='width:20%;'>" . $r_v->maximum_marks . "</td></tr>";

                    $minimum = $minimum + $r_v->minimum_marks;
                    $obtain = $obtain + $r_v->marks_obtained;
                    $maximum = $maximum + $r_v->maximum_marks;
                    $sr++;
                endforeach;
                $result_data.="</table>";
            endif;

#Get Percentage
            $percentage = number_format(($obtain / $maximum) * 100, 2) . "%";

#Get exam Grades
            $QueryGrOb = new examGrade();
            $all_grade = $QueryGrOb->listAll($school->id);

#Get Grade
            if (!empty($all_grade)): $grade = 'None';
                foreach ($all_grade as $kg => $kv):
                    if (($percentage >= $kv->minimum) && ($percentage < $kv->maximum)): $grade = $kv->title;
                    endif;
                endforeach;
            else:
                $grade = 'None';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'TITLE' => $group->title,
                'NAME' => $student->first_name . " " . $student->last_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'MINIMUM_TOTAL' => $minimum,
                'OBTAIN_TOTAL' => $obtain,
                'MAXIMUM_TOTAL' => $maximum,
                'PERCENTAGE' => $percentage,
                'GRADE' => $grade,
                'MARKS_RESULT' => $result_data,
                'SESSION' => date('d, M Y', strtotime($session->start_date)) . ' - ' . date('d, M Y', strtotime($session->end_date))
            );
            $content = '';
            if (is_object($dmc)):
                $content = $dmc->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, $content));
                        $content = str_replace('&nbsp;', '', $content);
                    endforeach;
                    echo $content;
                endif;
            endif;
        else:
#get exam Info
            $QueryG = new examinationGroup();
            $group = $QueryG->getRecord($exam_id);

#get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

#get session detail	
            $session = get_object('session', $session_id);

#get Student result
            $QueryRes = new examination();
            $Result = $QueryRes->single_student_all_examination_result($group->exam_id, $student_id);

            $result_data = '';
            $minimum = '0';
            $obtain = '0';
            $maximum = '0';
            $sr = '1';
            if (!empty($Result)):
                $result_data.="<table style='width: 100%; text-align: center; line-height: 40px;'>
												<tr><th style='text-align:left;width:10%;border-bottom:1px solid;' >Sr. No.</th><th style='text-align: left; width: 30%;border-bottom:1px solid;'>Subject Name</th><th style='width:20%;border-bottom:1px solid;'>Passing Marks</th><th style='width:20%;border-bottom:1px solid;'>Minimum Obtained</th><th style='width:20%;border-bottom:1px solid;'>Maximum Marks</th></tr>
												";
                foreach ($Result as $r_k => $r_v):
                    $result_data.="<tr><td style='text-align:left;width:10%;' >" . $sr . ".</td><td style='text-align: left; width: 30%;'>" . $r_v->subject . "</td><td style='width:20%;'>" . $r_v->minimum_marks . "</td><td style='width:20%;'>" . $r_v->marks_obtained . "</td><td style='width:20%;'>" . $r_v->maximum_marks . "</td></tr>";

                    $minimum = $minimum + $r_v->minimum_marks;
                    $obtain = $obtain + $r_v->marks_obtained;
                    $maximum = $maximum + $r_v->maximum_marks;
                    $sr++;
                endforeach;
                $result_data.="</table>";
            endif;

#Get Percentage
            $percentage = number_format(($obtain / $maximum) * 100, 2) . "%";

#Get exam Grades
            $QueryGrOb = new examGrade();
            $all_grade = $QueryGrOb->listAll($school->id);

#Get Grade
            if (!empty($all_grade)): $grade = 'None';
                foreach ($all_grade as $kg => $kv):
                    if (($percentage >= $kv->minimum) && ($percentage < $kv->maximum)): $grade = $kv->title;
                    endif;
                endforeach;
            else:
                $grade = 'None';
            endif;

            $replace = array('SCHOOL' => $school->school_name,
                'TITLE' => $group->title,
                'NAME' => $student->first_name . " " . $student->last_name,
                'CLASS' => $student->session_name,
                'SECTION' => $student->section,
                'ROLL_NO' => $student->roll_no,
                'GENDER' => $student->sex,
                'MINIMUM_TOTAL' => $minimum,
                'OBTAIN_TOTAL' => $obtain,
                'MAXIMUM_TOTAL' => $maximum,
                'MARKS_RESULT' => $result_data,
                'PERCENTAGE' => $percentage,
                'GRADE' => $grade,
                'SESSION' => date('d, M Y', strtotime($session->start_date)) . ' - ' . date('d, M Y', strtotime($session->end_date))
            );
            $content = '';
            if (is_object($dmc)):
                $content = $dmc->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, $content));
                        $content = str_replace('&nbsp;', '', $content);
                    endforeach;
                    echo $content;
                endif;
            endif;

#Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);
#Add entry to history
            $_POST['student_id'] = $student_id;
            $_POST['doc_type'] = $doc->type;
            $_POST['school_id'] = $school->id;
            $_POST['session_id'] = $session_id;
            $_POST['section'] = $ct_sec;
            $_POST['document_id'] = $doc_id;
            $_POST['document_text'] = $content;
            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);

        endif;
    endif;
elseif ($doc == 'test_report_card'):
    $test_report_card = get_object('document_master', $doc_id);
    if ($doc == 'test_report_card' && is_object($test_report_card) && !empty($student_id)):
        /* Get Last Entry */
        $QueryDocHis = new SchoolDocumentPrintHistory();
        $last_his = $QueryDocHis->getLastPrintDoc($school->id, $session_id, $ct_sec, $test_report_card->id, $student_id);

        /* Load the last updated doc if is it */
        if (is_object($last_his)):
            #get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

            #get session detail	
            $session = get_object('session', $session_id);

            // Get Student Address Details
            $obj = new studentAddress;
            $student_address = $obj->getStudentAddressByType($student_id, 'permanent');

            // Get Student Attendance
            $student_attendance = attendance::getTotalAttendanceOfStudent($student_id, $session_id, $ct_sec);

            // Get Total Working Days
            $total_working_days = attendance::getTotalWorkingDays($ct_sec, $session_id, $student_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;

            $replace = array(
                'LOGO' => $school_logo,
                'SCHOOL_NAME' => $school->school_name,
                'SCHOOL_ADDRESS' => $school->address1 . ' ' . $school->city . ' ' . $school->district,
                'SCHOOL_PHONE' => $school->school_phone,
                'SCHOOL_EMAIL' => $school->email_address,
                'SCHOOL_TOTAL_WORKING_DAYS' => $total_working_days,
                'SCHOOL_WEB' => $school->website_url,
                'SCHOOL_AFFL' => $school->affiliation_code,
                'STUDENT_NAME' => $student->first_name . " " . $student->last_name,
                'STUDENT_FATHER_NAME' => $student->father_name,
                'STUDENT_MOTHER_NAME' => $student->mother_name,
                'STUDENT_CLASS' => $student->session_name,
                'STUDENT_ADMISSION' => $student->reg_id,
                'STUDENT_DOB' => $student->date_of_birth,
                'STUDENT_SECTION' => $student->section,
                'STUDENT_TOTAL_ATTENDANCE' => $student_attendance,
                'STUDENT_HOUSE' => $student_address->city,
                'STUDENT_RATN' => $student_address->address1 . ', ' . $student_address->address2 . ', ' . $student_address->tehsil . ', ' . $student_address->district . ', ' . $student_address->city . ' (' . $student_address->state . ') - ' . $student->phone
            );

            $content = '';
            if (is_object($test_report_card)):
                $content = $test_report_card->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
        else:
            #// Get Examination Group Details
            $obj = new examinationGroup;
            $group = $obj->getRecord($group_id);

            $obj = new examination();
            $subjects = $obj->getSubjectMarks($group->exam_id, $student_id);

            // Get Exam Details
            $obj = new examination;
            $exams = $obj->getExamDetails($group->exam_id);

            #get Student detail	
            $QueryStu = new studentSession();
            $student = $QueryStu->SingleStudentsWithSession($student_id, $session_id, $ct_sec);

            #get session detail	
            $session = get_object('session', $session_id);

            // Get Student Address Details
            $obj = new studentAddress;
            $student_address = $obj->getStudentAddressByType($student_id, 'permanent');

            // Get Student Attendance
            $student_attendance = attendance::getTotalAttendanceOfStudent($student_id, $session_id, $ct_sec);

            // Get Total Working Days
            $total_working_days = attendance::getTotalWorkingDays($ct_sec, $session_id, $student_id);

            $im_obj = new imageManipulation();
            if ($student->photo):
                $student_image = "<img src='" . $im_obj->get_image_echo('student', 'medium', $student->photo) . "' style='max-width:165px'/>";
            else:
                $student_image = '<img src="assets/img/profile/img-1.jpg" style="max-width:165px"/>';
            endif;
            if ($school->logo):
                $school_logo = "<img src='" . $im_obj->get_image_echo('school', 'medium', $school->logo) . "' style='max-width:165px'/>";
            else:
                $school_logo = '<img src="assets/img/invoice/webgarh.png"  />';
            endif;
            $exam_table = '<table class="table table-bordered" style="width: 98%; border: 1px #ddd ridge">'
                    . '<thead><tr>'
                    . '<th style="text-align: center">SUBJECT</th><th style="text-align: center">' . ucfirst($group->title) . '</th>'
                    . '</tr></thead>'
                    . '<tbody><tr><td>'
                    . '<table class="table table-bordered" style="width: 98%; border: 1px #ddd ridge">'
                    . '<tr><td style="text-align: center"><b>MAX. MARKS</b></td></tr>'
                    . '<tr"><td style="text-align: center"><b>EXAMS</b></td></tr>';
            foreach ($subjects as $subject):
                $exam_table .= '<tr><td style="text-align: center">' . $subject->title . '</td></tr>';
            endforeach;
            $exam_table.='</table></td><td>'
                    . '<table class="table table-bordered" style="width: 98%; border: 1px #ddd ridge">'
                    . '<thead><tr>';
            foreach ($exams as $exam):
                $exam_table.='<td style="text-align: center">' . $exam->maximum_marks . '</td>';
            endforeach;
            $exam_table.='</tr></thead>'
                    . '<tbody><tr>';
            foreach ($exams as $exam):
                $exam_table.='<td style="text-align: center"><b>' . ucfirst($exam->title) . '</b></td>';
            endforeach;
            $exam_table .= '</tr></tbody>';
            foreach ($subjects as $subject):
                $exam_table.='<tbody><tr>';
                foreach ($exams as $exam):
                    $exam_table.='<td style="text-align: center">' . examination::getExamniationGrade($student_id, $exam->id, $exam->minimum_marks, $exam->maximum_marks, $subject->subject_id, $school->id) . '</td>';
                endforeach;
                $exam_table.='</tr></tbody>';
            endforeach;
            $exam_table.= '</table>'
                    . '</tbody></table>';
            $replace = array(
                'LOGO' => $school_logo,
                'SCHOOL_NAME' => $school->school_name,
                'SCHOOL_ADDRESS' => $school->address1 . ' ' . $school->city . ' ' . $school->district,
                'SCHOOL_PHONE' => $school->school_phone,
                'SCHOOL_EMAIL' => $school->email_address,
                'SCHOOL_TOTAL_WORKING_DAYS' => $total_working_days,
                'SCHOOL_WEB' => $school->website_url,
                'SCHOOL_AFFL' => $school->affiliation_code,
                'STUDENT_NAME' => $student->first_name . " " . $student->last_name,
                'STUDENT_FATHER_NAME' => $student->father_name,
                'STUDENT_MOTHER_NAME' => $student->mother_name,
                'STUDENT_CLASS' => $student->session_name,
                'STUDENT_ADMISSION' => $student->reg_id,
                'STUDENT_DOB' => $student->date_of_birth,
                'STUDENT_SECTION' => $student->section,
                'STUDENT_TOTAL_ATTENDANCE' => $student_attendance,
                'STUDENT_HOUSE' => $student_address->city,
                'STUDENT_RATN' => $student_address->address1 . ', ' . $student_address->address2 . ', ' . $student_address->tehsil . ', ' . $student_address->district . ', ' . $student_address->city . ' (' . $student_address->state . ') - ' . $student->phone,
                'EXAM_TABLE' => $exam_table
            );

            $content = '';
            if (is_object($test_report_card)):
                $content = $test_report_card->format;
                if (count($replace)):
                    foreach ($replace as $k => $v):
                        $literal = '{' . trim(strtoupper($k)) . '}';
                        $content = html_entity_decode(str_replace($literal, $v, trim($content)));
                    endforeach;
                    echo $content;
                endif;
            endif;
            #Get document
            $QueryDoc = new document();
            $doc = $QueryDoc->getRecord($doc_id);
            #Add entry to history
            $_POST['student_id'] = $student_id;
            $_POST['doc_type'] = $doc->type;
            $_POST['school_id'] = $school->id;
            $_POST['session_id'] = $session_id;
            $_POST['section'] = $ct_sec;
            $_POST['document_id'] = $doc_id;
            $_POST['document_text'] = $content;
            $QuerySess = new SchoolDocumentPrintHistory();
            $QuerySess->saveData($_POST);
        endif;
    endif;
endif;
?>