

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Quick SMS 
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>Quick SMS</li>
                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php  //include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            
			?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
        <? if($type=='select'):?>
			
			<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>List SMS</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
						<form action='' class="form-horizontal" method='get' style='margin:0'>
							<input type='hidden' name='Page' value='sms'/>
							<input type='hidden' name='action' value='list'/>
							<input type='hidden' name='section' value='list'/>
							<input type='hidden' name='type' value='type'/>							
							<div class="portlet-body">								
								<table class="table table-striped table-hover">
									<thead>
										 <tr>
											<th width='8%'></th>
											<th>SMS Text</th>
											</tr>
									</thead>
									<tbody>                                                                            
										<?php 
										if(!empty($record)):$sr=1;
										foreach($record as $key=>$object):?>
											<tr class="odd gradeX">
													<td class='hidden-480 sorting_disabled' style='text-align:center;'><input style='margin-left:0px;' type='radio' name='sms_id' <?=($sr=='1')?'checked':'';?> value='<?=$object->id?>' checked /> </td>												
													<td><?php echo $object->sms_text;?></td>
											</tr>
										<?php $sr++;
										endforeach;
										else:
											echo "<td colspan='2'>Sorry, No record found...!</td>";
										endif;?>
									</tbody>
								</table>	
								<div class="form-actions ">
									<? if(!empty($record)):?>
										<input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
									<?php endif;?>  
								</div>								
							</div>
							
							
						</form>	
						
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>
		<? else:?>	
			<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>Select Information
								<span class='code' style='margin-left:10px;font-size:13px;'>{ Note: This message will be sent to all active students.}</span>
								</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">	
								<form id="validation" class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?=make_admin_url('sms','list','list&type=type&sms_id='.$sms_id);?>">
							
									<div class="row-fluid">
										<div class="control-group">
  	                                        <label for="" class="control-label">SMS TEXT</label>
 													<div class="controls">
                                                      <textarea rows='4' style='width:90%;' ><?=$sms->sms_text?></textarea>
													</div>
                                        </div> 
									</div>
									<div class="row-fluid">
										
										<? if($sms_id=='1'):?>	
										<div class="span10 ">
											<div class="control-group">
  	                                        <label for="DATE" class="control-label">DATE<span class="required">*</span></label>
 													<div class="controls">
                                                      <input type="text" class="span5 new_format_text validate[required]" id="DATE" value="" name="DATE">
													  <span style='font-size:15px;color:green'>"Date of Meting"</span>
													</div>
                                            </div> 											
											<div class="control-group">
  	                                        <label for="FROM_TIME" class="control-label">FROM_TIME<span class="required">*</span></label>
 													<div class="controls">
                                                      <input type="text" class="span5 timepicker-default add-on validate[required]" id="FROM_TIME" value="" name="FROM_TIME">
													  <span style='font-size:15px;color:green'>"Meting Start Time"</span>
													</div>
                                            </div> 	 
											<div class="control-group">
  	                                        <label for="TO_TIME" class="control-label">TO_TIME<span class="required">*</span></label>
 													<div class="controls">
                                                      <input type="text" class="span5 timepicker-default add-on validate[required]" id="TO_TIME" value="" name="TO_TIME">
													  <span style='font-size:15px;color:green'>"Meting End Time"</span>
													</div>
                                            </div> 	 
										</div>
										
										<? elseif($sms_id=='2'):?>	
										<div class="span10 ">
											<div class="control-group">
  	                                        <label for="FROM_TIME" class="control-label">FROM_TIME<span class="required">*</span></label>
 													<div class="controls">
                                                      <input type="text" class="span5 timepicker-default add-on validate[required]" id="FROM_TIME" value="" name="FROM_TIME">
													  <span style='font-size:15px;color:green'>"School Start Time"</span>
													</div>
                                            </div> 	 
											<div class="control-group">
  	                                        <label for="TO_TIME" class="control-label">TO_TIME<span class="required">*</span></label>
 													<div class="controls">
                                                      <input type="text" class="span5 timepicker-default add-on validate[required]" id="TO_TIME" value="" name="TO_TIME">
													  <span style='font-size:15px;color:green'>"School End Time"</span>
													</div>
                                            </div> 	 
											<div class="control-group">
  	                                        <label for="START_DATE" class="control-label">START_DATE<span class="required">*</span></label>
 													<div class="controls">
                                                      <input type="text" class="span5 m-wrap new_format_text validate[required]" id="START_DATE" value="" name="START_DATE">
													  <span style='font-size:15px;color:green'>"Start Date"</span>
													</div>
                                            </div> 	 
										</div>
																			
 										<? endif;?>	
                                           
											
									</div>
									<div class="form-actions ">
										<a class="btn red" href="<?=make_admin_url('sms','list','list')?>"><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
										<input type="submit" tabindex="7" value="Submit" name="submit" class="btn blue"> 
									</div>	
								</form>
								
							</div>
						</div>	
					</div>	
			</div>			
		<? endif;?>	
        
		
		
		
		<div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



