
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Users 
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('user', 'list', 'list');?>">List Users</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        Edit User
                                    </li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('user', 'update', 'list')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Edit User</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>


                                            </div>
                                    </div>
                                    <div class="portlet-body form">

                                    
                                            <div class="control-group">
                                                    <label class="control-label" for="username">Username<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="username" value="<?php echo $page_cotent->username;?>" id="username" class="span6 m-wrap validate[required]"/>
                                                    </div>
                                            </div>        
                                            <div class="control-group">
                                                    <label class="control-label" for="password">Password<span class="required">*</span></label>
                                                     <div class="controls">
                                                       <input type="password" name="password"  value="<?php echo trim(encrypt_decrypt('decrypt',$page_cotent->password));?>" id="password" class="span6 m-wrap validate[required]" />
                                           
                                                     </div>
                                            </div>         
                                            <div class="control-group">
                                                    <label class="control-label" for="repassword">Confirm Password<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input type="password" id="repassword" value="<?php echo trim(encrypt_decrypt('decrypt',$page_cotent->password));?>"  class="span6 m-wrap validate[required,equals[password]]"  />
                                                    </div>
                                            </div>  
                                            <div class="control-group">
                                                    <label class="control-label" for="email">Email Address<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input type="text" name="email" id="email" value="<?php echo $page_cotent->email;?>" class="span6 m-wrap validate[required,custom[email]]" />
                                                    </div>
                                            </div> 

																						
											<div class="control-group">
                                                    <label class="control-label" for="is_active">Make User Active</label>
                                                    <div class="controls">
                                                       <input type="checkbox" name="is_active" id="is_active" value="1" <?=($page_cotent->is_active=='1')?'checked':'';?>/>
													</div>
                                            </div>
											

                                          <input type="hidden" name="id" value="<?php echo $page_cotent->id?>">
                                    
                                            <div class="form-actions">
                                                    <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
													<a href="<?php echo make_admin_url('user', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
                                  
                                  
                              </div> 
                            </div>
                        </div>
                          


                     </form>
                     <div class="clearfix"></div>
                                          
                                   
                
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    




<script type="text/javascript">
        jQuery(document).ready(function() {
			$(".select_admin").live('click',function(){	
				var type=$(this).val();

				if(type=='Payment' || type=='Affiliate'){
						$("#dyanamic_admin").show();
					}
					else{
						$("#dyanamic_admin").hide();
					}
				});	
			
		});
 </script>	















