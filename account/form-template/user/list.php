

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">

            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Users 
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>List Users</li>

            </ul>


            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>

    <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>

    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Manage Users</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">								
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th class="hidden-480">Sr. No.</th>
                                <th>Username</th>
                                <th class="hidden-480 sorting_disabled">Email</th>
                                <th class="hidden-480 sorting_disabled">User Type</th>
                                <th style='text-align:right;'>Action</th>
                            </tr>
                        </thead>
                        <tbody>                                                                            
                            <?php $sr = 1;
                            while ($QueryObj1 = $QueryObj->GetObjectFromRecord()):
                                ?>
                                <tr class="odd gradeX">
                                    <td  class="hidden-480"><?php echo $sr++; ?>.</td>
                                    <td><?php echo $QueryObj1->username; ?></td>
                                    <td class="hidden-480 sorting_disabled"><a href="mailto:<?php echo $QueryObj1->email; ?>"><?php echo $QueryObj1->email; ?></a></td>
                                    <td class="hidden-480 sorting_disabled"><?= ucfirst($QueryObj1->user_type) ?></td>
                                    <td style='text-align:right;'>
                                        <a class="btn blue icn-only mini tooltips" href="<?php echo make_admin_url('user', 'update', 'update', 'id=' . $QueryObj1->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                        <a class="btn red icn-only mini tooltips" href="<?php echo make_admin_url('user', 'delete', 'list', 'id=' . $QueryObj1->id . '&delete=1') ?>" onclick="return confirm('Are you sure? You are deleting this page.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                    </td>
                                </tr>
<?php endwhile; ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



