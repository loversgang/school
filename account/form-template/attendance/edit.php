
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Sessions
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('session', 'list', 'list');?>">List Session</a>
                                         <i class="icon-angle-right"></i>                                       
                                    </li>
                                    <li class="last">
                                        Edit Session
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
		  
			  
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('session', 'update', 'update&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Edit Session</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">      
									<!-- Basic Info  -->
										<h4 class="form-section"></h4>
                                        <div class="row-fluid">    
											
											<div class="control-group span6">
                                                    <label class="control-label" for="course_id">Select Course<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select name="course_id" id="course_id"  class="span10 m-wrap ">
														<?php $sr=1;while($course=$QueryCourse->GetObjectFromRecord()):?>
																	<option value='<?=$course->id?>' <?=($object->course_id==$course->id)?'selected':'';?>><?=ucfirst($course->course_name)?></option>
														<? $sr++; endwhile;?>															
                                                        </select>                      
													</div>
                                            </div>

										</div>		
										<div class="row-fluid">
										<div class="span6 ">
											<div class="control-group">
                                                    <label class="control-label" for="year">Select Year<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select  name="year" id="year"  class="span10 m-wrap ">
														<?php $start_year=$school->established_year;
															 if(!empty($start_year)){ $start_year='2000';}
																while($start_year<=date('Y')):?>
																	<option value='<?=$start_year?>' <?=($object->year==$start_year)?'selected':'';?>><?=$start_year?></option>
														<? $start_year++; endwhile;?>															
                                                        </select>                      
													</div>
                                            </div>											
											<div class="control-group">
  	                                        <label class="control-label" for="start_date">Start Date<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="start_date" value="<?=$object->start_date?>" id="start_date" class="span10 m-wrap normal_date validate[required]" />
													</div>
                                            </div> 	  
 											<div class="control-group">
  	                                        <label class="control-label" for="total_sections_allowed">Total Section<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="total_sections_allowed" value="<?=$object->total_sections_allowed?>" id="total_sections_allowed" class="span10 m-wrap validate[required,custom[onlyNumberSp],max[<?=$school_setting->max_sections_per_session?>]]"/>
                                                    </div>
                                            </div> 
											<div class="control-group">
  	                                        <label class="control-label" for="max_boy">Max Boys Per Section<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="max_boy" value="<?=$object->max_boy?>" id="max_boy" class="span10 m-wrap validate[required,custom[onlyNumberSp],max[<?=$school_setting->max_student_per_session?>]]"/>
                                                    </div>
                                            </div> 												

											<div class="control-group">
	                                        <label class="control-label" for="compulsory_subjects">Compulsory Subjects<span class="required">*</span></label>
                                                    <div class="controls">
													<? 	$det=html_entity_decode($object->compulsory_subjects);
														$all_c_sub= unserialize($det);?>
                                                   <?php $sr=1;while($subject=$QuerySubject->GetObjectFromRecord()):?>
														<div class="span6" <? if($sr%2){ echo 'style="margin-left:0px;"';}?>>													
															<span><input type="checkbox" name='compulsory_subjects[]' value="<?=$subject->id?>" <? if(in_array($subject->id,$all_c_sub)){ echo 'checked';}?> ></span><?=ucfirst($subject->name)?>
														</div>								
														<? $sr++; endwhile;?>	
													</div>
                                            </div> 											
 											<div class="control-group">
	                                        <label class="control-label" for="phone">Fee Heads<span class="required">*</span></label>
                                                    <div class="controls">
													<?php $sr_fee=1;while($fee=$QueryFee->GetObjectFromRecord()):?>
														<div class="span6" <? if($sr_fee%2){ echo 'style="margin-left:0px;"';}?>>													
															<span><input type="checkbox" class='fee_sub_head' value="<?=$fee->id?>" show_name='<?=ucfirst($fee->name)?>' <? if(in_array($fee->id,$all_heads)){ echo 'checked';}?> ></span><?=ucfirst($fee->name)?>
														</div>
														<? $sr_fee++; endwhile;?>													
													</div> 	
													<input type='hidden' value='0' id='total_head'/>	
											</div>
 											<div class="control-group">
												<div class="controls">
												<table class="table table-striped table-bordered table-advance table-hover">
												<thead>
												<tr><th>Fee Head Name</th><th>Amount (<?=CURRENCY_SYMBOL?>)</th>
												<tbody id='add_delete_heads'></tbody>
												<? if(!empty($pay_heads)): $st='0';
														foreach($pay_heads as $p_k=>$p_v):?>
															<tr class='delete_row<?=$p_v['fee_head']?>'><td><?=$p_v['title']?></td><td>
															<input type="hidden" name="head[<?=$p_v['fee_head']?>][title]" value="<?=$p_v['fee_head']?>"/>
															<input class="span10" type="text" placeholder="0.00" name="head[<?=$p_v['fee_head']?>][fee]" value="<?=$p_v['amount']?>"/></td></tr>
												<? 		endforeach;
													endif;?>
												</tr>
												</thead>
												</table>
												</div>  
											 </div>  
										</div>
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="incharge">In-charge Teacher<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select name="incharge" id="incharge"  class="span10 m-wrap ">
														<?php $sr=1;while($incharge=$QueryStaff->GetObjectFromRecord()):?>
																	<option value='<?=$incharge->id?>' <?=($object->incharge==$incharge->id)?'selected':'';?>><?=$incharge->title." ".$incharge->first_name." ".$incharge->last_name?></option>
														<? $sr++; endwhile;?>																													
                                                        </select>                      
													</div>
                                            </div>										
											<div class="control-group">
  	                                        <label class="control-label" for="end_date">End Date<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="end_date" value="<?=$object->end_date?>" id="end_date" class="span10 m-wrap normal_date validate[required]" />
													</div>
                                            </div> 	
											<div class="control-group">
  	                                        <label class="control-label" for="max_sections_students">Max Students Per Section<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="max_sections_students" value="<?=$object->max_sections_students?>" id="max_sections_students" class="span10 m-wrap validate[required,custom[onlyNumberSp],max[<?=$school_setting->max_student_per_session?>]]"/>
                                                    </div>
                                            </div>	
											<div class="control-group">
  	                                        <label class="control-label" for="max_girl">Max Girls Per Section<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="max_girl" value="<?=$object->max_girl?>" id="max_girl" class="span10 m-wrap validate[required,custom[onlyNumberSp],max[<?=$school_setting->max_student_per_session?>]]"/>
                                                    </div>
                                            </div> 												

											<div class="control-group">
	                                        <label class="control-label" for="elective_subjects">Elective Subjects<span class="required">*</span></label>
                                                    <div class="controls">
													<? 	$all_el_sub=array();
														$det1=html_entity_decode($object->elective_subjects);
														$all_el_sub= unserialize($det1);?>
                                                   <?php $sr=1;while($subject1=$QuerySubject1->GetObjectFromRecord()):?>
														<div class="span6" <? if($sr%2){ echo 'style="margin-left:0px;"';}?>>													
															<span><input type="checkbox" name='elective_subjects[]' value="<?=$subject1->id?>" <? if(in_array($subject1->id,$all_el_sub)){ echo 'checked';}?> ></span><?=ucfirst($subject1->name)?>
														</div>								
														<? $sr++; endwhile;?>	
													</div>													
                                            </div> 											

										</div>	
										</div>	
										   
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_keyword">Make Active</label>
                                                    <div class="controls">
                                                    <input type="checkbox" name="is_active" id="meta_keyword" value="1" <?=($object->is_active=='1')?'checked':'';?>>
													</div>
                                            </div> 
											
                                            <div class="form-actions">
													 <input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" />
													 <input type="hidden" name="id" value="<?=$object->id?>" tabindex="7" /> 													 
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('session', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
                              </div>
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

	
	 <script type="text/javascript">
        jQuery(document).ready(function() {
            $(".fee_sub_head").live("click",function(){
			//add_delete_heads
			var value=$(this).val();
			var show_name=$(this).attr('show_name');
			
			 if (!$(this).is(':checked')) {
					var current_hd=$('#total_head').val();
					var new_hd=value;
					$('.delete_row'+value+'').remove();
					$('#total_head').val(new_hd);			
				
				}
			else
				{
					var current_hd=$('#total_head').val();
					var new_hd=value;	
					$('#add_delete_heads').append("<tr class='delete_row"+new_hd+"'><td><input type='hidden' name='head["+new_hd+"][title]' class='span2' value='"+value+"'/>"+show_name+"</td><td><input type='text' name='head["+new_hd+"][fee]' class='span10' placeholder='0.00'/></td></tr>");
					$('#total_head').val(new_hd);
				}
			});	 
			});
 </script>		