<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');

if($_POST):
isset($_POST['id'])?$id=$_POST['id']:$id='';
isset($_POST['ct_s'])?$ct_s=$_POST['ct_s']:$ct_s='';

		#Get Section Students
			$QueryOb=new studentSession();
			$records=$QueryOb->sectionSessionStudents($id,$ct_s);
?>			
			<div class="row-fluid">	
				<div class="span12">
				<div class="control-group" >
					<label class="control-label">Select Student </label>
					<div class="controls">
								<table id="sample" class="table table-striped table-bordered table-advance table-hover">
									<thead>
										<tr>
											<th style='text-align:center;' class="hidden-480 sorting_disabled"></th>
											<th class="hidden-480">Reg ID</th>
											<th class="hidden-480" >Roll No.</th>
											<th class="hidden-480" >Name</th>
											<th class="hidden-480" >Father Name</th>
										</tr>
									</thead>
									
									<? if(!empty($records)):?>
									<tbody>
									<?php $sr=1;foreach($records as $sk=>$stu): $checked='';?>
										<tr class="odd gradeX">
											<td style='text-align:center;background:#F9F9F9;'>
													<input type="radio" name='st_id' checked value='<?=$stu->id?>'/>
													
											</td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->reg_id?></td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=$stu->roll_no?></td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
											<td class="hidden-480 sorting_disabled" style='background:#F9F9F9;'><?=ucfirst($stu->father_name)?></td>
											</tr>
									<? $sr++; endforeach;?>									
									</tbody>
								<? endif;?>
								</table>
					</div>
				</div>
				</div>	
			</div>	
<?		
endif;
?>