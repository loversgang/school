<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');

if($_POST):
isset($_POST['id'])?$id=$_POST['id']:$id='';
	
		/* Get session Pages*/
		 $QuerySec=new studentSession();
         $QuerySec->listAllSessionSection($id);
		 
			#get Session Info
			$QueryS=new session();
			$session=$QueryS->getRecord($id);		 
		
		 ?>
				
				<div class="control-group" >
					<label class="control-label">Select Section </label>
					<div class="controls">
					<? if($QuerySec->GetNumRows()>0): ?>					
						<select class="select2_category span5" data-placeholder="Select Session Students" name='ct_sect' id='ct_sect'>
							<?php $session_sc=1;while($sec=$QuerySec->GetObjectFromRecord()): print_r($sec->section); ?>
									<option value='<?=$sec->section?>'><?=ucfirst($sec->section)?></option>
							<? $session_sc++; endwhile;?>
						</select>
					<? else:?>
							<input type='text' value='Sorry, No Section Found.' disabled />
					<? endif;?>						
						
					</div>
				</div>
				
				<div class="clearfix"></div>
				<div id='show_students'></div>
<?		
endif;
?>