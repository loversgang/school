

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Class Attendance
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Add Attendance
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
		
			<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div>      
          
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
             
			<div class="row-fluid">
					<div class="span12">
						<div id="form_wizard_1" class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">
									<i class="icon-reorder"></i> Attendance Wizard - <span class="step-title">Step 1 of 2</span>
								</div>

							</div>
							<div class="portlet-body form">
								<div class="form-wizard">						
										<!-- Main steps -->
										<div class="navbar steps">
											<div class="navbar-inner">
												<ul class="row-fluid nav nav-pills">
													<li class="span4 <?=($type=='select')?'active':'';?>">
														<a class="step <?=($type=='select')?'active':'';?>">
														<span class="number">1</span>
														<span class="desc"><i class="icon-ok"></i> Select Session / Session & Date</span>   
														</a>
													</li>
													
													<li class="span5 <?=($type=='attendance')?'active':'';?>">
														<a class="step <?=($type=='attendance')?'active':'';?>">
														<span class="number">2</span>
														<span class="desc"><i class="icon-ok"></i> Attendance</span>   
														</a>
													</li>
												</ul>
											</div>
										</div>
										<!-- Main End -->
										<div class="progress progress-success progress-striped" id="bar">
											<div class="bar" style="width: <?=$per?>"></div>
										</div>
										
										<div class="tab-content">
											<!-- First steps -->
											<div id="tab1" class="tab-pane <?=($type=='select')?'active':'';?>">
												<form action="<?php echo make_admin_url('attendance', 'insert', 'insert')?>" method="GET" enctype="multipart/form-data" id="validation">
												<input type='hidden' name='Page' value='attendance'/>
												<input type='hidden' name='action' value='insert'/>
												<input type='hidden' name='section' value='insert'/>
												<input type='hidden' name='type' value='attendance'/>											
											
												<h3 class="block">Select Session & Section</h3>
													<div class="alert alert-block alert-success">
														<div class="row-fluid">
															<div class="span6">								
																<div class="control-group">
																	<label class="control-label">Select Session</label>
																	<div class="controls">
																		<select class="select2_category session_filter span9" data-placeholder="Select Session Students" name='session_id' id='session_id'>
																			<option value="">Select Session</option>
																			<?php $session_sr=1;while($session=$QuerySession->GetObjectFromRecord()): ?>
																					<option value='<?=$session->id?>' <? if($session->id==$session_id){ echo 'selected'; }?> ><?=ucfirst($session->title)?></option>
																			<? $session_sr++; endwhile;?>
																		</select>
																	</div>
																</div>
															</div>
															<div class="span6" id='dynamic_model'>								
																
															</div>
														</div>
														
														<div class="row-fluid">
																<div class="span5">
																<div class="control-group" >
																	<label class="control-label">Select Date </label>
																	<div class="controls">
																		<input type='text' value='<?=date('Y-m-d')?>' name='atten_date' class='upto_current_date span11 validate[required]' />																		
																	</div>
																</div>
																</div>
														</div>												
													</div>	
													<div class="form-actions clearfix" >
															<a href="<?php echo make_admin_url('attendance', 'list', 'list');?>" class="btn red" name="cancel" > Cancel</a>
															<input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 
													</div>
												</form>	
											</div>
											<!-- First steps End -->
											
											<!-- Second steps -->
											<div id="tab2" class="tab-pane <?=($type=='attendance')?'active':'';?>">
												<h3 id="title_show" class="block alert alert-info"><?=$object->title?><font class="green_color"> &gt; </font> Section <?=$ct_sect?> </h3>
													<div class="row-fluid">
															<div class="span12">
																<!-- BEGIN EXAMPLE TABLE PORTLET-->
																<div class="portlet box green">
																	<div class="portlet-title">
																		<div class="caption"><i class="icon-calendar"></i><? if(is_object($object)): echo $object->title; else: echo "Class"; endif;?> Attendance</div>
																		<div class="tools">
																			<font style='font-size:22px;'><?=date('d M, Y',strtotime($atten_date))?></font>
																		</div>
																	</div>
																	<div class="portlet-body">
																	<form action="<?php echo make_admin_url('attendance', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" id=''>									
																		<table class="table table-striped table-bordered table-hover attendance_table" id="">
																			<thead>
																				<tr>
																					<th class="hidden-480" style='vertical-align:top;'>Reg ID</th>
																					<th class="hidden-480" style='vertical-align:top;'>Roll No.</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;'>Name</th>
																					<th class="hidden-480 sorting_disabled" style='vertical-align:top;'>Father Name</th>
																					<th class="hidden-480 sorting_disabled" style='width:45%;vertical-align:top;text-align:center;'>
																					<div>Status</div><div class="span12">
																					<? $s1='1';
																						foreach($Attendance_array as $a_k=>$a_v):?>
																								<div class="span4">
																								<label class="radio">
																									<span><input type="radio" class='master_checker' value="<?=$a_k?>" <?=($a_k=='P')?'checked':'';?>></span> Set <?=$a_v?>
																								</label>
																								</div>
																					<? $s1++;
																						endforeach; ?>
																					</div>
																					</th>
																				</tr>
																			</thead>
																			<? if(!empty($atten_date)):?>
																			<? if(!empty($records)):?>
																			<tbody>
																			<?php $sr=0;foreach($records as $sk=>$stu): $checked='';?>
																				<tr class="odd gradeX">
																					<td class='main_title'><?=$stu->reg_id?></td>
																					<td class='main_title'><?=$stu->roll_no?></td>
																					<td class='main_title'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
																					<td class='main_title'><?=ucfirst($stu->father_name)?></td>
																					<td class="hidden-480 sorting_disabled" style='text-align:center;'>
																						<div class="span12">
																						<? foreach($Attendance_array as $a_k=>$a_v):?>
																									<div class="span4">
																										<label class="radio <?=$a_k?>">
																										<div class="radio"><span><input name='attendance[<?=$sr?>][status]' type="radio" value="<?=$a_k?>" class='all_atten' <? if(isset($stu->attendance) && ($a_k==$stu->attendance)): echo 'checked'; elseif($a_k=='P'): echo 'checked'; endif; ?>></span></div>
																										<?=$a_v?>
																										</label>
																									</div>
																						<? endforeach; ?>
																						</div>	
																					</td>
																					<input type='hidden' name='attendance[<?=$sr?>][student_id]' value='<?=$stu->id?>'/>
																					<input type='hidden' name='attendance[<?=$sr?>][date]' value='<?=$atten_date?>'/>
																					<input type='hidden' name='attendance[<?=$sr?>][roll_no]' value='<?=$stu->roll_no?>'/>
																					<input type='hidden' name='attendance[<?=$sr?>][section]' value='<?=$ct_sect?>'/>
																					<input type='hidden' name='attendance[<?=$sr?>][session_id]' value='<?=$session_id?>'/>											
																				</tr>
																			<? $sr++;
																				endforeach;?>									
																			</tbody>
																		<? endif;?>
																		<? endif;?>
																		</table>
																	<div class='span12' style='padding-right:35px; padding-top:10px;'>
																		<input type='hidden' name='date' value='<?=$date?>'/>
																		<input type='hidden' name='section' value='<?=$ct_sect?>'/>
																		<input type='hidden' name='ct_sect' value='<?=$ct_sect?>'/>
																		<input type='hidden' name='session_id' value='<?=$session_id?>'/>
																		
																	</div>
										
																	<div class="form-actions clearfix">
																		<input class="btn green" type="submit" value="Add Attendance" name="multi_select" style='float:right;'>
																		<a href='<?=make_admin_url('attendance','insert','insert')?>' class="btn red" style='float:right;margin-right:10px;'><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
																	</div>																	
																	<div class="clearfix"></div>
																	</form>	
																	</div>
																</div>
																						
																<!-- END EXAMPLE TABLE PORTLET-->
															</div>
														</div>											
											</div>
											<!-- Forth steps End -->
										</div>
										

									</div>
								
							</div>
						</div>
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

<script type="text/javascript">
	$(".session_filter").change(function(){	  
			var session_id=$("#session_id").val();
			if(session_id.length > 0){
					   var id=session_id;
					   $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section_only_sec','ajax_section_only_sec&temp=attendance');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#dynamic_model").html(data);
						}				
						});
			}
			else{
				return false;
			}
	});

</script> 	
	
	
<script type="text/javascript">

	$(".master_checker").click(function(){	 
			var ct_val=$(this).val();
			
			 if (!$(this).is(':checked')) {
					//$('.all_atten').val('P');
					$('.master_checker').removeAttr('checked');
					$('.master_checker').removeClass('radio');					
					$(".all_atten").each(function() {
						 if($(this).val()=='P'){
						   $(this).attr("checked",true);
						   $(this).parent().addClass('checked');
						}
						else{
						   $(this).removeAttr("checked");
						   $(this).parent().removeClass('checked');						
						}
					});					
				}
			else
				{
					$('.master_checker').removeAttr('checked');
					$('.master_checker').removeClass('radio');	
					$(".all_atten").each(function() {
						 if($(this).val()==ct_val){
						   $(this).attr("checked",true);
						   $(this).parent().addClass('checked');
						}
						else{
						   $(this).removeAttr("checked");
						   $(this).parent().removeClass('checked');						
						}
					});
					
					$(this).attr('checked',true); 
					$(this).parent().addClass('checked');	
					$(".master_checker").each(function() {
						 if($(this).val()!=ct_val){
						   $(this).removeAttr("checked");
						   $(this).parent().removeClass('checked');	
						}
					});					
					
				}			
	});	
</script> 
