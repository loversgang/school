
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Edit/View Class Attendance
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    All Sessions
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>



    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>

    <div class="row-fluid">
        <div class="span12">
            <div id="form_wizard_1" class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-reorder"></i> View Attendance Wizard - <span class="step-title">Step 1 of 3</span>
                    </div>
                    <div class="tools hidden-phone">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="config" data-toggle="modal" href="#portlet-config"></a>
                        <a class="reload" href="javascript:;"></a>
                        <a class="remove" href="javascript:;"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-wizard">						
                        <!-- Main steps -->
                        <div class="navbar steps">
                            <div class="navbar-inner">
                                <ul class="row-fluid nav nav-pills">
                                    <li class="span4 <?= ($type == 'select') ? 'active' : ''; ?>">
                                        <a class="step <?= ($type == 'select') ? 'active' : ''; ?>">
                                            <span class="number">1</span>
                                            <span class="desc"><i class="icon-ok"></i> Single / Multiple Students</span>   
                                        </a>
                                    </li>
                                    <li class="span4 <?= ($type == 'session') ? 'active' : ''; ?>">
                                        <a class="step <?= ($type == 'session') ? 'active' : ''; ?>">
                                            <span class="number">2</span>
                                            <span class="desc"><i class="icon-ok"></i> Select Session / Section & Date </span>   
                                        </a>
                                    </li>
                                    <li class="span4 <?= ($type == 'attendance') ? 'active' : ''; ?>">
                                        <a class="step <?= ($type == 'attendance') ? 'active' : ''; ?>" >
                                            <span class="number">3</span>
                                            <span class="desc"><i class="icon-ok"></i> View Attendance</span>   
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Main End -->
                        <div class="progress progress-success progress-striped" id="bar">
                            <div class="bar" style="width: <?= $per ?>"></div>
                        </div>

                        <div class="tab-content">

                            <!-- First steps -->
                            <div id="tab1" class="tab-pane <?= ($type == 'select') ? 'active' : ''; ?>">
                                <h3 class="block">Select Section</h3>
                                <div class="alert alert-block alert-success">
                                    <div class="row-fluid"><br/>
                                        <div class='span1'></div>
                                        <a href='<?= make_admin_url('attendance', 'list', 'list&type=session') ?>'>
                                            <div class='span4' style='text-align:center;'>
                                                <div class="large_tile selected bg-grey">
                                                    <div class="corner"></div>
                                                    <div class="tile-body">
                                                        <i class="icon-user"></i>
                                                    </div>
                                                    <div class="tile-object">
                                                        <div class="name">
                                                            Single Student
                                                        </div>
                                                    </div>
                                                </div>																
                                            </div>
                                        </a>
                                        <div class='span2'></div>
                                        <a href='<?= make_admin_url('attendance', 'list', 'list&select=multiple&type=session') ?>'>
                                            <div class="span4" style='text-align:center;'>
                                                <div class="large_tile selected bg-grey">
                                                    <div class="corner"></div>
                                                    <div class="tile-body">
                                                        <i class="icon-user "></i>
                                                    </div>
                                                    <div class="tile-object">
                                                        <div class="name">
                                                            Multiple Students
                                                        </div>
                                                    </div>
                                                </div>	
                                            </div>
                                        </a>			
                                        <div class='span2'></div>
                                    </div>
                                </div>	

                            </div>
                            <!-- First steps End-->										

                            <!-- Second steps -->
                            <div id="tab2" class="tab-pane <?= ($type == 'session') ? 'active' : ''; ?>">
                                <form action="<?php echo make_admin_url('attendance', 'attendance', 'attendance') ?>" method="GET" enctype="multipart/form-data" id="validation" class='validation'>
                                    <input type='hidden' name='Page' value='attendance'/>
                                    <input type='hidden' name='action' value='list'/>
                                    <input type='hidden' name='section' value='list'/>
                                    <input type='hidden' name='type' value='attendance'/>											


<? if ($select == 'multiple'): ?>
                                        <h3 class="block alert alert-info" id='title_show'>Multiple Students ></h3>	
                                        <h3 class="block">Select Session & Section</h3>	
                                        <div class="alert alert-block alert-success">
                                            <div class="row-fluid">
                                                <div class="span6">								
                                                    <div class="control-group">
                                                        <label class="control-label">Select Session</label>
                                                        <div class="controls">
                                                            <select class="select2_category session_filter span9" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                                <option value="">Select Session</option>
                                                                <?php $session_sr = 1;
                                                                while ($session = $QuerySession->GetObjectFromRecord()): ?>
                                                                    <option value='<?= $session->id ?>' <? if ($session->id == $session_id) {
                                                                echo 'selected';
                                                            } ?> ><?= ucfirst($session->title) ?></option>
        <? $session_sr++;
    endwhile; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" id='dynamic_model'>								

                                                </div>
                                            </div>

                                        </div>	
<? else: ?>
                                        <h3 class="block alert alert-info" id='title_show'>Single Student ></h3>
                                        <h3 class="block">Select Session & Section</h3>
                                        <div class="alert alert-block alert-success">
                                            <div class="row-fluid">
                                                <div class="span6">								
                                                    <div class="control-group">
                                                        <label class="control-label">Select Session</label>
                                                        <div class="controls">
                                                            <select class="select2_category session_filter_for_single span9" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                                <option value="">Select Session</option>
    <?php $session_sr = 1;
    while ($session = $QuerySession->GetObjectFromRecord()): ?>
                                                                    <option value='<?= $session->id ?>' <? if ($session->id == $session_id) {
            echo 'selected';
        } ?> ><?= ucfirst($session->title) ?></option>
        <? $session_sr++;
    endwhile; ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span6" id='dynamic_model'>	</div>														
                                            </div>

                                            <div id='dynamic_model'>			
                                                <input type='hidden' class='section_filter' value='A' id='ct_s'/>
                                            </div>												
                                        </div>													
<? endif; ?>													
                                    <div class="form-actions clearfix" >
                                        <input type='hidden' name='select' value='<?= $select ?>'/>
                                        <a href="<?php echo make_admin_url('attendance', 'list', 'list'); ?>" class="btn red" name="cancel" > Cancel</a>
                                        <input class="btn green check_valid" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                    </div>
                                </form>	
                            </div>
                            <!-- Second steps End -->


                            <!-- Third steps -->

                            <div id="tab3" class="tab-pane <?= ($type == 'attendance') ? 'active' : ''; ?>">

                                <div class="row-fluid">
                                    <div class="span12">
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->


<? if ($select == 'multiple'): ?>
                                            <h3 class="block alert alert-info" id='title_show'><? if (isset($object) && $object->title != '') {
        echo $object->title . "<font class='green_color'> > </font>";
    } ?> <? if (isset($ct_sect)) {
        echo "Section " . $ct_sect . "<font class='green_color'> > </font>";
    } ?> Multiple Students <font class='green_color'> > </font> Attendance</h3>	
                                            <div class="portlet box green">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-calendar"></i><font style='font-size:22px;'><? if (is_object($object)): echo $object->title;
                                                            else: echo "Class";
                                                            endif; ?> (<?= date('M Y', strtotime($year . '-' . $date . '-' . date('d'))) ?>) Attendance</font> </div>
                                                </div>
                                                <div class="portlet-body">																	
                                                    <table class="table table-striped table-bordered table-hover " id="sample_2">
                                                        <thead>
                                                            <tr>
                                                                <th class="hidden-480">Reg ID</th>
                                                                <th class="hidden-480" >Roll No.</th>
                                                                <th class="hidden-480" >Name</th>
                                                                <th class="hidden-480" >Father Name</th>
    <? foreach ($Attendance_array as $key => $value): ?>
                                                                    <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='<?= $key ?>'>&nbsp;<?= $value ?></font></th>
    <? endforeach; ?>
                                                                <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='NM'>&nbsp;Not Marked</font></th>
                                                                <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='W'>&nbsp;Working Days</font></th>																					
                                                            </tr>
                                                        </thead>
    <? if (!empty($records)): ?>
                                                            <tbody>
        <?php
        $sr = 1;
        foreach ($records as $sk => $stu): $checked = '';
            $Q_obj = new attendance();
            $r_atten = $Q_obj->checkSessionSectionMonthYearAttendace($session_id, $ct_sect, $stu->id, $date, $year);
            $t_attend = array_sum($r_atten);
            ?>
                                                                    <tr class="odd gradeX">
                                                                        <td class='main_title'><?= $stu->reg_id ?></td>
                                                                        <td class='main_title' style='text-align:center;'><?= $stu->roll_no ?></td>
                                                                        <td class='main_title'><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                                                        <td class='main_title'><?= ucfirst($stu->father_name) ?></td>
                                                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $r_atten['P'];
                                            else: echo '0';
                                            endif; ?></td>
                                                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
                                            else: echo '0';
                                            endif; ?></td>
                                                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
                                            else: echo '0';
                                            endif; ?></td>
                                                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('H', $r_atten)): echo $r_atten['H'];
                                            else: echo '0';
                                            endif; ?></td>
                                                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= ($working_days - $t_attend) ?></td>
                                                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $working_days ?></td>																				
                                                                    </tr>
        <? endforeach; ?>									
                                                            </tbody>

                                                            <? endif; ?>
                                                    </table>
                                                </div>	

                                                <!-- For Single Student -->	
                                                            <? else: ?>
                                                <h3 class="block alert alert-info" id='title_show'><? if (isset($object) && $object->title != '') {
                                                                echo $object->title . "<font class='green_color'> > </font>";
                                                            } ?> <? if (isset($ct_sect)) {
                                                                echo "Section " . $ct_sect . "<font class='green_color'> > </font>";
                                                            } ?> Single Student <font class='green_color'> > </font> Attendance</h3>	
                                                <div class="portlet box green">	
                                                    <div class="portlet-title">
                                                        <div class="caption"><i class="icon-calendar"></i><font style='font-size:22px;'><?= date('M Y', strtotime($year . '-' . $date . '-' . date('d'))) ?> Attendance</font> </div>
                                                    </div>
                                                    <div class="portlet-body">																	
                                                        <table class="table table-striped table-bordered table-hover " id="sample_2">
                                                            <thead>
                                                                <tr>
                                                                    <th class="hidden-480">Reg ID</th>
                                                                    <th class="hidden-480">Roll No.</th>
                                                                    <th class="hidden-480">Name</th>
                                                                    <th class="hidden-480">Father Name</th>
                                                            <? foreach ($Attendance_array as $key => $value): ?>
                                                                        <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='<?= $key ?>'>&nbsp;<?= $value ?></font></th>
    <? endforeach; ?>
                                                                    <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='NM'>&nbsp;Not Marked</font></th>
                                                                    <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='W'>&nbsp;Working Days</font></th>
                                                                </tr>
                                                            </thead>
    <? if (!empty($records)): ?>
                                                                <tbody>
        <?php
        $sr = 1;
        foreach ($records as $sk => $stu): $checked = '';

            $Q_obj = new attendance();
            $r_atten = $Q_obj->checkSessionSectionMonthYearAttendace($session_id, $ct_sect, $stu->id, $date, $year);
            $t_attend = array_sum($r_atten);
            ?>
                                                                        <tr class="odd gradeX">
                                                                            <td class='main_title'><?= $stu->reg_id ?></td>
                                                                            <td class='main_title' style='text-align:center;'><?= $stu->roll_no ?></td>
                                                                            <td class='main_title'><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                                                            <td class='main_title'><?= ucfirst($stu->father_name) ?></td>
                                                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo $r_atten['P'];
            else: echo '0';
            endif; ?></td>
                                                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo $r_atten['A'];
            else: echo '0';
            endif; ?></td>
                                                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo $r_atten['L'];
            else: echo '0';
            endif; ?></td>
                                                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><? if (!empty($r_atten) && array_key_exists('H', $r_atten)): echo $r_atten['H'];
            else: echo '0';
            endif; ?></td>
                                                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= ($working_days - $t_attend) ?></td>
                                                                            <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $working_days ?></td>
                                                                        </tr>
        <? endforeach; ?>									
                                                                </tbody>

    <? endif; ?>
                                                        </table>																	

<? endif; ?>

                                                    <div class="form-actions clearfix">
                                                        <a href='<?= make_admin_url('attendance', 'list', 'list&type=session&select=' . $select) ?>' class="btn red" ><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a>
                                                    </div>																	
                                                    <div class="clearfix"></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>											
                                </div>
                                <!-- Third steps End -->
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

    <script type="text/javascript">
        $(".session_filter").change(function () {
            var session_id = $("#session_id").val();

            if (session_id.length > 0) {
                var id = session_id;
                $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

                var dataString = 'id=' + id;
                $.ajax({
                    type: "POST",
                    url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section&temp=attendance'); ?>",
                    data: dataString,
                    success: function (data, textStatus) {
                        $("#dynamic_model").html(data);
                    }
                });
            }
            else {
                return false;
            }
        });

    </script> 

    <script type="text/javascript">
        $(".session_filter_for_single").live("change", function () {
            var session_id = $("#session_id").val();
            var ct_s = $("#ct_s").val();

            if (session_id.length > 0) {
                var id = session_id;
                $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

                var dataString = 'id=' + id + '&ct_s=' + ct_s;

                $.ajax({
                    type: "POST",
                    url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_without_st', 'ajax_section_without_st&temp=attendance'); ?>",
                    data: dataString,
                    success: function (data, textStatus) {
                        $("#dynamic_model").html(data);
                    }
                });
            }
            else {
                return false;
            }
        });
    </script> 
    <script type="text/javascript">
        $("#GetStudents").live("click", function () {
            var session_id = $("#session_id").val();
            var ct_s = $("#ct_s").val();

            if (session_id.length > 0) {
                var id = session_id;
                $("#show_students").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

                var dataString = 'id=' + id + '&ct_s=' + ct_s;

                $.ajax({
                    type: "POST",
                    url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_with_st', 'ajax_section_with_st&temp=attendance'); ?>",
                    data: dataString,
                    success: function (data, textStatus) {
                        $("#show_students").html(data);
                    }
                });
            }
            else {
                return false;
            }
        });
    </script> 
