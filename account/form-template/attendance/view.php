<!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Class Attendance
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        All Sessions
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
							<div class="alert alert-block alert-success">
								<div class="row-fluid">
									<form action="<?php echo make_admin_url('attendance', 'list', 'list')?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
									<input type='hidden' name='Page' value='attendance'/>
									<input type='hidden' name='action' value='list'/>
									<input type='hidden' name='section' value='list'/>
 																	
										<div class="span3">								
										<div class="control-group">
											<label class="control-label">Current Session</label>
											<div class="controls">
												<select class="select2_category session_filter span11" data-placeholder="Select Session Students" name='id' id='session_id'>
													<option value="">Select Session</option>
													<?php $session_sr=1;while($session=$QuerySession->GetObjectFromRecord()): ?>
															<option value='<?=$session->id?>' <? if($session->id==$id){ echo 'selected'; }?> ><?=ucfirst($session->title)?></option>
													<? $session_sr++; endwhile;?>
												</select>
											</div>
										</div>
										</div>
									<? #if Session has the sections
									if($total_section>1): ?>
										<div class="span6 ">
										<div class="control-group" >
											<label class="control-label">Select Section </label>
											<div class="controls">
												<select class="select2_category session_filter span4" data-placeholder="Select Session Students" name='ct_sect'>
													<option value="">Select Section</option>
													<?php $session_sr=1;while($sec=$QuerySec->GetObjectFromRecord()): ?>
															<option value='<?=$sec->section?>' <? if($sec->section==$ct_sect){ echo 'selected'; }?> ><?=ucfirst($sec->section)?></option>
													<? $session_sr++; endwhile;?>
												</select>
											</div>
										</div>
										</div>
									<? endif; ?>	
									</form>	
								</div>
							</div>	
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
             
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-calendar"></i><? if(is_object($object)): echo $object->title; else: echo "Class"; endif;?> Attendance</div>
								<div class="tools"><strong><?=$start?></strong> To <strong><?=$to_date?></strong>
									
								</div>
							</div>
							<div class="portlet-body">
								<div class="clearfix">
								</div>
								<table class="table table-striped table-bordered table-hover attendance_table" id="sample_2">
									<thead>
										<tr>
											<th class="hidden-480">Reg ID</th>
											<th class="hidden-480" style='width:20%;'>Name</th>
											<? while((strtotime($start))<=(strtotime($to_date))):?>
												 <th style='text-align:center;' class="hidden-480 sorting_disabled"><?=date('d M',strtotime($start))?></th>
											<? 
												$start = date("d M Y",strtotime("+1 days",strtotime($start)));
												endwhile;?>
										</tr>
									</thead>
									<? if(!empty($records)):?>
									<tbody>
									<?php $sr=1;foreach($records as $sk=>$stu): $checked='';?>
										<tr class="odd gradeX">
											<td class='main_title'><?=$stu->reg_id?></td>
											<td class='main_title'><?=ucfirst($stu->first_name)." ".$stu->last_name?></td>
											<td class="hidden-480 sorting_disabled" style='text-align:center;'><input type='text' s_id='<?=$id?>' st_id='<?=$stu->id?>' a_date='<?=date("d",strtotime("-3 days",strtotime(date('Y-m-d'))));?>' class='m-wrap span6 attendance' id='attendance'/></td>
											<td class="hidden-480 sorting_disabled" style='text-align:center;'><input type='text' s_id='<?=$id?>' st_id='<?=$stu->id?>' a_date='<?=date("d",strtotime("-2 days",strtotime(date('Y-m-d'))));?>' class='m-wrap span6 attendance' id='attendance'/></td>
											<td class="hidden-480 sorting_disabled" style='text-align:center;'><input type='text' s_id='<?=$id?>' st_id='<?=$stu->id?>' a_date='<?=date("d",strtotime("-1 days",strtotime(date('Y-m-d'))));?>' class='m-wrap span6 attendance' id='attendance'/></td>
											<td class="hidden-480 sorting_disabled" style='text-align:center;'><input type='text' s_id='<?=$id?>' st_id='<?=$stu->id?>' a_date='<?=date("d");?>' class='m-wrap span6 attendance' id='attendance'/></td>
											<td class="hidden-480 sorting_disabled" style='text-align:center;'><input type='text' s_id='<?=$id?>' st_id='<?=$stu->id?>' a_date='<?=date("d",strtotime("+1 days",strtotime(date('Y-m-d'))));?>' class='m-wrap span6' id='attendance' disabled=''/></td>
											<td class="hidden-480 sorting_disabled" style='text-align:center;'><input type='text' s_id='<?=$id?>' st_id='<?=$stu->id?>' a_date='<?=date("d",strtotime("+2 days",strtotime(date('Y-m-d'))));?>' class='m-wrap span6' id='attendance' disabled=''/></td>
											<td class="hidden-480 sorting_disabled" style='text-align:center;'><input type='text' s_id='<?=$id?>' st_id='<?=$stu->id?>' a_date='<?=date("d",strtotime("+3 days",strtotime(date('Y-m-d'))));?>' class='m-wrap span6' id='attendance' disabled=''/></td>
										</tr>
									<? endforeach;?>									
									</tbody>
								
								<? endif;?>
								</table>
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    
