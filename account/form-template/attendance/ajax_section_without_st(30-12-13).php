<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');

if($_POST):
isset($_POST['id'])?$id=$_POST['id']:$id='';
isset($_POST['ct_s'])?$ct_s=$_POST['ct_s']:$ct_s='';
		
		
		/* Get session Pages*/
		 $QuerySec=new studentSession();
         $QuerySec->listAllSessionSection($id);
		 
			#get Session Info
			$QueryS=new session();
			$session=$QueryS->getRecord($id);			 
		 ?>
			<div class="row-fluid">	
				
				<div class="control-group" >
					<label class="control-label">Select Section </label>
					<div class="controls">
					<? if($QuerySec->GetNumRows()>0): ?>	
						<select class="select2_category span5" data-placeholder="Select Session Students" name='ct_sect' id='ct_s'>
							<?php $session_sc=1;while($sec=$QuerySec->GetObjectFromRecord()): ?>
									<option value='<?=$sec->section?>'><?=ucfirst($sec->section)?></option>
							<? $session_sc++; endwhile;?>
						</select>						
						<select name='date' class='span4'>
						 <?=getMonths($session->start_date,$session->end_date);?>
						</select>
						<div class='span2 btn blue' id='GetStudents' style='float: right; margin-left: 0px; margin-right: 30px;'>Go</div>
					<? else:?>
							<input type='text' value='Sorry, No Section Found.' disabled />							
					<? endif;?>							
						
					</div>
				
				</div>	
			</div>	
			 
			<div id='show_students'></div>

<?		
endif;
?>