<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Types
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('leave_type', 'list', 'list'); ?>">List Types</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add Type
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data" id="validation">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Add New Type</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="control-group">
                            <label class="control-label" for="course_name">Type Title<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="type"  value="" id="type" class="span6 m-wrap validate[required]"  />

                            </div>
                        </div>        
                        <div class="control-group">
                            <label class="control-label" for="status" style="margin-top: -4px;">Make Active</label> 
                            <div class="controls">
                                <input type="checkbox" value="1" name="status" checked  />
                            </div>
                        </div>

                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('leave_types', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>