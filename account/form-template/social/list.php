
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Social Media Setting
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>Social Media Setting</li>
									
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
				</div>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-rss"></i>Social Media Setting</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body form">  
							
							<!-- Update Case -->	
								<form class="form-horizontal" action="<?php echo make_admin_url('social', 'list', 'list&id='.$object->id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          					<br/>					
											<div class="control-group">
                                                    <label class="control-label" for="facebook">Facebook Link</label>
                                                    <div class="controls">
                                                       <input type="text" name="facebook"  value="<?=$school->facebook?>" id="facebook" class="span6 m-wrap" />
													</div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="twitter">Twitter Link</label>
                                                    <div class="controls">
                                                       <input type="text" name="twitter"  value="<?=$school->twitter?>" id="twitter" class="span6 m-wrap" />
													</div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="google">Google Link</label>
                                                    <div class="controls">
                                                       <input type="text" name="google" value="<?=$school->google?>" id="google" class="span6 m-wrap" />
													</div>
                                            </div>											
										    <div class="form-actions">
													<input type="hidden" name="hidden" value="1" tabindex="7" /> 
													 <input type="hidden" name="is_active" value="1" tabindex="7" /> 
													 <input type="hidden" name="id" value="<?=$school->id?>" tabindex="7" /> 
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('social', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                             </div>
		
							</div>
							
						</div>
						
                        </form>	                      
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

