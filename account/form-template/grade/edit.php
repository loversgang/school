
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Grading System
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('grade', 'list', 'list');?>">List Grades</a>
                                         <i class="icon-angle-right"></i>                                       
                                    </li>
                                    <li class="last">
                                        Edit Grade
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
						<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
				</div>	            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('grade', 'update', 'update&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Edit Grade</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">
                                          <div class="control-group">
                                                    <label class="control-label" for="Title">Title<span class="required">*</span></label>
                                                     <div class="controls">
                                                       <input type="text" name="title"  value="<?=$object->title?>" id="Title" class="span5 m-wrap validate[required]" />
													 </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="minimum">Minimum Percentage (%)<span class="required">*</span></label>
                                                     <div class="controls">
                                                       <input type="text" name="minimum"  value="<?=$object->minimum?>" id="minimum" class="span5 m-wrap validate[required,custom[onlyNumberSp]]" />
													 </div>
                                            </div> 		
                                            <div class="control-group">
                                                    <label class="control-label" for="maximum">Maximum Percentage (%)<span class="required">*</span></label>
                                                     <div class="controls">
                                                       <input type="text" name="maximum"  value="<?=$object->maximum?>" id="maximum" class="span5 m-wrap validate[required,custom[onlyNumberSp]]" />
													 </div>
                                            </div>
                                    
                                            <div class="form-actions">
											<input type='hidden' name='school_id' value='<?=$school->id?>'/>
													 <input type='hidden' name='id' value='<?=$object->id?>'/>
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('grade', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
                                     
                              </div> 

                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

