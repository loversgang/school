<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Expenses
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    Add Expense
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-truck"></i>Manage Expenses</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form method="post" class="form-horizontal" id="validation" enctype="multipart/form-data">
                        <h4 class="form-section hedding_inner">Add Expense</h4>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label span4" for="name">Expense Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="name" value="" id="name" class="span8 m-wrap validate[required]"/>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label span4" for="date">Expense Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="date" value="" id="date" class="span8 m-wrap validate[required] new_format"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label span4" for="name">Select Vehicle<span class="required">*</span></label>
                                    <div class="controls">
                                        <select name="vehicle_id" class="m-wrap span8">
                                            <?php foreach ($vehicles as $vehicle) { ?>
                                                <option value="<?= $vehicle->id; ?>"><?= $vehicle->model; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label span4" for="amount">Expense Amount<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="number" step="any" name="amount" value="" id="amount" class="span8 m-wrap validate[required]"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid" id="more_docs">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label span4" for="title">Doc Title</label>
                                    <div class="controls">
                                        <input type="text" name="title[]" value="" id="title" class="span8 m-wrap"/>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label span4" for="doc">Attachment</label>
                                    <div class="controls">
                                        <input type="file" name="doc[]" value="" id="doc" class="span8 m-wrap"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label span8" for="doc"></label>
                            <div class="controls">
                                <button type="button" class="btn green span4" id="add_more_fields">Add More</button>
                            </div>
                        </div>
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('vehicles', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->
<script>
    $(document).on('click', '#add_more_fields', function () {
        $('#more_docs').append('<div class="row-fluid"><div class="span6"><div class="control-group"><label class="control-label span4" for="title">Doc Title</label><div class="controls"><input type="text" name="title[]" value="" id="title" class="span8 m-wrap"/></div></div></div><div class="span6"><div class="control-group"><label class="control-label span4" for="doc">Attachment</label><div class="controls"><input type="file" name="doc[]" value="" id="doc" class="span8 m-wrap"/><button type="button" id="remove_field" class="btn red"><i class="icon-trash"></i></button></div></div></div></div>');
    });
    $(document).on('click', '#remove_field', function () {
        $(this).closest('.row-fluid').hide(400, function () {
            $(this).remove();
        });
    });
</script>