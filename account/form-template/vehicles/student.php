

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Vehicle Students
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 

                                    <li class="last">
                                        Vehicle Students
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-left">
				<form class="form-horizontal" action="<?php echo make_admin_url('vehicles', 'student', 'student')?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
				<input type='hidden' name='Page' value='vehicles'/>
				<input type='hidden' name='action' value='student'/>
				<input type='hidden' name='section' value='student'/>
				<input type='hidden' name='ct_veh' value='<?=$ct_veh?>'/>
				
				<div class="control-group alert alert-success" style='margin-left:0px;padding-right:0px;'>
					<div class='span3'>
					<label class="control-label" style='float: none; text-align: left;'>Select Session</label>
					<select class="select2_category session_filter" data-placeholder="Select Session Students" name='id' id='session_id'>
							<option value="">Select Session</option>
							<?php $session_sr=1;while($session=$QuerySession->GetObjectFromRecord()): ?>
									<option value='<?=$session->id?>' <? if(is_object($select_session) && $session->id==$select_session->id){ echo 'selected'; }?> ><?=ucfirst($session->title)?></option>
							<? $session_sr++; endwhile;?>
					</select>
					</div>
					<div class='span3' id='ajex_section'>
					<label class="control-label" style='float: none; text-align: left;'>Select Session</label>
					<select class="select2_category span2" data-placeholder="Select Session Students" name='ct_sec'>
							<option value="">Select Section</option>
							
							<?php $sect_sr=1;while($sect=$QuerySec->GetObjectFromRecord()): ?>
									<option value='<?=$sect->section?>' <? if(!empty($ct_sec) && $sect->section==$ct_sec){ echo 'selected'; }?>  ><?=ucfirst($sect->section)?></option>
							<? $sect_sr++; endwhile;?>
					</select>	
					<input type='submit' name='go' value='Go' class='btn blue'/>
					</div>
				</div>
				</form>										
				</div>	
				<div class="tiles pull-right">
							<div class="tile bg-purple <?php echo ($section=='student')?'selected':''?>">
							<a href="<?php echo make_admin_url('vehicles', 'view', 'view&id='.$ct_veh);?>">
								<div class="corner"></div>
								<div class="tile-body"><i class="icon-arrow-left"></i></div>
								<div class="tile-object"><div class="name">Vehicle Students</div></div>
							</a> 
							</div>				
						<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<form class="form-horizontal" action="<?php echo make_admin_url('vehicles', 'student', 'student&id='.$id.'&ct_veh='.$ct_veh)?>" method="POST" enctype="multipart/form-data" id="validation">
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i><? if(is_object($object)): echo $object->title; else: echo "All"; endif;?> > Students</div>								
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
											<th class="hidden-480">Reg ID</th>
											<th class="hidden-480">Name</th>
											<th class="hidden-480">Father Name</th>
											<th class="hidden-480">Sex</th>											
											<th class="hidden-480">Session</th>
											<th class="hidden-480">Section</th>
											<th style='width:18%;' class="hidden-480 sorting_disabled"></th>
										</tr>
									</thead>
									<? if(!empty($AllStu)):?>
									<tbody>
									<?php $sr=1;foreach($AllStu as $sk=>$stu): $checked='';?>
										<tr class="odd gradeX" <? if(in_array($stu->id,$veh_st)): echo 'id="added"'; $checked='checked'; endif;?> >
											<td><input type="checkbox" class="checkboxes" value="1" name="multiopt[<?php echo $stu->id?>]" id="multiopt[<?php echo $stu->id?>]" <?=$checked?>/></td>
											<td><?=$stu->reg_id?></td>
											<td><?=$stu->first_name." ".$stu->last_name?></td>
											<td class="hidden-480"><?=$stu->father_name?></td>
											<td class="hidden-480"><?=$stu->sex?></td>
											<td class="center hidden-480"><?=$stu->session_name?></td>
											<td class="center hidden-480"><?=$stu->section?></td>
											<td class="hidden-480 sorting_disabled">
												<? if(in_array($stu->id,$veh_st)):?>
													<span class="btn green mini" >Added</span>
												<? endif;?>
											</td>
										<input type='hidden' name="ct_sec" value='<?=$ct_sec?>'/>
										<input type='hidden' name="session_id" value='<?=$id?>'/>										
										</tr>
									<? endforeach;?>									
									</tbody>
									<? endif;?>
								</table>
								<? if(!empty($AllStu)):?>
									<input type='submit' name='multi_select' value='Add Selected Students' class='btn green'/>
								<? endif;?>
							</div>
						</div>
						</form>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    
<script type="text/javascript">
	$(".session_filter").change(function(){	   
			var session_id=$("#session_id").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section','ajax_section&temp=vehicles');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#ajex_section").html(data);
						}				
						});
			}
			else{
				return false;
			}
	});
</script>


