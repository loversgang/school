<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Routes
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    Edit Route
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-truck"></i>Manage Routes</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form method="post" class="form-horizontal" id="validation">
                        <h4 class="form-section hedding_inner">Edit Route Point</h4>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label span4" for="route_from">Route From<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="route_from" value="<?php echo $route->route_from; ?>" id="route_from" class="span8 m-wrap validate[required]"/>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label span4" for="route_to">Route To<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="route_to" value="<?php echo $route->route_to; ?>" id="route_to" class="span8 m-wrap validate[required]"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="form-section hedding_inner">Route Stoppages</h4>
                        <?php if (!$stoppages) { ?>
                            <div class="row-fluid stoppage_fields">
                                <div class="span3">
                                    <div class="control-group">
                                        <label class="control-label span6" for="stoppage">Route Stoppage<span class="required">*</span></label>
                                        <div class="controls span6">
                                            <input type="text" name="stoppage[]" value="" id="stoppage" class="span12 m-wrap validate[required]"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="control-group">
                                        <label class="control-label span6" for="pick_time">Pick Time<span class="required">*</span></label>
                                        <div class="controls span6">
                                            <input type="text" name="pick_time[]" value="" id="pick_time" class="m-wrap span12 m-ctrl-small timepicker3 add-on validate[required]">
                                        </div>
                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="control-group">
                                        <label class="control-label span6" for="drop_time">Drop Time<span class="required">*</span></label>
                                        <div class="controls span6">
                                            <input type="text" name="drop_time[]" value="" id="drop_time" class="m-wrap span12 m-ctrl-small timepicker3 add-on validate[required]">
                                        </div>
                                    </div>
                                </div>
                                <div class="span3">
                                    <div class="control-group">
                                        <label class="control-label span6" for="amount">Amount<span class="required">*</span></label>
                                        <div class="controls span6">
                                            <input type="number" name="amount[]" value="" id="amount" class="span12 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="stoppage_fields">
                                <?php foreach ($stoppages as $stoppage) { ?>
                                    <div class="row-fluid" id="stoppage_single_<?= $stoppage->id ?>">
                                        <div class="span3">
                                            <div class="control-group">
                                                <label class="control-label span6" for="stoppage">Route Stoppage<span class="required">*</span></label>
                                                <div class="controls span6">
                                                    <input type="text" name="stoppage_old[]" value="<?= $stoppage->stoppage ?>" id="stoppage" class="span12 m-wrap validate[required]"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <div class="control-group">
                                                <label class="control-label span6" for="pick_time">Pick Time<span class="required">*</span></label>
                                                <div class="controls span6">
                                                    <input type="text" name="pick_time_old[]" value="<?= $stoppage->pick_time ?>" id="pick_time" class="m-wrap span12 m-ctrl-small timepicker3 add-on validate[required]">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span3">
                                            <div class="control-group">
                                                <label class="control-label span6" for="drop_time">Drop Time<span class="required">*</span></label>
                                                <div class="controls span6">
                                                    <input type="text" name="drop_time_old[]" value="<?= $stoppage->drop_time ?>" id="drop_time" class="m-wrap span12 m-ctrl-small timepicker3 add-on validate[required]">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span2">
                                            <div class="control-group">
                                                <label class="control-label span6" for="amount">Amount<span class="required">*</span></label>
                                                <div class="controls span6">
                                                    <input type="number" name="amount_old[]" value="<?= $stoppage->amount ?>" id="amount" class="span12 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                    <input type="hidden" name="stoppage_id[]" value="<?= $stoppage->id ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span1">
                                            <button type="button" data-id="<?= $stoppage->id ?>" id="delete_stoppage" class="btn red">X</button>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div class="row-fluid">
                            <div class="control-group">
                                <input type="button" class="span12 m-wrap btn red" id="add_more_stoppage_fields" value="Add More"/>
                            </div>
                        </div>
                        <div class="form-actions">
                            <input type="hidden" name="id" value="<?= $id; ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('vehicles', 'route', 'route'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->
<script>
    $(document).on('click', '#add_more_stoppage_fields', function () {
        $('.stoppage_fields').append('<div class="row-fluid"><div class="span3"><div class="control-group"><label class="control-label span6" for="stoppage">Route Stoppage<span class="required">*</span></label><div class="controls span6"><input type="text" name="stoppage[]" value="" id="stoppage" class="span12 m-wrap validate[required]"/></div></div></div><div class="span3"><div class="control-group"><label class="control-label span6" for="pick_time">Pick Time<span class="required">*</span></label><div class="controls span6"><input type="text" name="pick_time[]" value="" id="pick_time" class="m-wrap span12 m-ctrl-small timepicker3 add-on validate[required]"></div></div></div><div class="span3"><div class="control-group"><label class="control-label span6" for="drop_time">Drop Time<span class="required">*</span></label><div class="controls span6"><input type="text" name="drop_time[]" value="" id="drop_time" class="m-wrap span12 m-ctrl-small timepicker3 add-on validate[required]"></div></div></div><div class="span2"><div class="control-group"><label class="control-label span6" for="amount">Amount<span class="required">*</span></label><div class="controls span6"><input type="number" name="amount[]" value="" id="amount" class="span12 m-wrap validate[required,custom[onlyNumberSp]]"/></div></div></div><div class="span1"><button type="button" id="remove_field" class="btn red">X</button></div></div>');
        FormComponents.init();
    });
    $(document).on('click', '#remove_field', function () {
        $($(this).parents('.row-fluid')[0]).hide(400, function () {
            $(this).remove();
        });
    });
    $(document).on('click', '#delete_stoppage', function () {
        var stoppage_id = $(this).attr('data-id');
        if (confirm("Are You Sure? You want to delete this document?")) {
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=vehicles'); ?>", {act: 'delete_stoppage', stoppage_id: stoppage_id}, function (data) {
                if (data === 'success') {
                    $('#stoppage_single_' + stoppage_id).hide(400, function () {
                        $(this).remove();
                    });
                    toastr.success('Stoppage Deleted Successfully!', 'Success');
                }
            });
        }
    });
</script>