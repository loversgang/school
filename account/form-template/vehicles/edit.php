
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Vehicles
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('vehicles', 'list', 'list'); ?>">List Vehicles</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Edit Vehicle
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->


    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('vehicles', 'update', 'update') ?>" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-truck"></i>Edit Vehicle</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">      
                        <!-- Vehicle Detail  -->
                        <h4 class="form-section hedding_inner">Vehicle Information</h4>
                        <div class="row-fluid">
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="make">Company<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="make" value="<?= $object->make ?>" id="make" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="year">Modal Year<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="year" value="<?= $object->year ?>" id="year" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="color">Color<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="color" value="<?= $object->color ?>" id="color" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="type">Vehicle Type<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="type" value="<?= $object->type ?>" id="type" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="possible_rounds">Total Possible Rounds<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="number" name='possible_rounds' id='possible_rounds' value="<?= $object->possible_rounds ?>" class="m-wrap span10 validate[required]" > 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="permit">Permit Number<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name='permit' value="<?= $object->permit ?>" id='permit' class="m-wrap span10 validate[required]" > 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="insurance">Insurance Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name='insurance' value='<?= $object->insurance ?>' id='insurance' class="m-wrap span10 validate[required] new_format" > 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Vendor Or Owned<span class="required">*</span></label>
                                    <? if (empty($object->vendor)): ?>	
                                        <div class="controls">
                                            <label class="radio">
                                                <div class="radio owned"><span class=""><input checked="" type="radio" value="option1" name="optionsRadios1"></span></div>
                                                Owned
                                            </label>
                                            <label class="radio vendor">
                                                <div class="radio"><span><input type="radio" value="option2" name="optionsRadios1"></span></div>
                                                Vendor
                                            </label> 
                                            </label>  
                                        </div>
                                        <div class="controls" id='vendor_div'>
                                            <input type="text" name="vendor" value="" id="vendor" class="span10 m-wrap" readonly />
                                        </div>                                                   
                                    <? else: ?>
                                        <div class="controls">
                                            <label class="radio">
                                                <div class="radio owned"><span class=""><input type="radio" value="option1" name="optionsRadios1"></span></div>
                                                Owned
                                            </label>
                                            <label class="radio vendor">
                                                <div class="radio"><span><input checked="" type="radio" value="option2" name="optionsRadios1"></span></div>
                                                Vendor
                                            </label> 
                                            </label>  
                                        </div>
                                        <div class="controls" id='vendor_div'>
                                            <input type="text" name="vendor" value="<?= $object->vendor ?>" id="vendor" class="span10 m-wrap validate[required]" />
                                        </div>						
                                    <? endif; ?>							
                                </div>
                            </div>	
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="model">Model<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="model" value="<?= $object->model ?>" id="model" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="purchase_date">Purchase Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="purchase_date" value="<?= $object->purchase_date ?>" id="purchase_date" class="span10 m-wrap new_format validate[required]" />
                                    </div>
                                </div> 	  
                                <div class="control-group">
                                    <label class="control-label" for="vehicle_number">Vehicle Number<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="vehicle_number" value="<?= $object->vehicle_number ?>" id="vehicle_number" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>  
                                <div class="control-group">
                                    <label class="control-label" for="seating_capacity">Seating Capacity<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="seating_capacity" value="<?= $object->seating_capacity ?>" id="seating_capacity" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="max_seating_capacity">Maximum Seating Capacity<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="number" name='max_seating_capacity' value='<?= $object->max_seating_capacity ?>' id='max_seating_capacity' class="m-wrap span10 validate[required]" > 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="permit_expiry">Permit Expiry Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name='permit_expiry' id='permit_expiry' value="<?= $object->permit_expiry ?>" class="m-wrap span10 validate[required] new_format" > 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="insurance_expiry">Insurance Expiry<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name='insurance_expiry' id='insurance_expiry' value='<?= $object->insurance_expiry ?>' class="m-wrap span10 validate[required] new_format" > 
                                    </div>
                                </div>
                                <div class="span12">
                                    <h4 class="form-section hedding_inner">Doc. Attachment</h4>
                                    <div id="appendable_fields">
                                        <?php if (!$vehicle_docs) { ?>
                                            <div class="row-fluid">
                                                <div class="span4">
                                                    <div class="control-group">
                                                        <label class="control-label span4" for="title">Title</label>
                                                        <div class="controls span8">
                                                            <input type="text" name='title[]' id='title' value='' class="m-wrap span10" > 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span8">
                                                    <div class="control-group">
                                                        <label class="control-label span1" for="doc"></label>
                                                        <div class="controls span11">
                                                            <input type="file" name='doc[]' id='doc' value='' class="m-wrap span10" > 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <?php foreach ($vehicle_docs as $doc) { ?>
                                                <div class="row-fluid" id="doc_single_<?= $doc->id ?>">
                                                    <div class="span4">
                                                        <div class="control-group">
                                                            <label class="control-label span4" for="title">Title</label>
                                                            <div class="controls span8">
                                                                <input type="text" name='title_old[]' id='title_old' value='<?= $doc->title ?>' class="m-wrap span10" > 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span8">
                                                        <div class="control-group">
                                                            <label class="control-label span1" for="doc"></label>
                                                            <div class="controls span11">
                                                                <input type="file" name='doc_old[]' id='doc' value='' class="m-wrap span9" ><button id="delete_doc" class="btn red" type="button" data-id="<?= $doc->id; ?>"><i class="icon-trash"></i></button>
                                                                <input type="hidden" name='doc_id[]' id='doc_id' value='<?= $doc->id ?>' class="m-wrap span12">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label span9" for="doc"></label>
                                    <div class="controls span3">
                                        <input type="button" class="m-wrap btn red" id="add_more_doc_fields" value="Add More">
                                    </div>
                                </div>
                            </div>	
                        </div>			

                        <?
                        if (!empty($object->driver_id)):
                            $driv = get_object('staff', $object->driver_id);
                        endif;
                        if (!empty($object->conductor_id)):
                            $cond = get_object('staff', $object->conductor_id);
                        endif;
                        ?>
                        <!-- Docs Detail -->
                        <div id="rounds_html">
                            <?php
                            $routes_data = vehicle_route::getVehicleRoutes($school->id, $object->round_routes);
                            $k = 1;
                            if ($routes_data != '') {
                                foreach ($routes_data as $route_data) {
                                    ?>
                                    <div class="row-fluid" id="round_route">
                                        <div class="span6">
                                            <div class="control-group">
                                                <label class="control-label" for="round_route">Select Round</label>
                                                <div class="controls">
                                                    <input type="text" value="Round <?= $k++; ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="control-group">
                                                <label class="control-label" for="round_route">Select Route</label>
                                                <div class="controls">
                                                    <select name="round_route[]" id="round_route" class="m-wrap span10">
                                                        <?php foreach ($routes as $route) { ?>
                                                            <option value="<?= $route->id; ?>" <?= $route->id == $route_data->id ? "selected" : "" ?>><?= $route->route_from . " to " . $route->route_to ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <!-- Driver Detail  -->
                        <h4 class="form-section hedding_inner">Driver Information</h4>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="teacher_cat">Select Staff Category<span class="required">*</span></label>
                                    <div class="controls">
                                        <select class="teacher_cat"   class="span10 m-wrap ">
                                            <option value=''>Select Category</option>
                                            <?php
                                            $sr = 1;
                                            while ($cat = $QueryStaff->GetObjectFromRecord()):
                                                ?>
                                                <option value='<?= $cat->id ?>' <?
                                                if (is_object($driv) && $driv->staff_category == $cat->id) {
                                                    echo 'selected';
                                                }
                                                ?>><?= $cat->name ?></option>
                                                        <?
                                                        $sr++;
                                                    endwhile;
                                                    ?>
                                        </select>                      
                                    </div>
                                </div>	
                            </div>	
                            <div class="span6 ">	
                                <div class="control-group" id='dyanamic_teacher'>
                                    <label class="control-label" >Driver Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" value='<?
                                        if (is_object($driv)) {
                                            echo $driv->title . ' ' . $driv->first_name . ' ' . $driv->last_name;
                                        }
                                        ?>' class="m-wrap span8" readonly> 
                                        <input type="hidden" value='<?= $object->driver_id ?>'>														
                                    </div>
                                </div>		
                            </div>
                        </div>										
                        <!-- Conductor Detail  -->
                        <h4 class="form-section hedding_inner">Conductor Information</h4>										
                        <div class="row-fluid">
                            <div class="span6 ">										
                                <div class="control-group">
                                    <label class="control-label" for="conductor_cat">Select Staff Category<span class="required">*</span></label>
                                    <div class="controls">

                                        <select class="conductor_cat"  class="span10 m-wrap ">
                                            <option value=''>Select Category</option>
                                            <?php
                                            $sr = 1;
                                            while ($cat1 = $QueryStaffCond->GetObjectFromRecord()):
                                                ?>
                                                <option value='<?= $cat1->id ?>' <?
                                                if (is_object($cond) && $cond->staff_category == $cat1->id) {
                                                    echo 'selected';
                                                }
                                                ?>><?= $cat1->name ?></option>
                                                        <?
                                                        $sr++;
                                                    endwhile;
                                                    ?>
                                        </select>                      
                                    </div>
                                </div>
                            </div>
                            <div class="span6 ">	
                                <div class="control-group" id='dyanamic_conductor'>
                                    <label class="control-label" for="conductor_id">Conductor Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text"  id='conductor_id' value='<?
                                        if (is_object($cond)) {
                                            echo $cond->title . ' ' . $cond->first_name . ' ' . $cond->last_name;
                                        }
                                        ?>' class="m-wrap span8 validate[required]" readonly> 
                                        <input type="hidden" name='conductor_id' value='<?= $object->conductor_id ?>' >															
                                    </div>
                                </div>	

                            </div><br/>	

                            <div class="control-group">
                                <label class="control-label" for="meta_keyword">Make Active</label>
                                <div class="controls">
                                    <input type="checkbox" name="is_active" id="meta_keyword" value="1" <?= ($object->is_active == '1') ? 'checked' : ''; ?>>
                                </div>
                            </div> 

                            <div class="form-actions">
                                <input type="hidden" name="id" value="<?= $object->id ?>" tabindex="7" />
                                <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                                <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                <a href="<?php echo make_admin_url('vehicles', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
        <div class="clearfix"></div>

    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    
<script type="text/javascript">
    jQuery(document).ready(function () {
        /* Add New vendor */
        $(".vendor").live("click", function () {
            $("div#vendor_div").replaceWith("<div id='vendor_div' class='controls'><input type='text' name='vendor' value='' id='vendor' class='span10 m-wrap validate[required]'/></div>");
        });
        /* Add New vendor */
        $(".owned").live("click", function () {
            $("div#vendor_div").replaceWith("<div id='vendor_div' class='controls'><input type='text' name='vendor' readonly='' value='' id='vendor' class='span10 m-wrap'/></div>");
        });
        $(".teacher_cat").change(function () {
            var cat_id = $(this).val();
            var id = cat_id;
            $("#dyanamic_teacher").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            var dataString = 'id=' + id;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_driver&temp=vehicles'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#dyanamic_teacher").html(data);
                }
            });
        });
        $(".conductor_cat").change(function () {
            var cat_id = $(this).val();
            var id = cat_id;
            $("#dyanamic_conductor").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            var dataString = 'id=' + id;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_conductor', 'ajax_conductor&temp=vehicles'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#dyanamic_conductor").html(data);
                }
            });
        });
    });
    $(document).on('change', '#possible_rounds', function () {
        var rounds = $(this).val();
        if (rounds) {
            var rounds_html = '';
            for (var i = 1; i <= rounds; i++) {
                rounds_html += '<div class="row-fluid" id="round_route"><div class="span6"><div class="control-group"><label class="control-label" for="round_route">Select Round</label><div class="controls"><input type="text" value="Round ' + i + '" disabled></div></div></div><div class="span6"><div class="control-group"><label class="control-label">Select Route</label><div class="controls"><select name="round_route[]" id="round_route" class="m-wrap span10">';
<?php foreach ($routes as $route) { ?>
                    rounds_html += '<option value="<?= $route->id; ?>"><?= $route->route_from . " to " . $route->route_to ?></option>';
<?php } ?>
                rounds_html += '</select></div></div></div>';
            }
            $('#rounds_html').html(rounds_html);
        } else {
            $('#rounds_html').html('');
        }
    });
    $(document).on('keyup', '#possible_rounds', function () {
        $(this).change();
    });
    $(document).on('click', '#add_more_doc_fields', function () {
        $('#appendable_fields').append('<div class="row-fluid"><div class="span4"><div class="control-group"><label class="control-label span4" for="title">Title</label><div class="controls span8"><input type="text" name="title[]" id="title" value="" class="m-wrap span10"></div></div></div><div class="span8"><div class="control-group"><label class="control-label span1" for="doc"></label><div class="controls span11"><input type="file" name="doc[]" id="doc" value="" class="m-wrap span9"><button type="button" id="remove_field" class="btn red"><i class="icon-trash"></i></button></div></div></div></div>');
    });
    $(document).on('click', '#remove_field', function () {
        $($(this).parents('.row-fluid')[0]).hide(400, function () {
            $(this).remove();
        });
    });
    $(document).on('click', '#delete_doc', function () {
        var doc_id = $(this).attr('data-id');
        if (confirm("Are You Sure? You want to delete this document?")) {
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=vehicles'); ?>", {act: 'delete_doc', doc_id: doc_id}, function (data) {
                if (data === 'success') {
                    $('#doc_single_' + doc_id).hide(400, function () {
                        $(this).remove();
                    });
                    toastr.success('Document Deleted Successfully!', 'Success');
                }
            });
        }
    });
</script>	
