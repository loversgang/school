<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Vehicle Expenses
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    List Expenses
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-truck"></i>List Expenses > <?= $vehicle->model; ?></div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <table class="table table-bordered" id="sample_2">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $k = 1;
                            foreach ($expenses as $expense) {
                                ?>
                                <tr>
                                    <td><?php echo $k++; ?></td>
                                    <td><?php echo $expense->name; ?></td>
                                    <td><?php echo $expense->date; ?></td>
                                    <td><?php echo CURRENCY_SYMBOL . " " . $expense->amount; ?></td>
                                    <td><a href="<?php echo make_admin_url('vehicles', 'update_expense', 'update_expense', 'id=' . $expense->id); ?>" class="btn blue mini"><i class="icon-pencil"></i></a> <a href="<?php echo make_admin_url('vehicles', 'delete_expense', 'delete_expense', 'id=' . $expense->id . '&vehicle_id=' . $vehicle_id); ?>" class="btn red mini" onclick="return confirm('Are you sure? You are deleting this page.');"><i class="icon-remove"></i></a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->  
