<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Routes
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    List Routes
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-truck"></i>List Routes</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <table class="table table-bordered" id="sample_2">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Route From</th>
                                <th>Route To</th>
                                <th>Stoppage List</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $k = 1;
                            foreach ($routes as $route) {
                                ?>
                                <tr>
                                    <td><?php echo $k++; ?></td>
                                    <td><?php echo $route->route_from; ?></td>
                                    <td><?php echo $route->route_to; ?></td>
                                    <td><a class="btn green mini" href="<?php echo make_admin_url('vehicles', 'stoppages', 'stoppages', 'id=' . $route->id); ?>">Stoppages</a></td>
                                    <td><a href="<?php echo make_admin_url('vehicles', 'update_route', 'update_route', 'id=' . $route->id); ?>" class="btn blue mini"><i class="icon-pencil"></i></a> <a href="<?php echo make_admin_url('vehicles', 'delete_route', 'delete_route', 'id=' . $route->id); ?>" class="btn red mini" onclick="return confirm('Are you sure? You are deleting this page.');"><i class="icon-remove"></i></a></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->  
