<?php if (($section == 'expenses') || ($section == 'update_expense')) { ?>
    <div class="tile bg-blue <?php echo ($section == 'create_expense') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('vehicles', 'create_expense', 'create_expense'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-plus"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    Add Expense
                </div>
            </div>
        </a>   
    </div>
    <div class="tile bg-blue <?php echo ($section == 'route') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('vehicles', 'route', 'route'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-list"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    List Routes
                </div>
            </div>
        </a>   
    </div>
<?php } else { ?>
    <div class="tile bg-blue <?php echo ($section == 'create_route') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('vehicles', 'route', 'create_route'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-plus"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    Add Route Map
                </div>
            </div>
        </a>   
    </div>
    <div class="tile bg-blue <?php echo ($section == 'route') ? 'selected' : '' ?>">
        <a href="<?php echo make_admin_url('vehicles', 'route', 'route'); ?>">
            <div class="corner"></div>

            <div class="tile-body">
                <i class="icon-list"></i>
            </div>
            <div class="tile-object">
                <div class="name">
                    List Routes
                </div>
            </div>
        </a>   
    </div>
<?php } ?>
<div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('vehicles', 'list', 'list'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                List Vehicles
            </div>
        </div>
    </a>   
</div>

<div class="tile bg-blue <?php echo ($section == 'insert') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('vehicles', 'insert', 'insert'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-plus"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                New Vehicle
            </div>
        </div>
    </a> 
</div>

