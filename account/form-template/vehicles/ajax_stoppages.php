<?php
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'get_stoppages') {
        $obj = new vehicle_stoppages;
        $stoppages = $obj->listStoppages($route_id);
        $student_vehicle = vehicleStudent::getStudentVehicleDetails($vehicle_student_id);
        ?>
        <div class="control-group">		
            <label class="control-label" for="vehicle_stoppage">Select Stoppage<span class="required"></span></label>
            <div class="controls">
                <select id="vehicle_stoppage" class="span12 m-wrap">
                    <?php foreach ($stoppages as $stoppage) { ?>
                        <option value="<?= $stoppage->amount; ?>" data-attr="<?= $stoppage->id ?>" <?= $student_vehicle->stoppage_id == $stoppage->id ? 'selected' : '' ?>><?= $stoppage->stoppage; ?></option>
                    <?php } ?>
                </select>
            </div>	
        </div>
        <div class="control-group">
            <label for="amount" class="control-label">Amount(<?= CURRENCY_SYMBOL ?>) (Per Fee)<span class="required">*</span></label>
            <div class="controls">
                <input name="amount" style='padding-left:5px;' id='vehicle_amount' value="" class="span12 m-wrap" disabled/>
            </div>
        </div>
        <?php
    }
}
?>
