<?
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');

if ($_POST):
    isset($_POST['id']) ? $id = $_POST['id'] : $id = '';
    if ($id == '0'):
        ?>
        <label class="control-label" style='float: none; text-align: left;'>Select Section</label>
        <input type="text" value="All Sections" disabled class="span2"/>
        <input type='submit' name='go' value='Go' class='btn blue'/>
        <?php
    else:
#Get All current session Section
        $QuerySec = new vehicleStudent();
        $AllSection = $QuerySec->getSessionsection($id);
        ?>
        <label class="control-label" style='float: none; text-align: left;'>Select Section</label>
        <? if (!empty($AllSection)): ?>	
            <select class="select2_category session_filter span2" data-placeholder="Select Session Students" name='ct_sec'>
                <option value="0">All</option>
                <?php
                $sect_sr = 1;
                foreach ($AllSection as $sec_key => $sect):
                    ?>
                    <option value='<?= $sect->section ?>' <?
                    if (!empty($ct_sec) && $sect->section == $ct_sec) {
                        echo 'selected';
                    }
                    ?>  ><?= ucfirst($sect->section) ?></option>
                            <?
                            $sect_sr++;
                        endforeach;
                        ?>
            </select>
            <input type='submit' name='go' value='Go' class='btn blue'/>
        <? else: ?>
            <input type='text' value='Sorry, No Section Found.' disabled />
        <? endif; ?>						
    <?
    endif;
endif;
?>