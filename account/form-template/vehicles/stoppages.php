<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Stoppages
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    List Stoppages
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-truck"></i>List Stoppages</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <table class="table table-bordered" id="sample_2">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Stoppage</th>
                                <th>Amount</th>
                                <th>Pick Time</th>
                                <th>Drop Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $k = 1;
                            foreach ($stoppages as $stoppage) {
                                ?>
                                <tr>
                                    <td><?php echo $k++; ?></td>
                                    <td><?php echo $stoppage->stoppage; ?></td>
                                    <td><?php echo CURRENCY_SYMBOL . ' ' . $stoppage->amount; ?></td>
                                    <td><?php echo $stoppage->pick_time; ?></td>
                                    <td><?php echo $stoppage->drop_time; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->  
