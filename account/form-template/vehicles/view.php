

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Vehicle Students
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('vehicles', 'list', 'list'); ?>"> List Vehicles</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li class="last">
                    <i class="icon-certificate"></i>
                    <?= $object->make . " " . $object->vehicle_number ?></a> > Students
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>

    <div class="tiles pull-left">

        <form class="form-horizontal" action="<?php echo make_admin_url('vehicles', 'student', 'student') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
            <input type='hidden' name='Page' value='vehicles'/>
            <input type='hidden' name='action' value='view'/>
            <input type='hidden' name='section' value='view'/>
            <input type='hidden' name='id' value='<?= $id ?>'/>


            <? if (!empty($AllSession)): ?>
                <div class="control-group alert alert-success" style='margin-left:0px;padding-right:0px;'>

                    <div class='span3'>
                        <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                        <select class="select2_category session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                            <option value="0">ALL</option>
                            <?php
                            $session_sr = 1;
                            foreach ($AllSession as $s_key => $session):
                                ?>
                                <option value='<?= $session->id ?>' <?
                                if ($session->id == $session_id) {
                                    echo 'selected';
                                }
                                ?> ><?= ucfirst($session->title) ?></option>
                                        <?
                                        $session_sr++;
                                    endforeach;
                                    ?>
                        </select>
                    </div>

                    <div class='span3' id='ajex_section'>
                        <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                        <select class="select2_category span2" data-placeholder="Select Session Students" name='ct_sec'>
                            <option value="">Select Section</option>

                            <?php
                            $sect_sr = 1;
                            foreach ($AllSection as $sec_key => $sect):
                                ?>
                                <option value='<?= $sect->section ?>' <?
                                if (!empty($ct_sec) && $sect->section == $ct_sec) {
                                    echo 'selected';
                                }
                                ?>  ><?= ucfirst($sect->section) ?></option>
                                        <?
                                        $sect_sr++;
                                    endforeach;
                                    ?>
                        </select>	
                        <input type='submit' name='go' value='Go' class='btn blue'/>
                    </div>
                </div>
            <? endif; ?>
        </form>										
    </div>

    <div class="tiles pull-right">

        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <form class="form-horizontal" action="<?php echo make_admin_url('vehicles', 'view', 'view&id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
                <div class="portlet box red">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i><?= $object->make . " " . $object->vehicle_number ?> > Students</div>								
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>											
                                    <th class="hidden-480">Reg ID</th>
                                    <th class="hidden-480">Name</th>
                                    <th class="hidden-480">Father Name</th>
                                    <th class="hidden-480">Sex</th>		
                                    <th class="hidden-480">Session</th>	
                                    <th class="hidden-480">Section</th>	
                                    <th class="hidden-480">Route</th>
                                    <th class="hidden-480">Round</th>
                                    <th class="hidden-480">Stoppage</th>
                                    <th class="hidden-480">Pick Time</th>
                                    <th class="hidden-480">Drop Time</th>
                                    <th class="hidden-480" style='text-align:right;'>Amount</th>											
                                </tr>
                            </thead>
                            <? if (!empty($AllStu)): ?>
                                <tbody>
                                    <?php
                                    $sr = 1;
                                    foreach ($AllStu as $sk => $stu): $checked = '';
                                        $CurrObj = new studentSession();
                                        $current_session = $CurrObj->getStudentCurrentSession($school->id, $stu->id);
                                        ?>
                                        <tr class="odd gradeX" <?
                                        if (in_array($stu->id, $veh_st)): echo 'id="added"';
                                            $checked = 'checked';
                                        endif;
                                        ?> >

                                            <td><?= $stu->reg_id ?></td>
                                            <td><?= $stu->first_name . " " . $stu->last_name ?></td>
                                            <td><?= $stu->father_name ?></td>
                                            <td class="hidden-480"><?= $stu->sex ?></td>
                                            <td class="hidden-480"><?= $current_session ?></td>
                                            <td class="hidden-480"><?= $stu->section ?></td>
                                            <td>
                                                <?php
                                                $obj = new vehicle_route;
                                                $route = $obj->getRecord($stu->route_id);
                                                echo $route->route_from . ' - ' . $route->route_to;
                                                ?>
                                            </td>
                                            <td><?php echo $stu->round ?></td>
                                            <?php
                                            $objNew = new vehicle_stoppages;
                                            $stoppage = $objNew->getRecord($stu->stoppage_id);
                                            ?>
                                            <td><?php echo $stoppage->stoppage; ?></td>
                                            <td><?php echo $stoppage->pick_time; ?></td>
                                            <td><?php echo $stoppage->drop_time; ?></td>
                                            <td style='text-align:right;'>
                                                <button type="button" data-vehicle_student_id="<?= $stu->vehicle_student_id ?>" data-vehicle_id="<?= $id ?>" data-student_id="<?= $stu->id ?>" class="btn blue mini" id="shift_round" style="height: auto">Shift Round</button>
                                                <br/>
                                                <br/>
                                                <button type="button" class="btn green mini" style="height: auto"><?= CURRENCY_SYMBOL . number_format($stu->amount, 2) ?></button></td>
                                        </tr>
                                    <? endforeach; ?>									
                                </tbody>

                            <? endif; ?>
                        </table>
                    </div>
                </div>
            </form>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>

</div>
<!-- END PAGE CONTAINER-->    
<div class="clearfix"></div>
<div class="modal fade" id="shift_round_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h3 class="modal-title" id="myModalLabel" style="text-align: center">
                    <b id="label_store_name"></b>
                </h3>
            </div>
            <div class="modal-body" style="height: auto;overflow: visible !important">
                <div id="content"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTAINER-->    
<script type="text/javascript">
    $(".session_filter").change(function () {
        var session_id = $("#session_id").val();

        if (session_id.length > 0) {
            var id = session_id;
            $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id;

            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_vehicle', 'ajax_section_vehicle&temp=vehicles'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_section").html(data);
                }
            });
        }
        else {
            return false;
        }
    }).ready(function () {
        $(".session_filter").change();
    });

    $(document).on('click', '#shift_round', function () {
        var vehicle_id = $(this).attr('data-vehicle_id');
        var student_id = $(this).attr('data-student_id');
        var vehicle_student_id = $(this).attr('data-vehicle_student_id');
        var modal = $('#shift_round_modal');
        var modal_content = modal.find('#content');
        var modal_footer = modal.find('.modal-footer');
        modal_footer.show();
        var modal_label_name = modal.find('#label_store_name');
        modal_content.html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        modal_label_name.html('<code>Shift Round</code>');
        modal.modal();
        setTimeout(function () {
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=vehicles'); ?>", {act: 'get_student_data', vehicle_id: vehicle_id, student_id: student_id, vehicle_student_id: vehicle_student_id}, function (data) {
                modal_footer.hide();
                modal_content.html(data).hide().slideDown();
                $('#vehicle_route').change();
            });
        }, 000);
    });

    $(document).on('click', '#change_round', function () {
        var vehicle_student_id = $(this).attr('data-vehicle_student_id');
        var route_id = $('#vehicle_route').val();
        var round = $('select#round').val();
        var stoppage_id = $("#vehicle_stoppage option:selected").attr('data-attr');
        var amount = $('#vehicle_amount').val();
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=vehicles'); ?>", {act: 'update_student_vehicle_data', vehicle_student_id: vehicle_student_id, route_id: route_id, stoppage_id: stoppage_id, amount: amount, round: round}, function (data) {
            if (data === 'success') {
                toastr.success('Student Round Shifted Successfully!', 'Success');
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            } else {
                toastr.error('Something Went Wrong!', 'Error');
            }
        });
    });

    $(document).on('change', '#vehicle_route', function () {
        var vehicle_student_id = $('#change_round').attr('data-vehicle_student_id');
        var route_id = $(this).val();
        var student_id = $('#shift_round').attr('data-student_id');
        $("#vehicle_stoppages").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_stoppages', 'ajax_stoppages&temp=vehicles'); ?>", {act: 'get_stoppages', route_id: route_id, student_id: student_id, vehicle_student_id: vehicle_student_id}, function (data) {
            $('#vehicle_stoppages').html(data);
            $('#vehicle_stoppage').change();
        });
    }).ready(function () {
        $('#vehicle_route').change();
    });

    $(document).on('change', '#vehicle_stoppage', function () {
        var amount = $(this).val();
        $('input#vehicle_amount').val(amount);
    });
</script>
