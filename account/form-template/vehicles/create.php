
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Vehicles
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('vehicles', 'list', 'list'); ?>">List Vehicles</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add New Vehicle
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->


    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('vehicles', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-truck"></i>Add New Vehicle</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">      
                        <!-- Vehicle Detail  -->
                        <h4 class="form-section hedding_inner">Vehicle Information</h4>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="make">Company<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="make" value="" id="make" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="year">Model Year<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="year" value="" id="year" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 

                                <div class="control-group">
                                    <label class="control-label" for="color">Color<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="color" value="" id="color" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>



                                <div class="control-group">
                                    <label class="control-label" for="type">Vehicle Type<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="type" value="" id="type" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="possible_rounds">Total Possible Rounds<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="number" name='possible_rounds' id='possible_rounds' value='' class="m-wrap span10 validate[required]" > 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="permit">Permit Number<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name='permit' value='' id='permit' class="m-wrap span10 validate[required]" > 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="insurance">Insurance Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name='insurance' value='' id='insurance' class="m-wrap span10 validate[required]" > 
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="vendor">Vendor Or Owned<span class="required">*</span></label>
                                    <div class="controls">
                                        <label class="radio">
                                            <div class="radio owned"><span class=""><input checked="" type="radio" value="option1" name="optionsRadios1"></span></div>
                                            Owned
                                        </label>
                                        <label class="radio vendor">
                                            <div class="radio"><span><input type="radio" value="option2" name="optionsRadios1"></span></div>
                                            Vendor
                                        </label> 
                                        </label>  
                                    </div>
                                    <div class="controls" id='vendor_div'>
                                        <input type="text" name="vendor" value="" id="vendor" class="span10 m-wrap" readonly />
                                    </div>													
                                </div> 
                            </div>	
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="model">Model<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="model" value="" id="model" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="purchase_date">Purchase Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="purchase_date" value="" id="purchase_date" class="span10 m-wrap new_format validate[required]" />
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="vehicle_number">Vehicle Number<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="vehicle_number" value="" id="vehicle_number" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 

                                <div class="control-group">
                                    <label class="control-label" for="seating_capacity">Seating Capacity<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="seating_capacity" value="" id="seating_capacity" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="max_seating_capacity">Maximum Seating Capacity<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="number" name='max_seating_capacity' value='' id='max_seating_capacity' class="m-wrap span10 validate[required]" > 
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="permit_expiry">Permit Expiry Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name='permit_expiry' id='permit_expiry' value='' class="m-wrap span10 validate[required] new_format" > 
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="insurance_expiry">Insurance Expiry<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name='insurance_expiry' id='insurance_expiry' value='' class="m-wrap span10 validate[required] new_format" > 
                                    </div>
                                </div>
                                <div class="span12">
                                    <h4 class="form-section hedding_inner">Doc. Attachment</h4>
                                    <div id="appendable_fields">
                                        <div class="span4">
                                            <div class="control-group">
                                                <label class="control-label span4" for="title">Title</label>
                                                <div class="controls span8">
                                                    <input type="text" name='title[]' id='title' value='' class="m-wrap span10" > 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span8">
                                            <div class="control-group">
                                                <label class="control-label span1" for="doc"></label>
                                                <div class="controls span11">
                                                    <input type="file" name='doc[]' id='doc' value='' class="m-wrap span11" > 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label span9" for="doc"></label>
                                    <div class="controls span3">
                                        <input type="button" class="m-wrap btn red" id="add_more_doc_fields" value="Add More">
                                    </div>
                                </div>
                            </div>	
                        </div>
                        <!-- Docs Detail -->
                        <div id="rounds_html"></div>
                        <!-- Driver Detail  -->
                        <h4 class="form-section hedding_inner">Driver Information</h4>
                        <div class="row-fluid">
                            <div class="span6 ">	
                                <div class="control-group">
                                    <label class="control-label" for="teacher_cat">Select Staff Category<span class="required">*</span></label>
                                    <div class="controls">

                                        <select class="teacher_cat"  class="span10 m-wrap ">
                                            <option value=''>Select Category</option>
                                            <?php
                                            $sr = 1;
                                            while ($cat = $QueryStaff->GetObjectFromRecord()):
                                                ?>
                                                <option value='<?= $cat->id ?>'><?= $cat->name ?></option>
                                                <?
                                                $sr++;
                                            endwhile;
                                            ?>
                                        </select>                      
                                    </div>
                                </div>
                            </div>
                            <div class="span6 ">							
                                <div class="control-group" id='dyanamic_teacher'>
                                    <label class="control-label" for="driver_id">Driver Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name='driver_id' id='driver_id' class="m-wrap span8 validate[required]" readonly>                 
                                    </div>
                                </div>	
                            </div>
                        </div>										
                        <!-- Conductor Detail  -->
                        <h4 class="form-section hedding_inner">Conductor Information</h4>										
                        <div class="row-fluid">
                            <div class="span6 ">										
                                <div class="control-group">
                                    <label class="control-label" for="conductor_cat">Select Staff Category<span class="required">*</span></label>
                                    <div class="controls">

                                        <select class="conductor_cat span10 m-wrap">
                                            <option value=''>Select Category</option>
                                            <?php
                                            $sr = 1;
                                            while ($cat1 = $QueryStaffCond->GetObjectFromRecord()):
                                                ?>
                                                <option value='<?= $cat1->id ?>'><?= $cat1->name ?></option>
                                                <?
                                                $sr++;
                                            endwhile;
                                            ?>
                                        </select>                      
                                    </div>
                                </div>
                            </div>
                            <div class="span6 ">	
                                <div class="control-group" id='dyanamic_conductor'>
                                    <label class="control-label" for="conductor_id">Conductor Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name='conductor_id' id='conductor_id' class="m-wrap span8 validate[required]" readonly>                 
                                    </div>
                                </div>
                            </div><br/>	
                            <div class="control-group">
                                <label class="control-label" for="meta_keyword">Make Active</label>
                                <div class="controls">
                                    <input type="checkbox" name="is_active" id="meta_keyword" value="1">
                                </div>
                            </div> 

                            <div class="form-actions">
                                <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                                <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                <a href="<?php echo make_admin_url('vehicles', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
        </form>
        <div class="clearfix"></div>

    </div>

    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    

<script type="text/javascript">
    jQuery(document).ready(function () {
        /* Add New vendor */
        $(".vendor").live("click", function () {
            $("div#vendor_div").replaceWith("<div id='vendor_div' class='controls'><input type='text' name='vendor' value='' id='vendor' class='span10 m-wrap validate[required]'/></div>");
        });
        /* Add New vendor */
        $(".owned").live("click", function () {
            $("div#vendor_div").replaceWith("<div id='vendor_div' class='controls'><input type='text' name='vendor' readonly='' value='' id='vendor' class='span10 m-wrap'/></div>");
        });
        $(".teacher_cat").change(function () {
            var cat_id = $(this).val();
            var id = cat_id;
            $("#dyanamic_teacher").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            var dataString = 'id=' + id;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_driver&temp=vehicles'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#dyanamic_teacher").html(data);
                }
            });
        });
        $(".conductor_cat").change(function () {
            var cat_id = $(this).val();
            var id = cat_id;
            $("#dyanamic_conductor").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            var dataString = 'id=' + id;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_conductor', 'ajax_conductor&temp=vehicles'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#dyanamic_conductor").html(data);
                }
            });
        });
    });
    $(document).on('change', '#possible_rounds', function () {
        var rounds = $(this).val();
        if (rounds) {
            var rounds_html = '';
            for (var i = 1; i <= rounds; i++) {
                rounds_html += '<div class="row-fluid" id="round_route"><div class="span6"><div class="control-group"><label class="control-label" for="round_route">Select Round</label><div class="controls"><input type="text" value="Round ' + i + '" disabled></div></div></div><div class="span6"><div class="control-group"><label class="control-label">Select Route</label><div class="controls"><select name="round_route[]" id="round_route" class="m-wrap span10">';
<?php foreach ($routes as $route) { ?>
                    rounds_html += '<option value="<?= $route->id; ?>"><?= $route->route_from . " to " . $route->route_to ?></option>';
<?php } ?>
                rounds_html += '</select></div></div></div>';
            }
            $('#rounds_html').html(rounds_html);
        } else {
            $('#rounds_html').html('');
        }
    });
    $(document).on('keyup', '#possible_rounds', function () {
        $(this).change();
    });

    $(document).on('click', '#add_more_doc_fields', function () {
        $('#appendable_fields').append('<div class="row-fluid"><div class="span4"><div class="control-group"><label class="control-label span4" for="title">Title</label><div class="controls span8"><input type="text" name="title[]" id="title" value="" class="m-wrap span10"></div></div></div><div class="span8"><div class="control-group"><label class="control-label span1" for="doc"></label><div class="controls span11"><input type="file" name="doc[]" id="doc" value="" class="m-wrap span9"><button type="button" id="remove_field" class="btn red"><i class="icon-trash"></i></button></div></div></div></div>');
    });

    $(document).on('click', '#remove_field', function () {
        $($(this).parents('.row-fluid')[0]).hide(400, function () {
            $(this).remove();
        });
    });
</script>	