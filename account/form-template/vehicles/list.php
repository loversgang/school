<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Vehicles
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    All Vehicles
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-truck"></i>Manage Vehicles</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('session', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >	
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>Model</th>
                                    <th style='text-align:center' class="hidden-480">Make</th>
                                    <th style='text-align:center' class="hidden-480">Vehicle Number</th>
                                    <th style='text-align:center' class="hidden-480 sorting_disabled">Color</th>
                                    <th style='text-align:center' class="hidden-480 sorting_disabled">Expenses</th>
                                    <th style='text-align:center' class="hidden-480 sorting_disabled">Students</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <? if ($QueryObj->GetNumRows() != 0): ?>
                                <tbody>
                                    <?php
                                    $sr = 1;
                                    while ($object = $QueryObj->GetObjectFromRecord()):
                                        ?>
                                        <tr class="odd gradeX">
                                            <td><?= $object->model ?></td>
                                            <td style='text-align:center' class="hidden-480"><?= $object->make ?></td>
                                            <td style='text-align:center' class="hidden-480"><?= $object->vehicle_number ?></td>
                                            <td style='text-align:center' class="hidden-480"><?= $object->color ?></td>						<td>
                                                <a href="<?php echo make_admin_url('vehicles', 'expenses', 'expenses', 'vehicle_id=' . $object->id); ?>" class="btn green mini">Expense</a>
                                            </td>								
                                            <td style='text-align:center' class="hidden-480 sorting_disabled"><a class="btn mini yellow icn-only tooltips" href="<?php echo make_admin_url('vehicles', 'view', 'view', 'id=' . $object->id) ?>" title="click here to view students">Students </a> </td>
                                            <td style='text-align:right;'>
                                                <a class="mini btn blue icn-only tooltips" href="<?php echo make_admin_url('vehicles', 'update', 'update', 'id=' . $object->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                                <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('vehicles', 'delete', 'delete', 'id=' . $object->id . '&delete=1') ?>" onclick="return confirm('Are you sure? You are deleting this vehicle and its students.');" title="click here to delete this session"><i class="icon-remove icon-white"></i></a>
                                            </td>												
                                        </tr>
                                        <?php
                                        $sr++;
                                    endwhile;
                                    ?>
                                </tbody>
<?php endif; ?>  
                        </table>
                    </form>    
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



