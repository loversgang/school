<?php
include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'delete_doc') {
        $obj = new vehicle_docs;
        $obj->id = $doc_id;
        $obj->Delete();
        echo "success";
    }
    if ($act == 'delete_expense_doc') {
        $obj = new vehicle_expense_docs;
        $obj->id = $doc_id;
        $obj->Delete();
        echo "success";
    }
    if ($act == 'delete_stoppage') {
        $obj = new vehicle_stoppages;
        $obj->id = $stoppage_id;
        $obj->Delete();
        echo "success";
    }
    if ($act == 'get_student_data') {
        ?>
        <div class="form-horizontal">
            <?php
            $current_vehicle = get_object('school_vehicle', $vehicle_id);
            $obj = new vehicle_route;
            $routes = $obj->listVehicleRoute($school->id);
            $student_vehicle = vehicleStudent::getStudentVehicleDetails($vehicle_student_id);
            $rounds = $current_vehicle->possible_rounds;
            ?>
            <div class="row-fluid">
                <div class="control-group">		
                    <label class="control-label" for="vehicle_route">Select Route<span class="required"></span></label>
                    <div class="controls">
                        <select id="vehicle_route" class="span12 m-wrap">
                            <?php foreach ($routes as $route) { ?>
                                <option value="<?= $route->id; ?>"
                                        <?= $student_vehicle->route_id == $route->id ? 'selected' : '' ?>><?= $route->route_from . " To " . $route->route_to; ?></option>
                                    <?php } ?>
                        </select>
                    </div>	
                </div>
                <div id="vehicle_stoppages"></div>
                <div class="control-group">
                    <label class="control-label" for="round">Select Round</label>
                    <div class="controls">
                        <select class="m-wrap span12" id="round">
                            <?php for ($i = 1; $i <= $rounds; $i++) { ?>
                                <option value="<?= $i ?>" <?= $student_vehicle->round == $i ? 'selected' : '' ?>>Round <?= $i ?>
                                <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <button type="button" id="change_round" data-vehicle_student_id="<?= $vehicle_student_id ?>" class="btn blue pull-right">Update</button>
                    <button style="margin-right: 10px" type="button" class="btn default pull-right" data-dismiss="modal" aria-label="Close">Cancel</button>
                </div>
            </div>
        </div>
        <?php
    }
    if ($act == 'update_student_vehicle_data') {
        $arr['id'] = $vehicle_student_id;
        $arr['route_id'] = $route_id;
        $arr['stoppage_id'] = $stoppage_id;
        $arr['amount'] = $amount;
        $arr['round'] = $round;
        $obj = new vehicleStudent;
        $obj->saveData($arr);
        echo "success";
    }

    if ($act == 'get_vehicle_round') {
        $obj = new vehicle;
        $vehicle = $obj->getRecord($vehicle_id);
        $rounds = $vehicle->possible_rounds;
        $chk = vehicleStudent::chkStudentAssigned($student_id);
        if ($chk) {
            $student_vehicle = vehicleStudent::getStudentRoot($student_id);
            ?>
            <div class="control-group">
                <label class="control-label" for="round">Select Round</label>
                <div class="controls">
                    <select class="m-wrap span10" id="round">
                        <?php for ($i = 1; $i <= $rounds; $i++) { ?>
                            <option value="<?= $i ?>" <?= $student_vehicle->round == $i ? 'selected' : '' ?>>Round <?= $i ?>
                            <?php } ?>
                    </select>
                </div>
            </div>
        <?php } else { ?>
            <div class="control-group">
                <label class="control-label" for="round">Select Round</label>
                <div class="controls">
                    <select class="m-wrap span10" id="round">
                        <?php for ($i = 1; $i <= $rounds; $i++) { ?>
                            <option value="<?= $i ?>">Round <?= $i ?>
                            <?php } ?>
                    </select>
                </div>
            </div>
        <?php } ?>
        <?php
    }
}