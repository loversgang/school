<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Banks
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('bank', 'list', 'list'); ?>">List Banks</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add New Rule
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
    </div>	            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal validation" action="<?php echo make_admin_url('bank', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">	
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Add New Pay Rule</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="control-group">
                            <label class="control-label" for="bank">Bank Name<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="bank"  value="" id="bank" class="span6 m-wrap validate[required]" />
                            </div>
                        </div>        
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('bank'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>

                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>