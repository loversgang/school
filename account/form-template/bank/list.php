<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Bank
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li class="last">
                    List Bank
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Manage Bank</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">	
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th>Sr. No</th>
                                <th>Bank</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($banks as $key => $bank) { ?>
                                <tr>
                                    <td><?php echo $key+=1 ?></td>
                                    <td><?php echo $bank['bank'] ?></td>
                                    <td class=" ">
                                        <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url($Page, 'update', 'update&id=' . $bank['id']) ?>" title="" data-original-title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;
                                        <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url($Page, 'delete', 'delete&id=' . $bank['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="" data-original-title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>    
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>