
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Services / Products
                                    <a class="btn large grey" href="<?php echo make_admin_url('service', 'list', 'insert');?>"><i class="icon-plus"></i> Add New Service / Product</a>
                            </h3>
                            
                            <!--
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                     <li>
                                        <i class="icon-globe"></i>
                                               <a href="<?php echo make_admin_url('service', 'list', 'list');?>">List Services</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                     <li class="last">
                                        Trash
                                    </li>

                            </ul>-->


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php  #include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-globe"></i>Trash</div>
								
							</div>
							<div class="portlet-body">
							        <table class="table table-striped table-bordered table-hover" id="">
									<thead>
										 <tr>
                                                                                        
                                                                                        <th class="hidden-480">Sr. No</th>
                                                                                        <th>Name</th>
                                                                                 
                                                                                        <th class="hidden-480">Position </th>
                                                                                        <th>Show on Website</th>
                                                                                        <th >Action</th>
                                                                                        

                                                                                </tr>
									</thead>
									 <tbody>
                                                                            
                                                                              <? if($QueryObj->GetNumRows()!=0):?>
                                                                             <?php $sr=1;while($service=$QueryObj->GetObjectFromRecord()):?>
                                                                                    <tr class="odd gradeX">
                                                                                    
                                                                                        
                                                                                        <td class="hidden-480"><?php echo $sr++;?></td>
											<td><?php echo $service->name?></td>
                                                                                  
											<td class="hidden-480">
                                                                                           <?php echo $service->position?> 
                                                                                        </td>
											<td>
                                                                                            <span class="label label-<?php echo ($service->is_active=='1')?'success':'important';?>"><?php echo ($service->is_active=='1')?'Show':'Hide';?></span>
                                                                                        </td>
											<td>
                                                                                            
                                                                                               <a class="btn green icn-only tooltips" href="<?php echo make_admin_url('service', 'restore', 'restore', 'id='.$service->id)?>" class="tipTop smallbtn" title="click here to restore this record"><i class="icon-undo icon-white"></i></a>&nbsp;&nbsp;
                                                                                               <a class="btn red icn-only tooltips" href="<?php echo make_admin_url('service', 'permanent_delete', 'permanent_delete', 'id='.$service->id.'&delete=1')?>" onclick="return confirm('Are you deleting this record permanently?.');" class="tipTop smallbtn" title="click here to delete this record permanently" ><i class="icon-remove icon-white"></i></a>  
                                                                                         </td>
											

                                                                                    </tr>
                                                                             <?php endwhile;?>
                                                                           

									</tbody>
                                                                      
                                                                       <?php endif;?>  
								</table>
                                                            
                                                            
                              
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

