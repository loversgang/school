
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Services / Products
                                    <a class="btn large grey" href="<?php echo make_admin_url('service', 'list', 'insert');?>"><i class="icon-plus"></i> Add New Service / Product</a>
                            </h3>
                            <!--
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-globe"></i>
                                               <a href="<?php echo make_admin_url('service', 'list', 'list');?>">List Services</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        Edit Service
                                    </li>

                            </ul>->


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php #  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('service', 'update', 'list')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-globe"></i>Edit Service /  Product</div>
                                            <div class="tools">
                                                  <a target="_blank" href="<?php echo make_url_user('service_detail','id='.$id)?>" style="color:white !important" class=""><i class="icon-search icon-white"></i> &nbsp;view service /  product</a>


                                           </div>
                                    </div>
                                    <div class="portlet-body form">

                                             <div class="control-group">
                                                    <label class="control-label" for="name">Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="name" value="<?=$service->name?>" id="name" class="span12 m-wrap validate[required]" />
                                                    </div>  
                                            </div>
                                          <!--
                                            <div class="control-group">
                                                    <label class="control-label" for="urlname">Urlname</label>
                                                    <div class="controls">
                                                       <input type="text" name="urlname" id="urlname"  value="<?=$service->urlname;?>" class="span6 m-wrap" />
                                                    
                                                    </div>  
                                            </div>
                                          -->
                                            <div class="control-group">
                                                    <label for="image" class="control-label">Image</label>
                                                    <div class="controls">
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="fileupload-new thumbnail" style="max-width: 200px;position:relative ">
                                                                          <?php if($service->image && (file_exists(DIR_FS_SITE_UPLOAD.'photo/service/large/'.$service->image))):
                                                                                 $image_obj=new imageManipulation();?>
                                                                                 <img style="max-width:200px" src="<?=$image_obj->get_image('service','large', $service->image);?>">
                                                                                 
                                                                                 <div style="position:absolute;top:4px;right:4px;"  class="btn-group" data-toggle="buttons-radio">
                                                                                   <a onclick="return confirm('Image shall be permanently deleted. Are you sure?');" title="delete image" href="<?php echo make_admin_url('service', 'delete_image', 'delete_image', 'id='.$service->id);?>" class="grey btn icn-only mini"><i class="icon-white icon-remove"></i></a>
                                                                                   &nbsp;
                                                                                   <a href="<?=$image_obj->get_image('service','large',  $service->image);?>"  title="View Original" class="grey btn icn-only mini fancybox-button"><i class="icon-white icon-zoom-in"></i></a>
                                                                                 </div>
                                                                          
                                                                                 
                                                                                
                                                                          <?php else:?>
                                                                                  <img src="assets/img/noimage.jpg"/> 
                                                                          <?php endif;?>
                                                                       
                                                                    </div>
                                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                    <div>
                                                                            <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                                            <span class="fileupload-exists">Change</span>
                                                                            <input type="file" id="image" name="image" class="default" /></span>
                                                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                                    </div>
                                                            </div>
                                                           
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="short_description">About service / product in short<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <textarea id="short_description" class="span12 m-wrap validate[required]" name="short_description" rows="6"><?php echo html_entity_decode($service->short_description);?></textarea>
                                                       
                                                    </div>
                                             </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="long_description">About service / product in detail<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <textarea id="long_description" class="span12 ckeditor m-wrap" name="long_description" rows="6"><?php echo html_entity_decode($service->long_description);?></textarea>
                                                       
                                                    </div>
                                             </div>
                                           <!--
                                            <div class="control-group">
                                                    <label class="control-label" for="is_active">Show on Website</label>
                                                    <div class="controls">
                                                        <input type="checkbox" name="is_active" id="is_active" value="1" <?=($service->is_active=='1')?'checked':'';?> />
                                                    </div>  
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="position">Position<span class="required">*</span></label>
                                                    <div class="controls">
                                                         <input style="width:10%" type="text" id="position"  name="position" value="<?=$service->position?>" class="span6 m-wrap validate[required]" />
                                                    </div>  
                                            </div>
                                           -->
                                           <?php
                                             if($service->is_active=='1'):?>
                                                <input type="hidden" name="is_active"  value="1" /> 
                                            <?php endif;
                                            ?>
                                            
                                            <input type="hidden" name="id" value="<?php echo $service->id?>" />

                                            <br/>
                                            <h3>SEO Information</h3>
                                            <div class="control-group">
                                                   <label class="control-label" for="meta_name">Meta Title</label>
                                                   <div class="controls">
                                                       <input type="text" name="meta_name" id="meta_name"  value="<?=$service->meta_name;?>" class="span12 m-wrap" /> 
                                           
                                                   </div>  
                                            </div>  
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_keyword">Meta Keywords</label>
                                                    <div class="controls">
                                                    <input type="text" name="meta_keyword" id="meta_keyword"  value="<?=$service->meta_keyword;?>" class="span12 m-wrap" />
                                           
                                                    </div>
                                            </div>          
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_description">Meta Description</label>
                                                    <div class="controls">
                                            
                                                       <textarea rows="3" class="span12 m-wrap"  style="width: 346px; height: 82px;" id="meta_description"  name="meta_description"><?=$service->meta_description;?></textarea>
                                          
                                                    </div>
                                           </div>           
                                        
                                        
                                
                                            <div class="form-actions">
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('service', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                                    
                                            </div>
                                         
                               
                                  
                              </div> 
                            </div>
                        </div>
                          
                            
                            
                     </form>
                 
                
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



