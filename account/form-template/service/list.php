<!-- / Breadcrumbs -->
 <script type="text/javascript">
        jQuery(document).ready(function() {
           /* on click */
            $(".status_on").live("click",function(){
                
                var id=$(this).attr('id');
                var dataString = 'table=user_service&id='+id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo make_admin_url('status_off')?>",
                    data: dataString,
                    success: function(data, textStatus) {
                        
			$("#"+id).removeClass("status_on");
                        $("#"+id).addClass("status_off");
                    }
                });
            });
            
            $(".status_off").live("click",function(){
                var id=$(this).attr('id');
                var dataString = 'table=user_service&id='+id;
                $.ajax({
                    type: "GET",
                    url: "<?php echo make_admin_url('status_on')?>",
                    data: dataString,
                    success: function(data, textStatus) {
                       
                        $("#"+id).removeClass("status_off");
                        $("#"+id).addClass("status_on");
                    }
                });
            });
        });
    </script>

  

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Services / Products
                                    <a class="btn large grey" href="<?php echo make_admin_url('service', 'list', 'insert');?>"><i class="icon-plus"></i> Add New Service / Product</a>
                            </h3>
                           <!-- <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List Services</li>

                            </ul>-->


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php #  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-globe"></i>Manage Services /  Products</div>
								<div class="tools">
									  <a target="_blank" href="<?php echo make_url_user('service')?>" style="color:white !important" class=""><i class="icon-search icon-white"></i> &nbsp;view services / products </a>
                                                                   
									
								</div>
							</div>
							<div class="portlet-body">
							     <form action="<?php echo make_admin_url('service', 'update2', 'list', 'id='.$id);?>" method="post" id="form_data" name="form_data" >	
								<table class="table table-striped table-bordered table-hover" id="<?php echo $QueryObj->GetNumRows()!=0?'sample_1':''?>" >
									<thead>
										 <tr>
                                                                                        <th class="hidden-480" style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /></th>
                                                                                        <th class="hidden-480">Sr.No</th>
                                                                                        <th>Name</th>
                                                                                        
                                                                                        <th class="hidden-480">Position </th>
                                                                                        <th>Show on website</th>
                                                                                        <th >Action</th>
                                                                                        
                                                                                        

                                                                                </tr>
									</thead>
									 <tbody>
                                                                            
                                                                              <? if($QueryObj->GetNumRows()!=0):?>
                                                                             <?php $sr=1;while($service=$QueryObj->GetObjectFromRecord()):?>
                                                                                    <tr class="odd gradeX">
                                                                                        <td class="hidden-480">
                                                                                            <input class="checkboxes" id="multiopt[<?php echo $service->id ?>]" name="multiopt[<?php echo $service->id ?>]" type="checkbox" />
                                                                                        </td>
                                                                                        
                                                                                        <td class="hidden-480">
                                                                                           <?php echo $sr++;?>
                                                                                        </td>
											<td><?php echo $service->name?></td>
                                                                                        
											<td class="hidden-480">
                                                                                            <input type="text" name="position[<?php echo $service->id?>]" value="<?=$service->position;?>" size="3" style="width:20%;" />
                                                                                        </td>
											<td>
                                                                                             <div class="info-toggle-button tooltips <?php echo ($service->is_active=='1')?'status_on':'status_off';?>" id="<?php echo $service->id;?>" rel="<?php echo ($service->is_active=='1')?'on':'off';?>" title="Click here to change status" >
                                                                                                    <input type="checkbox" class="toggle" <?php echo ($service->is_active=='1')?'checked="checked"':'';?> />
                                                                                            </div>
                                                                                        </td>
											<td>
                                                                                            
                                                                                                <a class="btn blue icn-only tooltips" href="<?php echo make_admin_url('service', 'update', 'update', 'id='.$service->id)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                                                                            
                                                                                                <a target="_blank" class="btn green icn-only tooltips" href="<?php echo make_url_user('service_detail','id='.$service->id)?>" title="click here to view this record"><i class="icon-search icon-white"></i></a>&nbsp;&nbsp;
                                                                                                 
                                                                                                <a class="btn red icn-only tooltips" href="<?php echo make_admin_url('service', 'delete', 'list', 'id='.$service->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                                                                           
                                                                                        </td>
                                                                                
											

                                                                                    </tr>
                                                                             <?php endwhile;?>
                                                                           

									</tbody>
                                                                        <tfoot>
                                                                            <tr class="odd gradeX">
                                                                       
                                                                                <td class="hidden-480" colspan="3">
                                                                                    <div style=" width:220px;float:left">
                                                                                        <select  name="multiopt_action" style="width:150px" class="left_align regular">
                                                                                            <option value="delete">Delete</option>
                                                                                        </select>
                                                                                        <input style="float:right" type="submit" class="btn green large" name="multiopt_go" value="Go"  onclick="return confirm('Are you sure?');" />
                                                                                    </div>
                                                                                </td>
                                                                                <td colspan="3" class="hidden-480"><input type="submit" class="btn green" name="submit_position" value="Update" /></td>

                                                                            </tr> 
                                                                        </tfoot>
                                                                        <?php else:?>
                                                                         <tr class="odd gradeX">
                                                                             <td colspan="6">
                                                                                  
                                                                                <div style="text-align:center">  
                                                                                

                                                                                                    <a class="btn large grey" href="<?php echo make_admin_url('service', 'list', 'insert');?>">
                                                                                                       <img style="text-align:center" src="assets/img/upload_new_service.png"/>
                                                                                                   </a>    


                                                                                   <br/>
                                                                                    <div style="clear:both;height:10px">  </div>
                                                                                   <p>
                                                                                       No service /  product found, 
                                                                                       <a style="text-decoration:underline" class="black" href="<?php echo make_admin_url('service', 'list', 'insert');?>"> click to add new service /  product.</a>
                                                                                   </p>
                                                                                   <br/>
                                                                               </div>
                                                                                 
                                                                                 
                                                                                 
                                                                             </td>
                                                                         </tr>    
                                                                       <?php endif;?>  
								</table>
                                                              
                                                             </form>   
                                                              <div class="clearfix"></div>
                                                              <div class="btn-group pull-right" data-toggle="buttons-radio">
                                                               <a  href="<?php echo make_admin_url('service', 'thrash', 'thrash');?>" class="btn mini red pull-right"><i class="icon-red icon-trash"></i> &nbsp;view trash</a>
                                                             </div>
                                                             <div class="clearfix"></div>
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



