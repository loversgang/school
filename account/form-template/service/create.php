
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Services / Products
                            </h3>
                            <!--
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                   
                                    <li>
                                        <i class="icon-globe"></i>
                                               <a href="<?php echo make_admin_url('service', 'list', 'list');?>">List Services</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        New Service
                                    </li>

                            </ul>
                            -->

                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php # include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('service', 'update', 'list')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-globe"></i>New Service /  Product</div>
                                            <div class="tools">
                                                      <a target="_blank" href="<?php echo make_url_user('service')?>" style="color:white !important" class=""><i class="icon-search icon-white"></i> &nbsp;view services / products </a>


                                            </div>
                                           
                                    </div>
                                    <div class="portlet-body form">

                                             <div class="control-group">
                                                    <label class="control-label" for="name">Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="name" value="" id="name" class="span12 m-wrap validate[required]" />
                                                    </div>  
                                            </div>
                                           <!--
                                            <div class="control-group">
                                                    <label class="control-label" for="urlname">Urlname</label>
                                                    <div class="controls">
                                                       <input type="text" name="urlname" id="urlname"  value="" class="span6 m-wrap" />
                                                    
                                                    </div>  
                                            </div>-->
                                            <div class="control-group">
                                                    <label for="image" class="control-label">Image</label>
                                                    <div class="controls">
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                                                    </div>
                                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                    <div>
                                                                            <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                                            <span class="fileupload-exists">Change</span>
                                                                            <input type="file" id="image" name="image" class="default" /></span>
                                                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                                                    </div>
                                                            </div>
                                                           
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="short_description">About service / product in short<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <textarea id="short_description" class="span12 m-wrap validate[required]" name="short_description" rows="6"></textarea>
                                                       
                                                    </div>
                                             </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="long_description">About service / product in detail<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <textarea id="long_description" class="span12 ckeditor m-wrap" name="long_description" rows="6"></textarea>
                                                       
                                                    </div>
                                             </div>
                                           <!--
                                            <div class="control-group">
                                                    <label class="control-label" for="is_active">Show on Website</label>
                                                    <div class="controls">
                                                        <input type="checkbox" name="is_active" id="is_active" value="1"  />
                                                    </div>  
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="position">Position<span class="required">*</span></label>
                                                    <div class="controls">
                                                         <input style="width:10%" type="text" id="position"  name="position" value="" class="span6 m-wrap validate[required]" />
                                                    </div>  
                                            </div>-->
                                            <input type="hidden" name="is_active" id="is_active" value="1"  />  

                                            <br/>
                                            <h3>SEO Information</h3>
                                            <div class="control-group">
                                                   <label class="control-label" for="meta_name">Meta Title</label>
                                                   <div class="controls">
                                                       <input type="text" name="meta_name" id="meta_name"  value="" class="span12 m-wrap" /> 
                                           
                                                   </div>  
                                            </div>  
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_keyword">Meta Keywords</label>
                                                    <div class="controls">
                                                    <input type="text" name="meta_keyword" id="meta_keyword"  value="" class="span12 m-wrap" />
                                           
                                                    </div>
                                            </div>          
                                           <div class="control-group">
                                                    <label class="control-label" for="meta_description">Meta Description</label>
                                                    <div class="controls">
                                            
                                                       <textarea rows="3" class="span12 m-wrap"  style=" height: 82px;" id="meta_description"  name="meta_description"></textarea>
                                          
                                                    </div>
                                           </div>           
                                        
                                        
                                
                                            <div class="form-actions">
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('service', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                                    
                                            </div>
                                         
                                
                                  
                              </div> 
                            </div>
                        </div>
                          
                             
                           

                     </form>
                 
                   
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



