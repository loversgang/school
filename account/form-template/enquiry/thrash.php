
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Enquiries
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
                                    <li>
                                            <i class="icon-list"></i>
                                            <a href="<?php echo make_admin_url('enquiry', 'list', 'list');?>">All Enquiries</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 
                                    <li class="last">
                                        All Dead Enquiries
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut_list.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-male"></i>All Dead Enquiries</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
												<th>Sr. No.</th>
												<th class="hidden-480">Name</th>
												<th class="hidden-480">Father Name</th>
												<th style='text-align:center' class="hidden-480">On Date</th>
												<th class="hidden-480 sorting_disabled">City</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Mobile NO.</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Status</th>
												<th style='text-align:center'>Action</th>
										</tr>
									</thead>
                                        <? if($QueryObj->GetNumRows()!=0): $sr='1';?>
										<tbody>
                                            <?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
                                                <tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td class="hidden-480"><?=$object->name?></td>
													<td class="hidden-480"><?=$object->father_name?></td>
													<td style='text-align:center' class="hidden-480"><?=date('d M, Y',strtotime($object->on_date))?></td>
													<td class="hidden-480"><?=$object->city?></td>														
													<td style='text-align:center' class="hidden-480"><?=$object->phone?></td>
													<td style='text-align:center' class="hidden-480">
														<button class='btn red mini'><?=ucfirst($object->status)?></button>
													</td>
													<td style='text-align:right;'>
													<a class="mini btn blue icn-only tooltips" href="<?php echo make_admin_url('enquiry', 'update', 'update', 'id='.$object->id)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
													<a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('enquiry', 'delete', 'delete', 'id='.$object->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete record"><i class="icon-remove icon-white"></i></a>
													</td>												
												</tr>
                                            <?php $sr++;
												endwhile;?>
										</tbody>
									   <?php endif;?>  
								</table>                             
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER--> 
