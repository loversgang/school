<div class="tiles pull-right">

        <div class="tile bg-green <?php echo ($section=='list' && $type=='confirm')?'selected':''?>">
            <a href="<?php echo make_admin_url('enquiry', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Confirm Enquiries
                        </div>
                </div>
            </a>   
        </div>


        <div class="tile bg-blue <?php echo ($section=='list' && $type=='live')?'selected':''?>">
            <a href="<?php echo make_admin_url('enquiry', 'list', 'list&type=live');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-certificate"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Live Enquiries
                        </div>
                </div>
            </a>   
        </div>

           <div class="tile bg-red <?php echo ($section=='thrash')?'selected':''?>">
            <a href="<?php echo make_admin_url('enquiry', 'thrash', 'thrash');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-trash"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Dead Enquiries
                        </div>
                </div>
            </a> 
        </div>    

</div>