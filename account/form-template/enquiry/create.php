
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Enquiries
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('enquiry', 'list', 'list');?>">List Enquiries</a>
                                         <i class="icon-angle-right"></i>                                       
                                    </li>
                                    <li class="last">
                                        Add New Enquiry
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
		  
			  
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('enquiry', 'insert', 'insert')?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-male"></i>Add New Enquiry</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">      
									<h4 class="form-section hedding_inner">Basic Information</h4>
										<div class="row-fluid">
										<div class="span6 ">
 											<div class="control-group">
  	                                        <label class="control-label" for="name">Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="name" value="" id="name" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 	
 											<div class="control-group">
  	                                        <label class="control-label" for="course">Course<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="course" value="" id="course" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 	
											<div class="control-group">
  	                                        <label class="control-label" for="city">City<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="city" value="" id="city" class="span10 m-wrap validate[required]" />
													</div>
                                            </div> 	
											
 											<div class="control-group">
  	                                        <label class="control-label" for="phone">Mobile Phone<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="phone" value="" id="phone" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>												
											
											<div class="control-group">
  	                                        <label class="control-label" for="on_date">On Date <span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="on_date" value="" id="on_date" class="span10 m-wrap upto_current_date validate[required]"/>
                                                    </div>
                                            </div>	
 											<div class="control-group">
  	                                        <label class="control-label" for="status">Status</label>
                                                    <div class="controls">
                                                      <select name="status" id="status" class="span10 m-wrap">
														<option value='live'>Live</option>
														<option value='confirm'>Confirm</option>
														<option value='dead'>Dead</option>
													  </select>
                                                    </div>
                                            </div>	
										</div>	
										<div class="span6 ">
 											<div class="control-group">
  	                                        <label class="control-label" for="father_name">Father Name <span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="father_name" value="" id="father_name" class="span10 m-wrap validate[required]" />
													</div>
                                            </div> 
											<div class="control-group">
  	                                        <label class="control-label" for="detail">Remarks</label>
                                                    <div class="controls">
                                                      <textarea name="detail" rows='3' value="" id="detail" class="span10 m-wrap"></textarea>	
													</div>
                                            </div> 											
 											<div class="control-group">
  	                                        <label class="control-label" for="email">Email<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="email" value="" id="email" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>		  
 											<div class="control-group">
  	                                        <label class="control-label" for="address">Address<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <textarea name="address" rows='3' value="" id="address" class="span10 m-wrap validate[required]"></textarea>
                                                    </div>
                                            </div>											
 											
											

										</div>	
										</div>	

									<h4 class="form-section hedding_inner">Reference Detail</h4>
										<div class="row-fluid">
											<div class="span6 ">
												<div class="control-group">
												<label class="control-label" for="name">By Reference</label>
														<div class="controls">
															<select type="text" name="ref_name" id="ref_name" class="span10 m-wrap">
																<option value='Pamphlets'>Pamphlets</option>
																<option value='Poster'>Poster</option>
																<option value='Wall Painting'>Wall Painting</option>
																<option value='Newspaper'>Newspaper</option>
																<option value='Friends'>Friends</option>
																<option value='Parents'>Parents</option>
																<option value='Other' >Other</option>
															</select>
														</div>
												</div> 	
											</div>
											<div class="span6 ">
												<div class="control-group">
												<label class="control-label" for="reference_detail">Reference Remarks</label>
														<div class="controls">
														  <textarea name="reference_detail" rows='3' value="" id="reference_detail" class="span10 m-wrap"></textarea>	
														</div>
												</div> 
											</div>											
										</div>
 										
                                            <div class="form-actions">
													 <input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" /> 	
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('enquiry', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
                              </div>
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
                
	     </div>

             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    
