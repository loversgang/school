<div class="tiles pull-right">

        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('enquiry', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                List Enquiries
                        </div>
                </div>
            </a>   
        </div>

        <div class="tile bg-blue <?php echo ($section=='insert')?'selected':''?>">
            <a href="<?php echo make_admin_url('enquiry', 'insert', 'insert');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                New Enquiry
                        </div>
                </div>
            </a> 
        </div>

</div>