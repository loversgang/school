<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Loan
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('loan', 'list', 'list'); ?>">List Loan</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add Loan
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal"  method="POST"  id="validation">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Add New Loan</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="control-group">
                            <label class="control-label" for="loan_name">Amount<span class="required">*</span></label>
                            <div class="controls">
                                <input type="number" name="amount"  value="" id="loan_name" class="span6 m-wrap validate[required]" />
                            </div>
                        </div>        
                        <div class="control-group">
                            <label class="control-label" for="type">Type<span class="required"></span></label>
                            <div class="controls">
                                <input type="text" name="type" value="" class="span6 m-wrap validate[required]" />
                            </div>

                        </div>  
                        <div class="control-group">
                            <label class="control-label" for="description">Installments<span class="required"></span></label>
                            <div class="controls">
                                <select name="installment" id="installment"  class="span6 m-wrap validate[required]">
                                <option value="monthly">Monthly</option>
                                <!--<option value="yearly">Yearly</option>-->
                                </select>
                            </div>
                        </div>  
                        <div id="display_months"></div>
                        <div class="control-group">
                            <label class="control-label" for="total_installments">Total Installments<span class="required"></span></label>
                            <div class="controls">
                                <input type="number" name="total_installments" class="span6 m-wrap validate[required]" id="total_installments" />
                            </div>
                        </div>  
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                            <input type="hidden" name="staff_id" value="<?= $staff_id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('loan', 'list', 'list&staff_id=' . $staff_id); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<script>
    $(document).ready(function () {
        $('#installment').change();
    });

    $(document).on('change', '#installment', function () {
        if ($(this).val() == 'monthly') {
            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=loan'); ?>', {action: 'monthly'}, function (data) {
                $('#display_months').html(data);
            });
        } else {
            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=loan'); ?>', {action: 'yearly'}, function (data) {
                $('#display_months').html(data);
            });
        }
    });

//    $(document).on('keyup', '#total_installments', function () {
//        var total_installments = $(this).val();
//        var installment = $('#installment').val();
//        if (installment == 'yearly') {
//            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=loan'); ?>', {action: 'yearly', total_installments: total_installments}, function (data) {
//                $('#display_months').html(data);
//            });
//        }
//    });
</script>