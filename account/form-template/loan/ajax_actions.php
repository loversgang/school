<?php
include_once(DIR_FS_SITE . 'include/functionClass/loanClass.php');

if (isset($_POST['action'])) {
    extract($_POST);
    ?> 

    <?php if ($action == 'monthly') { ?>
        <div class="control-group">
            <label class="control-label" for="description">Start Month<span class="required"></span></label>
            <div class="controls">
                <select name="start_time" id="months"  class="span6 m-wrap validate[required]">
                    <?php for ($x = 1; $x <= 12; $x++) { ?>
                        <?php $month = date('F', mktime(0, 0, 0, $x, 1, date('Y'))); ?>
                        <option value="<?php echo $month ?>">
                            <?php echo $month ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
            <div class="control-group">
                <label class="control-label" for="description">Start Year<span class="required"></span></label>
                <div class="controls">
                    <select name="start_year" id="start_year"  class="span6 m-wrap validate[required]">
                        <?php
                        $year = date('Y');
                        for ($x = 1; $x <= 20; $x++) {
                            $y = $year++
                            ?>
                            <option value="<?php echo $y; ?>">
                                <?php
                                echo $y;
                                ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
    <?php } ?>
    <?php if ($action == 'yearly') { ?>
        <div class="control-group">
            <label class="control-label" for="description">Start Year<span class="required"></span></label>
            <div class="controls">
                <select name="start_time" id="months"  class="span6 m-wrap validate[required]">
                    <?php
                    $year = date('Y');
                    for ($x = 1; $x <= 20; $x++) {
                        $y = $year++
                        ?>
                        <option value="<?php echo $y; ?>">
                            <?php
                            echo $y;
                            ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
    <?php } ?>


    <?php if ($action == 'monthly_edit') { ?>
        <div class="control-group">
            <label class="control-label" for="description">Select Start Date<span class="required"></span></label>
            <div class="controls">
                <div class="span12">
                    <select name="start_time" id="months"  class="span6 m-wrap validate[required]">
                        <?php for ($x = 1; $x <= 12; $x++) { ?>
                            <?php $month = date('F', mktime(0, 0, 0, $x, 1, date('Y'))); ?>
                            <option value="<?php echo $month ?>" <?php echo $month == $data_month ? 'selected' : '' ?>>
                                <?php echo $month ?>
                            </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if ($action == 'yearly_edit') { ?>
        <div class="control-group">
            <label class="control-label" for="description">Select Start Year<span class="required"></span></label>
            <div class="controls">
                <select name="start_time" id="months"  class="span6 m-wrap validate[required]">
                    <?php
                    $year = date('Y');
                    for ($x = 1; $x <= 20; $x++) {
                        $y = $year++
                        ?>
                        <option value="<?php echo $y; ?>" <?php echo $y == $data_month ? 'selected' : '' ?>>
                            <?php
                            echo $y;
                            ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
    <?php } ?>
<?php } ?>