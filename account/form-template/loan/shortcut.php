<div class="tile bg-blue <?php echo ($section == 'insert') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('loan', 'insert', 'insert&staff_id=' . $staff_id) ?>">
        <div class="corner"></div>
        <div class="tile-body">
            <i class="icon-plus"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                Add New Loan
            </div>
        </div>
    </a> 
</div>
<div class="tile bg-purple <?php echo ($section == 'list') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('loan', 'list', 'list&staff_id=' . $staff_id) ?>">
        <div class="corner"></div>
        <div class="tile-body">
            <i class="icon-plus"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                List Loan
            </div>
        </div>
    </a> 
</div>
<div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('staff', 'list', 'list') ?>">
        <div class="corner"></div>
        <div class="tile-body">
            <i class="icon-plus"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                List Staff
            </div>
        </div>
    </a> 
</div>