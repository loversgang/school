<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Staff
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li class="last">
                    List Staff
                </li>
            </ul>
        </div>
    </div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Manage Loan</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form method="POST" id="form_data">	
                        <div style="overflow-y:hidden">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th>Sr. No</th>
                                        <th>Amount</th>
                                        <th>Title</th>
                                        <th>Installments</th>
                                        <th>Start Date</th>
                                        <th>Start Year</th>
                                        <th>Total Installments</th>
                                        <th>Installments Paid</th>
                                        <th >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($list_loans as $key => $loan) { ?>
                                        <tr class="odd gradeX">	
                                            <td><?php echo $key+=1 ?></td>				
                                            <td><?php echo $loan->amount ?></td>				
                                            <td><?php echo ucfirst($loan->type) ?></td>				
                                            <td>
                                                <?php echo ucfirst($loan->installment) ?>
                                            </td>				
                                            <td><?php echo ucfirst($loan->start_time) ?></td>				
                                            <td><?php echo ucfirst($loan->start_year) ?></td>				
                                            <td><?php echo ucfirst($loan->total_installments) ?></td>				
                                            <td><?php echo ucfirst($loan->installments_paid) ?></td>				
                                            <td>
                                                <div class="span6">
                                                    <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('loan', 'update', 'update&staff_id=' . $staff_id, 'id=' . $loan->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>
                                                </div>
                                                <div class="span6">
                                                    <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('loan', 'delete', 'delete&staff_id=' . $staff_id, 'id=' . $loan->id) ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</div>