<?
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/vehicleClass.php');

if($_POST):
isset($_POST['id'])?$id=$_POST['id']:$id='';		
		$current_vehicle=get_object('school_vehicle',$id);?>
		
					<div class="span6 "> 									
						<div class="control-group">
								<label class="control-label" for="ref_name1">Vehicle Make</label>
								<div class="controls">
								  <input type="text" value="<? if(is_object($current_vehicle)){ echo $current_vehicle->make; }?>" id="ref_name1" class="span10 m-wrap" disabled="disabled"/>
								</div>
						</div> 
					</div>				
					<div class="span6 ">								
						<div class="control-group">
								<label class="control-label" for="ref_phone1">Vehicle Number</label>
								<div class="controls">
								  <input type="text" value="<? if(is_object($current_vehicle)){ echo $current_vehicle->vehicle_number; }?>" id="ref_phone1" class="span10" disabled="disabled"/>
								</div>
						</div>	
					</div>		
					<div class="span6 ">
						<div class="control-group">
								<label class="control-label" for="ref_phone">Vehicle Route</label>
								<div class="controls">
								  <textarea  class="span10 m-wrap" disabled="disabled"><? if(is_object($current_vehicle)){ echo $current_vehicle->route_map;}?></textarea>
								</div>
						</div>
					</div>						
<?		
endif;
?>