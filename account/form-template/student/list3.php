
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Students
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    List Student
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Manage Students</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('student', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >		
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>																								
                                    <th>Sr. No.</th>
                                    <th>Reg ID</th>
                                    <th style='width:20%;'>Name</th>
                                    <th class="hidden-480 sorting_disabled">Father Name</th>
                                    <th class="hidden-480 sorting_disabled">Current Session</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <? if (!empty($school_students)): ?>
                                <tbody>
                                    <?php
                                    $sr = 1;
                                    foreach ($school_students as $s_k => $object):
                                        $CurrObj = new studentSession();
                                        $current_session = $CurrObj->getStudentCurrentSession($school->id, $object->id);
                                        ?>
                                        <tr class="odd gradeX">	
                                            <td><?= $sr ?>.</td>
                                            <td><?= $object->reg_id ?></td>
                                            <td><?= $object->first_name . " " . $object->last_name ?></td>
                                            <td class="hidden-480 sorting_disabled"><?= $object->father_name ?></td>
                                            <td><?= $current_session ?></td>													
                                            <td style='text-align:right;'>
                                                <a class="btn mini purple icn-only tooltips" href="<?php echo make_admin_url('view', 'list', 'list', 'id=' . $object->id) ?>" title="click here to see the full profile">Profile</a>&nbsp;&nbsp;
                                                <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('student', 'update', 'update', 'id=' . $object->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                                <!--
                                                <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('student', 'delete', 'delete', 'id=' . $object->id . '&delete=1') ?>" onclick="return confirm('Are you sure? All the student information will be deleted permanently.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                                -->
                                            </td>
                                        </tr>
                                    <?php $sr++;
                                endforeach;
                                ?>
                                </tbody>
<?php endif; ?>  
                        </table>
                    </form>    
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



