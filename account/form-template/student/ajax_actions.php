<?php
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsEmailClass.php');
include_once(DIR_FS_SITE . 'include/config_account/constant_account.php');
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'get_sessions_list') {
        $dates = explode(',', $sess_int);
        $sess_to = date('Y', strtotime($dates[1]));
        $sess_from = date('Y', strtotime($dates[0]));
        $session_int_array = array($sess_to, $sess_from);
        $session_int = implode('-', $session_int_array);
        $obj = new session;
        $All_Session = $obj->listOfYearSession($school->id, $session_int, 'object');
        ?>
        <label class="control-label" style='width:auto;'>Select Session Class</label>
        <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
            <option value="0">ALL</option>
            <?php
            if (!empty($All_Session)) {
                if (empty($session_id) && !empty($All_Session)):
                    $session_id = $All_Session['0']->id;
                endif;
                $session_sr = 1;
                foreach ($All_Session as $key => $session):
                    ?>
                    <option value='<?= $session->id ?>' <?
                    if ($session->id == $session_id):
                        echo 'selected';
                    endif;
                    ?> ><?= ucfirst($session->title) ?></option>
                            <?
                            $session_sr++;
                        endforeach;
                    }
                    ?>
        </select>
        <?php
    }
    if ($act == 'stop_concession') {
        $arr['id'] = $rel_id;
        $arr['student_id'] = $student_id;
        $arr['concession'] = $concession;
        $arr['concession_type'] = $concession_type;
        $arr['amount'] = $amount;
        $arr['start_concession'] = 0;
        $obj = new studentSession;
        $obj->saveData($arr);
        echo "success";
    }
    if ($act == 'start_concession') {
        $arr['id'] = $rel_id;
        $arr['student_id'] = $student_id;
        $arr['concession'] = $concession;
        $arr['concession_type'] = $concession_type;
        $arr['amount'] = $amount;
        $arr['start_concession'] = 1;
        $obj = new studentSession;
        $obj->saveData($arr);
        echo "success";
    }

    if ($act == 'send_login_sms') {
#Send and Add Entry to sms counter if set in setting
        if ($school_setting->is_sms_allowed == '1') {
            $obj = new student;
            $student = $obj->getStudent($student_id);

            $object = new studentFamily;
            $parents = $object->getStudentFamilyDeatils($student_id);

            $arr['NAME'] = $student->first_name . ' ' . $student->last_name;
            $arr['ID'] = $student->student_login_id;
            $arr['PASS'] = $student->password;

            $msg = get_database_msg_only('21', $arr);
            $POST['school_id'] = $school->id;
            $POST['sms_type'] = 'login_details';
            $POST['student_id'] = $student_id;
            $POST['type'] = 'sms';
            $POST['on_date'] = date('Y-m-d');
            $POST['to_number'] = $parents->mobile;
            $POST['sms_text'] = $msg;
            $QuerySave = new schoolSmsEmailHistory();
            $sms_history_id = $QuerySave->saveData($POST);

// Send SMS
            $send_sms = send_sms($parents->mobile, $msg);
            if ($send_sms == '1') {
                $sms_arr['id'] = $sms_history_id;
                $sms_arr['sms_send_status'] = '1';
                $sms_arr['sms_send_date'] = date('Y-m-d');
                $QueryUpd = new schoolSmsEmailHistory();
                $QueryUpd->saveData($sms_arr);
                echo "success";
            } else {
                echo "failed";
            }
        } else {
            echo "disabled";
        }
    }

    if ($act == 'send_login_email') {
        #Send and Add Entry to email counter if set in setting
        if ($school_setting->is_email_allowed == '1') {
            $obj = new student;
            $student = $obj->getStudent($student_id);

            $object = new studentFamily;
            $parents = $object->getStudentFamilyDeatils($student_id);

            $arr['NAME'] = $student->first_name . ' ' . $student->last_name;
            $arr['ID'] = $student->student_login_id;
            $arr['PASS'] = $student->password;
            $arr['EMAIL'] = $school->email_address;
            $arr['SITE_NAME'] = SITE_NAME;
            $msg = get_database_msg('22', $arr);
            $subject = get_database_msg_subject('22', $arr);
            $POST['school_id'] = $school->id;
            $POST['email_type'] = 'login_details';
            $POST['type'] = 'email';
            $POST['on_date'] = date('Y-m-d');
            $POST['email_subject'] = $subject;
            $POST['to_email'] = $parents->email;
            $POST['email_text'] = $msg;
            $POST['from_email'] = $school->email_address;
            $POST['email_school_name'] = $school->school_name;
            $QuerySave = new schoolSmsEmailHistory();
            $email_history_id = $QuerySave->saveData($POST);

            // Send Login Email
            $send_email = send_email_by_cron($parents->email, $msg, $subject, $school->school_name);
            if ($send_email == '1') {
                $email_arr['id'] = $email_history_id;
                $email_arr['email_send_status'] = '1';
                $email_arr['sms_email_date'] = date('Y-m-d');
                $QueryUpd = new schoolSmsEmailHistory();
                $QueryUpd->saveData($email_arr);
                echo "success";
            } else {
                echo "failed";
            }
        } else {
            echo "disabled";
        }
    }
}    
