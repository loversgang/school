<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Students
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>">List Students</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add New Student
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
    </div>	            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <div id="form_wizard_1" class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-reorder"></i> Add New Student </span>
                    </div>
                    <div class="tools hidden-phone">
                        <a class="collapse" href="javascript:;"></a>
                        <a class="config" data-toggle="modal" href="#portlet-config"></a>
                        <a class="reload" href="javascript:;"></a>
                        <a class="remove" href="javascript:;"></a>
                    </div>
                </div>
                <?
                if ($type == 'student_detail'):
                    $per = '1';
                elseif ($type == 'address'):
                    $per = '14';
                elseif ($type == 'family'):
                    $per = '28';
                elseif ($type == 'previous_school'):
                    $per = '42';
                elseif ($type == 'document'):
                    $per = '56';
                elseif ($type == 'admission'):
                    $per = '70';
                elseif ($type == 'vehicle'):
                    $per = '84';
                else:
                    $per = '14';
                endif;
                ?>							
                <div class="portlet-body form">
                    <div class="form-wizard">
                        <div class="navbar steps">
                            <div class="navbar-inner">
                                <ul class="row-fluid nav nav-pills">
                                    <li class="span2 <?= ($type == 'student_detail') ? 'active' : ''; ?>" style='margin-left:0px;width:14%;'>
                                        <a class="step <?= ($type == 'student_detail') ? 'active' : ''; ?>" href="<? //=make_admin_url('student','update','update&type=student_detail&s_id='.$s_id);      ?>">
                                            <span class="number">1</span>
                                            <span class="desc"><i class="icon-ok"></i>Information</span>   
                                        </a>
                                    </li>
                                    <li class="span2 <?= ($type == 'address') ? 'active' : ''; ?>" style='margin-left:0px;width:14%;'>
                                        <a class="step <?= ($type == 'address') ? 'active' : ''; ?>" href="<? //=make_admin_url('student','update','update&type=address&s_id='.$s_id);      ?>">
                                            <span class="number">2</span>
                                            <span class="desc"><i class="icon-ok"></i>Address</span>   
                                        </a>
                                    </li>
                                    <li class="span2 <?= ($type == 'family') ? 'active' : ''; ?>" style='margin-left:0px;width:14%;'>
                                        <a class="step <?= ($type == 'family') ? 'active' : ''; ?>" href="<? //=make_admin_url('student','update','update&type=family&s_id='.$s_id);      ?>">
                                            <span class="number">3</span>
                                            <span class="desc"><i class="icon-ok"></i>Family Detail</span>   
                                        </a>
                                    </li>
                                    <li class="span2 <?= ($type == 'previous_school') ? 'active' : ''; ?>" style='margin-left:0px;'>
                                        <a class="step <?= ($type == 'previous_school') ? 'active' : ''; ?>" href="<? //=make_admin_url('student','update','update&type=previous_school&s_id='.$s_id);      ?>">
                                            <span class="number">4</span>
                                            <span class="desc"><i class="icon-ok"></i> Previous School</span>   
                                        </a> 
                                    </li>
                                    <li class="span2 <?= ($type == 'document') ? 'active' : ''; ?>" style='margin-left:0px;width:14%;'>
                                        <a class="step <?= ($type == 'document') ? 'active' : ''; ?>" href="<? //=make_admin_url('student','update','update&type=document&s_id='.$s_id);      ?>">
                                            <span class="number">5</span>
                                            <span class="desc"><i class="icon-ok"></i> Documents</span>   
                                        </a> 
                                    </li>	
                                    <li class="span2 <?= ($type == 'admission') ? 'active' : ''; ?>" style='margin-left:0px;width:14%;'>
                                        <a class="step <?= ($type == 'admission') ? 'active' : ''; ?>" href="<? //=make_admin_url('student','update','update&type=admission&s_id='.$s_id);      ?>">
                                            <span class="number">6</span>
                                            <span class="desc"><i class="icon-ok"></i> Admission</span>   
                                        </a> 
                                    </li>	
                                    <li class="span2 <?= ($type == 'vehicle') ? 'active' : ''; ?>" style='margin-left:0px;'>
                                        <a class="step <?= ($type == 'vehicle') ? 'active' : ''; ?>" href="<? //=make_admin_url('student','update','update&type=admission&s_id='.$s_id);      ?>">
                                            <span class="number">7</span>
                                            <span class="desc"><i class="icon-ok"></i>Assign Vehicle</span>   
                                        </a> 
                                    </li>														
                                </ul>
                            </div>
                        </div>

                        <div class="progress progress-success progress-striped" id="bar">
                            <div class="bar" style="width: <?= $per ?>%;"></div>
                        </div>
                        <div class="tab-content">
                            <div class="alert alert-error hide">
                                <button data-dismiss="alert" class="close"></button>
                                You have some form errors. Please check below.
                            </div>
                            <div class="alert alert-success hide">
                                <button data-dismiss="alert" class="close"></button>
                                Your form validation is successful!
                            </div>

                            <!-- First Step  Student Detail-->
                            <div id="tab1" class="tab-pane <?= ($type == 'student_detail') ? 'active' : ''; ?>">
                                <form class="form-horizontal" action="<?php echo make_admin_url('student', 'insert', 'insert&type=student_detail') ?>" method="POST" enctype="multipart/form-data" id="validation">
                                    <h3 class="block">Provide Basic Details</h3>
                                    <h4 class="form-section hedding_inner">Basic Information</h4>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="reg_id">Registration ID<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="reg_id" value="<?
                                                    if (isset($_POST['reg_id'])) {
                                                        echo $_POST['reg_id'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->reg_id;
                                                    } else {
                                                        echo $prefix;
                                                    }
                                                    ?>" id="reg_id" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div> 														
                                            <div class="control-group">
                                                <label class="control-label" for="first_name">First Name<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="first_name" value="<?
                                                    if (isset($_POST['first_name'])) {
                                                        echo $_POST['first_name'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->first_name;
                                                    }
                                                    ?>" id="first_name" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="sex">Gender<span class="required">*</span></label>
                                                <div class="controls">
                                                    <select name="sex" id="school_type"  class="span10 m-wrap ">
                                                        <option value="Male" <?
                                                        if (isset($_POST['sex']) && $_POST['sex'] == 'Male') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->sex == 'Male') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Male</option>
                                                        <option value="Female" <?
                                                        if (isset($_POST['sex']) && $_POST['sex'] == 'Female') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->sex == 'Female') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Female</option>																													
                                                    </select>                      
                                                </div>
                                            </div> 
                                            <div class="control-group">
                                                <label class="control-label" for="cast">Cast</label>
                                                <div class="controls">
                                                    <input type="text" name="cast" value="<?
                                                    if (isset($_POST['cast'])) {
                                                        echo $_POST['cast'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->cast;
                                                    }
                                                    ?>" id="cast" class="span10 m-wrap"/>
                                                </div>
                                            </div>		
                                            <div class="control-group">
                                                <label class="control-label" for="category">Category</label>
                                                <div class="controls">
                                                    <select name="category" id="school_type"  class="span10 m-wrap ">
                                                        <option>None</option>
                                                        <option value="GEN" <?
                                                        if (isset($_POST['category']) && $_POST['category'] == 'GEN') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->category == 'GEN') {
                                                            echo 'selected';
                                                        }
                                                        ?>>GEN</option>
                                                        <option value="OBC" <?
                                                        if (isset($_POST['category']) && $_POST['category'] == 'OBC') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->category == 'OBC') {
                                                            echo 'selected';
                                                        }
                                                        ?>>OBC</option>	
                                                        <option value="SC" <?
                                                        if (isset($_POST['category']) && $_POST['category'] == 'SC') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->category == 'SC') {
                                                            echo 'selected';
                                                        }
                                                        ?>>SC</option>	
                                                        <option value="ST" <?
                                                        if (isset($_POST['category']) && $_POST['category'] == 'ST') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->category == 'ST') {
                                                            echo 'selected';
                                                        }
                                                        ?>>ST</option>	
                                                        <option value="OTHER" <?
                                                        if (isset($_POST['category']) && $_POST['category'] == 'OTHER') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->category == 'OTHER') {
                                                            echo 'selected';
                                                        }
                                                        ?>>OTHER</option>																				
                                                    </select> 
                                                </div>
                                            </div>															
                                            <div class="control-group">
                                                <label class="control-label" for="email">Email Address</label>
                                                <div class="controls">
                                                    <input type="text" name="email" value="<?
                                                    if (isset($_POST['email'])) {
                                                        echo $_POST['email'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->email;
                                                    }
                                                    ?>" id="email" class="span10 m-wrap"/>
                                                </div>
                                            </div> 

                                            <div class="control-group">
                                                <label class="control-label" for="image">Photo</label>
                                                <div class="controls">
                                                    <?
                                                    if (is_object($s_info) && $s_info->photo):
                                                        $im_obj = new imageManipulation()
                                                        ?>
                                                        <div class="item span10" style='text-align:center;'>	
                                                            <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                                <div class="zoom">
                                                                    <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                                    <div class="zoom-icon"></div>
                                                                </div>
                                                            </a>
                                                            <div class="details">
                                                                <a href="<?= make_admin_url('student', 'delete_image', 'delete_image&s_id=' . $s_id . '&type=student_detail'); ?>" class="icon" onclick="return confirm('Are you sure? You are deleting this image.');" title="click here to delete this image"><i class="large icon-remove"></i></a>    
                                                            </div>	
                                                        </div>															
                                                    <? else: ?>
                                                        <input type="file" name="image"  id="image" class="span10 m-wrap"/>
                                                    <? endif; ?> 
                                                </div>
                                            </div> 														
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="date_of_admission">Registration Date<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="date_of_admission" value="<?
                                                    if (isset($_POST['date_of_admission'])) {
                                                        echo $_POST['date_of_admission'];
                                                    } elseif (is_object($s_info)) {
                                                        echo date('m/d/Y', strtotime($s_info->date_of_admission));
                                                    }
                                                    ?>" id="date_of_admission" class='span10 m-wrap m-ctrl-medium upto_current_date validate[required]'/>
                                                </div>
                                            </div> 															
                                            <div class="control-group">
                                                <label class="control-label" for="last_name">Last Name</label>
                                                <div class="controls">
                                                    <input type="text" name="last_name" value="<?
                                                    if (isset($_POST['last_name'])) {
                                                        echo $_POST['last_name'];
                                                    } elseif (isset($_POST['cast'])) {
                                                        echo $_POST['cast'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->last_name;
                                                    }
                                                    ?>" id="last_name" class="span10 m-wrap"/>
                                                </div>
                                            </div>		
                                            <div class="control-group">
                                                <label class="control-label" for="date_of_birth">Date Of Birth<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="date_of_birth" value="<?
                                                    if (isset($_POST['date_of_birth'])) {
                                                        echo $_POST['date_of_birth'];
                                                    } elseif (is_object($s_info)) {
                                                        echo date('Y-m-d', strtotime($s_info->date_of_birth));
                                                    }
                                                    ?>" id="date_of_birth" class='span10 m-wrap m-ctrl-medium upto_current_date validate[required]'/>
                                                </div>
                                            </div> 	  											
                                            <div class="control-group">
                                                <label class="control-label" for="religion">Religion</label>
                                                <div class="controls">
                                                    <input type="text" name="religion" value="<?
                                                    if (isset($_POST['religion'])) {
                                                        echo $_POST['religion'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->religion;
                                                    }
                                                    ?>" id="religion" class="span10 m-wrap"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="phone">Mobile Phone</label>
                                                <div class="controls">
                                                    <input type="text" name="phone" value="<?
                                                    if (isset($_POST['phone'])) {
                                                        echo $_POST['phone'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->phone;
                                                    }
                                                    ?>" id="phone" class="span10 m-wrap"/>
                                                </div>
                                            </div> 	  
                                            <div class="control-group">
                                                <label class="control-label" for="course_of_admission">Course Of Admission<span class="required">*</span></label>
                                                <div class="controls">
                                                    <select name="course_of_admission" id="designation"  class="span10 m-wrap ">
                                                        <?php
                                                        $sr = 1;
                                                        while ($sub = $QueryCourse->GetObjectFromRecord()):
                                                            ?>
                                                            <option value='<?= $sub->course_name ?>' <?
                                                            if (isset($_POST['course_of_admission']) && $_POST['course_of_admission'] == $sub->course_name) {
                                                                echo 'selected';
                                                            } elseif (is_object($s_info) && $s_info->course_of_admission == $sub->course_name) {
                                                                echo 'selected';
                                                            }
                                                            ?>><?= ucfirst($sub->course_name) ?></option>
                                                                    <?
                                                                    $sr++;
                                                                endwhile;
                                                                ?>	
                                                    </select>  																	  
                                                </div>
                                            </div> 	 															
                                        </div>	
                                    </div>

                                    <!-- Health Detail -->
                                    <h4 class="form-section hedding_inner">Health Detail</h4>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="blood_group">Blood Group</label>
                                                <div class="controls">
                                                    <input type="text" name="blood_group" value="<?
                                                    if (isset($_POST['blood_group'])) {
                                                        echo $_POST['blood_group'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->blood_group;
                                                    }
                                                    ?>" id="blood_group" class="span10 m-wrap"/>
                                                </div>
                                            </div> 														
                                            <div class="control-group">
                                                <label class="control-label" for="height">Height</label>
                                                <div class="controls">
                                                    <input type="text" name="height" value="<?
                                                    if (isset($_POST['height'])) {
                                                        echo $_POST['height'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->height;
                                                    }
                                                    ?>" id="height" class="span10 m-wrap"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="eye_right">EYE Right</label>
                                                <div class="controls">
                                                    <input type="text" name="eye_right" value="<?
                                                    if (isset($_POST['eye_right'])) {
                                                        echo $_POST['eye_right'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->eye_right;
                                                    }
                                                    ?>" id="vision" class="span10 m-wrap"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="family_doctor">Family Doctor Name </label>
                                                <div class="controls">
                                                    <input type="text" name="family_doctor" value="<?
                                                    if (isset($_POST['family_doctor'])) {
                                                        echo $_POST['family_doctor'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->family_doctor;
                                                    }
                                                    ?>" id="family_doctor" class="span10 m-wrap"/>
                                                </div>
                                            </div>			
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="allergies">Allergies</label>
                                                <div class="controls">
                                                    <input type="text" name="allergies" value="<?
                                                    if (isset($_POST['allergies'])) {
                                                        echo $_POST['allergies'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->allergies;
                                                    }
                                                    ?>" id="allergies" class='span10 m-wrap'/>
                                                </div>
                                            </div> 															
                                            <div class="control-group">
                                                <label class="control-label" for="weight">Weight</label>
                                                <div class="controls">
                                                    <input type="text" name="weight" value="<?
                                                    if (isset($_POST['weight'])) {
                                                        echo $_POST['weight'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->weight;
                                                    }
                                                    ?>" id="weight" class="span10 m-wrap"/>
                                                </div>
                                            </div>		
                                            <div class="control-group">
                                                <label class="control-label" for="eye_left">EYE Left</label>
                                                <div class="controls">
                                                    <input type="text" name="eye_left" value="<?
                                                    if (isset($_POST['eye_left'])) {
                                                        echo $_POST['eye_left'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->eye_left;
                                                    }
                                                    ?>" id="eye_left" class='span10 m-wrap'/>
                                                </div>
                                            </div> 
                                            <div class="control-group">
                                                <label class="control-label" for="family_doctor_no">Family Doctor No. </label>
                                                <div class="controls">
                                                    <input type="text" name="family_doctor_no" value="<?
                                                    if (isset($_POST['family_doctor_no'])) {
                                                        echo $_POST['family_doctor_no'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->family_doctor_no;
                                                    }
                                                    ?>" id="family_doctor_no" class="span10 m-wrap"/>
                                                </div>
                                            </div>																
                                        </div>	
                                    </div>

                                    <!-- Health Detail -->
                                    <h4 class="form-section hedding_inner">In Case Of Emergency</h4>
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="contact_person">Contact Person</label>
                                                <div class="controls">
                                                    <input type="text" name="contact_person" value="<?
                                                    if (isset($_POST['contact_person'])) {
                                                        echo $_POST['contact_person'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->contact_person;
                                                    }
                                                    ?>" id="contact_person" class="span10 m-wrap"/>
                                                </div>
                                            </div> 														
                                            <div class="control-group">
                                                <label class="control-label" for="contact_person_no">Contact No.</label>
                                                <div class="controls">
                                                    <input type="text" name="contact_person_no" value="<?
                                                    if (isset($_POST['contact_person_no'])) {
                                                        echo $_POST['contact_person_no'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->contact_person_no;
                                                    }
                                                    ?>" id="contact_person_no" class="span10 m-wrap"/>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="contact_relation">Relation</label>
                                                <div class="controls">
                                                    <input type="text" name="contact_relation" value="<?
                                                    if (isset($_POST['contact_relation'])) {
                                                        echo $_POST['contact_relation'];
                                                    } elseif (is_object($s_info)) {
                                                        echo $s_info->contact_relation;
                                                    }
                                                    ?>" id="contact_relation" class='span10 m-wrap'/>
                                                </div>
                                            </div> 															

                                        </div>	
                                    </div>													

                                    <!-- Reference Detail  -->										
                                    <h4 class="form-section hedding_inner">Reference  Detail</h4>
                                    <div class="row-fluid">
                                        <div class="span6 ">      											
                                            <div class="control-group">
                                                <label class="control-label" for="ref_name">By Reference </label>
                                                <div class="controls">
                                                    <select type="text" name="ref_name" id="ref_name" class="span10 m-wrap">
                                                        <option value='Pamphlets' <?
                                                        if (isset($_POST['ref_name']) && $_POST['ref_name'] == 'Pamphlets') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->ref_name == 'Pamphlets') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Pamphlets</option>
                                                        <option value='Poster' <?
                                                        if (isset($_POST['ref_name']) && $_POST['ref_name'] == 'Poster') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->ref_name == 'Poster') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Poster</option>
                                                        <option value='Wall Painting' <?
                                                        if (isset($_POST['ref_name']) && $_POST['ref_name'] == 'Wall Painting') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->ref_name == 'Wall Painting') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Wall Painting</option>
                                                        <option value='Newspaper' <?
                                                        if (isset($_POST['ref_name']) && $_POST['ref_name'] == 'Newspaper') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->ref_name == 'Newspaper') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Newspaper</option>
                                                        <option value='Friends' <?
                                                        if (isset($_POST['ref_name']) && $_POST['ref_name'] == 'Friends') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->ref_name == 'Friends') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Friends</option>
                                                        <option value='Parents' <?
                                                        if (isset($_POST['ref_name']) && $_POST['ref_name'] == 'Parents') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->ref_name == 'Parents') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Parents</option>
                                                        <option value='Other' <?
                                                        if (isset($_POST['ref_name']) && $_POST['ref_name'] == 'Other') {
                                                            echo 'selected';
                                                        } elseif (is_object($s_info) && $s_info->ref_name == 'Other') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Other</option>
                                                    </select>
                                                </div>
                                            </div> 															
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="ref_remark">Remark</label>
                                                <div class="controls">
                                                    <textarea class="span10 m-wrap" name='ref_remark'><?
                                                        if (isset($_POST['ref_remark'])) {
                                                            echo $_POST['ref_remark'];
                                                        } elseif (is_object($s_info)) {
                                                            echo html_entity_decode($s_info->ref_remark);
                                                        }
                                                        ?></textarea>
                                                </div>
                                            </div>
                                        </div>	
                                    </div>														
                                    <div class="form-actions">

                                        <input type="hidden" name="is_active" value="1"  />
                                        <input type="hidden" name="school_id" value="<?= $school->id ?>"  />
                                        <button class="btn blue button-next" type="submit" name="submit"  tabindex="7"> Submit & Next</button> &nbsp;&nbsp;
                                        <? if (is_object($s_info)): ?>
                                            <input type="hidden" name="id" value="<?= $s_info->id ?>"  /> 
                                            <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=address&s_id=' . $s_id); ?>" class="btn red" name="cancel" > Skip <i class="m-icon-swapright m-icon-white"></i></a>&nbsp;&nbsp;
                                        <? endif; ?>					 
                                        <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                                    </div> 
                                </form>	
                            </div>


                            <!-- Second Step  Address Detail-->
                            <div id="tab2" class="tab-pane <?= ($type == 'address') ? 'active' : ''; ?>">												
                                <form class="form-horizontal validation" action="<?php echo make_admin_url('student', 'insert', 'insert&type=address') ?>" method="POST" enctype="multipart/form-data" >
                                    <h3 class="block">Provide your address details</h3>
                                    <!-- Permanent Info  -->										
                                    <h4 class="form-section hedding_inner">Permanent Address</h4>
                                    <div class="row-fluid">
                                        <div class="span6 ">      											
                                            <div class="control-group">
                                                <label class="control-label" for="permanent_address1">Address<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="permanent_address1" name="permanent[address1]" value="<?
                                                    if (is_object($s_p_ad)) {
                                                        echo $s_p_ad->address1;
                                                    }
                                                    ?>" id="permanent_address1" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div> 
                                            <div class="control-group">
                                                <label class="control-label" for="permanent_tehsil">Tehsil<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="permanent_tehsil" name="permanent[tehsil]" value="<?
                                                    if (is_object($s_p_ad)) {
                                                        echo $s_p_ad->tehsil;
                                                    }
                                                    ?>" id="permanent_tehsil" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>	
                                            <div class="control-group">
                                                <label class="control-label" for="permanent_state">State<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="permanent_state" name="permanent[state]" value="<?
                                                    if (is_object($s_p_ad)) {
                                                        echo $s_p_ad->state;
                                                    }
                                                    ?>" id="permanent_state" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>										

                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="permanent_city">City<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="permanent_city" name="permanent[city]" value="<?
                                                    if (is_object($s_p_ad)) {
                                                        echo $s_p_ad->city;
                                                    }
                                                    ?>" id="permanent_city" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>												

                                            <div class="control-group">
                                                <label class="control-label" for="permanent_district">District<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="permanent_district" name="permanent[district]" value="<?
                                                    if (is_object($s_p_ad)) {
                                                        echo $s_p_ad->district;
                                                    }
                                                    ?>" id="permanent_district" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="permanent_zip">Zip Code<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="permanent_zip" name="permanent[zip]" value="<?
                                                    if (is_object($s_p_ad)) {
                                                        echo $s_p_ad->zip;
                                                    }
                                                    ?>" id="permanent_zip" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>
                                            <? if (is_object($s_p_ad)): ?>
                                                <input type="hidden" name="permanent[id]" value="<?= $s_p_ad->id ?>"  /> 
                                            <? endif; ?>															
                                            <input type='hidden' name='permanent[address_type]' value='permanent'/>
                                            <input type='hidden' name='permanent[student_id]' value='<?= $s_id ?>'/>
                                        </div>	
                                    </div>	

                                    <!-- Correspondence Address  -->										
                                    <h4 class="form-section hedding_inner">Correspondence Address <font style='float: right; margin-right: 60px; font-weight: normal; font-size: 17px;'>Same As Permanent <input type='checkbox' id='same_address' /></font></h4>
                                    <div class="row-fluid">
                                        <div class="span6 ">      											
                                            <div class="control-group">
                                                <label class="control-label" for="correspondence_address1">Address<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="correspondence_address1" name="correspondence[address1]" value="<?
                                                    if (is_object($s_c_ad)) {
                                                        echo $s_c_ad->address1;
                                                    }
                                                    ?>" id="correspondence_address1" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div> 
                                            <div class="control-group">
                                                <label class="control-label" for="correspondence_tehsil">Tehsil<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="correspondence_tehsil" name="correspondence[tehsil]" value="<?
                                                    if (is_object($s_c_ad)) {
                                                        echo $s_c_ad->tehsil;
                                                    }
                                                    ?>" id="correspondence_tehsil" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>	
                                            <div class="control-group">
                                                <label class="control-label" for="correspondence_state">State<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="correspondence[state]" value="<?
                                                    if (is_object($s_c_ad)) {
                                                        echo $s_c_ad->state;
                                                    }
                                                    ?>" id="correspondence_state" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div> 	
                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="correspondence_city">City<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="correspondence_city" name="correspondence[city]" value="<?
                                                    if (is_object($s_c_ad)) {
                                                        echo $s_c_ad->city;
                                                    }
                                                    ?>" id="correspondence_city" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>												

                                            <div class="control-group">
                                                <label class="control-label" for="permanent_district">District<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="correspondence_district" name="correspondence[district]" value="<?
                                                    if (is_object($s_c_ad)) {
                                                        echo $s_c_ad->district;
                                                    }
                                                    ?>" id="correspondence_district" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="correspondence_zip">Zip Code<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" id="correspondence_zip" name="correspondence[zip]" value="<?
                                                    if (is_object($s_c_ad)) {
                                                        echo $s_c_ad->zip;
                                                    }
                                                    ?>" id="correspondence_zip" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>
                                            <? if (is_object($s_c_ad)): ?>
                                                <input type="hidden" name="correspondence[id]" value="<?= $s_c_ad->id ?>"  /> 
                                            <? endif; ?>																
                                            <input type='hidden' name='correspondence[address_type]' value='correspondence'/>
                                            <input type='hidden' name='correspondence[student_id]' value='<?= $s_id ?>'/>
                                        </div>	
                                    </div>	
                                    <div class="form-actions">
                                        <input type='hidden' name='student_id' value='<?= $s_id ?>'/>
                                        <input type='hidden' name='s_id' value='<?= $s_id ?>'/>
                                        <input type="hidden" name="school_id" value="<?= $school->id ?>"  />
                                        <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=student_detail&s_id=' . $s_id); ?>" class="btn red" name="cancel" > <i class="m-icon-swapleft m-icon-white"></i> Back </a>&nbsp;&nbsp;
                                        <button class="btn blue button-next check_valid" type="submit" name="submit"  tabindex="7"> Submit & Next</button> &nbsp;&nbsp;
                                        <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=family&s_id=' . $s_id); ?>" class="btn red" name="cancel" > Skip <i class="m-icon-swapright m-icon-white"></i></a>&nbsp;&nbsp;
                                        <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                                    </div> 
                                </form>	
                            </div>


                            <!-- Family Detail -->
                            <div id="tab3" class="tab-pane <?= ($type == 'family') ? 'active' : ''; ?>">
                                <form class="form-horizontal validation" action="<?php echo make_admin_url('student', 'insert', 'insert&type=family') ?>" method="POST" enctype="multipart/form-data">
                                    <h3 class="block">Provide your Family details</h3>
                                    <h4 class="form-section hedding_inner">Family Detail</h4>
                                    <div class="row-fluid">
                                        <div class="span6 ">      											
                                            <div class="control-group">
                                                <label class="control-label" for="father_name">Father Name<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="family[father_name]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->father_name;
                                                    }
                                                    ?>" id="father_name" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div> 
                                            <div class="control-group">
                                                <label class="control-label" for="father_occupation">Father Occupation<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="family[father_occupation]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->father_occupation;
                                                    }
                                                    ?>" id="father_occupation" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>	
                                            <div class="control-group">
                                                <label class="control-label" for="father_education">Father Education<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="family[father_education]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->father_education;
                                                    }
                                                    ?>" id="father_education" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>	

                                            <div class="control-group">
                                                <label class="control-label" for="email">Email Address <span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="family[email]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->email;
                                                    }
                                                    ?>" id="email" class="span10 m-wrap validate[required,custom[email]]"/>
                                                </div>
                                            </div>															
                                            <div class="control-group">
                                                <label class="control-label" for="household_annual_income">Annual Income (<?= CURRENCY_SYMBOL ?>)<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="family[household_annual_income]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->household_annual_income;
                                                    }
                                                    ?>" id="household_annual_income" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div> 	

                                            <div class="control-group">
                                                <label class="control-label" for="phone">Land Line Number</label>
                                                <div class="controls">
                                                    <input type="text" name="family[phone]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->phone;
                                                    }
                                                    ?>" id="phone" class="span10 m-wrap"/>
                                                </div>
                                            </div>																

                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="mother_name">Mother Name<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="family[mother_name]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->mother_name;
                                                    }
                                                    ?>" id="mother_name" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>	
                                            <div class="control-group">
                                                <label class="control-label" for="mother_occupation">Mother Occupation<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="family[mother_occupation]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->mother_occupation;
                                                    }
                                                    ?>" id="mother_occupation" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="mother_education">Mother Education<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="family[mother_education]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->mother_education;
                                                    }
                                                    ?>" id="mother_education" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="mobile">Mobile Number 1<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="family[mobile]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->mobile;
                                                    }
                                                    ?>" id="mobile" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                </div>
                                            </div>	

                                            <div class="control-group">
                                                <label class="control-label" for="mobile2">Mobile Number 2</label>
                                                <div class="controls">
                                                    <input type="text" name="family[mobile2]" value="<?
                                                    if (is_object($s_f)) {
                                                        echo $s_f->mobile2;
                                                    }
                                                    ?>" id="mobile2" class="span10 m-wrap validate[custom[onlyNumberSp]]"/>
                                                </div>
                                            </div>																
                                            <div class="control-group">
                                                <label class="control-label" for="image">Family Photo</label>																	
                                                <div class="controls">
                                                    <?
                                                    if (is_object($s_f) && $s_f->family_photo):
                                                        $im_obj = new imageManipulation()
                                                        ?>
                                                        <div class="item span10" style='text-align:center;'>	
                                                            <a class="fancybox-button" data-rel="fancybox-button" title="Family Photo" href="<?= $im_obj->get_image('student_family', 'large', $s_f->family_photo); ?>">
                                                                <div class="zoom">
                                                                    <img src="<?= $im_obj->get_image('student_family', 'medium', $s_f->family_photo); ?>" />
                                                                    <div class="zoom-icon"></div>
                                                                </div>
                                                            </a>
                                                            <div class="details">
                                                                <a href="<?= make_admin_url('student', 'delete_image', 'delete_image&s_id=' . $s_id . '&type=family&del_id=' . $s_f->id); ?>" class="icon" onclick="return confirm('Are you sure? You are deleting this image.');" title="click here to delete this image"><i class="large icon-remove"></i></a>    
                                                            </div>	
                                                        </div>															
                                                    <? else: ?>
                                                        <input type="file" name="image"  id="image" class="span10 m-wrap"/>
                                                    <? endif; ?> 
                                                </div>
                                            </div> 																
                                            <? if (is_object($s_f)): ?>
                                                <input type="hidden" name="family[id]" value="<?= $s_f->id ?>"  /> 
                                            <? endif; ?>																
                                        </div>	
                                    </div>

                                    <!-- Sibling Detail -->
                                    <h4 class="form-section hedding_inner">Sibling Study In School</h4>
                                    <div class="row-fluid">
                                        <? if ($QueryS->GetNumRows()): $sib_r = 0; ?>
                                            <div class="control-group" id='sibling'>
                                                <? while ($sib = $QueryS->GetObjectFromRecord()): ?>
                                                    <div>
                                                        <span class='span3'><input type="text" name="sibling[<?= $sib_r ?>][reg_id]" value="<?= $sib->reg_id ?>" id="" class="span11 m-wrap" placeholder='Sibling Registration Id'/></span>
                                                        <span class='span3'><input type="text" name="sibling[<?= $sib_r ?>][name]" value="<?= $sib->name ?>" id="" class="span11 m-wrap" placeholder='Sibling Name'/></span>
                                                        <span class='span3'><input type="text" name="sibling[<?= $sib_r ?>][class]" value="<?= $sib->class ?>" id="" class="span11 m-wrap" placeholder='Class'/></span>
                                                        <span class='span2'><input type="text" name="sibling[<?= $sib_r ?>][relation]" value="<?= $sib->relation ?>" id="" class="span11 m-wrap" placeholder='Relation'/></span>&nbsp;&nbsp;<a class="btn red" href="<?= make_admin_url('student', 'delete_record', 'delete_record&type=family&s_id=' . $s_id . '&del_id=' . $sib->id); ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                                        <input type="hidden" name="sibling[<?= $sib_r ?>][id]" value="<?= $sib->id ?>"/>
                                                    </div><div style='clear:both;height:10px;'></div>																

                                                    <?
                                                    $sib_r++;
                                                endwhile;
                                                ?>
                                            </div> 
                                            <input type='hidden' value='<?= $sib_r ?>' id='total_sib'>
                                        <? else: ?>
                                            <div class="control-group" id='sibling'>
                                                <div>
                                                    <span class='span3'><input type="text" name="sibling[0][reg_id]" value="" id="" class="span11 m-wrap" placeholder='Sibling Registration Id'/></span>
                                                    <span class='span3'><input type="text" name="sibling[0][name]" value="" id="" class="span11 m-wrap" placeholder='Sibling Name'/></span>
                                                    <span class='span3'><input type="text" name="sibling[0][class]" value="" id="" class="span11 m-wrap" placeholder='Class'/></span>
                                                    <span class='span3'><input type="text" name="sibling[0][relation]" value="" id="" class="span11 m-wrap" placeholder='Relation'/></span><br/>
                                                </div><div style='clear:both;height:10px;'></div>
                                                <div>
                                                    <span class='span3'><input type="text" name="sibling[1][reg_id]" value="" id="" class="span11 m-wrap" placeholder='Sibling Registration Id'/></span>
                                                    <span class='span3'><input type="text" name="sibling[1][name]" value="" id="" class="span11 m-wrap" placeholder='Sibling Name'/></span>
                                                    <span class='span3'><input type="text" name="sibling[1][class]" value="" id="" class="span11 m-wrap" placeholder='Class'/></span>
                                                    <span class='span3'><input type="text" name="sibling[1][relation]" value="" id="" class="span11 m-wrap" placeholder='Relation'/></span>
                                                </div><div style='clear:both;height:10px;'></div>
                                            </div> 
                                            <input type='hidden' value='1' id='total_sib'>
                                        <? endif; ?>
                                        <span class='span12' style='text-align:right;margin-left:0px;padding-right:20px;'><a class="btn icn-only green" id='add_new_sib'>Add More<i class="m-icon-swapright m-icon-white"></i></a></span>																	
                                        <div style='clear:both;height:10px;'></div>
                                    </div>	
                                    <div class="form-actions">
                                        <input type='hidden' name='family[student_id]' value='<?= $s_id ?>'/>
                                        <input type='hidden' name='sibling[student_id]' value='<?= $s_id ?>'/>															 
                                        <input type='hidden' name='student_id' value='<?= $s_id ?>'/>
                                        <input type='hidden' name='s_id' value='<?= $s_id ?>'/>
                                        <input type="hidden" name="school_id" value="<?= $school->id ?>"  />
                                        <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=address&s_id=' . $s_id); ?>" class="btn red" name="cancel" > <i class="m-icon-swapleft m-icon-white"></i> Back </a>&nbsp;&nbsp;
                                        <button class="btn blue button-next check_valid" type="submit" name="submit"  tabindex="7"> Submit & Next </button> &nbsp;&nbsp;
                                        <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=previous_school&s_id=' . $s_id); ?>" class="btn red" name="cancel" > Skip <i class="m-icon-swapright m-icon-white"></i></a>&nbsp;&nbsp;
                                        <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                                    </div> 
                                </form>	
                            </div>

                            <!-- Step 4 Previous School -->
                            <div id="tab4" class="tab-pane <?= ($type == 'previous_school') ? 'active' : ''; ?>">
                                <form class="form-horizontal validation" action="<?php echo make_admin_url('student', 'insert', 'insert&type=previous_school') ?>" method="POST" enctype="multipart/form-data">
                                    <h3 class="block">Provide your Previous School details</h3>
                                    <h4 class="form-section hedding_inner">Previous School Detail</h4>
                                    <div class="row-fluid">

                                        <? if ($P_school->GetNumRows()): $PS_r = 0; ?>
                                            <div id='p_school'>
                                                <? while ($sch = $P_school->GetObjectFromRecord()): ?>	
                                                    <h4>School <?= $PS_r + 1 ?></h4><hr/>
                                                    <div>
                                                        <span class='span3'><input type="text" name="school[<?= $PS_r ?>][school_name]" value="<?= $sch->school_name ?>" id="" class="span11 m-wrap" placeholder='School Name'/></span>
                                                        <span class='span3'><input type="text" name="school[<?= $PS_r ?>][class]" value="<?= $sch->class ?>" id="" class="span11 m-wrap" placeholder='Class'/></span>
                                                        <span class='span3'><input type="text" name="school[<?= $PS_r ?>][address1]" value="<?= $sch->address1 ?>" id="" class="span11 m-wrap" placeholder='Address1'/></span>
                                                        <span class='span3'><input type="text" name="school[<?= $PS_r ?>][address2]" value="<?= $sch->address2 ?>" id="" class="span11 m-wrap" placeholder='Address2'/></span>
                                                    </div><div style='clear:both;height:10px;'></div>
                                                    <div>
                                                        <span class='span3'><input type="text" name="school[<?= $PS_r ?>][percentage]" value="<?= $sch->percentage ?>" id="" class="span11 m-wrap" placeholder='Percentage'/></span>
                                                        <span class='span3'><input type="text" name="school[<?= $PS_r ?>][city]" value="<?= $sch->city ?>" id="" class="span11 m-wrap" placeholder='City'/></span>
                                                        <span class='span3'><input type="text" name="school[<?= $PS_r ?>][state]" value="<?= $sch->state ?>" id="" class="span11 m-wrap" placeholder='State'/></span>
                                                        <span class='span2'><input type="text" name="school[<?= $PS_r ?>][country]" value="<?= $sch->country ?>" id="" class="span11 m-wrap" placeholder='Country'/></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn red" href="<?= make_admin_url('student', 'delete_record', 'delete_record&type=previous_school&s_id=' . $s_id . '&del_id=' . $sch->id); ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                                    </div><div style='clear:both;height:10px;'></div>
                                                    <input type="hidden" name="school[<?= $PS_r ?>][id]" value="<?= $sch->id ?>"/>
                                                    <?
                                                    $PS_r++;
                                                endwhile;
                                                ?>
                                            </div>
                                            <hr/>	
                                            <input type='hidden' value='<?= $PS_r ?>' id='total_school'>
                                            <span class='span12' style='text-align:right;margin-left:0px;padding-right:20px;'><a class="btn icn-only green" id='add_new_school'>Add More<i class="m-icon-swapright m-icon-white"></i></a></span>																	
                                            <div style='clear:both;height:10px;'></div>
                                        <? else: ?>
                                            <div id='p_school'>
                                                <h4>School 1</h4><hr/>
                                                <div>
                                                    <span class='span3'><input type="text" name="school[0][school_name]" value="" id="" class="span11 m-wrap" placeholder='School Name'/></span>
                                                    <span class='span3'><input type="text" name="school[0][class]" value="" id="" class="span11 m-wrap" placeholder='Class'/></span>
                                                    <span class='span3'><input type="text" name="school[0][address1]" value="" id="" class="span11 m-wrap" placeholder='Address1'/></span>
                                                    <span class='span3'><input type="text" name="school[0][address2]" value="" id="" class="span11 m-wrap" placeholder='Address2'/></span>
                                                </div><div style='clear:both;height:10px;'></div>
                                                <div>
                                                    <span class='span3'><input type="text" name="school[0][percentage]" value="" id="" class="span11 m-wrap" placeholder='Percentage'/></span>
                                                    <span class='span3'><input type="text" name="school[0][city]" value="" id="" class="span11 m-wrap" placeholder='City'/></span>
                                                    <span class='span3'><input type="text" name="school[0][state]" value="" id="" class="span11 m-wrap" placeholder='State'/></span>
                                                    <span class='span3'><input type="text" name="school[0][country]" value="" id="" class="span11 m-wrap" placeholder='Country'/></span>
                                                </div><div style='clear:both;height:10px;'></div>
                                            </div>
                                            <hr/>	
                                            <input type='hidden' value='1' id='total_school'>
                                            <span class='span12' style='text-align:right;margin-left:0px;padding-right:20px;'><a class="btn icn-only green" id='add_new_school'>Add More<i class="m-icon-swapright m-icon-white"></i></a></span>																	
                                            <div style='clear:both;height:10px;'></div>															
                                        <? endif; ?>	
                                    </div>
                                    <div class="form-actions">
                                        <input type='hidden' name='student_id' value='<?= $s_id ?>'/>
                                        <input type='hidden' name='s_id' value='<?= $s_id ?>'/>
                                        <input type="hidden" name="school_id" value="<?= $school->id ?>"  />
                                        <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=family&s_id=' . $s_id); ?>" class="btn red" name="cancel" > <i class="m-icon-swapleft m-icon-white"></i> Back </a>&nbsp;&nbsp;
                                        <button class="btn blue button-next" type="submit" name="submit"  tabindex="7"> Submit & Next </button> &nbsp;&nbsp;
                                        <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=document&s_id=' . $s_id); ?>" class="btn red" name="cancel" > Skip <i class="m-icon-swapright m-icon-white"></i></a>&nbsp;&nbsp;
                                        <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                                    </div> 
                                </form>	
                            </div>


                            <!-- Step 5 Student Documents -->
                            <div id="tab5" class="tab-pane <?= ($type == 'document') ? 'active' : ''; ?>">
                                <form class="form-horizontal validation" action="<?php echo make_admin_url('student', 'insert', 'insert&type=document') ?>" method="POST" enctype="multipart/form-data">
                                    <h3 class="block">Provide your Document details</h3>
                                    <h4 class="form-section hedding_inner">Student Documents</h4>
                                    <div class="row-fluid">
                                        <? if ($S_doc->GetNumRows()): $Sd_r = '0'; ?>
                                            <div class="control-group" id='more_doc'>
                                                <table class="table table-striped table-bordered table-advance table-hover">
                                                    <thead>
                                                        <tr>																	
                                                            <th class="hidden-phone span5">Title</th>
                                                            <th class="hidden-phone span2" > File</th>
                                                            <th class="hidden-phone span3" > </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>	
                                                        <? while ($s_d = $S_doc->GetObjectFromRecord()): ?>														
                                                            <tr>
                                                                <td><?= $s_d->title ?></td>
                                                                <td><a href="<?= DIR_WS_SITE_UPLOAD . 'file/student_document/' . $s_d->file ?>"><?= $s_d->file ?></a></span></td>
                                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn red" href="<?= make_admin_url('student', 'delete_record', 'delete_record&type=document&s_id=' . $s_id . '&del_id=' . $s_d->id); ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a></td></tr>
                                                        <div style='clear:both;height:10px;'></div>
                                                        <?
                                                        $Sd_r++;
                                                    endwhile;
                                                    ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <hr/><input type='hidden' value='1' id='total_doc'>
                                            <span class='span12' style='text-align:right;margin-left:0px;'><a class="btn icn-only green" id='add_new_doc'>Add More<i class="m-icon-swapright m-icon-white"></i></a></span>
                                            <div style='clear:both;height:10px;'></div>	
                                            <div class="form-actions">
                                                <input type='hidden' name='s_id' value='<?= $s_id ?>'/>
                                                <input type='hidden' name='student_id' value='<?= $s_id ?>'/>
                                                <input type="hidden" name="school_id" value="<?= $school->id ?>"  />
                                                <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=previous_school&s_id=' . $s_id); ?>" class="btn red" name="cancel" > <i class="m-icon-swapleft m-icon-white"></i> Back </a>&nbsp;&nbsp;
                                                <button class="btn blue button-next" type="submit" name="submit"  tabindex="7"> Submit & Next </button> &nbsp;&nbsp;
                                                <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=admission&s_id=' . $s_id); ?>" class="btn red" name="cancel" > Skip <i class="m-icon-swapright m-icon-white"></i></a>&nbsp;&nbsp;
                                                <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
                                        <? else: ?>
                                            <div class="control-group" id='more_doc'>			
                                                <div>
                                                    <span class='span6'><input type="text" name="document[0][title]" value="" id="" class="span11 m-wrap" placeholder='Document Title'/></span>
                                                    <span class='span6'><input type='file' name="file[]" class='span10' id='file0'/></span>
                                                </div><div style='clear:both;height:10px;'></div>
                                                <div>
                                                    <span class='span6'><input type="text" name="document[1][title]" value="" id="" class="span11 m-wrap" placeholder='Document Title'/></span>
                                                    <span class='span6'><input type='file' name="file[]" class='span10' id='file0'/></span>
                                                </div><div style='clear:both;height:10px;'></div>
                                            </div>
                                            <hr/><input type='hidden' value='1' id='total_doc'>
                                            <span class='span12' style='text-align:right;margin-left:0px;'><a class="btn icn-only green" id='add_new_doc'>Add More<i class="m-icon-swapright m-icon-white"></i></a></span>
                                            <div style='clear:both;height:10px;'></div>	
                                            <div class="form-actions">
                                                <input type='hidden' name='s_id' value='<?= $s_id ?>'/>
                                                <input type='hidden' name='student_id' value='<?= $s_id ?>'/>
                                                <input type="hidden" name="school_id" value="<?= $school->id ?>"  />
                                                <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=previous_school&s_id=' . $s_id); ?>" class="btn red" name="cancel" > <i class="m-icon-swapleft m-icon-white"></i> Back </a>&nbsp;&nbsp;
                                                <button class="btn blue button-next" type="submit" name="submit"  tabindex="7"> Submit & Next </button> &nbsp;&nbsp;
                                                <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=admission&s_id=' . $s_id); ?>" class="btn red" name="cancel" > Skip <i class="m-icon-swapright m-icon-white"></i></a>&nbsp;&nbsp;
                                                <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                                            </div>															

                                        <?
                                        endif;
                                        ;
                                        ?>																
                                    </div>
                                </form>	
                            </div>	

                            <!-- Step 6 Student Admission -->
                            <div id="tab6" class="tab-pane <?= ($type == 'admission') ? 'active' : ''; ?>">
                                <form class="form-horizontal validation" action="<?php echo make_admin_url('student', 'insert', 'insert&type=admission') ?>" method="POST" enctype="multipart/form-data">
                                    <h3 class="block">Add Student To Session</h3>
                                    <!-- student Detail  -->	
                                    <h4 class="form-section hedding_inner">Student Granted</h4>	
                                    <div class="row-fluid">	
                                        <div class="span6">
                                            <div class="control-group">
                                                <label class="control-label" for="concession">Student Granted<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input name="concession" style='padding-left:5px;' value='<?
                                                    if (is_object($ct_addmission)): echo $ct_addmission->concession;
                                                    endif;
                                                    ?>' id='concession' class="span10 m-wrap"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="concession_type">Type<span class="required">*</span></label>
                                                <div class="controls">
                                                    <select name="concession_type" id='concession_type' class="span10 m-wrap">
                                                        <option value="one_time" <?
                                                        if (is_object($ct_addmission)): if ($ct_addmission->concession_type == 'one_time'):
                                                                echo "selected";
                                                            endif;
                                                        endif;
                                                        ?>>One Time</option>
                                                        <option value="regular" <?
                                                        if (is_object($ct_addmission)): if ($ct_addmission->concession_type == 'regular'):
                                                                echo "selected";
                                                            endif;
                                                        endif;
                                                        ?>>Regular</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span6">
                                            <div class="control-group">
                                                <label for="amount" class="control-label">Discount From Tuition Fee</label>
                                                <div class="controls">
                                                    <input name="amount" style='padding-left:5px;' value='<?
                                                    if (is_object($ct_addmission)): echo $ct_addmission->amount;
                                                    endif;
                                                    ?>' id='amount' class="span10 m-wrap validate[custom[onlyNumberSp]]"/>
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label for="start_concession" class="control-label">Start Concession</label>
                                                <div class="controls">
                                                    <input type="checkbox" name="start_concession" value="1"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>		
                                    <h4 class="form-section hedding_inner">Session Detail</h4>
                                    <div class="row-fluid">
                                        <div class="span6">			
                                            <div class="control-group">
                                                <label class="control-label" for="subscription_id">Session<span class="required">*</span></label>
                                                <div class="controls">						
                                                    <select name="session_id" id="select_session" class="span10 m-wrap">														
                                                        <?php
                                                        $session_sr = 1;
                                                        while ($session = $QuerySession->GetObjectFromRecord()):
                                                            if (is_object($ct_addmission)):
                                                                $current_session = get_object('session', $ct_addmission->session_id);
                                                            elseif ($session_sr == '1'):
                                                                $current_session = get_object('session', $session->id);
                                                            endif;
                                                            ?>
                                                            <option value='<?= $session->id ?>' <?
                                                            if (is_object($ct_addmission) && $ct_addmission->session_id == $session->id): echo "selected";
                                                            endif;
                                                            ?>><?= ucfirst($session->title) ?></option>
                                                                    <?
                                                                    $session_sr++;
                                                                endwhile;
                                                                ?>		
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-fluid" id='another_session'>
                                        <div class="span6 ">  																									
                                            <div class="control-group">
                                                <label class="control-label" for="ref_name">Session Start From</label>
                                                <div class="controls">
                                                    <input type="text" value="<?
                                                    if (is_object($current_session)) {
                                                        echo $current_session->start_date;
                                                    }
                                                    ?>" id="ref_name" class="span10 m-wrap" disabled="disabled"/>
                                                </div>
                                            </div> 
                                        </div>				
                                        <div class="span6 ">

                                            <div class="control-group">
                                                <label class="control-label" for="ref_phone">Session Ends On</label>
                                                <div class="controls">
                                                    <input type="text" value="<?
                                                    if (is_object($current_session)) {
                                                        echo $current_session->end_date;
                                                    }
                                                    ?>" id="ref_phone" class="span10" disabled="disabled"/>
                                                </div>
                                            </div>	

                                        </div>	
                                    </div>	
                                    <div class="form-actions">
                                        <input type='hidden' name='s_id' value="<?= $s_id ?>"/>
                                        <input type='hidden' name='student_id' value="<?= $s_id ?>"/>
                                        <a href="<?php echo make_admin_url('student', 'insert', 'insert&type=document&s_id=' . $s_id); ?>" class="btn red" name="cancel" > <i class="m-icon-swapleft m-icon-white"></i> Back </a>&nbsp;&nbsp;
                                        <button class="btn blue button-next check_valid" type="submit" name="submit"  tabindex="7"> Next </button> &nbsp;&nbsp;
                                        <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                                    </div> 
                                </form>	
                            </div>	




                            <!-- Step 7 Student Vehicle -->
                            <div id="tab6" class="tab-pane <?= ($type == 'vehicle') ? 'active' : ''; ?>">
                                <form class="form-horizontal validation" action="<?php echo make_admin_url('student', 'insert', 'insert&type=vehicle') ?>" method="POST" enctype="multipart/form-data">
                                    <h3 class="block">Assign Vehicle</h3>
                                    <h4 class="form-section hedding_inner">Student Detail</h4>
                                    <div class="row-fluid">														
                                        <div class="span6">
                                            <!-- Student Info -->
                                            <div class="alert alert-block alert-success fade in">

                                                <div class="row-fluid">	
                                                    <div class='span4'>Session: </div>
                                                    <div class='span6'><?= $student->session_name ?></div>												
                                                </div>
                                                <div class="row-fluid">	
                                                    <div class='span4'>Section: </div>
                                                    <div class='span6'><?= $student->section ?></div>												
                                                </div>																									
                                                <div class="row-fluid">	
                                                    <div class='span4'>Reg Id: </div>
                                                    <div class='span6'><?= $student->reg_id ?></div>												
                                                </div>
                                                <div class="row-fluid">	
                                                    <div class='span4'>Name: </div>
                                                    <div class='span6'><?= ucfirst($student->first_name) . ' ' . $student->last_name ?></div>												
                                                </div>	
                                                <div class="row-fluid">	
                                                    <div class='span4'>Roll No.: </div>
                                                    <div class='span6'><?= $student->roll_no ?></div>		
                                                </div>
                                            </div>
                                        </div>						
                                    </div>
                                    <!-- vehicle Detail  -->										
                                    <h4 class="form-section hedding_inner">Vehicle Detail</h4>
                                    <div class="row-fluid">		
                                        <div class='span6'>	
                                            <div class="control-group">
                                                <label class="control-label" for="subscription_id">Select Vehicle<span class="required">*</span></label>
                                                <div class="controls">							
                                                    <?php if ($QueryVeh->GetNumRows() > 0): ?>
                                                        <select name="vehicle_id" id="select_vehicle" class="span10 m-wrap">	
                                                            <?
                                                            $session_sr = 1;
                                                            while ($vehicle = $QueryVeh->GetObjectFromRecord()):
                                                                if ($session_sr == '1'):
                                                                    $current_vehicle = get_object('school_vehicle', $vehicle->id);
                                                                endif;
                                                                ?>
                                                                <option value='<?= $vehicle->id ?>' <?
                                                                if (is_object($VehstudentAdd) && $VehstudentAdd->vehicle_id == $vehicle->id): echo 'selected';
                                                                endif;
                                                                ?> ><?= ucfirst($vehicle->model) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endwhile;
                                                                    ?>
                                                        </select>
                                                    <? else: $current_vehicle = ''; ?>
                                                        <input type="text" value='No Vehicle Found..!' id="select_vehicle" disabled='disabled' class="span10 m-wrap validate[required]"/>
                                                    <? endif; ?>
                                                </div>
                                            </div> 
                                        </div>	
                                        <div class='span6'>
                                            <? if (is_object($current_vehicle)): ?>									
                                                <div class="control-group">
                                                    <label for="amount" class="control-label">Amount(<?= CURRENCY_SYMBOL ?>) (Per Fee)<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <input name="amount" id='amount' style='padding-left:5px;' value="<?
                                                        if (is_object($VehstudentAdd)): echo $VehstudentAdd->amount;
                                                        endif;
                                                        ?>" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                                    </div>
                                                </div>
                                            <? else: ?>	
                                                <div class="control-group">		
                                                    <label class="control-label" for="amount">Amount(<?= CURRENCY_SYMBOL ?>)<span class="required"></span></label>
                                                    <div class="controls">
                                                        <input type="text" value='No Vehicle Found..!' id="amount" disabled='disabled' class="span10 m-wrap"/>
                                                    </div>	
                                                </div>					
                                            <? endif; ?>
                                        </div>	
                                    </div>
                                    <div class="row-fluid" id='another_vehicle'>
                                        <div class="span6 "> 
                                            <div class="control-group">
                                                <label class="control-label" for="ref_name">Vehicle Make</label>
                                                <div class="controls">
                                                    <input type="text" value="<?
                                                    if (is_object($VehstudentAdd)): echo $VehstudentAdd->make;
                                                    elseif (is_object($current_vehicle)): echo $current_vehicle->make;
                                                    endif;
                                                    ?>" id="ref_name" class="span10 m-wrap" disabled="disabled"/>
                                                </div>
                                            </div> 
                                        </div>				
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="ref_phone">Vehicle Number</label>
                                                <div class="controls">
                                                    <input type="text" value="<?
                                                    if (is_object($VehstudentAdd)): echo $VehstudentAdd->vehicle_number;
                                                    elseif (is_object($current_vehicle)): echo $current_vehicle->vehicle_number;
                                                    endif;
                                                    ?>" id="ref_phone" class="span10" disabled="disabled"/>
                                                </div>
                                            </div>	
                                        </div>
                                    </div>	
                                    <div class="form-actions">
                                        <input type='hidden' name='s_id' value="<?= $s_id ?>"/>
                                        <input type='hidden' name='session_id' value="<?= $student->session_id ?>"/>
                                        <input type='hidden' name='section' value="<?= $student->section ?>"/>
                                        <input type='hidden' name='student_id' value="<?= $s_id ?>"/>
                                        <input type='hidden' name='from_date' value="<?= date('Y-m-d') ?>"/>
                                        <input type='hidden' name='to_date' value="<?= $session_info->end_date ?>"/>						
                                        <a href="<?php echo make_admin_url('student', 'update', 'update&type=admission&s_id=' . $s_id); ?>" class="btn red" name="cancel" > <i class="m-icon-swapleft m-icon-white"></i> Back </a>&nbsp;&nbsp;
                                        <? if (is_object($current_vehicle)): ?>
                                            <? if (is_object($VehstudentAdd) && strtotime($VehstudentAdd->to_date) >= strtotime(date('Y-m-d'))): ?>	
                                                <button class="btn red button-next check_valid" type="submit" name="stop_vehicle"  tabindex="7"> Stop Vehicle Service </button> &nbsp;&nbsp;
                                            <? else: ?>
                                                <button class="btn blue button-next check_valid" type="submit" name="submit"  tabindex="7"> Start Vehicle Service </button> &nbsp;&nbsp;
                                            <? endif; ?>
                                        <? endif; ?>
                                        <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>" class="btn yellow" name="cancel" > Skip & Finish  <i class="m-icon-swapright m-icon-white"></i></a>
                                    </div> 
                                </form>	
                            </div>

                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        /* Add New sibling */
        $("#add_new_sib").live("click", function () {
            var current_ql = $('#total_sib').val();
            var new_ql = parseInt(current_ql) + parseInt('1');

            $('#sibling').append("<div class='delete_row" + new_ql + "'><span class='span3'><input type='text' name='sibling[" + new_ql + "][reg_id]' class='span11' placeholder='Sibling Registration Id'/></span><span class='span3'><input type='text' name='sibling[" + new_ql + "][name]' class='span11' placeholder='Sibling Name'/></span><span class='span3'><input type='text' name='sibling[" + new_ql + "][class]' class='span11' placeholder='Class'/></span><span class='span2'><input type='text' name='sibling[" + new_ql + "][relation]' class='span11' placeholder='Relation'/></span>&nbsp;&nbsp;<a class='btn red icn-only delete_row'><i class='icon-remove icon-white'></i></a><div style='clear:both;height:10px;'></div></div>");
            $('#total_sib').val(new_ql);
        });
        /* Delete sibling */
        $(".delete_row").live("click", function () {
            var current_ql = $('#total_sib').val();
            var new_ql = parseInt(current_ql) - parseInt('1');
            if (new_ql < 1)
            {
                return false;
            }
            else {
                $('.delete_row' + current_ql + '').css('display', 'none');
                $('#total_sib').val(new_ql);
            }
        });
        /* Add New school */
        $("#add_new_school").live("click", function () {
            var current_ql = $('#total_school').val();
            var new_ql = parseInt(current_ql) + parseInt('1');

            $('#p_school').append("<div class='delete" + new_ql + "'><h4>School " + new_ql + "</h4><hr/><div><span class='span3'><input type='text' name='school[" + new_ql + "][school_name]' class='span11' placeholder='School Name'/></span><span class='span3'><input type='text' name='school[" + new_ql + "][class]' class='span11' placeholder='Class'/></span><span class='span3'><input type='text' name='school[" + new_ql + "][address1]' class='span11' placeholder='Address1'/></span><span class='span3'><input type='text' name='school[" + new_ql + "][address2]' class='span11' placeholder='Address2'/></span></div><div style='clear:both;height:10px;'></div><div><span class='span3'><input type='text' name='school[" + new_ql + "][percentage]' class='span11' placeholder='Percentage'/></span><span class='span3'><input type='text' name='school[" + new_ql + "][city]' class='span11' placeholder='City'/></span><span class='span3'><input type='text' name='school[" + new_ql + "][state]' class='span11' placeholder='State'/></span><span class='span2'><input type='text' name='school[" + new_ql + "][country]' class='span11' placeholder='Country'/></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class='btn red icn-only delete_row'><i class='icon-remove icon-white'></i></a><div style='clear:both;height:10px;'></div></div>");
            $('#total_school').val(new_ql);
        });
        /* Delete school */
        $(".delete_row").live("click", function () {
            var current_ql = $('#total_school').val();
            var new_ql = parseInt(current_ql) - parseInt('1');
            if (new_ql < 0)
            {
                return false;
            }
            else {
                $('.delete' + current_ql + '').css('display', 'none');
                $('#total_school').val(new_ql);
            }
        });
        /* Add New Document */
        $("#add_new_doc").live("click", function () {
            var current_doc = $('#total_doc').val();
            var new_doc = parseInt(current_doc) + parseInt('1');

            $('#more_doc').append("<div class='delete_row_doc" + new_doc + "'><span class='span6'><input type='text' name='document[" + new_doc + "][title]' class='span11' placeholder='Document Title'/></span><span class='span6'><input type='file' name='file[" + new_doc + "]'/>&nbsp;&nbsp;<a class='btn red icn-only delete_row_doc'><i class='icon-remove icon-white'></i></a></span><div style='clear:both;height:10px;'></div></div>");
            $('#total_doc').val(new_doc);
        });
        /* Delete Document */
        $(".delete_row_doc").live("click", function () {
            var current_doc = $('#total_doc').val();
            var new_doc = parseInt(current_doc) - parseInt('1');

            if (new_doc < 0)
            {
                return false;
            }
            else {
                $('.delete_row_doc' + current_doc + '').css('display', 'none');
                $('#total_doc').val(new_doc);
            }
        });

        $("#select_session").live("change", function () {
            var id = $(this).val();
            $("#another_session").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id;

            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'multi_session', 'multi_session&temp=student'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#another_session").html(data);
                }
            });
        });
        $("#select_vehicle").live("change", function () {
            var id = $(this).val();
            $("#another_vehicle").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id;

            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'multi_vehicle', 'multi_vehicle&temp=student'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#another_vehicle").html(data);
                }
            });
        });
        $("#same_address").live("click", function () {
            if ($('#same_address').attr('checked')) {
                $('#correspondence_address1').val($('#permanent_address1').val());
                $('#correspondence_address2').val($('#permanent_address2').val());
                $('#correspondence_city').val($('#permanent_city').val());
                $('#correspondence_tehsil').val($('#permanent_tehsil').val());
                $('#correspondence_district').val($('#permanent_district').val());
                $('#correspondence_state').val($('#permanent_state').val());
                $('#correspondence_country').val($('#permanent_country').val());
                $('#correspondence_zip').val($('#permanent_zip').val());
            } else {
                $('#correspondence_address1').val('');
                $('#correspondence_address2').val('');
                $('#correspondence_city').val('');
                $('#correspondence_tehsil').val('');
                $('#correspondence_district').val('');
                $('#correspondence_state').val('');
                $('#correspondence_country').val('');
                $('#correspondence_zip').val('');
            }
        });

    });
</script>
