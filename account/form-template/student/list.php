<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Students
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li class="last">
                    List Student
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Manage Student</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <!-- select Session And Section -->
                    <div class="row-fluid">
                        <div class='span3'>
                            <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                            <select class="select2_category" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                                <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                    <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class='span4'>
                            <div id="session_list_details"></div>
                        </div>
                        <div class='span3' id='ajex_section'>
                            <label class="control-label" style='width:auto;'>Select Section</label>
                            <? if ($QuerySec->GetNumRows() > 0): ?>
                                <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                    <option value="0">All</option>
                                    <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                        <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_sec) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                    <? endwhile; ?>	
                                </select>
                            <? else: ?>
                                <input type='text' class="span10" value='No Section Found.' disabled />
                            <? endif; ?>											
                        </div>
                        <div class='span2'>	
                            <label class="control-label">&nbsp;</label>
                            <span class='btn medium green' id='go'>Go</span>									
                        </div>	
                    </div>	

                    <!-- List OF Student -->
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-user"></i>Student List </div>
                            <div class="tools">
                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                <a  href="javascript:;" class="reload"></a>
                            </div>
                        </div>	
                        <div class="portlet-body">
                            <div id="ajex_student">
                                <table class="table table-bordered table-striped table-hover" id='sample_2'>
                                    <thead>
                                        <tr>
                                            <th class='hidden-480 sorting_disabled'>Sr.No.</th>
                                            <th>Reg ID.</th>
                                            <th>Name</th>
                                            <th>Father Name</th>
                                            <th>Current Session</th>
                                            <th>Login Details</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <? if (!empty($record)): ?>
                                        <tbody>
                                            <?php
                                            $sr = 1;
                                            foreach ($record as $s_k => $object):
                                                $CurrObj = new studentSession();
                                                $current_session = $CurrObj->getStudentCurrentSession($school->id, $object->id);
                                                $Obj = new studentSession();
                                                $get_session_id = $CurrObj->getStudentCurrentSession($school->id, $object->id, TRUE);
                                                ?>
                                                <tr>	
                                                    <td class='hidden-480 sorting_disabled'><?php echo $sr++; ?>.</td>												
                                                    <td><?= $object->reg_id ?></td>	
                                                    <td><?= $object->first_name . " " . $object->last_name ?></td>
                                                    <td><?= $object->father_name ?></td>
                                                    <td><?= $current_session ?></td>
                                                    <td>
                                                        <?php if (!empty($object->student_login_id) || !empty($object->password)) { ?>
                                                            <a id="send_login_sms" data-student_id="<?= $object->id ?>" class="btn mini green icn-only tooltips" href="" title="Send Login Details Via SMS">SMS</a>&nbsp;&nbsp;
                                                            <a id="send_login_email" data-student_id="<?= $object->id ?>" class="btn mini blue icn-only tooltips" href="" title="Send Login Details Via Email">Email</a>&nbsp;&nbsp;
                                                        <?php } else { ?>
                                                            <span class="text-error">No Details Found</span>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <a class="btn mini green icn-only tooltips" href="<?php echo make_admin_url('messages', 'compose', 'compose', 'user_id=' . $object->id . '&user_type=student') ?>" title="click here to send message">Send Message</a>&nbsp;&nbsp;
                                                        <a class="btn mini purple icn-only tooltips" href="<?php echo make_admin_url('view', 'list', 'list', 'id=' . $object->id) ?>" title="click here to see the full profile">Profile</a>&nbsp;&nbsp;
                                                        <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('student', 'update', 'update', 'id=' . $object->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                                <?php
                                            endforeach;
                                            ?>
                                        </tbody>
                                    <? else: ?>
                                        <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>					
                                    <?php endif; ?>  
                                </table>	
                            </div>					
                        </div>					
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('change', '#sess_int', function () {
        var sess_int = $(this).val();
        $("#session_list_details").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=student'); ?>", {act: 'get_sessions_list', sess_int: sess_int}, function (data) {
            $('#session_list_details').html(data);
        });
    }).ready(function () {
        $('#sess_int').change();
    }).on('change', '.session_filter', function () {
        /* Session Students */
        var session_id = $("#session_id").val();
        if (session_id.length > 0) {
            var id = session_id;
            $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            var dataString = 'id=' + id;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section&temp=student'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_section").html(data);
                }
            });
        }
        else {
            return false;
        }
    });
    /* Filter Students */
    $("#go").live("click", function () {
        var sess_int = $('#sess_int').val();
        var session_id = $('#session_id').val();
        var ct_sec = $('#ct_sec').val();
        var dataString = 'session_id=' + session_id + '&ct_sec=' + ct_sec + '&sess_int=' + sess_int;
        $("#ajex_student").html('<div style="width:100%;text-align:center;padding-top:50px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_student', 'ajax_section_student&temp=student'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                $("#ajex_student").html(data);
                App.init();
                FormComponents.init();
                TableManaged.init();
            }
        });
    });
    $(".session_filter_group").live("change", function () {
        var session_id = $(this).val();
        $('#session_filter_group').submit();
    });

    $(document).on('click', '#stuck_off_student', function () {
        $(this).html('<i class="icon-refresh"></i> Stuck Off');
        var student_id = $(this).attr('data-student_id');
        var session_id = $(this).attr('data-session_id');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_student_stuck_off', 'ajax_student_stuck_off&temp=student'); ?>", {action: 'stuck_off_student', student_id: student_id, session_id: session_id}, function (data) {
            if (data === 'success') {
                window.location.href = "<?php echo make_admin_url('student', 'stuck_off', 'stuck_off') ?>";
            }
        });
    });

    $(document).on('click', '#not_allowed_stuck_off', function () {
        toastr.error('Student not stucked off.Please Add student to session first!', 'Error');
    });

    // Send Login SMS
    $(document).on('click', '#send_login_sms', function (e) {
        e.preventDefault();
        var el = $(this);
        var html = el.html();
        el.html('Sending..');
        var student_id = $(this).attr('data-student_id');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=student'); ?>", {act: 'send_login_sms', student_id: student_id}, function (data) {
            if (data === 'success') {
                toastr.success('Sms Sent Successfully!', 'Success');
            } else if (data === 'failed') {
                toastr.error('Sms Sending Failed!', 'Failed');
            } else {
                toastr.info('Sms Service Disabled!', 'Info');
            }
            el.html(html);
        });
    });

    // Send Login Email
    $(document).on('click', '#send_login_email', function (e) {
        e.preventDefault();
        var el = $(this);
        var html = el.html();
        el.html('Sending..');
        var student_id = $(this).attr('data-student_id');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=student'); ?>", {act: 'send_login_email', student_id: student_id}, function (data) {
            if (data === 'success') {
                toastr.success('Email Sent Successfully!', 'Success');
            } else if (data === 'failed') {
                toastr.error('Email Sending Failed!', 'Failed');
            } else {
                toastr.info('Email Service Disabled!', 'Info');
            }
            el.html(html);
        });
    });
</script>