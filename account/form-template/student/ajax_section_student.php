<?
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');

if ($_POST):
    isset($_POST['session_id']) ? $session_id = $_POST['session_id'] : $session_id = '';
    isset($_POST['ct_sec']) ? $ct_sec = $_POST['ct_sec'] : $ct_sec = '';
    isset($_POST['sess_int']) ? $sess_int = $_POST['sess_int'] : $sess_int = '';
    if ($session_id == '0'):
        $obj = new studentSession;
        $record = $obj->SelectedSessionStudents($sess_int);
    //pr($record);
    else:
        #Get Section Students
        $QueryOb = new studentSession();
        $record = $QueryOb->sectionSessionStudents($session_id, $ct_sec);
    endif;
    ?>			
    <table class="table table-striped table-bordered table-hover" id='sample_2'>
        <thead>
            <tr>
                <th class='hidden-480 sorting_disabled'>Sr.No.</th>
                <th>Reg ID.</th>
                <th>Name</th>
                <th>Father Name</th>
                <th>Current Session</th>
                <th>Login Details</th>
                <th>Action</th>
            </tr>
        </thead>
        <? if (!empty($record)): ?>
            <tbody>
                <?php
                $sr = 1;
                foreach ($record as $s_k => $object):
                    $CurrObj = new studentSession();
                    $current_session = $CurrObj->getStudentCurrentSession($school->id, $object->id);
                    $Obj = new studentSession();
                    $get_session_id = $CurrObj->getStudentCurrentSession($school->id, $object->id, TRUE);
                    ?>
                    <tr class="odd gradeX current_student" rel="<?= $object->id ?>">	
                        <td class='hidden-480 sorting_disabled'><?php echo $sr++; ?>.</td>
                        <td><?= $object->reg_id ?></td>	
                        <td><?= $object->first_name . " " . $object->last_name ?></td>
                        <td><?= $object->father_name ?></td>
                        <td><?= $current_session ?></td>
                        <td>
                            <?php if (!empty($object->student_login_id) || !empty($object->password)) { ?>
                                <a id="send_login_sms" data-student_id="<?= $object->id ?>" class="btn mini green icn-only tooltips" href="" title="Send Login Details Via SMS">SMS</a>&nbsp;&nbsp;
                                <a id="send_login_email" data-student_id="<?= $object->id ?>" class="btn mini blue icn-only tooltips" href="" title="Send Login Details Via Email">Email</a>&nbsp;&nbsp;
                            <?php } else { ?>
                                <span class="text-error">No Details Found</span>
                            <?php } ?>
                        </td>
                        <td>
                            <a class="btn mini green icn-only tooltips" href="<?php echo make_admin_url('messages', 'compose', 'compose', 'user_id=' . $object->id . '&user_type=student') ?>" title="click here to send message">Send Message</a>&nbsp;&nbsp;
                            <a class="btn mini purple icn-only tooltips" href="<?php echo make_admin_url('view', 'list', 'list', 'id=' . $object->id) ?>" title="click here to see the full profile">Profile</a>&nbsp;&nbsp;
                            <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('student', 'update', 'update', 'id=' . $object->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <?php
                endforeach;
                ?>
            </tbody>
        <? else: ?>
            <tr><td></td><td colspan='4'>Sorry No Record Found..!</td></tr>									
        <?php endif; ?>  
    </table>			
    <?
endif;
?>
