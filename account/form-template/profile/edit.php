
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Change Password
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li><a href="<?php echo make_admin_url('profile', 'list', 'list');?>">Institute Profile</a>
									<i class="icon-angle-right"></i></li>
									<li>Change Password</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
				</div>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<form class="form-horizontal" action="<?php echo make_admin_url('profile', 'update', 'update&id='.$object->id)?>" method="POST" enctype="multipart/form-data" id="validation">
                        <div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-cogs"></i>Change Password</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body form">  
							
							<!-- Update Case -->	
								<form class="form-horizontal" action="<?php echo make_admin_url('profile', 'list', 'list&id='.$object->id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          										
										<h4 class="form-section hedding_inner">Change Password</h4>
										<div class="row-fluid">
										<div class="span12" id='large_space'>
						                    <div class="control-group">
                                                    <label class="control-label" for="password">Password<span class="required">*</span></label>
                                                    <div class="controls">
                                                          <input type="text" id="password" name='password' value='<?=$school->password?>' class="span6 m-wrap validate[required]"/>
													</div>
                                            </div>
										</div>
										</div>
										    <div class="form-actions">
													<input type="hidden" name="hidden" value="1" tabindex="7" /> 
													 <input type="hidden" name="is_active" value="1" tabindex="7" /> 
													 <input type="hidden" name="id" value="<?=$school->id?>" tabindex="7" /> 
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('profile', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                             </div>
		
							</div>
							
						</div>
						
                        </form>	                      
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

