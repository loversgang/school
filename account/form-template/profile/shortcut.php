
        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('profile', 'list', 'list');?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-user"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Institute Profile
                        </div>
                </div>
            </a>   
        </div>
		
        <div class="tile bg-red <?php echo ($section=='update')?'selected':''?>">
            <a href="<?php echo make_admin_url('profile', 'update', 'update');?>">
                <div class="corner"></div>
                <div class="tile-body">
                        <i class="icon-lock"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Change Password
                        </div>
                </div>
            </a> 
        </div>
		
