
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
             <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Institute Profile
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        Institute Profile
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>           

            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<?php include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
   				</div>         
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
            <!-- BEGIN PAGE CONTENT-->
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('profile', 'list', 'list&id='.$object->id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box green">
                               
									<div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Institute Profile</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                <div class="portlet-body form">
									<div class="alert alert-block alert-error fade in"> 
									<!-- School Info  -->
										<h4 class="form-section hedding_inner">Institute Info</h4>
										<div class="row-fluid">
                                            <div class="control-group">
                                                    <label class="control-label" for="school_name">Institute Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled="disabled" value="<?=$object->school_name?>" id="school_name" class="span4 m-wrap validate[required]"/>
                                                    </div>
                                            </div>										
										</div>
										<div class="row-fluid">
										
										<div class="span6 ">

                                            <div class="control-group">
                                                    <label class="control-label" for="username">Username<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled="disabled" value="<?=$object->username?>" id="username" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>											
                                            <div class="control-group">
                                                    <label class="control-label" for="address1">Address1<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled="disabled" value="<?=$object->address1?>" id="address1" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="city">City<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled="disabled" value="<?=$object->city?>" id="city" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>												
                                            <div class="control-group">
                                                    <label class="control-label" for="state">State<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled="disabled" value="<?=$object->state?>" id="state" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 
											
										</div>
										<div class="span6 ">	
                                            <div class="control-group">
                                                    <label class="control-label" for="school_type">Institute Type<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select  id="school_type"  class="span10 m-wrap" disabled="disabled">
														<?php $sr=1;while($s_type=$QueryS_type->GetObjectFromRecord()):?>
																	<option value='<?=$s_type->id?>' <? if($school->school_type==$s_type->id){ echo 'selected';} ?> disabled="disabled"><?=ucfirst($s_type->name)?></option>
														<? $sr++; endwhile;?>
                                                        </select>                      
													</div>
                                            </div> 
										
                                            <div class="control-group">
                                                    <label class="control-label" for="address2">Address2</label>
                                                    <div class="controls">
                                                      <input type="text" disabled="disabled" value="<?=$object->address2?>" id="address2" class="span10"/>
                                                    </div>
                                            </div>											
  
                                            <div class="control-group">
                                                    <label class="control-label" for="District">District<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled="disabled" value="<?=$object->district?>" id="district" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>   
                                            <div class="control-group">
                                                    <label class="control-label" for="country">Country<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" disabled="disabled" value="<?=$object->country?>" id="country" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 
										</div>	
										</div>	
									</div>
									
									<!-- Contact Info  -->
										<h4 class="form-section hedding_inner">Institute Logo <font style='font-size:13px;'>(Institute Logo should be 150px(width)*100px(height))</font></h4>
										<div class="row-fluid">
											<div class='span6'>
                                            <div class="control-group">
                                                    <label class="control-label" for="image">Logo<br/><font style='font-size:12px;'>(You can change your logo)</font></label>
                                                    <div class="controls">
													<? if($object->logo):
															$im_obj=new imageManipulation()?>
														<div class="item span10" style='text-align:center;'>	
														<a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<?=$im_obj->get_image('school','large',$object->logo);?>">
															<div class="zoom">
																<img src="<?=$im_obj->get_image('school','medium',$object->logo);?>" />
																<div class="zoom-icon"></div>
															</div>
														</a>
														<div class="details">
															<a href="<?=make_admin_url('profile','delete_image','delete_image&id='.$object->id);?>" class="icon" onclick="return confirm('Are you sure? You are deleting this image.');" title="click here to delete this image"><i class="large icon-remove"></i></a>    
														</div>	
														</div>															
													<? else:?>
                                                      <input type="file" name="image"  id="image" class="span10 m-wrap"/>
													<? endif;?> 
                                                    </div>
                                            </div>
											</div>		
										</div>	
											
									
									<!-- Contact Info  -->
										<h4 class="form-section hedding_inner">Contact Info</h4>
										<div class="row-fluid">
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="school_head_name">Institute Head Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="school_head_name" value="<?=$object->school_head_name?>" id="school_head_name" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 	
                                            <div class="control-group">
                                                    <label class="control-label" for="school_head_email">Institute Head Email<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="school_head_email" value="<?=$object->school_head_email?>" id="school_head_email" class="span10 m-wrap validate[required,custom[email]]"/>
                                                    </div>
                                            </div>  											
                                            <div class="control-group">
                                                    <label class="control-label" for="phone1_contact_name">Contact Person Name1<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="phone1_contact_name" value="<?=$object->phone1_contact_name?>" id="phone1_contact_name" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 										
									
                                            <div class="control-group">
                                                    <label class="control-label" for="phone2_contact_name">Contact Person Name2</label>
                                                    <div class="controls">
                                                      <input type="text" name="phone2_contact_name" value="<?=$object->phone2_contact_name?>" id="phone2_contact_name" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="email_address">Institute Email<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="email_address" value="<?=$object->email_address?>" id="email_address" class="span10 m-wrap validate[required,custom[email]]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="website_url">Website Url</label>
                                                    <div class="controls">
                                                      <input type="text" name="website_url" value="<?=$object->website_url?>" id="website_url" class="span10 m-wrap"/>
                                                    </div>
                                            </div>  											
										</div>
										<div class="span6 ">	
                                            <div class="control-group">
                                                    <label class="control-label" for="school_head_phone">Mobile No.<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="school_head_phone" value="<?=$object->school_head_phone?>" id="school_head_phone" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="fax">Fax<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="fax" value="<?=$object->fax?>" id="fax" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 											
                                            <div class="control-group">
                                                    <label class="control-label" for="phone1">Contact Person Phone1<span class="required" style='padding-left:0px;'>*</span></label>
                                                    <div class="controls">
                                                      <input type="text" name="phone1" value="<?=$object->phone1?>" id="phone1" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>  										
                                            <div class="control-group">
                                                    <label class="control-label" for="phone2">Contact Person Phone2</label>
                                                    <div class="controls">
                                                      <input type="text" name="phone2" value="<?=$object->phone2?>" id="phone2" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 
											
                                            <div class="control-group">
                                                    <label class="control-label" for="school_phone">Institute Landline No.</label>
                                                    <div class="controls">
                                                      <input type="text" name="school_phone" value="<?=$object->school_phone?>" id="school_phone" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 											
										</div>
										</div>	

									<!-- Other Info  -->
										<h4 class="form-section hedding_inner">Other Info</h4>
										
										<div class="row-fluid">
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="affiliation_board">Affiliation Board</label>
                                                    <div class="controls">
                                                      <input type="text" name="affiliation_board" value="<?=$object->affiliation_board?>" id="affiliation_board" class="span10 m-wrap"/>
                                                    </div>
                                            </div>
                                            <div class="control-group">
                                                    <label class="control-label" for="established_year">Established Year</label>
                                                    <div class="controls">
                                                      <input type="text" name="established_year" value="<?=$object->established_year?>" id="established_year" class="span10 m-wrap"/>
                                                    </div>
                                            </div>												
										</div>
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="affiliation_code">Affiliation Code</label>
                                                    <div class="controls">
                                                      <input type="text" name="affiliation_code" value="<?=$object->affiliation_code?>" id="affiliation_code" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 
										</div>
										</div>
										
									<? /*	
									<!-- Other Info  -->
										<h4 class="form-section hedding_inner">Subscription Plan Info</h4>
										<!-- Get Subscription Name -->
										<? $sub=get_object('subscription_plan',$object->subscription_id);
										if($sub):?>
										<div class="row-fluid">
										<div class="span12 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="subscription_id">Subscription Plan</label>
                                                    <div class="controls">
                                                     <input type="text" value="<?=$sub->name?>" disabled = "disabled"  class="span4 m-wrap"/>
													</div>
                                            </div>
										</div>	
										</div>
										<? endif;?>
										<div class="row-fluid" id='dynamic_model'>
										<div class="span6 ">	
                                            <div class="control-group">
                                                    <label class="control-label" for="set_up_fee">Set-up Fee (<?=CURRENCY_SYMBOL?>)</label>
                                                    <div class="controls">
                                                      <input type="text" value="<?=$object->set_up_fee?>" disabled = "disabled" id="set_up_fee" class="span10 m-wrap"/>
                                                    </div>
                                            </div>	
                                            <div class="control-group">
                                                    <label class="control-label" for="payment_type">Subscription Type</label>
                                                    <div class="controls">
                                                      <input type="text" value="<?=$object->payment_type?>" disabled = "disabled" id="payment_type" class="span10 m-wrap"/>
                                                    </div>
                                            </div>												
										</div>
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="subscription_fee">Subscription Fee (<?=CURRENCY_SYMBOL?>)</label>
                                                    <div class="controls">
                                                      <input type="text" value="<?=$object->subscription_fee?>" id="subscription_fee" disabled = "disabled" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 
										</div>
										</div>
									*/ ?>							
										
                                   
                                            <div class="form-actions">
													 <input type="hidden" name="hidden" value="1" tabindex="7" /> 
													 <input type="hidden" name="is_active" value="1" tabindex="7" /> 
													 <input type="hidden" name="id" value="<?=$school->id?>" tabindex="7" /> 
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('profile', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                             </div>
                                     
                              </div> 
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
	     </div>
             <div class="clearfix"></div>
    </div>
		
