<div class="tiles pull-right">

        <div class="tile bg-yellow <?php echo ($setting_type=='general')?'selected':''?>">
            <a href="<?php echo make_admin_url('setting', 'list', 'list','sname=general'.'&setting_type=general');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-cogs"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                General Settings
                        </div>
                </div>
            </a>   
        </div>




        <div class="tile bg-yellow <?php echo ($setting_type=='website')?'selected':''?>">
            <a href="<?php echo make_admin_url('setting', 'list', 'list','sname=action'.'&setting_type=website');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-cloud"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Website Settings
                        </div>
                </div>
            </a> 
        </div>

        <div class="tile bg-yellow <?php echo ($setting_type=='social')?'selected':''?>">
            <a href="<?php echo make_admin_url('setting', 'list', 'list','sname=twitter'.'&setting_type=social');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-group"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Social Media Settings
                        </div>
                </div>
            </a> 
        </div>

</div>