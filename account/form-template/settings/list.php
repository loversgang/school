
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Settings <small>manage website settings</small>
                            </h3>
                            <!--
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>Settings</li>

                            </ul>-->


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            <?php  #include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<div class="span3">
							<ul class="ver-inline-menu tabbable margin-bottom-10">
                                                            
                                                             
                                                            
                                                                    <li class="<?php echo $sname=='general'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=general'.'&setting_type=general');?>"><i class="icon-cogs"></i> General Settings</a></li>
                                                                    <li class="<?php echo $sname=='email'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=email'.'&setting_type=website');?>"><i class="icon-table"></i> Notification Emails </a></li>
                                                                    <li class="<?php echo $sname=='site-verify'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=site-verify'.'&setting_type=website');?>"><i class="icon-tint"></i> Site Verifications</a></li>
                                                                    <li class="<?php echo $sname=='analytics'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=analytics'.'&setting_type=website');?>"><i class="icon-google-plus-sign"></i> Google Analytics</a></li>
                                                                    <li class="<?php echo $sname=='social'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=social'.'&setting_type=social');?>"><i class="icon-group"></i>  Social Media</a></li>
                                                                    <li class="<?php echo $sname=='cache-debugging'?'active':''?>"><a href="<?php echo make_admin_url('setting', 'list', 'list', 'sname=cache-debugging'.'&setting_type=cache');?>"><i class="icon-magic"></i>  Cache</a></li>
                                                         
                                                                   
                                                           
                                                            
                                                            
								
							</ul>
						</div>
						
                                                  <!-- / Box -->
                                                  <div class="span9">
                                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                     <div class="portlet box light-grey">
                                                            <div class="portlet-title">
                                                                    <div class="caption"></i><?php echo ucfirst($sname);?> Settings </div>
                                          
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <form class="form-horizontal" action="<?php echo make_admin_url('setting', 'update', 'list');?>" method="POST" enctype="multipart/form-data" id="validation">
                                                                 <?php
                                                            
                                                                    foreach($ob as $kk=>$vv):
                                                                 ?>
                                                                         <div class="control-group">
                                                                                <label class="control-label"><?php echo ucfirst($vv['title']);?>:</label>
                                                                                <div class="controls">
                                                                                    <?php echo get_setting_control($vv['id'], $vv['type'], $vv['value']);?>
                                                                                </div>    
                                                                        </div>
                                                                    
                                                                       
                                                                    
                                                                    
                                                                    

                                                              <?php endforeach;?>   
                                                                <?php if($sname=='cache-debugging'):?>
                                                                         <div class="control-group">
                                                                                <label class="control-label">Clear all cache:</label>
                                                                                <div class="controls">
                                                                                    
                                                                                   <a style="cursor: pointer;line-height:32px" onclick="javascript:r=confirm('Press &quot;OK&quot; if you want to proceed with deleting or press &quot;Cancel&quot; to go back.'); if(r==true){ window.location='<?php echo make_admin_url('setting', 'list', 'list', 'clearcache');?>'}">click to clear all cache</a>
                                                                       
                                                                                </div>
                                                                         </div>       
                                                                 <?php endif;?>
                                                                
                                                           
                                                                <div class="form-actions">
                                                                    
                                                                         <input  type="hidden" name="sname1" value="<?php echo $sname?>"/>
                                                                         <input  type="hidden" name="setting_type" value="<?php echo $setting_type?>"/>
                                                                         
                                                                         <input class="btn grey" type="submit" name="Submit" value="Submit" tabindex="7" /> 
                                                                         <a href="<?php echo make_admin_url('setting', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>

                                                                </div>

                                                                </form>

                                                            </div> 
                                                    </div>
                                                </div>

						<!--end span9-->                                   
					</div>
				</div>

            <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    





