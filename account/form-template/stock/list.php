<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Stock
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>List Stock</li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>

    <div class="clearfix"></div>
    <div style="clear:both;"></div>	
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-file-text"></i>Manage Stock</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('category', 'list', 'list'); ?>" method="post" id="form_data" name="form_data" >	
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>Product</th>
                                    <th>Available</th>					
                                    <th>Quantity</th>					
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($stocks as $key => $stock) { ?>
                                    <tr>
                                        <td>
                                            <?php
                                            $sales_category = get_object('school_sales_category', $stock['sales_cat_id']);
                                            echo $sales_category->title;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $sales = get_object('school_sales', $stock['sales_id']);
                                            echo $sales->name;
                                            ?>
                                        </td>		
                                        <td><?php echo $stock['stock_available'] ?></td>	
                                        <td><?php echo $stock['stock'] ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>                          
                        </table>
                    </form>    
                </div>
            </div>                                                
        </div>
    </div>
</div>