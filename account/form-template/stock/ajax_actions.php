<?php
include_once(DIR_FS_SITE . 'include/functionClass/school_salesClass.php');
if (isset($_POST)) {
    extract($_POST);

    if ($action == 'sales_using_cat_id') {
        $query = new school_sales;
        $sales = $query->sales_using_sales_cat_id($sales_cat_id);
        if (count($sales) > 0) {
            ?>
            <div class="control-group" id="product_found">
                <label class="control-label" for="school_sales">Sales<span class="required">*</span></label>
                <div class="controls">
                    <select class="span6 validate[required]" name="sales_id" id="sales_id">
                        <?php foreach ($sales as $sale) { ?>
                            <option value="<?php echo $sale['id'] ?>"><?php echo $sale['name'] ?></option>
                            <?php ?>
                        </select>
                    </div>
                </div>
                <?php
            }
        } else {
            ?>
            <div class="control-group" id="no_product">
                <div class="controls">
                    <div class="alert alert-info span6">no product available for this category</div>
                </div>
            </div>
            <?php
        }
    }
}
?>