<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Stock
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('stock', 'list', 'list'); ?>">List Stock</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add Stock
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal" method="POST" enctype="multipart/form-data" id="validation">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Add New Stock</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form product_div">
                        <div class="control-group">
                            <label class="control-label" for="sales_category">Sales Category<span class="required">*</span></label>
                            <div class="controls">
                                <select class="span6 validate[required]"  name="sales_cat_id" id="sales_cat_id">
                                    <option value="">Select Product Category</option>
                                    <?php foreach ($sales_category as $sale_category) { ?>
                                        <option value="<?php echo $sale_category['id'] ?>"><?php echo $sale_category['title'] ?></option>

                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div id="sales"></div>
                        <div class="control-group">
                            <label class="control-label" for="stock_available">Stock Available<span class="required">*</span></label>
                            <div class="controls">
                                <select class="span6 validate[required]"  name="stock_available" id="stock_available">
                                    <option value="YES">YES</option>
                                    <option value="NO">NO</option>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="stock">How Much Stock Available<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="stock" id="stock" class="span6 m-wrap validate[required]" />
                            </div>
                        </div>  
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" id="submit" /> 
                            <a href="<?php echo make_admin_url('stock', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>

                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>

<script>
    $(document).on('change', '#sales_cat_id', function () {
        var sales_cat_id = $(this).val();
        if (sales_cat_id) {
            $('#sales').html('<center>Loading...</center>');
            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=stock'); ?>', {action: 'sales_using_cat_id', sales_cat_id: sales_cat_id}, function (data) {
                $('#sales').html(data);
                $('#product_found').closest('.product_div').find('#stock_available').attr('disabled', false).$('#submit').attr('disabled', false);
                $('#no_product').closest('.product_div').find('#stock_available').attr('disabled', true);
                $('#no_product').closest('.product_div').find('#stock').attr('disabled', true).val(0);
                ;
                $('#no_product').closest('.product_div').find('#stock_available option[value="NO"]').attr('selected', true);
                $('#no_product').closest('.product_div').find('#submit').attr('disabled', true);
            });
        } else {
            $('#sales').find('.control-group').remove();

        }
    });
    $(document).on('change', '#stock_available', function () {
        var stock_available = $(this).val();
        if (stock_available == 'NO') {
            $('#stock').val(0);
            $('#stock').attr('disabled', true);
        } else {
            $('#stock').val('');
            $('#stock').attr('disabled', false);
        }
    });
</script>