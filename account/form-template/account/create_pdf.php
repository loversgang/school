	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
<style>
body {
    color: #000000;
    direction: ltr;
    font-family: 'Open Sans';
    font-size: 13px;
	background-color: #FFF !important;
}
.page-title {
    color: #666666;
    display: block;
    font-family: 'Open Sans';
    font-size: 30px;
    font-weight: 300;
    letter-spacing: -1px;
    margin: 20px 0 15px;
    padding: 0;
}
.portlet.box.blue {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color #B4CEF8 #B4CEF8;
    border-image: none;
    border-right: 1px solid #B4CEF8;
    border-style: none solid solid;
    border-width: 0 1px 1px;
}
.portlet.box {
    padding: 0 !important;
}
.portlet-body.blue, .portlet.blue {
    background-color: #4B8DF8 !important;
}
.portlet {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}

.portlet-title .caption {
    display: inline-block;
    float: left;
    font-size: 18px;
    font-weight: 400;
    margin: 0 0 7px;
    padding: 0 0 8px;
}
</style>


<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');

isset($_REQUEST['from'])?$from=$_REQUEST['from']:$from='';
isset($_REQUEST['to'])?$to=$_REQUEST['to']:$to='';
isset($_REQUEST['cat'])?$cat=$_REQUEST['cat']:$cat='';

		#Get Exp Category
		$QuerySalCat = new staffCategory();
		$QuerySalCat->listAll($school->id);	
	
		#Get Records
		$QueryObj = new staffSalary();
		$record=$QueryObj->SalaryReport($school->id,$from,$to,$cat);
?>

<? if(!empty($record)):?>
  <!-- BEGIN PAGE CONTAINER-->
    <div style='padding-left:20px;padding-right:20px;'>
            <!-- BEGIN PAGE HEADER-->

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    <?=$school->school_name?>
                            </h3>
 
                            <!-- END PAGE TITLE & BREADCRUMB-->


				<div style='margin-left:0px;padding-right:0px;'>
				<table>
					<? if($cat): $category=get_object('staff_category_master',$cat);?>
						<tr><td>Category Name </td><td>&nbsp;:&nbsp;</td><td><?=$category->name;?> </td></tr>
					<? endif;?>					
					<tr><td>From Date </td><td>&nbsp;:&nbsp;</td><td><?=$from?></td></tr>
					<tr><td>To Date </td><td>&nbsp;:&nbsp;</td><td><?=$to?> </td></tr>
				</table>
				</div>

              <!-- BEGIN PAGE CONTENT-->

					
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box blue">
							<div class="portlet-title">
								<div class="caption">Salary Report</div>								
							</div>
							<div class="portlet-body">
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover">
									<thead>
										 <tr>
												<th style="width:10%px;">Sr. No.</th>
												<th style='width:30%;'>Staff Name</th>
												<th class="hidden-480 sorting_disabled" style='width:20%;'>Category</th>
												<th style='text-align:center;width:20%;' class="hidden-480 sorting_disabled" >Date</th>
												<th class="hidden-480 sorting_disabled" style='width:20%;text-align:right;'>Amount </th>
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';?>
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):?>
                                                <tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td><?=$object->title.' '.ucfirst($object->first_name).' '.$object->last_name?></td>
													<td class="hidden-480 sorting_disabled"><?=$object->category?></td>
													<td style='text-align:center' class="hidden-480 sorting_disabled"><?=$object->payment_date?></td>
													<td style='text-align:right;'><?=number_format($object->amount,2)?></td>													
												</tr>
                                            <?php $sr++; $sum=$sum+$object->amount;
												endforeach;?>
										</tbody>
									   <?php endif;?>  
								</table>
									<? if(!empty($record)):?>
											<table class="table table-striped table-hover">
											<thead>
												 <tr>
														<th style="width:10%px;"></th>
														<th style='width:30%;'></th>
														<th class="hidden-480 sorting_disabled" style='width:20%;'></th>
														<th style='text-align:right;width:20%;padding-right:1px;' class="hidden-480 sorting_disabled">Total =</th>
														<th class="hidden-480 sorting_disabled" style='width:20%;text-align:right;'><?=number_format($sum,2)?></th>
												</tr>
											</thead>	
											</table>
									<?php endif;?> 	
                            </form> 									
								</div>
							</div>
						</div>
					
	
    </div>
<? endif;?>	