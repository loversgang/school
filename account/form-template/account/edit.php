

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Accounting Reports
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 
                                    <li>
                                            <i class="icon-list"></i>
                                            <a href="<?php echo make_admin_url('report', 'list', 'list');?>">List</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 
                                    <li class="last">
                                        Income Report
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div> 			
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-left">
				<form class="form-horizontal" action="<?php echo make_admin_url('account', 'student', 'student')?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
				<input type='hidden' name='Page' value='account'/>
				<input type='hidden' name='action' value='update'/>
				<input type='hidden' name='section' value='update'/>
				
				<div class="control-group alert alert-success span8" style='margin-left:0px;padding-right:0px;'>
					<div class='span2' style='margin-left: 5px;'>
					<label class="control-label" style='float: none; text-align: left;'>From Date</label>
						<input type='text' class="span2 upto_current_date" placeholder="Select date" name='from' value='<?=$from?>'/>
					</div>
					
					<div class='span2' style='margin-left: 5px;'>
					<label class="control-label" style='float: none; text-align: left;'>To Date</label>
						<input type='text' class="span2 upto_current_date" placeholder="Select date" name='to' value='<?=$to?>'/>
					</div>	
					
					<div class='span4' style='margin-left: 5px; padding-right:20px'>
					<label class="control-label" style='float: none; text-align: left;'>Select Income Category</label>
							<select class="select2_category span3" data-placeholder="Select Session Students" name='cat' >
								<option value="">Select All</option>
								<?php $session_sr=1;while($IncCat=$QueryIncCat->GetObjectFromRecord()): ?>
										<option value='<?=$IncCat->id?>' <? if($IncCat->id==$cat){ echo 'selected'; }?> ><?=ucfirst($IncCat->title)?></option>
								<? $session_sr++; endwhile;?>
							</select>	
						<input type='submit' name='go' value='Go' class='btn blue'/>
					</div>
					<div class="clearfix"></div>
					&nbsp;&nbsp;You can generate report for the maximum three months.
				</div>
				</form>										
				</div>	
				
				
				<div class="tiles pull-right">
								
								 <? if(!empty($record)): ?>
									<div class="tile bg-yellow " id="print_document">							
												<a id='print'>
													<div class="corner"></div>
													<div class="tile-body"><i class="icon-print"></i></div>
													<div class="tile-object"><div class="name">Print</div></div>
												</a>
									</div><br/>
									<a href='<?=make_admin_url('account','pdf','pdf&type=income&from='.$from.'&to='.$to.'&cat='.$cat)?>' class="btn green"><i class="icon-download"></i> PDF</a>
									<a href='<?=make_admin_url('account','download','download&type=income&from='.$from.'&to='.$to.'&cat='.$cat)?>' class="btn blue"><i class="icon-download"></i> Excel</a>
								<? endif; ?>			
								
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<form class="form-horizontal" action="<?php echo make_admin_url('account', 'lsit', 'lsit')?>" method="POST" enctype="multipart/form-data" id="validation">
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-rupee"></i>Income Report</div>								
							</div>
							<div class="portlet-body">
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
												<th style="width:10%px;">Sr. No</th>
												<th style='width:25%;'>Title</th>
												<th style='width:25%;'>Description</th>
												<th class="hidden-480 sorting_disabled" style='width:20%;'>Category</th>
												<th style='text-align:center;width:10%;' class="hidden-480 sorting_disabled" >Date</th>
												<th class="hidden-480 sorting_disabled" style='width:10%;text-align:right;'>Amount (<?=CURRENCY_SYMBOL?>)</th>
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';?>
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):?>
                                                <tr class="odd gradeX">
													<td><?=$sr?>.</td>
													<td><?=$object->title?></td>
													<td><?=html_entity_decode($object->remarks)?></td>
													<td class="hidden-480 sorting_disabled"><?=$object->category?></td>
													<td style='text-align:center' class="hidden-480 sorting_disabled"><?=$object->on_date?></td>
													<td style='text-align:right;'><?=CURRENCY_SYMBOL." ".number_format($object->amount,2)?></td>													
												</tr>
                                            <?php $sr++; $sum=$sum+$object->amount;
												endforeach;?>
										</tbody>
									   <?php endif;?>  
								</table>
									<? if(!empty($record)):?>
											<table class="table table-striped table-hover">
											<thead>
												 <tr>
														<th style="width:10%px;"></th>
														<th style='width:25%;'></th>
														<th style="width:25%px;"></th>
														<th class="hidden-480 sorting_disabled" style='width:20%;'></th>
														<th style='text-align:right;width:10%;padding-right:1px;' class="hidden-480 sorting_disabled" >Total (<?=CURRENCY_SYMBOL?>) =</th>
														<th class="hidden-480 sorting_disabled" style='width:10%;text-align:right;'><?=CURRENCY_SYMBOL." ".number_format($sum,2)?></th>
												</tr>
											</thead>	
											</table>
									<?php endif;?> 	
                            </form> 									
								</div>
							</div>
						</div>
						</form>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
	
<script type="text/javascript">
	$("#print").live("click",function(){  
			var from='<?=$from?>';
			var to='<?=$to?>';
			var cat='<?=$cat?>';
				var dataString = 'from='+from+'&to='+to+'&cat='+cat;				
				$.ajax({
				type: "POST",						
				url: "<?=make_admin_url_window('ajax_calling','edit_rep','edit_rep&temp=account');?>",
				data: dataString,
				success: function(data, textStatus) {
					var DocumentContainer = document.getElementById(data);
					var WindowObject = window.open('', "PrintWindow", "");
					WindowObject.document.writeln(data);
					WindowObject.document.close();
					WindowObject.focus();
					WindowObject.print();
					WindowObject.close();
				return false;
				}				
				});
	});
</script>			