<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Accounting Reports
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('report', 'list', 'list'); ?>">List</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li class="last">
                    Full Report
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div> 			
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-left">
        <form class="form-horizontal" action="<?php echo make_admin_url('account', 'insert', 'insert') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
            <input type='hidden' name='Page' value='account'/>
            <input type='hidden' name='action' value='report'/>
            <input type='hidden' name='section' value='report'/>

            <div class="control-group alert alert-success span5" style='margin-left:0px;padding-right:0px;'>
                <div class='span2' style='margin-left: 5px;'>
                    <label class="control-label" style='float: none; text-align: left;'>From Date</label>
                    <input type='text' class="span2 upto_current_date" placeholder="Select date" name='from' value='<?= $from ?>'/>
                </div>

                <div class='span3' style='margin-left: 5px;'>
                    <label class="control-label" style='float: none; text-align: left;'>To Date</label>
                    <input type='text' class="span2 upto_current_date" placeholder="Select date" name='to' value='<?= $to ?>'/>
                    &nbsp;&nbsp;&nbsp;
                    <input type='submit' name='go' value='Go' class='btn blue'/>
                </div>
                You can generate report for the maximum three months.
            </div>
        </form>										
    </div>	


    <div class="tiles pull-right">
        <div class="tile bg-yellow " id="print_document">							
            <a id='print'>
                <div class="corner"></div>
                <div class="tile-body"><i class="icon-print"></i></div>
                <div class="tile-object"><div class="name">Print</div></div>
            </a>	
        </div><br/>
        <a href='<?= make_admin_url('account', 'pdf', 'pdf&type=full&from=' . $from . '&to=' . $to) ?>' class="btn green"><i class="icon-download"></i> PDF</a>
        <a href='<?= make_admin_url('account', 'download', 'download&type=full&from=' . $from . '&to=' . $to) ?>' class="btn blue"><i class="icon-download"></i> Excel</a>


    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <form class="form-horizontal" action="<?php echo make_admin_url('account', 'lsit', 'lsit') ?>" method="POST" enctype="multipart/form-data" id="validation">
                <div class="portlet box yellow">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-rupee"></i>Full Report &nbsp;&nbsp;&nbsp;(<?= $from ?> to <?= $to ?>)</div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix"></div>
                        <div class="row-fluid">
                            <!-- List all Expenses-->
                            <div class='span6'>
                                <h4 class="form-section hedding_inner">Expenses</h4>

                                <!-- EXP Cat -->
                                <? if (!empty($CatExpRecord)): ?>															
                                    <div class="row-fluid">																	
                                        <table class="table table-striped table-hover">
                                            <? foreach ($CatExpRecord as $exp_key => $exp_cat): ?>
                                                <tr><td class='span8'><?= $exp_cat->category_name ?></td><td class='span3' style='text-align:right;'><?= CURRENCY_SYMBOL . ' ' . number_format($exp_cat->amount, 2) ?></td></tr>
                                                <?
                                                $exp_amount = $exp_amount + $exp_cat->amount;
                                            endforeach;
                                            ?>
                                            <tr><td class='span8'></td><td class='span3' style='text-align:right;'><strong><?= CURRENCY_SYMBOL . ' ' . number_format($exp_amount, 2) ?></strong></td></tr>	
                                        </table>
                                    </div>			
                                <? endif; ?>

                                <!-- Salary Exp -->
                                <?
                                if (!empty($SalRecord)):
                                    foreach ($SalRecord as $Sal_key => $Sal_cat):
                                        ?>
                                        <div class="row-fluid">
                                            <div class='span8 report_head' style='padding-left:2.0641%'>Salaries</div><div class='span4 report_head' style='text-align: right; padding-right: 8px;'><?= CURRENCY_SYMBOL . ' ' . number_format($Sal_cat->amount, 2) ?></div>
                                        </div>
                                        <?
                                        $exp_amount = $exp_amount + $Sal_cat->amount;
                                    endforeach;
                                endif;
                                ?>

                            </div>


                            <!-- List all Incomes-->
                            <div class='span6'>
                                <h4 class="form-section hedding_inner">Incomes</h4>
                                <? if (!empty($CatIncRecord)): ?>

                                    <div class="row-fluid">
                                        <table class="table table-striped table-hover">														
                                            <? foreach ($CatIncRecord as $Inc_key => $inc_cat): ?>
                                                <tr><td class='span8'><?= $inc_cat->category_name ?></td><td class='span3' style='text-align:right;'><?= CURRENCY_SYMBOL . ' ' . number_format($inc_cat->amount, 2) ?></td></tr>
                                                <?
                                                $inc_amount = $inc_amount + $inc_cat->amount;
                                            endforeach;
                                            ?>
                                            <tr><td class='span8'></td><td class='span3' style='text-align:right;'><strong><?= CURRENCY_SYMBOL . ' ' . number_format($inc_amount, 2) ?></strong></td></tr>	
                                        </table>
                                    </div>			
                                <? endif; ?>

                                <!-- Fee Exp -->
                                <?
                                if (!empty($FeeRecord)):
                                    foreach ($FeeRecord as $fee_key => $fee_cat):
                                        ?>
                                        <div class="row-fluid">
                                            <div class='span8 report_head' style='padding-left:2.0641%'>Student Fee</div><div class='span4 report_head' style='text-align: right; padding-right: 8px;'><?= CURRENCY_SYMBOL . ' ' . number_format($fee_cat->amount, 2) ?></div>
                                        </div>
                                        <?
                                        $inc_amount = $inc_amount + $fee_cat->amount;
                                    endforeach;
                                endif;
                                ?>

                            </div>
                        </div>
                        <div class="row-fluid">
                            <br/>
                        </div>											
                        <div class="row-fluid report_total">
                            <div class='span6'>
                                <div class="row-fluid">
                                    <div class='span8' style='padding-left:2.5641%'><h4 class="media-heading total_sum">Total</h4></div><div class='span4' style='text-align: right; padding-right: 8px;'>
                                        <h4 class="media-heading total_sum" style='text-rendering: auto'><?= CURRENCY_SYMBOL . ' ' . number_format($exp_amount, 2) ?></h4></div>
                                </div>
                            </div>
                            <div class='span6'>
                                <div class="row-fluid">
                                    <div class='span8' style='padding-left:2.5641%'><h4 class="media-heading total_sum">Total</h4></div><div class='span4' style='text-align: right; padding-right: 8px;'><h4 class="media-heading total_sum" style='text-rendering: auto'><?= CURRENCY_SYMBOL . ' ' . number_format($inc_amount, 2) ?></h4></div>
                                </div>												
                            </div>
                        </div><br/>
                        <div class="row-fluid">
                            <div class='span6'></div>
                            <div class='span6 report_total'> 
                                <div class="span8 report_head" style="padding-left:2.0641%">Net Income</div>
                                <div class="span4 report_head" style="text-align: right; padding-right: 8px; text-rendering: auto"><?= CURRENCY_SYMBOL . ' ' . number_format($inc_amount - $exp_amount, 2) ?></div>
                            </div>
                        </div>
                    </div>
            </form>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    $("#print").live("click", function () {
        var from = '<?= $from ?>';
        var to = '<?= $to ?>';

        var dataString = 'from=' + from + '&to=' + to;
        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'report_rep', 'report_rep&temp=account'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                var DocumentContainer = document.getElementById(data);
                var WindowObject = window.open('', "PrintWindow", "");
                WindowObject.document.writeln(data);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
                return false;
            }
        });
    });
</script>	