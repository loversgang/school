	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>


<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');

isset($_REQUEST['from'])?$from=$_REQUEST['from']:$from='';
isset($_REQUEST['to'])?$to=$_REQUEST['to']:$to='';
isset($_REQUEST['cat'])?$cat=$_REQUEST['cat']:$cat='';
isset($_REQUEST['session_id'])?$session_id=$_REQUEST['session_id']:$session_id='';
isset($_REQUEST['ct_sec'])?$ct_sec=$_REQUEST['ct_sec']:$ct_sec='';


		#Get Student previous Fee List
		$QueryRec = new studentFeeRecord();
		$record=$QueryRec->listReport($school->id,$from,$to,$session_id,$ct_sec);	
?>

<? if(!empty($record)):?>
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    <?=$school->school_name?>
                            </h3>
                            <ul class="breadcrumb hidden-print">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'update', 'update');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 

                                    <li class="last">
                                        Fee Report
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-left">

				
				<div class="control-group span12" style='margin-left:0px;padding-right:0px;'>
				<table>
					<? if($session_id): $category=get_object('session',$session_id);?>
						<tr><td>Session Name </td><td>&nbsp;:&nbsp;</td><td><?=$category->title;?> </td></tr>
					<? endif;?>	
					<? if($ct_sec):?>
						<tr><td>Section </td><td>&nbsp;:&nbsp;</td><td><?=$ct_sec?> </td></tr>
					<? endif;?>							
					<tr><td>From Date </td><td>&nbsp;:&nbsp;</td><td><?=$from?></td></tr>
					<tr><td>To Date </td><td>&nbsp;:&nbsp;</td><td><?=$to?> </td></tr>
				</table>
				</div>					
													
				</div>	
        
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<form class="form-horizontal" action="<?php echo make_admin_url('account', 'lsit', 'lsit')?>" method="POST" enctype="multipart/form-data" id="validation">
						<div class="portlet box purple">
							<div class="portlet-title">
								<div class="caption"><i class="icon-rupee"></i>Fee Report</div>								
							</div>
							<div class="portlet-body">
								<div class="clearfix">
							<form action="<?php echo make_admin_url('expense', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >		
								<table class="table table-striped table-bordered table-hover">
									<thead>
										 <tr>
												<th style='width:10%;'>Reg ID</th>
												<th style='width:20%;'>Name</th>
												<th style='width:20%;' class="hidden-480 sorting_disabled" >Father Name</th>
												<th style='width:25%;' class="hidden-480 sorting_disabled" >Session</th>
												<th style='text-align:center;width:15%;' class="hidden-480 sorting_disabled" >Date</th>
												<th class="hidden-480 sorting_disabled" style='text-align:right;width:10%;'>Amount (<?=CURRENCY_SYMBOL?>)</th>
										</tr>
									</thead>
                                        <? if(!empty($record)): $sum='';?>
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):?>
                                                <tr class="odd gradeX">
													<td><?=$object->reg_id?></td>
													<td><?=ucfirst($object->first_name)." ".$object->last_name?></td>
													<td><?=$object->father_name?></td>
													<td><?=$object->session_name?> (<?=$object->section?>)</td>													
													<td style='text-align:center' class="hidden-480 sorting_disabled"><?=$object->payment_date?></td>
													<td style='text-align:right;'><?=CURRENCY_SYMBOL." ".number_format($object->amount,2)?></td>													
												</tr>
                                            <?php $sr++; $sum=$sum+$object->amount;
												endforeach;?>
										</tbody>
									   <?php endif;?>  
								</table>
									<? if(!empty($record)):?>
											<table class="table table-striped table-hover">
											<thead>
												 <tr>
														<th style='width:10%;'></th>
														<th style='width:20%;'></th>
														<th style='width:20%;'></th>
														<th style='width:25%;'></th>														
														<th style='text-align:right;width:15%;padding-right:1px;' class="hidden-480 sorting_disabled" >Total (<?=CURRENCY_SYMBOL?>) =</th>
														<th class="hidden-480 sorting_disabled" style='width:10%;text-align:right;'><?=CURRENCY_SYMBOL." ".number_format($sum,2)?></th>
												</tr>
											</thead>	
											</table>
									<?php endif;?> 	
                            </form> 									
								</div>
							</div>
						</div>
						</form>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
    </div>
<? endif;?>		
	<script type="text/javascript">
	$(".session_filter").change(function(){	   
			var session_id=$("#session_id").val();
			
			if(session_id.length > 0){
					   var id=session_id;
					   $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
						
						var dataString = 'id='+id;
						
						$.ajax({
						type: "POST",						
						url: "<?=make_admin_url_window('ajax_calling','ajax_section','ajax_section&temp=account');?>",
						data: dataString,
						success: function(data, textStatus) {						
						$("#ajex_section").html(data);
						}				
						});
			}
			else{
				return false;
			}
	});

</script>