<?
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');

if ($_POST):
    isset($_POST['id']) ? $id = $_POST['id'] : $id = '';

    /* Get session Pages */
    $QuerySec = new studentSession();
    $QuerySec->listAllSessionSection($id);
    ?>
    <label class="control-label" style='float: none; text-align: left;'>Select Section</label>
    <select class="select2_category session_filter" data-placeholder="Select Session Students" name='ct_sec' style='width:105px'>
        <option value=''>All</option>
        <?php $session_sc = 1;
        while ($sec = $QuerySec->GetObjectFromRecord()): print_r($sec->section);
            ?>
            <option value='<?= $sec->section ?>'><?= ucfirst($sec->section) ?></option>
            <? $session_sc++;
        endwhile;
        ?>
    </select>
    <input type='submit' name='go' value='Go' class='btn blue'/>
    <?
endif;
?>