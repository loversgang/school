<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage School Expenses
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    List Expenses
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>

    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>    
    <div class="tiles pull-right">
        <div class="tile bg-yellow " id="print_document">							
            <a id='print'>
                <div class="corner"></div>
                <div class="tile-body"><i class="icon-print"></i></div>
                <div class="tile-object"><div class="name">Print</div></div>
            </a>	
        </div><br/>
        <a href='<?= make_admin_url('account', 'pdf', 'pdf&type=full&from=' . $from . '&to=' . $to) ?>' class="btn green"><i class="icon-download"></i> PDF</a>
        <a href='<?= make_admin_url('account', 'download', 'download&type=full&from=' . $from . '&to=' . $to) ?>' class="btn blue"><i class="icon-download"></i> Excel</a>


    </div>            
    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-rupee"></i>Manage Expenses</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row-fluid">
                        <div class="span4">
                            <div class="control-group">
                                <label class="control-label">Select Year</label>
                                <div class="controls">
                                    <select id="year">
                                        <?php foreach ($expense_years as $years) { ?>
                                            <option value="<?php echo $years->year ?>"><?php echo $years->year ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label class="control-label">Select Category</label>
                                <div class="controls">
                                    <select id="expense_cat">
                                        <option value="0">ALL</option>
                                        <?php foreach ($cats as $cat) { ?>
                                            <option value="<?php echo $cat->id ?>"><?php echo $cat->title ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="span1">
                            <div class="control-group">
                                <label class="control-label">&nbsp;</label>
                                <div class="controls">
                                    <button type="button" id="go_button" class="btn blue">Go</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="expense_data"></div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    
<script>
    var ajax_url = "<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=expense'); ?>";
    $(document).ready(function () {
        $('#go_button').click();
    });
    $(document).on('click', '#go_button', function () {
        var year = $('#year').val();
        var cat = $('#expense_cat').val();
        $('#expense_data').html('<div style="width:100%;text-align:center;padding-top:50px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post(ajax_url, {act: 'get_expense_data', year: year, cat: cat}, function (data) {
            $('#expense_data').html(data).slideDown();
            TableManaged.init();
        });
    });
</script>


