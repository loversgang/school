	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');

isset($_REQUEST['from'])?$from=$_REQUEST['from']:$from='';
isset($_REQUEST['to'])?$to=$_REQUEST['to']:$to='';
isset($_REQUEST['cat'])?$cat=$_REQUEST['cat']:$cat='';
isset($_REQUEST['session_id'])?$session_id=$_REQUEST['session_id']:$session_id='';
isset($_REQUEST['ct_sec'])?$ct_sec=$_REQUEST['ct_sec']:$ct_sec='';


		#Get Exp Records
		$QueryObjExp = new schoolExpense();
		$CatExpRecord=$QueryObjExp->CatExpenseReport($school->id,$from,$to);	

	
		#Get Salary Exp Records
		$QuerySalObj = new staffSalary();
		$SalRecord=$QuerySalObj->SumSalaryReport($school->id,$from,$to);
		
		#Get Income Records
		$QueryObjInc = new schoolIncome();
		$CatIncRecord=$QueryObjInc->CatIncomeReport($school->id,$from,$to);

		#Get Fee Income Records
		$QueryFeeObj = new studentFeeRecord();
		$FeeRecord=$QueryFeeObj->OverAlllistReport($school->id,$from,$to);		
		$exp_amount='';
		$inc_amount='';
?>

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    <?=$school->school_name?>
                            </h3>
                            <ul class="breadcrumb hidden-print">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'update', 'update');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 

                                    <li class="last">
                                       Full Report
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-left">

				<div class="control-group span12" style='margin-left:0px;padding-right:0px;'>
				<table>
					<tr><td>From Date </td><td>&nbsp;:&nbsp;</td><td><?=$from?></td></tr>
					<tr><td>To Date </td><td>&nbsp;:&nbsp;</td><td><?=$to?> </td></tr>
				</table>
				</div>														
				</div>	
				
				
				<div class="tiles pull-right hidden-print">
									<div class="tile bg-yellow " id="print_document">							
												<a class="hidden-print" onclick="javascript:window.print();">
													<div class="corner"></div>
													<div class="tile-body"><i class="icon-print"></i></div>
													<div class="tile-object"><div class="name">Print</div></div>
												</a>
									</div>
								<div class="tile bg-purple <?php echo ($section=='report_rep')?'selected':''?>">
										<a href="<?php echo make_admin_url('account', 'report', 'report');?>">
											<div class="corner"></div>
											<div class="tile-body"><i class="icon-arrow-left"></i></div>
											<div class="tile-object"><div class="name">Back To Report</div></div>
										</a> 
								</div>	
															
				</div>         
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<form class="form-horizontal" action="<?php echo make_admin_url('account', 'lsit', 'lsit')?>" method="POST" enctype="multipart/form-data" id="validation">
						<div class="portlet box yellow">
							<div class="portlet-title">
								<div class="caption"><i class="icon-rupee"></i>Full Report &nbsp;&nbsp;&nbsp;(<?=$from?> to <?=$to?>)</div>								
							</div>
							<div class="portlet-body">
								<div class="clearfix"></div>
											<div class="row-fluid">
												<!-- List all Expenses-->
												<div class='span6'>
													<h4 class="form-section hedding_inner">Expenses</h4>
														
															<!-- EXP Cat -->
															<? if(!empty($CatExpRecord)):  ?>															
																<div class="row-fluid">																	
																		<table class="table table-striped table-hover">
																			<? foreach($CatExpRecord as $exp_key=>$exp_cat):	?>
																					<tr><td class='span8'><?=$exp_cat->category_name?></td><td class='span3' style='text-align:right;'><?=CURRENCY_SYMBOL.' '.number_format($exp_cat->amount,2)?></td></tr>
																			<?	$exp_amount=$exp_amount+$exp_cat->amount;
																				endforeach; ?>
																		<tr><td class='span8'></td><td class='span3' style='text-align:right;'><strong><?=CURRENCY_SYMBOL.' '.number_format($exp_amount,2)?></strong></td></tr>	
																		</table>
																</div>			
															<? endif;?>
														
															<!-- Salary Exp -->
															<? if(!empty($SalRecord)): 
															    foreach($SalRecord as $Sal_key=>$Sal_cat):	?>
																	<div class="row-fluid">
																		<div class='span8 report_head' style='padding-left:2.0641%'>Salaries</div><div class='span4 report_head' style='text-align: right; padding-right: 8px;'><?=CURRENCY_SYMBOL.' '.number_format($Sal_cat->amount,2)?></div>
																	</div>
															<?	$exp_amount=$exp_amount+$Sal_cat->amount;
																endforeach;
															endif;?>
														
												</div>
												
												
												<!-- List all Incomes-->
												<div class='span6'>
													<h4 class="form-section hedding_inner">Incomes</h4>
														<? if(!empty($CatIncRecord)): ?>
															
																<div class="row-fluid">
																		<table class="table table-striped table-hover">														
																		<? foreach($CatIncRecord as $Inc_key=>$inc_cat):	?>
																		<tr><td class='span8'><?=$inc_cat->category_name?></td><td class='span3' style='text-align:right;'><?=CURRENCY_SYMBOL.' '.number_format($inc_cat->amount,2)?></td></tr>
																	<?	$inc_amount=$inc_amount+$inc_cat->amount;
																		endforeach; ?>
																		<tr><td class='span8'></td><td class='span3' style='text-align:right;'><strong><?=CURRENCY_SYMBOL.' '.number_format($inc_amount,2)?></strong></td></tr>	
																		</table>
																</div>			
														<?	endif;?>
															
															<!-- Fee Exp -->
															<? if(!empty($FeeRecord)): 
															    foreach($FeeRecord as $fee_key=>$fee_cat):	?>
																	<div class="row-fluid">
																		<div class='span8 report_head' style='padding-left:2.0641%'>Student Fee</div><div class='span4 report_head' style='text-align: right; padding-right: 8px;'><?=CURRENCY_SYMBOL.' '.number_format($fee_cat->amount,2)?></div>
																	</div>
															<?	$inc_amount=$inc_amount+$fee_cat->amount;
																endforeach; 
															endif;?>
															
												</div>
											</div>
											<div class="row-fluid">
												<br/>
											</div>											
											<div class="row-fluid report_total">
												<div class='span6'>
													<div class="row-fluid">
														<div class='span8' style='padding-left:2.5641%'><h4 class="media-heading total_sum">Total</h4></div><div class='span4' style='text-align: right; padding-right: 8px;'>
															<h4 class="media-heading total_sum" style='text-rendering: auto'><?=CURRENCY_SYMBOL.' '.number_format($exp_amount,2)?></h4></div>
													</div>
												</div>
												<div class='span6'>
													<div class="row-fluid">
														<div class='span8' style='padding-left:2.5641%'><h4 class="media-heading total_sum">Total</h4></div><div class='span4' style='text-align: right; padding-right: 8px;'><h4 class="media-heading total_sum" style='text-rendering: auto'><?=CURRENCY_SYMBOL.' '.number_format($inc_amount,2)?></h4></div>
													</div>												
												</div>
											</div><br/>
											<div class="row-fluid">
											<div class='span6'></div>
											<div class='span6 report_total'> 
															<div class="span8 report_head" style="padding-left:2.0641%">Net Income</div>
															<div class="span4 report_head" style="text-align: right; padding-right: 8px; text-rendering: auto"><?=CURRENCY_SYMBOL.' '.number_format($inc_amount-$exp_amount,2)?></div>
											</div>
							</div>
						</div>
						</form>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
    </div>