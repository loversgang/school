<?php
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolExpenseClass.php');
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'delete_doc') {
        $obj = new expense_docs;
        $obj->id = $doc_id;
        $obj->Delete();
        echo "success";
    }
    if ($act == 'get_expense_data') {
        if ($cat == '0') {
            $QueryObj = new schoolExpense();
            $record = $QueryObj->listAll($school->id, $year);
        } else {
            $QueryObj = new schoolExpense();
            $record = $QueryObj->listAll($school->id, $year, $cat);
        }
        ?>
        <form action="<?php echo make_admin_url('expense', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >		
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr>
                        <th style="width:45px;">Sr. No</th>
                        <th style='width:30%;'>Title</th>
                        <th class="hidden-480 sorting_disabled">Category</th>
                        <th style='text-align:center' class="hidden-480 sorting_disabled">On Date</th>
                        <th class="hidden-480 sorting_disabled">Amount (<?= CURRENCY_SYMBOL ?>)</th>
                        <th >Action</th>
                    </tr>
                </thead>
                <? if (!empty($record)): ?>
                    <tbody>
                        <?php
                        $sr = 1;
                        foreach ($record as $key => $object):
                            ?>
                            <tr class="odd gradeX">
                                <td><?= $sr ?>.</td>
                                <td><?= $object->title ?></td>
                                <td class="hidden-480 sorting_disabled"><?= $object->category ?></td>
                                <td style='text-align:center' class="hidden-480 sorting_disabled"><?= $object->on_date ?></td>
                                <td><?= CURRENCY_SYMBOL . " " . number_format($object->amount, 2) ?></td>													
                                <td style='text-align:right;'>
                                    <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('expense', 'update', 'update', 'id=' . $object->id.'&type=account') ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                    <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('expense', 'delete', 'delete', 'id=' . $object->id . '&delete=1&type=account') ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                </td>
                            </tr>
                            <?php
                            $sr++;
                        endforeach;
                        ?>
                    </tbody>
                <?php endif; ?>  
            </table>
        </form>
        <?php
    }
}