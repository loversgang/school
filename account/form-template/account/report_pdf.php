	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
<style>
body {
    color: #000000;
    direction: ltr;
    font-family: 'Open Sans';
    font-size: 13px;
	background-color: #FFF !important;
}
.page-title {
    color: #666666;
    display: block;
    font-family: 'Open Sans';
    font-size: 30px;
    font-weight: 300;
    letter-spacing: -1px;
    margin: 20px 0 15px;
    padding: 0;
}
.portlet.box.yellow {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: -moz-use-text-color #FCCB7E #FCCB7E;
    border-image: none;
    border-right: 1px solid #FCCB7E;
    border-style: none solid solid;
    border-width: 0 1px 1px;
}
.portlet.box {
    padding: 0 !important;
}
.portlet-body.yellow, .portlet.yellow {
    background-color: #FFB848 !important;
}
.portlet {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}

.portlet-title .caption {
    display: inline-block;
    float: left;
    font-size: 18px;
    font-weight: 400;
    margin: 0 0 7px;
    padding: 0 0 8px;
}
</style>

<?php 
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolExpenseClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolIncomeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/feeTypeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE.'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');

isset($_REQUEST['from'])?$from=$_REQUEST['from']:$from='';
isset($_REQUEST['to'])?$to=$_REQUEST['to']:$to='';
isset($_REQUEST['cat'])?$cat=$_REQUEST['cat']:$cat='';
isset($_REQUEST['session_id'])?$session_id=$_REQUEST['session_id']:$session_id='';
isset($_REQUEST['ct_sec'])?$ct_sec=$_REQUEST['ct_sec']:$ct_sec='';


		#Get Exp Records
		$QueryObjExp = new schoolExpense();
		$CatExpRecord=$QueryObjExp->CatExpenseReport($school->id,$from,$to);	

	
		#Get Salary Exp Records
		$QuerySalObj = new staffSalary();
		$SalRecord=$QuerySalObj->SumSalaryReport($school->id,$from,$to);
		
		#Get Income Records
		$QueryObjInc = new schoolIncome();
		$CatIncRecord=$QueryObjInc->CatIncomeReport($school->id,$from,$to);

		#Get Fee Income Records
		$QueryFeeObj = new studentFeeRecord();
		$FeeRecord=$QueryFeeObj->OverAlllistReport($school->id,$from,$to);		
		$exp_amount='';
		$inc_amount='';
?>

  <!-- BEGIN PAGE CONTAINER-->
    <div style='padding-left:20px;padding-right:20px;'>
            <!-- BEGIN PAGE HEADER-->

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    <?=$school->school_name?>
                            </h3>
 
                            <!-- END PAGE TITLE & BREADCRUMB-->


				<div style='margin-left:0px;padding-right:0px;'>
				<table>
					<tr><td>From Date </td><td></td><td><?=$from?></td></tr>
					<tr><td>To Date </td><td></td><td><?=$to?> </td></tr>
				</table>
				</div>

              <!-- BEGIN PAGE CONTENT-->

					
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box purple">
							<div class="portlet-title">
								<div class="caption"></i>Full Report (<?=$from?> to <?=$to?>)</div>								
							</div>
							<div class="portlet-body">
											<div style='width:100%'>
												<table class="table table-striped table-bordered table-hover" style='width:100%'>
												<tr><th colspan='2'>Expense</th><th colspan='2'>Income</th>
												</tr>
												<tr><td colspan='2'>
														<? if(!empty($CatExpRecord)):  ?>
															<table style='width:100%;border:none;'>
																<? foreach($CatExpRecord as $exp_key=>$exp_cat):?>
																	<tr>
																		<td style='border:none;'><?=$exp_cat->category_name?></td><td style='border:none;text-align:right;'><?=number_format($exp_cat->amount,2)?></td>
																	</tr>
																<?	$exp_amount=$exp_amount+$exp_cat->amount;
																	endforeach; ?>
																	<tr><td style='border:none;'></td><td style='border:none;text-align:right;'><strong><?=number_format($exp_amount,2)?></strong></td></tr>
															</table>		
														<? endif;?>	
															<!-- Salary Exp -->
														<? if(!empty($SalRecord)):?> 
																<table style='width:100%;border:none;'>
																<?  foreach($SalRecord as $Sal_key=>$Sal_cat):	?>
																	<tr>		
																		<td style='border:none;'>Salaries</td><td style='border:none;text-align:right;'><?=number_format($Sal_cat->amount,2)?></td>
																	</tr>
																<?	$exp_amount=$exp_amount+$Sal_cat->amount;
																	endforeach;?>
																</table>
														<? endif;?>														
												<td colspan='2'>												
														<? if(!empty($CatIncRecord)):  ?>
															<table style='width:100%;border:none;'>
																<? foreach($CatIncRecord as $inc_key=>$inc_cat):?>
																	<tr>
																		<td style='border:none;'><?=$inc_cat->category_name?></td><td style='border:none;text-align:right;'><?=number_format($inc_cat->amount,2)?></td>
																	</tr>
																<?	$inc_amount=$inc_amount+$inc_cat->amount;
																	endforeach; ?>
																	<tr><td style='border:none;'></td><td style='border:none;text-align:right;'><strong><?=number_format($inc_amount,2)?></strong></td></tr>
															</table>		
														<? endif;?>	
															<!-- Salary Exp -->
														<? if(!empty($FeeRecord)):?> 
																<table style='width:100%;border:none;'>
																<?  foreach($FeeRecord as $fee_key=>$fee_cat):	?>
																	<tr>		
																		<td style='border:none;'>Student Fee</td><td style='border:none;text-align:right;'><?=number_format($fee_cat->amount,2)?></td>
																	</tr>
																<?	$inc_amount=$inc_amount+$fee_cat->amount;
																	endforeach;?>
																</table>
														<? endif;?>													
												</td>
												</tr>
												<tr><td style='text-align:left;font-weight:bold;padding-top: 16px;padding-bottom:16px;border-right:0px;'>Total</td><td style='text-align:right;font-weight:bold;padding: 16px;border-left:none;'><?=number_format($exp_amount,2)?></strong></td><td style='text-align:left;font-weight:bold;padding-top:16px;padding-bottom:16px;'>Total</td><td style='text-align:right;font-weight:bold;padding: 16px;border-left:none;'><?=number_format($inc_amount,2)?></strong></td></tr>
												<tr><td colspan='2'></td><td style='text-align:left;font-weight:bold;padding-top: 16px;padding-bottom:16px;'>Net Income</td><td style='text-align:right;font-weight:bold;padding: 16px;border-left:none;'><?=number_format($inc_amount-$exp_amount,2)?></strong></td></tr>
												</table>					
											</div>
							</div>
						</div>
    </div>