
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Notice Board
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    List Notice Board
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-list"></i>Manage Notice Board</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('course', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >	
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th style="width:45px;">Sr. No.</th>
                                    <th style='width:50%;'>Page Name</th>
                                    <th style='text-align:center' class="hidden-480 sorting_disabled">On Date</th>

                                    <th>Action</th>

                                </tr>
                            </thead>
                            <? if ($QueryObj->GetNumRows() != 0): ?>
                                <tbody>
                                    <?php $sr = 1;
                                    while ($object = $QueryObj->GetObjectFromRecord()): ?>
                                        <tr class="odd gradeX">
                                            <td><?= $sr ?>.</td>
                                            <td><?php echo $object->name ?></td>
                                            <td style='text-align:center' class="hidden-480"><?php echo $object->on_date ?></td>

                                            <td style='text-align:right;'>
                                                <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('notice', 'update', 'update', 'id=' . $object->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
                                                <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('notice', 'delete', 'delete', 'id=' . $object->id . '&delete=1') ?>" onclick="return confirm('Are you sure? You are deleting this page.');" title="click here to delete this page"><i class="icon-remove icon-white"></i></a>
                                            </td>

                                        </tr>
                                        <?php $sr++;
                                    endwhile;
                                    ?>
                                </tbody>
<?php endif; ?>  
                        </table>
                    </form>    
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



