
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Notice Board
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('notice', 'list', 'list'); ?>">List Notice Board</a> 
                    <i class="icon-angle-right"></i>
                </li>  
                <li class="last">
                    Add New Notice Board
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->


    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('notice', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-list"></i>Add Notice Board</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">      
                        <div class="control-group">
                            <label class="control-label" for="name">Title<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="name"  value="" id="name" class="span6 m-wrap validate[required]" />
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="on_date">On Date<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="on_date"  id="ui_date_picker" class="span6 m-wrap validate[required]" />
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label" for="short_description">Short Description<span class="required">*</span></label>
                            <div class="controls">
                                <textarea rows="3" class="span6 m-wrap validate[required]" style="height:82px;" id="short_description" name="short_description" maxlength=150'></textarea>
                            </div>
                        </div>											
                        <div class="control-group">
                            <label class="control-label" for="description">Full Description</label>
                            <div class="controls">
                                <textarea id="description" class="span12 ckeditor m-wrap" name="description" rows="6" style="visibility: hidden; display: none;">
                                </textarea>
                            </div>
                        </div> 
                        <!--	 
<div class="control-group">
<label class="control-label" for="short_description">Image</label>
<div class="controls">
<input type="file" name="image"  class="span6 m-wrap" />
                                                </div>
</div>
                        -->

                        <h4 class="form-section hedding_inner">SEO Information</h4>
                        <div class="control-group">
                            <label class="control-label" for="meta_name">Meta Title</label>
                            <div class="controls">
                                <input type="text" name="meta_name" id="meta_name" class="span12 m-wrap" value=""> 
                            </div>  
                        </div>  
                        <div class="control-group">
                            <label class="control-label" for="meta_keyword">Meta Keywords</label>
                            <div class="controls">
                                <input type="text" name="meta_keyword" id="meta_keyword" class="span12 m-wrap" value="">
                            </div>
                        </div>          
                        <div class="control-group">
                            <label class="control-label" for="meta_description">Meta Description</label>
                            <div class="controls">
                                <textarea rows="3" class="span12 m-wrap" style=" height: 82px;" id="meta_description" name="meta_description"></textarea>
                            </div>
                        </div>   
                        <div class="control-group">
                            <label class="control-label" for="meta_keyword">Make Active</label>
                            <div class="controls">
                                <input type="checkbox" name="is_active" id="meta_keyword" value="1">
                            </div>
                        </div>   										   
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('notice', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="clearfix"></div>

    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    

