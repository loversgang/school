<?php error_reporting(0); ?>
<style>
    #term_ist {
        height: 300px;
        width: 100%;
    }
    #term_2nd {
        height: 300px;
        width: 100%;
    }
    #exam_final {
        height: 300px;
        width: 100%;
    }
</style>
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                <?= ucfirst($s_info->first_name) . " " . $s_info->last_name ?> > Profile
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('student', 'list', 'list'); ?>">List Students</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Students Information
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div> 		            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <div id="form_wizard_1" class="portlet box purple">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-user"></i> Student Information </span>
                    </div>
                    <div class="tools hidden-phone">

                    </div>
                </div>

                <div class="portlet-body form">
                    <div class="form-wizard">
                        <div class="tab-content">

                            <!-- First Step  Student Detail-->
                            <div id="tab1" class="tab-pane <?= ($type == 'student_detail') ? 'active' : ''; ?>">
                                <div class="row-fluid">
                                    <!-- Photo -->
                                    <ul class="unstyled profile-nav span3" id='left_user_info'>
                                        <li>
                                            <?
                                            if (is_object($s_info) && $s_info->photo):
                                                $im_obj = new imageManipulation()
                                                ?>
                                                <div class="item" style='text-align:center;'>	
                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                        <div class="zoom">
                                                            <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                            <div class="zoom-icon"></div>
                                                        </div>
                                                    </a>
                                                </div>															
                                            <? else: ?>
                                                <img src="assets/img/profile/img.jpg" alt="">
                                            <? endif; ?> 
                                        </li>
                                        <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=student_detail&id=' . $id) ?>">Basic Information</a></li>
                                        <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=family&id=' . $id) ?>">Family Information</a></li>
                                        <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=document&id=' . $id) ?>">Document Information </a></li>
                                        <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=previous_school&id=' . $id) ?>">Previous Schools Information</a></li>
                                        <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=fee&id=' . $id) ?>">Fee Information</a></li>
                                        <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=attendance&id=' . $id) ?>">Attendance Information</a></li>
                                        <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=exam&id=' . $id) ?>">Exam Information</a></li>
                                        <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=print&id=' . $id) ?>">Print Documents</a></li>
                                    </ul>	
                                    <div class="span9">
                                        <!-- Basic Info -->
                                        <h4 class="form-section hedding_inner1">Basic Information</h4>				
                                        <ul class="unstyled span5" id='user_info'>
                                            <li><span>Registration Id :</span> <?= $s_info->reg_id ?></li>
                                            <li><span>Registration Date :</span> <?= $s_info->date_of_admission ?></li>																
                                            <li><span>First Name :</span> <?= $s_info->first_name ?></li>										
                                            <li><span>Sex :</span> <?= $s_info->sex ?></li>										
                                            <li><span>Cast :</span> <?= $s_info->cast ?></li>
                                            <li><span>Category :</span> <?= $s_info->category ?></li>
                                            <li><span>Mobile Number:</span> <?= $s_info->phone ?></li>
                                        </ul>
                                        <ul class="unstyled span5" id='user_info'>	
                                            <li><span></span></li>															
                                            <li><span></span></li>										
                                            <li><span>Last Name :</span> <?= $s_info->last_name ?></li>										
                                            <li><span>Date Of Birth :</span> <?= $s_info->date_of_birth ?></li>										
                                            <li><span>Religion :</span> <?= $s_info->religion ?></li>
                                            <li><span>Email :</span> <a href="mailto:<?= $s_info->email ?>"> <?= $s_info->email ?></a></li>	
                                        </ul>
                                        <div class="clearfix"></div>

                                        <!-- Address Info -->
                                        <h4 class="form-section hedding_inner1">Address Information</h4>				
                                        <ul class="unstyled span10" id='user_info'>
                                            <li><span>Permanent Address :</span> 
                                                <?
                                                if (isset($s_p_ad) && !empty($s_p_ad->address1)) {
                                                    echo $s_p_ad->address1 . ", ";
                                                }
                                                if (isset($s_p_ad) && !empty($s_p_ad->city)) {
                                                    echo $s_p_ad->city . ", ";
                                                }
                                                if (isset($s_p_ad) && !empty($s_p_ad->tehsil)) {
                                                    echo $s_p_ad->tehsil . ", ";
                                                }
                                                if (isset($s_p_ad) && !empty($s_p_ad->district)) {
                                                    echo $s_p_ad->district . ", ";
                                                }
                                                if (isset($s_p_ad) && !empty($s_p_ad->state)) {
                                                    echo $s_p_ad->state . ", ";
                                                }
                                                if (isset($s_p_ad) && !empty($s_p_ad->zip)) {
                                                    echo $s_p_ad->zip;
                                                }
                                                ?>
                                            </li>
                                            <li><span>Correspondence Address :</span> 
                                                <?
                                                if (isset($s_c_ad) && !empty($s_c_ad->address1)) {
                                                    echo $s_c_ad->address1 . ", ";
                                                }
                                                if (isset($s_c_ad) && !empty($s_c_ad->city)) {
                                                    echo $s_c_ad->city . ", ";
                                                }
                                                if (isset($s_c_ad) && !empty($s_c_ad->tehsil)) {
                                                    echo $s_c_ad->tehsil . ", ";
                                                }
                                                if (isset($s_c_ad) && !empty($s_c_ad->district)) {
                                                    echo $s_c_ad->district . ", ";
                                                }
                                                if (isset($s_c_ad) && !empty($s_c_ad->state)) {
                                                    echo $s_c_ad->state . ", ";
                                                }
                                                if (isset($s_c_ad) && !empty($s_c_ad->zip)) {
                                                    echo $s_c_ad->zip;
                                                }
                                                ?>
                                            </li>				
                                        </ul>

                                        <div class="clearfix"></div>


                                        <!-- Health Info -->
                                        <h4 class="form-section hedding_inner1">Health Information</h4>				
                                        <ul class="unstyled span5" id='user_info'>
                                            <li><span>Blood Group :</span> <?= $s_info->blood_group ?></li>										
                                            <li><span>Height :</span> <?= $s_info->height ?></li>										
                                            <li><span>Eye Right :</span> <?= $s_info->eye_right ?></li>
                                            <li><span>Family Doctor Name :</span> <?= $s_info->family_doctor ?></li>
                                        </ul>
                                        <ul class="unstyled span5" id='user_info'>										
                                            <li><span>Allergies :</span> <?= $s_info->allergies ?></li>										
                                            <li><span>Weight :</span> <?= $s_info->weight ?></li>										
                                            <li><span>Eye Left :</span> <?= $s_info->eye_left ?></li>
                                            <li><span>Family Doctor No. :</span> <?= $s_info->family_doctor_no ?></li>
                                        </ul>
                                        <div class="clearfix"></div>

                                        <!-- Health Info -->
                                        <h4 class="form-section hedding_inner1">In Case Of Emergency</h4>				
                                        <ul class="unstyled span5" id='user_info'>
                                            <li><span>Contact Person :</span> <?= $s_info->contact_person ?></li>										
                                            <li><span>Contact No :</span> <?= $s_info->contact_person_no ?></li>	
                                        </ul>
                                        <ul class="unstyled span5" id='user_info'>										
                                            <li><span>Relation :</span> <?= $s_info->contact_relation ?></li>
                                        </ul>
                                        <div class="clearfix"></div>															

                                        <!-- Reference Detail -->
                                        <h4 class="form-section hedding_inner1">Reference Detail</h4>				
                                        <ul class="unstyled span5" id='user_info'>
                                            <li><span> By Reference :</span> <?= $s_info->ref_name ?></li>																
                                        </ul>
                                        <ul class="unstyled span5" id='user_info'>	
                                            <li><span>Remark :</span><?= $s_info->ref_remark ?></li>										
                                        </ul>
                                        <div class="clearfix"></div>

                                    </div>		
                                </div>
                            </div>


                            <!-- Family Detail -->
                            <div id="tab3" class="tab-pane <?= ($type == 'family') ? 'active' : ''; ?>">
                                <!-- Photo -->
                                <ul class="unstyled profile-nav span3" id='left_user_info'>
                                    <li>
                                        <?
                                        if (is_object($s_info) && $s_info->photo):
                                            $im_obj = new imageManipulation()
                                            ?>
                                            <div class="item" style='text-align:center;'>	
                                                <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                    <div class="zoom">
                                                        <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                        <div class="zoom-icon"></div>
                                                    </div>
                                                </a>
                                            </div>															
                                        <? else: ?>
                                            <img src="assets/img/profile/img.jpg" alt="">
                                        <? endif; ?> 
                                    </li>
                                    <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=student_detail&id=' . $id) ?>">Basic Information</a></li>
                                    <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=family&id=' . $id) ?>">Family Information</a></li>
                                    <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=document&id=' . $id) ?>">Document Information </a></li>
                                    <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=previous_school&id=' . $id) ?>">Previous Schools Information</a></li>
                                    <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=fee&id=' . $id) ?>">Fee Information</a></li>
                                    <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=attendance&id=' . $id) ?>">Attendance Information</a></li>
                                    <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=exam&id=' . $id) ?>">Exam Information</a></li>
                                    <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=print&id=' . $id) ?>">Print Documents</a></li>
                                </ul>	
                                <div class="span9">
                                    <!-- Family Info -->
                                    <h4 class="form-section hedding_inner1">Family Detail</h4>				
                                    <ul class="unstyled span5" id='user_info'>
                                        <li><span>Father Name :</span> <?
                                            if (is_object($s_f)) {
                                                echo $s_f->father_name;
                                            }
                                            ?></li>										
                                        <li><span>Father Occupation :</span> <?
                                            if (is_object($s_f)) {
                                                echo $s_f->father_occupation;
                                            }
                                            ?></li>										
                                        <li><span>Father Education :</span> <?
                                            if (is_object($s_f)) {
                                                echo $s_f->father_education;
                                            }
                                            ?></li>
                                        <li><span>Email Address :</span> <?
                                            if (is_object($s_f)) {
                                                echo "<a href='mailto:" . $s_f->email . "'>" . $s_f->email . "</a>";
                                            }
                                            ?></li>
                                        <li><span>Annual Income (<?= CURRENCY_SYMBOL ?>) :</span> <?
                                            if (is_object($s_f)) {
                                                echo $s_f->household_annual_income;
                                            }
                                            ?></li>
                                    </ul>
                                    <ul class="unstyled span5" id='user_info'>										
                                        <li><span>Mother Name :</span> <?
                                            if (is_object($s_f)) {
                                                echo $s_f->mother_name;
                                            }
                                            ?></li>										
                                        <li><span>Mother Occupation :</span> <?
                                            if (is_object($s_f)) {
                                                echo $s_f->mother_occupation;
                                            }
                                            ?></li>										
                                        <li><span>Mother Education :</span> <?
                                            if (is_object($s_f)) {
                                                echo $s_f->mother_education;
                                            }
                                            ?></li>
                                        <li><span>Mobile Number. :</span> <?
                                            if (is_object($s_f)) {
                                                echo $s_f->mobile;
                                            }
                                            ?></li>
                                        <li><span>Family Photo :</span> 
                                            <?
                                            if (is_object($s_f) && $s_f->family_photo):
                                                $im_obj = new imageManipulation()
                                                ?>
                                                <div class="item span10" style='text-align:center;'>	
                                                    <a class="fancybox-button" data-rel="fancybox-button" title="Family Photo" href="<?= $im_obj->get_image('student_family', 'large', $s_f->family_photo); ?>">
                                                        <div class="zoom">
                                                            <img src="<?= $im_obj->get_image('student_family', 'medium', $s_f->family_photo); ?>" style='margin-top: 6px;'/>
                                                            <div class="zoom-icon"></div>
                                                        </div>
                                                    </a>

                                                </div>															
                                            <? else: ?>
                                                <img src="assets/img/profile/noimage.jpg" style='border:2px solid #CCCCCC;margin-top: 6px;'/>
                                            <? endif; ?>

                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>

                                    <!-- Sibling Info -->
                                    <h4 class="form-section hedding_inner1">Sibling Study in School</h4>
                                    <? if ($QueryS->GetNumRows()): $sib_r = 1; ?>
                                        <? while ($sib = $QueryS->GetObjectFromRecord()): ?>
                                            <div class='span2'><strong>Sibling <?= $sib_r ?></strong></div>
                                            <div class='span9'>
                                                <ul class="unstyled span10" id='user_info'>					
                                                    <li><span>Registration ID :</span><?= $sib->reg_id ?></li>
                                                    <li><span>Name :</span><?= $sib->name ?></li>
                                                    <li><span>Class :</span><?= $sib->class ?></li>
                                                    <li><span>Relation :</span><?= $sib->relation ?></li>
                                                </ul>
                                            </div>
                                            <div class="clearfix"></div>
                                            <hr/>
                                            <?
                                            $sib_r++;
                                        endwhile;
                                    else:
                                        ?>
                                        <ul class="unstyled span11" id='user_info'>
                                            <li></li>															
                                            <li><span>Sorry, No Record Found..!</span></li>
                                            <li></li>	
                                        </ul>		
                                    <? endif; ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <!-- Step 4 Previous School -->
                            <div id="tab4" class="tab-pane <?= ($type == 'previous_school') ? 'active' : ''; ?>">

                                <div class="row-fluid">
                                    <!-- Photo -->
                                    <ul class="unstyled profile-nav span3" id='left_user_info'>
                                        <li>
                                            <?
                                            if (is_object($s_info) && $s_info->photo):
                                                $im_obj = new imageManipulation()
                                                ?>
                                                <div class="item" style='text-align:center;'>	
                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                        <div class="zoom">
                                                            <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                            <div class="zoom-icon"></div>
                                                        </div>
                                                    </a>
                                                </div>															
                                            <? else: ?>
                                                <img src="assets/img/profile/img.jpg" alt="">
                                            <? endif; ?> 
                                        </li>
                                        <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=student_detail&id=' . $id) ?>">Basic Information</a></li>
                                        <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=family&id=' . $id) ?>">Family Information</a></li>
                                        <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=document&id=' . $id) ?>">Document Information </a></li>
                                        <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=previous_school&id=' . $id) ?>">Previous Schools Information</a></li>
                                        <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=fee&id=' . $id) ?>">Fee Information</a></li>
                                        <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=attendance&id=' . $id) ?>">Attendance Information</a></li>
                                        <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=exam&id=' . $id) ?>">Exam Information</a></li>
                                        <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=print&id=' . $id) ?>">Print Documents</a></li>
                                    </ul>	
                                    <div class="span9">
                                        <!-- Address Info -->
                                        <h4 class="form-section hedding_inner1">Previous Schools Information</h4>				
                                        <? if ($P_school->GetNumRows()): $PS_r = 1; ?>
                                            <? while ($sch = $P_school->GetObjectFromRecord()): ?>
                                                <div class='span2'><strong>School <?= $PS_r ?></strong></div>
                                                <div class='span9'>
                                                    <ul class="unstyled span10" id='user_info'>					
                                                        <li><span>School Name :</span><?= $sch->school_name ?></li>
                                                        <li><span>Class :</span><?= $sch->class ?></li>
                                                        <li><span>Address1 :</span><?= $sch->address1 ?></li>
                                                        <li><span>Address2 :</span><?= $sch->address2 ?></li>
                                                        <li><span>City :</span><?= $sch->city ?></li>
                                                        <li><span>State :</span><?= $sch->state ?></li>
                                                        <li><span>Country :</span><?= $sch->country ?></li>
                                                        <li><span>Percentage :</span><?= $sch->percentage ?></li>
                                                    </ul>
                                                </div>
                                                <div class="clearfix"></div>
                                                <hr/>
                                                <?
                                                $PS_r++;
                                            endwhile;
                                        else:
                                            ?>
                                            <ul class="unstyled span11" id='user_info'>
                                                <li></li>															
                                                <li><span>Sorry, No Record Found..!</span></li>
                                                <li></li>	
                                            </ul>																	
                                        <? endif; ?>	
                                    </div>		
                                </div>	
                            </div>


                            <!-- Step 5 Student Documents -->
                            <div id="tab5" class="tab-pane <?= ($type == 'document') ? 'active' : ''; ?>">
                                <div class="row-fluid">
                                    <!-- Photo -->
                                    <ul class="unstyled profile-nav span3" id='left_user_info'>
                                        <li>
                                            <?
                                            if (is_object($s_info) && $s_info->photo):
                                                $im_obj = new imageManipulation()
                                                ?>
                                                <div class="item" style='text-align:center;'>	
                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                        <div class="zoom">
                                                            <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                            <div class="zoom-icon"></div>
                                                        </div>
                                                    </a>
                                                </div>																
                                            <? else: ?>
                                                <img src="assets/img/profile/img.jpg" alt="">
                                            <? endif; ?> 
                                        </li>
                                        <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=student_detail&id=' . $id) ?>">Basic Information</a></li>
                                        <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=family&id=' . $id) ?>">Family Information</a></li>
                                        <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=document&id=' . $id) ?>">Document Information </a></li>
                                        <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=previous_school&id=' . $id) ?>">Previous Schools Information</a></li>
                                        <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=fee&id=' . $id) ?>">Fee Information</a></li>
                                        <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=attendance&id=' . $id) ?>">Attendance Information</a></li>
                                        <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=exam&id=' . $id) ?>">Exam Information</a></li>
                                        <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=print&id=' . $id) ?>">Print Documents</a></li>
                                    </ul>	
                                    <div class="span9">
                                        <!-- Address Info -->
                                        <h4 class="form-section hedding_inner1">Document Detail</h4>				
                                        <ul class="unstyled span10" id='user_info'>
                                            <? if ($S_doc->GetNumRows()): $Sd_r = '0'; ?>																
                                                <? while ($s_d = $S_doc->GetObjectFromRecord()): ?>														
                                                    <li><span><?= $s_d->title ?>:</span> 
                                                        <a target='_blank' href="<?= DIR_WS_SITE_UPLOAD . 'file/student_document/' . $s_d->file ?>"><?= $s_d->file ?></a>
                                                    </li>
                                                    <?
                                                    $Sd_r++;
                                                endwhile;
                                            else:
                                                ?>	
                                                <li></li>															
                                                <li><span>Sorry, No Record Found..!</span></li>
                                                <li></li>														
                                            <? endif; ?>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>		
                                </div>	
                            </div>	

                            <!-- Get Student Fee Info -->
                            <div id="tab6" class="tab-pane <?= ($type == 'fee') ? 'active' : ''; ?>">
                                <div class="row-fluid">
                                    <!-- Photo -->
                                    <ul class="unstyled profile-nav span3" id='left_user_info'>
                                        <li>
                                            <?
                                            if (is_object($s_info) && $s_info->photo):
                                                $im_obj = new imageManipulation()
                                                ?>
                                                <div class="item" style='text-align:center;'>	
                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                        <div class="zoom">
                                                            <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                            <div class="zoom-icon"></div>
                                                        </div>
                                                    </a>
                                                </div>														
                                            <? else: ?>
                                                <img src="assets/img/profile/img.jpg" alt="">
                                            <? endif; ?> 
                                        </li>
                                        <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=student_detail&id=' . $id) ?>">Basic Information</a></li>
                                        <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=family&id=' . $id) ?>">Family Information</a></li>
                                        <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=document&id=' . $id) ?>">Document Information </a></li>
                                        <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=previous_school&id=' . $id) ?>">Previous Schools Information</a></li>
                                        <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=fee&id=' . $id) ?>">Fee Information</a></li>
                                        <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=attendance&id=' . $id) ?>">Attendance Information</a></li>
                                        <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=exam&id=' . $id) ?>">Exam Information</a></li>
                                        <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=print&id=' . $id) ?>">Print Documents</a></li>
                                    </ul>	
                                    <div class="span9">
                                        <form action="<?php echo make_admin_url('view', 'list', 'list') ?>" method="GET" id='session_filter_fee'>
                                            <input type='hidden' name='Page' value='view'/>
                                            <input type='hidden' name='action' value='list'/>
                                            <input type='hidden' name='section' value='list'/>
                                            <input type='hidden' name='type' value='fee'/>
                                            <input type='hidden' name='id' value='<?= $id ?>'/>
                                            <div class='span12'>
                                                <div class='span7'></div>
                                                <div class='span5'>															
                                                    <label class="control-label">Select Session</label>
                                                    <div class="controls">
                                                        <select class="select2_category session_filter_fee span11" data-placeholder="Select Session Students" name='session_id' >
                                                            <option value="">Select Session</option>
                                                            <?php
                                                            $session_sr = 1;
                                                            foreach ($Session_list as $s_k => $session):
                                                                ?>
                                                                <option value='<?= $session->session_id ?>' <?
                                                                if ($session->session_id == $session_id) {
                                                                    echo 'selected';
                                                                }
                                                                ?> ><?= ucfirst($session->session_name) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                    ?>
                                                        </select>
                                                    </div>	
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>																
                                        </form>		
                                        <h4 class="form-section hedding_inner1">Fee Detail</h4>				
                                        <div class="portlet-body">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="hidden-480" >Sr. No.</th>
                                                        <th class="hidden-480" style='text-align:center;'>Payment Date</th>
                                                        <th class="hidden-480" style='text-align:center;'>Receipt No.</th>
                                                        <th class="hidden-480 sorting_disabled" style='vertical-align:top;text-align:center'>Total Paid(<?= CURRENCY_SYMBOL ?>)</th>
                                                        <th class="hidden-480" style='text-align:center;'>Balance</th>
                                                    </tr>
                                                </thead>
                                                <? if (!empty($Fee_list)): ?>
                                                    <tbody>
                                                        <?php
                                                        $sr = 1;
                                                        foreach ($Fee_list as $sk => $rec): $checked = '';
                                                            ?>
                                                            <tr class="odd gradeX">
                                                                <td><?= $sr ?>.</td>                                                                                                                                                                        
                                                                <td style='text-align:center;'><?= date('d M, Y', strtotime($rec->payment_date)) ?></td>
                                                                <td style='text-align:center;'><?= $rec->id ?></td>
                                                                <td style='text-align:center;'><?= CURRENCY_SYMBOL . ' ' . number_format($rec->total_amount, 2) ?></td>
                                                                <td style='text-align:center;'>
                                                                    <?
                                                                    $QueryFList = new studentSession();
                                                                    $recc = $QueryFList->getBalanceDetail($rec->session_id, $rec->student_id, $rec->payment_date);

                                                                    echo CURRENCY_SYMBOL . number_format($recc, 2);
                                                                    ?>  


                                                                </td>

                                                            </tr>
                                                            <?
                                                            $sr++;
                                                        endforeach;
                                                        ?>									
                                                    </tbody>
                                                <? endif; ?>
                                            </table>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>		
                                </div>	
                            </div>
                            <!-- Get Student Attendance Info -->
                            <div id="tab7" class="tab-pane <?= ($type == 'attendance') ? 'active' : ''; ?>">
                                <div class="row-fluid">
                                    <!-- Photo -->
                                    <ul class="unstyled profile-nav span3" id='left_user_info'>
                                        <li>
                                            <?
                                            if (is_object($s_info) && $s_info->photo):
                                                $im_obj = new imageManipulation()
                                                ?>
                                                <div class="item" style='text-align:center;'>	
                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                        <div class="zoom">
                                                            <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                            <div class="zoom-icon"></div>
                                                        </div>
                                                    </a>
                                                </div>
                                            <? else: ?>
                                                <img src="assets/img/profile/img.jpg" alt="">
                                            <? endif; ?> 
                                        </li>
                                        <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=student_detail&id=' . $id) ?>">Basic Information</a></li>
                                        <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=family&id=' . $id) ?>">Family Information</a></li>
                                        <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=document&id=' . $id) ?>">Document Information </a></li>
                                        <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=previous_school&id=' . $id) ?>">Previous Schools Information</a></li>
                                        <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=fee&id=' . $id) ?>">Fee Information</a></li>
                                        <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=attendance&id=' . $id) ?>">Attendance Information</a></li>
                                        <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=exam&id=' . $id) ?>">Exam Information</a></li>
                                        <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=print&id=' . $id) ?>">Print Documents</a></li>
                                    </ul>	
                                    <div class="span9">
                                        <form action="<?php echo make_admin_url('view', 'list', 'list') ?>" method="GET" id='session_filter_attendance'>
                                            <input type='hidden' name='Page' value='view'/>
                                            <input type='hidden' name='action' value='list'/>
                                            <input type='hidden' name='section' value='list'/>
                                            <input type='hidden' name='type' value='attendance'/>
                                            <input type='hidden' name='id' value='<?= $id ?>'/>
                                            <div class='span12'>
                                                <div class='span7'></div>
                                                <div class='span5'>
                                                    <label class="control-label">Select Session</label>
                                                    <div class="controls">
                                                        <select class="select2_category session_filter_attendance span11" data-placeholder="Select Session Students" name='session_id' >
                                                            <option value="">Select Session</option>
                                                            <?php
                                                            $session_sr = 1;
                                                            foreach ($Session_list as $s_k => $session):
                                                                ?>
                                                                <option value='<?= $session->session_id ?>' <?
                                                                if ($session->session_id == $session_id) {
                                                                    echo 'selected';
                                                                }
                                                                ?> ><?= ucfirst($session->session_name) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                    ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>	
                                        </form>		
                                        <h4 class="form-section hedding_inner1">Attendance Detail   </h4>				
                                        <div class="portlet-body">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="hidden-480" style='text-align:center;'>Month</th>
                                                        <? foreach ($Attendance_array as $key => $value): ?>
                                                            <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='<?= $key ?>'>&nbsp;<?= $value ?></font></th>
                                                        <? endforeach; ?>
                                                        <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='NM'>&nbsp;Not Marked</font></th>
                                                        <th style='text-align:center;' class="hidden-480 sorting_disabled"><font class='W'>&nbsp;Working Days</font></th>
                                                    </tr>
                                                </thead>
                                                <?
                                                if (!empty($session_id) && ($session_id !== 'None')):
                                                    $All_months = getMonthsArray($session_info->start_date, $session_info->end_date);
                                                    if (!empty($All_months)):
                                                        ?>
                                                        <tbody>
                                                            <?php
                                                            $sr = 1;
                                                            foreach ($All_months as $m_k => $m_v): $checked = '';
                                                                if (($m_k) && strlen($m_k) == '1'):
                                                                    $m_number = '0' . $m_k;
                                                                else:
                                                                    $m_number = $m_k;
                                                                endif;

                                                                /* Get working days */
                                                                $first_date_of_month = $m_v . "-01";

                                                                $last_date_of_month = date("Y-m-d", strtotime("+1 month -1 second", strtotime(date($m_v . '-01'))));

                                                                $total_sunday = getTotalSunday($first_date_of_month, $last_date_of_month);
                                                                $total_month_days = (date('d', strtotime($last_date_of_month)));
                                                                $working_days = $total_month_days - $total_sunday;

                                                                /* Get Attendance */
                                                                $Q_obj = new attendance();
                                                                $r_atten = $Q_obj->checkSessionSectionMonthYearAttendace($session_id, $section_name, $id, date('m', strtotime($m_v . '-01')), date('Y', strtotime($m_v . '-01')));

                                                                $t_attend = array_sum($r_atten);
                                                                $M_obj = new schoolHolidays();
                                                                $M_holiday = $M_obj->checkMonthlyHoliday($school->id, date('m', strtotime($m_v . '-01')), date('Y', strtotime($m_v . '-01')));
                                                                ?>
                                                                <tr class="odd gradeX">
                                                                    <td style='text-align:center;'><?= date('M Y', strtotime($m_v . '-01')) ?></td>
                                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?
                                                                        if (!empty($r_atten) && array_key_exists('P', $r_atten)): echo "<font class='P'>&nbsp;" . $r_atten['P'] . "</font>";
                                                                        else: echo '0';
                                                                        endif;
                                                                        ?></td>
                                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?
                                                                        if (!empty($r_atten) && array_key_exists('A', $r_atten)): echo "<font class='A'>&nbsp;" . $r_atten['A'] . "</font>";
                                                                        else: echo '0';
                                                                        endif;
                                                                        ?></td>
                                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?
                                                                        if (!empty($r_atten) && array_key_exists('L', $r_atten)): echo "<font class='L'>&nbsp;" . $r_atten['L'] . "</font>";
                                                                        else: echo '0';
                                                                        endif;
                                                                        ?></td>
                                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= ($working_days - $t_attend - $M_holiday) ?></td>
                                                                    <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $working_days - $M_holiday ?></td>	
                                                                </tr>
                                                                <?
                                                                $sr++;
                                                            endforeach;
                                                            ?>									
                                                        </tbody>
                                                    <? endif; ?>	
<? else: ?>	
                                                    <tbody>
                                                        <tr><td colspan='8'> Sorry,No Record Found..! Please select the session</td></tr>
                                                    </tbody>
<? endif; ?>
                                            </table>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>		
                                </div>	
                            </div>	

                            <!-- Get Student Exam Info -->
                            <div id="tab8" class="tab-pane <?= ($type == 'exam') ? 'active' : ''; ?>">
                                <div class="row-fluid">
                                    <!-- Photo -->
                                    <ul class="unstyled profile-nav span3" id='left_user_info'>
                                        <li>
                                            <?
                                            if (is_object($s_info) && $s_info->photo):
                                                $im_obj = new imageManipulation()
                                                ?>
                                                <div class="item" style='text-align:center;'>	
                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                        <div class="zoom">
                                                            <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                            <div class="zoom-icon"></div>
                                                        </div>
                                                    </a>
                                                </div>														
                                            <? else: ?>
                                                <img src="assets/img/profile/img.jpg" alt="">
<? endif; ?> 
                                        </li>
                                        <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=student_detail&id=' . $id) ?>">Basic Information</a></li>
                                        <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=family&id=' . $id) ?>">Family Information</a></li>
                                        <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=document&id=' . $id) ?>">Document Information </a></li>
                                        <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=previous_school&id=' . $id) ?>">Previous Schools Information</a></li>
                                        <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=fee&id=' . $id) ?>">Fee Information</a></li>
                                        <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=attendance&id=' . $id) ?>">Attendance Information</a></li>
                                        <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=exam&id=' . $id) ?>">Exam Information</a></li>
                                        <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=print&id=' . $id) ?>">Print Documents</a></li>
                                    </ul>	
                                    <div class="span9">
                                        <form action="<?php echo make_admin_url('view', 'list', 'list') ?>" method="GET" id='session_filter_exam'>
                                            <input type='hidden' name='Page' value='view'/>
                                            <input type='hidden' name='action' value='list'/>
                                            <input type='hidden' name='section' value='list'/>
                                            <input type='hidden' name='type' value='exam'/>
                                            <input type='hidden' name='id' value='<?= $id ?>'/>
                                            <div class='span12'>
                                                <div class='span7'></div>
                                                <div class='span5'>															
                                                    <label class="control-label">Select Session</label>
                                                    <div class="controls">
                                                        <select class="select2_category session_filter_exam span11" data-placeholder="Select Session Students" name='session_id' >
                                                            <option value="">Select Session</option>
                                                            <?php
                                                            $session_sr = 1;
                                                            foreach ($Session_list as $s_k => $session):
                                                                ?>
                                                                <option value='<?= $session->session_id ?>' <?
                                                                if ($session->session_id == $session_id) {
                                                                    echo 'selected';
                                                                }
                                                                ?> ><?= ucfirst($session->session_name) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                    ?>
                                                        </select>
                                                    </div>	
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>																
                                        </form>		
                                        <h4 class="form-section hedding_inner1">Exam Detail</h4>
                                        <div id="content">
                                            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                                                <li class="active"><a href="#ist_term" data-toggle="tab"><span style="margin: 10px"><font size="4"><b>Ist Terms</b></font></span></a></li>
                                                <li><a href="#2nd_term" data-toggle="tab" id="2nd_term_div"><span style="margin: 10px"><font size="4"><b>2nd Terms</b></font></span></a></li>
                                                <li><a href="#final_exam" data-toggle="tab" id="final_exam_div"><span style="margin: 10px"><font size="4"><b>Final Exam</b></font></span></a></li>
                                            </ul>
                                            <div id="my-tab-content" class="tab-content">
                                                <div class="tab-pane active" id="ist_term">
                                                    <div class="chart">
                                                        <div id="term_ist"></div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="2nd_term">
                                                    <div class="chart">
                                                        <div id="term_2nd"></div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="final_exam">
                                                    <div class="chart">
                                                        <div id="exam_final"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>		
                                </div>	
                            </div>


                            <!-- Get Print Documents -->
                            <div id="tab9" class="tab-pane <?= ($type == 'print') ? 'active' : ''; ?>">
                                <div class="row-fluid">
                                    <!-- Photo -->
                                    <ul class="unstyled profile-nav span3" id='left_user_info'>
                                        <li>
                                            <?
                                            if (is_object($s_info) && $s_info->photo):
                                                $im_obj = new imageManipulation()
                                                ?>
                                                <div class="item" style='text-align:center;'>	
                                                    <a class="fancybox-button" data-rel="fancybox-button" title="<?= $s_info->first_name . " " . $s_info->last_name ?>" href="<?= $im_obj->get_image('student', 'large', $s_info->photo); ?>">
                                                        <div class="zoom">
                                                            <img src="<?= $im_obj->get_image('student', 'medium', $s_info->photo); ?>" />
                                                            <div class="zoom-icon"></div>
                                                        </div>
                                                    </a>
                                                </div>														
                                            <? else: ?>
                                                <img src="assets/img/profile/img.jpg" alt="">
<? endif; ?> 
                                        </li>
                                        <li><a class='<?= ($type == 'student_detail') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=student_detail&id=' . $id) ?>">Basic Information</a></li>
                                        <li><a class='<?= ($type == 'family') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=family&id=' . $id) ?>">Family Information</a></li>
                                        <li><a class='<?= ($type == 'document') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=document&id=' . $id) ?>">Document Information </a></li>
                                        <li><a class='<?= ($type == 'previous_school') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=previous_school&id=' . $id) ?>">Previous Schools Information</a></li>
                                        <li><a class='<?= ($type == 'fee') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=fee&id=' . $id) ?>">Fee Information</a></li>
                                        <li><a class='<?= ($type == 'attendance') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=attendance&id=' . $id) ?>">Attendance Information</a></li>
                                        <li><a class='<?= ($type == 'exam') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=exam&id=' . $id) ?>">Exam Information</a></li>
                                        <li><a class='<?= ($type == 'print') ? 'selected' : ''; ?>' href="<?= make_admin_url('view', 'list', 'list&type=print&id=' . $id) ?>">Print Documents</a></li>
                                    </ul>	
                                    <div class="span9">
                                        <form action="<?php echo make_admin_url('view', 'list', 'list') ?>" method="GET" id='session_filter'>
                                            <input type='hidden' name='Page' value='view'/>
                                            <input type='hidden' name='action' value='list'/>
                                            <input type='hidden' name='section' value='list'/>
                                            <input type='hidden' name='type' value='print'/>
                                            <input type='hidden' name='id' value='<?= $id ?>'/>
                                            <div class='span12'>
                                                <div class='span7'></div>
                                                <div class='span5'>															
                                                    <label class="control-label">Select Session</label>
                                                    <div class="controls">
                                                        <select class="select2_category session_filter span11" data-placeholder="Select Session Students" name='session_id' >
                                                            <option value="">Select Session</option>
                                                            <?php
                                                            $session_sr = 1;
                                                            foreach ($Session_list as $s_k => $session):
                                                                ?>
                                                                <option value='<?= $session->session_id ?>' <?
                                                                if ($session->session_id == $session_id) {
                                                                    echo 'selected';
                                                                }
                                                                ?> ><?= ucfirst($session->session_name) ?></option>
                                                                        <?
                                                                        $session_sr++;
                                                                    endforeach;
                                                                    ?>
                                                        </select>
                                                    </div>	
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>																
                                        </form>		
                                        <h4 class="form-section hedding_inner1">Document Printing History</h4>				
                                        <div class="portlet-body">
                                            <table class="table table-striped table-bordered table-hover" id="sample_3">
                                                <thead>
                                                    <tr>
                                                        <th class="hidden-480" >Sr. No.</th>
                                                        <th class="hidden-480" style='text-align:center;'>On Date</th>
                                                        <th class="hidden-480">Document Type</th>
                                                        <th class="hidden-480">Reprint</th>
                                                        <th class="hidden-480">Session</th>	
                                                        <th class="hidden-480">Action</th>																						
                                                    </tr>
                                                </thead>
                                                    <? if (!empty($Print_list)): ?>
                                                    <tbody>
                                                        <?php
                                                        $sr = 1;
                                                        foreach ($Print_list as $sk => $p_rec):
                                                            ?>
                                                            <tr class="odd gradeX">
                                                                <td><?= $sr ?>.</td>
                                                                <td style='text-align:center;'><?= $p_rec->on_date ?></td>
                                                                <td><?= $p_rec->name ?></td>
                                                                <td><a href="<?= make_admin_url('view', 'print', 'print&id=' . $p_rec->his_id) ?>">Print Again</a></td>
                                                                <td><?= $p_rec->session_name ?></td>
                                                                <td><a href='<?= make_admin_url('view', 'del_rec', 'del_rec&id=' . $id . '&session_id=' . $session_id . '&del_id=' . $p_rec->his_id) ?>' onclick="return confirm('Are you sure? You are deleting this record.');" class='btn red mini'><i class="icon-remove icon-white"></i></a></td>																					
                                                            </tr>
                                                            <?
                                                            $sr++;
                                                        endforeach;
                                                        ?>									
                                                    </tbody>
<? endif; ?>
                                            </table>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div><br/><br/>
                                        <h4 class="form-section hedding_inner1">Print Documents</h4>
                                        <div class="row-fluid">	
                                            <? if (!empty($session_id)): ?>
                                                <div class="span1">	</div>	
    <? if (!empty($result)): ?>																																			
                                                    <div class="span3">															
                                                        <a class="btn black big" href="<?= make_admin_url('exam', 'group', 'group&session_id=' . $session_id) ?>" style='height:auto;line-height:18px;'>Student DMC <br/><span style='font-size:13px;'>for <?= $session_info->title ?></span></a>
                                                    </div>
                                                <? endif; ?>			
    <? if (is_object($student_icard)): ?>
                                                    <div class="span3">															
                                                        <a class="btn black big" href="<?= make_admin_url('session', 'print', 'print&id=' . $id . '&ct_session=' . $session_id . '&ct_section=' . $section_name . '&doc_id=' . $student_icard->id) ?>" title='Print ID Card' style='height:auto;line-height:18px;'>Student ID Card<br/><span style='font-size:13px;'>for <?= $session_info->title ?></span></a>
                                                    </div>
                                                <? endif; ?>	

    <? if (is_object($certificate)): ?>
                                                    <div class="span4">															
                                                        <a class="btn black big" href="<?= make_admin_url('view', 'update', 'update&id=' . $id . '&session_id=' . $session_id . '&ct_section=' . $section_name . '&doc_id=' . $certificate->id) ?>" style='height:auto;line-height:18px;'>Character Certificate <br/><span style='font-size:13px;'>for <?= $session_info->title ?></span></a>
                                                    </div>
                                                <? endif; ?>	

                                            <? else: ?>
                                                <p style='font-size: 15px; margin-left: 40px;'> To Print the various documents please select the session..!</p>
<? endif; ?>		
                                        </div>


                                    </div>		
                                </div>	
                            </div>


                        </div>

                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>
<script type="text/javascript">

    $(".session_filter").live("change", function () {
        var session_id = $(this).val();
        $('#session_filter').submit();
    });
    $(".session_filter_exam").live("change", function () {
        var session_id = $(this).val();
        $('#session_filter_exam').submit();
    });
    $(".session_filter_fee").live("change", function () {
        var session_id = $(this).val();
        $('#session_filter_fee').submit();
    });
    $(".session_filter_attendance").live("change", function () {
        var session_id = $(this).val();
        if (session_id.length > 0) {
            $('#session_filter_attendance').submit();
        }
    });

</script>
<?php
$obj = new studentSession;
$session = $obj->getStudentCurrentSessionInfo($school->id, $id);
$obj = new examination;
$subjects = $obj->getTotalSubjects($session->id, $session->section);
?>
<script>
    //Ist Term Exam Graph
    var data = [<?php
foreach ($subjects as $subject) {
    $result1 = examination::getMarksObtained($subject->id, 'FA1', $id, $session->id, $session->section);
    $result2 = examination::getMarksObtained($subject->id, 'FA2', $id, $session->id, $session->section);
    $result3 = examination::getMarksObtained($subject->id, 'SA1', $id, $session->id, $session->section);
    $maximum_marks_total1 = ($result1->max_marks + $result2->max_marks + $result3->max_marks);
    $marks_obtained1 = ($result1->marks + $result2->marks + $result3->marks);
    $marks_percent1 = ($marks_obtained1 * 100 / $maximum_marks_total1);
    if ($marks_percent1 != '0') {
        echo round($marks_percent1) . ",";
    }
}
?>];
    var total = 0;
    for (var val in data) {
        total += data[val];
    }
    var dummyData = total;
    data.push(dummyData);
    if (data.length === 2) {
    var seriesColorsArray = ['#DF7401', '#fff'];
    } else if (data.length === 3) {
    var seriesColorsArray = ['#DF7401', '#2E9AFE', '#fff'];
    } else if (data.length === 4) {
    var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#fff'];
    } else if (data.length === 5) {
    var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#fff'];
    } else if (data.length === 6) {
    var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#fff'];
    } else if (data.length === 7) {
    var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#999', '#fff'];
    }
    var dataLabelsArray = [<?php
foreach ($subjects as $subject) {
    echo "'" . $subject->name . "',";
}
?>'OPTIONAL'];
            var plot1 = jQuery.jqplot('term_ist', [data],
                    {
                        seriesDefaults: {
                            // Make this a pie chart.
                            renderer: jQuery.jqplot.PieRenderer,
                            rendererOptions: {
                                highlightMouseOver: false,
                                highlightMouseDown: false,
                                highlightColor: null,
                                startAngle: 180,
                                shadowDepth: 0,
                                dataLabelThreshold: 0,
                                showDataLabels: true,
                                dataLabels: data,
                                dataLabelFormatString: "<span style='font-size: 18px;color: #000 !important'>%s %</span>"
                            }
                        },
                        //Setting the series colors - defined above.. 
                        seriesColors: seriesColorsArray,
                        highlighter: {
                            show: false
                        },
                        // Make the backround transparent, remove canvas borders..
                        grid: {
                            backgroundColor: 'transparent',
                            drawBorder: false,
                            shadow: false
                        },
                        legend: {marginTop: '-200px', labels: dataLabelsArray, show: true, location: 's', rendererOptions: {
                                numberRows: 1
                            }}
                    }
            );

    // Second Term Exam Graph
    $(document).on('click', '#2nd_term_div', function () {
        var data = [<?php
foreach ($subjects as $subject) {
    $result4 = examination::getMarksObtained($subject->id, 'FA3', $id, $session->id, $session->section);
    $result5 = examination::getMarksObtained($subject->id, 'FA4', $id, $session->id, $session->section);
    $result6 = examination::getMarksObtained($subject->id, 'SA2', $id, $session->id, $session->section);
    $maximum_marks_total2 = ($result4->max_marks + $result5->max_marks + $result6->max_marks);
    $marks_obtained2 = ($result4->marks + $result5->marks + $result6->marks);
    $marks_percent2 = ($marks_obtained2 * 100 / $maximum_marks_total2);
    if ($marks_percent2 != '0') {
        echo round($marks_percent2) . ",";
    }
}
?>];
        var total = 0;
        for (var val in data) {
            total += data[val];
        }
        var dummyData = total;
        data.push(dummyData);
        if (data.length === 2) {
        var seriesColorsArray = ['#DF7401', '#fff'];
        } else if (data.length === 3) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#fff'];
        } else if (data.length === 4) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#fff'];
        } else if (data.length === 5) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#fff'];
        } else if (data.length === 6) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#fff'];
        } else if (data.length === 7) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#999', '#fff'];
        }
        var dataLabelsArray = [<?php
foreach ($subjects as $subject) {
    echo "'" . $subject->name . "',";
}
?>'OPTIONAL'];
                var plot1 = jQuery.jqplot('term_2nd', [data],
                        {
                            seriesDefaults: {
                                // Make this a pie chart.
                                renderer: jQuery.jqplot.PieRenderer,
                                rendererOptions: {
                                    highlightMouseOver: false,
                                    highlightMouseDown: false,
                                    highlightColor: null,
                                    startAngle: 180,
                                    shadowDepth: 0,
                                    dataLabelThreshold: 0,
                                    showDataLabels: true,
                                    dataLabels: data,
                                    dataLabelFormatString: "<span style='font-size: 18px;color: #000 !important'>%s %</span>"
                                }
                            },
                            //Setting the series colors - defined above.. 
                            seriesColors: seriesColorsArray,
                            highlighter: {
                                show: false
                            },
                            // Make the backround transparent, remove canvas borders..
                            grid: {
                                backgroundColor: 'transparent',
                                drawBorder: false,
                                shadow: false
                            },
                            legend: {marginTop: '-200px', labels: dataLabelsArray, show: true, location: 's', rendererOptions: {
                                    numberRows: 1
                                }}
                        }
                );
    });

    // Final Exam Graph
    $(document).on('click', '#final_exam_div', function () {
        var data = [<?php
foreach ($subjects as $subject) {
    $result1 = examination::getMarksObtained($subject->id, 'FA1', $id, $session->id, $session->section);
    $result2 = examination::getMarksObtained($subject->id, 'FA2', $id, $session->id, $session->section);
    $result3 = examination::getMarksObtained($subject->id, 'SA1', $id, $session->id, $session->section);
    $result4 = examination::getMarksObtained($subject->id, 'FA3', $id, $session->id, $session->section);
    $result5 = examination::getMarksObtained($subject->id, 'FA4', $id, $session->id, $session->section);
    $result6 = examination::getMarksObtained($subject->id, 'SA2', $id, $session->id, $session->section);
    $final_maximum_marks = ($result1->max_marks + $result2->max_marks + $result3->max_marks + $result4->max_marks + $result5->max_marks + $result6->max_marks);
    $final_marks_obtained = ($result1->marks + $result2->marks + $result3->marks + $result4->marks + $result5->marks + $result6->marks);
    $total_marks_percent = ($final_marks_obtained * 100 / $final_maximum_marks);
    if ($total_marks_percent != '0') {
        echo round($total_marks_percent) . ",";
    }
}
?>];
        var total = 0;
        for (var val in data) {
            total += data[val];
        }
        var dummyData = total;
        data.push(dummyData);
        if (data.length === 2) {
        var seriesColorsArray = ['#DF7401', '#fff'];
        } else if (data.length === 3) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#fff'];
        } else if (data.length === 4) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#fff'];
        } else if (data.length === 5) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#fff'];
        } else if (data.length === 6) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#fff'];
        } else if (data.length === 7) {
        var seriesColorsArray = ['#DF7401', '#2E9AFE', '#3ADF00', '#FE2E64', '#000', '#999', '#fff'];
        }
        var dataLabelsArray = [<?php
foreach ($subjects as $subject) {
    echo "'" . $subject->name . "',";
}
?>'OPTIONAL'];
                var plot1 = jQuery.jqplot('exam_final', [data],
                        {
                            seriesDefaults: {
                                // Make this a pie chart.
                                renderer: jQuery.jqplot.PieRenderer,
                                rendererOptions: {
                                    highlightMouseOver: false,
                                    highlightMouseDown: false,
                                    highlightColor: null,
                                    startAngle: 180,
                                    shadowDepth: 0,
                                    dataLabelThreshold: 0,
                                    showDataLabels: true,
                                    dataLabels: data,
                                    dataLabelFormatString: "<span style='font-size: 18px;color: #000 !important'>%s %</span>"
                                }
                            },
                            //Setting the series colors - defined above.. 
                            seriesColors: seriesColorsArray,
                            highlighter: {
                                show: false
                            },
                            // Make the backround transparent, remove canvas borders..
                            grid: {
                                backgroundColor: 'transparent',
                                drawBorder: false,
                                shadow: false
                            },
                            legend: {marginTop: '-200px', labels: dataLabelsArray, show: true, location: 's', rendererOptions: {
                                    numberRows: 1
                                }}
                        }
                );
    });

    // Include bootstrap Tabs
    jQuery(document).ready(function ($) {
        $('#tabs').tab();
    });
</script>    