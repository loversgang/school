
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <div class="hidden-print" >
			<!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Sessions
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 
		                            <li>
                                            <i class="icon-list"></i>
                                            <a href="<?php echo make_admin_url('student', 'list', 'list');?>"> List Student</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li class="last">
									 <i class="icon-certificate"></i>
									 <a href="<?php echo make_admin_url('view', 'list', 'list&type=print&id='.$id.'&session_id='.$session_id);?>">Student Profile</a> 
									 <i class="icon-angle-right"></i>
								   </li>							
                                
                                    <li class="last">
                                        Print Certificate
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<div class="tile bg-purple <?php echo ($section=='update')?'selected':''?>" id='print_document'>							
						<a class="hidden-print" href="<?php echo make_admin_url('view', 'update', 'update&id='.$id.'&session_id='.$session_id.'&ct_section='.$ct_section.'&doc_id='.$doc_id);?>">
							<div class="corner"></div>
							<div class="tile-body"><i class="icon-arrow-left"></i></div>
							<div class="tile-object"><div class="name">Back To Print</div></div>
						</a>
					</div>					
					
					<div class="tile bg-blue <?php echo ($section=='view')?'selected':''?>">							
						<a class="hidden-print" href="<?php echo make_admin_url('view', 'view', 'view&id='.$id.'&session_id='.$session_id.'&ct_section='.$ct_section.'&doc_id='.$doc_id);?>">
							<div class="corner"></div>
							<div class="tile-body"><i class="icon-edit"></i></div>
							<div class="tile-object"><div class="name">Edit Document</div></div>
						</a>
					</div>							
				</div>            
           <div class="clearfix"></div>

            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
         </div>   
           <br/>	

           <div class="clearfix"></div>
			 
			<div class="row-fluid">
			<form class="form-horizontal" action="<?php echo make_admin_url('view', 'view', 'view&id='.$id.'&session_id='.$session_id.'&ct_section='.$ct_section.'&doc_id='.$doc_id);?>" method="POST" enctype="multipart/form-data" id="validation">
				<div class="control-group">
							<textarea id="document_text" class="span12 ckeditor m-wrap" name="document_text">
								<?=html_entity_decode($content)?>
							</textarea>
				</div> 
				 
				<div class="form-actions">
						 <input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" /> 	
						 <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
						 <a href="<?php echo make_admin_url('view', 'update', 'update&id='.$id.'&session_id='.$session_id.'&ct_section='.$ct_section.'&doc_id='.$doc_id);?>" class="btn" name="cancel" > Cancel</a>
				</div>
			</form>		
			</div>
    <!-- END PAGE CONTAINER-->    
	</div>