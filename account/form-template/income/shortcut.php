

        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('income', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                List Incomes
                        </div>
                </div>
            </a>   
        </div>

        <div class="tile bg-yellow <?php echo ($section=='cat_list')?'selected':''?>">
            <a href="<?php echo make_admin_url('income', 'cat_list', 'cat_list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-retweet"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Income Categories
                        </div>
                </div>
            </a>   
        </div>


        <div class="tile bg-blue <?php echo ($section=='insert')?'selected':''?>">
            <a href="<?php echo make_admin_url('income', 'insert', 'insert');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                New Incomes
                        </div>
                </div>
            </a> 
        </div>

