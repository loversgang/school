
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Time Slots
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-calendar"></i>
                    <a href="<?php echo make_admin_url('time_table', 'list', 'list'); ?>">Time Table Home</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add Time Slots
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>	            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('time_table', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-calendar"></i>Add Time Slots</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <input type="hidden" name="session_year" value="<?php echo $sess_int; ?>"/>
                        <div class="row-fluid">
                            <div class="span12" style="background-color: #f5f5f5; padding: 10px;margin-bottom: 10px">
                                <div class="row-fluid">
                                    <div class="span4">
                                        <div class="control-group">
                                            <label class="control-label" style="text-align: left">Select Session</label>
                                            <select class="span10 select2 m-wrap validate[required]" data-placeholder="Select Session Students" name='session_id' id='session_id'>
                                                <?php
                                                while ($object = $QueryObj->GetObjectFromRecord()):
                                                    $course = get_object('school_course', $object->course_id);
                                                    ?>
                                                    <option value="<?php echo $object->id; ?>"><?php echo $course->course_name; ?></option>
                                                <?php endwhile; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div id="list_all_sections"></div>
                                    <div class="span4">
                                        <div class="control-group">
                                            <label class="control-label" style="text-align: left">Select Day</label>
                                            <select class="span10 select2 m-wrap validate[required]" data-placeholder="Select Day" name='day' id='day'>
                                                <option value="Mon">Monday</option>
                                                <option value="Tue">Tuesday</option>
                                                <option value="Wed">Wednesday</option>
                                                <option value="Thu">Thursday</option>
                                                <option value="Fri">Friday</option>
                                                <option value="Sat">Saturday</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="append_fields" style="padding: 10px">
                                <div class="row-fluid">
                                    <div class="fields_appended">
                                        <div id="list_all_subjects"></div>
                                        <div class="span3">
                                            <div class="control-group control-group-staff staff">
                                                <label class="control-label" style="text-align: left">Select Staff</label>
                                                <select class="span10 select2 m-wrap validate[required]" data-placeholder="Select Staff" name='staff_id[]' id='staff_id'>
                                                    <?php
                                                    foreach ($staff_list as $staff) {
                                                        ?>
                                                        <option value="<?php echo $staff->id; ?>"><?php echo $staff->title; ?> <?php echo $staff->first_name; ?> <?php echo $staff->last_name; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="control-group control-group-staff comment" style="display: none">
                                                <label class="control-label" style="text-align: left">Comment</label>
                                                <textarea name="comment[]" class="m-wrap span10"></textarea>
                                            </div>
                                        </div>
                                        <div class="span2">
                                            <div class="control-group">
                                                <label class="control-label" style="text-align: left" for="time_from">Time From</label> 
                                                <input type="text" name="time_from[]" id="time_from" class="span12 timepicker3 add-on validate[required] m-wrap"/>
                                            </div>
                                        </div>
                                        <div class="span2">
                                            <div class="control-group">
                                                <label class="control-label" style="text-align: left" for="time_to">Time To</label> 
                                                <input type="text" name="time_to[]" id="time_to" class="span12 timepicker3 add-on validate[required] m-wrap"/>
                                            </div>
                                        </div>
                                        <div class="span2">
                                            <label class="control-label">&nbsp;</label>
                                            <button type="button" id="add_more_time_slots" class="btn red"><i class="icon-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="form-actions">
                                <input type='hidden' name="school_id" value='<?= $school->id ?>'/>	
                                <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                <a href="<?php echo make_admin_url('time_table', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->
<script>
    $(document).ready(function () {
        $('#session_id').change();
    });
    $(document).on('change', '#session_id', function () {
        var session_id = $(this).val();
        $('#list_all_sections').html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $('#list_all_subjects').html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=time_table'); ?>", {act: 'get_sections', session_id: session_id}, function (data) {
            $('#list_all_sections').html(data);
        });
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=time_table'); ?>", {act: 'get_subjects', session_id: session_id}, function (data) {
            $('#list_all_subjects').html(data);
        });
    });
    $(document).on('click', '#add_more_time_slots', function () {
        var session_id = $('#session_id').val();
        $('.all_subjects').html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=time_table'); ?>", {act: 'get_subjects_append', session_id: session_id}, function (data) {
            $('.all_subjects').html(data);
        });
        var html = '';
        html += '<div class="row-fluid"><div class="fields_appended"><div class="all_subjects"></div><div class="span3"><div class="control-group control-group-staff staff"><label class="control-label" style="text-align: left"></label><select class="span10 select2 m-wrap validate[required]" data-placeholder="Select Staff" name="staff_id[]" id="staff_id">';
<?php foreach ($staff_list as $staff) { ?>
            html += '<option value="<?php echo $staff->id; ?>"><?php echo $staff->title; ?> <?php echo $staff->first_name; ?> <?php echo $staff->last_name; ?></option>';
<?php } ?>
        html += '</select></div><div class="control-group control-group-staff comment" style="display: none"><label class="control-label" style="text-align: left">Comment</label><textarea name="comment[]" class="m-wrap span10"></textarea></div></div><div class="span2"><div class="control-group"><label class="control-label" style="text-align: left" for="time_from"></label> <input type="text" name="time_from[]" id="time_from" class="span12 timepicker3 add-on validate[required] m-wrap"/></div></div><div class="span2"><div class="control-group"><label class="control-label" style="text-align: left" for="time_to"></label><input type="text" name="time_to[]" id="time_to" class="span12 timepicker3 add-on validate[required] m-wrap"/></div></div><div class="span2"><label class="control-label"></label><button type="button" id="remove_appended_fields" class="btn red"><i class="icon-trash"></i></button></div></div></div>';
        $('#append_fields').append(html);
        FormComponents.init();
    });
    $(document).on('click', '#remove_appended_fields', function () {
        $($(this).parents('.fields_appended')[0]).hide(400, function () {
            $(this).parent().remove();
        });
    });
    $(document).on('change', '#subject', function () {
        var subject_id = $(this).val();
        $(this).closest('.fields_appended').find('.control-group-staff').hide().find('select,textarea');
        if (subject_id === '0') {
            $(this).closest('.fields_appended').find('.control-group-staff.comment').show().find('textarea');
        } else {
            $(this).closest('.fields_appended').find('.control-group-staff.staff').show().find('select');
        }
    });
</script>