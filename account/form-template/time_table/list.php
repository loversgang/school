
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Time Table
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>Time Table</li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>		
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-file-text"></i>Manage Time Table</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row-fluid">
                        <div class='span3' style="display: none">
                            <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                            <select class="select2_category" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                                <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                    <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                                <? endforeach; ?>
                            </select>
                        </div>
                        <div class='span4'>
                            <div id="session_list_details"></div>
                        </div>
                        <div class='span3' id='ajex_section'>
                            <label class="control-label" style='width:auto;'>Select Section</label>
                            <? if ($QuerySec->GetNumRows() > 0): ?>
                                <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                    <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                        <option value='<?= $sec->section ?>'><?= ucfirst($sec->section) ?></option>
                                    <? endwhile; ?>	
                                </select>
                            <? else: ?>
                                <input type='text' class="span10" value='No Section Found.' disabled />
                            <? endif; ?>
                        </div>
                        <div class='span2'>	
                            <label class="control-label">&nbsp;</label>
                            <span class='btn medium green' id='go'>Go</span>									
                        </div>
                    </div>
                    <div id="show_time_table"></div>                                                
                </div>                                                
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

    <script>
        var auto_first = true;
        $(document).ready(function () {
            $('#sess_int').change();
        });

        $(document).on('change', '#sess_int', function () {
            var sess_int = $(this).val();
            $("#session_list_details").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=time_table'); ?>", {act: 'get_sessions_list', sess_int: sess_int}, function (data) {
                $('#session_list_details').html(data);
                if (auto_first === true) {
                    $('.session_filter').change();
                }
            });
        });

        $(document).on('change', '.session_filter', function () {
            var session_id = $("#session_id").val();
            if (session_id.length > 0) {
                var id = session_id;
                $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
                var dataString = 'id=' + id;
                $.ajax({
                    type: "POST",
                    url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section&temp=time_table'); ?>",
                    data: dataString,
                    success: function (data) {
                        $("#ajex_section").html(data);
                        if (auto_first === true) {
                            $("#go").click();
                            auto_first = false;
                        }
                    }
                });
            }
            else {
                return false;
            }
        });

        $("#go").on("click", function () {
            var session_id = $('#session_id').val();
            var ct_sec = $('#ct_sec').val();
            $("#show_time_table").html('<div style="width:100%;text-align:center;padding-top:50px;"><img src="assets/img/ajax-loading.gif"/></div>');
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=time_table'); ?>", {act: 'get_time_table', session_id: session_id, ct_sec: ct_sec}, function (data) {
                $("#show_time_table").html(data);
            });
        });
    </script>

