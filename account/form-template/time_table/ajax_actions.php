<?php
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/timeTableClass.php');

if (isset($_POST)) {
    extract($_POST);
    if ($act == 'get_sections') {
        /* Get session Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);
        ?>
        <div class="span4">
            <div class="control-group">
                <label class="control-label" style="text-align: left">Select Section</label>
                <? if ($QuerySec->GetNumRows() >= 0) { ?>	
                    <select class="m-wrap span10" data-placeholder="Select Sections" name='section' id='section'>
                        <?php
                        $session_sc = 1;
                        while ($sec = $QuerySec->GetObjectFromRecord()) {
                            ?>
                            <option value='<?= $sec->section ?>'><?= ucfirst($sec->section) ?></option>
                            <?
                            $session_sc++;
                        }
                        ?>
                    </select>
                <? } else { ?>
                    <input type='text' class="span10" value='No Section.' disabled />
                    <?
                }
                ?>
            </div>
        </div>   
        <?php
    }
    if ($act == 'get_subjects') {
        // Get session subjects
        $obj = new session;
        $session = $obj->getRecord($session_id);
        $subjects = $session->compulsory_subjects . ',' . $session->elective_subjects;
        $object = new subject;
        $subjects_array = $object->getSessionSubjects($school->id, $subjects);
        ?>
        <div class="span3">
            <div class="control-group">
                <label class="control-label" style="text-align: left">Subject</label>
                <select class="m-wrap span10" data-placeholder="Select Subject" name='subject[]' id='subject'>
                    <?php foreach ($subjects_array as $subject) { ?>
                        <option value='<?= $subject->id ?>'><?= ucfirst($subject->name) ?></option>
                    <?php } ?>
                    <option value='0'>LUNCH BREAK</option>
                </select>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_subjects_append') {
        // Get session subjects
        $obj = new session;
        $session = $obj->getRecord($session_id);
        $subjects = $session->compulsory_subjects . ',' . $session->elective_subjects;
        $object = new subject;
        $subjects_array = $object->getSessionSubjects($school->id, $subjects);
        ?>
        <div class="span3">
            <div class="control-group">
                <label class="control-label" style="text-align: left"></label>
                <select class="m-wrap span10" data-placeholder="Select Subject" name='subject[]' id='subject'>
                    <?php foreach ($subjects_array as $subject) { ?>
                        <option value='<?= $subject->id ?>'><?= ucfirst($subject->name) ?></option>
                    <?php } ?>
                    <option value='0'>LUNCH BREAK</option>
                </select>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_sections_edit') {
        $obj = new timeTable;
        $time_table = $obj->getRecord($time_id);
        /* Get session Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($session_id);
        ?>
        <div class="span4">
            <div class="control-group">
                <label class="control-label" style="text-align: left">Select Section</label>
                <? if ($QuerySec->GetNumRows() >= 0) { ?>	
                    <select class="m-wrap span10" data-placeholder="Select Sections" name='section' id='section'>
                        <?php
                        $session_sc = 1;
                        while ($sec = $QuerySec->GetObjectFromRecord()) {
                            ?>
                            <option value='<?= $sec->section ?>' <?= $time_table->section == $sec->section ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                            <?
                            $session_sc++;
                        }
                        ?>
                    </select>
                <? } else { ?>
                    <input type='text' class="span10" value='No Section.' disabled />
                    <?
                }
                ?>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_subjects_edit') {
        $obj = new timeTable;
        $time_table = $obj->getRecord($time_id);
        // Get session subjects
        $obj = new session;
        $session = $obj->getRecord($session_id);
        $subjects = $session->compulsory_subjects . ',' . $session->elective_subjects;
        $object = new subject;
        $subjects_array = $object->getSessionSubjects($school->id, $subjects);
        ?>
        <div class="span3">
            <div class="control-group">
                <label class="control-label" style="text-align: left">Subject</label>
                <select class="m-wrap span10" data-placeholder="Select Subject" name='subject' id='subject'>
                    <?php foreach ($subjects_array as $subject) { ?>
                        <option value='<?= $subject->id ?>' <?= $time_table->subject == $subject->id ? 'selected' : '' ?>><?= ucfirst($subject->name) ?></option>
                    <?php } ?>
                    <option value='0'<?= $time_table->subject == '0' ? 'selected' : '' ?>>LUNCH BREAK</option>
                </select>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_sessions_list') {
        $dates = explode(',', $sess_int);
        $sess_to = date('Y', strtotime($dates[1]));
        $sess_from = date('Y', strtotime($dates[0]));
        $session_int_array = array($sess_to, $sess_from);
        $session_int = implode('-', $session_int_array);
        $obj = new session;
        $All_Session = $obj->listOfYearSession($school->id, $session_int, 'object');
        ?>
        <label class="control-label" style='width:auto;'>Select Session Class</label>
        <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
            <?php
            if (!empty($All_Session)) {
                if (empty($session_id) && !empty($All_Session)):
                    $session_id = $All_Session['0']->id;
                endif;
                $session_sr = 1;
                foreach ($All_Session as $key => $session):
                    ?>
                    <option value='<?= $session->id ?>' <?
                    if ($session->id == $session_id):
                        echo 'selected';
                    endif;
                    ?> ><?= ucfirst($session->title) ?></option>
                            <?
                            $session_sr++;
                        endforeach;
                    }
                    ?>
        </select>
        <?php
    }
    if ($act == 'get_time_table') {
        $session = get_object('session', $session_id);
        $obj = new timeTable();
        $times = $obj->getSessionTiming($session_id, $ct_sec);
        $temp = array();
        foreach ($times as $time_table) {
            $ts = strtotime($time_table->time_from);
            $temp[$ts] = $time_table;
        }
        ksort($temp);
        $time_tables = $temp;
        ?>
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Time Table > <?php echo $session->title; ?> > <?php echo $ct_sec; ?> </div>
            </div>	
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr style="background: #eee">
                            <th style="text-align: center">Time</th>
                            <th style="text-align: center">Monday</th>
                            <th style="text-align: center">Tuesday</th>
                            <th style="text-align: center">Wednesday</th>
                            <th style="text-align: center">Thursday</th>
                            <th style="text-align: center">Friday</th>
                            <th style="text-align: center">Saturday</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($time_tables as $time_table) { ?>
                            <tr>
                                <td style="background: #eee">
                        <center>
                            <?php echo $time_table->time_from ?> - <?php echo $time_table->time_to ?>
                        </center>
                        </td>
                        <td>
                        <center>
                            <?php
                            $day = timeTable::getClassTime($session_id, $ct_sec, 'Mon', $time_table->time_from, $time_table->time_to);
                            if (is_object($day)) {
                                if ($day->subject == '0') {
                                    echo "LUNCH BREAK" . '<br/>';
                                    echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                } else {
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? $subject->name : '-';
                                    echo "<br/>";
                                    $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                    echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                }
                            } else {
                                echo "-";
                            }
                            ?>
                            <?php if (is_object($day)) { ?>
                                <br/>
                                <a href="<?php echo make_admin_url('time_table', 'update', 'update', 'id=' . $day->id); ?>">Edit</a>
                            <?php } ?>
                        </center>
                        </td>
                        <td>
                        <center>
                            <?php
                            $day = timeTable::getClassTime($session_id, $ct_sec, 'Tue', $time_table->time_from, $time_table->time_to);
                            if (is_object($day)) {
                                if ($day->subject == '0') {
                                    echo "LUNCH BREAK" . '<br/>';
                                    echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                } else {
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? $subject->name : '-';
                                    echo "<br/>";
                                    $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                    echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                }
                            } else {
                                echo "-";
                            }
                            ?>
                            <?php if (is_object($day)) { ?>
                                <br/>
                                <a href="<?php echo make_admin_url('time_table', 'update', 'update', 'id=' . $day->id); ?>">Edit</a>
                            <?php } ?>
                        </center>
                        </td>
                        <td>
                        <center>
                            <?php
                            $day = timeTable::getClassTime($session_id, $ct_sec, 'Wed', $time_table->time_from, $time_table->time_to);
                            if (is_object($day)) {
                                if ($day->subject == '0') {
                                    echo "LUNCH BREAK" . '<br/>';
                                    echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                } else {
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? $subject->name : '-';
                                    echo "<br/>";
                                    $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                    echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                }
                            } else {
                                echo "-";
                            }
                            ?>
                            <?php if (is_object($day)) { ?>
                                <br/>
                                <a href="<?php echo make_admin_url('time_table', 'update', 'update', 'id=' . $day->id); ?>">Edit</a>
                            <?php } ?>
                        </center>
                        </td>
                        <td>
                        <center>
                            <?php
                            $day = timeTable::getClassTime($session_id, $ct_sec, 'Thu', $time_table->time_from, $time_table->time_to);
                            if (is_object($day)) {
                                if ($day->subject == '0') {
                                    echo "LUNCH BREAK" . '<br/>';
                                    echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                } else {
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? $subject->name : '-';
                                    echo "<br/>";
                                    $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                    echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                }
                            } else {
                                echo "-";
                            }
                            ?>
                            <?php if (is_object($day)) { ?>
                                <br/>
                                <a href="<?php echo make_admin_url('time_table', 'update', 'update', 'id=' . $day->id); ?>">Edit</a>
                            <?php } ?>
                        </center>
                        </td>
                        <td>
                        <center>
                            <?php
                            $day = timeTable::getClassTime($session_id, $ct_sec, 'Fri', $time_table->time_from, $time_table->time_to);
                            if (is_object($day)) {
                                if ($day->subject == '0') {
                                    echo "LUNCH BREAK" . '<br/>';
                                    echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                } else {
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? $subject->name : '-';
                                    echo "<br/>";
                                    $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                    echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                }
                            } else {
                                echo "-";
                            }
                            ?>
                            <?php if (is_object($day)) { ?>
                                <br/>
                                <a href="<?php echo make_admin_url('time_table', 'update', 'update', 'id=' . $day->id); ?>">Edit</a>
                            <?php } ?>
                        </center>
                        </td>
                        <td>
                        <center>
                            <?php
                            $day = timeTable::getClassTime($session_id, $ct_sec, 'sat', $time_table->time_from, $time_table->time_to);
                            if (is_object($day)) {
                                if ($day->subject == '0') {
                                    echo "LUNCH BREAK" . '<br/>';
                                    echo '<font style="font-size: 10px;color: green">' . $day->comment . '</font>';
                                } else {
                                    $subject = is_object($day) ? get_object('subject_master', $day->subject) : '';
                                    echo is_object($subject) ? $subject->name : '-';
                                    echo "<br/>";
                                    $staff = is_object($day) ? get_object('staff', $day->staff_id) : '';
                                    echo is_object($staff) ? '<font color="green">' . $staff->title . ' ' . $staff->first_name . ' ' . $staff->last_name . '</font>' : '-';
                                }
                            } else {
                                echo "-";
                            }
                            ?>
                            <?php if (is_object($day)) { ?>
                                <br/>
                                <a href="<?php echo make_admin_url('time_table', 'update', 'update', 'id=' . $day->id); ?>">Edit</a>
                            <?php } ?>
                        </center>
                        </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
    }
}
?>