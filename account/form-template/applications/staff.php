<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Applications
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    All Applications
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box <?
            if ($type == 'confirm'): echo 'green';
            else: echo 'blue';
            endif;
            ?>">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-male"></i>Manage Applications</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div style="overflow-y: hidden">
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <td>Date</td>
                                    <td>Name</td>
                                    <td class="hidden-480">Application Type</td>
                                    <td class="hidden-480">Subject</td>
                                    <td>Message</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($listStaff as $list) { ?>
                                    <tr>
                                        <?php
                                        $staffInformation = application :: staffInformation($list['staff_id']);
                                        ?>
                                        <td><?php echo $list['on_date'] ?></td>
                                        <td width="15%"><?php echo $staffInformation->title . ' ' . $staffInformation->first_name . ' ' . $staffInformation->last_name ?></td>
                                        <td class="hidden-480">
                                            <?php
                                            $leave_type = get_object('leave_types', $list['leave_type_id']);
                                            echo $leave_type->type;
                                            ?>

                                        </td>
                                        <td class="hidden-480"><?php echo $list['subject'] ?></td>
                                        <td>
                                            <div class="span6">
                                                <?php if (strlen($list['message']) > 10) { ?>
                                                    <?php echo substr($list['message'], 0, 10) ?>....
                                                <?php } else { ?>
                                                    <?php echo $list['message'] ?>
                                                <?php } ?>
                                            </div>
                                            <div class="span6">
                                                <button id="read" class="btn green mini" data-toggle="modal" data-target="#myModal" data-id="<?php echo $list['id'] ?>" data-message="<?php echo $list['message'] ?>" data-subject="<?php echo $list['subject'] ?>" data-count="<?php echo $staff_application_count; ?>">View</button>
                                            </div>
                                        </td>
                                        <td>
                                            <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('applications', 'staff_delete', 'staff_delete&id=' . $list['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="" data-original-title="click here to delete record"><i class="icon-remove icon-white"></i></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>                             
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
require 'model.php';
?>
<script>
    $(document).on('click', '#read', function () {
        var id = $(this).attr('data-id');
        var count = $(this).attr('data-count');
        $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=applications'); ?>', {action: 'set_notification_read', id: id}, function () {
//            $('#decrease_notification').html(count - 1);
        });
        var message = $(this).attr('data-message');
        var subject = $(this).attr('data-subject');
        $('#display_message').html(message);
        $('#display_subject').html(subject);
    });
</script>