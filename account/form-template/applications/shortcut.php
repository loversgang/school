<div class="tile bg-green <?php echo ($action == 'student') ? 'selected' : '' ?>">
    <a href="<?php echo (isset($_GET['action']) && $_GET['action'] == 'student') ? 'javascript:;' : make_admin_url('applications', 'student', 'student'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                Student Applications
            </div>
        </div>
    </a>   
</div>
<div class="tile bg-blue <?php echo ($action == 'staff') ? 'selected' : '' ?>">
    <a href="<?php echo (isset($_GET['action']) && $_GET['action'] == 'staff') ? 'javascript:;' : make_admin_url('applications', 'staff', 'staff'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                Staff Applications
            </div>
        </div>
    </a>   
</div>