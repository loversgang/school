<style>
    .modal.fade.in {
        top: 30%;
    }
</style>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
                <h4 class="modal-title" id="display_subject"></h4>
            </div>
            <div class="modal-body">
                <p id="display_message"></p><br />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn blue" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>