<?php
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/subjectClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/syllabusClass.php');

if (isset($_POST)) {
    extract($_POST);
    if ($act == 'get_sections') {
        // Get session subjects
        $obj = new session;
        $session = $obj->getRecord($session_id);
        $subjects = $session->compulsory_subjects . ',' . $session->elective_subjects;
        $object = new subject;
        $subjects_array = $object->getSessionSubjects($school->id, $subjects);
        ?>
        <div class="control-group">
            <label class="control-label">Subject</label>
            <div class="controls">
                <select class="span6" data-placeholder="Select Subject" name='subject_id' id='subject_id'>
                    <?php foreach ($subjects_array as $subject) { ?>
                        <option value='<?= $subject->id ?>'><?= ucfirst($subject->name) ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php
    }
    if ($act == 'get_sections_edit') {
        $obj = new syllabus;
        $syllabus = $obj->getRecord($syllabus_id);

        // Get session subjects
        $obj = new session;
        $session = $obj->getRecord($session_id);
        $subjects = $session->compulsory_subjects . ',' . $session->elective_subjects;
        $object = new subject;
        $subjects_array = $object->getSessionSubjects($school->id, $subjects);
        ?>
        <div class="control-group">
            <label class="control-label">Subject</label>
            <div class="controls">
                <select class="span6" data-placeholder="Select Subject" name='subject_id' id='subject_id'>
                    <?php foreach ($subjects_array as $subject) { ?>
                        <option value='<?= $subject->id ?>' <?= $syllabus->subject_id == $subject->id ? 'selected' : '' ?>><?= ucfirst($subject->name) ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php
    }
}
?>