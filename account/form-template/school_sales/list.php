<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Products
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>List Products</li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>
    <div class="clearfix"></div>
    <div style="clear:both;"></div>	
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-file-text"></i>Manage Products</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div style="overflow-y: hidden">
                        <form method="post">	
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>                                                                                                    
                                        <th>Name</th>					
                                        <th>Sales Category</th>					
                                        <th>Price</th>					
                                        <!--<th>Description</th>-->					
    <!--                                    <th style='width:20%;' class="hidden-480">Image</th>                                         -->

                                        <th>Total Stock</th>
                                        <th>Stock Available</th>
                                        <th>Stock Left</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($lists2 as $key => $list) { ?>
                                        <tr>
                                            <td><?php echo $list['name'] ?></td>		
                                            <td><?php echo $list['title'] ?></td>	
                                            <td><?php echo $list['price'] ?></td>	
    <!--                                            <td>
                                                <div class="span6">
                                            <?php if (strlen($list['description']) < 10) { ?>
                                                <?php echo $list['description'] ?>
                                            <?php } else { ?>
                                                <?php echo substr($list['description'], 0, 10) ?>.....
                                            <?php } ?>
                                                </div>
                                                <div class="span6">

                                                    <button type="button" data-title="<?php echo $list['name'] ?>" data-detail="<?php echo $list['description'] ?>" data-valid="<?php echo $list['file'] ?>" data-file="<?php echo DIR_WS_SITE_UPLOAD . 'file/school_sales/' . $list['file'] ?>" class="btn blue icn-only tooltips mini" id="view">View File</button>

                                                </div>
                                            -->                                            </td>                                                				
                                            <td>
                                                <input type="number" name="stock_available[<?php echo $list['id'] ?>]" class="span6" value="<?php echo isset($list['stock_available']) ? $list['stock_available'] : '' ?>" id="stock_available_<?php echo $list['id'] ?>">
                                            </td>
                                            <td>
                                                <select  name="stock[<?php echo $list['id'] ?>]" id="stock" data-id="<?php echo $list['id'] ?>" class="span12">
                                                    <option value="YES" <?php echo ($list['stock'] == 'YES') ? 'selected' : '' ?>>YES</option>
                                                    <option value="NO" <?php echo ($list['stock'] == 'NO') ? 'selected' : '' ?>>NO</option>
                                                </select>
                                            </td>

                                            <td>
                                                <input type="number" name="stock_left[<?php echo $list['id'] ?>]" class="span6" value="<?php echo isset($list['stock_left']) ? $list['stock_left'] : '' ?>" id="stock_left_<?php echo $list['id'] ?>" <?php echo $list['stock'] == 'NO' ? 'readonly' : '' ?>>
                                            </td>
                                            <td style='text-align:right;'>
                                                <div class="span6">
                                                    <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('school_sales', 'update', 'update', 'id=' . $list['id']) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>
                                                </div>
                                                <div class="span6">
                                                    <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('school_sales', 'delete', 'delete', '&id=' . $list['id']) ?>" onclick="return confirm('Are you sure? You are deleting this record permanently.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                                </div>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>                          
                            </table>
                            <br />
                            <button type="submit" name="submit" class="btn blue icn-only tooltips" title="click Here to save stock">Save Stock</button>
                        </form>    
                    </div>
                </div>
            </div>                                                
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<script>
    $(document).on('click', '#stock', function () {
        var stock = $(this).val();
        var id = $(this).attr('data-id');
        if (stock == 'NO') {
            $('#stock_left_' + id).attr('readonly', true).val(0);
        } else {
            $('#stock_left_' + id).removeAttr('readonly').val(1);
        }
    });
</script>