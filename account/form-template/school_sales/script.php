<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
    $(document).on('click', '#view', function () {
        $('#data-file').hide();
        var data_detail = $(this).attr('data-detail');
        var data_title = $(this).attr('data-title');
        var data_file = $(this).attr('data-file');
        var data_valid = $(this).attr('data-valid');
        $('#data-detail').html(data_detail);
        $('#data-title').html(data_title);
        if (data_valid) {
            $('#data-file').show();
            $('#data-file').html('<a class="mini btn blue icn-only tooltips" href="' + data_file + '" target="_blank">View File</a>');
        }
        $('#myModal').modal();
    });
</script>