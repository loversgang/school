<div class="tile bg-blue">
    <a href="<?php echo ($action == 'list') ? make_admin_url('school_sales_category', 'list', 'list') : 'javascript:;'; ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                Product Categories
            </div>
        </div>
    </a>   
</div>
<div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
    <a href="<?php echo ($action !== 'list') ? make_admin_url('school_sales', 'list', 'list') : 'javascript:;'; ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                List Products
            </div>
        </div>
    </a>   
</div>
<div class="tile bg-blue <?php echo ($section == 'insert') ? 'selected' : '' ?>">
    <a href="<?php echo ($action !== 'insert') ? make_admin_url('school_sales', 'insert', 'insert') : 'javascript:;'; ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-plus"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                New Product
            </div>
        </div>
    </a> 
</div>