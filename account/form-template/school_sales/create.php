<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Products
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('school_sales', 'list', 'list'); ?>">List Products</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add Product
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div id="required"></div>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Add New Product</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="control-group">
                            <label class="control-label" for="name">Name<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="name"  value="" id="course_name" class="span6 m-wrap validate[required]" />
                            </div>
                        </div>   
                        <div class="control-group">
                            <label class="control-label" for="sales_category">Sales Category<span class="required">*</span></label>
                            <div class="controls">
                                <select class="span6 validate[required]"  name="sales_cat_id" id="sales_cat_id">
                                    <option value="">Select Category</option>
                                    <?php foreach ($category_lists as $category_list) { ?>
                                        <option value="<?php echo $category_list['id'] ?>"><?php echo $category_list['title'] ?></option>
                                    <?php } ?> 
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="price">Price<span class="required">*</span></label>
                            <div class="controls">
                                <input type="number" name="price"  value="" id="course_name" class="span6 m-wrap validate[required]" />

                            </div>
                        </div>        
                        <div class="control-group">
                            <label class="control-label" for="stock_available">Total Stock<span class="required">*</span></label>
                            <div class="controls">
                                <input type="number" name="stock_available"  value="" id="stock_available" class="span6 m-wrap validate[required]" />

                            </div>
                        </div>        
                        <div class="control-group">
                            <label class="control-label" for="description">Description<span class="required"></span></label>
                            <div class="controls">
                                <textarea type="text" name="description" id="description" class="span6 m-wrap validate[required]"></textarea>
                            </div>
                        </div>  
                        <div class="control-group">
                            <label class="control-label" for="file">File<span class="required"></span></label>
                            <div class="controls">
                                <input type="file" name="file"  id ="file" value="" class="" > 
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="is_active" style="margin-top: -4px;">Make Active</label> 
                            <div class="controls">
                                <input type="checkbox" name="is_active" checked  />
                            </div>
                        </div>
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" id="submit" /> 
                            <a href="<?php echo make_admin_url('school_sales', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>

                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    

<script>
    $(document).on('click', '#submit', function (event) {
        if ($('#file').val()) {
            var file_information = $('input#file')[0].files[0];
            if (file_information.type == 'image/jpeg' || file_information.type == 'image/png' || file_information.type == 'image/jpg') {
                return;
            } else {
                event.preventDefault();
                $('#required').html('<div class="alert alert-danger"><button class="close" data-dismiss="alert"></button><strong>File not allowed</strong></div>');
            }
        }

    });
</script>