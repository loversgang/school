<?php

include_once(DIR_FS_SITE . 'include/functionClass/class.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');

if (isset($_POST['action']) && $_POST['action'] == 'stuck_off_student') {
    extract($_POST);

    // Inactive Student
    $arr['id'] = $student_id;
    $arr['is_active'] = 0;
    $arr['is_deleted'] = 1;
    $obj = new student;
    $obj->saveStudentDetails($arr);

    // Save Stucked off student Details
    $stuck_arr['student_id'] = $student_id;
    $stuck_arr['session_id'] = $session_id;
    $stuck_arr['stuck_off'] = 1;
    $stuck_arr['revert'] = 0;
    $stuck_arr['stuck_off_date'] = $stuck_date;
    $QueryObj = new stuck_off;
    $QueryObj->saveStuckOffStudent($stuck_arr);
    $admin_user->set_pass_msg('Student Stucked Off Successfully!');
    echo "success";
}
if (isset($_POST['action']) && $_POST['action'] == 'revert_stucked_off_student') {
    extract($_POST);

    // Inactive Student
    $arr['id'] = $student_id;
    $arr['is_active'] = 1;
    $arr['is_deleted'] = 0;
    $obj = new student;
    $obj->saveStudentDetails($arr);

    // Save Stucked off student Details
    $stuck_arr['id'] = $stuck_id;
    $stuck_arr['stuck_off'] = 0;
    $stuck_arr['revert'] = 1;
    $stuck_arr['revert_date'] = date('Y-m-d');
    $QueryObj = new stuck_off;
    $QueryObj->saveStuckOffStudent($stuck_arr);
    $admin_user->set_pass_msg('Student Reverted Successfully!');
    echo "success";
}
?>