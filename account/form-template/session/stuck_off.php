<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Stuck Off List
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    All Stucked Students
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Manage Struck Off List</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <!-- select Session And Section -->
                    <div class="row-fluid">
                        <div class="row-fluid">
                            <div class='span3'>
                                <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                                <select class="select2_category" data-placeholder="Select Session Students" name='sess_int' id='sess_int'>
                                    <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                                        <option value='<?= $val['start_date'] ?>,<?= $val['end_date'] ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                                    <? endforeach; ?>
                                </select>
                            </div>
                            <div class='span4'>
                                <div id="session_list_details"></div>
                            </div>
                            <div class='span3' id='ajex_section'>
                                <label class="control-label" style='width:auto;'>Select Section</label>
                                <? if ($QuerySec->GetNumRows() > 0): ?>
                                    <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                                        <?php while ($sec = $QuerySec->GetObjectFromRecord()): ?>
                                            <option value='<?= $sec->section ?>' <?= ($sec->section == $ct_section) ? 'selected' : '' ?>><?= ucfirst($sec->section) ?></option>
                                        <? endwhile; ?>	
                                    </select>
                                <? else: ?>
                                    <input type='text' class="span10" value='No Section Found.' disabled />
                                <? endif; ?>											
                            </div>
                            <div class='span2'>	
                                <label class="control-label">&nbsp;</label>
                                <span class='btn medium green' id='go'>Go</span>									
                            </div>	
                        </div>	

                        <!-- List OF Student -->
                        <div class="portlet box blue">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-user"></i>Student List </div>
                                <div class="tools">
                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                    <a  href="javascript:;" class="reload"></a>
                                </div>
                            </div>	
                            <div class="portlet-body">								
                                <table class="table table-striped table-bordered table-hover" id="ajex_student">
                                    <thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Student Name</th>
                                            <th>Session</th>
                                            <th>Stuck Off Date</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $k = 1;
                                        foreach ($record as $list) {
                                            ?>
                                            <tr>
                                                <td><?php echo $k++; ?></td>
                                                <td><?php echo $list->first_name . ' ' . $list->last_name; ?></td>
                                                <td><?php echo $list->title; ?></td>
                                                <td><?php echo $list->stuck_off_date; ?></td>
                                                <td><button class="btn blue mini" id="revert_stucked_student" data-stuck_id="<?php echo $list->id; ?>" data-student_id="<?php echo $list->student_id; ?>"><i class="icon-undo"></i> Revert</button></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>	
                            </div>							
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('#sess_int').change();
            }).on('change', '#sess_int', function () {
                var sess_int = $(this).val();
                $("#session_list_details").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
                $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=session'); ?>", {act: 'get_sessions_list', sess_int: sess_int}, function (data) {
                    $('#session_list_details').html(data);
                    $('.session_filter').change();
                });
            }).on('change', '.session_filter', function () {
                var session_id = $("#session_id").val();
                if (session_id.length > 0) {
                    var id = session_id;
                    $("#ajex_section").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
                    var dataString = 'id=' + id;
                    $.ajax({
                        type: "POST",
                        url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section&temp=session'); ?>",
                        data: dataString,
                        success: function (data, textStatus) {
                            $("#ajex_section").html(data);
                        }
                    });
                }
                else {
                    return false;
                }
            }).on("click", '#go', function () {
                /* Filter Students */
                var sess_int = $('#sess_int').val();
                var session_id = $('#session_id').val();
                var ct_sec = $('#ct_sec').val();
                var dataString = 'session_id=' + session_id + '&ct_sec=' + ct_sec + '&sess_int=' + sess_int;
                $("#ajex_student").html('<div style="width:100%;text-align:center;padding-top:50px;"><img src="assets/img/ajax-loading.gif"/></div>');
                $.ajax({
                    type: "POST",
                    url: "<?= make_admin_url_window('ajax_calling', 'ajax_struck_off_section_student', 'ajax_struck_off_section_student&temp=session'); ?>",
                    data: dataString,
                    success: function (data, textStatus) {
                        $("#ajex_student").html(data);
                        App.init();
                        FormComponents.init();
                        TableManaged.init();
                    }
                });
            }).on('click', '#revert_stucked_student', function () {
                var stuck_id = $(this).attr('data-stuck_id');
                var student_id = $(this).attr('data-student_id');
                if (confirm('Are You Sure?')) {
                    $(this).html('<i class="icon-refresh"></i> Reverting..');
                    $.post("<?= make_admin_url_window('ajax_calling', 'ajax_student_stuck_off', 'ajax_student_stuck_off&temp=session'); ?>", {action: 'revert_stucked_off_student', student_id: student_id, stuck_id: stuck_id}, function (data) {
                        if (data === 'success') {
                            window.location.href = "<?php echo make_admin_url('session', 'stuck_off', 'stuck_off') ?>";
                        }
                    });
                }
            }).on('change', '.session_filter_group', function () {
                var session_id = $(this).val();
                $('#session_filter_group').submit();
            });
        </script>