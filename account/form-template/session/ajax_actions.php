<?php
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'get_sessions_list') {
        $dates = explode(',', $sess_int);
        $sess_to = date('Y', strtotime($dates[1]));
        $sess_from = date('Y', strtotime($dates[0]));
        $session_int_array = array($sess_to, $sess_from);
        $session_int = implode('-', $session_int_array);
        #get_all current session 
        $QuerySession = new session();
        $All_Session = $QuerySession->listAllCurrentSessionStruckOff($school->id, '1', $session_int, 'array');
        ?>
        <label class="control-label" style='width:auto;'>Select Session Class</label>
        <select class="select2_category span10 session_filter" data-placeholder="Select Session Students" name='session_id' id='session_id'>
            <?php if (!empty($All_Session)): ?>
                <option value="0">ALL</option>
                <?php
                if (empty($session_id) && !empty($All_Session)):
                    $session_id = $All_Session['0']->id;
                endif;
                $session_sr = 1;
                foreach ($All_Session as $key => $session):
                    ?>
                    <option value='<?= $session->id ?>' <?
                    if ($session->id == $session_id):
                        echo 'selected';
                    endif;
                    ?> ><?= ucfirst($session->title) ?></option>
                            <?
                            $session_sr++;
                        endforeach;
                    else:
                        ?>
                <option value="null">No Session Found..!</option>
            <?php endif; ?>
        </select>
        <?php
    }
}