
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Edit <?= ucfirst($student->first_name) . " " . $student->last_name ?> Details For Session <?= $session->title ?>
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('session', 'list', 'list'); ?>">List Session</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Edit Sessions Student
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <div class="tile bg-red <?php echo ($section == 'section') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('session', 'view', 'view&id=' . $ct_session); ?>">
                <div class="corner"></div>
                <div class="tile-body"><i class="icon-arrow-left"></i></div>
                <div class="tile-object"><div class="name">Session Students</div></div>
            </a> 
        </div>	
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->


    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('session', 'section', 'section&ct_session=' . $ct_session . '&id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Edit Sessions Student</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">      
                        <!-- Basic Info  -->

                        <!-- Reference Detail  -->										
                        <h4 class="form-section hedding_inner">Session Basics</h4>
                        <div class="row-fluid">    
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="year">Session Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" value="<?= $session->title ?>" class="span10 m-wrap" disabled='' />    
                                    </div>
                                </div>
                            </div>
                        </div>		
                        <div class="row-fluid">
                            <div class="span6 ">

                                <div class="control-group">
                                    <label class="control-label" for="start_date">Session Starts From<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" value="<?= date('d M, Y', strtotime($session->start_date)) ?>" id="start_date" class="span10 m-wrap" disabled='' />
                                    </div>
                                </div> 	  
                                <div class="control-group">
                                    <label class="control-label" for="end_date">Student Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" value="<?= $student->first_name . " " . $student->last_name ?>" id="start_date" class="span10 m-wrap" disabled=''/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="roll_no">Roll No<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="roll_no" value="<?= $session_student->roll_no ?>" id="roll_no" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 										
                            </div>
                            <div class="span6 ">				
                                <div class="control-group">
                                    <label class="control-label" for="end_date">Session Ends On<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" value="<?= date('d M, Y', strtotime($session->end_date)) ?>" id="start_date" class="span10 m-wrap" disabled=''/>
                                    </div>
                                </div> 	
                                <div class="control-group">
                                    <label class="control-label" for="">Registration ID<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" value="<?= $student->reg_id ?>" id="" class="span10 m-wrap" disabled=''/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="section">Section<span class="required">*</span></label>
                                    <div class="controls">
                                        <?
                                        if (!empty($all_section)):
                                            echo "<select name=section class='span10 m-wrap validate[required]'/>";
                                            foreach ($all_section as $s_key => $s_val):
                                                ?>
                                                <option value='<?= $s_val ?>' <?
                                                if ($session_student->section == $s_val): echo "selected";
                                                endif;
                                                ?>><?= $s_val ?></option>
                                                        <?
                                                    endforeach;
                                                    echo "</select>";
                                                else:
                                                    ?>
                                            <input type="text" name="section" value="<?= $session_student->section ?>" id="section" class="span10 m-wrap validate[required]"/>
                                        <? endif; ?>

                                    </div>
                                </div>				
                                <div class="control-group">
                                    <label class="control-label" for="section">Stuck Off Student</label>
                                    <div class="controls">
                                        <a class="btn red" id="stuck_student" style="margin-left: 10px"><i class="icon-circle"></i> Stuck Off</a>
                                    </div>
                                </div>
                            </div>	
                        </div>
                        <h4 class="form-section hedding_inner">Student Granted</h4>	
                        <div class="row-fluid">

                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="concession">Student Granted<span class="required">*</span></label>
                                    <div class="controls">
                                        <input name="concession" style='padding-left:5px;' value="<?php echo $session_student->concession ?>" id='concession' class="span10 m-wrap"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="concession_type">Type<span class="required">*</span></label>
                                    <div class="controls">
                                        <select name="concession_type" id='concession_type' class="span10 m-wrap">
                                            <option value="one_time" <?
                                            if (is_object($session_student)): if ($session_student->concession_type == 'one_time'):
                                                    echo "selected";
                                                endif;
                                            endif;
                                            ?>>One Time</option>
                                            <option value="regular" <?
                                            if (is_object($session_student)): if ($session_student->concession_type == 'regular'):
                                                    echo "selected";
                                                endif;
                                            endif;
                                            ?>>Regular</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label for="amount" class="control-label">Discount From Tuition Fee</label>
                                    <div class="controls">
                                        <input name="amount" style='padding-left:5px;' value='<?php
                                        if (is_object($session_student)): echo $session_student->amount;
                                        endif;
                                        ?>' id='amount' class="span10 m-wrap validate[custom[onlyNumberSp]]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="start_concession" class="control-label"></label>
                                    <div class="controls">
                                        <?php
                                        if (is_object($session_student)):
                                            if ($session_student->start_concession == '1'):
                                                ?>
                                                <button class="btn red" id="stop_concession" data-rel_id="<?php echo $session_student->id; ?>" data-student_id="<?php echo $student->id; ?>">Stop Concession</button>
                                            <?php else: ?>
                                                <button type="button" class="btn blue" id="start_concession" data-rel_id="<?php echo $session_student->id; ?>" data-student_id="<?php echo $student->id; ?>">Start Concession</button>
                                            <?php
                                            endif;
                                        else:
                                            ?>
                                            <div class="alert alert-danger span10">Add Student To Session First..!!</div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>	

                        <h4 class="form-section hedding_inner">Assign Vehicle</h4>
                        <?php
                        /* display message */
                        display_message(1);
                        $error_obj->errorShow();
                        ?>
                        <div class="row-fluid">		
                            <div class='span6'>
                                <?php
                                $chk = vehicleStudent::chkStudentAssigned($student->id);
                                if ($chk) {
                                    $student_vehicle = vehicleStudent::getStudentRoot($student->id);
                                    ?>
                                    <div class="control-group">		
                                        <label class="control-label" for="vehicle_route">Select Route<span class="required"></span></label>
                                        <div class="controls">
                                            <select id="vehicle_route" class="span10 m-wrap">
                                                <?php foreach ($routes as $route) { ?>
                                                    <option value="<?= $route->id; ?>"
                                                            <?= $student_vehicle->route_id == $route->id ? 'selected' : '' ?>><?= $route->route_from . " To " . $route->route_to; ?></option>
                                                        <?php } ?>
                                            </select>
                                        </div>	
                                    </div>
                                    <div id="vehicle_stoppages"></div>
                                <?php } else { ?>
                                    <div class="control-group">		
                                        <label class="control-label" for="vehicle_route">Select Route<span class="required"></span></label>
                                        <div class="controls">
                                            <select id="vehicle_route" class="span10 m-wrap">
                                                <?php foreach ($routes as $route) { ?>
                                                    <option value="<?= $route->id; ?>"><?= $route->route_from . " To " . $route->route_to; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>	
                                    </div>
                                    <div id="vehicle_stoppages"></div>
                                <?php } ?>
                                <div class="control-group">
                                    <label class="control-label" for="subscription_id">Select Vehicle<span class="required">*</span></label>
                                    <div class="controls">							
                                        <?php if ($QueryVeh->GetNumRows() > 0): ?>
                                            <select name="vehicle_id" id="select_vehicle" class="span10 m-wrap">	
                                                <?
                                                $session_sr = 1;
                                                while ($vehicle = $QueryVeh->GetObjectFromRecord()):
                                                    if ($session_sr == '1'):
                                                        $current_vehicle = get_object('school_vehicle', $vehicle->id);
                                                    endif;
                                                    ?>
                                                    <option value='<?= $vehicle->id ?>' <?
                                                    if (is_object($VehstudentAdd) && $VehstudentAdd->vehicle_id == $vehicle->id): echo 'selected';
                                                    endif;
                                                    ?> ><?= ucfirst($vehicle->model) ?></option>
                                                            <?
                                                            $session_sr++;
                                                        endwhile;
                                                        ?>
                                            </select>
                                        <? else: $current_vehicle = ''; ?>
                                            <input type="text" value='No Vehicle Found..!' id="select_vehicle" disabled='disabled' class="span10 m-wrap validate[required]"/>
                                        <? endif; ?>
                                    </div>
                                </div>
                                <div id="get_vehicle_round">
                                    <?php
                                    $chk = vehicleStudent::chkStudentAssigned($student->id);
                                    if ($chk) {
                                        $student_vehicle = vehicleStudent::getStudentRoot($student->id);
                                        $obj = new vehicle;
                                        $vehicle = $obj->getRecord($student_vehicle->vehicle_id);
                                        //pr($vehicle); exit;
                                        $rounds = $vehicle->possible_rounds;
                                        ?>
                                        <div class="control-group">
                                            <label class="control-label" for="round">Select Round</label>
                                            <div class="controls">
                                                <select class="m-wrap span10" id="round">
                                                    <?php
                                                    for ($i = 1; $i <= $rounds; $i++) {
                                                        ?>
                                                        <option value="<?= $i ?>" <?= $student_vehicle->round == $i ? 'selected' : '' ?>>Round <?= $i ?>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                    } else {
                                        $obj = new vehicle;
                                        $vehicle = $obj->getRecord($first_vehicle->id);
                                        $rounds = $vehicle->possible_rounds;
                                        ?>
                                        <div class="control-group">
                                            <label class="control-label" for="round">Select Round</label>
                                            <div class="controls">
                                                <select class="m-wrap span10" id="round">
                                                    <?php
                                                    for ($i = 1; $i <= $rounds; $i++) {
                                                        ?>
                                                        <option value="<?= $i ?>">Round <?= $i ?>
                                                        <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>	
                            <div class='span6'>			
                                <? if (is_object($current_vehicle)): ?>	
                                    <div class="control-group">
                                        <label for="amount" class="control-label">Amount(<?= CURRENCY_SYMBOL ?>) (Per Fee)<span class="required">*</span></label>
                                        <div class="controls">
                                            <input name="amount" style='padding-left:5px;' id='vehicle_amount' value="<?
                                            if (is_object($VehstudentAdd)): echo $VehstudentAdd->amount;
                                            endif;
                                            ?>" class="span10 m-wrap" disabled/>
                                        </div>
                                    </div>
                                <? else: ?>	
                                    <div class="control-group">		
                                        <label class="control-label" for="amount">Amount(<?= CURRENCY_SYMBOL ?>)<span class="required"></span></label>
                                        <div class="controls">
                                            <input type="text" value='No Vehicle Found..!' id="vehicle_amount" class="span10 m-wrap" disabled/>
                                        </div>	
                                    </div>					
                                <? endif; ?>
                                <div id="full_vehicle_detail">
                                    <div class="control-group">
                                        <label class="control-label" for="ref_phone">Vehicle Number</label>
                                        <div class="controls">
                                            <input type="text" value="<?
                                            if (is_object($VehstudentAdd)): echo $VehstudentAdd->vehicle_number;
                                            elseif (is_object($current_vehicle)): echo $current_vehicle->vehicle_number;
                                            endif;
                                            ?>" id="ref_phone" class="span10" disabled="disabled"/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="ref_name">Vehicle Make</label>
                                        <div class="controls">
                                            <input type="text" value="<?
                                            if (is_object($VehstudentAdd)): echo $VehstudentAdd->make;
                                            elseif (is_object($current_vehicle)): echo $current_vehicle->make;
                                            endif;
                                            ?>" id="ref_name" class="span10 m-wrap" disabled="disabled"/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <? if (is_object($current_vehicle)): ?>
                                                <? if (is_object($VehstudentAdd) && strtotime($VehstudentAdd->to_date) >= strtotime(date('Y-m-d'))): ?>	
                                                    <span class="btn red button-next check_valid" type="submit" name="stop_vehicle" tabindex="7" id='vehicle_service_end'> Stop Vehicle Service </span> &nbsp;&nbsp;
                                                <? else: ?>
                                                    <span class="btn blue button-next check_valid" type="submit" name="submit" tabindex="7" id='vehicle_service_start'> Start Vehicle Service </span> &nbsp;&nbsp;
                                                <? endif; ?>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>	
                        </div>

                        <h4 class="form-section hedding_inner">Session Fee Details</h4>										
                        <div class="row-fluid">
                            <div class="control-group span8">
                                <div class="controls" >
                                    <table class="table table-striped table-bordered table-advance table-hover">
                                        <thead>
                                            <tr><th>Fee Heads Name</th><th>Amount (<?= CURRENCY_SYMBOL ?>)</th>
                                        <tbody id='add_delete_heads'></tbody>
                                        <?
                                        $t_am = '0';
                                        $new_added = 0;
                                        if (!empty($pay_heads)): $st = '0';
                                            foreach ($pay_heads as $p_k => $p_v):
                                                if (array_key_exists($p_v['fee_head'], $Sesspay_heads)):
                                                    $a = $p_v['fee_head'];
                                                    unset($Sesspay_heads[$a]);
                                                endif;
                                                ?>
                                                <tr class='delete_row<?= $p_v['fee_head'] ?>'><td><?= $p_v['title'] ?> &nbsp;(<?= ucfirst($p_v['type']) ?>)</td><td>
                                                        <input type="hidden" name="head[<?= $p_v['fee_head'] ?>][title]" value="<?= $p_v['fee_head'] ?>"/>
                                                        <input class="span10 new_row change_value" type="text" placeholder="0.00" name="head[<?= $p_v['fee_head'] ?>][fee]" value="<?= $p_v['amount'] ?>" add="<?
                                                        if ($p_v['type'] == 'Regular'): echo '1';
                                                        else: echo '0';
                                                        endif;
                                                        ?>"/></td></tr>
                                                    <?
                                                    if (ucfirst($p_v['type']) == 'Regular'):
                                                        $ct_amount = $p_v['amount'] * $session->frequency;
                                                    else:
                                                        $ct_amount = $p_v['amount'];
                                                    endif;
                                                    $t_am = $t_am + $ct_amount;
                                                endforeach;
                                            endif;

                                            # Now display if new heads of array
                                            if (!empty($Sesspay_heads)): $new_added = '1';
                                                foreach ($Sesspay_heads as $new_key => $new_value):
                                                    ?>
                                                <tr class='delete_row<?= $new_value['fee_head'] ?> removed'><td><?= $new_value['title'] ?> &nbsp;(<?= ucfirst($new_value['type']) ?>)</td><td>
                                                        <input type="hidden" name="head[<?= $new_value['fee_head'] ?>][title]" value="<?= $new_value['fee_head'] ?>"/>
                                                        <input class="span10 new_row change_value" type="text" placeholder="0.00" name="head[<?= $new_value['fee_head'] ?>][fee]" value="<?= $new_value['amount'] ?>" add="<?
                                                        if ($new_value['type'] == 'Regular'): echo '1';
                                                        else: echo '0';
                                                        endif;
                                                        ?>"/></td></tr>
                                                    <?
                                                    if (ucfirst($new_value['type']) == 'Regular'):
                                                        $ct_amount = $new_value['amount'] * $session->frequency;
                                                    else:
                                                        $ct_amount = $new_value['amount'];
                                                    endif;
                                                    $t_am = $t_am + $ct_amount;
                                                endforeach;
                                            endif;
                                            ?>
                                        </thead>
                                    </table>
                                </div>  
                            </div> 
                            <? if ($new_added): ?>
                                <div class="alert alert-block alert-danger fade in span4">New Fee Heads Added to session</div>
                            <? endif; ?>	
                            <div class="clearfix"></div>				

                        </div>										
                        <!-- Subject Setting  -->
                        <h4 class="form-section hedding_inner">Choose Subjects</h4>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="user_name"><strong>Compulsory Subjects</strong></label>
                                    <div class="controls">
                                        <?
                                        if (!empty($ComSessSub)):
                                            foreach ($ComSessSub as $com_key => $com_value):
                                                ?>
                                                <label class="checkbox line">
                                                    <input type="checkbox" value="<?= $com_value->id ?>" <?
                                                    if (in_array($com_value->id, $cmpSub)) {
                                                        echo 'checked';
                                                    }
                                                    ?> checked disabled /><?= $com_value->name ?>
                                                    <input type='hidden' name="Compulsory[]"  value="<?= $com_value->id ?>"/>
                                                </label>
                                                <?
                                            endforeach;
                                        else:
                                            echo '<p><br/><br/>Sorry,No Subject Found..!</p>';
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label" for="user_name"><strong>Elective Subjects</strong></label>
                                    <div class="controls">
                                        <?
                                        if (!empty($ElcSessSub)):
                                            foreach ($ElcSessSub as $elc_key => $elc_value):
                                                ?>
                                                <label class="checkbox line">
                                                    <input type="checkbox" name="Elective[]"  value="<?= $elc_value->id ?>" <?
                                                    if (in_array($elc_value->id, $elcSub)) {
                                                        echo 'checked';
                                                    }
                                                    ?>/><?= $elc_value->name ?>
                                                </label>
                                                <?
                                            endforeach;
                                        else:
                                            echo '<p><br/><br/>Sorry,No Subject Found..!</p>';
                                        endif;
                                        ?>
                                    </div>
                                </div>
                            </div>											
                        </div>

                        <div class="form-actions">
                            <input type="hidden" name="student_id" value="<?= $session_student->student_id ?>" tabindex="7" /> 
                            <input type='hidden' value="<?= $session->frequency ?>" id='frequency'/>
                            <input type="hidden" name="id" value="<?= $id ?>" tabindex="7" />
                            <input type="hidden" name="session_id" value="<?= $ct_session ?>" tabindex="7" />													 
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('session', 'view', 'view&id=' . $ct_session); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->   
<div class="modal fade" id="stuck_off_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h3 class="modal-title" id="myModalLabel" style="text-align: center">
                    <b id="label_store_name"><code>Stuck OFF Date</code></b>
                </h3>
            </div>
            <div class="modal-body" style="height: auto;overflow: visible !important">
                <div id="content">
                    <div class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label">Student Name</label>
                            <div class="controls">
                                <input type="text" value="<?= $student->first_name . " " . $student->last_name ?>" disabled/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Stuck Off Date</label>
                            <div class="controls">
                                <input type="text" id="stuck_date" value="<?php echo date('Y-m-d') ?>" class="new_format validate[required]"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="stuck_off_student" class="btn blue pull-right">Save</button>
                <button type="button" class="btn default" style="margin-right: 10px" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTAINER-->    
<script type="text/javascript">
    jQuery(document).ready(function () {
        $(".change_value").live("change", function () {
            GetValue();
        });

        $("#select_vehicle").live("change", function () {
            var id = $(this).val();
            var session_id = '<?= $session->id ?>';
            var ct_sec = '<?= $session_student->section ?>';
            var student_id = '<?= $student->id ?>';
            $('#vehicle_amount').css("border", "1px solid #E5E5E5");
            $("#full_vehicle_detail").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id + '&session_id=' + session_id + '&ct_sec=' + ct_sec + '&student_id=' + student_id;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'multi_vehicle', 'multi_vehicle&temp=session'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    var dataUrl = 'act=get_vehicle_round&vehicle_id=' + id + '&student_id=' + student_id;
                    $.ajax({
                        type: "POST",
                        url: "<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=vehicles'); ?>",
                        data: dataUrl,
                        success: function (data, textStatus) {
                            $('#get_vehicle_round').html(data);
                        }
                    });
                    $("#full_vehicle_detail").html(data);
                }
            });

        });

        $("#vehicle_service_start").live("click", function () {
            toastr.remove();
            var amount = $('#vehicle_amount').val();
            if (amount.length > 0) {
                $('#vehicle_amount').css("border", "1px solid #E5E5E5");
            }
            else {
                $('#vehicle_amount').css("border", "1px solid #F7023B");
                return false;
            }
            var id = $('#select_vehicle').val();
            var route_id = $('select#vehicle_route').val();
            var round = $('select#round').val();
            var stoppage_id = $("#vehicle_stoppage option:selected").attr('data-attr');
            var session_id = '<?= $session->id ?>';
            var ct_sec = '<?= $session_student->section ?>';
            var student_id = '<?= $student->id ?>';
            $("#full_vehicle_detail").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id + '&session_id=' + session_id + '&ct_sec=' + ct_sec + '&student_id=' + student_id + '&route_id=' + route_id + '&stoppage_id=' + stoppage_id + '&round=' + round + '&amount=' + amount + '&start=1';
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'multi_vehicle', 'service&temp=session'); ?>",
                data: dataString,
                success: function (data) {
                    data = $.parseJSON(data);
                    if (data.status === 'assigned') {
                        toastr.success('Student has been successfully added into vehicle.', 'Success');
                    } else if (data.status === 'unassigned') {
                        toastr.success('Operation has been performed successfully!', 'Success');
                    } else {
                        toastr.error('Something Went Wrong!');
                    }
                    $("#full_vehicle_detail").html(data.html);
                    $('#vehicle_route').change();
                }
            });
        });

        $("#vehicle_service_end").live("click", function () {
            toastr.remove();
            var amount = $('#vehicle_amount').val();
            if (amount.length > 0) {
                $('#vehicle_amount').css("border", "1px solid #E5E5E5");
            }
            else {
                $('#vehicle_amount').css("border", "1px solid #F7023B");
                return false;
            }
            var id = $('#select_vehicle').val();
            var route_id = $('select#vehicle_route').val();
            var round = $('select#round').val();
            var stoppage_id = $("#vehicle_stoppage option:selected").attr('data-attr');
            var session_id = '<?= $session->id ?>';
            var ct_sec = '<?= $session_student->section ?>';
            var student_id = '<?= $student->id ?>';
            $("#full_vehicle_detail").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id + '&session_id=' + session_id + '&ct_sec=' + ct_sec + '&student_id=' + student_id + '&route_id=' + route_id + '&stoppage_id=' + stoppage_id + '&round=' + round + '&amount=' + amount + '&start=0';
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'multi_vehicle', 'service&temp=session'); ?>",
                data: dataString,
                success: function (data) {
                    data = $.parseJSON(data);
                    if (data.status === 'assigned') {
                        toastr.success('Student has been successfully added into vehicle.', 'Success');
                    } else if (data.status === 'unassigned') {
                        toastr.success('Operation has been performed successfully!', 'Success');
                    } else {
                        toastr.error('Something Went Wrong!');
                    }
                    $("#full_vehicle_detail").html(data.html);
                    $('#vehicle_route').change();
                }
            });
        });

    });

    function GetValue() {
        var Contain = "0";
        var frequency = 0;
        frequency = $("#frequency").val();
        if (frequency == "") {
            frequency = 1;

        }

        $(".new_row").each(function () {

            if ($(this).val() != '') {
                if ($(this).attr('add') == "1") {
                    head_val = $(this).val().replace(",", "");
                    Contain1 = parseInt(frequency) * parseInt(head_val);
                }
                else {
                    head_val = $(this).val().replace(",", "");
                    Contain1 = parseInt(head_val);
                }
            }
            Contain = parseInt(Contain) + parseInt(Contain1);
        });
        $('#make_total').html(Contain);
    }

    $(document).on('click', '#stuck_off_student', function () {
        var stuck_date = $('#stuck_date').val();
        var student_id = "<?= $student->id ?>";
        var session_id = "<?= $ct_session ?>";
        if (confirm('Are You Sure?')) {
            $(this).html('<i class="icon-refresh"></i> Stuck Off');
            $.post("<?= make_admin_url_window('ajax_calling', 'ajax_student_stuck_off', 'ajax_student_stuck_off&temp=session'); ?>", {action: 'stuck_off_student', student_id: student_id, session_id: session_id, stuck_date: stuck_date}, function (data) {
                if (data === 'success') {
                    window.location.href = "<?php echo make_admin_url('session', 'stuck_off', 'stuck_off') ?>";
                }
            });
        }
    });

    $(document).on('change', '#vehicle_route', function () {
        var route_id = $(this).val();
        var student_id = "<?= $student->id ?>";
        $("#vehicle_stoppages").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_stoppages', 'ajax_stoppages&temp=session'); ?>", {act: 'get_stoppages', route_id: route_id, student_id: student_id}, function (data) {
            $('#vehicle_stoppages').html(data);
            $('#vehicle_stoppage').change();
        });
    }).ready(function () {
        $('#vehicle_route').change();
    });

    $(document).on('change', '#vehicle_stoppage', function () {
        var amount = $(this).val();
        $('input#vehicle_amount').val(amount);
    });
    $(document).on('click', '#stuck_student', function () {
        var modal = $('#stuck_off_modal');
        modal.modal();
    });

    var ajax_url = "<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=student'); ?>";
    $(document).on('click', '#stop_concession', function (e) {
        $(this).html('Stopping..Please Wait..');
        toastr.remove();
        e.preventDefault();
        var rel_id = $(this).attr('data-rel_id');
        var student_id = $(this).attr('data-student_id');
        var concession = $('#concession').val();
        var concession_type = $('#concession_type').val();
        var amount = $('#amount').val();
        $.post(ajax_url, {act: 'stop_concession', rel_id: rel_id, student_id: student_id, concession: concession, concession_type: concession_type, amount: amount}, function (data) {
            if (data === 'success') {
                toastr.success('Operation Performed Successfully!', 'Success');
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            }
        });
    });
    $(document).on('click', '#start_concession', function (e) {
        $(this).html('Starting..Please Wait..');
        toastr.remove();
        e.preventDefault();
        var rel_id = $(this).attr('data-rel_id');
        var student_id = $(this).attr('data-student_id');
        var concession = $('#concession').val();
        var concession_type = $('#concession_type').val();
        var amount = $('#amount').val();
        $.post(ajax_url, {act: 'start_concession', rel_id: rel_id, student_id: student_id, concession: concession, concession_type: concession_type, amount: amount}, function (data) {
            if (data === 'success') {
                toastr.success('Operation Performed Successfully!', 'Success');
                setTimeout(function () {
                    window.location.reload();
                }, 1000);
            }
        });
    });
</script>
