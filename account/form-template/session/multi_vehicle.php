<?
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');

if ($_POST):
    isset($_POST['id']) ? $id = $_POST['id'] : $id = '';
    isset($_POST['session_id']) ? $session_id = $_POST['session_id'] : $session_id = '';
    isset($_POST['student_id']) ? $student_id = $_POST['student_id'] : $student_id = '';
    isset($_POST['ct_sec']) ? $ct_sec = $_POST['ct_sec'] : $ct_sec = '';

    #get Vehicle student info
    $QueryChkStu = new vehicleStudent();
    $VehstudentAdd = $QueryChkStu->checkVehicleStudentAssignCurrentVehicle($session_id, $ct_sec, $student_id, $id);
// List Vehicle Routes
    #get Vehicle
    $current_vehicle = get_object('school_vehicle', $id);
    $obj = new vehicle_route;
    $routes = $obj->listVehicleRoute($school->id);

    #get All Active Vehicle List
    $QueryVeh = new vehicle();
    $QueryVeh->listAll($school->id, '1');
    ?>
    <div class="control-group">
        <label class="control-label" for="ref_name1">Vehicle Make</label>
        <div class="controls">
            <input type="text" value="<?
            if (is_object($current_vehicle)) {
                echo $current_vehicle->make;
            }
            ?>" id="ref_name1" class="span10 m-wrap" disabled="disabled"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="ref_phone1">Vehicle Number</label>
        <div class="controls">
            <input type="text" value="<?
            if (is_object($current_vehicle)) {
                echo $current_vehicle->vehicle_number;
            }
            ?>" id="ref_phone1" class="span10" disabled="disabled"/>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <? if (is_object($current_vehicle)): ?>
                <? if (is_object($VehstudentAdd) && strtotime($VehstudentAdd->to_date) >= strtotime(date('Y-m-d'))): ?>	
                    <span class="btn red button-next check_valid" type="submit" name="stop_vehicle"  tabindex="7" id='vehicle_service_end'> Stop Vehicle Service </span> &nbsp;&nbsp;
                <? else: ?>
                    <span class="btn blue button-next check_valid" type="submit" name="submit"  tabindex="7" id='vehicle_service_start'> Start Vehicle Service </span> &nbsp;&nbsp;
                <? endif; ?>
            <? endif; ?>
        </div>
    </div>
    <?
endif;
?>