<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Student Details For Session <?= $object->title ?>
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('session', 'list', 'list'); ?>"> List Sessions</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li class="last">
                    <i class="icon-certificate"></i>
                    <?= $object->title ?> >
                    Students
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <div class="tile bg-purple <?php echo ($section == 'view') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('session', 'list', 'list'); ?>">
                <div class="corner"></div>
                <div class="tile-body"><i class="icon-arrow-left"></i></div>
                <div class="tile-object"><div class="name">Back To List</div></div>
            </a> 
        </div>
        <div class="tile bg-red <?php echo ($section == 'stuck_off') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('session', 'stuck_off', 'stuck_off'); ?>">
                <div class="corner"></div>

                <div class="tile-body">
                    <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Stuck Off List
                    </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-grey">
            <a href="<?php echo make_admin_url('student', 'insert', 'insert'); ?>">
                <div class="corner"></div>
                <div class="tile-body"><i class="icon-plus-sign"></i></div>
                <div class="tile-object"><div class="name">Create New Student</div></div>
            </a> 
        </div>								
        <div class="tile bg-yellow <?php echo ($section == 'previous') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('session', 'previous', 'previous&ct_session=' . $id); ?>">
                <div class="corner"></div>
                <div class="tile-body"></div>
                <div class="tile-object"><div class="name">Add From Existing Session</div></div>
            </a> 
        </div>								
        <div class="tile bg-blue <?php echo ($section == 'other') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('session', 'other', 'other&ct_session=' . $id); ?>">
                <div class="corner"></div>
                <div class="tile-body"></div>
                <div class="tile-object"><div class="name">Add From All Students</div></div>
            </a> 
        </div>	
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <form class="form-horizontal" action="<?php echo make_admin_url('session', 'view', 'view&id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
                <div class="portlet box red">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i><?= $object->title ?> > Students</div>								
                    </div>
                    <div class="portlet-body">
                        <div class="clearfix">
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th class="hidden-480">Section</th>
                                    <th class="hidden-480">Roll No.</th>
                                    <th class="hidden-480">Student Name</th>
                                    <th class="hidden-480">Father Name</th>
                                    <th class="hidden-480">Gender</th>
                                    <th class="hidden-480">Student Granted</th>
                                    <th class="hidden-480 sorting_disabled" style='text-align:right;'>Action</th>
                                </tr>
                            </thead>
                            <? if (!empty($AllStu)): ?>
                                <tbody>
                                    <?php
                                    $sr = 1;
                                    foreach ($AllStu as $sk => $stu): $checked = '';
                                        ?>
                                        <tr class="odd gradeX" <?
                                        if (in_array($stu->id, $session_st)): echo 'id="added"';
                                            $checked = 'checked';
                                        endif;
                                        ?> >
                                            <td class="center hidden-480"><?= $stu->section ?></td>
                                            <td class="center hidden-480"><?= $stu->roll_no ?></td>											
                                            <td><?= $stu->first_name . " " . $stu->last_name ?></td>
                                            <td><?= $stu->father_name ?></td>
                                            <td class="hidden-480"><?= $stu->sex ?></td>											
                                            <td class="center hidden-480"><?
                                                if (empty($stu->concession)): echo "None";
                                                else: echo $stu->concession;
                                                endif;
                                                ?></td>
                                            <td class="hidden-480 sorting_disabled" style='text-align:right;'>
                                                <a href="<?= make_admin_url('view', 'list', 'list&id=' . $stu->id) ?>" class="btn purple mini tooltips" title='click here to view student profile' target='_blank'>Student Profile</a>
                                                <? if (in_array($stu->id, $session_st)): ?>
                                                    <a href="<?= make_admin_url('session', 'section', 'section&ct_session=' . $id . '&ct_section=' . $stu->section . '&id=' . $stu->rel_id) ?>" class="btn blue mini tooltips" title='click here to edit student detail in this session'><i class="icon-edit icon-white"></i></a>													
                                                <? endif; ?>
                                                <a href="<?= make_admin_url('session', 'remove_to_session', 'remove_to_session&id=' . $id . '&st_id=' . $stu->id) ?>" class="btn red mini tooltips" title='click here to remove this student from session' onclick="return confirm('Are you sure? You are deleting this record form this session.');"><i class="icon-remove icon-white"></i></a>

                                            </td>
                                        </tr>
                                    <? endforeach; ?>									
                                </tbody>

                            <? endif; ?>
                        </table>	
                    </div>
                </div>
            </form>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



