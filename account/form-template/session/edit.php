
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Sessions
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-certificate"></i>
                    <a href="<?php echo make_admin_url('session', 'list', 'list'); ?>">List Sessions</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Edit Session
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->


    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('session', 'update', 'update&id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-certificate"></i>Edit Session</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form" id='s_d'>      
                        <!-- Basic Info  -->
                        <h4 class="form-section"></h4>
                        <h4 class="form-section hedding_inner">Session Basics</h4>
                        <div class="row-fluid">  
                            <div class="control-group span6">
                                <label class="control-label" for="course_id">Select Course<span class="required">*</span></label>
                                <div class="controls">
                                    <select name="course_id" id="course_id"  class="span10 m-wrap ">
                                        <?php $sr = 1;
                                        while ($course = $QueryCourse->GetObjectFromRecord()):
                                            ?>
                                            <option value='<?= $course->id ?>' <?= ($object->course_id == $course->id) ? 'selected' : ''; ?>><?= ucfirst($course->course_name) ?></option>
                                            <? $sr++;
                                        endwhile;
                                        ?>															
                                    </select>                      
                                </div>
                            </div>
                        </div>	
                        <div class="row-fluid">  
                            <!--
                            <div class="control-group span6">
<label class="control-label" for="title">Session<span class="required">*</span></label>
<div class="controls">
                                                    <input type="text" name="title" id='title' value="<?= $object->title ?>" class="span10 m-wrap validate[required]" />                    
                                            </div>
</div>
                            -->
                        </div>										
                        <div class="row-fluid">
                            <div class="span6 ">											
                                <div class="control-group">
                                    <label class="control-label" for="start_date">Session Starts From<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" disabled value="<?= date('d-M-Y', strtotime($object->start_date)) ?>" id="start_date" class="span10 m-wrap new_format_text validate[required]" />
                                        <input type="hidden" name="start_date" value="<?= date('d-M-Y', strtotime($object->start_date)) ?>">
                                    </div>													
                                </div> 	  
                                <div class="control-group">
                                    <div class="controls" style='margin-left:39px;'>
                                        <span class="custom_help">Total Possible Sections in Session<span class="required">*</span><input type="text" name="total_sections_allowed" value="<?= $object->total_sections_allowed ?>" id="total_sections_allowed" style='text-align:center;' class="custom_input span2 m-wrap validate[required,custom[onlyNumberSp],max[<?= $school_setting->max_sections_per_session ?>]]"/>
                                        </span>
                                    </div>									
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="meta_keyword">Activate this session</label>
                                    <div class="controls"style='padding-top:6px;'>
                                        <input type="checkbox" name="is_active" id="meta_keyword" value="1" <?= ($object->is_active == '1') ? 'checked' : ''; ?>>
                                    </div>
                                </div> 											
                            </div>

                            <div class="span6 ">

                                <div class="control-group">
                                    <label class="control-label" for="end_date">Session Ends On<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" disabled value="<?= date('d-M-Y', strtotime($object->end_date)) ?>" id="end_date" class="span10 m-wrap new_format_text validate[required]" />
                                        <input type="hidden" name="end_date" value="<?= date('d-M-Y', strtotime($object->end_date)) ?>">
                                    </div>                                                    

                                </div> 	
                                <div class="control-group">  	                                
                                    <div class="controls" style='margin-left:60px;'>
                                        <span class="custom_help">Maximum Students That A Section Can Take <span class="required">*</span> <input type="text" name="max_sections_students" value="<?= $object->max_sections_students ?>" style='text-align:center;' id="max_sections_students" class="custom_input span2 m-wrap validate[required,custom[onlyNumberSp],max[<?= $school_setting->max_student_per_session ?>]]"/>
                                        </span>
                                    </div>														
                                </div>	


                            </div>	

                        </div>




                        <h4 class="form-section hedding_inner">Possible Subjects In This Session</h4>	
                        <?
                        $all_c_sub = array();
                        $all_el_sub = array();

                        $all_c_sub = explode(',', $object->compulsory_subjects);
                        $all_el_sub = explode(',', $object->elective_subjects);
                        ?>
                        <div class="row-fluid">
                            <div class="row-fluid">					
                                <div class="span4" style='margin-left: 35px; margin-right: 16px;'><strong>Compulsory Subjects List</strong></div>
                                <div class="span6" style='margin-left: 0px;'><strong>Selected Compulsory Subjects</strong></div>
                            </div>	
                            <div class="control-group" style='margin-bottom:0px;'>											
                                <div class="controls" style='margin-left:35px;'>
                                    <select multiple="multiple " class="multiple_selection" for="compulsory_subjects" name='compulsory_subjects[]'>
                                        <?php $sr = 1;
                                        while ($subject = $QuerySubject->GetObjectFromRecord()):
                                            ?>
                                            <option value='<?= $subject->id ?>' <?
                                                if (in_array($subject->id, $all_c_sub)) {
                                                    echo 'selected';
                                                }
                                                ?>><?= $subject->name ?></option>
    <? $sr++;
endwhile;
?>
                                    </select>
                                </div>
                            </div>
                            <div class="row-fluid">					
                                <div class="span4" style='margin-left: 35px; margin-right: 16px;'>Click on any subject to add to the selected list.</div>
                                <div class="span6" style='margin-left: 0px;'>Click on any subject to remove from the selected list. </div>
                            </div>	
                            <br/>													
                        </div>	


                        <!-- Elective Subjects-->
                        <div class="row-fluid">
                            <div class="row-fluid">					
                                <div class="span4" style='margin-left: 35px; margin-right: 16px;'><strong>Elective Subjects List</strong></div>
                                <div class="span6" style='margin-left: 0px;'><strong>Selected Elective Subjects</strong></div>
                            </div>											
                            <div class="control-group" style='margin-bottom:0px;'>
                                <div class="controls" style='margin-left:35px;'>
                                    <select multiple="multiple" class='multiple_selection' for="compulsory_subjects" name='elective_subjects[]'>
<?php $sr = 1;
while ($subject1 = $QuerySubject1->GetObjectFromRecord()):
    ?>
                                            <option value='<?= $subject1->id ?>'  <?
    if (in_array($subject1->id, $all_el_sub)) {
        echo 'selected';
    }
    ?>><?= $subject1->name ?></option>
    <? $sr++;
endwhile;
?>	
                                    </select>
                                </div>
                            </div>
                            <div class="row-fluid">					
                                <div class="span4" style='margin-left: 35px; margin-right: 16px;'>Click on any subject to add to the selected list.</div>
                                <div class="span6" style='margin-left: 0px;'>Click on any subject to remove from the selected list. </div>
                            </div>	
                            <div class="clearfix" id='se_d'></div>	
                            <br/>

                        </div>											




                        <h4 class="form-section hedding_inner">Session Fee Details</h4>
                        <div class="row-fluid">	
                            <div class="span6">	
                                <div class="control-group">
                                    <label class="control-label" for="interval_of_fee">Payment Fee Interval<span class="required">*</span></label>
                                    <div class="controls">
                                        <select name="frequency" id="frequency" class="span10 m-wrap ">
                                            <option value='1' <?php echo $object->frequency == '1' ? "selected" : "" ?>>Monthly</option>
                                            <option value='3' <?php echo $object->frequency == '3' ? "selected" : "" ?>>Quarterly</option>
                                            <option value='6' <?php echo $object->frequency == '6' ? "selected" : "" ?>>Half Yearly</option>
                                            <option value='12' <?php echo $object->frequency == '12' ? "selected" : "" ?>>Yearly</option>
                                        </select>
                                    </div>   
                                </div>
                            </div>
                            <div class="span6"></div>
                        </div>										
                        <div class="row-fluid">										
                            <div class="span6">

                                <div class="control-group">
                                    <div class="controls" style='margin-left:29px;'>
                                        <span class="help-block custom_help">Student will pay <input style='text-align:center;' type="text" name="late_fee_rate" value="<?= $object->late_fee_rate ?>" id="late_fee_rate" class="custom_input span2 m-wrap validate[required]"/>
                                            amount after the late fee days.</span>
                                    </div>								
                                </div> 	
                                <div class="control-group">
                                    <div class="controls" style='margin-left:29px;'>														  														  
                                        <span class="help-block custom_help">Student will pay <input style='text-align:center;' type="text" name="absent_fine" value="<?= $object->absent_fine ?>" id="absent_fine" class="custom_input span2 m-wrap validate[required]"/>
                                            fine per absent.</span>
                                    </div>
                                </div> 	
                            </div>
                            <div class="span6">												
                                <div class="control-group">
                                    <div class="controls" style='margin-left:85px;'>
                                        <span class="help-block custom_help">Student will pay fine after <input style='text-align:center;' type="text" name="late_fee_days" value="<?= $object->late_fee_days ?>" id="max_girl" class="custom_input span2 m-wrap validate[required,custom[onlyNumberSp],max[<?= $school_setting->max_student_per_session ?>]]"/>
                                            days per fee interval.</span>
                                    </div>
                                </div> 	
                            </div>											
                        </div>

                        <div class="row-fluid">										
                            <div class="span7 ">
                                <div class="control-group">
                                    <label class="control-label" for="phone">Select Fee Heads</label>
                                    <div class="controls">
                                                         <?php
                                                         if ($QueryFee->GetNumRows() != '0'):
                                                             $sr_fee = 1;
                                                             while ($fee = $QueryFee->GetObjectFromRecord()):
                                                                 ?>
                                                <div class="span12" <?
                                                if ($sr_fee != 1) {
                                                    echo 'style="margin-left:0px;"';
                                                }
                                                ?>>												
                                                    <span><input type="checkbox" class='fee_sub_head' value="<?= $fee->id ?>" show_name='<?= ucfirst($fee->name) ?> (<?= ucfirst($fee->type) ?>)' <?
                                        if (in_array($fee->id, $all_heads)) {
                                            echo 'checked';
                                        }
                                        ?> add_type="<?
                                        if ($fee->type == 'Regular'): echo '1';
                                        else: echo '0';
                                        endif;
                                        ?>" ></span><?= ucfirst($fee->name) ?> &nbsp;(<?= ucfirst($fee->type) ?>)
                                                </div>
        <?
        $sr_fee++;
    endwhile;
else:
    echo '&nbsp;&nbsp;&nbsp;&nbsp;Sorry, No Record Found..!';
endif;
?>
                                        <span class='help-block'><strong>Note: </strong><a href='<?= make_admin_url('fee_head', 'list', 'list') ?>' target='_blank'>Click here</a> to add/edit fee heads.</span>														
                                    </div> 	
                                    <input type='hidden' value='0' id='total_head'/>	
                                </div>											
                            </div>
                            <div class="span5">
                                <div class="control-group">
                                    <div class="controls" style='margin-left:0px;'>
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr><th>Fee Heads Name</th><th>Amount (<?= CURRENCY_SYMBOL ?>)</th>
                                            <tbody id='add_delete_heads'></tbody>
                                                <?
                                                $t_am = '0';
                                                if (!empty($pay_heads)): $st = '0';
                                                    foreach ($pay_heads as $p_k => $p_v):
                                                        ?>
                                                    <tr class='delete_row<?= $p_v['fee_head'] ?>'><td><?= $p_v['title'] ?> &nbsp;(<?= ucfirst($p_v['type']) ?>)</td><td>
                                                            <input type="hidden" name="head[<?= $p_v['fee_head'] ?>][title]" value="<?= $p_v['fee_head'] ?>"/>
                                                            <input class="span10 new_row change_value" type="text" placeholder="0.00" name="head[<?= $p_v['fee_head'] ?>][fee]" value="<?= $p_v['amount'] ?>" add="<?
                                                if ($p_v['type'] == 'Regular'): echo '1';
                                                else: echo '0';
                                                endif;
                                                ?>"/></td></tr>
        <?
        if (ucfirst($p_v['type']) == 'Regular'):
            $ct_amount = $p_v['amount'] * $object->frequency;
        else:
            $ct_amount = $p_v['amount'];
        endif;
        $t_am = $t_am + $ct_amount;
    endforeach;
else:
    echo "<tr><td colspan='2'>Sorry, No Fee Head Added..!</td>";
endif;
?>													
                                            </thead>
                                        </table>
                                        <span class='help-block'>
                                            <strong>Note:</strong> You can add as many fees heads to the session as you desire. However, you can alter the fee for each student later.  
                                            All the selected fee heads will be available for each student of this session, to select from.
                                        </span>
                                    </div>  
                                </div> 											

                            </div>
                        </div>	
                        <div class="row-fluid">	
                            <div class="control-group">
                                <label class="control-label" for="position">Position</label>
                                <div class="controls">
                                    <input type="text" name="position" value="<?= $object->position ?>" id="position" class="span1 m-wrap" style='text-align:center;' />
                                </div>													
                            </div> 										
                        </div>	
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" />
                            <input type="hidden" name="id" value="<?= $object->id ?>" tabindex="7" /> 													 
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('session', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="clearfix"></div>

    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    


<script type="text/javascript">
    jQuery(document).ready(function () {
        $(".fee_sub_head").live("click", function () {
            //add_delete_heads
            var value = $(this).val();
            var show_name = $(this).attr('show_name');
            var add_type = $(this).attr('add_type');

            if (!$(this).is(':checked')) {
                var current_hd = $('#total_head').val();
                var new_hd = value;
                $('.delete_row' + value + '').remove();
                $('#total_head').val(new_hd);
                GetValue();
            }
            else
            {
                var current_hd = $('#total_head').val();
                var new_hd = value;
                $('#add_delete_heads').append("<tr class='delete_row" + new_hd + "'><td><input type='hidden' name='head[" + new_hd + "][title]' class='span2' value='" + value + "'/>" + show_name + "</td><td><input type='text' name='head[" + new_hd + "][fee]' value='' add='" + add_type + "' class='span10 new_row change_value' placeholder='0.00'/></td></tr>");
                $('#total_head').val(new_hd);
            }
        });
        $(".change_value").live("change", function () {
            GetValue();
        });
    });

    function GetValue() {
        var Contain = "0";
        var frequency = 0;
        frequency = $("#frequency").val();
        if (frequency == "") {
            frequency = 1;

        }

        $(".new_row").each(function () {

            if ($(this).val() != '') {
                if ($(this).attr('add') == "1") {
                    head_val = $(this).val().replace(",", "");
                    Contain1 = parseInt(frequency) * parseInt(head_val);
                }
                else {
                    head_val = $(this).val().replace(",", "");
                    Contain1 = parseInt(head_val);
                }
            }
            Contain = parseInt(Contain) + parseInt(Contain1);
        });
        $('#make_total').html(Contain);
    }
</script>		