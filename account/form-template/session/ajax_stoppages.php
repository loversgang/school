<?php
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'get_stoppages') {
        $obj = new vehicle_stoppages;
        $stoppages = $obj->listStoppages($route_id);
        $chk = vehicleStudent::chkStudentAssigned($student_id);
        if ($chk) {
            $student_vehicle = vehicleStudent::getStudentRoot($student_id);
            ?>
            <div class="control-group">		
                <label class="control-label" for="vehicle_stoppage">Select Stoppage<span class="required"></span></label>
                <div class="controls">
                    <select id="vehicle_stoppage" class="span10 m-wrap">
                        <?php foreach ($stoppages as $stoppage) { ?>
                            <option value="<?= $stoppage->amount; ?>" data-attr="<?= $stoppage->id ?>" <?= $student_vehicle->stoppage_id == $stoppage->id ? 'selected' : '' ?>><?= $stoppage->stoppage; ?></option>
                        <?php } ?>
                    </select>
                </div>	
            </div>
            <?php
        } else {
            ?>
            <div class="control-group">		
                <label class="control-label" for="vehicle_stoppage">Select Stoppage<span class="required"></span></label>
                <div class="controls">
                    <select id="vehicle_stoppage" class="span10 m-wrap">
                        <?php foreach ($stoppages as $stoppage) { ?>
                            <option value="<?= $stoppage->amount; ?>" data-attr="<?= $stoppage->id ?>"><?= $stoppage->stoppage; ?></option>
                        <?php } ?>
                    </select>
                </div>	
            </div>
            <?php
        }
    }
}
?>
