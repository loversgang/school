

<div class="tile bg-red <?php echo ($section == 'stuck_off') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('session', 'stuck_off', 'stuck_off'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                Stuck Off List
            </div>
        </div>
    </a>   
</div>
<div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('session', 'list', 'list'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                List Sessions
            </div>
        </div>
    </a>   
</div>

<div class="tile bg-blue <?php echo ($section == 'insert') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('session', 'insert', 'insert'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-plus"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                New Session
            </div>
        </div>
    </a> 
</div>

