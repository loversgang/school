
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <div class="hidden-print" >
			<!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Sessions
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 
		                            <li>
                                            <i class="icon-list"></i>
                                            <a href="<?php echo make_admin_url('session', 'list', 'list');?>"> List Sessions</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li class="last">
									 <i class="icon-certificate"></i>
									 <a href="<?php echo make_admin_url('session', 'view', 'view&id='.$ct_session);?>"><?=$student->session_name?></a> 
									 <i class="icon-angle-right"></i>
								   </li>							
                                
                                    <li class="last">
                                        Students
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<div class="tile bg-purple <?php echo ($section=='list')?'selected':''?>" id='print_document'>							
						<a class="hidden-print" href="<?=make_admin_url('session','view','view&id='.$ct_session)?>">
							<div class="corner"></div>
							<div class="tile-body"><i class="icon-arrow-left"></i></div>
							<div class="tile-object"><div class="name">Back To Session</div></div>
						</a>
					</div>					
					<div class="tile bg-green" id='print_document'>					
						<a class="hidden-print" href="<?=make_admin_url('session','confirm_print','confirm_print&id='.$id.'&ct_session='.$ct_session.'&ct_section='.$ct_section.'&doc_id='.$doc_id)?>">
							<div class="corner"></div>
							<div class="tile-body"><i class="icon-briefcase"></i></div>
							<div class="tile-object"><div class="name">Confirm Printing</div></div>
						</a>
					</div>
					<div class="tile bg-yellow <?php echo ($section=='print')?'selected':''?>" id='print_document'>							
						<a class="hidden-print" onclick="javascript:window.print();">
							<div class="corner"></div>
							<div class="tile-body"><i class="icon-print"></i></div>
							<div class="tile-object"><div class="name">Print</div></div>
						</a>
					</div>						
						
				</div>            
           <div class="clearfix"></div>

            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
         </div>   
           <br/>	

           <div class="clearfix"></div>
			 
			<div class="row-fluid">
					
				<?=html_entity_decode($content)?>	
			</div>
    <!-- END PAGE CONTAINER-->    
	</div>