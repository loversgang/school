<?
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffDesignationClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffCategoryClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/vehicleClass.php');
// List Vehicle Routes
$obj = new vehicle_route;
$routes = $obj->listVehicleRoute($school->id);
if ($_POST):
    isset($_POST['id']) ? $id = $_POST['id'] : $id = '';
    isset($_POST['session_id']) ? $session_id = $_POST['session_id'] : $session_id = '';
    isset($_POST['student_id']) ? $student_id = $_POST['student_id'] : $student_id = '';
    isset($_POST['ct_sec']) ? $ct_sec = $_POST['ct_sec'] : $ct_sec = '';
    isset($_POST['start']) ? $start = $_POST['start'] : $start = '0';
    isset($_POST['amount']) ? $amount = $_POST['amount'] : $amount = '';
    isset($_POST['route_id']) ? $route_id = $_POST['route_id'] : $route_id = '';
    isset($_POST['stoppage_id']) ? $stoppage_id = $_POST['stoppage_id'] : $stoppage_id = '';
    isset($_POST['round']) ? $round = $_POST['round'] : $round = '';

    #get Vehicle
    $current_vehicle = get_object('school_vehicle', $id);

    #get All Active Vehicle List
    $QueryVeh = new vehicle();
    $QueryVeh->listAll($school->id, '1');
    ob_start();
    $status = 'error';
    if ($start == '1'):
        #now check the vehicle already assign another or not.
        $stuVeh = new vehicleStudent();
        $assign = $stuVeh->checkOtherVehicleAssign($session_id, $ct_sec, $student_id, $id);

        if (is_object($assign)):
            $admin_user->set_error();
            $admin_user->set_pass_msg(MSG_VEHICLE_SINGLE_STUDENT_ALREDY_ADDED_OLD);
        else:
            #now Assign vehicle
            $stuVeh = new vehicleStudent();
            $stuVeh->start_vehicle($session_id, $ct_sec, $student_id, $id, $route_id, $stoppage_id, $round, $amount);

            //$admin_user->set_pass_msg(MSG_VEHICLE_SINGLE_STUDENT_ADDED);
            $status = 'assigned';
        endif;
        #get Vehicle student info
        $QueryChkStu = new vehicleStudent();
        $VehstudentAdd = $QueryChkStu->checkVehicleStudentAssign($session_id, $ct_sec, $student_id);
        ?>
        <div class="control-group">
            <label class="control-label" for="ref_name1">Vehicle Make</label>
            <div class="controls">
                <input type="text" value="<?
                if (is_object($current_vehicle)) {
                    echo $current_vehicle->make;
                }
                ?>" id="ref_name1" class="span10 m-wrap" disabled="disabled"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="ref_phone1">Vehicle Number</label>
            <div class="controls">
                <input type="text" value="<?
                if (is_object($current_vehicle)) {
                    echo $current_vehicle->vehicle_number;
                }
                ?>" id="ref_phone1" class="span10" disabled="disabled"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <? if (is_object($current_vehicle)): ?>
                    <? if (is_object($VehstudentAdd) && strtotime($VehstudentAdd->to_date) >= strtotime(date('Y-m-d'))): ?>	
                        <span class="btn red button-next check_valid" type="submit" name="stop_vehicle"  tabindex="7" id='vehicle_service_end'> Stop Vehicle Service </span> &nbsp;&nbsp;
                    <? else: ?>
                        <span class="btn blue button-next check_valid" type="submit" name="submit"  tabindex="7" id='vehicle_service_start'> Start Vehicle Service </span> &nbsp;&nbsp;
                    <? endif; ?>
                <? endif; ?>
            </div>
        </div>
        <?
    else:
        #get Vehicle student info
        $QueryChkStu = new vehicleStudent();
        $VehstudentAdd = $QueryChkStu->checkVehicleStudentAssign($session_id, $ct_sec, $student_id);

        $QueryStStu = new vehicleStudent();
        $QueryStStu->stop_vehicle($VehstudentAdd->assign_id);

        //$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
        $status = 'unassigned';
        #get Vehicle student info
        $QueryChkStu = new vehicleStudent();
        $VehstudentAdd = $QueryChkStu->checkVehicleStudentAssign($session_id, $ct_sec, $student_id);
        ?>
        <div class="control-group">
            <label class="control-label" for="ref_name1">Vehicle Make</label>
            <div class="controls">
                <input type="text" value="<?
                if (is_object($current_vehicle)) {
                    echo $current_vehicle->make;
                }
                ?>" id="ref_name1" class="span10 m-wrap" disabled="disabled"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="ref_phone1">Vehicle Number</label>
            <div class="controls">
                <input type="text" value="<?
                if (is_object($current_vehicle)) {
                    echo $current_vehicle->vehicle_number;
                }
                ?>" id="ref_phone1" class="span10" disabled="disabled"/>
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <? if (is_object($current_vehicle)): ?>
                    <? if (is_object($VehstudentAdd) && strtotime($VehstudentAdd->to_date) >= strtotime(date('Y-m-d'))): ?>	
                        <span class="btn red button-next check_valid" type="submit" name="stop_vehicle"  tabindex="7" id='vehicle_service_end'> Stop Vehicle Service </span> &nbsp;&nbsp;
                    <? else: ?>
                        <span class="btn blue button-next check_valid" type="submit" name="submit"  tabindex="7" id='vehicle_service_start'> Start Vehicle Service </span> &nbsp;&nbsp;
                    <? endif; ?>
                <? endif; ?>
            </div>
        </div>
    <?
    endif;
    $html = ob_get_clean();
//    $html = htmlentities($html);
    $response = array(
        'status' => $status,
        'html' => $html
    );
    echo json_encode($response);
endif;
?>