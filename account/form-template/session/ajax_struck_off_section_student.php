<?
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');

if ($_POST):
    isset($_POST['session_id']) ? $session_id = $_POST['session_id'] : $session_id = '';
    isset($_POST['ct_sec']) ? $ct_sec = $_POST['ct_sec'] : $ct_sec = '';
    isset($_POST['sess_int']) ? $sess_int = $_POST['sess_int'] : $sess_int = '';

    #Get Struck Off Students
    $obj = new stuck_off;
    $record = $obj->listStuckOffStudents($session_id, $ct_sec, $sess_int);
    ?>			
    <table class="table table-striped table-bordered table-hover" id="ajex_student">
        <thead>
            <tr>
                <th>S.No.</th>
                <th>Student Name</th>
                <th>Session</th>
                <th>Stuck Off Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $k = 1;
            foreach ($record as $list) {
                ?>
                <tr>
                    <td><?php echo $k++; ?></td>
                    <td><?php echo $list->first_name . ' ' . $list->last_name; ?></td>
                    <td><?php echo $list->title; ?></td>
                    <td><?php echo $list->stuck_off_date; ?></td>
                    <td><button class="btn blue mini" id="revert_stucked_student" data-stuck_id="<?php echo $list->id; ?>" data-student_id="<?php echo $list->student_id; ?>"><i class="icon-undo"></i> Revert</button></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>			
    <?
endif;
?>
