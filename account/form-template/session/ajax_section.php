<?
include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/courseClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/studentSessionClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/sessionClass.php');

if ($_POST):
    isset($_POST['id']) ? $id = $_POST['id'] : $id = '';
    if ($id == 'null'):
        ?>
        <label class="control-label" style='float: none; text-align: left;'>Select Section</label>
        <input type='text' class="span10" value='No Section.' disabled />
        <?php
    else:
        /* Get session Pages */
        $QuerySec = new studentSession();
        $QuerySec->listAllSessionSection($id);
        ?>
        <label class="control-label" style='float: none; text-align: left;'>Select Section</label>
        <? if ($QuerySec->GetNumRows() >= 0): ?>	
            <select class="select2_category span10" data-placeholder="Select Session Students" name='ct_sec' id='ct_sec'>
                <option value="0">All</option>
                <?php
                $session_sc = 1;
                while ($sec = $QuerySec->GetObjectFromRecord()):
                    ?>
                    <option value='<?= $sec->section ?>'><?= ucfirst($sec->section) ?></option>
                    <?
                    $session_sc++;
                endwhile;
                ?>
            </select>
        <? else: ?>
            <input type='text' class="span10" value='No Section.' disabled />
        <? endif; ?>						
    <?
    endif;
endif;
?>