  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Sessions
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
                                    <li>
                                        <i class="icon-certificate"></i>
                                               <a href="<?php echo make_admin_url('session', 'list', 'list');?>">List Session</a>
                                         <i class="icon-angle-right"></i>                                       
                                    </li>
									<li>
										<i class="icon-book"></i>
                                         <a href="<?php echo make_admin_url('session', 'update', 'update&id='.$id);?>"><?=$object->title?></a>
										<i class="icon-angle-right"></i>  
									</li>									
                                    <li class="last">
                                       Section In-charge Name
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
						<div class="tile bg-purple selected">
							<a href="<?=make_admin_url('session','update','update&id='.$id);?>">
								<div class="corner"></div>
								<div class="tile-body">
										<i class="icon-arrow-left"></i>
								</div>
								<div class="tile-object">
										<div class="name">
												Back To Session
										</div>
								</div>
							</a>   
						</div>
				  
				
						<div class="tile bg-green">
							<a href="<?=make_admin_url('session','list','list');?>">
								<div class="corner"></div>
								<div class="tile-body">
										<i class="icon-list"></i>
								</div>
								<div class="tile-object">
										<div class="name">
												List Session
										</div>
								</div>
							</a>   
						</div>
				</div>
				
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('session', 'session_sec', 'session_sec&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-certificate"></i><?=$object->title?> >> Sections In-charge Teacher</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">
										<br/>
										<? if($object->total_sections_allowed>0): $start='1';?>
								
										
											<?if(!empty($record)):
												foreach($record as $r_k=>$r_v):?>
														<? if(!empty($r_v->staff_id)):
															$curr_staff=get_object('staff',$r_v->staff_id);																
														endif;?>			
														<h4 class="form-section hedding_inner">Section <?=$r_v->section?></h4>
														<div class="row-fluid">
														<div class="span6 ">
															<div class="control-group">
																<label class="control-label" for="teacher_cat">Select Staff Category<span class="required">*</span></label>
																<div class="controls">																
																	<select class="teacher_cat" class="span10 m-wrap" startrel="<?=$start?>">
																	<option value=''>Select Category</option>
																	<?php $sr=1;
																		foreach($all_Staff_cat as $cat_key=>$cat):?>
																				<option value='<?=$cat->id?>' <? if(is_object($curr_staff) && $curr_staff->staff_category==$cat->id){ echo 'selected';}?>><?=$cat->name?></option>
																	<? $sr++; endforeach;?>																													
																	</select>                      
																</div>
															</div>	
														</div>	
														<div class="span6 ">	
															<div class="control-group" id='div<?=$start?>'>
															<label class="control-label" for="incharge<?=$start?>">Teacher Name <span class="required">*</span></label>
																	<div class="controls">
																		<input type="text" value='<? if(is_object($curr_staff)){ echo $curr_staff->title.' '.$curr_staff->first_name.' '.$curr_staff->last_name;} ?>' class="m-wrap span8 validate[required]" readonly> 
																		<input type="hidden" name='incharge[<?=$start?>][id]' value='<?=$r_v->staff_id?>'>	
																	</div>
															</div> 		
														</div>
														</div>
														<input type='hidden' name="incharge[<?=$start?>][section]" value="<?=$Section_Name_List[$start]?>"/>
	      
													
											<?
												$start++;
												endforeach;?>
											
												<? 	if(count($record)<$object->total_sections_allowed):
														while($start<=$object->total_sections_allowed):?>
														
														<h4 class="form-section hedding_inner">Section <?=$Section_Name_List[$start]?></h4>
														<div class="row-fluid">
														<div class="span6 ">
															<div class="control-group">
																<label class="control-label" for="teacher_cat">Select Staff Category<span class="required">*</span></label>
																<div class="controls">																
																	<select class="teacher_cat" class="span10 m-wrap" startrel="<?=$start?>">
																	<option value=''>Select Category</option>
																	<?php $sr=1;
																		foreach($all_Staff_cat as $cat_key=>$cat):?>
																				<option value='<?=$cat->id?>'><?=$cat->name?></option>
																	<? $sr++; endforeach;?>																													
																	</select>                      
																</div>
															</div>	
														</div>	
														<div class="span6 ">	
															<div class="control-group" id='div<?=$start?>'>
															<label class="control-label" for="incharge<?=$start?>">Teacher Name <span class="required">*</span></label>
																	<div class="controls">
																		<input type="text" name="incharge[<?=$start?>][id]" id="incharge<?=$start?>" class="m-wrap span8 validate[required]" readonly>
																	</div>
															</div> 		
														</div>
														</div>
														<input type='hidden' name="incharge[<?=$start?>][section]" value="<?=$Section_Name_List[$start]?>"/>
	             
														<? $start++;
														endwhile;
													endif;
											else:											
												while($start<=$object->total_sections_allowed):?>
														<h4 class="form-section hedding_inner">Section <?=$Section_Name_List[$start]?></h4>
														<div class="row-fluid">
														<div class="span6 ">
															<div class="control-group">
																<label class="control-label" for="teacher_cat">Select Staff Category<span class="required">*</span></label>
																<div class="controls">																
																	<select class="teacher_cat" class="span10 m-wrap" startrel="<?=$start?>">
																	<option value=''>Select Category</option>
																	<?php $sr=1;
																		foreach($all_Staff_cat as $cat_key=>$cat):?>
																				<option value='<?=$cat->id?>'><?=$cat->name?></option>
																	<? $sr++; endforeach;?>																													
																	</select>                      
																</div>
															</div>	
														</div>	
														<div class="span6 ">	
															<div class="control-group" id='div<?=$start?>'>
															<label class="control-label" for="incharge<?=$start?>">Teacher Name <span class="required">*</span></label>
																	<div class="controls">
																		<input type="text" name="incharge[<?=$start?>][id]" id="incharge<?=$start?>" class="m-wrap span8 validate[required]" readonly>
																	</div>
															</div> 		
														</div>
														</div>
														<input type='hidden' name="incharge[<?=$start?>][section]" value="<?=$Section_Name_List[$start]?>"/>
	      
													<? $start++;
													endwhile;
											endif;?>
                                   
                                            <div class="form-actions">
													 <input type="hidden" name="session_id" value="<?=$id?>" tabindex="7" /> 	
													 <input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" /> 	
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('session', 'list', 'list');?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
										<? endif;?>
                              </div> 

                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    
<script type="text/javascript">
        jQuery(document).ready(function() {
		
		$(".teacher_cat").change(function(){	   
				var cat_id=$(this).val();
				var startdiv = $(this).attr("startrel");

						   var id=cat_id;
						   $("#div"+startdiv).html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');
							
							var dataString = 'id='+id+'&startdiv='+startdiv;
							
							$.ajax({ 
							type: "POST",						
							url: "<?=make_admin_url_window('ajax_calling','ajax_section','ajax_teacher&temp=session');?>",
							data: dataString,
							success: function(data, textStatus) {						
							$("#div"+startdiv).html(data);
							}				
							});
				
		});	

		
			});
 </script>	
