  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                   Edit Sessions Students
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('session', 'list', 'list');?>">List Session</a>
                                         <i class="icon-angle-right"></i>                                       
                                    </li>
                                    <li class="last">
                                        Edit Sessions Student Fee
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<div class="tile bg-red <?php echo ($section=='session_fee')?'selected':''?>">
							<a href="<?php echo make_admin_url('session', 'view', 'view&id='.$ct_session);?>">
								<div class="corner"></div>
								<div class="tile-body"><i class="icon-arrow-left"></i></div>
								<div class="tile-object"><div class="name">Session Students</div></div>
							</a> 
							</div>	
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
		  
			  
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('session', 'session_fee', 'session_fee&ct_session='.$ct_session.'&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Edit Sessions Student Fee</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">      
									<!-- Basic Info  -->
										<h4 class="form-section hedding_inner">Session & Student Detail</h4>
                                        <div class="row-fluid">    
											<div class="control-group">
                                                    <label class="control-label" for="year">Session Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                          <input type="text" value="<?=$session->title?>" disabled='' />    
													</div>
                                            </div>
										</div>		
										<div class="row-fluid">
										<div class="span6 ">										
											<div class="control-group">
  	                                        <label class="control-label" for="start_date">Start Date<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" value="<?=$session->start_date?>" id="start_date" disabled='' />
													</div>
                                            </div> 	  
											<div class="control-group">
  	                                        <label class="control-label" for="end_date">Student Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" value="<?=$student->first_name." ".$student->last_name?>" id="start_date"  disabled=''/>
													</div>
                                            </div> 
											<div class="control-group">
  	                                        <label class="control-label" for="roll_no">Roll No<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" value="<?=$session_student->roll_no?>" id="roll_no"  disabled=''/>
                                                    </div>
                                            </div> 										
										</div>
										<div class="span6">				
											<div class="control-group">
  	                                        <label class="control-label" for="end_date">End Date<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" value="<?=$session->end_date?>" id="start_date"  disabled=''/>
													</div>
                                            </div> 	
											<div class="control-group">
  	                                        <label class="control-label" for="">Registration ID<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" value="<?=$student->reg_id?>" id=""  disabled=''/>
													</div>
                                            </div> 
											<div class="control-group">
  	                                        <label class="control-label" for="section">Section<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input type="text" value="<?=$session_student->section?>" id="section" disabled=''/>
                                                    </div>
                                            </div> 
										</div>	
										</div>
										<h4 class="form-section hedding_inner">Fee Detail</h4>										
										<div class="row-fluid">
												<div class="control-group span8">
													<div class="controls" >
													<table class="table table-striped table-bordered table-advance table-hover">
													<thead>
													<tr><th>Fee Head Name</th><th>Amount (<?=CURRENCY_SYMBOL?>)</th>
													<tbody id='add_delete_heads'></tbody>
													<? $t_am='0'; $new_added=0;
														if(!empty($pay_heads)): $st='0'; 
															foreach($pay_heads as $p_k=>$p_v): 
															 if(array_key_exists($p_v['fee_head'],$Sesspay_heads)):
															 	$a=$p_v['fee_head'];
																unset($Sesspay_heads[$a]);		
															 endif;	?>
																<tr class='delete_row<?=$p_v['fee_head']?>'><td><?=$p_v['title']?> &nbsp;(<?=ucfirst($p_v['type'])?>)</td><td>
																<input type="hidden" name="head[<?=$p_v['fee_head']?>][title]" value="<?=$p_v['fee_head']?>"/>
																<input class="span10 new_row change_value" type="text" placeholder="0.00" name="head[<?=$p_v['fee_head']?>][fee]" value="<?=$p_v['amount']?>" add="<? if($p_v['type']=='Regular'): echo '1'; else: echo '0'; endif; ?>"/></td></tr>
													<? 		
																if(ucfirst($p_v['type'])=='Regular'): 																
																	$ct_amount=$p_v['amount']*$session->frequency;																
																else:
																	$ct_amount=$p_v['amount'];
																endif;
																$t_am=$t_am+$ct_amount;
															endforeach;
															# Now display if new heads of array
																	if(!empty($Sesspay_heads)): $new_added='1';
																		foreach($Sesspay_heads as $new_key=>$new_value):?>
																		<tr class='delete_row<?=$new_value['fee_head']?> removed'><td><?=$new_value['title']?> &nbsp;(<?=ucfirst($new_value['type'])?>)</td><td>
																		<input type="hidden" name="head[<?=$new_value['fee_head']?>][title]" value="<?=$new_value['fee_head']?>"/>
																		<input class="span10 new_row change_value" type="text" placeholder="0.00" name="head[<?=$new_value['fee_head']?>][fee]" value="<?=$new_value['amount']?>" add="<? if($new_value['type']=='Regular'): echo '1'; else: echo '0'; endif; ?>"/></td></tr>
																			<? 		
																			if(ucfirst($new_value['type'])=='Regular'): 																
																				$ct_amount=$new_value['amount']*$session->frequency;																
																			else:
																				$ct_amount=$new_value['amount'];
																			endif;
																			$t_am=$t_am+$ct_amount;																
																		endforeach;
																	endif;
													   else:
														echo "<tr><td colspan='2'>Sorry, No Fee Head Added..!</td>";
														endif;?>
													<footer><tr><th>Total Session Fee (<?=CURRENCY_SYMBOL?>)</th><th>
													<div id='make_total' class='span4'><?=$t_am?></div> </th></footer>
													</thead>
													</table>
													</div>  
												</div> 
												<? if($new_added):?>
													<div class="alert alert-block alert-danger fade in span4">New Fee Heads Added to session</div>
												<? endif;?>	
											<div class="clearfix"></div>				
												
										</div>	
                                        
										<div class="form-actions">
													 <input type='hidden' value="<?=$session->frequency?>" id='frequency'/>
													 <input type="hidden" name="student_id" value="<?=$id?>" tabindex="7" />
													 <input type="hidden" name="ct_section" value="<?=$ct_section?>" tabindex="7" />
													 <input type="hidden" name="id" value="<?=$id?>" tabindex="7" /> 
													 <input type="hidden" name="school_id" value="<?=$school->id?>" tabindex="7" /> 	
                                                     <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('session', 'view', 'view&id='.$ct_session);?>" class="btn" name="cancel" > Cancel</a>
                                        </div>
                              </div>
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    
	 <script type="text/javascript">
        jQuery(document).ready(function() {
		$(".change_value").live("change",function(){	
					GetValue();								
		});	
	});
	
		function GetValue(){
			var Contain = "0";
			var frequency=0;
			frequency=$("#frequency").val();
			if(frequency==""){
			frequency=1;
			
			}
		
			$(".new_row").each(function(){	

				if($(this).val()!=''){
					if($(this).attr('add')=="1"){
							Contain1 = parseInt(frequency) * parseInt($(this).val());							
					}
					else{						
							Contain1 = parseInt($(this).val());							
						} 	
				}					
					Contain=parseInt(Contain)+parseInt(Contain1);					
			});	
			$('#make_total').html(Contain);		
		}	
 </script>	