<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Sessions
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    All Sessions
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-left">
        <form class="form-horizontal" action="<?php echo make_admin_url('session', 'student', 'student') ?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
            <input type='hidden' name='Page' value='session'/>
            <input type='hidden' name='action' value='list'/>
            <input type='hidden' name='section' value='list'/>




            <div class="control-group alert alert-success" style='margin-left:0px;padding-right:0px;'>	
                <div class='span3'>
                    <label class="control-label" style='float: none; text-align: left;'>Select Session</label>
                    <select class="select2_category session_filter" data-placeholder="Select Session Students" name='sess_int' id='session_id'>
                        <option value="">Select Session</option>
                        <?php foreach ($AllSessYear as $s_key => $val): $yearSes = $val['from'] . '-' . $val['to'] ?>
                        <option value='<?= $yearSes ?>' <?= ($sess_int == $yearSes) ? 'selected' : '' ?> ><?= $val['from'] ?> - <?= $val['to'] ?></option>
                        <? endforeach; ?>
                    </select>
                </div>

                <div class='span3' id='ajex_section'>
                    <label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>

                    <input type='submit' name='go' value='Go' class='btn blue'/>
                </div>
            </div>

        </form>										
    </div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-certificate"></i>Manage Sessions > </div>
                    <div class="caption"><?php echo empty($sess_int) ? $AllSessYear[0]['from'] . '-' . $AllSessYear[0]['to'] : $sess_int; ?></div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('session', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >	
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>
                                    <th>Position</th>
                                    <th>Session</th>
                                    <th>Course Name</th>
                                    <th style='text-align:center' class="hidden-480">Start Date</th>
                                    <th style='text-align:center' class="hidden-480">End Date</th>
                                    <th style='text-align:center' class="hidden-480 sorting_disabled">Students</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <? if ($QueryObj->GetNumRows() != 0): ?>
                            <tbody>
                                <?php
                                $sr = 1;
                                while ($object = $QueryObj->GetObjectFromRecord()):
                                $course = get_object('school_course', $object->course_id);
                                ?>
                                <tr class="odd gradeX">
                                    <td><?= $object->position ?></td>
                                    <td><?= make_session_name($object->start_date, $object->end_date) ?></td>
                                    <td><?= $course->course_name ?></td>
                                    <td style='text-align:center' class="hidden-480"><?= date('d M, Y', strtotime($object->start_date)) ?></td>
                                    <td style='text-align:center' class="hidden-480"><?= date('d M, Y', strtotime($object->end_date)) ?></td>
                                    <td style='text-align:center' class="hidden-480 sorting_disabled"><a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('session', 'view', 'view', 'id=' . $object->id) ?>" title="click here to add/delete students in this session">Students In This Session </a> </td>
                                    <td style='text-align:right'>
                                        <a class="btn mini black icn-only tooltips" href="<?php echo make_admin_url('session_info', 'list', 'list', 'id=' . $object->id) ?>" title="click here to view session information"><i class="icon-search icon-white"></i></a>&nbsp;
                                        <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('session', 'update', 'update', 'id=' . $object->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp; 
                                        <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('session', 'delete', 'delete', 'id=' . $object->id . '&delete=1') ?>" onclick="return confirm('Are you sure? You are deleting this session.');" title="click here to delete this session"><i class="icon-remove icon-white"></i></a>
                                    </td>												
                                </tr>
                                <?php
                                $sr++;
                                endwhile;
                                ?>
                            </tbody>
                            <?php endif; ?>  


                        </table>
                    </form>    
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



