

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Add From All Students
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 
                                   
                                    <li class="last">
                                        All Students
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
	
				<div class="tiles pull-right">
							<div class="tile bg-purple <?php echo ($section=='other')?'selected':''?>">
							<a href="<?php echo make_admin_url('session', 'view', 'view&id='.$ct_session);?>">
								<div class="corner"></div>
								<div class="tile-body"><i class="icon-arrow-left"></i></div>
								<div class="tile-object"><div class="name">Session Students</div></div>
							</a> 
							</div>				
						<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->

				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<form class="form-horizontal" action="<?php echo make_admin_url('session', 'other', 'other&id='.$id.'&ct_session='.$ct_session)?>" method="POST" enctype="multipart/form-data" id="validation">
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i><? if(is_object($object)): echo $object->title; else: echo "All"; endif;?> > Students</div>								
							</div>
							<div class="portlet-body">
								<div class="clearfix">
									
								</div>
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										<tr>
											<th style="width:8px;"><input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" /></th>
											<th class="hidden-480">Reg ID</th>
											<th class="hidden-480">Name</th>
											<th class="hidden-480">Father Name</th>
											<th class="hidden-480">Gender</th>	
											<th class="hidden-480">Course Of Admission</th>	
											<th style='width:18%;' class="hidden-480 sorting_disabled"></th>
										</tr>
									</thead>
									<? if(!empty($AllStu)):?>
									<tbody>
									<?php $sr=1;foreach($AllStu as $sk=>$stu): $checked='';?>
										<tr class="odd gradeX" <? if(in_array($stu->id,$session_st)): echo 'id="added"'; $checked='checked'; endif;?> >
											<td><input type="checkbox" class="checkboxes" value="<?php echo $stu->id?>" name="multiopt[<?php echo $stu->id?>]" id="multiopt[<?php echo $stu->id?>]" <?=$checked?>/></td>
											<td><?=$stu->reg_id?></td>
											<td><?=$stu->first_name." ".$stu->last_name?></td>
											<td><?=$stu->father_name?></td>
											<td class="hidden-480"><?=$stu->sex?></td>
											<td class="hidden-480"><?=$stu->course_of_admission?></td>
											<td class="hidden-480 sorting_disabled">
												<? if(in_array($stu->id,$session_st)):?>
													<span class="btn green mini" >Added</span>
												<? endif;?>
											</td>											
										</tr>
									<? endforeach;?>
									<? endif;?>	
									</tbody>
								</table>
								<? if(!empty($AllStu)):?>
								<input type='submit' name='multi_select' value='Add Selected Students' class='btn green'/>
								<? endif;?>
							</div>
						</div>
						</form>
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    
<script type="text/javascript">
	$(".session_filter").change(function(){	 
			$("#session_filter").submit();
	});
</script> 


