<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Staff
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('staff', 'list', 'list'); ?>">List Staff</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Edit Staff
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
    </div>	            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('staff', 'update', 'update&id=' . $id) ?>" method="POST" enctype="multipart/form-data" id="validation">
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Edit Staff</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <h4 class="form-section hedding_inner">Basic Information</h4>
                        <div class="row-fluid">    
                            <div class="control-group span6">
                                <label class="control-label" for="title">Title<span class="required">*</span></label>
                                <div class="controls">
                                    <select  name="title" id="title"  class="span10 m-wrap ">
                                        <option value="Mr." <?= $staff->title == 'Mr.' ? "selected" : "" ?> >Mr.</option>
                                        <option value="Mrs." <?= $staff->title == 'Mrs.' ? "selected" : "" ?>>Mrs.</option>
                                        <option value="Miss." <?= $staff->title == 'Miss.' ? "selected" : "" ?>>Miss</option>					
                                    </select>                      
                                </div>
                            </div>	
                        </div>		
                        <div class="row-fluid">
                            <div class="span6 ">				
                                <div class="control-group">
                                    <label class="control-label" for="first_name">First Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="first_name" value="<?= $staff->first_name ?>" id="first_name" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="sex">Sex<span class="required">*</span></label>
                                    <div class="controls">
                                        <select name="sex" id="school_type"  class="span10 m-wrap ">
                                            <option value="Male" <?= $staff->sex == 'Male' ? "selected" : "" ?>>Male</option>
                                            <option value="Female" <?= $staff->sex == 'Female' ? "selected" : "" ?>>Female</option>					</select>                      
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="email">Email Address<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="email" value="<?= $staff->email ?>" id="email" class="span10 m-wrap validate[required,custom[email]]"/></div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="phone">Landline Phone<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="phone" value="<?= $staff->phone ?>" id="phone" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="image">Photo</label>
                                    <div class="controls">
                                        <?
                                        if ($staff->photo):
                                            $im_obj = new imageManipulation()
                                            ?>
                                            <div class="item span10" style='text-align:center;'>	
                                                <a class="fancybox-button" data-rel="fancybox-button" title="<?= $staff->title . " " . $staff->first_name . " " . $staff->last_name ?>" href="<?= $im_obj->get_image('staff', 'large', $staff->photo); ?>">
                                                    <div class="zoom">
                                                        <img src="<?= $im_obj->get_image('staff', 'medium', $staff->photo); ?>" />
                                                        <div class="zoom-icon"></div>
                                                    </div>
                                                </a>
                                                <div class="details">
                                                    <a href="<?= make_admin_url('staff', 'delete_image', 'delete_image&id=' . $staff->id); ?>" class="icon" onclick="return confirm('Are you sure? You are deleting this image.');" title="click here to delete this image"><i class="large icon-remove"></i></a>    
                                                </div>	
                                            </div>					<? else: ?>
                                            <input type="file" name="image"  id="image" class="span10 m-wrap"/>
                                        <? endif; ?> 
                                    </div>
                                </div> 				
                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="last_name">Last Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="last_name" value="<?= $staff->last_name ?>" id="last_name" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="marital">Marital Status<span class="required">*</span></label>
                                    <div class="controls">
                                        <select  name="marital" id="marital"  class="span10 m-wrap ">
                                            <option value="Married" <?= $staff->marital == 'Married' ? "selected" : "" ?>>Married</option>
                                            <option value="Unmarried" <?= $staff->marital == 'Unmarried' ? "selected" : "" ?>>Unmarried</option>
                                        </select>                      
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="date_of_birth">Date Of Birth<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="date_of_birth" value="<?= $staff->date_of_birth ?>" id="date_of_birth" class='span10 m-wrap m-ctrl-medium upto_current_date validate[required]'/>
                                    </div>
                                </div> 	  				
                                <div class="control-group">
                                    <label class="control-label" for="mobile">Mobile Phone<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="mobile" value="<?= $staff->mobile ?>" id="mobile" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                    </div>
                                </div> 	                                           
                            </div>	
                        </div>	
                        <h4 class="form-section hedding_inner">Permanent Address</h4>
                        <div class="row-fluid">
                            <div class="span6 ">      					<div class="control-group">
                                    <label class="control-label" for="permanent_address1">Address1<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="permanent_address1" value="<?= $staff->permanent_address1 ?>" id="permanent_address1" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="permanent_city">City<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="permanent_city" value="<?= $staff->permanent_city ?>" id="permanent_city" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="permanent_country">Country<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="permanent_country" value="<?= $staff->permanent_country ?>" id="permanent_country" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>				
                            </div>
                            <div class="span6 ">	
                                <div class="control-group">
                                    <label class="control-label" for="permanent_address2">Address2</label>
                                    <div class="controls">
                                        <input type="text" name="permanent_address2" value="<?= $staff->permanent_address2 ?>" id="permanent_address2" class="span10"/>
                                    </div>
                                </div>					
                                <div class="control-group">
                                    <label class="control-label" for="permanent_state">State<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="permanent_state" value="<?= $staff->permanent_state ?>" id="permanent_state" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="permanent_zip">Zip Code<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="permanent_zip" value="<?= $staff->permanent_zip ?>" id="permanent_zip" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                            </div>	
                        </div>	
                        <h4 class="form-section hedding_inner">Correspondence Address<font style='float: right; margin-right: 60px; font-weight: normal; font-size: 17px;'>Same As Permanent <input type='checkbox' id='same_address' /></font></h4>
                        <div class="row-fluid">
                            <div class="span6 ">      				
                                <div class="control-group">
                                    <label class="control-label" for="correspondence_address1">Address1<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_address1" value="<?= $staff->correspondence_address1 ?>" id="correspondence_address1" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="correspondence_city">City<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_city" value="<?= $staff->correspondence_city ?>" id="correspondence_city" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="correspondence_country">Country<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_country" value="<?= $staff->correspondence_country ?>" id="correspondence_country" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 				
                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="correspondence_address2">Address2</label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_address2" value="<?= $staff->correspondence_address2 ?>" id="correspondence_address2" class="span10"/>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="state">State<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_state" value="<?= $staff->correspondence_state ?>" id="correspondence_state" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="correspondence_zip">Zip Code<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_zip" value="<?= $staff->correspondence_zip ?>" id="correspondence_zip" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                            </div>	
                        </div>		
                        <h4 class="form-section hedding_inner">Pay Rules</h4>
                        <div class="row-fluid">
                            <div class="span6">	
                                <div class="control-group">
                                    <label class="control-label" for="phone">Select Earning Heads</label>
                                    <div class="controls">
                                        <?php foreach ($earningHeads as $k => $earning) { ?>
                                            <div class="span12" <?php if ($k == '0') { ?>style="margin-left: 10px;"<?php } ?>>				
                                                <span>
                                                    <input type="checkbox" class='salary_pay_rule_heads_earning' value="<?= $earning->id ?>" data-pay_rule_title="<?= ucfirst($earning->title) ?>" data-id="<?= $earning->id ?>" <?php echo array_key_exists($earning->id, $earning_heads) ? 'checked' : ''; ?>>
                                                </span><?= ucfirst($earning->title) ?>
                                            </div>
                                        <?php } ?>		
                                    </div> 	
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <div class="controls" style='margin-left:0px;'>
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width:30%">Earning Heads Name</th>
                                                    <th style="width:30%">Amount (<?= CURRENCY_SYMBOL ?>)</th>
                                                    <th style="width:40%">Type</th>
                                                </tr>
                                            </thead>
                                            <tbody id='add_pay_rule_heads_earning'>
                                                <?php if ($earning_heads) { ?>
                                                    <?php foreach ($earning_heads as $k => $earning) { ?>
                                                        <tr class="head_<?php echo $k; ?>">
                                                            <td>
                                                                <?php
                                                                echo staffPayHeads::getFieldDetail($k, 'title');
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <input type="number" step="any" name="earning[<?php echo $k; ?>][amount]" class="span10" value="<?php echo $earning['amount']; ?>" placeholder="0.00"/>
                                                            </td>
                                                            <td>
                                                                <span>
                                                                    <label>
                                                                        <input type="radio" name="earning[<?php echo $k; ?>][type]" value="flat" <?php echo $earning['type'] == 'flat' ? 'checked' : '' ?>/> Manual
                                                                    </label>
                                                                    <!--                                                                    <label>
                                                                                                                                            <input type="radio" name="earning[<?php echo $k; ?>][type]" value="percentage" <?php echo $earning['type'] == 'percentage' ? 'checked' : '' ?>/> %age
                                                                                                                                        </label> -->
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <tr id="no_record_earning"><td colspan="3">No Record Found..!</td></tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>  
                                </div>	
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span6">	
                                <div class="control-group">
                                    <label class="control-label" for="phone">Select Deduction Heads</label>
                                    <div class="controls">

                                        <?php foreach ($deductionHeads as $k => $deduction) { ?>

                                            <div class="span12" <?php if ($k == '0') { ?>style="margin-left: 10px;"<?php } ?>>				
                                                <span>
                                                    <input type="checkbox" class='salary_pay_rule_heads_deduction' value="<?= $deduction->id ?>" data-pay_rule_title="<?= $deduction->title ?>" data-pay_rule_dediction_for="<?= $deduction->earning_deduction ?>" data-id="<?= $deduction->id ?>" <?php echo array_key_exists($deduction->id, $deduction_heads) ? 'checked' : ''; ?>>
                                                </span><?= ucfirst($deduction->title) ?>
                                            </div>
                                        <?php } ?>		
                                        <span class='help-block'><strong>Note: </strong><a href='<?= make_admin_url('staff_pay_rule') ?>' target='_blank'>Click here</a> to add/edit salary heads.</span>
                                    </div> 	
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <div class="controls" style='margin-left:0px;'>
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Amount (<?= CURRENCY_SYMBOL ?>)</th>
                                                    <th>Type</th>
                                                    <th>Deduction For</th>
                                                </tr>
                                            </thead>
                                            <tbody id='add_pay_rule_heads_deduction'>
                                                <?php if ($deduction_heads) { ?>
                                                    <?php foreach ($deduction_heads as $k => $deduction) { ?>
                                                        <tr class="head_<?php echo $k; ?>">
                                                            <td>
                                                                <?php
                                                                echo staffPayHeads::getFieldDetail($k, 'title');
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <input type="hidden" name="deduction[<?php echo $k; ?>][deduction_for]" value="<?php echo $deduction['deduction_for']; ?>"/>
                                                                <input type="number" step="any" name="deduction[<?php echo $k; ?>][amount]" class="span10" value="<?php echo $deduction['amount']; ?>" placeholder="0.00"/>
                                                            </td>
                                                            <td>
                                                                <span>
                                                                    <label>
                                                                        <input type="radio" name="deduction[<?php echo $k; ?>][type]" value="flat" <?php echo $deduction['type'] == 'flat' ? 'checked' : '' ?>/> Manual
                                                                    </label>
                                                                    <label>
                                                                        <input type="radio" name="deduction[<?php echo $k; ?>][type]" value="percentage" <?php echo $deduction['type'] == 'percentage' ? 'checked' : '' ?>/> %age
                                                                    </label> 
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                echo staffPayHeads::getFieldDetail($k, 'earning_deduction');
                                                                ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                <?php } else { ?>
                                                    <tr id="no_record_deduction"><td colspan="3">No Record Found..!</td></tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>  
                                </div>	
                            </div>
                        </div>
                        <h4 class="form-section hedding_inner">Salary Detail</h4>
                        <div class="row-fluid">
                            <div class="span6 ">   
                                <div class="control-group">
                                    <label class="control-label" for="designation">Designation<span class="required">*</span></label>
                                    <div class="controls">
                                        <select  name="designation" id="designation"  class="span10 m-wrap ">
                                            <?php
                                            $sr = 1;
                                            while ($sub = $QueryDes->GetObjectFromRecord()):
                                                ?>
                                                <option value='<?= $sub->id ?>' <?= $staff->designation == $sub->id ? "selected" : "" ?>><?= ucfirst($sub->name) ?></option>
                                                <?
                                                $sr++;
                                            endwhile;
                                            ?>	
                                        </select>                      
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="join_date">Join Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="join_date" value="<?= $staff->join_date ?>" id="join_date" class='span10 m-wrap m-ctrl-medium new_format'/>
                                    </div>
                                </div>
                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="staff_category">Staff Category<span class="required">*</span></label>
                                    <div class="controls">
                                        <select  name="staff_category" id="staff_category"  class="span10 m-wrap ">
                                            <?php
                                            $sr = 1;
                                            while ($cat = $QueryCat->GetObjectFromRecord()):
                                                ?>
                                                <option value='<?= $cat->id ?>' <?= $staff->staff_category == $cat->id ? "selected" : "" ?>><?= ucfirst($cat->name) ?></option>
                                                <?
                                                $sr++;
                                            endwhile;
                                            ?>	
                                        </select>                      
                                    </div>
                                </div>	
                            </div>	
                        </div>	
                        <h4 class="form-section hedding_inner">Quit Detail</h4>
                        <div class="row-fluid">
                            <div class="span6 ">  
                                <div class="control-group">
                                    <label class="control-label" for="is_quit">Is Quit</label>
                                    <div class="controls" style='margin-top:5px;'>
                                        <input type="checkbox"  name="is_quit" id="is_quit" value="1" <?= ($staff->is_quit == '1') ? 'checked' : ''; ?>/>
                                    </div>
                                </div> 
                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="school_head_phone">Quit Date</label>
                                    <div class="controls">
                                        <input type="text" name="quit_date" value="<?
                                        if (!empty($staff->quit_date) && $staff->quit_date != '0000-00-00'): echo $staff->quit_date;
                                        endif;
                                        ?>" id="quit_date" class='span10 m-wrap m-ctrl-medium new_format'/>
                                    </div>
                                </div>
                            </div>	
                        </div>				
                        <h4 class="form-section hedding_inner">Education Qualification</h4>
                        <? $Ql = '0'; ?>
                        <div class="row-fluid">
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr>
                                        <th>Qualification</th>
                                        <th class="hidden-phone"><i class="icon-briefcase"></i> University/Institute</th>
                                        <th class="hidden-phone"><i class="icon-question-sign"></i> Year Of Passing</th>
                                        <th class="hidden-phone" style='width:20%;'><i class="icon-bookmark"></i> Percentage (%)</th>
                                        <th class="hidden-phone" style='width:7%;'></th>
                                    </tr>
                                </thead>
                                <tbody id='more_ql'>
                                    <?php
                                    $Q_sr = 0;
                                    if ($QueryQual->GetNumRows()):
                                        while ($Qual = $QueryQual->GetObjectFromRecord()):
                                            ?>
                                            <tr>
                                                <td><input type='text' name="qualification[<?= $Q_sr ?>][qualification]" class='span9' id='qualification<?= $Q_sr ?>' value='<?= $Qual->qualification ?>'/></td>
                                                <td class="hidden-phone"><input type='text' name="qualification[<?= $Q_sr ?>][institute]" class='span9' id='institute<?= $Q_sr ?>' value='<?= $Qual->institute ?>'/></td>
                                                <td class="hidden-phone"><input type='text' name="qualification[<?= $Q_sr ?>][year_of_passing]" class='span6' id='year_of_passing<?= $Q_sr ?>' value='<?= $Qual->year_of_passing ?>'/></td>
                                                <td class="hidden-phone"><input type='text' name="qualification[<?= $Q_sr ?>][percentage]" class='span4' id='percentage<?= $Q_sr ?>' value='<?= $Qual->percentage ?>'/>%</td>
                                                <td class="hidden-phone"><a class="btn red" href="<?= make_admin_url('staff', 'delete_qualification', 'delete_qualification&id=' . $staff->id . '&q_id=' . $Qual->id); ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a></td>
                                        <input type='hidden' name="qualification[<?= $Q_sr ?>][id]" class='span4' id='id<?= $Q_sr ?>' value='<?= $Qual->id ?>'/>
                                        </tr>	

                                        <?
                                        $Q_sr++;
                                    endwhile;
                                    ?>
                                <? else: ?>
                                    <tr><td colspan='4'> Sorry, No Record Found..!</td></tr>
                                <? endif; ?>
                                </tbody>
                            </table>
                            <input type='hidden' value='<?= $Q_sr ?>' id='total_ql'>
                            <span class='span12' style='text-align:right;margin-left:0px;'><a class="btn icn-only green" id='add_new_ql'>Add More<i class="m-icon-swapright m-icon-white"></i></a></span>
                        </div>

                        <!-- Experience  -->
                        <h4 class="form-section hedding_inner">Experience</h4>
                        <div class="row-fluid">
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr>
                                        <th>Designation</th>
                                        <th class="hidden-phone">Department</th>
                                        <th class="hidden-phone"> From</th>
                                        <th class="hidden-phone" style='width:20%;'> To</th>
                                        <th class="hidden-phone" style='width:7%;'></th>
                                    </tr>
                                </thead>
                                <tbody id='more_ex'>
                                    <?php
                                    $E_sr = 0;
                                    if ($QueryExp->GetNumRows()):
                                        while ($Exp = $QueryExp->GetObjectFromRecord()):
                                            ?>
                                            <tr>
                                                <td><input type='text' name="experience[<?= $E_sr ?>][designation]" class='span9' id='designation<?= $E_sr ?>' value='<?= $Exp->designation ?>'/></td>
                                                <td class="hidden-phone"><input type='text' name="experience[<?= $E_sr ?>][department]" class='span9' id='department<?= $E_sr ?>' value='<?= $Exp->department ?>'/></td>
                                                <td class="hidden-phone"><input type='text' name="experience[<?= $E_sr ?>][from_date]" class='span6 m-wrap m-ctrl-medium upto_current_date' id='from_date<?= $E_sr ?>' value='<?= $Exp->from_date ?>'/></td>
                                                <td class="hidden-phone"><input type='text' name="experience[<?= $E_sr ?>][to_date]" class='span7 m-wrap m-ctrl-medium upto_current_date' id='to_date<?= $E_sr ?>' value='<?= $Exp->to_date ?>'/></td>
                                                <td class="hidden-phone"><a class="btn red" href="<?= make_admin_url('staff', 'delete_exp', 'delete_exp&id=' . $staff->id . '&e_id=' . $Exp->id); ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a></td>
                                        <input type='hidden' name="experience[<?= $E_sr ?>][id]" class='span4' id='id<?= $E_sr ?>' value='<?= $Exp->id ?>'/>
                                        </tr>	

                                        <?
                                        $E_sr++;
                                    endwhile;
                                    ?>
                                <? else: ?>
                                    <tr><td colspan='<?= $E_sr ?>'> Sorry, No Record Found..!</td></tr>															
                                <? endif; ?>					
                                </tbody>
                            </table>
                            <input type='hidden' value='2' id='total_ex'>
                            <span class='span12' style='text-align:right;margin-left:0px;'><a class="btn icn-only green" id='add_new_ex'>Add More<i class="m-icon-swapright m-icon-white"></i></a></span>
                        </div>	


                        <!-- Documents  -->
                        <h4 class="form-section hedding_inner">Documents</h4>

                        <div class="row-fluid">
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr>																	
                                        <th class="hidden-phone">Title</th>
                                        <th class="hidden-phone" style='width:60%;'> File</th>
                                        <th class="hidden-phone" style='width:7%;'> </th>
                                    </tr>
                                </thead>
                                <tbody id='more_doc'>
                                    <?php
                                    if ($QueryDoc->GetNumRows()):
                                        $D_sr = 1;
                                        while ($Doc = $QueryDoc->GetObjectFromRecord()):
                                            ?>
                                            <tr>
                                                <td><?= $Doc->document_title ?></td>
                                                <td class="hidden-phone"><a target="_BLANK" href="<?= DIR_WS_SITE_UPLOAD . 'file/document/' . $Doc->file ?>"><?= $Doc->file ?></a></td>
                                                <td class="hidden-phone"><a class="btn red" href="<?= make_admin_url('staff', 'delete_doc', 'delete_doc&id=' . $staff->id . '&d_id=' . $Doc->id); ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a></td>
                                            </tr>																	
                                            <?
                                            $D_sr++;
                                        endwhile;
                                        ?>
                                    <? else: ?>
                                        <tr><td colspan='2'> Sorry, No Record Found..!</td></tr>
                                    <? endif; ?>															
                                </tbody>
                            </table>
                            <input type='hidden' value='1' id='total_doc'>
                            <span class='span12' style='text-align:right;margin-left:0px;'><a class="btn icn-only green" id='add_new_doc' >Add More<i class="m-icon-swapright m-icon-white"></i></a></span>
                        </div>	

                        <h3 class="form-section"></h3>
                        <div class="control-group">
                            <label class="control-label" for="is_active">Make Active</label>
                            <div class="controls" style='margin-top:5px;'>
                                <input type="checkbox" name="is_active" id="is_active" value="1" <?= ($staff->is_active == '1') ? 'checked' : ''; ?>/>
                            </div>
                        </div> 
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>"  />
                            <input type="hidden" name="id" value="<?= $staff->id ?>"  />
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('staff', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>    
                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        /* Add New Qualification */
        $("#add_new_ql").live("click", function () {
            var current_ql = $('#total_ql').val();
            var new_ql = parseInt(current_ql) + parseInt('1');

            $('#more_ql').append("<tr class='delete_row" + new_ql + "'><td><input type='text' name='qualification[" + new_ql + "][qualification]' class='span9 validate[required]' id='qualification" + new_ql + "'/></td><td class='hidden-phone'><input type='text' name='qualification[" + new_ql + "][institute]' class='span9 validate[required]' id='institute" + new_ql + "'/></td><td class='hidden-phone'><input type='text' name='qualification[" + new_ql + "][year_of_passing]' class='span6 validate[required,custom[onlyNumberSp]]' id='year_of_passing" + new_ql + "'/></td><td class='hidden-phone'><input type='text' name='qualification[" + new_ql + "][percentage]' class='span4 validate[required]' id='percentage" + new_ql + "'/>%</td><td><a class='btn red icn-only delete_row'><i class='icon-remove icon-white'></i></a></td></tr>");
            $('#total_ql').val(new_ql);
        });
        /* Delete Qualificatioin */
        $(".delete_row").live("click", function () {
            var current_ql = $('#total_ql').val();
            var new_ql = parseInt(current_ql) - parseInt('1');
            if (new_ql < 0)
            {
                return false;
            }
            else {
                $('.delete_row' + current_ql + '').css('display', 'none');
                $('#total_ql').val(new_ql);
            }
        });
        /* Add New experience */
        $("#add_new_ex").live("click", function () {
            var current_ex = $('#total_ex').val();
            var new_ex = parseInt(current_ex) + parseInt('1');

            var dataString = 'id=' + new_ex;

            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_add_staff', 'ajax_add_staff&temp=staff'); ?>",
                data: dataString,
                success: function (data, textStatus)
                {
                    $("#more_ex").append(data);
                    $('#total_ex').val(new_ex);
                }
            });
        });
        /* Delete Experience */
        $(".delete_row_ex").live("click", function () {
            var current_ex = $('#total_ex').val();
            var new_ex = parseInt(current_ex) - parseInt('1');
            if (new_ex < 2)
            {
                return false;
            }
            else {
                $('.delete_row_ex' + current_ex + '').css('display', 'none');
                $('#total_ex').val(new_ex);
            }
        });
        /* Add New Document */
        $("#add_new_doc").live("click", function () {
            var current_doc = $('#total_doc').val();
            var new_doc = parseInt(current_doc) + parseInt('1');

            $('#more_doc').append("<tr class='delete_row_doc" + new_doc + "'><td><input type='text' name='document[" + new_doc + "][document_title]' class='span10'/></td><td class='hidden-phone'><input type='file' name='file[" + new_doc + "]'/></td><td><a class='btn red icn-only delete_row_doc'><i class='icon-remove icon-white'></i></a></td></tr>");
            $('#total_doc').val(new_doc);
        });
        /* Delete Document */
        $(".delete_row_doc").live("click", function () {
            var current_doc = $('#total_doc').val();
            var new_doc = parseInt(current_doc) - parseInt('1');
            if (new_doc < 1)
            {
                return false;
            }
            else {
                $('.delete_row_doc' + current_doc + '').css('display', 'none');
                $('#total_doc').val(new_doc);
            }
        });
        $("#same_address").live("click", function () {
            if ($('#same_address').attr('checked')) {
                $('#correspondence_address1').val($('#permanent_address1').val());
                $('#correspondence_address2').val($('#permanent_address2').val());
                $('#correspondence_city').val($('#permanent_city').val());
                $('#correspondence_state').val($('#permanent_state').val());
                $('#correspondence_country').val($('#permanent_country').val());
                $('#correspondence_zip').val($('#permanent_zip').val());
            } else {
                $('#correspondence_address1').val('');
                $('#correspondence_address2').val('');
                $('#correspondence_city').val('');
                $('#correspondence_state').val('');
                $('#correspondence_country').val('');
                $('#correspondence_zip').val('');
            }
        });
    });
</script>

<script>
    $(document).on('change', '.salary_pay_rule_heads_earning', function () {
        $('#no_record_earning').remove();
        var pay_rule_id = $(this).attr('data-id');
        var pay_rule_title = $(this).attr('data-pay_rule_title');
        if ($(this).closest('span').hasClass('checked')) {
            $('#add_pay_rule_heads_earning').append('<tr class="head_' + pay_rule_id + '"><td>' + pay_rule_title + '</td><td><input type="number" step="any" name="earning[' + pay_rule_id + '][amount]" class="span10" placeholder="0.00"/></td><td><span><label><input type="radio" name="earning[' + pay_rule_id + '][type]" value="flat" checked/> Manual</label></span></td></tr>');
//            <label><input type="radio" name="earning[' + pay_rule_id + '][type]" value="percentage"/> %age</label>
        } else {
            $('.head_' + pay_rule_id).remove();
        }
    });
    $(document).on('change', '.salary_pay_rule_heads_deduction', function () {
        $('#no_record_deduction').remove();
        var pay_rule_id = $(this).attr('data-id');
        var pay_rule_title = $(this).attr('data-pay_rule_title');
        var data_pay_rule_deduction_for = $(this).attr('data-pay_rule_dediction_for');
        if ($(this).closest('span').hasClass('checked')) {
            $('#add_pay_rule_heads_deduction').append('<tr class="head_' + pay_rule_id + '"><td>' + pay_rule_title + '</td><td><input type="hidden" name="deduction[' + pay_rule_id + '][deduction_for]" value="' + data_pay_rule_deduction_for + '"/><input type="number" step="any" name="deduction[' + pay_rule_id + '][amount]" class="span10" placeholder="0.00"/></td><td><span><label><input type="radio" name="deduction[' + pay_rule_id + '][type]" value="flat" checked/> Manual</label><label><input type="radio" name="deduction[' + pay_rule_id + '][type]" value="percentage"/> %age</label></span></td><td>' + data_pay_rule_deduction_for + '</td></tr>');
        } else {
            $('.head_' + pay_rule_id).remove();
        }
    });
</script>   