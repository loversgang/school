<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Staff
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('staff', 'list', 'list'); ?>">List Staff</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add New Staff
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
    </div>	            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal validation" action="<?php echo make_admin_url('staff', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">	
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Add New Staff</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <h4 class="form-section hedding_inner">Basic Information</h4>
                        <div class="row-fluid">    
                            <div class="control-group span6">
                                <label class="control-label" for="title">Title<span class="required">*</span></label>
                                <div class="controls">
                                    <select  name="title" id="title"  class="span10 m-wrap ">
                                        <option value="Mr.">Mr.</option>
                                        <option value="Mrs.">Mrs.</option>
                                        <option value="Miss.">Miss</option>					</select>                      
                                </div>
                            </div>	
                        </div>		
                        <div class="row-fluid">
                            <div class="span6 ">				
                                <div class="control-group">
                                    <label class="control-label" for="first_name">First Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="first_name" value="" id="first_name" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="sex">Sex<span class="required">*</span></label>
                                    <div class="controls">
                                        <select name="sex" id="school_type"  class="span10 m-wrap ">
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>					</select>                      
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="email">Email Address<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="email" value="" id="email" class="span10 m-wrap validate[required,custom[email]]"/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="phone">Landline Phone<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="phone" value="" id="phone" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="image">Photo</label>
                                    <div class="controls">
                                        <input type="file" name="image" value="" id="image" class="span10 m-wrap"/>
                                    </div>
                                </div> 				
                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="last_name">Last Name<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="last_name" value="" id="last_name" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="marital">Marital Status<span class="required">*</span></label>
                                    <div class="controls">
                                        <select  name="marital" id="marital"  class="span10 m-wrap ">
                                            <option value="Married">Married</option>
                                            <option value="Unmarried">Unmarried</option>
                                        </select>                      
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="date_of_birth">Date Of Birth<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="date_of_birth" value="" id="date_of_birth" class='span10 m-wrap m-ctrl-medium upto_current_date validate[required]'/>
                                    </div>
                                </div> 	  				
                                <div class="control-group">
                                    <label class="control-label" for="mobile">Mobile Phone<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="mobile" value="" id="mobile" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
                                    </div>
                                </div> 	                                           
                            </div>	
                        </div>	
                        <h4 class="form-section hedding_inner">Permanent Address</h4>
                        <div class="row-fluid">
                            <div class="span6 ">      					<div class="control-group">
                                    <label class="control-label" for="permanent_address1">Address1<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="permanent_address1" value="" id="permanent_address1" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="permanent_city">City<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="permanent_city" value="" id="permanent_city" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="permanent_country">Country<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="permanent_country" value="" id="permanent_country" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>				
                            </div>
                            <div class="span6 ">	
                                <div class="control-group">
                                    <label class="control-label" for="permanent_address2">Address2</label>
                                    <div class="controls">
                                        <input type="text" name="permanent_address2" value="" id="permanent_address2" class="span10"/>
                                    </div>
                                </div>					
                                <div class="control-group">
                                    <label class="control-label" for="permanent_state">State<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="permanent_state" value="" id="permanent_state" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="permanent_zip">Zip Code<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="permanent_zip" value="" id="permanent_zip" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                            </div>	
                        </div>	
                        <h4 class="form-section hedding_inner">Correspondence Address<font style='float: right; margin-right: 60px; font-weight: normal; font-size: 17px;'>Same As Permanent <input type='checkbox' id='same_address' /></font></h4>
                        <div class="row-fluid">
                            <div class="span6 ">      					<div class="control-group">
                                    <label class="control-label" for="correspondence_address1">Address1<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_address1" value="" id="correspondence_address1" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 
                                <div class="control-group">
                                    <label class="control-label" for="correspondence_city">City<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_city" value="" id="correspondence_city" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="correspondence_country">Country<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_country" value="" id="correspondence_country" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div> 				
                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="correspondence_address2">Address2</label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_address2" value="" id="correspondence_address2" class="span10"/>
                                    </div>
                                </div>	
                                <div class="control-group">
                                    <label class="control-label" for="state">State<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_state" value="" id="correspondence_state" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="correspondence_zip">Zip Code<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="correspondence_zip" value="" id="correspondence_zip" class="span10 m-wrap validate[required]"/>
                                    </div>
                                </div>
                            </div>	
                        </div>			
                        <h4 class="form-section hedding_inner">Education Qualification</h4>
                        <? $Ql = '0'; ?>
                        <div class="row-fluid">
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr>
                                        <th>Qualification</th>
                                        <th class="hidden-phone"><i class="icon-briefcase"></i> University/Institute</th>
                                        <th class="hidden-phone"><i class="icon-question-sign"></i> Year Of Passing</th>
                                        <th class="hidden-phone" style='width:20%;'><i class="icon-bookmark"></i> Percentage (%)</th>
                                    </tr>
                                </thead>
                                <tbody id='more_ql'>
                                    <tr>
                                        <td><input type='text' name="qualification[0][qualification]" class='span9' id='qualification0'/> </td>
                                        <td class="hidden-phone"><input type='text' name="qualification[0][institute]" class='span9' id='institute0'/></td>
                                        <td class="hidden-phone"><input type='text' name="qualification[0][year_of_passing]" class='span6' id='year_of_passing0'/></td>
                                        <td class="hidden-phone"><input type='text' name="qualification[0][percentage]" class='span4' id='percentage0'/>%</td>
                                    </tr>
                                    <tr>
                                        <td><input type='text' name="qualification[1][qualification]" class='span9' id='qualification1'/> </td>
                                        <td class="hidden-phone"><input type='text' name="qualification[1][institute]" class='span9' id='institute1'/></td>
                                        <td class="hidden-phone"><input type='text' name="qualification[1][year_of_passing]" class='span6' id='year_of_passing1'/></td>
                                        <td class="hidden-phone"><input type='text' name="qualification[1][percentage]" class='span4' id='percentage1'/>%</td>
                                    </tr>
                                    <tr>
                                        <td><input type='text' name="qualification[2][qualification]" class='span9' id='qualification2'/> </td>
                                        <td class="hidden-phone"><input type='text' name="qualification[2][institute]" class='span9' id='institute2'/></td>
                                        <td class="hidden-phone"><input type='text' name="qualification[2][year_of_passing]" class='span6' id='year_of_passing2'/></td>
                                        <td class="hidden-phone"><input type='text' name="qualification[2][percentage]" class='span4' id='percentage2'/>%</td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type='hidden' value='2' id='total_ql'>
                            <span class='span12' style='text-align:right;margin-left:0px;'><a class="btn icn-only green" id='add_new_ql'>Add More<i class="m-icon-swapright m-icon-white"></i></a></span>
                        </div>
                        <h4 class="form-section hedding_inner">Experience</h4>
                        <div class="row-fluid">
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr>
                                        <th>Designation</th>
                                        <th class="hidden-phone">Department</th>
                                        <th class="hidden-phone"> From</th>
                                        <th class="hidden-phone" style='width:20%;'> To</th>
                                        <th class="hidden-phone" style='width:7%;'> </th>
                                    </tr>
                                </thead>
                                <tbody id='more_ex'>
                                    <tr>
                                        <td><input type='text' name="experience[0][designation]" class='span9' id='designation0'/> </td>
                                        <td class="hidden-phone"><input type='text' name="experience[0][department]" class='span9' id='department0'/></td>
                                        <td class="hidden-phone"><input type='text' name="experience[0][from_date]" class='span6 m-wrap m-ctrl-medium date-picker' id='from_date0'/></td>
                                        <td class="hidden-phone"><input type='text' name="experience[0][to_date]" class='span7 m-wrap m-ctrl-medium date-picker' id='to_date0'/></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input type='text' name="experience[1][designation]" class='span9' id='designation1'/> </td>
                                        <td class="hidden-phone"><input type='text' name="experience[1][department]" class='span9' id='department1'/></td>
                                        <td class="hidden-phone"><input type='text' name="experience[1][from_date]" class='span6 m-wrap m-ctrl-medium date-picker' id='from_date1'/></td>
                                        <td class="hidden-phone"><input type='text' name="experience[1][to_date]" class='span7 m-wrap m-ctrl-medium date-picker' id='to_date1'/></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input type='text' name="experience[2][designation]" class='span9' id='designation2'/> </td>
                                        <td class="hidden-phone"><input type='text' name="experience[2][department]" class='span9' id='department2'/></td>
                                        <td class="hidden-phone"><input type='text' name="experience[2][from_date]" class='span6 m-wrap m-ctrl-medium date-picker' id='from_date2'/></td>
                                        <td class="hidden-phone"><input type='text' name="experience[2][to_date]" class='span7 m-wrap m-ctrl-medium date-picker' id='to_date2'/></td>
                                        <td></td>
                                    </tr>				
                                </tbody>
                            </table>
                            <input type='hidden' value='2' id='total_ex'>
                            <span class='span12' style='text-align:right;margin-left:0px;'><a class="btn icn-only green" id='add_new_ex'>Add More<i class="m-icon-swapright m-icon-white"></i></a></span>
                        </div>	
                        <h4 class="form-section hedding_inner">Documents</h4>
                        <div class="row-fluid">
                            <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr>				
                                        <th class="hidden-phone">Title</th>
                                        <th class="hidden-phone" style='width:60%;'> File</th>
                                    </tr>
                                </thead>
                                <tbody id='more_doc'>
                                    <tr>
                                        <td class="hidden-phone"><input type='text' name="document[0][document_title]" class='span10' id='document_title0'/></td>
                                        <td class="hidden-phone"><input type='file' name="file[]" class='span10' id='file0'/></td>
                                    </tr>
                                    <tr>
                                        <td class="hidden-phone"><input type='text' name="document[1][document_title]" class='span10' id='document_title1'/></td>
                                        <td class="hidden-phone"><input type='file' name="file[]" class='span10' id='file1'></td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type='hidden' value='1' id='total_doc'>
                            <span class='span12' style='text-align:right;margin-left:0px;'><a class="btn icn-only green" id='add_new_doc'>Add More<i class="m-icon-swapright m-icon-white"></i></a></span>
                        </div>	
                        <h4 class="form-section hedding_inner">Pay Rules</h4>
                        <div class="row-fluid">
                            <div class="span6">	
                                <div class="control-group">
                                    <label class="control-label" for="phone">Select Earning Heads</label>
                                    <div class="controls">
                                        <?php foreach ($earningHeads as $k => $earning) { ?>
                                            <div class="span12" <?php if ($k == '0') { ?>style="margin-left: 10px;"<?php } ?>>				
                                                <span>
                                                    <input type="checkbox" class='salary_pay_rule_heads_earning' value="<?= $earning->id ?>" data-pay_rule_title="<?= ucfirst($earning->title) ?>" data-id="<?= $earning->id ?>">
                                                </span><?= ucfirst($earning->title) ?>
                                            </div>
                                        <?php } ?>		
                                    </div> 	
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <div class="controls" style='margin-left:0px;'>
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Earning Heads Name</th>
                                                    <th>Amount (<?= CURRENCY_SYMBOL ?>)</th>
                                                    <th>Type</th>
                                                </tr>
                                            </thead>
                                            <tbody id='add_pay_rule_heads_earning'></tbody>
                                        </table>
                                    </div>  
                                </div>	
                            </div>
                        </div>
                        <div class="row-fluid">
                            <div class="span6">	
                                <div class="control-group">
                                    <label class="control-label" for="phone">Select Deduction Heads</label>
                                    <div class="controls">
                                        <?php foreach ($deductionHeads as $k => $deduction) { ?>
                                            <div class="span12" <?php if ($k == '0') { ?>style="margin-left: 10px;"<?php } ?>>				
                                                <span>
                                                    <input type="checkbox" class='salary_pay_rule_heads_deduction' value="<?= $deduction->id ?>" data-pay_rule_title="<?= $deduction->title ?>" data-pay_rule_deduction_for="<?= $deduction->earning_deduction ?>" data-id="<?= $deduction->id ?>">
                                                </span><?= ucfirst($deduction->title) ?>
                                            </div>
                                        <?php } ?>		
                                        <span class='help-block'><strong>Note: </strong><a href='<?= make_admin_url('staff_pay_rule') ?>' target='_blank'>Click here</a> to add/edit salary heads.</span>
                                    </div> 	
                                </div>
                            </div>
                            <div class="span6">
                                <div class="control-group">
                                    <div class="controls" style='margin-left:0px;'>
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Deduction Heads Name</th>
                                                    <th>Amount (<?= CURRENCY_SYMBOL ?>)</th>
                                                    <th>Type</th>
                                                    <th>Deduction For</th>

                                                </tr>
                                            </thead>
                                            <tbody id='add_pay_rule_heads_deduction'></tbody>
                                        </table>
                                    </div>  
                                </div>	
                            </div>
                        </div>
                        <h4 class="form-section hedding_inner">Salary Detail</h4>
                        <div class="row-fluid">
                            <div class="span6 ">   
                                <div class="control-group">
                                    <label class="control-label" for="designation">Designation<span class="required">*</span></label>
                                    <div class="controls">
                                        <? if ($QueryDes->GetNumRows() > 0): ?>
                                            <select  name="designation" id="designation"  class="span10 m-wrap ">
                                                <?php
                                                $sr = 1;
                                                while ($sub = $QueryDes->GetObjectFromRecord()):
                                                    ?>
                                                    <option value='<?= $sub->id ?>'><?= ucfirst($sub->name) ?></option>
                                                    <?
                                                    $sr++;
                                                endwhile;
                                                ?>	
                                            </select>
                                        <? else: ?>
                                            <input type="text" name="designation" value="" id="designation" class="span10 m-wrap validate[required]" readonly />
                                        <? endif; ?>					</div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="join_date">Join Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="join_date" value="" id="join_date" class='span10 m-wrap m-ctrl-medium new_format'/>
                                    </div>
                                </div>
                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="staff_category">Staff Category<span class="required">*</span></label>
                                    <div class="controls">
                                        <? if ($QueryCat->GetNumRows() > 0): ?>
                                            <select  name="staff_category" id="staff_category"  class="span10 m-wrap ">
                                                <?php
                                                $sr = 1;
                                                while ($cat = $QueryCat->GetObjectFromRecord()):
                                                    ?>
                                                    <option value='<?= $cat->id ?>'><?= ucfirst($cat->name) ?></option>
                                                    <?
                                                    $sr++;
                                                endwhile;
                                                ?>	
                                            </select> 
                                        <? else: ?>
                                            <input type="text" name="staff_category" value="" id="staff_category" class="span10 m-wrap validate[required]" readonly />
                                        <? endif; ?>					</div>
                                </div>	
                            </div>	
                        </div>	
                        <h4 class="form-section hedding_inner">Quit Detail</h4>
                        <div class="row-fluid">
                            <div class="span6 ">  
                                <div class="control-group">
                                    <label class="control-label" for="is_quit">Is Quit</label>
                                    <div class="controls" style='margin-top:5px;'>
                                        <input type="checkbox"  name="is_quit" id="is_quit" value="1"/>
                                    </div>
                                </div> 
                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="school_head_phone">Quit Date</label>
                                    <div class="controls">
                                        <input type="text" name="quit_date" value='' id="quit_date" class='span10 m-wrap m-ctrl-medium new_format'/>
                                    </div>
                                </div>
                            </div>	
                        </div>						
                        <h3 class="form-section"></h3>
                        <div class="control-group">
                            <label class="control-label" for="is_active">Make Active</label>
                            <div class="controls" style='margin-top:5px;'>
                                <input type="checkbox" name="is_active" id="is_active" value="1" />
                            </div>
                        </div> 
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>"  />
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('staff', 'list', 'list'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>                                         
                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        /* Add New Qualification */
        $("#add_new_ql").live("click", function () {
            var current_ql = $('#total_ql').val();
            var new_ql = parseInt(current_ql) + parseInt('1');
            $('#more_ql').append("<tr class='delete_row" + new_ql + "'><td><input type='text' name='qualification[" + new_ql + "][qualification]' class='span9 validate[required]' id='qualification" + new_ql + "'/></td><td class='hidden-phone'><input type='text' name='qualification[" + new_ql + "][institute]' class='span9 validate[required]' id='institute" + new_ql + "'/></td><td class='hidden-phone'><input type='text' name='qualification[" + new_ql + "][year_of_passing]' class='span6 validate[required,custom[onlyNumberSp]]' id='year_of_passing" + new_ql + "'/></td><td class='hidden-phone'><input type='text' name='qualification[" + new_ql + "][percentage]' class='span4 validate[required]' id='percentage" + new_ql + "'/>%&nbsp;&nbsp;<a class='btn red icn-only delete_row'><i class='icon-remove icon-white'></i></a></td></tr>");
            $('#total_ql').val(new_ql);
        });
        /* Delete Qualificatioin */
        $(".delete_row").live("click", function () {
            var current_ql = $('#total_ql').val();
            var new_ql = parseInt(current_ql) - parseInt('1');
            if (new_ql < 2)
            {
                return false;
            }
            else {
                $('.delete_row' + current_ql + '').css('display', 'none');
                $('#total_ql').val(new_ql);
            }
        });
        /* Add New experience */
        $("#add_new_ex").live("click", function () {
            var current_ex = $('#total_ex').val();
            var new_ex = parseInt(current_ex) + parseInt('1');
            var dataString = 'id=' + new_ex;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_add_staff', 'ajax_add_staff&temp=staff'); ?>",
                data: dataString,
                success: function (data, textStatus)
                {
                    $("#more_ex").append(data);
                    $('#total_ex').val(new_ex);
                }
            });
        });
        /* Delete Experience */
        $(".delete_row_ex").live("click", function () {
            var current_ex = $('#total_ex').val();
            var new_ex = parseInt(current_ex) - parseInt('1');
            if (new_ex < 2)
            {
                return false;
            }
            else {
                $('.delete_row_ex' + current_ex + '').css('display', 'none');
                $('#total_ex').val(new_ex);
            }
        });
        /* Add New Document */
        $("#add_new_doc").live("click", function () {
            var current_doc = $('#total_doc').val();
            var new_doc = parseInt(current_doc) + parseInt('1');

            $('#more_doc').append("<tr class='delete_row_doc" + new_doc + "'><td><input type='text' name='document[" + new_doc + "][document_title]' class='span10'/></td><td class='hidden-phone'><input type='file' name='file[" + new_doc + "]'/>&nbsp;&nbsp;<a class='btn red icn-only delete_row_doc'><i class='icon-remove icon-white'></i></a></td></tr>");
            $('#total_doc').val(new_doc);
        });
        /* Delete Document */
        $(".delete_row_doc").live("click", function () {
            var current_doc = $('#total_doc').val();
            var new_doc = parseInt(current_doc) - parseInt('1');
            if (new_doc < 1)
            {
                return false;
            }
            else {
                $('.delete_row_doc' + current_doc + '').css('display', 'none');
                $('#total_doc').val(new_doc);
            }
        });
        $("#same_address").live("click", function () {
            if ($('#same_address').attr('checked')) {
                $('#correspondence_address1').val($('#permanent_address1').val());
                $('#correspondence_address2').val($('#permanent_address2').val());
                $('#correspondence_city').val($('#permanent_city').val());
                $('#correspondence_state').val($('#permanent_state').val());
                $('#correspondence_country').val($('#permanent_country').val());
                $('#correspondence_zip').val($('#permanent_zip').val());
            } else {
                $('#correspondence_address1').val('');
                $('#correspondence_address2').val('');
                $('#correspondence_city').val('');
                $('#correspondence_state').val('');
                $('#correspondence_country').val('');
                $('#correspondence_zip').val('');
            }
        });
    });
</script>
<script>
    $(document).on('change', '.salary_pay_rule_heads_earning', function () {
        $('#no_record_earning').remove();
        var pay_rule_id = $(this).attr('data-id');
        var pay_rule_title = $(this).attr('data-pay_rule_title');
        if ($(this).closest('span').hasClass('checked')) {
            $('#add_pay_rule_heads_earning').append('<tr class="head_' + pay_rule_id + '"><td>' + pay_rule_title + '</td><td><input type="number" step="any" name="earning[' + pay_rule_id + '][amount]" class="span10" placeholder="0.00"/></td><td><span><label><input type="radio" name="earning[' + pay_rule_id + '][type]" value="flat" checked/> Manual</label></span></td></tr>');
//            <label><input type="radio" name="earning[' + pay_rule_id + '][type]" value="percentage"/> %age</label>
        } else {
            $('.head_' + pay_rule_id).remove();
        }
    });
    $(document).on('change', '.salary_pay_rule_heads_deduction', function () {
        $('#no_record_deduction').remove();
        var pay_rule_id = $(this).attr('data-id');
        var pay_rule_title = $(this).attr('data-pay_rule_title');
        var data_pay_rule_deduction_for = $(this).attr('data-pay_rule_deduction_for');
        if ($(this).closest('span').hasClass('checked')) {
            $('#add_pay_rule_heads_deduction').append('<tr class="head_' + pay_rule_id + '"><td>' + pay_rule_title + '</td><td><input type="hidden" name="deduction[' + pay_rule_id + '][deduction_for]" value="' + data_pay_rule_deduction_for + '"/><input type="number" step="any" name="deduction[' + pay_rule_id + '][amount]" class="span10" placeholder="0.00"/></td><td><span><label><input type="radio" name="deduction[' + pay_rule_id + '][type]" value="flat" checked/> Manual</label><label><input type="radio" name="deduction[' + pay_rule_id + '][type]" value="percentage"/> %age</label></span></td><td>' + data_pay_rule_deduction_for + '</td></tr>');
        } else {
            $('.head_' + pay_rule_id).remove();
        }
    });
</script> 