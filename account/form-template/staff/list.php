<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Staff
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   

                <li class="last">
                    List Staff
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Manage Staff</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('staff', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >		
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th class='hidden-480 sorting_disabled' style='width:1%;'>Sr. No</th>
                                    <th>Name</th>
                                    <th class='hidden-480 sorting_disabled'  style='width:1%;'>Designation</th>
                                    <th class="hidden-480 sorting_disabled">Staff Category</th>
                                    <th class="hidden-480 sorting_disabled" style='text-align:center;'>Loan</th>
                                    <th class="hidden-480 sorting_disabled" style='text-align:center;'>Salary</th>
                                    <th>Login Details</th>
                                    <th >Action</th>
                                </tr>
                            </thead>
                            <? if (!empty($record)): ?>
                                <tbody>
                                    <?php
                                    $sr = 1;
                                    foreach ($record as $s_k => $object):
                                        ?>
                                        <tr class="odd gradeX">	
                                            <td class='hidden-480 sorting_disabled'><?= $sr ?>.</td>												
                                            <td><?= $object->title . " " . $object->first_name . " " . $object->last_name ?></td>											
                                            <td class='hidden-480 sorting_disabled'><?= $object->designation ?></td>
                                            <td class="hidden-480 sorting_disabled"><?= $object->staff_category ?></td>
                                            <td>
                                                <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('loan', 'list', 'list', 'staff_id=' . $object->id) ?>" title="click here to view loan">Loan <i class="icon-arrow-right"></i></a>&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <a class="btn mini yellow icn-only tooltips" href="<?php echo make_admin_url('salary', 'list', 'list', 'staff_id=' . $object->id) ?>" title="click here to view salary">Salary <i class="icon-arrow-right"></i></a>&nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <?php if (!empty($object->staff_login_id) || !empty($object->password)) { ?>
                                                    <a id="send_login_sms" data-staff_id="<?= $object->id ?>" class="btn mini green icn-only tooltips" href="" title="Send Login Details Via SMS">SMS</a>&nbsp;&nbsp;
                                                    <a id="send_login_email" data-staff_id="<?= $object->id ?>" class="btn mini blue icn-only tooltips" href="" title="Send Login Details Via Email">Email</a>&nbsp;&nbsp;
                                                <?php } else { ?>
                                                    <span class="text-error">No Details Found</span>
                                                <?php } ?>
                                            </td>
                                            <td style="text-align:left">
                                                <a class="btn mini green icn-only tooltips" href="<?php echo make_admin_url('messages', 'compose', 'compose', 'user_id=' . $object->id . '&user_type=staff') ?>" title="click here to send message">Send Message</a>&nbsp
                                                <? if (isset($staff_icard) && (!empty($staff_icard->id))): ?>			
                                                    <a class="btn mini black icn-only tooltips" href="<?php echo make_admin_url('staff', 'view', 'view', 'id=' . $object->id . '&doc_id=' . $staff_icard->id) ?>" title="click here to view ID Card">ID Card</a>&nbsp
                                                <? endif; ?>
                                                <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('staff', 'update', 'update', 'id=' . $object->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp
                                                <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('staff', 'delete', 'delete', 'id=' . $object->id . '&delete=1') ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $sr++;
                                    endforeach;
                                    ?>
                                </tbody>
                            <?php endif; ?>  
                        </table>
                    </form>    
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<script>
    // Send Login SMS
    $(document).on('click', '#send_login_sms', function (e) {
        e.preventDefault();
        var el = $(this);
        var html = el.html();
        el.html('Sending..');
        var staff_id = $(this).attr('data-staff_id');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=staff'); ?>", {act: 'send_login_sms', staff_id: staff_id}, function (data) {
            if (data === 'success') {
                toastr.success('Sms Sent Successfully!', 'Success');
            } else if (data === 'failed') {
                toastr.error('Sms Sending Failed!', 'Failed');
            } else {
                toastr.info('Sms Service Disabled!', 'Info');
            }
            el.html(html);
        });
    });
    // Send Login Email
    $(document).on('click', '#send_login_email', function (e) {
        e.preventDefault();
        var el = $(this);
        var html = el.html();
        el.html('Sending..');
        var staff_id = $(this).attr('data-staff_id');
        $.post("<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=staff'); ?>", {act: 'send_login_email', staff_id: staff_id}, function (data) {
            if (data === 'success') {
                toastr.success('Email Sent Successfully!', 'Success');
            } else if (data === 'failed') {
                toastr.error('Email Sending Failed!', 'Failed');
            } else {
                toastr.info('Email Service Disabled!', 'Info');
            }
            el.html(html);
        });
    });
</script>
