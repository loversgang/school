<div class="tile bg-purple <?php echo ($section == 'list1') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('staff', 'list1', 'list1'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                Struck Off Staff List
            </div>
        </div>
    </a>   
</div>
<div class="tile bg-grey <?php echo ($section == 'list' && $active == '0') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('staff', 'list', 'list&active=0'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                Non-Active Staff
            </div>
        </div>
    </a>   
</div>
<div class="tile bg-green <?php echo ($section == 'list') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('staff', 'list', 'list'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-list"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                List Active Staff
            </div>
        </div>
    </a>   
</div>
<div class="tile bg-blue <?php echo ($section == 'insert') ? 'selected' : '' ?>">
    <a href="<?php echo make_admin_url('staff', 'insert', 'insert'); ?>">
        <div class="corner"></div>

        <div class="tile-body">
            <i class="icon-plus"></i>
        </div>
        <div class="tile-object">
            <div class="name">
                Add New Staff
            </div>
        </div>
    </a> 
</div>