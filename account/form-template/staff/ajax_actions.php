<?php

include_once(DIR_FS_SITE . 'include/functionClass/emailsClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/schoolSmsEmailClass.php');
include_once(DIR_FS_SITE . 'include/config_account/constant_account.php');
if (isset($_POST['act'])) {
    extract($_POST);
    if ($act == 'send_login_sms') {
        #Send and Add Entry to sms counter if set in setting
        if ($school_setting->is_sms_allowed == '1') {
            $obj = new staff;
            $staff = $obj->getStaff($staff_id);

            $arr['NAME'] = $staff->first_name . ' ' . $staff->last_name;
            $arr['ID'] = $staff->staff_login_id;
            $arr['PASS'] = $staff->password;

            $msg = get_database_msg_only('21', $arr);
            $POST['school_id'] = $school->id;
            $POST['sms_type'] = 'login_details';
            //$POST['student_id'] = $student_id;
            $POST['type'] = 'sms';
            $POST['on_date'] = date('Y-m-d');
            $POST['to_number'] = $staff->mobile;
            $POST['sms_text'] = $msg;
            $QuerySave = new schoolSmsEmailHistory();
            $sms_history_id = $QuerySave->saveData($POST);

            // Send SMS
            $send_sms = send_sms($staff->mobile, $msg);
            if ($send_sms == '1') {
                $sms_arr['id'] = $sms_history_id;
                $sms_arr['sms_send_status'] = '1';
                $sms_arr['sms_send_date'] = date('Y-m-d');
                $QueryUpd = new schoolSmsEmailHistory();
                $QueryUpd->saveData($sms_arr);
                echo "success";
            } else {
                echo "failed";
            }
        } else {
            echo "disabled";
        }
    }

    if ($act == 'send_login_email') {
        #Send and Add Entry to email counter if set in setting
        if ($school_setting->is_email_allowed == '1') {
            $obj = new staff;
            $staff = $obj->getStaff($staff_id);

            $arr['NAME'] = $staff->first_name . ' ' . $staff->last_name;
            $arr['ID'] = $staff->staff_login_id;
            $arr['PASS'] = $staff->password;
            $arr['EMAIL'] = $school->email_address;
            $arr['SITE_NAME'] = SITE_NAME;
            $msg = get_database_msg('22', $arr);
            $subject = get_database_msg_subject('22', $arr);
            $POST['school_id'] = $school->id;
            $POST['email_type'] = 'login_details';
            $POST['type'] = 'email';
            $POST['on_date'] = date('Y-m-d');
            $POST['email_subject'] = $subject;
            $POST['to_email'] = $staff->email;
            $POST['email_text'] = $msg;
            $POST['from_email'] = $school->email_address;
            $POST['email_school_name'] = $school->school_name;
            $QuerySave = new schoolSmsEmailHistory();
            $email_history_id = $QuerySave->saveData($POST);

            // Send Login Email
            $send_email = send_email_by_cron($staff->email, $msg, $subject, $school->school_name);
            if ($send_email == '1') {
                $email_arr['id'] = $email_history_id;
                $email_arr['email_send_status'] = '1';
                $email_arr['sms_email_date'] = date('Y-m-d');
                $QueryUpd = new schoolSmsEmailHistory();
                $QueryUpd->saveData($email_arr);
                echo "success";
            } else {
                echo "failed";
            }
        } else {
            echo "disabled";
        }
    }
}    