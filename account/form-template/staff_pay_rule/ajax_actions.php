<?php
include_once(DIR_FS_SITE . 'include/functionClass/staffClass.php');

if (isset($_POST['action'])) {
    extract($_POST);
    if ($action == 'show_all_earnings') {
        $query = new staffPayHeads;
        $earnings = $query->all_earnings();

        ?>
        <div class="control-group">
            <label class="control-label" for="title">All Earnings<span class="required">*</span></label>
            <div class="controls">
                <select name="earning_deduction" class="span6">
                    <?php foreach ($earnings as $earning) { ?>
                        <option value="<?php echo $earning ?>"><?php echo $earning ?></option>
                    <?php } ?>
                </select>
            </div>
        </div> 
        <?php
    }
}
?>