<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Pay Rules
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('staff_pay_rule', 'list', 'list'); ?>">List Pay Rules</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Add New Rule
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
    </div>	            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal validation" action="<?php echo make_admin_url('staff_pay_rule', 'insert', 'insert') ?>" method="POST" enctype="multipart/form-data" id="validation">	
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Add New Pay Rule</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="control-group">
                            <label class="control-label" for="title">Type<span class="required">*</span></label>
                            <div class="controls">
                                <select name="type" class="span6" id="ear_ded">
                                    <option value="earning">Earning</option>
                                    <option value="deduction">Deduction</option>
                                </select>
                            </div>
                        </div>  
                        <div id="deduction_click"></div>
                        <div class="control-group">
                            <label class="control-label" for="title">Title<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="title"  value="" id="title" class="span6 m-wrap validate[required]" />
                            </div>
                        </div>        
                        <div class="form-actions">
                            <input type="hidden" name="school_id" value="<?= $school->id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('staff_pay_rule'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>

                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<script>
    $(document).on('change', '#ear_ded', function () {
        $('#deduction_click').show();
        if ($(this).val() === 'deduction') {
            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=staff_pay_rule'); ?>', {action: 'show_all_earnings'}, function (data) {
                $('#deduction_click').html(data);
            });
        } else {
            $('#deduction_click').hide();
        }
    });
</script>