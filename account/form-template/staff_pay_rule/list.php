<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Pay Rules
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li class="last">
                    List Pay Rules
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Manage Pay Rules</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">	
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th class='hidden-480 sorting_disabled' style='width:8%;'>Sr. No</th>
                                <th>Type</th>
                                <th class='hidden-480 sorting_disabled'>Title</th>
                                <th>Deduction from Earnings</th>
                                <th >Action</th>
                            </tr>
                        </thead>
                        <? if (!empty($payHeads)): ?>
                            <tbody>
                                <?php
                                $sr = 1;
                                foreach ($payHeads as $payHead):
                                    ?>
                                    <tr class="odd gradeX">	
                                        <td class='hidden-480 sorting_disabled'><?= $sr ?>.</td>
                                        <td><?= ucfirst($payHead->type); ?></td>
                                        <td><?= ucfirst($payHead->title); ?></td>
                                        <td><?= ucfirst($payHead->earning_deduction); ?></td>
                                        <td>
                                            <a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('staff_pay_rule', 'update', 'update', 'id=' . $payHead->id) ?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp
                                            <a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('staff_pay_rule', 'delete', 'delete', 'id=' . $payHead->id . '&delete=1') ?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record"><i class="icon-remove icon-white"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    $sr++;
                                endforeach;
                                ?>
                            </tbody>
                        <?php endif; ?>  
                    </table>    
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>