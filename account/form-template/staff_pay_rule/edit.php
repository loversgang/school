<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3 class="page-title">
                Manage Pay Rules
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-user"></i>
                    <a href="<?php echo make_admin_url('staff_pay_rule', 'list', 'list'); ?>">List Pay Rules</a>
                    <i class="icon-angle-right"></i>                                       
                </li>
                <li class="last">
                    Edit Rule
                </li>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?> 
    </div>	            
    <div class="clearfix"></div>
    <?php
    display_message(1);
    $error_obj->errorShow();
    ?>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <form class="form-horizontal validation" action="<?php echo make_admin_url('staff_pay_rule', 'update', 'update', 'id=' . $payHead->id) ?>" method="POST" enctype="multipart/form-data" id="validation">	
            <div class="span12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-user"></i>Edit Rule</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="control-group">
                            <label class="control-label" for="title">Type<span class="required">*</span></label>
                            <div class="controls">
                                <select name="type" class="span6" id="ear_ded" disabled>
                                    <option value="earning" <?php echo $payHead->type == 'earning' ? 'selected' : ''; ?>>Earning</option>
                                    <option value="deduction" <?php echo $payHead->type == 'deduction' ? 'selected' : ''; ?>>Deduction</option>
                                </select>
                            </div>
                        </div>
                        <?php if ($payHead->type !== 'earning') { ?>
                            <div class="control-group" id="deduction_click">
                                <label class="control-label" for="title">All Earnings<span class="required">*</span></label>
                                <div class="controls">
                                    <select name="earning_deduction" id="earning_deduction" class="span6">
                                        <?php foreach ($earnings as $earning) { ?>
                                            <option <?php echo ($payHead->earning_deduction == $earning) ? 'selected' : '' ?> value="<?php echo $earning ?>"><?php echo $earning ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> 
                        <?php } ?>
                        <div id="deduction_click"></div>
                        <div class="control-group">
                            <label class="control-label" for="title">Title<span class="required">*</span></label>
                            <div class="controls">
                                <input type="text" name="title"  value="<?php echo $payHead->title; ?>" id="title" class="span6 m-wrap validate[required]" />
                            </div>
                        </div>        
                        <div class="form-actions">
                            <input type="hidden" name="id" value="<?= $payHead->id ?>" tabindex="7" /> 	
                            <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                            <a href="<?php echo make_admin_url('staff_pay_rule'); ?>" class="btn" name="cancel" > Cancel</a>
                        </div>
                    </div> 
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
<script>
    $(document).on('change', '#ear_ded', function () {
        $('#deduction_click').show();
        if ($(this).val() === 'deduction') {
            $.post('<?= make_admin_url_window('ajax_calling', 'ajax_actions', 'ajax_actions&temp=staff_pay_rule'); ?>', {action: 'show_all_earnings'}, function (data) {
                $('#deduction_click').html(data);
            });
        } else {
            $('#deduction_click').hide();
        }
        if ($('#ear_ded').val() === 'earning') {
            $('#earning_deduction').val('');
        }
    });
</script>