

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Examinations Groups
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>                                   

                                    <li class="last">
                                        All Examinations Groups
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
						<div class="tile bg-grey <?php echo ($section=='add_group')?'selected':''?>">
							<a href="<?php echo make_admin_url('exam', 'add_group', 'add_group');?>">
								<div class="corner"></div>

								<div class="tile-body">
										<i class="icon-plus-sign"></i>
								</div>
								<div class="tile-object">
										<div class="name">
												Add New Group
										</div>
								</div>
							</a>   
						</div>				
							<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>  
				</div>            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->

            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-list"></i>Manage Examinations Groups</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
															<form action="<?php echo make_admin_url('exam', 'group', 'group')?>" method="GET" id='session_filter'>
															<input type='hidden' name='Page' value='exam'/>
															<input type='hidden' name='action' value='group'/>
															<input type='hidden' name='section' value='group'/>															
																<div class="row-fluid">
																<div class='span5'>															
																	<label class="control-label">Select Session</label>
																	<div class="controls">
																		<select class="select2_category session_filter span8" data-placeholder="Select Session Students" name='session_id' >
																			<option value="">Select Session</option>
																			<?php $session_sr=1;foreach($Session_list as $s_k=>$session): ?>
																					<option value='<?=$session->session_id?>' <? if($session->session_id==$session_id){ echo 'selected'; }?> ><?=ucfirst($session->session_name)?></option>
																			<? $session_sr++; endforeach;?>
																		</select>
																	</div>	
																</div>
																<div class='span7'></div>
																<div class="clearfix"></div>
																</div>
																<div class="clearfix"></div>																
															</form>								
							<form action="<?php echo make_admin_url('exam', 'update2', 'update2');?>" method="post" id="form_data" name="form_data" >	
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
												<th>Examination Title</th>
												<th class="hidden-480">Session Name</th>
												<th class="hidden-480" style='text-align:center'>Section</th>
												<th class="hidden-480" style='text-align:center'>From Date</th>
												<th class="hidden-480" style='text-align:center'>To Date</th>
												<th class="hidden-480" style='text-align:center;' >Action</th>
										</tr>
									</thead>
                                        <? if(!empty($result)):?>
										<tbody>
                                            <?php $sr=1; foreach($result as $key=>$object_1):?>
                                                <tr class="odd gradeX">
													<td><?=$object_1->title?></td>
													<td class="hidden-480"><?=$object_1->session_name?></td>
													<td style='text-align:center' class="hidden-480"><?=$object_1->section?></td>
													<td style='text-align:center' class="hidden-480"><?=$object_1->from_date?></td>
													<td style='text-align:center' class="hidden-480"><?=$object_1->to_date?></td>
													<td style='text-align:right;'>
													<a class="btn mini black icn-only tooltips" href="<?php echo make_admin_url('exam', 'student', 'student', 'id='.$object_1->id)?>" title="click here to view students & report Card">Students & Report Cards</a>&nbsp;&nbsp;
													<a class="btn mini blue icn-only tooltips" href="<?php echo make_admin_url('exam', 'edit_group', 'edit_group', 'id='.$object_1->id)?>" title="click here to edit this record"><i class="icon-edit icon-white"></i></a>&nbsp;&nbsp;
													<a class="btn mini red icn-only tooltips" href="<?php echo make_admin_url('exam', 'delete_group', 'delete_group', 'id='.$object_1->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this exam group.');" title="click here to delete this group"><i class="icon-remove icon-white"></i></a>
													</td>
																									
												</tr>
                                            <?php $sr++;
												endforeach;?>
										</tbody>
									   <?php endif;?>  
								</table>
                            </form>    
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
<!-- END PAGE CONTAINER-->    
<script type="text/javascript">
	$(".session_filter").live("change", function(){
			var session_id=$(this).val();	
				$('#session_filter').submit();				
	});
</script>	


