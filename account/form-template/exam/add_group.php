

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Examinations
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('exam', 'group', 'group'); ?>">List Groups</a> 
                    <i class="icon-angle-right"></i>
                </li>   
                <li class="last">
                    Add New Group  
                </li>

            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>

    <div class="row-fluid">
        <div class="span12">
            <div id="form_wizard_1" class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-reorder"></i> Examination Wizard - <span class="step-title">Step 1 of 2</span>
                    </div>
                </div>
                <div class="portlet-body form">

                    <div class="form-wizard">
                        <!-- main Head -->
                        <div class="navbar steps">
                            <div class="navbar-inner">
                                <ul class="row-fluid nav nav-pills">
                                    <li class="span4 <?= ($type == 'select') ? 'active' : ''; ?>">
                                        <a class="step <?= ($type == 'select') ? 'active' : ''; ?>" >
                                            <span class="number">1</span>
                                            <span class="desc"><i class="icon-ok"></i> Select Session & Section</span>   
                                        </a>
                                    </li>
                                    <li class="span6 <?= ($type == 'group') ? 'active' : ''; ?>">
                                        <a class="step <?= ($type == 'group') ? 'active' : ''; ?>" >
                                            <span class="number">2</span>
                                            <span class="desc"><i class="icon-ok"></i> Examination Group Detail</span>   
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <!-- main Head -->
                        <div class="progress progress-success progress-striped" id="bar">
                            <div class="bar" style="width: <?= $per ?>;"></div>
                        </div>
                        <div class="tab-content">
                            <!-- First Step -->
                            <div id="tab1" class="tab-pane <?= ($type == 'select') ? 'active' : ''; ?>">
                                <form action="<?php echo make_admin_url('exam', 'add_group', 'add_group') ?>" method="GET" enctype="multipart/form-data" id="validation">
                                    <input type='hidden' name='Page' value='exam'/>
                                    <input type='hidden' name='action' value='add_group'/>
                                    <input type='hidden' name='section' value='add_group'/>
                                    <input type='hidden' name='type' value='group'/>


                                    <h3 class="block">Select Session & Section</h3>
                                    <div class="alert alert-block alert-success">
                                        <div class="row-fluid">
                                            <div class="span6">								
                                                <div class="control-group">
                                                    <label class="control-label">Current Session</label>
                                                    <div class="controls">
                                                        <select class="select2_category session_filter span9" data-placeholder="Select Session Students" name='id' id='session_id'>
                                                            <option value="">Select Session</option>
                                                            <?php $session_sr = 1;
                                                            while ($session = $QuerySession->GetObjectFromRecord()): ?>
                                                                <option value='<?= $session->id ?>' <? if ($session->id == $id) {
                                                                echo 'selected';
                                                            } ?> ><?= ucfirst($session->title) ?></option>
    <? $session_sr++;
endwhile; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span6" id='dynamic_model'>								

                                            </div>																


                                        </div>
                                    </div>	
                                    <div class="form-actions clearfix" >
                                        <a href="<?php echo make_admin_url('exam', 'group', 'group'); ?>" class="btn red" name="cancel" > Cancel</a>
                                        <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 

                                    </div>
                                </form>			
                            </div>
                            <!-- First Step End -->


                            <!-- Second Step -->
                            <div id="tab2" class="tab-pane <?= ($type == 'group') ? 'active' : ''; ?>">
                                <form class="form-horizontal" action="<?php echo make_admin_url('exam', 'add_group', 'add_group&id=' . $id . '&ct_sect=' . $ct_sect . '&type=exam') ?>" method="POST" enctype="multipart/form-data" id="validation">

                                    <h3 class="block alert alert-info"><?= $object->title . "<font class='green_color'> > </font>" . " Section " . $ct_sect ?></h3>
                                    <h4 class="form-section hedding_inner">Examination Group Detail</h4>													
                                    <div class="row-fluid">					
                                        <div class="control-group span6">
                                            <label class="control-label" for="title"> Title<span class="required">*</span></label>
                                            <div class="controls">
                                                <input type="text" name="title" value="" id="exam" class="span10 m-wrap validate[required]"/>
                                            </div>
                                        </div> 
                                    </div> 		
                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="remarks">Remarks</label>
                                                <div class="controls">
                                                    <textarea type="text" name="remarks" rows='3' value="" id="remarks" class="span10 m-wrap"></textarea>
                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="from_date">Intimation SMS</span></label>
                                                <div class="controls">
                                                    <label class="radio line">
                                                        <span>
                                                            <input type="radio" value="1" name="intimation_sms" >
                                                        </span>		Yes	
                                                    </label>
                                                    <label class="radio line">
                                                        <span>
                                                            <input type="radio" value="0" checked name="intimation_sms" >
                                                        </span>		No	
                                                    </label>																		
                                                </div>
                                            </div> 	

                                        </div>
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="from_date">From Date<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="from_date" value="" id="from_date" class="span10 m-wrap new_format validate[required]" />
                                                </div>
                                            </div> 	

                                            <div class="control-group">
                                                <label class="control-label" for="to_date">To Date<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="to_date" value="" id="to_date" class="span10 m-wrap new_format validate[required]" />
                                                </div>
                                            </div>
                                            <div class="control-group">
                                                <label class="control-label" for="from_date">Term Exam</span></label>
                                                <div class="controls">
                                                    <label class="radio line">
                                                        <span>
                                                            <input type="radio" value="1" name="term_exam" >
                                                        </span>		Yes	
                                                    </label>
                                                    <label class="radio line">
                                                        <span>
                                                            <input type="radio" value="0" checked name="term_exam" >
                                                        </span>		No	
                                                    </label>																		
                                                </div>
                                            </div>																
                                        </div>															

                                    </div>	
                                    <div class="row-fluid">	
                                        <div class="control-group span8">
                                            <label class="control-label" for="phone">Select Examinations<span class="required">*</span></label>
                                            <div class="controls">
                                                <table class="table table-striped table-bordered table-advance table-hover" id="sample">
                                                    <thead>
                                                        <tr><th><input type="checkbox" class="group-checkable master_checker" data-set="#sample.checkboxes" /></th> 
                                                            <th class="hidden-480 sorting_disabled" style='background:#DDDDDD;width:50%;'>Exam Title</th>
                                                            <th class="hidden-480 sorting_disabled" style='background:#DDDDDD;width:30%;'>Subject</th>
                                                            <th class="hidden-480 sorting_disabled" style='background:#DDDDDD;width:20%;'>Date</th>
                                                        </tr></thead>
                                                    <tbody id='ajex_exam_table'>
<? if (!empty($result)): ?>
                                                            <?php $sr_ex = 1;
                                                            foreach ($result as $e_k => $ex): ?>
                                                                <tr>
                                                                    <td><input type="checkbox" class="checkboxes sub_checkbox" value="<?= $ex->id ?>" name='exam[]'>												
                                                                    <td><?= ucfirst($ex->title) ?></td><td><?= ucfirst($ex->subject) ?></td><td><?= ucfirst($ex->date) ?></td>
                                                                    </td>	
                                                                </tr>	
        <? $sr_ex++;
    endforeach; ?>
<? endif; ?>																		
                                                    </tbody>
                                                </table>																		
                                            </div> 	

                                        </div>
                                    </div>

                                    <input type='hidden' name='type' value='result'/>
                                    <input type='hidden' name='session_id' id='session' value='<?= $id ?>'/>
                                    <input type='hidden' name='section' id='ct_sec' value='<?= $ct_sect ?>'/>
                                    <input type='hidden' name='school_id' value='<?= $school->id ?>'/>												
                                    <div class="form-actions clearfix" >
                                        <a href="<?php echo make_admin_url('exam', 'add_group', 'add_group'); ?>" class="btn red" name="cancel" > Cancel</a>
                                        <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 

                                    </div>
                            </div>	
                            </form>	
                        </div>
                        <!-- Second Step End-->

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>																



<div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    

<script type="text/javascript">
    $(".session_filter").change(function () {
        var session_id = $("#session_id").val();
        if (session_id.length > 0) {
            var id = session_id;
            $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'id=' + id;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section&temp=exam'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#dynamic_model").html(data);
                }
            });
        }
        else {
            return false;
        }
    });

    $("#from_date,#to_date").live('change', function () {
        var session_id = $("#session").val();
        var ct_sec = $("#ct_sec").val();
        var from = $("#from_date").val();
        var to = $("#to_date").val();

        if (session_id.length > 0) {
            var id = session_id;
            $("#ajex_exam_table").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'session_id=' + session_id + '&ct_sec=' + ct_sec + '&from=' + from + '&to=' + to;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_exam', 'ajax_section_exam&temp=exam'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_exam_table").html(data);
                }
            });
        }
        else {
            return false;
        }
    });

    $(".master_checker").click(function () {
        var ct_val = $(this).val();

        if (!$(this).is(':checked')) {
            $(".sub_checkbox").each(function () {
                $(this).removeAttr("checked");
                $(this).parent().removeClass('checked');
            });
        }
        else
        {
            $(".sub_checkbox").each(function () {
                $(this).attr("checked", true);
                $(this).parent().addClass('checked');
            });
        }
    });

</script> 
