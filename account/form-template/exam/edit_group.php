
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Examinations
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('exam', 'group', 'group'); ?>">List Groups</a> 
                    <i class="icon-angle-right"></i>
                </li>   
                <li class="last">
                    Edit Examination Group  
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->


    <div class="row-fluid">
        <form class="form-horizontal" action="<?php echo make_admin_url('exam', 'edit_group', 'edit_group') ?>" method="POST" enctype="multipart/form-data" id="validation">
            <!-- / Box -->
            <div class="span12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption"><i class="icon-paste"></i>Edit Examination Group</div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                        </div>
                    </div>
                    <div class="portlet-body form">      
                        <!-- Examination Info  -->
                        <h3 class="block alert alert-info"><?= $object->title . "<font class='green_color'> > </font>" . " Section " . $ct_sect ?></h3>
                        <h4 class="form-section hedding_inner">Examination Group Detail</h4>
                        <div class="row-fluid">					
                            <div class="control-group span6">
                                <label class="control-label" for="title"> Title<span class="required">*</span></label>
                                <div class="controls">
                                    <input type="text" name="title" value="<?= $ExamGroup->title ?>" id="exam" class="span10 m-wrap validate[required]"/>
                                </div>
                            </div> 
                        </div> 		
                        <div class="row-fluid">
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="remarks">Remarks</label>
                                    <div class="controls">
                                        <textarea type="text" name="remarks" id="remarks" class="span10 m-wrap"><?= $ExamGroup->remarks ?> </textarea>
                                    </div>
                                </div>
                                <!--	
                                        <div class="control-group">
                                        <label class="control-label" for="from_date">Result SMS</span></label>
                                                        <div class="controls">
                                                                <label class="radio line">
                                                                        <span>
                                                                        <input type="radio" value="1" name="send_sms" <?= ($ExamGroup->send_sms == '1') ? 'checked' : ''; ?> >
                                                                        </span>		Yes	
                                                                </label>
                                                                <label class="radio line">
                                                                        <span>
                                                                        <input type="radio" value="0"  name="send_sms" <?= ($ExamGroup->send_sms == '0') ? 'checked' : ''; ?> >
                                                                        </span>		No	
                                                                </label>																		
                                                        </div>
                                        </div> 
                                -->															

                            </div>
                            <div class="span6 ">
                                <div class="control-group">
                                    <label class="control-label" for="from_date">From Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="from_date" value="<?= $ExamGroup->from_date ?>" id="from_date" class="span10 m-wrap new_format validate[required]" />
                                    </div>
                                </div> 	

                                <div class="control-group">
                                    <label class="control-label" for="to_date">To Date<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="to_date" value="<?= $ExamGroup->to_date ?>" id="to_date" class="span10 m-wrap new_format validate[required]" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="from_date">Term Exam</span></label>
                                    <div class="controls">
                                        <label class="radio line">
                                            <span>
                                                <input type="radio" value="1" <?= ($ExamGroup->term_exam == '1') ? 'checked' : ''; ?> name="term_exam" >
                                            </span>		Yes	
                                        </label>
                                        <label class="radio line">
                                            <span>
                                                <input type="radio" value="0" <?= ($ExamGroup->term_exam == '0') ? 'checked' : ''; ?> name="term_exam" >
                                            </span>		No	
                                        </label>																		
                                    </div>
                                </div>															
                            </div>															

                        </div>	
                        <? $selected_exam = explode(',', $ExamGroup->exam_id); ?>
                        <div class="row-fluid">	
                            <div class="control-group span8">
                                <label class="control-label" for="phone">Select Examinations<span class="required">*</span></label>
                                <div class="controls">
                                    <table class="table table-striped table-bordered table-advance table-hover" id="sample">
                                        <thead>
                                            <tr><th><input type="checkbox" class="group-checkable master_checker" data-set="#sample.checkboxes" /></th> 
                                                <th class="hidden-480 sorting_disabled" style='background:#DDDDDD;'>Exam Title</th>
                                                <th class="hidden-480 sorting_disabled" style='background:#DDDDDD;'>Subject</th>
                                                <th class="hidden-480 sorting_disabled" style='background:#DDDDDD;'>Date</th>
                                            </tr></thead><tbody></tbody>
                                        <tbody id='ajex_exam_table'>

                                            <? if (!empty($result)): ?>
                                                <?php
                                                $sr_ex = 1;
                                                foreach ($result as $e_k => $ex):
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" class="checkboxes sub_checkbox" value="<?= $ex->id ?>" name='exam[]' <? if (in_array($ex->id, $selected_exam)) {
                                                        echo "checked";
                                                    } ?>>												
                                                        </td>
                                                        <td><?= ucfirst($ex->title) ?></td>
                                                        <td><?= ucfirst($ex->subject) ?></td>
                                                        <td><?= ucfirst($ex->date) ?></td>
                                                    </tr>	
        <? $sr_ex++;
    endforeach; ?>
<? endif; ?>																		
                                        </tbody>
                                    </table>																		
                                </div> 	

                            </div>
                        </div>

                        <input type='hidden' name='id' value='<?= $id ?>'/>
                        <input type='hidden' id='session' value='<?= $ExamGroup->session_id ?>'/>
                        <input type='hidden' id='ct_sec' value='<?= $ExamGroup->section ?>'/>
                        <input type='hidden' name='school_id' value='<?= $school->id ?>'/>												
                        <div class="form-actions clearfix" >
                            <a href="<?php echo make_admin_url('exam', 'group', 'group'); ?>" class="btn red" name="cancel" > Cancel</a>
                            <input class="btn green" type="submit" name="submit" value="Submit" tabindex="7" /> 

                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="clearfix"></div>

    </div>

    <div class="clearfix"></div>             
</div>
<!-- END PAGE CONTAINER-->    

<script type="text/javascript">
    $(".master_checker").click(function () {
        var ct_val = $(this).val();

        if (!$(this).is(':checked')) {
            $(".sub_checkbox").each(function () {
                $(this).removeAttr("checked");
                $(this).parent().removeClass('checked');
            });
        }
        else
        {
            $(".sub_checkbox").each(function () {
                $(this).attr("checked", true);
                $(this).parent().addClass('checked');
            });
        }
    });

    $("#from_date,#to_date").live('change', function () {
        var session_id = $("#session").val();
        var ct_sec = $("#ct_sec").val();
        var from = $("#from_date").val();
        var to = $("#to_date").val();
<? if (isset($selected_exam)): ?>
            var selected_exam = '<?= implode(',', $selected_exam) ?>';
<? else: ?>
            var selected_exam = '';
<? endif; ?>
        if (session_id.length > 0) {
            var id = session_id;
            $("#ajex_exam_table").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

            var dataString = 'session_id=' + session_id + '&ct_sec=' + ct_sec + '&from=' + from + '&to=' + to + '&selected_exam=' + selected_exam;
            $.ajax({
                type: "POST",
                url: "<?= make_admin_url_window('ajax_calling', 'ajax_section_exam', 'ajax_section_exam&temp=exam'); ?>",
                data: dataString,
                success: function (data, textStatus) {
                    $("#ajex_exam_table").html(data);
                }
            });
        }
        else {
            return false;
        }
    });
</script> 	