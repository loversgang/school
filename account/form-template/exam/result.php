
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <div class="hidden-print" >
			<!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Examinations Groups
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 
                                    <li>
                                            <i class="icon-list"></i>
                                            <a href="<?php echo make_admin_url('exam', 'group', 'group');?>">All Examinations Groups</a> 
                                            <i class="icon-angle-right"></i>
                                    </li> 									
                                    <li>
                                            <i class="icon-book"></i>
                                            <a href="<?php echo make_admin_url('exam', 'student', 'student&id='.$id);?>"><?=$group->title?></a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li class="last">
                                        Result
                                    </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
	
					<div class="tile bg-purple <?php echo ($section=='result')?'selected':''?>" id='print_document'>							
						<a class="hidden-print" href="<?=make_admin_url('exam','student','student&id='.$id)?>">
							<div class="corner"></div>
							<div class="tile-body"><i class="icon-arrow-left"></i></div>
							<div class="tile-object"><div class="name">Back To Exam</div></div>
						</a>
					</div>	
					<div class="tile bg-green" id='print_document'>			
						<a class="hidden-print" href="<?=make_admin_url('exam','confirm_print_group','confirm_print_group&id='.$id.'&st_id='.$st_id.'&doc_id='.$doc_id)?>">
							<div class="corner"></div>
							<div class="tile-body"><i class="icon-briefcase"></i></div>
							<div class="tile-object"><div class="name">Confirm Printing</div></div>
						</a>
					</div>						
					<div class="tile bg-yellow <?php echo ($section=='print')?'selected':''?>" id='print_document'>							
						<a class="hidden-print" onclick="javascript:window.print();">
							<div class="corner"></div>
							<div class="tile-body"><i class="icon-print"></i></div>
							<div class="tile-object"><div class="name">Print</div></div>
						</a>
					</div>						
				</div>            
           <div class="clearfix"></div>

            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
         </div>   
           <br/>	

           <div class="clearfix"></div>
			 
			<div class="row-fluid">					
				<?=html_entity_decode($content)?>	
			</div>
    <!-- END PAGE CONTAINER-->   
<br/><br/><br/>	
	</div>