<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Examinations
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('exam', 'list', 'list'); ?>">All Examination</a> 
                    <i class="icon-angle-right"></i>
                </li>   

                <li class="last">
                    View Examinations Result
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i><?= $object->title . "&nbsp; >>&nbsp;  " . " Section " . $exam->section ?> &nbsp; >>&nbsp; <?= $exam->title ?>&nbsp;  >>&nbsp;  Result</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <form action="<?php echo make_admin_url('exam', 'update2', 'update2'); ?>" method="post" id="form_data" name="form_data" >	
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr>											
                                    <th class="hidden-480" style='vertical-align:top;'>Roll No.</th>
                                    <th class="hidden-480 sorting_disabled" style='vertical-align:top;'>Name</th>
                                    <th class="hidden-480 sorting_disabled" style='vertical-align:top;'>Father Name</th>
                                    <th class="hidden-480 sorting_disabled" style='text-align:center;'>Minimum Marks</th>
                                    <th class="hidden-480 sorting_disabled" style='text-align:center;'>Marks Obtained</th>
                                    <th class="hidden-480 sorting_disabled" style='text-align:center;'>Maximum Marks</th>
                                    <th class="hidden-480 sorting_disabled" style='text-align:center;'>Percentage</th>
                                    
                                    <? if (!empty($all_grade)): ?>
                                        <th class="hidden-480 sorting_disabled" style='text-align:center;'>Grade</th>
<? endif; ?>
                                    <th class="hidden-480 sorting_disabled" style='text-align:center;'>Action</th>
                                </tr>
                            </thead>
                                <? if (!empty($result)): ?>
                                <tbody>
    <?php $sr = 1;
    foreach ($result as $key => $obj): ?>
                                        <tr class="odd gradeX">												
                                            <td class="hidden-480 main_title"><?= $obj->roll_no ?></td>
                                            <td class="hidden-480 main_title"><?= $obj->first_name . " " . $obj->last_name ?></td>
                                            <td class='main_title'><?= ucfirst($obj->father_name) ?></td>
                                            <td style='text-align:center' class="hidden-480"><?= $obj->minimum_marks ?></td>
                                            <td style='text-align:center' class="hidden-480"><?= $obj->marks_obtained ?></td>
                                            <td style='text-align:center' class="hidden-480"><?= $obj->maximum_marks ?></td>
                                            <td style='text-align:center' class="hidden-480"><?= number_format(($obj->marks_obtained / $obj->maximum_marks) * 100, 2) ?>%</td>
                                            <?
                                            if (!empty($all_grade)): $grade = '';
                                                foreach ($all_grade as $kg => $kv):
                                                    if (((($obj->marks_obtained / $obj->maximum_marks) * 100) >= $kv->minimum) && ((($obj->marks_obtained / $obj->maximum_marks) * 100) < $kv->maximum)): $grade = $kv->title;
                                                    endif;
                                                endforeach;
                                                ?>
                                                <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $grade ?></td>
        <? endif; ?>							
                                            <td style='text-align:right;'>
                                        <? if (isset($dmc) && (!empty($dmc->id))): ?>
                                                    <a class="btn mini black icn-only tooltips" href="<?php echo make_admin_url('exam', 'print', 'print', 'id=' . $id . '&st_id=' . $obj->student_id . '&doc_id=' . $dmc->id) ?>" title="click here to view Report Card">Report Card</a>&nbsp;&nbsp;
                                    <? endif; ?>
                                            </td>											
                                        </tr>
        <?php $sr++;
    endforeach;
    ?>
                                </tbody>
<?php endif; ?>  
                        </table>
                    </form>    
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



