		<div class="tile bg-yellow <?php echo ($section=='grade')?'selected':''?>">
			<a href="<?php echo make_admin_url('grade', 'list', 'list');?>">
				<div class="corner"></div>

				<div class="tile-body">
						<i class="icon-plus"></i>
				</div>
				<div class="tile-object">
						<div class="name">
								Manage Grading
						</div>
				</div>
			</a>   
		</div>	
		<div class="tile bg-purple <?php echo ($section=='group')?'selected':''?>">
			<a href="<?php echo make_admin_url('exam', 'group', 'group');?>">
				<div class="corner"></div>

				<div class="tile-body">
						<i class="icon-list"></i>
				</div>
				<div class="tile-object">
						<div class="name">
								Exam Groups
						</div>
				</div>
			</a>   
		</div>	

        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('exam', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                All Examinations
                        </div>
                </div>
            </a>   
        </div>

        <div class="tile bg-blue <?php echo ($section=='insert')?'selected':''?>">
            <a href="<?php echo make_admin_url('exam', 'insert', 'insert');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                New Examination
                        </div>
                </div>
            </a> 
        </div>

