

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Examinations Groups
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li> 
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('exam', 'group', 'group'); ?>">All Examinations Groups</a> 
                    <i class="icon-angle-right"></i>
                </li> 									

                <li class="last">
                    <?= $group->title ?>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <div class="tile bg-purple <?php echo ($section == 'student') ? 'selected' : '' ?>">
            <a href="<?php echo make_admin_url('exam', 'group', 'group'); ?>">
                <div class="corner"></div>

                <div class="tile-body">
                    <i class="icon-arrow-left"></i>
                </div>
                <div class="tile-object">
                    <div class="name">
                        Back To Groups
                    </div>
                </div>
            </a>   
        </div>			
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>  
    </div>            
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>
    <!-- BEGIN PAGE CONTENT-->

    <div class="row-fluid">


        <div class="span12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i><?= $object->title . "&nbsp; >>&nbsp;  " . " Section " . $group->section ?> &nbsp; >>&nbsp; <?= $group->title ?>&nbsp;  >>&nbsp;  Students</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>											
                                <th class="hidden-480" style='vertical-align:top;'>Roll No.</th>
                                <th class="hidden-480 sorting_disabled" style='vertical-align:top;'>Name</th>
                                <th class="hidden-480 sorting_disabled" style='vertical-align:top;'>Father Name</th>
                                <th class="hidden-480 sorting_disabled" style='text-align:center;'>Exam Titles</th>
                                <th class="hidden-480 sorting_disabled" style='text-align:center;width:15%;'>Action</th>
                            </tr>
                        </thead>
                            <? if (!empty($result)): ?>
                            <tbody>
                                <?php
                                $sr = 1;
                                foreach ($result as $key => $obj):
                                    $ShowObj = new examination();
                                    $show = $ShowObj->getExamNames($group->exam_id, $obj->id);
                                    ?>
                                    <tr class="odd gradeX">													
                                        <td class="hidden-480"><?= $obj->roll_no ?></td>
                                        <td class="hidden-480"><?= $obj->first_name . " " . $obj->last_name ?></td>
                                        <td class='hidden-480'><?= ucfirst($obj->father_name) ?></td>
                                        <td style='text-align:center' class="hidden-480"><?= $show ?></td>
                                        <td style='text-align:center' class="hidden-480">
        <? if (isset($dmc) && (!empty($dmc->id))): ?>
                                                <a class="btn mini black icn-only tooltips" href="<?php echo make_admin_url('exam', 'result', 'result', 'id=' . $id . '&st_id=' . $obj->id . '&doc_id=' . $dmc->id) ?>" title="click here to view Report Card">Report Card</a>&nbsp;&nbsp;
                                    <? endif; ?>
                                        </td>											
                                    </tr>
                                <?php $sr++;
                            endforeach;
                            ?>
                            </tbody>
<?php endif; ?>  
                    </table>
                </div>
            </div>

            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

    <div class="clearfix"></div>


</div>
<!-- END PAGE CONTAINER-->    



