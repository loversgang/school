

<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
    <!-- BEGIN PAGE HEADER-->
    <div class="row-fluid">
        <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Manage Examinations
            </h3>
            <ul class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="<?php echo make_admin_url('home', 'list', 'list'); ?>">Home</a> 
                    <i class="icon-angle-right"></i>
                </li>                                   
                <li>
                    <i class="icon-list"></i>
                    <a href="<?php echo make_admin_url('exam', 'list', 'list'); ?>">All Examination</a> 
                    <i class="icon-angle-right"></i>
                </li>   
                <li class="last">
                    Edit Examination  
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="clearfix"></div>
    <div class="tiles pull-right">
        <?php include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/shortcut.php'); ?>
    </div>
    <div class="clearfix"></div>
    <?php
    /* display message */
    display_message(1);
    $error_obj->errorShow();
    ?>

    <div class="clearfix"></div>

    <div class="row-fluid">
        <div class="span12">
            <div id="form_wizard_1" class="portlet box blue">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-reorder"></i> Edit Examination Wizard - <span class="step-title">Step 1 of 2</span>
                    </div>
                </div>
                <div class="portlet-body form">

                    <div class="form-wizard">
                        <!-- main Head -->
                        <div class="navbar steps">
                            <div class="navbar-inner">
                                <ul class="row-fluid nav nav-pills">
                                    <li class="span4 <?= ($type == 'select') ? 'active' : ''; ?>">
                                        <a class="step <?= ($type == 'select') ? 'active' : ''; ?>" >
                                            <span class="number">1</span>
                                            <span class="desc"><i class="icon-ok"></i> Examination Detail</span>   
                                        </a>
                                    </li>
                                    <li class="span4 <?= ($type == 'result') ? 'active' : ''; ?>">
                                        <a class="step <?= ($type == 'result') ? 'active' : ''; ?>" >
                                            <span class="number">2</span>
                                            <span class="desc"><i class="icon-ok"></i> Examination Marks</span>   
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- main Head -->
                        <div class="progress progress-success progress-striped" id="bar">
                            <div class="bar" style="width: <?= $per ?>;"></div>
                        </div>
                        <div class="tab-content">

                            <!-- First Step -->
                            <div id="tab2" class="tab-pane <?= ($type == 'select') ? 'active' : ''; ?>">
                                <form class="form-horizontal" action="<?php echo make_admin_url('exam', 'update', 'update&id=' . $id . '&type=exam') ?>" method="POST" enctype="multipart/form-data" id="validation">

                                    <h3 class="block alert alert-info" id='title_show'><?= $object->title . "<font class='green_color'> > </font>" . " Section " . $exam->section ?></h3>
                                    <h4 class="form-section hedding_inner">Examination Detail</h4>


                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label" for="title">Examination Title<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="title" value="<?= $exam->title ?>" id="exam" class="span10 m-wrap validate[required]"/>
                                                </div>
                                            </div> 
                                            <div class="control-group">
                                                <label class="control-label" for="title">Examination Type<span class="required">*</span></label>
                                                <div class="controls">
                                                    <select name="exam_type" class="span10 m-wrap">
                                                        <option value="">No Type</option>
                                                        <option value="FA1" <?= $exam->exam_type=='FA1'?'selected':'' ?>>FA1</option>
                                                        <option value="FA2" <?= $exam->exam_type=='FA2'?'selected':'' ?>>FA2</option>
                                                        <option value="FA3" <?= $exam->exam_type=='FA3'?'selected':'' ?>>FA3</option>
                                                        <option value="FA4" <?= $exam->exam_type=='FA4'?'selected':'' ?>>FA4</option>
                                                        <option value="SA1" <?= $exam->exam_type=='SA1'?'selected':'' ?>>SA1</option>
                                                        <option value="SA2" <?= $exam->exam_type=='SA2'?'selected':'' ?>>SA2</option>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="control-group">
                                                <label class="control-label" for="subject_id">Examination Subject<span class="required">*</span></label>
                                                <div class="controls">
                                                    <select name="subject_id" id="subject_id"  class="span10 m-wrap ">
                                                        <?php $sr = 1;
                                                        while ($sub = $QuerySubject->GetObjectFromRecord()):
                                                            ?>
                                                            <option value='<?= $sub->id ?>' <?= $sub->id == $exam->subject_id ? "selected" : "" ?> ><?= $sub->name ?></option>
                                                            <? $sr++;
                                                        endwhile;
                                                        ?>																													
                                                    </select>
                                                </div>
                                            </div> 

                                            <div class="control-group">
                                                <label class="control-label" for="date_of_examination">Examination Date<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="date_of_examination" value="<?= $exam->date_of_examination ?>" id="date_of_examination" class="span10 m-wrap new_format validate[required]" />
                                                </div>
                                            </div> 	

                                            <div class="control-group">
                                                <label class="control-label" for="minimum_marks">Passing Marks<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="minimum_marks" value="<?= $exam->minimum_marks ?>" id="minimum_marks" class="span10 m-wrap validate[required,custom[onlyNumberSp]]" />
                                                </div>
                                            </div> 



                                        </div>
                                        <div class="span6 ">

                                            <div class="control-group">
                                                <?
                                                if (!empty($exam->teacher_id)):
                                                    $tech = get_object('staff', $exam->teacher_id);
                                                endif;
                                                ?>
                                                <label class="control-label" for="teacher_cat">Select Staff Category<span class="required">*</span></label>
                                                <div class="controls">
                                                    <select class="teacher_cat span9 m-wrap">
                                                        <option value=''>Select Category</option>
                                                        <?php $sr = 1;
                                                        while ($cat = $QueryStaff->GetObjectFromRecord()):
                                                            ?>
                                                            <option value='<?= $cat->id ?>' <?
                                                                    if (is_object($tech) && $tech->staff_category == $cat->id) {
                                                                        echo 'selected';
                                                                    }
                                                                    ?> ><?= $cat->name ?></option>
    <? $sr++;
endwhile;
?>																													
                                                    </select>                      
                                                </div>
                                            </div>															
                                            <div class="control-group" id='dyanamic_teacher'>
                                                <label class="control-label" for="teacher_id">Teacher Name<span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text"  value='<?
if (is_object($tech)) {
    echo $tech->title . ' ' . $tech->first_name . ' ' . $tech->last_name;
}
?>' id='teacher_id' class="m-wrap span9 validate[required]" readonly> 

                                                </div>
                                            </div>

                                            <div class="control-group">
                                                <label class="control-label" for="time_of_examination">Examination Time</label>
                                                <div class="controls">
                                                    <input type="text" name='time_of_examination' value='<?= $exam->time_of_examination ?>' class="m-wrap span9 m-ctrl-small timepicker3 add-on" style='text-align:left;'>
                                                </div>
                                            </div> 

                                            <div class="control-group">
                                                <label class="control-label" for="maximum_marks">Maximum Marks <span class="required">*</span></label>
                                                <div class="controls">
                                                    <input type="text" name="maximum_marks" value="<?= $exam->maximum_marks ?>" id="maximum_marks" class="span9 m-wrap validate[required,custom[onlyNumberSp]]" />
                                                </div>
                                            </div> 																

                                        </div>	
                                    </div>

                                    <input type='hidden' name='type' value='result'/>
                                    <input type='hidden' name='id' value='<?= $exam->id ?>'/>
                                    <input type='hidden' name='session_id' value='<?= $exam->session_id ?>'/>
                                    <input type='hidden' name='section' value='<?= $exam->section ?>'/>
                                    <input type='hidden' name='school_id' value='<?= $school->id ?>'/>												
                                    <div class="form-actions clearfix" >
                                        <a href="<?php echo make_admin_url('exam', 'list', 'list'); ?>" class="btn red" name="cancel" > Cancel</a>
                                        <input class="btn green" type="submit" name="submit1" value="Submit" tabindex="7" /> 

                                    </div>													
                                </form>	
                            </div>
                            <!-- Second Step End-->

                            <!-- Third Step -->
                            <div id="tab3" class="tab-pane <?= ($type == 'result') ? 'active' : ''; ?>">
                                <h3 class="block alert alert-info" id='title_show'><?= $object->title . "<font class='green_color'> > </font>" . " Section " . $exam->section . "<font class='green_color'> > </font>" . $exam->title ?></h3>
                                <div class="row-fluid">

                                    <div class="span12">
                                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-calendar"></i>Examination Marks of <?= $subject->name ?></div>
                                                <div class="tools">
                                                    <font style='font-size:18px;'><?= date('d M, Y', strtotime($exam->date_of_examination)) ?></font>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <form class="form-horizontal" action="<?php echo make_admin_url('exam', 'update', 'update&exam_id=' . $exam_id) ?>" method="POST" enctype="multipart/form-data" id='validation'>									
                                                    <table class="table table-striped table-bordered table-hover attendance_table" id="">
                                                        <thead>
                                                            <tr>

                                                                <th class="hidden-480" style='vertical-align:top;'>Roll No.</th>
                                                                <th class="hidden-480 sorting_disabled" style='vertical-align:top;'>Name</th>
                                                                <th class="hidden-480 sorting_disabled" style='vertical-align:top;'>Father Name</th>
                                                                <th class="hidden-480 sorting_disabled" style='text-align:center;'>Minimum Marks</th>																					
                                                                <th class="hidden-480 sorting_disabled" style='text-align:center;'>Maximum Marks</th>
                                                                <th class="hidden-480 sorting_disabled" style='text-align:center;'>Marks Obtained</th>
                                                            </tr>
                                                        </thead>
                                                            <? if (!empty($records)): ?>
                                                            <tbody>
    <?php $sr = 0;
    foreach ($records as $sk => $stu): $checked = '';
        ?>
        <?
        # get the student marks
        $St_marks = new examinationMarks();
        $marks = $St_marks->get_student_exam_marks($exam_id, $stu->id);
        ?>
                                                                    <tr class="odd gradeX">

                                                                        <td class='main_title'><?= $stu->roll_no ?></td>
                                                                        <td class='main_title'><?= ucfirst($stu->first_name) . " " . $stu->last_name ?></td>
                                                                        <td class='main_title'><?= ucfirst($stu->father_name) ?></td>
                                                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $exam->minimum_marks ?></td>
                                                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><?= $exam->maximum_marks ?></td>
                                                                        <td class="hidden-480 sorting_disabled" style='text-align:center;'><input type='text' name='marks[<?= $stu->id ?>][marks_obtained]' placeholder='0' class='m-wrap span4 validate[required,custom[onlyNumberSp]]' style='text-align:center;background:#fff;' value='<?
                                                                if ($marks): echo $marks->marks_obtained;
                                                                else: echo '0';
                                                                endif;
                                                                ?>'/></td>
                                                                <input type='hidden' name='marks[<?= $stu->id ?>][exam_id]' value='<?= $exam_id ?>'/>
                                                                <input type='hidden' name='marks[<?= $stu->id ?>][roll_no]' value='<?= $stu->roll_no ?>'/>
                                                                <input type='hidden' name='section' value='<?= $exam->section ?>'/>
                                                                <input type='hidden' name='marks[<?= $stu->id ?>][max_mark]' value='<?= $exam->maximum_marks ?>'/>
                                                                </tr>
        <?
        $sr++;
    endforeach;
    ?>									
                                                            </tbody>
<? endif; ?>
                                                    </table>
                                                    <div class='span12' style='padding-right:35px; padding-top:10px;'>
                                                        <input type='hidden' name='school_id' value='<?= $school->id ?>'/>
                                                        <input type='hidden' name='date' value='<?= $date ?>'/>
                                                        <input type='hidden' name='section' value='<?= $ct_sect ?>'/>
                                                        <input type='hidden' name='ct_sect' value='<?= $ct_sect ?>'/>
                                                        <input type='hidden' name='session_id' value='<?= $id ?>'/>
                                                    </div>

                                                    <div class="form-actions clearfix">
                                                        <input class="btn green check_valid" type="submit" value="Submit Marks" name="submit_marks" style='float:right;'>
                                                        <a href='<?= make_admin_url('exam', 'update', 'update&id=' . $exam->id) ?>' class="btn red" style='float:right;margin-right:10px;'><i class="m-icon-swapleft"></i>&nbsp;&nbsp;Back</a> 
                                                    </div>																	
                                                    <div class="clearfix"></div>
                                                </form>	
                                            </div>
                                        </div>

                                        <!-- END EXAMPLE TABLE PORTLET-->
                                    </div>
                                </div>	
                            </div>
                            <!-- Third Step End -->

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>																



    <div class="clearfix"></div>
</div>
<!-- END PAGE CONTAINER-->    

<script type="text/javascript">
    $(".session_filter").change(function () {
        var session_id = $("#session_id").val();

        var id = session_id;
        $("#dynamic_model").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

        var dataString = 'id=' + id;

        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_section&temp=exam'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                $("#dynamic_model").html(data);
            }
        });

    });

    $(".teacher_cat").change(function () {
        var cat_id = $(this).val();

        var id = cat_id;
        $("#dyanamic_teacher").html('<div style="width:100%;text-align:center;padding-top:5px;"><img src="assets/img/ajax-loading.gif"/></div>');

        var dataString = 'id=' + id;

        $.ajax({
            type: "POST",
            url: "<?= make_admin_url_window('ajax_calling', 'ajax_section', 'ajax_teacher&temp=exam'); ?>",
            data: dataString,
            success: function (data, textStatus) {
                $("#dyanamic_teacher").html(data);
            }
        });

    });

</script> 
