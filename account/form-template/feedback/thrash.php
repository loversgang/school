

  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Contact Requests
                                    <a class="btn large grey" href="<?php echo make_admin_url('feedback', 'list', 'list');?>"><i class="icon-envelope"></i> View all Contact Requests</a>
                            </h3>
                            
                           


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-envelope"></i>Trash</div>
								
							</div>
							<div class="portlet-body">
							        <table class="table table-striped table-bordered table-hover" id="">
									<thead>
										 <tr>
                                                                                        
                                                                                        <th class="hidden-480">Sr. No</th>
                                                                                        <th>Name</th>
                                                                                 
                                                                                        <th class="hidden-480">Email </th>
                                                                                        <th>Date</th>
                                                                                        <th >Action</th>
                                                                                        

                                                                                </tr>
									</thead>
									 <tbody>
                                                                            
                                                                              <? if($QueryObj->GetNumRows()!=0):?>
                                                                             <?php $sr=1;while($feedback=$QueryObj->GetObjectFromRecord()):?>
                                                                                    <tr class="odd gradeX">
                                                                                    
                                                                                        
                                                                                        <td class="hidden-480"><?php echo $sr++;?></td>
											<td><?php echo $feedback->title?></td>
                                                                                  
											<td class="hidden-480">
                                                                                           <?php echo $feedback->email?> 
                                                                                        </td>
											<td>
                                                                                            <?php echo date('d F, Y, g:i a', strtotime($feedback->date)); ?>
                                                                                        </td>
											<td>
                                                                                            
                                                                                               <a class="btn green icn-only tooltips" href="<?php echo make_admin_url('feedback', 'restore', 'restore', 'id='.$feedback->id)?>" class="tipTop smallbtn" title="click here to restore this record"><i class="icon-undo icon-white"></i></a>&nbsp;&nbsp;
                                                                                               <a class="btn red icn-only tooltips" href="<?php echo make_admin_url('feedback', 'permanent_delete', 'permanent_delete', 'id='.$feedback->id.'&delete=1')?>" onclick="return confirm('Are you deleting this record permanently?.');" class="tipTop smallbtn" title="click here to delete this record permanently" ><i class="icon-remove icon-white"></i></a>  
                                                                                         </td>
											

                                                                                    </tr>
                                                                             <?php endwhile;?>
                                                                           

									</tbody>
                                                                      
                                                                       <?php endif;?>  
								</table>
                                                            
                                                            
                              
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

