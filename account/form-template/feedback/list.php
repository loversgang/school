
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Contact Requests
                                    
                            </h3>
             


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

            
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-envelope"></i>Manage Contact Requests</div>
								
							</div>
							<div class="portlet-body">
							      <? if($QueryObj->GetNumRows()!=0):?>
                                                                     <?php $sr=1;while($feedback=$QueryObj->GetObjectFromRecord()):?>
                                                                                   <div class="row-fluid">

                                                                                            <div class="span12 blog-article">
                                                                                                    <h2 class="<?php echo $feedback->is_read==0?'header_bold':""?>" ><a href="<?php echo make_admin_url('feedback', 'view', 'view', 'id=' . $feedback->id)?>"><?php echo ucwords($feedback->title)?></a></h2>
                                                                                                    <ul class="unstyled inline">
                                                                                                            <li><i class="icon-calendar"></i> <?php echo date('d F, Y, g:i a', strtotime($feedback->date)); ?></a></li>
                                                                                                            <li><i class="icon-phone"></i> <?php echo $feedback->phone?></li>
                                                                                                            <li><i class="icon-envelope"></i> <a href="mailto:<?php echo $feedback->email?>"><?php echo $feedback->email?></a></li>
                                                                                                    </ul>
                                                                                                    <div style="clear:both;height:10px;"></div>
                                                                                                    <p><?php echo limit_text($feedback->message,100) ?>...</p>
                                                                                                    <a class="btn blue tooltips" href="<?php echo make_admin_url('feedback', 'view', 'view', 'id=' . $feedback->id)?>" title="click here to view this record">
                                                                                                       Read more 
                                                                                                       <i class="m-icon-swapright m-icon-white"></i>
                                                                                                    </a>
                                                                                                     <a class="btn red tooltips" href="<?php echo make_admin_url('feedback', 'delete', 'list', 'id='.$feedback->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record">
                                                                                                        Delete
                                                                                                       <i class="icon-remove m-icon-white"></i>
                                                                                                    </a>
                                                                                            </div>
                                                                                    </div>
                                                                                    <hr>
                                                                             <?php endwhile;?>
                                                                               <?php
                                                                               if($total_pages>1):

                                                                                   echo pagingAdmin($p,$total_pages,$total_records,'feedback');

                                                                               endif;
                                                                               ?>       
                                                                     <?php else:?>
                                                                                    
                                                                         <div class="row-fluid">

                                                                                <div class="span12 blog-article">

                                                                                     No contact request found.
                                                                                </div>
                                                                         </div>
                                                                                
                                                                           
                                                                      <?php endif;?>                   
                                                            
                                                              <div class="clearfix"></div>
                                                              <div class="btn-group pull-right" data-toggle="buttons-radio">
                                                               <a  href="<?php echo make_admin_url('feedback', 'thrash', 'thrash');?>" class="btn mini red pull-right"><i class="icon-red icon-trash"></i> &nbsp;view trash</a>
                                                             </div>
                                                             <div class="clearfix"></div>
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    









