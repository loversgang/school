
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Contact Requests
                                   <a class="btn large grey" href="<?php echo make_admin_url('feedback', 'list', 'list');?>"><i class="icon-envelope"></i> View all Contact Requests</a>
                            </h3>
                          

                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>

              <?php #  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
            
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                    <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-envelope"></i>View Contact Request</div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                         <div class="row-fluid">

                                            <div class="span12 blog-article">
                                                    <h2><?php echo ucwords($feedback->title)?></h2>
                                                    <ul class="unstyled inline">
                                                            <li><i class="icon-calendar"></i> <?php echo date('d F, Y, g:i a', strtotime($feedback->date)); ?></a></li>
                                                            <li><i class="icon-phone"></i> <?php echo $feedback->phone?></li>
                                                            <li><i class="icon-envelope"></i> <a href="mailto:<?php echo $feedback->email?>"><?php echo $feedback->email?></a></li>
                                                    </ul>
                                                    <div style="clear:both;height:10px;"></div>
                                                    <p><?php echo $feedback->message ?></p>

                                                     <a class="btn red tooltips" href="<?php echo make_admin_url('feedback', 'delete', 'list', 'id='.$feedback->id.'&delete=1')?>" onclick="return confirm('Are you sure? You are deleting this record.');" title="click here to delete this record">
                                                        Delete
                                                       <i class="icon-remove m-icon-white"></i>
                                                    </a>
                                            </div>
                                         </div>
                                    </div>
                            </div>
                        </div>
               
                 
                
                
	     </div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



