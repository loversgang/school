



  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Clients
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List Clients</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
				</div>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>Manage Clients</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
							<form action="<?php echo make_admin_url('school', 'update2', 'update2', 'id='.$id);?>" method="post" id="form_data" name="form_data" >	
								<table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										 <tr>
												<th>School ID</th>
												<th>School</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Payments</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Settings</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Status</th>
												<th >Action</th>
										</tr>
									</thead>
                                        <? if($QueryObj->GetNumRows()!=0):?>
										<tbody>
                                            <?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
                                                <tr class="odd gradeX">
													<td><?=$object->client_id?></td>
													<td><?php echo $object->school_name.', '.$object->address1?></td>
													<td class="hidden-480" style='text-align:center'><a href='<?=make_admin_url('school','payment','payment&id='.$object->id);?>' class="btn yellow icn-only tooltips" title="click here to school payment">Payment <i class="m-icon-swapright m-icon-white"></i></a></td>
													<td class="hidden-480" style='text-align:center'><a href='<?=make_admin_url('school','setting','setting&id='.$object->id);?>' class="btn red icn-only tooltips" title="click here to school setting">Settings <i class="m-icon-swapright m-icon-white"></i></a></td>
													<td style='text-align:center' class="hidden-480">
  														<div class="btn-group tooltips" id='on_off_button' data-toggle="buttons-radio" title="Click here to change status">
															<div class="btn status_on <?php echo ($object->is_active=='1')?'active':'';?>" on="<?php echo $object->id;?>" rel="<?php echo ($object->is_active=='1')?'on':'off';?>"  >SHOW</div>
															<div class="btn status_off <?php echo ($object->is_active=='0')?'active':'';?>" off="<?php echo $object->id;?>" rel="<?php echo ($object->is_active=='1')?'off':'on';?>">HIDE</div>
														</div>													
													</td>
													<td>
													<a class="btn blue icn-only tooltips" href="<?php echo make_admin_url('school', 'update', 'update', 'id='.$object->id)?>" title="click here to view this record"><i class="icon-search icon-white"></i></a>&nbsp;&nbsp;
													</td>
												</tr>
                                            <?php $sr++;
												endwhile;?>
										</tbody>
									   <?php endif;?>  
								</table>
                            </form>    
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



