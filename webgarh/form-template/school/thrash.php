
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Clients
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List Clients</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
				</div>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>Trash</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">      
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
									<thead>
										 <tr>
												
												<th class="hidden-480">Sr. No</th>
												<th>School Name</th>
												<th class="hidden-480">Email</th>												
												<th >Action</th>
												<th style='border-left:none;border-right:none;'></th>
												<th style='border-left:none;border-right:none;'></th>
										</tr>
									</thead>
									 <tbody>                                                                            
											<? if($QueryObj->GetNumRows()!=0):?>
											<?php $sr=1;while($object=$QueryObj->GetObjectFromRecord()):?>
											<tr class="odd gradeX">							
													<td class="hidden-480"><?php echo $sr++;?></td>
													<td><?php echo $object->school_name?></td>
													<td class="hidden-480"><?php echo $object->email_address?></td>																								
													<td>
													</td>
													<td style='border-left:none;border-right:none;'></td> 
													<td style='border-left:none;border-right:none;'></td>
											</tr>
											<?php endwhile;?>            
										<?php endif;?>  
									</tbody>
								</table>                  
                              
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    

