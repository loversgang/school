
  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">

                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Clients > <?=$school->school_name.', '.$school->address1?>
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                        <i class="icon-home"></i>
                                        <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                        <i class="icon-angle-right"></i>
                                    </li>
                                    <li><a href="<?php echo make_admin_url('school', 'list', 'list');?>">List Clients<i class="icon-angle-right"></i></a></li>
									<li>Client Payment</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
							<div class="tile bg-purple <?php echo ($section=='invoice')?'selected':''?>">
							<a href="<?php echo make_admin_url('school', 'payment', 'payment&id='.$_GET['s_id']);?>">
								<div class="corner"></div>
								<div class="tile-body"><i class="icon-arrow-left"></i></div>
								<div class="tile-object"><div class="name">Back To Payment</div></div>
							</a> 
							</div>			
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut_payment_edit.php');?>
				</div>
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
             <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                   <form class="form-horizontal" action="<?php echo make_admin_url('school', 'edit_payment', 'edit_payment&s_id='.$_GET['s_id'])?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>Edit Client Payment</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">
                                            <div class="control-group">
                                                    <label class="control-label" for="payment_type">Client<span class="required">*</span></label>
                                                    <div class="controls">
                                                          <input type="text"  value='<?=$school->school_name.", ".$school->address1?>' readonly class="span6 m-wrap validate[required]"/>
													</div>
                                            </div>  
									
                                            <div class="control-group">
                                                    <label class="control-label" for="payment_type">Payment Type<span class="required">*</span></label>
                                                    <div class="controls">
                                                          <input type="text" name="payment_type" value='<?=$invoice->payment_type?>' readonly class="span6 m-wrap validate[required]"/>
													</div>
                                            </div>
											
											<? if($invoice->set_up_fee!=0):?>		
												<div class="control-group">
														<label class="control-label" for="set_up_fee">Set-up Fee (<?=CURRENCY_SYMBOL?>)</label>
														 <div class="controls">
														   <input type="text" name="set_up_fee" id="set_up_fee" value='<?=$invoice->set_up_fee?>' class="span6 m-wrap" />
														</div>
												</div>
											<? endif;?>		
                                            <div class="control-group">
                                                    <label class="control-label" for="subscription_price">Subscription Fee (<?=CURRENCY_SYMBOL?>)<span class="required">*</span></label>
                                                     <div class="controls">
                                                       <input type="text" name="subscription_price"  value='<?=$invoice->subscription_price?>' id="subscription_price" class="span6 m-wrap validate[required]" />
													</div>
                                            </div> 
										
                                            <div class="control-group">
                                                    <label class="control-label" for="subscription_price">Remarks</label>
                                                     <div class="controls">
                                                       <textarea name="remarks" id="remarks" class="span6 m-wrap"><?=$invoice->remarks?></textarea>
													</div>
                                            </div> 
											<input type="hidden" name="payment_date" value='<?=date('Y-m-d')?>'/> 
                                    <?php if(defined('BOTTOM_ACTION') && BOTTOM_ACTION==1):?>
                                            <div class="form-actions">
													 <input type="hidden" name="id" value="<?=$invoice->id?>"/>
													 <input type="hidden" name="admin_id" value="<?=$admin_user->get_user_id()?>"/>
													 <input class="btn blue" type="submit" name="submit" value="Submit" tabindex="7" /> 
                                                     <a href="<?php echo make_admin_url('school', 'payment', 'payment&id='.$_GET['s_id']);?>" class="btn" name="cancel" > Cancel</a>
                                            </div>
                                     <?php endif;?>  
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
	     </div>
             <div class="clearfix"></div>
    </div>
    <!-- END PAGE CONTAINER-->    

