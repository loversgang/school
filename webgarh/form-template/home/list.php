                      <!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							Dashboard <small>statistics and more</small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>Dashboard</li>
							
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
                                
                                <?php 
                                /* display message */
                                display_message(1);
                                $error_obj->errorShow();
                                ?>
                                
                                
				<div id="dashboard">
					<!-- BEGIN DASHBOARD STATS -->
					
					
					<!-- END DASHBOARD STATS -->
					<div class="clearfix"></div>
											<div class="portlet box red">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i>Graph7</div>
								<div class="tools">
									<a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
								</div>
							</div>
							<div class="portlet-body">
								<h4>Labels can be hidden if the slice is less than a given percentage of the pie (10% in this case).</h4>
								<div id="pie_chart_7" class="chart"></div>
							</div>
						</div>
				
					<?php /*
                                                if((GOOGLE_AC!='') && (ANALYTIC_USER!='') && (ANALYTIC_PASS!='') && (ANALYTIC_TABLE!='')):
                                                        //include_once('google_analytics.php');

                                                endif;		
                                        */?>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->    