



  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    Manage Monthly Account
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List Clients</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            
				
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>
				<div class="tiles pull-left">
				<form class="form-horizontal" action="<?php echo make_admin_url('payment', 'list', 'list')?>" method="GET" enctype="multipart/form-data" id='session_filter'>									
				<input type='hidden' name='Page' value='payment'/>
				<input type='hidden' name='action' value='list'/>
				<input type='hidden' name='section' value='list'/>
				
				<div class="control-group alert alert-success span8" style='margin-left:0px;padding-right:0px;'>
					<div class='span2' style='margin-left: 5px;'>
					<label class="control-label" style='float: none; text-align: left;'>Select Month & Year</label>
							<select class="select2_category span2" data-placeholder="Select Session Students" name='month' >
								<option value="">Select Month</option>
								<?=select_month($month);?>
							</select>	
					</div>
										
					
					<div class='span4' style='margin-left: 5px; padding-right:20px'>
					<label class="control-label" style='float: none; text-align: left;'>&nbsp;</label>
							<select class="select2_category span2" data-placeholder="Select Session Students" name='year' >
								<?=select_year_current($year);?>
							</select>&nbsp;&nbsp;
						<input type='submit' name='go' value='Go' class='btn blue'/>
					</div>
					<div class="clearfix"></div>
					&nbsp;&nbsp;Please select the months to view the record.
				</div>
				</form>										
				</div>	
             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>Manage Accounts</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
							
								<table class="table table-striped table-bordered table-hover" id="">
									<thead>
										 <tr>
												<th>Month & Year</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Total Billing School</th>
												<th style='text-align:right' class="hidden-480 sorting_disabled">Total Received Amount</th>
												<th style='text-align:right'>Action</th>
										</tr>
									</thead>
										<tbody>
                                                <tr class="odd gradeX">
													<td><?php 
													if(!empty($month)):
														echo date('M, Y',strtotime('01-'.$month.'-'.$year));
													else:
														echo $year;
													endif;
													?>
													</td>
													
													<td class="hidden-480" style='text-align:center'>
														<?=$Bschool?>
													</td>
													
													<td class="hidden-480" style='text-align:right'>
														<?=$Bschool." X ".number_format(WEBGARH_AMOUNT,2)?> = <strong><? echo number_format($Bschool*WEBGARH_AMOUNT,2);?></strong>
													</td>
												
													<td style='text-align:right'>
													<? if(!empty($month) && $record['amount']<1):?>
															<a class="btn green icn-only tooltips" onclick="return confirm('Are you sure? You want to Add Entry for this month.');" href="<?php echo make_admin_url('payment', 'payment', 'payment&entry=1&month='.$month.'&year='.$year)?>" title="click here to Add Monthly Entry">Add Entry</a>
													<? elseif(empty($month) && !empty($year)):?>
															Please Select Month
													<? else:?>													
															<a class="btn red icn-only tooltips" onclick="return confirm('Are you sure? You want to Delete All Entries for this month.');" href="<?php echo make_admin_url('payment', 'payment', 'payment&entry=0&month='.$month.'&year='.$year)?>" title="click here to delete Monthly Entry">Delete Entry</a>
													<? endif;?>
													
													</td>
												</tr>
                                          </tbody>
									  
								</table>
         
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



