 <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    List Of Clients
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>
                                        <i class="icon-user"></i>
                                               <a href="<?php echo make_admin_url('webgarh', 'list', 'list');?>">List Clients</a>
                                         <i class="icon-angle-right"></i>
                                       
                                    </li>
                                    <li class="last">
                                        View Client
                                    </li>

                            </ul>


                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="clearfix"></div>
				<div class="tiles pull-right">
					<?php  include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');?>
				</div>
           <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

            <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                
                
                   <form class="form-horizontal" action="<?php echo make_admin_url('webgarh', 'update', 'update&id='.$id)?>" method="POST" enctype="multipart/form-data" id="validation">
                          <!-- / Box -->
                          <div class="span12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                             <div class="portlet box blue">
                                    <div class="portlet-title">
                                            <div class="caption"><i class="icon-user"></i>View Client</div>
                                            <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                            </div>
                                    </div>
                                    <div class="portlet-body form">
									
									<!-- webgarh Info  -->
										<h4 class="form-section alert alert-info"><strong>Institute Information</strong></h4>
										<div class="row-fluid">
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="webgarh_name">Institute Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="webgarh_name" value="<?=$webgarh->school_name?>" id="webgarh_name" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>
											
                                            <div class="control-group">
                                                    <label class="control-label" for="username">Username<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="username" value="<?=$webgarh->username?>" id="username" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>
										
                                            <div class="control-group">
                                                    <label class="control-label" for="address1">Address1<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="address1" value="<?=$webgarh->address1?>" id="address1" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="city">City<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="city" value="<?=$webgarh->city?>" id="city" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>												
                                            <div class="control-group">
                                                    <label class="control-label" for="state">State<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="state" value="<?=$webgarh->state?>" id="state" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="image">Logo</label>
                                                    <div class="controls">
													<? if($webgarh->logo):
															$im_obj=new imageManipulation()?>
														<div class="item span10" style='text-align:center;'>	
														<a class="fancybox-button" data-rel="fancybox-button" title="Photo" href="<?=$im_obj->get_image('school','large',$webgarh->logo);?>">
															<div class="zoom">
																<img src="<?=$im_obj->get_image('school','medium',$webgarh->logo);?>" />
																<div class="zoom-icon"></div>
															</div>
														</a>
														</div>															
													<? else:?>
                                                      <input disabled type="file" name="image"  id="image" class="span10 m-wrap"/><br/>
													  <font style="font-size:10px;">Note: Institute Logo should be 150px(width)*100px(height).</font>
													<? endif;?> 
                                                    </div>
                                            </div> 											
										</div>
										<div class="span6 ">	
                                            <div class="control-group">
                                                    <label class="control-label" for="webgarh_type">Institute Type<span class="required">*</span></label>
                                                    <div class="controls">
                                                        <select disabled name="webgarh_type" id="webgarh_type"  class="span10 m-wrap ">
														<?php $sr=1;while($s_type=$QueryS_type->GetObjectFromRecord()):?>
																	<option value='<?=$s_type->id?>' <? if($webgarh->school_type==$s_type->id){ echo 'select disableded';} ?>><?=ucfirst($s_type->name)?></option>
														<? $sr++; endwhile;?>															
                                                        </select disabled>   														
													</div>
                                            </div>
											
                                            <div class="control-group">
                                                    <label class="control-label" for="password">Password<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="password" value="<?=trim(encrypt_decrypt('decrypt',$webgarh->password))?>" id="password" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>	
											
                                            <div class="control-group">
                                                    <label class="control-label" for="address2">Address2</label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="address2" value="<?=$webgarh->address2?>" id="address2" class="span10"/>
                                                    </div>
                                            </div>											
  
                                            <div class="control-group">
                                                    <label class="control-label" for="District">District<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="district" value="<?=$webgarh->district?>" id="district" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>  										
                                            <div class="control-group">
                                                    <label class="control-label" for="country">Country<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <select disabled name="country" id="country" class="span10 m-wrap validate[required]">
													   <?php foreach($countries as $key=>$value):?>
																<option value="<?=$value?>" <? if(trim($webgarh->country)==trim($value)): echo 'select disableded'; elseif($value=="India"): echo 'select disableded'; endif;?>><?=$value?></option>
													   <?php endforeach;?>
													  </select disabled>
													</div>
                                            </div> 
										</div>	
										</div>	

									<!-- Contact Info  -->
										<h4 class="form-section alert alert-info"><strong>Contact Information</strong></h4>
										<div class="row-fluid">
										<div class="span6 ">
                                            <div class="control-group">
                                                    <label class="control-label" for="webgarh_head_name">Institute Head Name<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="webgarh_head_name" value="<?=$webgarh->school_head_name?>" id="webgarh_head_name" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 	
                                            <div class="control-group">
                                                    <label class="control-label" for="webgarh_head_email">Institute Head Email<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="webgarh_head_email" value="<?=$webgarh->school_head_email?>" id="webgarh_head_email" class="span10 m-wrap validate[required,custom[email]]"/>
                                                    </div>
                                            </div>  											
                                            <div class="control-group">
                                                    <label class="control-label" for="phone1_contact_name">Contact Person Name1<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="phone1_contact_name" value="<?=$webgarh->phone1_contact_name?>" id="phone1_contact_name" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 										
									
                                            <div class="control-group">
                                                    <label class="control-label" for="phone2_contact_name">Contact Person Name2</label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="phone2_contact_name" value="<?=$webgarh->phone2_contact_name?>" id="phone2_contact_name" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="email_address">Institute Email<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="email_address" value="<?=$webgarh->email_address?>" id="email_address" class="span10 m-wrap validate[required,custom[email]]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="website_url">Website Url</label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="website_url" value="<?=$webgarh->website_url?>" id="website_url" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 	 											
										</div>
										<div class="span6 ">	
                                            <div class="control-group">
                                                    <label class="control-label" for="webgarh_head_phone">Mobile No.<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="webgarh_head_phone" value="<?=$webgarh->school_head_phone?>" id="webgarh_head_phone" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 
                                            <div class="control-group">
                                                    <label class="control-label" for="fax">Fax<span class="required">*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="fax" value="<?=$webgarh->fax?>" id="fax" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div> 											
                                            <div class="control-group">
                                                    <label class="control-label" for="phone1">Contact Person Phone1<span class="required" style='padding-left:0px;'>*</span></label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="phone1" value="<?=$webgarh->phone1?>" id="phone1" class="span10 m-wrap validate[required]"/>
                                                    </div>
                                            </div>  										
                                            <div class="control-group">
                                                    <label class="control-label" for="phone2">Contact Person Phone2</label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="phone2" value="<?=$webgarh->phone2?>" id="phone2" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 	
                                            <div class="control-group">
                                                    <label class="control-label" for="webgarh_phone">Institute Landline No.</label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="webgarh_phone" value="<?=$webgarh->school_phone?>" id="webgarh_phone" class="span10 m-wrap"/>
                                                    </div>
                                            </div> 											
										</div>
										</div>	
										
										
									<!-- Other Info  -->
										<h4 class="form-section alert alert-info"><strong>Subscription Plan Information</strong></h4>
											<div class="row-fluid" id='dynamic_model'>
											<div class="span6 ">	
												<div class="control-group">
														<label class="control-label" for="set_up_fee">Set-up Fee (<?=CURRENCY_SYMBOL?>)<span class="required">*</span></label>
														<div class="controls">
														  <input disabled type="text"  name="subscription[set_up_fee]" value="<?=$webgarh->set_up_fee?>" id="set_up_fee" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
														</div>
												</div>	
												<div class="control-group">
														<label class="control-label" for="payment_type">Subscription Period<span class="required">*</span></label>
														<div class="controls">
														  <select disabled name="subscription[payment_type]" id="payment_type" class="span10 m-wrap ">
															<option value='1' <?php echo $webgarh->payment_type=='1'?"select disableded":""?>>Monthly</option>
															<option value='3' <?php echo $webgarh->payment_type=='3'?"select disableded":""?>>Quarterly</option>
															<option value='6' <?php echo $webgarh->payment_type=='6'?"select disableded":""?>>Half Yearly</option>
															<option value='12' <?php echo $webgarh->payment_type=='12'?"select disableded":""?>>Yearly</option>
														  </select disabled>
														</div>
												</div>												
											</div>
											<div class="span6 ">
												<div class="control-group">
														<label class="control-label" for="subscription_fee">Subscription Fee (<?=CURRENCY_SYMBOL?>)<span class="required">*</span></label>
														<div class="controls">
														  <input disabled type="text"  name="subscription[subscription_fee]" value="<?=$webgarh->subscription_fee?>" id="subscription_fee" class="span10 m-wrap validate[required,custom[onlyNumberSp]]"/>
														</div>
												</div> 
											</div>
											</div>
																				
										
											
											
									<!-- SMS Setting  -->
										<h4 class="form-section alert alert-info"><strong>SMS/Emails </strong></h4>
										<div class="row-fluid">
										<div class="span6" id='large_space'>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_sms_allowed">SMS Allowed OR Not<span class="required">*</span></label>
                                                    <div class="controls">
                                                       	<label class="checkbox line">
														<input disabled type="checkbox" name="is_sms_allowed"  value="1" <?=($setting->is_sms_allowed=='1')?'checked':'';?>/>
														</label>
                                                    </div>
                                            </div>										
                                            <div class="control-group">
                                                    <label class="control-label" for="max_sms_limit">Maximum Number of SMS's Allowed (Per/Month)</label>
                                                    <div class="controls">
                                                      <input disabled type="text" name="max_sms_limit" value="<?=$setting->max_sms_limit?>" id="max_sms_limit" class="span10 m-wrap"/>
                                                    </div>
                                            </div>
											 
                                            <div class="control-group">
                                                    <label class="control-label" for="user_name">SMS's</label>
													<div class="controls">
														<label class="checkbox line">
														<input disabled type="checkbox" name="sms[admission]"  value="1" <? if(in_array('admission',$smsArray)) { echo 'checked';}?>/>New Admission
														</label>
														<label class="checkbox line">
														<input disabled type="checkbox" name="sms[present]"  value="1" <? if(in_array('present',$smsArray)) { echo 'checked';}?>/>Present Attendance - Daily
														</label>
														<label class="checkbox line">
														<input disabled type="checkbox" name="sms[absent]"  value="1" <? if(in_array('absent',$smsArray)) { echo 'checked';}?>/>Absent From School - Daily
														</label>
														<label class="checkbox line">
														<input disabled type="checkbox" name="sms[fee_deposit]"  value="1" <? if(in_array('fee_deposit',$smsArray)) { echo 'checked';}?>/>Fee Deposit
														</label>
														<!--
														<label class="checkbox line">
														<input type="checkbox" name="sms[fee_pending]"  value="1" <? if(in_array('fee_pending',$smsArray)) { echo 'checked';}?>/>Fee Pending
														</label>
														-->
														
														<label class="checkbox line">
														<input disabled type="checkbox" name="sms[exam_result]"  value="1" <? if(in_array('exam_result',$smsArray)) { echo 'checked';}?>/>Exam Result
														</label>																											
														<label class="checkbox line">
														<input disabled type="checkbox" name="sms[birthday]"  value="1" <? if(in_array('birthday',$smsArray)) { echo 'checked';}?>/>Birthday Wishes
														</label>
														<label class="checkbox line">
														<input disabled type="checkbox" name="sms[holiday]"  value="1"<? if(in_array('holiday',$smsArray)) { echo 'checked';}?> />Holiday
														</label>
													</div>
                                            </div>
										</div>	
										<!-- Email setting-->
										<div class="span6" id='large_space'>
										<div class="control-group"></div><div class="control-group">&nbsp;</div>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_email_allowed">Email's Allowed OR Not<span class="required">*</span></label>
                                                    <div class="controls">
                                                       	<label class="checkbox line">
														<input disabled type="checkbox" name="is_email_allowed"  value="1" <?=($setting->is_email_allowed=='1')?'checked':'';?>/>
														</label>
                                                    </div>
                                            </div>	
												<input type='hidden' name="max_email_limit" value="150000"/>
                                            <div class="control-group">
                                                    <label class="control-label" for="user_name">Email's</label>
													<div class="controls">
														<label class="checkbox line">
														<input disabled type="checkbox" name="email[admission]"  value="1" <? if(in_array('admission',$emailArray)) { echo 'checked';}?>/>New Admission
														</label>
														<label class="checkbox line">
														<input disabled type="checkbox" name="email[present]"  value="1" <? if(in_array('present',$emailArray)) { echo 'checked';}?>/>Present Attendance - Daily
														</label>
														<label class="checkbox line">
														<input disabled type="checkbox" name="email[absent]"  value="1" <? if(in_array('absent',$emailArray)) { echo 'checked';}?>/>Absent From School - Daily
														</label>
														<label class="checkbox line">
														<input disabled type="checkbox" name="email[fee_deposit]"  value="1" <? if(in_array('fee_deposit',$emailArray)) { echo 'checked';}?>/>Fee Deposit
														</label>
														<!--
														<label class="checkbox line">
														<input type="checkbox" name="email[fee_pending]"  value="1" <? if(in_array('fee_pending',$emailArray)) { echo 'checked';}?>/>Fee Pending
														</label>
														-->
														<label class="checkbox line">
														<input disabled type="checkbox" name="email[exam_result]"  value="1" <? if(in_array('exam_result',$emailArray)) { echo 'checked';}?>/>Exam Result
														</label>							
														<label class="checkbox line">
														<input disabled type="checkbox" name="email[birthday]"  value="1" <? if(in_array('birthday',$emailArray)) { echo 'checked';}?>/>Birthday Wishes
														</label>
														<label class="checkbox line">
														<input disabled type="checkbox" name="email[holiday]"  value="1"<? if(in_array('holiday',$emailArray)) { echo 'checked';}?> />Holiday
														</label>
													</div>
                                            </div>
										</div>	
									</div>	

									<!-- School website  -->
										<h4 class="form-section alert alert-info"><strong>Client website</strong></h4>

										<div class="row-fluid">
										<div class="span12" id='large_space'>
                                            <div class="control-group">
                                                    <label class="control-label" for="is_website">Allow Website OR Not<span class="required">*</span></label>
                                                    <div class="controls">
                                                       	<label class="checkbox line">
														<input disabled type="checkbox" name="is_website"  value="1" <?=($webgarh->is_website=='1')?'checked':'';?> id='master_checker'/>
														</label>
                                                    </div>													
                                            </div>
											<? if($webgarh->is_website=='1'):?>
											<div class="control-group">
													<label class="control-label" for="is_website">Website URL<span class="required">*</span></label>
                                                    <div class="controls">
                                                       <h5><strong><? echo DIR_WS_SITE.'site/'.$id;?></strong></h5>
                                                    </div>
											</div>	
											<? endif;?>	
										</div>
										</div>
									
                                    
                              </div> 
                            </div>
                        </div>
                     </form>
                     <div class="clearfix"></div>
	     </div>
             <div class="clearfix"></div>
    </div>
		
