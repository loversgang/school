
        <div class="tile bg-green <?php echo ($section=='list')?'selected':''?>">
            <a href="<?php echo make_admin_url('school', 'list', 'list');?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-list"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                List Clients
                        </div>
                </div>
            </a>   
        </div>
        <div class="tile bg-blue <?php echo ($section=='new_payment')?'selected':''?>">
            <a href="<?php echo make_admin_url('school', 'new_payment', 'new_payment&id='.$id);?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-plus"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                New Payment
                        </div>
                </div>
            </a> 
        </div>

        <div class="tile bg-red <?php echo ($section=='setting')?'selected':''?>">
            <a href="<?php echo make_admin_url('school', 'setting', 'setting&id='.$id);?>">
                <div class="corner"></div>

                <div class="tile-body">
                        <i class="icon-cogs"></i>
                </div>
                <div class="tile-object">
                        <div class="name">
                                Client Settings
                        </div>
                </div>
            </a> 
        </div>
