



  <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
            <!-- BEGIN PAGE HEADER-->
            <div class="row-fluid">
                    <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                    List Of Clients
                            </h3>
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="icon-home"></i>
                                            <a href="<?php echo make_admin_url('home', 'list', 'list');?>">Home</a> 
                                            <i class="icon-angle-right"></i>
                                    </li>
                                    <li>List Clients</li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
            </div>
            
				
            <div class="clearfix"></div>
            <?php 
            /* display message */
            display_message(1);
            $error_obj->errorShow();
            ?>

             <div class="clearfix"></div>
              <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
					<div class="span12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i>Manage Clients</div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
								</div>
							</div>
							<div class="portlet-body">
							<form action="<?php echo make_admin_url('webgarh', 'update2', 'update2', 'id='.$id);?>" method="post" id="form_data" name="form_data" >	
								<table class="table table-striped table-bordered table-hover" id="sample_2">
									<thead>
										 <tr>
												<th style='width:40%'>Institute Name</th>
												<th style='text-align:left' class="hidden-480 sorting_disabled">Students</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Payment Fee</th>
												<th style='text-align:center' class="hidden-480 sorting_disabled">Status</th>
												<th style='text-align:center' >Action</th>
										</tr>
									</thead>
                                        <? if(!empty($record)):?>
										<tbody>
                                            <?php $sr=1;foreach($record as $key=>$object):?>
                                                <tr class="odd gradeX">
													<td><?php echo $object->school_name.', '.$object->address1?>
													<br/><span class='help' style='color:#666666;font-size:11px;'>Sign-Up Date : <?=date('d M,Y',strtotime($object->create_date))?></span>
													</td>
													<td class="hidden-480" style='text-align:left'>
													Active: <strong><? if($object->active_student): echo $object->active_student; else: echo '0'; endif;?></strong><br/>
													In-active: <strong><? if($object->in_active_student): echo $object->in_active_student; else: echo '0'; endif;?></strong>
													</td>
													<td class="hidden-480" style='text-align:right'>
													Set-Up Fee:&nbsp;&nbsp;  <strong><?=number_format($object->set_up_fee,2)?></strong><br/>
													Subscription Fee:&nbsp;&nbsp; <strong><?=number_format($object->subscription_fee,2)?></strong>
													 
													</td>
													<td style='text-align:center' class="hidden-480">
  														<? 	if($object->is_active=='1'):
																echo "Active";
															else:
																echo "Not Active";
															endif;
															?>												
													</td>
													<td style='text-align:center;'>
													
													  <? if($object->billing=='1'):?>
															<a class="btn red icn-only tooltips" onclick="return confirm('Are you sure? You want to disabled billing for this school.');" href="<?php echo make_admin_url('webgarh', 'payment', 'payment', 'id='.$object->webgarh_id.'&billing=0')?>" title="click here to Disable Billing">Disable Billing</a>
													  <? else:?>
														<a class="btn green icn-only tooltips" onclick="return confirm('Are you sure? You want to enable billing for this school.');" href="<?php echo make_admin_url('webgarh', 'payment', 'payment', 'id='.$object->webgarh_id.'&billing=1')?>" title="click here to Enable Billing">Enable Billing</a>
													  <? endif;?>
													</td>
												</tr>
                                            <?php $sr++;
												endforeach;?>
										</tbody>
									   <?php endif;?>  
								</table>
                            </form>    
							</div>
						</div>
                                                
						<!-- END EXAMPLE TABLE PORTLET-->
					</div>
				</div>

             <div class="clearfix"></div>
             
             
    </div>
    <!-- END PAGE CONTAINER-->    



