<!DOCTYPE html>
<!--
--- About Us  ---
Content Management System
Developed by:- cWebConsultants India
http://www.cwebconsultants.com
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php echo SITE_NAME?> | Webgarh Control Panel</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
        
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/chosen-bootstrap/chosen/chosen.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-tags-input/jquery.tagsinput.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-datepicker/css/datepicker.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-timepicker/compiled/timepicker.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-colorpicker/css/colorpicker.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css" />
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-multi-select/css/multi-select-metro.css" />
	<link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.css"/>
	<!-- END PAGE LEVEL STYLES -->
        
        
        <!-- BEGIN PAGE LEVEL STYLES for datatables-->
	<link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/select2/select2_metro.css" />
	<link rel="stylesheet" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/data-tables/DT_bootstrap.css" />
	<!-- END PAGE LEVEL STYLES -->
        
        <!-- BEGIN PAGE LEVEL STYLES for validation -->
	
        <link rel="stylesheet" type="text/css" href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/validation/validationEngine.jquery.css" />
      
	<!-- END PAGE LEVEL STYLES -->
        
        <!--fancybox-->
         <link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
 
        <!--fancybox ends-->
        
         <!-- Image Croping Css -->
         <link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/jrac/style.jrac.css" rel="stylesheet" type="text/css" media="all"></link>		
         <link href="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/css/jrac/base-jquery-ui.css" rel="stylesheet" type="text/css" media="all"></link>
         <!--jrac ends-->
        
        <!-- BEGIN CORE PLUGINS -->
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!--[if lt IE 9]>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/excanvas.min.js"></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/respond.min.js"></script>  
	<![endif]-->   
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>  
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript" ></script>
	<!-- END CORE PLUGINS -->
        
        <script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/ckeditor/ckeditor.js"></script>  
        <script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/ckfinder/ckfinder.js"></script>
        <script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/ckeditor/adapters/jquery.js"></script> 
        
        <script>
		jQuery(document).ready(function() {       
		
                   
                    var config = {
                        toolbar:
                        [
                                ['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
                                ['UIColor', 'Image', 'Format', 'TextColor', 'Source','PasteText']
                        ]
                   };

                    var config2 = {
                        toolbar:
								[
								['Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink', '-', 'Source'],
								['UIColor', 'Image', 'TextColor', 'BGColor','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ],
								['Styles','Format','Font','FontSize'],
								['-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl'],
								['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ]
								]
                    };

                $('.editor').ckeditor(config);
                $('.editor_full').ckeditor(config2);
                CKFinder.setupCKEditor( null, '<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/ckfinder/' );
                // Initialize the editor.
                // Callback function can be passed and executed after full instance creation.
                   
                
                   
		});
	</script>
        
	<link rel="shortcut icon" href="favicon.ico" />
        
        
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top hidden-print">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" style="margin-left:5px;color:white;width:auto" href="<?php echo make_admin_url('home', 'list', 'list');?>">
				    Webgarh Control Panel
				</a>
                                
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
				<img src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/img/menu-toggler.png" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->            
				<!-- BEGIN TOP NAVIGATION MENU -->              
				<ul class="nav pull-right">
	

					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img alt="" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/img/avatar1_small.jpg" />
						<span class="username"><?php echo ($_SESSION['admin_session_secure']['username']);?></span>
						<i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo make_admin_url('logout');?>"><i class="icon-key"></i> Log Out</a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU --> 
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
        
	<!-- BEGIN CONTAINER -->
	<div class="page-container">



