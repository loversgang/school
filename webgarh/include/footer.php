
</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="footer hidden-print">
		<div class="footer-inner">
			<?php echo date("Y")?>  &copy; <?php echo SITE_NAME?>
		</div>
		<div class="footer-tools">
			<span class="go-top">
			<i class="icon-angle-up"></i>
			</span>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	
        
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/clockface/js/clockface.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-daterangepicker/date.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script> 
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>  
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js"></script>   
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery.input-ip-address-control-1.0.min.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jquery-multi-select/js/jquery.multi-select.js"></script>   
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script> 
	<!-- END PAGE LEVEL PLUGINS -->
        
        <!-- BEGIN PAGE LEVEL PLUGINS for dattables-->

	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/data-tables/DT_bootstrap.js"></script>
	<!-- END PAGE LEVEL PLUGINS -->
        
         <!-- BEGIN PAGE LEVEL PLUGINS for validation-->
        
        <!-- Validation engine -->
        <script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/validation/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
        <script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/validation/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        
        <!--fancybox-->
        <script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>   
	<script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
 
        <!--fancybox ends-->
        
        <!-- Image Croping js -->
         <script type="text/javascript" src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/plugins/jrac/jquery.jrac.js"></script> 
         <!--jrac ends-->
        
        
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/scripts/app.js"></script>
	<script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/scripts/form-components.js"></script>   
        <script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/scripts/table-managed.js"></script>    
        <script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/scripts/ui-jqueryui.js"></script>     
        <script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/scripts/form-validation.js"></script> 
        <script src="<?=DIR_WS_SITE_PUBLIC_ASSET?>assets/scripts/gallery.js"></script>  
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
		jQuery(document).ready(function() {       
		   // initiate layout and plugins
		   App.init();
		   FormComponents.init();
                   TableManaged.init();
                   UIJQueryUI.init();
                   Gallery.init();
                   FormValidation.init();
                 
                
                   
		});
	</script>
	<!-- END JAVASCRIPTS -->   
        
        
</body>
<!-- END BODY -->
</html>