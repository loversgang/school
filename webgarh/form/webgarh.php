<!-- section coding -->
<?php


/* sections */
switch ($section):
    case 'list':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/list.php');   
		break;
    case 'ajax_plan':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/ajax_plan.php');           
	    break;	
    case 'payment':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/payment.php');           
	    break;	
    case 'setting':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/setting.php');           
	    break;	
    case 'website':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/website.php');           
	    break;		
    case 'shortcut':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/shortcut.php');           
	    break;	
    case 'insert':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/create.php');
            break;  
    case 'new_payment':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/new_payment.php');
            break;	
    case 'edit_payment':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/edit_payment.php');
            break;				
    case 'invoice':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/invoice.php');
            break; 			
    case 'update':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/edit.php');
            break;  
    case 'thrash':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/thrash.php');
           break;   
    case 'download':
            include_once(DIR_FS_SITE.ADMIN_FOLDER.'/form-template/'.$modName.'/download.php');
           break; 		   
    default:break;
endswitch;
?>