<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subscriptionPlanClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSubscriptionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/paymentHistoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSettingClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSmsClass.php');


$modName='school';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['p_id'])?$p_id=$_GET['p_id']:$p_id='0';

$SubObj = new subscriptionPlan();
$SubObj->listAll(1);

#get School Types
$QueryS_type=new schoolType;
$QueryS_type->listAll();

switch ($action):
	case'list':
		 $QueryObj = new school();
	     $QueryObj->listAll();
		 break;
	case'insert': 
			if(isset($_POST['submit'])):			
				
				#get max id for the school unique id
				$QueryUn = new school();
				$Unique_id=$QueryUn->get_last_unique_id();
				/* Add New School */
				$_POST['client_id']=$Unique_id;
				$QueryObj = new school();
				$school_id=$QueryObj->saveData($_POST);
				
				$_POST['subscription']['school_id']=$school_id;
				
				/* Add Subscription */
				$QbSub= new schoolSubscription();
				$QbSub->saveData($_POST['subscription']);
				

					
				/* Save School default SMS */
				$_POST['sms']=array('admission'=>1,'monthly_report'=>1,'present'=>1,'absent'=>1,'fee_deposit'=>1,'fee_pending'=>1,'birthday'=>1,'holiday'=>1);		

				if(!empty($_POST['sms'])):
					$QuerySms = new schoolSms();
					$QuerySms->saveData($_POST['sms'],$school_id);
				endif;
				
				/* Save School default Settings */	
				
				$_POST['max_courses_allowed']=100;
				$_POST['max_sections_per_session']=100;
				$_POST['max_teacher_allowed']=100;
				$_POST['max_student_per_session']=100;
				$_POST['is_sms_allowed']=1;
				$_POST['max_sms_limit']=10000;
				$_POST['school_id']=$school_id;				
							
				$QueryObj = new schoolSetting();
				$QueryObj->saveData($_POST);				
				
				$admin_user->set_pass_msg(MSG_CLIENT_ADDED);
				Redirect(make_admin_url('school', 'list', 'list'));				
			endif;
		break;
       
	case'update':
				/* Get School with plan*/				
				$Query = new school();
				$school=$Query->getSchoolWithPlan($id);	
			
			if(isset($_POST['submit'])):
				/* Edit School */				
				$QueryObj = new school();
				$school_id=$QueryObj->saveData($_POST);
				
				$_POST['subscription']['school_id']=$school_id;
				
				/* Edit Subscription */
				$QbSub= new schoolSubscription();
				$QbSub->saveData($_POST['subscription']);
				
				$admin_user->set_pass_msg(MSG_CLIENT_UPDATE);
				Redirect(make_admin_url('school', 'list', 'list'));				
			endif;	
		break;
	case'delete_image':
			#delete school logo
			$QueryObj = new school();
			$QueryObj->deleteImage($id);
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('school', 'update', 'update&id='.$id));	
		break;		
		
	case'delete':
			#soft delete
			$QueryObj = new school();
			$QueryObj->deleteRecord($id);
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('school', 'list', 'list'));
		break;
            
        case'permanent_delete':
			#delete school
			$QueryObj = new school();
			$QueryObj->purgeObject($id);
			
			#delete school school subscription
			$QueryNew = new schoolSubscription();
			$QueryNew->delete_where_school($id);		
			
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('school', 'thrash', 'thrash'));
        	break;
          
        case'restore':
			#restore school
            $QueryObj = new school();
            $QueryObj->restoreObject($id);
            $admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
            Redirect(make_admin_url('school', 'list', 'list'));
            break;
			
        case'payment':
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);
			
			
			/* Get School No of payment till now */				
			$QueryPay = new school();
			$no_payment=$QueryPay->getSchoolNoPayment($school->create_date,date('Y-m-d'),$school->payment_type);				
			
			#get previous payments of school
			$pay=new paymentHistory();
			$prevous_payment=$pay->get_total_school_payments($id);

			#total_payment	
			$total_payment=(($no_payment*$school->subscription_fee)+$school->set_up_fee);	
				
			#Total pending fee	
			$total_pending=($total_payment-$prevous_payment);			
			
			
			
			#list Payment History of a school
            $QueryObj = new paymentHistory();
            $QueryObj->listAll($id);

            break;
			
        case'setting':
			$smsArray=array();
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);	
			
			/* Get School setting*/				
			$QuerySchool = new schoolSetting();
			$setting=$QuerySchool->getSchoolSetting($school->id);	

			/* Get School setting*/				
			$QuerySchool = new schoolSms();
			$smsArray=$QuerySchool->getSchoolSmsArray($school->id);
			
				if(isset($_POST['submit'])): 
					/* Delete all SMS of this school first */	
					$QuerySmsDel = new schoolSms();
					$QuerySmsDel->deleteSchoolRecord($_POST['school_id']);	
					
					/* Save SMS */	
					if(!empty($_POST['sms'])):
						$QuerySms = new schoolSms();
						$QuerySms->saveData($_POST['sms'],$_POST['school_id']);
					endif;
				
					/* Save Settings */				
					$QueryObj = new schoolSetting();
					$QueryObj->saveData($_POST);
					$admin_user->set_pass_msg(MSG_CLIENT_SETTING_UPDATE);
					Redirect(make_admin_url('school', 'setting', 'setting&id='.$id));					
				endif;
            break;
			
        case'new_payment': 
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);	
			
			#list Payment History of a school
            $QueryObj = new paymentHistory();
            $QueryObj->listAll($id);			
			
				if(isset($_POST['submit'])):
					/* Add New Payment */				
					$QueryObj = new paymentHistory();
					$QueryObj->saveData($_POST);
					$admin_user->set_pass_msg(MSG_CLIENT_PAYMENT_ADDED);
					Redirect(make_admin_url('school', 'payment', 'payment&id='.$id));					
				endif;
            break;	
			
        case'edit_payment': 
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($_GET['s_id']);	
			
			/* Get Invoice detail*/				
			$QueryInvoice = new paymentHistory();
			$invoice=$QueryInvoice->getPaymentRecord($p_id);	
			
			#list Payment History of a school
            $QueryObj = new paymentHistory();
            $QueryObj->listAll($id);			
			
				if(isset($_POST['submit'])):
					/* Add New Payment */				
					$QueryObj = new paymentHistory();
					$QueryObj->saveData($_POST);
					$admin_user->set_pass_msg(MSG_CLIENT_PAYMENT_UPDATE);
					Redirect(make_admin_url('school', 'payment', 'payment&id='.$_GET['s_id']));					
				endif;
            break;			
			
        case'invoice': 
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);	
			
			/* Get Invoice detail*/				
			$QueryInvoice = new paymentHistory();
			$invoice=$QueryInvoice->getPaymentRecord($p_id);				

            break;			
        case'download':
			#download invoice 	
					Redirect(make_admin_url_window('ajax_calling','download','download','temp=school&id='.$id.'&p_id='.$p_id));
			break;
			
        case'delete_payment':
			#delete payment 			
					$QueryObj = new paymentHistory();
					$QueryObj->deleteRecord($_GET['p_id']);		
					$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
					Redirect(make_admin_url('school', 'payment', 'payment&id='.$id));			
			break;
			
        case'thrash':
			#list thrash
            $QueryObj = new school();
            $QueryObj->getThrash();
            break;
	default:break;
endswitch;
?>
