<?php
/*Include Classes*/
include_once(DIR_FS_SITE.'include/functionClass/class.php');
include_once(DIR_FS_SITE.'include/functionClass/adminUserClass.php');
include_once(DIR_FS_SITE.'include/functionClass/webgarhschoolClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolClass.php');
include_once(DIR_FS_SITE.'include/functionClass/studentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/subscriptionPlanClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSubscriptionClass.php');
include_once(DIR_FS_SITE.'include/functionClass/paymentHistoryClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSettingClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolSmsClass.php');
include_once(DIR_FS_SITE.'include/functionClass/schoolEmailClass.php');
include_once(DIR_FS_SITE.'include/functionClass/documentClass.php');
include_once(DIR_FS_SITE.'include/functionClass/themeClass.php');
include_once(DIR_FS_SITE.'include/functionClass/contentClass.php');

$modName='payment';
isset($_GET['action'])?$action=$_GET['action']:$action='list';
isset($_GET['section'])?$section=$_GET['section']:$section='list';
isset($_GET['id'])?$id=$_GET['id']:$id='0';
isset($_GET['p_id'])?$p_id=$_GET['p_id']:$p_id='0';
isset($_GET['month'])?$month=$_GET['month']:$month='';
isset($_GET['year'])?$year=$_GET['year']:$year=date('Y');


if(!empty($month)): 
$pos = strlen($month);
	if ($pos == '1'): 
	   $month='0'.$month;
	endif;   
endif;


switch ($action):
	case'list':
			$QueryObjPay = new webgarhPaymentSchool();
			$record=$QueryObjPay->getAccountPayment($month,$year);	
			
			$QueryBill = new webgarhSchool();
			$Bschool=$QueryBill->get_total_billing_school();			
			
	break;
       
	case'update':	
					
				/* Get School with plan*/				
				$Query = new school();
				$webgarh=$Query->getSchoolWithPlan($id);	
				
				
			/* Get School setting*/				
			$QuerySchool = new schoolSetting();
			$setting=$QuerySchool->getSchoolSetting($webgarh->id);	

			/* Get School sms array*/				
			$QuerySchool = new schoolSms();
			$smsArray=$QuerySchool->getSchoolSmsArray($webgarh->id);
			
			/* Get School Emails array*/				
			$QuerySchoolEmail = new schoolEmail();
			$emailArray=$QuerySchoolEmail->getSchoolEmailArray($webgarh->id);

			/* Get All Themes*/				
			$QueryTheme = new theme();
			$All_theme=$QueryTheme->listThemes('1');			
		
			
		break;
	       
      
        case'payment': 
		
				if($_REQUEST['entry']=='1'):
					#Add New Entry For this Month
					$QueryObj = new webgarhPaymentSchool();
					$QueryObj->AddMonthlyEntries($month,$year);	
				else:
					#Delete All Entry For this Month
					$QueryObj = new webgarhPaymentSchool();
					$QueryObj->DeleteMonthlyEntries($month,$year);						
				endif;
			$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
			Redirect(make_admin_url('payment', 'list', 'list&month='.$month.'&year='.$year));			
		
            break;
			
        case'setting':		

			
			$smsArray=array();
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);	
			
			/* Get School setting*/				
			$QuerySchool = new schoolSetting();
			$setting=$QuerySchool->getSchoolSetting($school->id);	

			/* Get School sms array*/				
			$QuerySchool = new schoolSms();
			$smsArray=$QuerySchool->getSchoolSmsArray($school->id);
			
			/* Get School Emails array*/				
			$QuerySchoolEmail = new schoolEmail();
			$emailArray=$QuerySchoolEmail->getSchoolEmailArray($school->id);			
			
			/* Get School document tamplate array */				
			$QuerySchDoc = new SchoolDocumentTemplate();
			$DocTemp=$QuerySchDoc->getSchoolDocumentTemplate($school->id);			
			
			
				if(isset($_POST['submit'])): 
					
					/* Check School Prefix */
					$_POST['reg_id_prefix']=strtoupper($_POST['reg_id_prefix']);					
					$QueryObj = new schoolSetting();
					$rec=$QueryObj->checkSchoolPrefix($id,$_POST['reg_id_prefix']);
					if(is_object($rec)):
						$admin_user->set_error();  
						$admin_user->set_pass_msg(MSG_REG_ID_EXIST);
						Redirect(make_admin_url('school', 'setting', 'setting&id='.$id));   					
					endif;
					
					$_POST['reg_id_prefix']=strtoupper(str_replace(' ','',$_POST['reg_id_prefix']));
				
					/* Delete all School Template Relation first */	
					$QuerySmsDel = new SchoolDocumentTemplate();
					$QuerySmsDel->deleteSchoolRecord($_POST['school_id']);	
					
					/* Save Document_rel */	
					if(!empty($_POST['doc'])):
						$QuerySms = new SchoolDocumentTemplate();
						$QuerySms->saveData($_POST['doc'],$_POST['school_id']);
					endif;				
				
				
					/* Delete all SMS of this school first */	
					$QuerySmsDel = new schoolSms();
					$QuerySmsDel->deleteSchoolRecord($_POST['school_id']);	
					
					/* Delete all Emails of this school first */	
					$QueryEmailDel = new schoolEmail();
					$QueryEmailDel->deleteSchoolRecord($_POST['school_id']);						
					
					/* Save SMS */	
					if(!empty($_POST['sms'])):
						$QuerySms = new schoolSms();
						$QuerySms->saveData($_POST['sms'],$_POST['school_id']);
					endif;

					/* Save Email */	
					if(!empty($_POST['email'])):
						$QuerySms = new schoolEmail();
						$QuerySms->saveData($_POST['email'],$_POST['school_id']);
					endif;					
				
					/* Save Settings */				
					$QueryObj = new schoolSetting();
					$QueryObj->saveData($_POST);
					$admin_user->set_pass_msg(MSG_CLIENT_SETTING_UPDATE);
					Redirect(make_admin_url('school', 'setting', 'setting&id='.$id));					
				endif;
            break;
			
        case'website':

		
			/* Get All Themes*/				
			$QueryTheme = new theme();
			$All_theme=$QueryTheme->listThemes('1');			
			

			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);	
			
				if(isset($_POST['submit'])):  
					if(!isset($_POST['theme'])):
						$_POST['theme']='';						
						if (is_dir(DIR_FS_SITE_PUBLIC.'site/'.$id)): 
							unlink(DIR_FS_SITE_PUBLIC.'/site/'.$id.'/index.php');
							unlink(DIR_FS_SITE_PUBLIC.'site/'.$id.'/.htaccess');							
							unlink(DIR_FS_SITE_PUBLIC.'site/'.$id.'/error_log');		
							@rmdir(DIR_FS_SITE_PUBLIC.'site/'.$id);							
					   endif;
					   
					   if($_POST['is_website']=='1'):	
							$_POST['is_website']='0';					   
							$QueryObj = new school();
							$QueryObj->saveData($_POST);					   
					   
							$admin_user->set_error();
							$admin_user->set_pass_msg(MSG_SELECT_WEBSITE);
							Redirect(make_admin_url('school', 'website', 'website&id='.$id));
						else:
							$_POST['is_website']='0';					   
							$QueryObj = new school();
							$QueryObj->saveData($_POST);							
					   endif;
					else:
						if (!is_dir(DIR_FS_SITE_PUBLIC.'site/'.$id)):
							@mkdir(DIR_FS_SITE_PUBLIC.'site/'.$id,0755);
							chmod(DIR_FS_SITE_PUBLIC.'site/'.$id, 0755);  // octal; correct value of mode
							copy(DIR_FS_SITE_PUBLIC.'new/index.php', DIR_FS_SITE_PUBLIC.'site/'.$id.'/index.php');
							copy(DIR_FS_SITE_PUBLIC.'new/.htaccess', DIR_FS_SITE_PUBLIC.'/site/'.$id.'/.htaccess');
					   endif;					
					endif;
					/* Save website data */				
					$QueryObj = new school();
					$QueryObj->saveData($_POST);	
					$admin_user->set_pass_msg(MSG_CLIENT_WEBSITE_UPDATE);
					Redirect(make_admin_url('school', 'website', 'website&id='.$id));					
				endif;
            break;
			
        
			
        case'invoice': 
			/* Get School with plan*/				
			$Query = new school();
			$school=$Query->getSchoolWithPlan($id);	
			
			/* Get Invoice detail*/				
			$QueryInvoice = new paymentHistory();
			$invoice=$QueryInvoice->getPaymentRecord($p_id);				

            break;			
        case'download':
			#download invoice 	
					Redirect(make_admin_url_window('ajax_calling','download','download','temp=school&id='.$id.'&p_id='.$p_id));
		
			break;			
        case'delete_payment':
			#delete payment 			
					$QueryObj = new paymentHistory();
					$QueryObj->deleteRecord($_GET['p_id']);		
					$admin_user->set_pass_msg(OPERATION_PERFORM_SUCCESS);
					Redirect(make_admin_url('school', 'payment', 'payment&id='.$id));			
			break;
			
        case'thrash':
			#list thrash

				if($login_user->type=='Affiliate' || $login_user->type=='Payment'):
					$QueryObj = new school();
					$QueryObj->getThrashWithAfilate($login_user->list_clients);
				else:
					$QueryObj = new school();
					$QueryObj->getThrash();	
				endif;			
			
            break;
	default:break;
endswitch;
?>
